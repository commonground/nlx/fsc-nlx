# Auditlog

NLX is an open source peer-to-peer system facilitating federated authentication, secure connecting and protocolling in a large-scale, dynamic API ecosystem with many organizations.
The Auditlog is a optional component which enables users expose the content of the auditlog database through an API.

## Prerequisites

-   Kubernetes 1.11+

## Installing the Chart

To install the Chart with the release name `auditlog`:

```console
## add the Common Ground Helm repository
$ helm repo add commonground https://charts.commonground.nl

## Install the nlx-auditlog helm Chart
$ helm install auditlog commonground/fsc-nlx-auditlog
```

> **Tip**: List all releases using `helm list`

## Upgrading the Chart

Currently, our Helm charts use the same release version as the NLX release version.
To know what has changed for the Helm charts, look at the changes in our [CHANGELOG](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/CHANGELOG.md)
that are prefixed with 'Helm'.

## Uninstalling the Chart

To uninstall or delete the `auditlog` deployment:

```console
$ helm delete auditlog
```

## Parameters

### Global parameters

| Name                                            | Description                                                                                                     | Value |
| ----------------------------------------------- | --------------------------------------------------------------------------------------------------------------- | ----- |
| `global.imageRegistry`                          | Global Docker Image registry                                                                                    | `""`  |
| `global.imageTag`                               | Global Docker Image tag                                                                                         | `""`  |
| `global.imagePullSecrets`                       | Global image pull secrets                                                                                       | `[]`  |
| `global.certificates.internal.caCertificatePEM` | Global root certificate of your internal PKI. If not set the value of 'tls.internal.rootCertificatePEM' is used | `""`  |

### Deployment Parameter

| Name                                | Description                                                                                                            | Value                                   |
| ----------------------------------- | ---------------------------------------------------------------------------------------------------------------------- | --------------------------------------- |
| `image.registry`                    | Image registry (ignored if 'global.imageRegistry' is set)                                                              | `docker.io`                             |
| `image.repository`                  | Image repository of the Auditlog.                                                                                      | `federatedserviceconnectivity/auditlog` |
| `image.pullPolicy`                  | Image pull policy                                                                                                      | `Always`                                |
| `image.tag`                         | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used               | `""`                                    |
| `image.pullSecrets`                 | Secrets for the image repository                                                                                       | `[]`                                    |
| `replicaCount`                      | Number of controller replicas                                                                                          | `1`                                     |
| `serviceAccount.create`             | Specifies whether a service account should be created                                                                  | `true`                                  |
| `serviceAccount.annotations`        | Annotations to add to the service account                                                                              | `{}`                                    |
| `serviceAccount.name`               | The name of the service account to use. If not set and create is true, a name is generated using the fullname template | `""`                                    |
| `podAnnotations`                    | Annotations added to the pod                                                                                           | `{}`                                    |
| `podSecurityContext.fsGroup`        | GroupID under which the pod should be started                                                                          | `1001`                                  |
| `resources`                         | Pod resource requests & limits                                                                                         | `{}`                                    |
| `nodeSelector`                      | Node labels for pod assignment                                                                                         | `{}`                                    |
| `tolerations`                       | Node tolerations for pod assignment                                                                                    | `[]`                                    |
| `affinity`                          | Node affinity for pod assignment                                                                                       | `{}`                                    |
| `securityContext.runAsNonRoot`      | Run container as a non-root user                                                                                       | `true`                                  |
| `securityContext.runAsUser`         | Run container as specified user                                                                                        | `1001`                                  |
| `securityContext.capabilities.drop` | Drop all capabilities by default                                                                                       | `["ALL"]`                               |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### FSC NLX Auditlog parameters

| Name                     | Description                                                                                                                               | Value  |
| ------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------- | ------ |
| `config.logType`         | Possible values 'live', 'local'. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger | `live` |
| `config.logLevel`        | Possible values 'debug', 'warn', 'info'. Override the default logLevel set by 'config.logType'                                            | `info` |
| `config.retentionPeriod` | Duration after which the auditlog records are removed. See https://pkg.go.dev/time#ParseDuration                                          | `720h` |

### FSC NLX PostgreSQL Parameters

| Name                            | Description                                                                                                                              | Value      |
| ------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- | ---------- |
| `db.hostname`                   | PostgreSQL hostname                                                                                                                      | `postgres` |
| `db.port`                       | PostgreSQL port                                                                                                                          | `5432`     |
| `db.database`                   | PostgreSQL database                                                                                                                      | `postgres` |
| `db.username`                   | PostgreSQL username. Will be stored in a Kubernetes secret                                                                               | `""`       |
| `db.password`                   | PostgreSQL password. Will be stored in a Kubernetes secret                                                                               | `""`       |
| `db.sslMode`                    | PostgreSQL SSL mode                                                                                                                      | `require`  |
| `db.connectTimeout`             | The connection timeout for PostgreSQL                                                                                                    | `10`       |
| `db.existingSecret.name`        | Use existing secret for password details ('postgresql.username' and 'postgresql.password' will be ignored and picked up from this secret | `""`       |
| `db.existingSecret.usernameKey` | Key for username value in aforementioned existingSecret                                                                                  | `username` |
| `db.existingSecret.passwordKey` | Key for password value in aforementioned existingSecret                                                                                  | `password` |

### Certificates parameters

| Name                                     | Description                                                                                                                                                                  | Value |
| ---------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `certificates.internal.caCertificatePEM` | The CA root certificate of your internal PKI                                                                                                                                 | `""`  |
| `certificates.internal.certificatePEM`   | The certificate signed by your internal PKI                                                                                                                                  | `""`  |
| `certificates.internal.keyPEM`           | the private key of 'certificates.internal.certificatePEM'                                                                                                                    | `""`  |
| `certificates.internal.existingSecret`   | Use of existing secret with your FSC NLX keypair ('certificates.internal.certificatePEM' and 'certificates.internal.keyPEM'. will be ingored and picked up from this secret) | `""`  |

### Exposure parameters

| Name           | Description                                      | Value       |
| -------------- | ------------------------------------------------ | ----------- |
| `service.type` | Service Type (ClusterIP, NodePort, LoadBalancer) | `ClusterIP` |
| `service.port` | Port exposed by the service                      | `8443`      |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install auditlog -f values.yaml .
```

> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/nlx-auditlog/values.yaml)
