# controller

NLX is an open source peer-to-peer system facilitating federated authentication, secure connecting and protocolling in a large-scale, dynamic API ecosystem with many organizations.
The Controller API is at the heart of NLX controller. It is used by the controller UI to manage your NLX setup and your Inways and Outways use the Controller API to retrieve their configuration.

## Prerequisites

-   Kubernetes 1.11+

## Installing the Chart

To install the Chart with the release name `controller`:

```console
## add the Common Ground Helm repository
$ helm repo add commonground https://charts.commonground.nl

## Install the nlx-controller helm Chart
$ helm install controller commonground/fsc-nlx-controller
```

> **Tip**: List all releases using `helm list`

## Upgrading the Chart

Currently, our Helm charts use the same release version as the NLX release version.
To know what has changed for the Helm charts, look at the changes in our [CHANGELOG](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/CHANGELOG.md)
that are prefixed with 'Helm'.

## Uninstalling the Chart

To uninstall or delete the `controller` deployment:

```console
$ helm delete controller
```

## Parameters

### Global parameters

| Name                                            | Description                                                                                                     | Value |
| ----------------------------------------------- | --------------------------------------------------------------------------------------------------------------- | ----- |
| `global.imageRegistry`                          | Global Docker Image registry                                                                                    | `""`  |
| `global.imageTag`                               | Global Docker Image tag                                                                                         | `""`  |
| `global.groupID`                                | Global FSC Group ID                                                                                             | `""`  |
| `global.imagePullSecrets`                       | Global image pull secrets                                                                                       | `[]`  |
| `global.certificates.group.caCertificatePEM`    | Global FSC NLX CA root certificate. If not set the value of 'tls.organization.rootCertificatePEM' is used       | `""`  |
| `global.certificates.internal.caCertificatePEM` | Global root certificate of your internal PKI. If not set the value of 'tls.internal.rootCertificatePEM' is used | `""`  |

### Deployment Parameters

| Name                         | Description                                                                                                            | Value                                     |
| ---------------------------- | ---------------------------------------------------------------------------------------------------------------------- | ----------------------------------------- |
| `image.registry`             | Image registry (ignored if 'global.imageRegistry' is set)                                                              | `docker.io`                               |
| `image.repository`           | Image repository of the controller API.                                                                                | `federatedserviceconnectivity/controller` |
| `image.tag`                  | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used               | `""`                                      |
| `image.pullPolicy`           | Image pull policy                                                                                                      | `Always`                                  |
| `image.pullSecrets`          | Secrets for the image repository                                                                                       | `[]`                                      |
| `replicaCount`               | Number of controller replicas                                                                                          | `1`                                       |
| `serviceAccount.create`      | Specifies whether a service account should be created                                                                  | `true`                                    |
| `serviceAccount.annotations` | Annotations to add to the service account                                                                              | `{}`                                      |
| `serviceAccount.name`        | The name of the service account to use. If not set and create is true, a name is generated using the fullname template | `""`                                      |
| `resources`                  | Pod resource requests & limits                                                                                         | `{}`                                      |
| `nodeSelector`               | Node labels for pod assignment                                                                                         | `{}`                                      |
| `affinity`                   | Node affinity for pod assignment                                                                                       | `{}`                                      |
| `tolerations`                | Node tolerations for pod assignment                                                                                    | `[]`                                      |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### FSC NLX Controller Parameters

| Name                                               | Description                                                                                                                                                 | Value          |
| -------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| `config.logType`                                   | Possible values 'live', 'local'. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger                   | `live`         |
| `config.logLevel`                                  | Possible values 'debug', 'warn', 'info'. Override the default logLevel set by 'config.logType'                                                              | `info`         |
| `config.groupID`                                   | The ID of the Group                                                                                                                                         | `""`           |
| `config.directoryPeerID`                           | The ID of the Peer acting as the Directory                                                                                                                  | `""`           |
| `config.managerInternalAddress`                    | Internal address of the Manager                                                                                                                             | `""`           |
| `config.auditlog.type`                             | Possible values 'stdout', 'rest'. Type of the auditlogger                                                                                                   | `stdout`       |
| `config.auditlog.rest.address`                     | address of the Audit log                                                                                                                                    | `""`           |
| `config.csrfProtection.enabled`                    | Enables protection against CSRF                                                                                                                             | `false`        |
| `config.csrfProtection.authKey`                    | A 32-bytes long authkey used to provide CSRF protection. Will be stored in a secret if set.                                                                 | `""`           |
| `config.csrfProtection.authKeyExistingSecret`      | The name of an existing secret containing a 32-bytes long authkey used to provide CSRF protection. The key must be authkey.                                 | `""`           |
| `config.authn.type`                                | Type of authentication mechanism: none or oidc                                                                                                              | `none`         |
| `config.authn.oidc.secretKey`                      | Secret key that is used for signing sessions                                                                                                                | `""`           |
| `config.authn.oidc.clientId`                       | the OIDC client ID                                                                                                                                          | `""`           |
| `config.authn.oidc.clientSecret`                   | the OIDC client secret                                                                                                                                      | `""`           |
| `config.authn.oidc.discoveryUrl`                   | the OIDC discovery URL                                                                                                                                      | `""`           |
| `config.authn.oidc.redirectUrl`                    | the OIDC redirect URL                                                                                                                                       | `""`           |
| `config.authn.oidc.logoutUrl`                      | the OIDC logout URL                                                                                                                                         | `""`           |
| `config.authn.oidc.sessionCookieSecure`            | use 'secure' cookies                                                                                                                                        | `true`         |
| `config.authn.oidc.existingSecret.name`            | Use existing secret for password details ('config.authn.oidc.secretKey' and 'config.authn.oidc.clientSecret' will be ignored and picked up from this secret | `""`           |
| `config.authn.oidc.existingSecret.secretKeyKey`    | Key for secretKey value in aforementioned existingSecret                                                                                                    | `secretKey`    |
| `config.authn.oidc.existingSecret.clientSecretKey` | Key for clientSecret value in aforementioned existingSecret                                                                                                 | `clientSecret` |
| `config.authz.type`                                | Type of authorization mechanism: rbac or rest                                                                                                               | `rbac`         |
| `config.authz.rest.address`                        | Address of the REST authorization server authorizing requests                                                                                               | `""`           |

### TLS certificates used by FSC NLX components for internal communications

| Name                                     | Description                                                                                                                                                                  | Value     |
| ---------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- |
| `certificates.internal.caCertificatePEM` | The CA root certificate of your internal PKI                                                                                                                                 | `""`      |
| `certificates.internal.certificatePEM`   | The certificate signed by your internal PKI                                                                                                                                  | `""`      |
| `certificates.internal.keyPEM`           | the private key of 'certificates.internal.certificatePEM'                                                                                                                    | `""`      |
| `certificates.internal.existingSecret`   | Use of existing secret with your FSC NLX keypair ('certificates.internal.certificatePEM' and 'certificates.internal.keyPEM'. will be ingored and picked up from this secret) | `""`      |
| `securityContext.runAsNonRoot`           | Run container as a non-root user                                                                                                                                             | `true`    |
| `securityContext.runAsUser`              | Run container as specified user                                                                                                                                              | `1001`    |
| `securityContext.capabilities.drop`      | Drop all capabilities by default                                                                                                                                             | `["ALL"]` |
| `podSecurityContext.fsGroup`             | GroupID under which the pod should be started                                                                                                                                | `1001`    |

### Exposure parameters

| Name                   | Description                                      | Value       |
| ---------------------- | ------------------------------------------------ | ----------- |
| `service.type`         | Service Type (ClusterIP, NodePort, LoadBalancer) | `ClusterIP` |
| `service.portUi`       | Port exposed by the controller UI service        | `80`        |
| `service.portInternal` | Port exposed for the internal communication      | `443`       |
| `service.portApi`      | Port exposed by the API service                  | `81`        |
| `ingress.enabled`      | Enable ingress                                   | `false`     |
| `ingress.class`        | Ingress class                                    | `""`        |
| `ingress.annotations`  | Ingress annotations                              | `{}`        |
| `ingress.hosts`        | Ingress accepted hostnames                       | `[]`        |
| `ingress.tls`          | Ingress TLS configuration                        | `[]`        |

### FSC NLX PostgreSQL Parameters

| Name                                    | Description                                                                                                                              | Value            |
| --------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| `postgresql.hostname`                   | PostgreSQL hostname                                                                                                                      | `postgresql`     |
| `postgresql.port`                       | PostgreSQL port                                                                                                                          | `5432`           |
| `postgresql.database`                   | PostgreSQL database                                                                                                                      | `nlx_controller` |
| `postgresql.username`                   | PostgreSQL username. Will be stored in a Kubernetes secret                                                                               | `""`             |
| `postgresql.password`                   | PostgreSQL password. Will be stored in a Kubernetes secret                                                                               | `""`             |
| `postgresql.sslMode`                    | PostgreSQL SSL mode                                                                                                                      | `require`        |
| `postgresql.connectTimeout`             | The connection timeout for PostgreSQL                                                                                                    | `10`             |
| `postgresql.existingSecret.name`        | Use existing secret for password details ('postgresql.username' and 'postgresql.password' will be ignored and picked up from this secret | `""`             |
| `postgresql.existingSecret.usernameKey` | Key for username value in aforementioned existingSecret                                                                                  | `username`       |
| `postgresql.existingSecret.passwordKey` | Key for password value in aforementioned existingSecret                                                                                  | `password`       |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install controller -f values.yaml .
```

> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/nlx-controller/values.yaml)
