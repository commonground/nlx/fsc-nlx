# Manager

FSC NLX Manager.

## Prerequisites

-   Kubernetes 1.11+

## Installing the Chart

To install the Chart with the release name `manager`:

```console
## add the Common Ground Helm repository
$ helm repo add commonground https://charts.commonground.nl

## Install the nlx-manager helm Chart
$ helm install manager commonground/fsc-nlx-manager
```

> **Tip**: List all releases using `helm list`

## Upgrading the Chart

Currently, our Helm charts use the same release version as the NLX release version.
To know what has changed for the Helm charts, look at the changes in our [CHANGELOG](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/CHANGELOG.md)
that are prefixed with 'Helm'.

## Uninstalling the Chart

To uninstall or delete the `manager` deployment:

```console
$ helm delete manager
```

## Parameters

### Global parameters

| Name                                                           | Description                                                                                                                    | Value |
| -------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ | ----- |
| `global.imageRegistry`                                         | Global Docker Image registry                                                                                                   | `""`  |
| `global.imageTag`                                              | Global Docker Image tag                                                                                                        | `""`  |
| `global.groupID`                                               | Global FSC Group ID                                                                                                            | `""`  |
| `global.imagePullSecrets`                                      | Global image pull secrets                                                                                                      | `[]`  |
| `global.certificates.group.caCertificatePEM`                   | Global FSC NLX CA root certificate. If not set the value of 'tls.organization.rootCertificatePEM' is used                      | `""`  |
| `global.certificates.internal.caCertificatePEM`                | Global root certificate of your internal PKI. If not set the value of 'tls.internal.rootCertificatePEM' is used                | `""`  |
| `global.certificates.internalUnauthenticated.caCertificatePEM` | Global root certificate of your internal PKI. If not set the value of 'tls.internalUnauthenticated.rootCertificatePEM' is used | `""`  |

### Deployment Parameter

| Name                                | Description                                                                                                            | Value                                  |
| ----------------------------------- | ---------------------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| `image.registry`                    | Image registry (ignored if 'global.imageRegistry' is set)                                                              | `docker.io`                            |
| `image.repository`                  | Image repository of the manager API.                                                                                   | `federatedserviceconnectivity/manager` |
| `image.tag`                         | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used               | `""`                                   |
| `image.pullPolicy`                  | Image pull policy                                                                                                      | `Always`                               |
| `image.pullSecrets`                 | Secrets for the image repository                                                                                       | `[]`                                   |
| `serviceAccount.create`             | Specifies whether a service account should be created                                                                  | `true`                                 |
| `serviceAccount.annotations`        | Annotations to add to the service account                                                                              | `{}`                                   |
| `serviceAccount.name`               | The name of the service account to use. If not set and create is true, a name is generated using the fullname template | `""`                                   |
| `replicaCount`                      | Number of controller replicas                                                                                          | `1`                                    |
| `podAnnotations`                    | annotations added to the pod                                                                                           | `{}`                                   |
| `podSecurityContext.fsGroup`        | GroupID under which the pod should be started                                                                          | `1001`                                 |
| `securityContext.runAsNonRoot`      | Run container as a non-root user                                                                                       | `true`                                 |
| `securityContext.runAsUser`         | Run container as specified user                                                                                        | `1001`                                 |
| `securityContext.capabilities.drop` | Drop all capabilities by default                                                                                       | `["ALL"]`                              |
| `resources`                         | Pod resource requests & limits                                                                                         | `{}`                                   |
| `nodeSelector`                      | Node labels for pod assignment                                                                                         | `{}`                                   |
| `tolerations`                       | Node tolerations for pod assignment                                                                                    | `[]`                                   |
| `extraEnv`                          | Extra env items for pod assignment                                                                                     | `[]`                                   |
| `affinity`                          | Node affinity for pod assignment                                                                                       | `{}`                                   |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### FSC NLX Manager parameters

| Name                                        | Description                                                                                                                                                                                                             | Value    |
| ------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- |
| `config.logType`                            | Possible values 'live', 'local'. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger                                                                               | `live`   |
| `config.logLevel`                           | Possible values 'debug', 'warn', 'info'. Override the default logLevel set by 'config.logType'                                                                                                                          | `info`   |
| `config.groupID`                            | FSC Group ID                                                                                                                                                                                                            | `""`     |
| `config.selfAddress`                        | The Address that can be used by the FSC NLX Group to reach this Manager                                                                                                                                                 | `""`     |
| `config.controllerAPIAddress`               | The address of the Controller API                                                                                                                                                                                       | `""`     |
| `config.txLogAPIAddress`                    | The Address of the Transaction Log API. Can be empty when this Manager functions as the Directory of the Group                                                                                                          | `""`     |
| `config.tokenTTL`                           | Duration of token validity, how long is the time to live for the generated tokens, format is specified in string, e.g. '1h', '300s', or '5m'## @param config.directoryPeerID The ID of the Peer acting as the Directory | `1h`     |
| `config.directoryPeerID`                    | The ID of the Peer acting as the Directory                                                                                                                                                                              | `""`     |
| `config.directoryManagerAddress`            | The address of the Manager acting as the Directory                                                                                                                                                                      | `""`     |
| `config.autoSignGrants`                     | The contracts containing grants which should be signed automatically                                                                                                                                                    | `[]`     |
| `config.autoSignPolicyDecisionPointAddress` | Address of the policy decision point which decides if a submitted Contract should get an automatic accept signature                                                                                                     | `""`     |
| `config.auditlog.type`                      | Possible values 'stdout', 'rest'. Type of the auditlogger                                                                                                                                                               | `stdout` |
| `config.auditlog.rest.address`              | address of the Audit log                                                                                                                                                                                                | `""`     |

### Certificates parameters

| Name                                                    | Description                                                                                                                                                                           | Value |
| ------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `certificates.internal.caCertificatePEM`                | The CA root certificate of your internal PKI                                                                                                                                          | `""`  |
| `certificates.internal.certificatePEM`                  | The certificate signed by your internal PKI                                                                                                                                           | `""`  |
| `certificates.internal.keyPEM`                          | the private key of 'certificates.internal.certificatePEM'                                                                                                                             | `""`  |
| `certificates.internal.existingSecret`                  | Use of existing secret with your FSC NLX keypair ('certificates.internal.certificatePEM' and 'certificates.internal.keyPEM'. will be ingored and picked up from this secret)A         | `""`  |
| `certificates.internalUnauthenticated.caCertificatePEM` | the CA certificate used by the unauthenticated internal service                                                                                                                       | `""`  |
| `certificates.internalUnauthenticated.certificatePEM`   | the certificate used in the unauthenticated internal service                                                                                                                          | `""`  |
| `certificates.internalUnauthenticated.keyPEM`           | the private key used in the unauthenticated internal service                                                                                                                          | `""`  |
| `certificates.internalUnauthenticated.existingSecret`   | use existing secret with the FSC NLX keypair used for the unauthenticated endpoint                                                                                                    | `""`  |
| `certificates.group.caCertificatePEM`                   | The CA certificate of the Group                                                                                                                                                       | `""`  |
| `certificates.group.peer.keyPEM`                        | Private Key of 'certificates.group.peer.certificatePEM'                                                                                                                               | `""`  |
| `certificates.group.peer.existingSecret`                | Use existing secret with your FSC NLX keypair (`certificates.group.peer.certificatePEM` and `certificates.group.peer.keyPEM` will be ignored and picked up from the secret)           | `""`  |
| `certificates.group.peer.certificatePEM`                | PEM certificates for the peer                                                                                                                                                         | `""`  |
| `certificates.group.token.keyPEM`                       | Private Key of 'certificates.group.token.certificatePEM'                                                                                                                              | `""`  |
| `certificates.group.token.existingSecret`               | Use existing secret with your FSC NLX keypair (`certificates.group.token.certificatePEM` and `certificates.group.token.keyPEM` will be ignored and picked up from the secret)         | `""`  |
| `certificates.group.token.certificatePEM`               | PEM certificates for the signing tokens                                                                                                                                               | `""`  |
| `certificates.group.signature.certificatePEM`           | The certificate used to create signatures for Contracts                                                                                                                               | `""`  |
| `certificates.group.signature.keyPEM`                   | Private Key of 'certificates.group.signature.certificatePEM'                                                                                                                          | `""`  |
| `certificates.group.signature.existingSecret`           | Use existing secret with your FSC NLX keypair (`certificates.group.signature.certificatePEM` and `certificates.group.signature.keyPEM` will be ignored and picked up from the secret) | `""`  |

### FSC NLX PostgreSQL Parameters

| Name                                    | Description                                                                                                                              | Value         |
| --------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- | ------------- |
| `postgresql.hostname`                   | PostgreSQL hostname                                                                                                                      | `postgresql`  |
| `postgresql.port`                       | PostgreSQL port                                                                                                                          | `5432`        |
| `postgresql.database`                   | PostgreSQL database                                                                                                                      | `nlx_manager` |
| `postgresql.username`                   | PostgreSQL username. Will be stored in a Kubernetes secret                                                                               | `""`          |
| `postgresql.password`                   | PostgreSQL password. Will be stored in a Kubernetes secret                                                                               | `""`          |
| `postgresql.sslMode`                    | PostgreSQL SSL mode                                                                                                                      | `require`     |
| `postgresql.connectTimeout`             | The connection timeout for PostgreSQL                                                                                                    | `10`          |
| `postgresql.existingSecret.name`        | Use existing secret for password details ('postgresql.username' and 'postgresql.password' will be ignored and picked up from this secret | `""`          |
| `postgresql.existingSecret.usernameKey` | Key for username value in aforementioned existingSecret                                                                                  | `username`    |
| `postgresql.existingSecret.passwordKey` | Key for password value in aforementioned existingSecret                                                                                  | `password`    |

### Exposure parameters

| Name                                   | Description                                          | Value       |
| -------------------------------------- | ---------------------------------------------------- | ----------- |
| `service.external.type`                | Service Type (ClusterIP, NodePort, LoadBalancer)     | `ClusterIP` |
| `service.external.port`                | Port exposed by the external service                 | `8443`      |
| `service.external.annotations`         | annotations added to the external service            | `{}`        |
| `service.internal.type`                | Service Type (ClusterIP, NodePort, LoadBalancer)     | `ClusterIP` |
| `service.internal.port`                | Port exposed by the internal service                 | `8080`      |
| `service.internalUnauthenticated.type` | Service Type (ClusterIP, NodePort, LoadBalancer)     | `ClusterIP` |
| `service.internalUnauthenticated.port` | Port exposed by the internal unauthenticated service | `8081`      |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install manager -f values.yaml .
```

> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/nlx-manager/values.yaml)
