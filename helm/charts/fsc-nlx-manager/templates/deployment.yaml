apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "nlx-manager.fullname" . }}
  labels:
    {{- include "nlx-manager.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "nlx-manager.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "nlx-manager.selectorLabels" . | nindent 8 }}
    spec:
      imagePullSecrets:
        {{- include "nlx-manager.imagePullSecrets" . }}
      serviceAccountName: {{ include "nlx-manager.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ template "nlx-manager.image" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: internal
              containerPort: 8080
              protocol: TCP
            - name: internal-unauth
              containerPort: 8081
              protocol: TCP
            - name: external
              containerPort: 8443
              protocol: TCP
            - name: monitoring
              containerPort: 8082
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /health/live
              port: monitoring
            initialDelaySeconds: 3
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /health/ready
              port: monitoring
            initialDelaySeconds: 3
            periodSeconds: 10
          env:
            - name: LOG_TYPE
              value: {{ .Values.config.logType }}
            - name: LOG_LEVEL
              value: {{ .Values.config.logLevel }}
            - name: LISTEN_ADDRESS_EXTERNAL
              value: 0.0.0.0:8443
            - name: LISTEN_ADDRESS_INTERNAL
              value: 0.0.0.0:8080
            - name: LISTEN_ADDRESS_INTERNAL_UNAUTHENTICATED
              value: 0.0.0.0:8081
            - name: MONITORING_ADDRESS
              value: 0.0.0.0:8082
            - name: GROUP_ID
              value: {{ default .Values.config.groupID .Values.global.groupID }}
            - name: SELF_ADDRESS
              value: {{ .Values.config.selfAddress }}
            - name: CONTROLLER_API_ADDRESS
              value: {{ .Values.config.controllerAPIAddress }}
            - name: DIRECTORY_PEER_ID
              value: {{ .Values.config.directoryPeerID | quote }}
            - name: DIRECTORY_MANAGER_ADDRESS
              value: {{ .Values.config.directoryManagerAddress }}
            {{- if .Values.config.autoSignGrants }}
            - name: AUTO_SIGN_GRANTS
              value: {{ join "," .Values.config.autoSignGrants }}
            {{- end }}
            {{- if .Values.config.txLogAPIAddress }}
            - name: TX_LOG_API_ADDRESS
              value: {{ .Values.config.txLogAPIAddress }}
            {{- end }}
            - name: TOKEN_TTL
              value: {{ .Values.config.tokenTTL | default "1h" }}
            - name: TLS_CERT
              value: /certificate-internal/tls.crt
            - name: TLS_KEY
              value: /certificate-internal/tls.key
            - name: TLS_ROOT_CERT
              value: /config/root-internal.pem
            - name: TLS_INTERNAL_UNAUTHENTICATED_CERT
              value: /certificate-internal-unauthenticated/tls.crt
            - name: TLS_INTERNAL_UNAUTHENTICATED_KEY
              value: /certificate-internal-unauthenticated/tls.key
            - name: TLS_INTERNAL_UNAUTHENTICATED_ROOT_CERT
              value: /config/root-internal-unauthenticated.pem
            - name: TLS_GROUP_ROOT_CERT
              value: /config/root-group.pem
            - name: TLS_GROUP_CERT
              value: /certificate-peer/tls.crt
            - name: TLS_GROUP_KEY
              value: /certificate-peer/tls.key
            - name: TLS_GROUP_TOKEN_CERT
              value: /certificate-token/tls.crt
            - name: TLS_GROUP_TOKEN_KEY
              value: /certificate-token/tls.key
            - name: TLS_GROUP_CONTRACT_CERT
              value: /certificate-signature/tls.crt
            - name: TLS_GROUP_CONTRACT_KEY
              value: /certificate-signature/tls.key
            - name: AUDITLOG_TYPE
              value: {{ .Values.config.auditlog.type }}
            {{- if eq .Values.config.auditlog.type "rest" }}
            - name: AUDITLOG_REST_ADDRESS
              value: {{ required "Auditlog address is required when auditlog type is 'rest'" .Values.config.auditlog.rest.address }}
            {{- end }}
            - name: POSTGRES_HOST
              value: {{ required "PostgreSQL hostname is required" .Values.postgresql.hostname }}
            - name: POSTGRES_PORT
              value: {{ required "PostgreSQL port number is required" .Values.postgresql.port | quote }}
            - name: POSTGRES_DATABASE
              value: {{ required "PostgreSQL database name is required" .Values.postgresql.database }}
            - name: PGSSLMODE
              value: {{ .Values.postgresql.sslMode }}
            - name: PGCONNECT_TIMEOUT
              value: {{ .Values.postgresql.connectTimeout | quote }}
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: {{ template "nlx-manager.postgresql.secret" . }}
                  key: {{ .Values.postgresql.existingSecret.usernameKey }}
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ template "nlx-manager.postgresql.secret" . }}
                  key: {{ .Values.postgresql.existingSecret.passwordKey }}
            - name: STORAGE_POSTGRES_DSN
              value: 'postgres://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(POSTGRES_HOST):$(POSTGRES_PORT)/$(POSTGRES_DATABASE)'
          {{- if .Values.config.autoSignPolicyDecisionPointAddress }}
            - name: AUTO_SIGN_PDP_ADDRESS
              value: {{ .Values.config.autoSignPolicyDecisionPointAddress }}
            - name: AUTO_SIGN_PDP_ROOT_CA
              value: /config/root-internal.pem
          {{- end }}
          {{- if .Values.extraEnv }}
            {{- toYaml .Values.extraEnv | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: certificate-internal
              mountPath: /certificate-internal
            - name: certificate-internal-unauthenticated
              mountPath: /certificate-internal-unauthenticated
            - name: certificate-peer
              mountPath: /certificate-peer
            - name: certificate-token
              mountPath: /certificate-token
            - name: certificate-signature
              mountPath: /certificate-signature
            - name: config
              mountPath: /config
              readOnly: true
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      initContainers:
        - name: migrations
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ template "nlx-manager.image" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          args:
            - '/usr/local/bin/nlx-manager'
            - migrate
            - up
          env:
            - name: POSTGRES_HOST
              value: {{ required "PostgreSQL hostname is required" .Values.postgresql.hostname }}
            - name: POSTGRES_PORT
              value: {{ required "PostgreSQL port number is required" .Values.postgresql.port | quote }}
            - name: POSTGRES_DATABASE
              value: {{ required "PostgreSQL database name is required" .Values.postgresql.database }}
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: {{ template "nlx-manager.postgresql.secret" . }}
                  key: {{ .Values.postgresql.existingSecret.usernameKey }}
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ template "nlx-manager.postgresql.secret" . }}
                  key: {{ .Values.postgresql.existingSecret.passwordKey }}
            - name: PGSSLMODE
              value: {{ .Values.postgresql.sslMode }}
            - name: PGCONNECT_TIMEOUT
              value: {{ .Values.postgresql.connectTimeout | quote }}
            - name: POSTGRES_DSN
              value: 'postgres://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(POSTGRES_HOST):$(POSTGRES_PORT)/$(POSTGRES_DATABASE)'
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        - name: certificate-peer
          secret:
            secretName: {{ default (printf "%s-certificate-peer" (include "nlx-manager.fullname" .)) .Values.certificates.group.peer.existingSecret }}
            defaultMode: 0640
        - name: certificate-internal
          secret:
            secretName: {{ default (printf "%s-certficate-internal" (include "nlx-manager.fullname" .)) .Values.certificates.internal.existingSecret }}
            defaultMode: 0640
        - name: certificate-internal-unauthenticated
          secret:
            secretName: {{ default (printf "%s-certificate-internal-unauthenticated" (include "nlx-manager.fullname" .)) .Values.certificates.internalUnauthenticated.existingSecret }}
            defaultMode: 0640
        - name: certificate-token
          secret:
            secretName: {{ default (printf "%s-certificates-token" (include "nlx-manager.fullname" .)) .Values.certificates.group.token.existingSecret }}
            defaultMode: 0640
        - name: certificate-signature
          secret:
            secretName: {{ default (printf "%s-certificates-signature" (include "nlx-manager.fullname" .)) .Values.certificates.group.signature.existingSecret }}
            defaultMode: 0640
        - name: config
          configMap:
            name: {{ template "nlx-manager.fullname" . }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
