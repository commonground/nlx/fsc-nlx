# nlx-directory-ui

The NLX Directory provides an overview of the Peers who are participant in an NLX Group.

## Prerequisites

- Kubernetes 1.11+

## Installing the Chart

To install the Chart with the release name `nlx-directory-ui`:

```console
## add the Common Ground Helm repository
$ helm repo add commonground https://charts.commonground.nl

## Install the nlx-directory-ui helm Chart
$ helm install nlx-directory-ui commonground/fsc-nlx-directory-ui
```

> **Tip**: List all releases using `helm list`

## Upgrading the Chart

Currently, our Helm charts use the same release version as the FSC NLX release version.
To know what has changed for the Helm charts, look at the changes in our [CHANGELOG](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/CHANGELOG.md)
that are prefixed with 'Helm'.

## Uninstalling the Chart

To uninstall or delete the `nlx-directory-ui` deployment:

```console
$ helm delete nlx-directory-ui
```

## Parameters

### Global parameters

| Name                                         | Description                                                                                               | Value |
| -------------------------------------------- | --------------------------------------------------------------------------------------------------------- | ----- |
| `global.imageRegistry`                       | Global Docker Image registry                                                                              | `""`  |
| `global.imageTag`                            | Global Docker Image tag                                                                                   | `""`  |
| `global.imagePullSecrets`                    | Global image pull secrets                                                                                 | `[]`  |
| `global.certificates.group.caCertificatePEM` | Global FSC-NLX CA root certificate. If not set the value of 'tls.organization.rootCertificatePEM' is used | `""`  |

### Deployment Parameters

| Name                                | Description                                                                                                            | Value                                       |
| ----------------------------------- | ---------------------------------------------------------------------------------------------------------------------- | ------------------------------------------- |
| `image.registry`                    | Image registry (ignored if 'global.imageRegistry' is set)                                                              | `docker.io`                                 |
| `image.repository`                  | Image repository of the controller API.                                                                                | `federatedserviceconnectivity/directory-ui` |
| `image.tag`                         | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used               | `""`                                        |
| `image.pullPolicy`                  | Image pull policy                                                                                                      | `Always`                                    |
| `image.pullSecrets`                 | Secrets for the image repository                                                                                       | `[]`                                        |
| `serviceAccount.create`             | Specifies whether a service account should be created                                                                  | `true`                                      |
| `serviceAccount.annotations`        | Annotations to add to the service account                                                                              | `{}`                                        |
| `serviceAccount.name`               | The name of the service account to use. If not set and create is true, a name is generated using the fullname template | `""`                                        |
| `replicaCount`                      | Number of controller replicas                                                                                          | `1`                                         |
| `podSecurityContext.fsGroup`        | GroupID under which the pod should be started                                                                          | `1001`                                      |
| `securityContext.runAsNonRoot`      | Run container as a non-root user                                                                                       | `true`                                      |
| `securityContext.runAsUser`         | Run container as specified user                                                                                        | `1001`                                      |
| `securityContext.capabilities.drop` | Drop all capabilities by default                                                                                       | `["ALL"]`                                   |
| `resources`                         | Pod resource requests & limits                                                                                         | `{}`                                        |
| `nodeSelector`                      | Node labels for pod assignment                                                                                         | `{}`                                        |
| `affinity`                          | Node affinity for pod assignment                                                                                       | `{}`                                        |
| `tolerations`                       | Node tolerations for pod assignment                                                                                    | `[]`                                        |
| `annotations`                       | Annotations added to the deployment                                                                                    | `{}`                                        |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### FSC NLX directory parameters

| Name                                  | Description                                                                                                                                                       | Value  |
| ------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------ |
| `config.logType`                      | Possible values 'live', 'local'. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger                         | `live` |
| `config.logLevel`                     | Possible values 'debug', 'warn', 'info'. Override the default logLevel set by 'config.logType'                                                                    | `info` |
| `config.directoryManagerAddress`      | Address of the manager component of the directory                                                                                                                 | `""`   |
| `config.baseURLPath`                  | URL path the UI is served from                                                                                                                                    | `/`    |
| `certificates.group.caCertificatePEM` | The CA certificate of the Group                                                                                                                                   | `""`   |
| `certificates.group.certificatePEM`   | The Group certificate                                                                                                                                             | `""`   |
| `certificates.group.keyPEM`           | Private Key of 'certificates.group.certificatePEM'                                                                                                                | `""`   |
| `certificates.group.existingSecret`   | Use existing secret with your FSC-NLX keypair (`certificates.group.certificatePEM` and `certificates.group.keyPEM` will be ignored and picked up from the secret) | `""`   |

### Exposure parameters

| Name                  | Description                                      | Value       |
| --------------------- | ------------------------------------------------ | ----------- |
| `service.type`        | Service Type (ClusterIP, NodePort, LoadBalancer) | `ClusterIP` |
| `service.port`        | Port exposed by the service\                     | `80`        |
| `service.annotations` | Annotations to be included in the                | `{}`        |
| `ingress.class`       | Ingress class                                    | `""`        |
| `ingress.annotations` | Ingress annotations                              | `{}`        |
| `ingress.hosts`       | Ingress accepted hostnames                       | `[]`        |
| `ingress.tls`         | Ingress TLS configuration                        | `[]`        |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install nlx-directory-ui -f values.yaml .
```
> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/nlx-directory/values.yaml)
