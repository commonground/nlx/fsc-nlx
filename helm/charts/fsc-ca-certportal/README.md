# ca-certportal

This is the Chart for the FSC NLX ca-certportal. It provides a
web interface that allows requesting a testing certificate easily.

## Prerequisites

- Kubernetes 1.11+

## Installing the Chart

This chart is created for internal use only.

## Parameters

### Global parameters

| Name                      | Description                  | Value |
| ------------------------- | ---------------------------- | ----- |
| `global.imageRegistry`    | Global Docker Image registry | `""`  |
| `global.imageTag`         | Global Docker Image tag      | `""`  |
| `global.imagePullSecrets` | Global image pull secrets    | `[]`  |

### Deployment Parameters

| Name                                | Description                                                                                                                  | Value                                        |
| ----------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| `image.registry`                    | Image registry (ignored if 'global.imageRegistry' is set)                                                                    | `docker.io`                                  |
| `image.repository`                  | Image repository of the controller API.                                                                                      | `federatedserviceconnectivity/ca-certportal` |
| `image.tag`                         | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used                     | `""`                                         |
| `image.pullPolicy`                  | Image pull policy                                                                                                            | `Always`                                     |
| `image.pullSecrets`                 | Secrets for the image repository                                                                                             | `[]`                                         |
| `replicaCount`                      | Number of controller replicas                                                                                                | `1`                                          |
| `serviceAccount.create`             | If 'true' create a new service account                                                                                       | `true`                                       |
| `serviceAccount.name`               | Service account to be used. If not set and 'ServiceAccount.create' is 'true' a name is generated using the fullname template | `""`                                         |
| `serviceAccount.annotations`        | Annotations to add to the service account                                                                                    | `{}`                                         |
| `podSecurityContext`                | Security context for the pod                                                                                                 | `{}`                                         |
| `securityContext.runAsNonRoot`      | Run container as a non-root user                                                                                             | `true`                                       |
| `securityContext.runAsUser`         | Run container as specified user                                                                                              | `1001`                                       |
| `securityContext.capabilities.drop` | Drop all capabilities by default                                                                                             | `["ALL"]`                                    |
| `resources`                         | Pod resource requests & limits                                                                                               | `{}`                                         |
| `nodeSelector`                      | Node labels for pod assignment                                                                                               | `{}`                                         |
| `affinity`                          | Node affinity for pod assignment                                                                                             | `{}`                                         |
| `tolerations`                       | Node tolerations for pod assignment                                                                                          | `[]`                                         |

### CA Cert Portal parameters

| Name              | Description                                                                                                                                 | Value  |
| ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ------ |
| `config.logType`  | Possible values: 'live', 'local'. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger. | `live` |
| `config.logLevel` | Possible values 'debug', 'warn', 'info'. Override the default logLevel set by 'config.logType'                                              | `info` |
| `config.caHost`   | The host of the Certificate Authority.                                                                                                      | `""`   |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### Exposure parameters

| Name                  | Description                                       | Value       |
| --------------------- | ------------------------------------------------- | ----------- |
| `service.type`        | Service Type (ClusterIP, NodePort, LoadBalancer)  | `ClusterIP` |
| `service.port`        | Port exposed by the service for the apps-overview | `8080`      |
| `ingress.enabled`     | Enable ingress                                    | `false`     |
| `ingress.class`       | Ingress class                                     | `""`        |
| `ingress.annotations` | Ingress annotations                               | `{}`        |
| `ingress.hosts`       | Ingress accepted hostnames                        | `[]`        |
| `ingress.tls`         | Ingress TLS configuration                         | `[]`        |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install ca-certportal -f values.yaml .
```
> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/ca-certportal/values.yaml)
