apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "nlx-inway.fullname" . }}
  labels:
    {{- include "nlx-inway.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 1
  selector:
    matchLabels:
      {{- include "nlx-inway.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "nlx-inway.selectorLabels" . | nindent 8 }}
      annotations:
        checksum/configmap: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        checksum/secret-tls: {{ include (print $.Template.BasePath "/secret-tls.yaml") . | sha256sum }}
    spec:
      imagePullSecrets:
        {{- include "nlx-inway.imagePullSecrets" . }}
      serviceAccountName: {{ include "nlx-inway.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: nlx-inway
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ template "nlx-inway.image" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: https
              containerPort: 8443
              protocol: TCP
            - name: management
              containerPort: 8444
              protocol: TCP
            - name: monitoring
              containerPort: 8081
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /health/live
              port: monitoring
            initialDelaySeconds: 3
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /health/ready
              port: monitoring
            initialDelaySeconds: 3
            periodSeconds: 10
          env:
            - name: LISTEN_ADDRESS
              value: 0.0.0.0:8443
            - name: MONITORING_ADDRESS
              value: 0.0.0.0:8081
            - name: GROUP_ID
              value: {{ default .Values.config.groupID .Values.global.groupID }}
            - name: NAME
              value: {{ default (include "nlx-inway.fullname" .) .Values.config.name }}
            - name: SELF_ADDRESS
              value: {{ include "nlx-inway.selfAddress" . }}
            - name: MANAGER_INTERNAL_UNAUTHENTICATED_ADDRESS
              value: {{ .Values.config.managerInternalUnauthenticatedAddress }}
            - name: CONTROLLER_API_ADDRESS
              value: {{ .Values.config.controllerApiAddress }}
            - name: TLS_ROOT_CERT
              value: /config/root-internal.pem
            - name: TLS_CERT
              value: /certificate-internal/tls.crt
            - name: TLS_KEY
              value: /certificate-internal/tls.key
            - name: TX_LOG_API_ADDRESS
              value: {{ required "Transaction Log API address is required" .Values.config.transactionLogApiAddress }}
          {{- if .Values.config.authorizationService.enabled }}
            - name: AUTHORIZATION_SERVICE_ADDRESS
              value: {{ .Values.config.authorizationService.url }}
            - name: AUTHORIZATION_ROOT_CA
              value: /config/root-internal.pem
            - name: AUTHORIZATION_WITH_BODY
              value: {{ quote .Values.config.authorizationService.withBody }}
            - name: AUTHORIZATION_MAX_BODY_SIZE
              value: {{ quote .Values.config.authorizationService.maxBodySize }}
            - name: AUTHORIZATION_BODY_CHUNK_SIZE
              value: {{ quote .Values.config.authorizationService.bodyChunkSize }}
          {{- end }}
          {{- if .Values.config.serviceProxyCacheSize }}
            - name: SERVICE_PROXY_CACHE_SIZE
              value: {{ .Values.config.serviceProxyCacheSize | quote }}
          {{- end }}
            - name: TLS_GROUP_ROOT_CERT
              value: "/config/root-group.pem"
            - name: TLS_GROUP_CERT
              value: "/certificate-group/tls.crt"
            - name: TLS_GROUP_KEY
              value: "/certificate-group/tls.key"
            - name: LOG_TYPE
              value: {{ .Values.config.logType }}
            - name: LOG_LEVEL
              value: {{ .Values.config.logLevel }}
          {{- if .Values.extraEnv }}
            {{- toYaml .Values.extraEnv | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: certificate-group
              mountPath: /certificate-group
            - name: certificate-internal
              mountPath: /certificate-internal
            - name: config
              mountPath: /config
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        - name: certificate-group
          secret:
            secretName: {{ default (printf "%s-group-tls" (include  "nlx-inway.fullname" .)) .Values.certificates.group.existingSecret }}
            defaultMode: 0640
        - name: certificate-internal
          secret:
            secretName: {{ default (printf "%s-internal-tls" (include "nlx-inway.fullname" .)) .Values.certificates.internal.existingSecret }}
            defaultMode: 0640
        - name: config
          configMap:
            name: {{ template "nlx-inway.fullname" . }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
