# keycloak

FSC-NLX is an open source peer-to-peer system facilitating federated authentication, secure connecting and protocolling in a large-scale, dynamic API ecosystem with many organizations.
The `Controller` component uses OIDC as a means of login for administrators. For this a OIDC IDP is required. `Keycloak` is used as IDP for the `Controller`

## Prerequisites

-   Kubernetes 1.11+

## Installing the Chart

To install the Chart with the release name `keycloak`:

```console
## add the Common Ground Helm repository
$ helm repo add commonground https://charts.commonground.nl

## Install the nlx-keycloak helm Chart
$ helm install keycloak commonground/fsc-nlx-keycloak
```

> **Tip**: List all releases using `helm list`

## Upgrading the Chart

Currently, our Helm charts use the same release version as the NLX release version.
To know what has changed for the Helm charts, look at the changes in our [CHANGELOG](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/CHANGELOG.md)
that are prefixed with 'Helm'.

## Uninstalling the Chart

To uninstall or delete the `keycloak` deployment:

```console
$ helm delete keycloak
```

## Parameters

### Deployment Parameters

| Name                         | Description                                                                                                                                                                                | Value                              |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------- |
| `image.registry`             | Image registry (ignored if 'global.imageRegistry' is set)                                                                                                                                  | `registry.gitlab.com`              |
| `image.repository`           | Image repository of the docs API.                                                                                                                                                          | `commonground/nlx/images/keycloak` |
| `image.tag`                  | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used                                                                                   | `76a9edf3`                         |
| `image.pullPolicy`           | Image pull policy                                                                                                                                                                          | `Always`                           |
| `image.pullSecrets`          | Secrets for the image repository                                                                                                                                                           | `[]`                               |
| `replicaCount`               | Number of controller replicas                                                                                                                                                              | `1`                                |
| `serviceAccount.create`      | Specifies whether a service account should be created                                                                                                                                      | `true`                             |
| `serviceAccount.annotations` | Annotations to add to the service account                                                                                                                                                  | `{}`                               |
| `serviceAccount.name`        | The name of the service account to use. If not set and create is true, a name is generated using the fullname template                                                                     | `""`                               |
| `podSecurityContext`         | Security context for the pod                                                                                                                                                               | `{}`                               |
| `securityContext`            | Optional security context. The YAML block should adhere to the [SecurityContext spec](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16/#securitycontext-v1-core) | `{}` | `{}`                               |
| `resources`                  | Pod resource requests & limits                                                                                                                                                             | `{}`                               |
| `nodeSelector`               | Node labels for pod assignment                                                                                                                                                             | `{}`                               |
| `affinity`                   | Node affinity for pod assignment                                                                                                                                                           | `{}`                               |
| `tolerations`                | Node tolerations for pod assignment                                                                                                                                                        | `[]`                               |
| `extraEnv`                   | Extra env items for pod assignment                                                                                                                                                         | `[]`                               |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### FSC NLX Keycloak parameters

| Name                          | Description                            | Value                                             |
| ----------------------------- | -------------------------------------- | ------------------------------------------------- |
| `config.adminUsername`        | username of the admin user             | `keycloak-admin`                                  |
| `config.adminPassword`        | password of the admin user             | `keycloak`                                        |
| `config.keycloakHostURL`      | URL of the client login/logout portals | `""`                                              |
| `config.keycloakHostAdminURL` | URL of the admin portal                | `""`                                              |
| `config.javaOptsHeap`         | Java Heap memory configuration         | `-XX:MaxHeapFreeRatio=30 -XX:MaxRAMPercentage=65` |

### Exposure parameters

| Name                     | Description                                                                                                                                                                                                                                                                                                                                      | Value          |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------- |
| `service.type`           | Service Type (ClusterIP, NodePort, LoadBalancer)                                                                                                                                                                                                                                                                                                 | `LoadBalancer` |
| `service.annotations`    | Annotations to be included in the                                                                                                                                                                                                                                                                                                                | `{}`           |
| `service.port`           | Port exposed by the service                                                                                                                                                                                                                                                                                                                      | `8080`         |
| `service.nodePort`       | Port exposed for Inway traffic if 'service.type' is 'NodePort'                                                                                                                                                                                                                                                                                   | `8080`         |
| `service.loadBalancerIP` | Only applies when using 'service.type' 'LoadBalancer'. A loadBalancer will get created with the IP specified in this field. This feature depends on whether the underlying cloud-provider supports specifying the loadbalancerIP when a load balancer is created. This field will be ignored if the cloud-provider does not support the feature. | `""`           |
| `ingress.enabled`        | Enable ingress                                                                                                                                                                                                                                                                                                                                   | `false`        |
| `ingress.class`          | Ingress class                                                                                                                                                                                                                                                                                                                                    | `""`           |
| `ingress.annotations`    | Ingress annotations                                                                                                                                                                                                                                                                                                                              | `{}`           |
| `ingress.host`           | Ingress accepted hostname                                                                                                                                                                                                                                                                                                                        | `""`           |
| `ingress.tls`            | Ingress TLS configuration                                                                                                                                                                                                                                                                                                                        | `[]`           |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install keycloak -f values.yaml .
```

> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/fsc-nlx-keycloak/values.yaml)
