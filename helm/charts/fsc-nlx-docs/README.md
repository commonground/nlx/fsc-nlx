# nlx-docs

This is the Chart for the NLX documentation page.

## Prerequisites

- Kubernetes 1.11+

## Installing the Chart

This chart is created for internal use only.

## Parameters

### Global parameters

| Name                      | Description                  | Value |
| ------------------------- | ---------------------------- | ----- |
| `global.imageRegistry`    | Global Docker Image registry | `""`  |
| `global.imageTag`         | Global Docker Image tag      | `""`  |
| `global.imagePullSecrets` | Global image pull secrets    | `[]`  |

### Deployment Parameters

| Name                                | Description                                                                                                            | Value                               |
| ----------------------------------- | ---------------------------------------------------------------------------------------------------------------------- | ----------------------------------- |
| `replicaCount`                      | Number of controller replicas                                                                                          | `1`                                 |
| `image.registry`                    | Image registry (ignored if 'global.imageRegistry' is set)                                                              | `docker.io`                         |
| `image.repository`                  | Image repository of the docs API.                                                                                      | `federatedserviceconnectivity/docs` |
| `image.tag`                         | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used               | `""`                                |
| `image.pullPolicy`                  | Image pull policy                                                                                                      | `Always`                            |
| `image.pullSecrets`                 | Secrets for the image repository                                                                                       | `[]`                                |
| `serviceAccount.create`             | Specifies whether a service account should be created                                                                  | `true`                              |
| `serviceAccount.annotations`        | Annotations to add to the service account                                                                              | `{}`                                |
| `serviceAccount.name`               | The name of the service account to use. If not set and create is true, a name is generated using the fullname template | `""`                                |
| `podSecurityContext`                | Security context for the pod                                                                                           | `{}`                                |
| `securityContext.runAsNonRoot`      | Run container as a non-root user                                                                                       | `true`                              |
| `securityContext.runAsUser`         | Run container as specified user                                                                                        | `1001`                              |
| `securityContext.capabilities.drop` | Drop all capabilities by default                                                                                       | `["ALL"]`                           |
| `resources`                         | Pod resource requests & limits                                                                                         | `{}`                                |
| `nodeSelector`                      | Node labels for pod assignment                                                                                         | `{}`                                |
| `tolerations`                       | Node tolerations for pod assignment                                                                                    | `[]`                                |
| `affinity`                          | Node affinity for pod assignment                                                                                       | `{}`                                |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### Exposure parameters

| Name                  | Description                                      | Value       |
| --------------------- | ------------------------------------------------ | ----------- |
| `service.type`        | Service Type (ClusterIP, NodePort, LoadBalancer) | `ClusterIP` |
| `service.port`        | Port exposed by the docs service                 | `80`        |
| `ingress.enabled`     | Enable ingress                                   | `false`     |
| `ingress.class`       | Ingress class                                    | `""`        |
| `ingress.annotations` | Ingress annotations                              | `{}`        |
| `ingress.hosts`       | Ingress accepted hostnames                       | `[]`        |
| `ingress.tls`         | Ingress TLS configuration                        | `[]`        |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install nlx-docs -f values.yaml .
```
> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/nlx-docs/values.yaml)
