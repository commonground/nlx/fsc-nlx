# ca-cfssl-unsafe

This is the Chart for the NLX ca-cfssl-unsafe. It is used in
non-production environments to generate certificates on the fly from
a central root CA.

Unsafe-ca is based on [cfssl](https://github.com/cloudflare/cfssl).

## Prerequisites

- Kubernetes 1.11+

## Installing the Chart

This chart is created for internal use only.

## Parameters

### Global parameters

| Name                      | Description               | Value |
| ------------------------- | ------------------------- | ----- |
| `global.imagePullSecrets` | Global image pull secrets | `[]`  |

### Deployment Parameters

| Name                         | Description                                                                                                                                                                                | Value         |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------- |
| `image.registry`             | Image registry (ignored if 'global.imageRegistry' is set)                                                                                                                                  | `docker.io`   |
| `image.repository`           | Image repository of the controller API.                                                                                                                                                    | `cfssl/cfssl` |
| `image.tag`                  | Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used                                                                                   | `v1.6.5`      |
| `image.pullPolicy`           | Image pull policy                                                                                                                                                                          | `Always`      |
| `image.pullSecrets`          | Secrets for the image repository                                                                                                                                                           | `[]`          |
| `replicaCount`               | Number of controller replicas                                                                                                                                                              | `1`           |
| `serviceAccount.create`      | Specifies whether a service account should be created                                                                                                                                      | `true`        |
| `serviceAccount.annotations` | Annotations to add to the service account                                                                                                                                                  | `{}`          |
| `serviceAccount.name`        | The name of the service account to use. If not set and create is true, a name is generated using the fullname template                                                                     | `""`          |
| `podSecurityContext`         | Security context for the pod                                                                                                                                                               | `{}`          |
| `securityContext`            | Optional security context. The YAML block should adhere to the [SecurityContext spec](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16/#securitycontext-v1-core) | `{}` | `{}`          |
| `resources`                  | Pod resource requests & limits                                                                                                                                                             | `{}`          |
| `nodeSelector`               | Node labels for pod assignment                                                                                                                                                             | `{}`          |
| `affinity`                   | Node affinity for pod assignment                                                                                                                                                           | `{}`          |
| `tolerations`                | Node tolerations for pod assignment                                                                                                                                                        | `[]`          |

### Unsafe CA specific settings

| Name                                     | Description                                                                              | Value                                                        |
| ---------------------------------------- | ---------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| `config.existingSecret`                  | Secret containing the root certificate and key of the CA                                 | `""`                                                         |
| `config.caConfig.signing.default.usages` | PKI usages, can be array of: 'signing', 'key encipherment', 'server auth', 'client auth' | `["signing","key encipherment","server auth","client auth"]` |
| `config.caConfig.signing.default.expiry` | Certificate expiry                                                                       | `26280h`                                                     |

### Common Parameters

| Name               | Description                   | Value |
| ------------------ | ----------------------------- | ----- |
| `nameOverride`     | Override deployment name      | `""`  |
| `fullnameOverride` | Override full deployment name | `""`  |

### Exposure parameters

| Name           | Description                                      | Value       |
| -------------- | ------------------------------------------------ | ----------- |
| `service.type` | Service Type (ClusterIP, NodePort, LoadBalancer) | `ClusterIP` |
| `service.port` | Port exposed by the service                      | `8888`      |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install ca-cfssl-unsafe -f values.yaml .
```
> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/ca-cfssl-unsafe/values.yaml)
