###########
## Chart ##
###########
global:
  groupID: "fsc-default"

postgresql:
  storageSize: 256Mi

outway:
  ingress:
    enabled: true
    host: nlx-outway-vgs-bv-{{DOMAIN_SUFFIX}}

controller:
  api:
    ingress:
      enabled: true
      host: vgs-bv-fsc-nlx-controller-api-{{DOMAIN_SUFFIX}}

manager:
  api:
    ingress:
      enabled: true
      host: vgs-bv-fsc-nlx-mgr-int-api-{{DOMAIN_SUFFIX}}

opa:
  enabled: true

################
## Sub-charts ##
################
ca:
  certificates:
    controller-internal:
      commonName: "vergunningsoftware-bv-fsc-nlx-controller"
      dnsNames:
        - vergunningsoftware-bv-fsc-nlx-controller
        - "vgs-bv-fsc-nlx-controller-api-{{DOMAIN_SUFFIX}}"
      issuerRef:
        name: "vergunningsoftware-bv"
    manager-internal:
      commonName: "vergunningsoftware-bv-fsc-nlx-manager-internal"
      dnsNames:
        - vergunningsoftware-bv-fsc-nlx-manager-internal
        - "vgs-bv-fsc-nlx-mgr-int-api-{{DOMAIN_SUFFIX}}"
      issuerRef:
        name: "vergunningsoftware-bv"

fsc-nlx-manager:
  config:
    selfAddress: "https://vergunningsoftware-bv-fsc-nlx-manager-external:8443"
    directoryManagerAddress: "https://shared-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  ingress:
    hosts:
      - "nlx-controller-vgs-bv-{{DOMAIN_SUFFIX}}"

parkeerrechten-admin-fsc:
  enabled: true
  organizationName: "Vergunningsoftware BV"
  municipalities: "Stijns"
  outwayAddress: "http://vergunningsoftware-bv-fsc-nlx-outway"
  ingress:
    enabled: true
    hosts:
      # abbreviated name, because https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/technical-docs/notes.md#1215-rename-current-organizations
      - nlx-pa-fsc-vgs-bv-{{DOMAIN_SUFFIX}}
