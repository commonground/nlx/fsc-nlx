###########
## Chart ##
###########
global:
  groupID: "fsc-default"

postgresql:
  storageSize: 256Mi

opa:
  enabled: true

controller:
  api:
    ingress:
      enabled: true
      host: rvrd-fsc-nlx-controller-api-{{DOMAIN_SUFFIX}}

manager:
  api:
    ingress:
      enabled: true
      host: rvrd-fsc-nlx-manager-int-api-{{DOMAIN_SUFFIX}}

################
## Sub-charts ##
################
ca:
  certificates:
    controller-internal:
      commonName: "rvrd-fsc-nlx-controller"
      dnsNames:
        - rvrd-fsc-nlx-controller
        - "rvrd-fsc-nlx-controller-api-{{DOMAIN_SUFFIX}}"
      issuerRef:
        name: "rvrd"
    manager-internal:
      commonName: "rvrd-fsc-nlx-manager-internal"
      dnsNames:
        - rvrd-fsc-nlx-manager-internal
        - "rvrd-fsc-nlx-manager-int-api-{{DOMAIN_SUFFIX}}"
      issuerRef:
        name: "rvrd"

fsc-nlx-manager:
  config:
    selfAddress: "https://rvrd-fsc-nlx-manager-external:8443"
    directoryManagerAddress: "https://shared-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  ingress:
    hosts:
      - "nlx-controller-rvrd-{{DOMAIN_SUFFIX}}"
