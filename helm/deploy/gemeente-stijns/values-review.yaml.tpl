###########
## Chart ##
###########
global:
  groupID: "fsc-default"

postgresql:
  storageSize: 256Mi

outway:
  ingress:
    enabled: true
    host: nlx-outway-gemeente-stijns-{{DOMAIN_SUFFIX}}

outway-2:
  ingress:
    enabled: true
    host: nlx-outway-2-gemeente-stijns-{{DOMAIN_SUFFIX}}

controller:
  api:
    ingress:
      enabled: true
      host: gemeente-stijns-ctrl-{{DOMAIN_SUFFIX}}

manager:
  api:
    ingress:
      enabled: true
      host: gemeente-stijns-manager-int-{{DOMAIN_SUFFIX}}

################
## Sub-charts ##
################
ca:
  certificates:
    controller-internal:
      commonName: "gemeente-stijns-fsc-nlx-controller"
      dnsNames:
        - gemeente-stijns-fsc-nlx-controller
        - "gemeente-stijns-ctrl-{{DOMAIN_SUFFIX}}"
      issuerRef:
        name: "gemeente-stijns"
    manager-internal:
      commonName: "gemeente-stijns-fsc-nlx-manager-internal"
      dnsNames:
        - gemeente-stijns-fsc-nlx-manager-internal
        - "gemeente-stijns-manager-int-{{DOMAIN_SUFFIX}}"
      issuerRef:
        name: "gemeente-stijns"

fsc-nlx-manager:
  config:
    selfAddress: "https://gemeente-stijns-fsc-nlx-manager-external:8443"
    directoryManagerAddress: "https://shared-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  ingress:
    hosts:
      - "nlx-controller-gemeente-stijns-{{DOMAIN_SUFFIX}}"

fsc-nlx-outway:
  extraEnv:
    - name: TX_LOG_METADATA_HEADERS
      value: "Fsc-Grant-Hash"

fsc-nlx-inway:
  extraEnv:
    - name: TX_LOG_METADATA_HEADERS
      value: "Fsc-Grant-Hash"

parkeerrechten-api:
  enabled: true
