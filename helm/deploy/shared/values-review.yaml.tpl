###########
## Chart ##
###########
global:
  groupID: "fsc-default"

postgresql:
  storageSize: 256Mi

################
## Sub-charts ##
################
fsc-nlx-docs:
  ingress:
    hosts:
      - "docs-{{DOMAIN_SUFFIX}}"

fsc-ca-certportal:
  ingress:
    hosts:
      - "certportal-{{DOMAIN_SUFFIX}}"

fsc-apps-overview:
  config:
    environmentSubdomain: "review"
    reviewSlugWithDomain: "{{DOMAIN_SUFFIX}}"
  ingress:
    enabled: true
    hosts:
      - "{{DOMAIN_SUFFIX}}"

fsc-nlx-manager:
  config:
    selfAddress: "https://shared-fsc-nlx-manager-external:8443"
    directoryManagerAddress: "https://shared-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  ingress:
    enabled: true
    hosts:
      - "controller-{{DOMAIN_SUFFIX}}"

fsc-nlx-directory-ui:
  config:
    directoryManagerAddress: "https://shared-fsc-nlx-manager-external:8443"
  ingress:
    hosts:
      - "directory-ui-{{DOMAIN_SUFFIX}}"
