###########
## Chart ##
###########
global:
  groupID: "fsc-default"

postgresql:
  storageSize: 256Mi

outway:
  ingress:
    enabled: true
    host: nlx-outway-gemeente-riemer-{{DOMAIN_SUFFIX}}

controller:
  api:
    ingress:
      enabled: true
      host: gemeente-riemer-ctrl-{{DOMAIN_SUFFIX}}

manager:
  api:
    ingress:
      enabled: true
      host: gemeente-riemer-manager-int-{{DOMAIN_SUFFIX}}

################
## Sub-charts ##
################
ca:
  certificates:
    controller-internal:
      commonName: "gemeente-riemer-fsc-nlx-controller"
      dnsNames:
        - gemeente-riemer-fsc-nlx-controller
        - "gemeente-riemer-ctrl-{{DOMAIN_SUFFIX}}"
      issuerRef:
        name: "gemeente-riemer"
    manager-internal:
      commonName: "gemeente-riemer-fsc-nlx-manager-internal"
      dnsNames:
        - gemeente-riemer-fsc-nlx-manager-internal
        - "gemeente-riemer-manager-int-{{DOMAIN_SUFFIX}}"
      issuerRef:
        name: "gemeente-riemer"

fsc-nlx-manager:
  config:
    selfAddress: "https://gemeente-riemer-fsc-nlx-manager-external:8443"
    directoryManagerAddress: "https://shared-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  config:
    authn:
      oidc:
        discoveryUrl: https://keycloak-gemeente-riemer-{{DOMAIN_SUFFIX}}/realms/organization-a
        redirectUrl: https://nlx-controller-gemeente-riemer-{{DOMAIN_SUFFIX}}/oidc/callback
        logoutUrl: https://keycloak-gemeente-riemer-{{DOMAIN_SUFFIX}}/realms/organization-a/protocol/openid-connect/logout?client_id=nlx-controller-a&post_logout_redirect_uri=https://nlx-controller-gemeente-riemer-{{DOMAIN_SUFFIX}}
  ingress:
    hosts:
      - nlx-controller-gemeente-riemer-{{DOMAIN_SUFFIX}}

fsc-nlx-keycloak:
  config:
    keycloakHostURL: https://keycloak-gemeente-riemer-{{DOMAIN_SUFFIX}}
    keycloakHostAdminURL: https://keycloak-gemeente-riemer-{{DOMAIN_SUFFIX}}
  ingress:
    enabled: true
    host: keycloak-gemeente-riemer-{{DOMAIN_SUFFIX}}
