## [1.9.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.8.0...v1.9.0) (2024-12-19)

### Build System

* upgrade cucumber/godog to v0.15.0 ([8c01876](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8c01876c64215a078be8a944e8aee0ade93670e8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* upgrade Go dependencies to their latest version ([726053c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/726053c775785bd9087ec41461ed86f801115837)), closes [1#L11](https://gitlab.com/commonground/1/issues/L11) [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* upgrade golang-migrate v4.17.1 -> v4.18.1 ([b4b791d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b4b791d168110c49ed364c6aa031ab27031a6b56)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* upgrade gorilla/sessions v1.3.0 -> v1.4.0 ([56bef74](https://gitlab.com/commonground/nlx/fsc-nlx/commit/56bef744595082c6a6ebbaeb18607b975f04765d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Documentation

* clean up ([99636a7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/99636a73b3a681e7e50da9073562c698a717a1a5)), closes [fsc-nlx#527](https://gitlab.com/commonground/fsc-nlx/issues/527)

### Features

* **inway:** validate the inway self address ([553151a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/553151af841f8417e17ce3780406c0883a0a2d06)), closes [fsc-nlx#522](https://gitlab.com/commonground/fsc-nlx/issues/522)

### Bug Fixes

* **helm:** fixed invalid values and deprecates annotations ([bc0c09e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bc0c09e324d40d963616d55b892ca278330ebfb6)), closes [fsc-nlx#515](https://gitlab.com/commonground/fsc-nlx/issues/515)
* **manager:** log failed announce calls at debug level ([dcb42c4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dcb42c40e7a064a9a5e088cd439744de9c4b65f5)), closes [fsc-nlx#523](https://gitlab.com/commonground/fsc-nlx/issues/523)
* renamed e2e given to prevent ambigious error ([9abe73d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9abe73d5208071a2eead15f1efbc8babfc414681)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **txlog:** apply peer id filter when filtering on transaction ids ([7dcdb2a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7dcdb2a94ba861c5ccc2132a7c48bbb2ee845fb3)), closes [fsc-nlx#516](https://gitlab.com/commonground/fsc-nlx/issues/516)
* **txlog:** get logs limit is validated ([f131d86](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f131d86bcb2098f5503e715ce573fdbcb5b3a339)), closes [fsc-nlx#518](https://gitlab.com/commonground/fsc-nlx/issues/518)

### Code Refactoring

* adjust regex for Service name in the E2E test features ([bacd947](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bacd94721011b386de0e78308e0661e7111fcb1b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Tests

* **controller:** extend tests for the Controller UI routes ([a849829](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a849829d0d0a71ef89187fd98e7a557aea333e3f)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

## [1.8.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.7.0...v1.8.0) (2024-12-16)

### Continuous Integration

* exclude directory-ui code from the coverage report ([3910135](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3910135f1356340112c805a4bbce7258b20d6c72)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Features

* **controller:** add additional input validation to update peer ([4bb593a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4bb593ae7acca9e768626ca9457d03497099cec3)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** disable assigning Peer roles in the UI ([2c0f5e0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2c0f5e092797a26fe37948ea73d8bbefa0d57b04)), closes [fsc-nlx#521](https://gitlab.com/commonground/fsc-nlx/issues/521)

### Bug Fixes

* **controller:** create auditlog records for failed retry contract attempts ([7812bbb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7812bbba268dfe6d17210ca6b0ee449050eb6924)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** add uuid validation for the cursor for external GET /services ([6e3864f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6e3864f6b669a7aef23fbc70098c220bf09b5a08)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Code Refactoring

* **controller:** extract generation of the Asset Map from the port ([3b7f4e4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3b7f4e4048cf962736f0e3f551bfaf1112a6ca8e)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** remove unused parameters ([eaefe6a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eaefe6a3ddeece07cbc5e17dfe6d0f336c8815fe)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** cleanup internal GetContracts test and parallelize tests ([279a0fd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/279a0fd2141ea4ecdd8137b8b25a297aeef04cf5)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** remove redundant copy of loop variable ([3618cb7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3618cb72f4ed0e4280eb05f5bb00c8ec9d9dd048)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** remove unused SQL file ([d5a8b70](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d5a8b702dd19e5307762137543b09833d4657781)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** rework external GET /services endpoint ([9bdca1e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9bdca1e308bb244bf32ac172a1630008af348056)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** rework the GET /logs test to make it easier adding extra tests ([5da785d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5da785daf30a67098e5458487b413be5f3362152)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** rework the RetryContractDistribution tests to use the port ([89d03db](https://gitlab.com/commonground/nlx/fsc-nlx/commit/89d03dbbe7bad9d3caad0ac54ccc621f69bdc043)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Tests

* **controller:** add component test for get services ([513d6a4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/513d6a424550b8e4f301f0ae9e8ac3e899407dff)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add event login to list auditlog records test ([6e535c1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6e535c18000c7ca2f586e143195a400eeb289383)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add missing method to fake manager for tests ([8537e85](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8537e85e552e9ebd6e11b73635847b1f104fb29b)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for connection get peer ids ([1d55d07](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1d55d07439d80d1e0154238807fe1ccc8046088b)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for create peer command ([89b19d6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/89b19d64c04269a16420d147344e6d957e119fd5)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for GET /actions of the UI port ([deb87ee](https://gitlab.com/commonground/nlx/fsc-nlx/commit/deb87ee955b868001b355f3bdb9159ed4ab25ea7)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for GET /directories of the UI port ([dbb44fd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dbb44fda52811fdfac9e681e24814cb0a72c2882)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for get health query ([795cf45](https://gitlab.com/commonground/nlx/fsc-nlx/commit/795cf45ac0d09065031a64bfcc8b6393ba2cffbf)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for get peer info query ([dfae895](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dfae895472a6218f622e8281a766d2e5eb7b0a82)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for list auditlog records query ([8285c5a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8285c5a9e2a721d8db626fd71147d7a35c538091)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for list inway query ([4e1000a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4e1000abc3390c8fbf41c388cde4eb1590607275)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for list outgoing connection for service query ([0f4c42a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0f4c42acca8f229bfac1be2b7a537a2c555a0c47)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for list outgoing connections query ([49924c0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/49924c0461abd57f1df036ca2fb3d87a2fc3498e)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for list outways query ([5bb73f1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5bb73f12dcf6af16a9db26fe3d649e111759b4cc)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for list pending contracts query ([fa696d9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fa696d948fafb869468b071da79420019341a314)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for query list delegated connections ([ceb2f79](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ceb2f7932c51028f6d294b710fb7347be7c281be)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for query list delegated service connections ([9068d89](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9068d893bc40ced6fc647a29a9c3526317f96787)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for query list incoming connections ([f82d4bb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f82d4bb286720f0fd19f48941b57f039aa05fc1b)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for retry contract command ([9f5b1bd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9f5b1bdb3ea175df7879637f237518aaf05feff5)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for sync peers command ([ad179a3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ad179a34f9a42d34dd9d88722da9903bac26a318)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for various routes of the UI port ([97cd06d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/97cd06df412d564d7dec2e794dd13a73394e6086)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add test for various routes of the UI port ([b45aeb4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b45aeb4f517a0a6f1eb2d0372cb2c64f4f12d11b)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** add tests for update peer command ([2ce6f80](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2ce6f8031f1925ec32aa1304aab0d3e3699fd939)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** manager adapter tests for the Controller ([54513fb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/54513fbf96c5ecc5fc0f3fa193832fa13b22f12c)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** refactor accept contract test to use fakes instead of mocks ([49a5be2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/49a5be2603528b5d52a07e7fc93cb54ffde0b423)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** refactor delete service test to use fakes instead of mocks ([ce49571](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ce495713aaedc3989498bcfa48d4566731966bb5)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** refactor test for reject contract to use fakes instead of mocks ([d95ab48](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d95ab48e90e1a458d61f0ff97f84b95c7fe731a4)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** refactor test for reject contract to use fakes instead of mocks ([e1dd4a1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e1dd4a12f16db50a82f395b2d980726be5076fb8)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** refactor test for update service use fakes instead of mocks ([cceb2ab](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cceb2abe26501a1d9a65476be7a54ed704e320e3)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** refactor tests for create service to use fakes instead of mocks ([e7cc0f4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e7cc0f4279766e7baf7bdd9d128a4379b9b7f25f)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** refactor the test for the command create contract ([fa9593f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fa9593f08a07e588524e006d3214d0386d48b7f0)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** rework PostgreSQL setup to avoid duplicate DB names ([4343df8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4343df8e778023ed15200c69b3c43bbca0c69018)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** use fakes instead of mocks in list txlog records query ([77c7b56](https://gitlab.com/commonground/nlx/fsc-nlx/commit/77c7b56168d0563618d0235e4f98dd4883568240)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **controller:** use fakes instead of mocks in the list service publications test ([ba56961](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ba5696162ba2be25868a28c8f0c0c129ee3b638d)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* get JWK with EC certificate ([026ceaf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/026ceaf19325dd35d85bab12f7b9ce42b405378d)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* get JWKS with EC certificate external API ([64fcde3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/64fcde3c66c84663de679032aa0e253f633ec1a6)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add extra tests for the internal update Peer method ([4cba163](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4cba1636441996c84e668b43b04600fde2f68320)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test for retrieving Failed Contract Distributions ([d8c8577](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d8c8577c0987c2055422ff43e99ace60b04bf102)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test for the external GetHealth endpoint ([6fcd5b1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6fcd5b1258f8c289e3b507a6e5f84bb76932504a)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test for the external Submit Contract with a Service using HTTP2 ([6241df5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6241df514aa3d86bee17c9b6c4b45a42ada5c12b)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test for validations errors of the RetryContractDistribution handler ([e1c3371](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e1c3371a9d5c395aa3c2932308bdadfc381c4540)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test retrieving contracts with a DelegatedServicePublicationGrant ([255956c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/255956c2775ef7f9bba931e936adf26e2ed7b11a)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test retrieving contracts with a ServiceConnectionGrant ([c34c0c6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c34c0c6e672ccddb07b65438da32b65ceb6bc0c0)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test with filter by Grant Hash for the int GET /contracts ([b061db8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b061db83fef01364c971d80fe4f7014ff8e10975)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add tests for Manager domain ([f472ccb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f472ccb024b1594d74afdd7468a7858f83d0b82f)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** extend tests for external GET /logs ([6dd16f8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6dd16f835396b470549ead2d11afb2b04a2794e4)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** extend tests for the external GET /services filters and ordering ([18a41d5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/18a41d55fff5870891d7eb7ef267dbfb363fd732)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** extract contract creation in a separate function ([31f65eb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/31f65ebc5cb9100d05425a031e0fdf5bb0a2446e)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* regenerate testing pki material broke tests ([ff0845f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ff0845f2d239f19b6e83d42b135f13e507aee996)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

## [1.7.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.6.2...v1.7.0) (2024-11-28)

### Build System

* add nolint annotation ([06eb516](https://gitlab.com/commonground/nlx/fsc-nlx/commit/06eb516951b9ea192bd1158f8361fa6d20f3122f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* configure the copyloopvar linter so it is enabled ([af75824](https://gitlab.com/commonground/nlx/fsc-nlx/commit/af75824f1abd65a32caa0906b2d86e4fbf0a4b16)), closes [fsc-nlx#510](https://gitlab.com/commonground/fsc-nlx/issues/510)
* fix keycloak error on macos 15 ([b279a72](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b279a723dacb22f492bd92a6033009f3ed9ec653)), closes [/github.com/keycloak/keycloak/issues/24821#issuecomment-2173010754](https://gitlab.com/commonground//github.com/keycloak/keycloak/issues/24821/issues/issuecomment-2173010754) [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* increase max connections for PostgreSQL ([b05fe98](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b05fe98087e22e812782a633f844412d0f9099ed)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** fix merge conflicts after rebase ([34afe77](https://gitlab.com/commonground/nlx/fsc-nlx/commit/34afe77ae636655331d206a4696e72008b8f5c0c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Continuous Integration

* disable go cache ([f69cdc9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f69cdc9d55cde17b11821afaaf19e0fe07c7578b)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* enable E2E tests for the beta branch ([154529b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/154529b4f874b01322b53afb72dfcc6733849254)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* ensure creating a pre-release works as expected ([5ae56db](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5ae56db0542cb9d2b24c181af6494d865ef2aaf5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* exclude mocks and migrations from the test coverage report ([58f8692](https://gitlab.com/commonground/nlx/fsc-nlx/commit/58f869254e30a1f362e448b1123e0cf3b2e498ee)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* remove apps-overview from the Go coverage report ([8bf6b1f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8bf6b1f26925d875e6dcbaa35af51bb10e418d4d)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* remove ca-certportal from the Go coverage report ([a4786f2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a4786f2090707435d95ef6c78cdcb4f8f51923b5)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* remove exclusion of protobuf files for coverage ([c390759](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c3907590491edf973893e40c61cce1c2fbd1e4f5)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* reorder arguments of the go test command ([548258e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/548258e17e941ea350ffa5ddac60f891355f6c45)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* use coverpkg flag for the Go tests ([837e528](https://gitlab.com/commonground/nlx/fsc-nlx/commit/837e528042041d37f1c886a0b5bbf4ffb7c06410)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Features

* add synchronize peers function ([f3c5d54](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f3c5d5437adf078e81e9a685911a9b8460e012fe)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** add loader indicator when loading Services of a Directory ([9b735fa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9b735faa8326848aa87ba4cd80c28a447ced4471)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** enable assigning the role 'Directory' to a Peer ([3178fe1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3178fe1e19bb5f575cb00dfe46b615bcc978e1f5)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** enable selecting a Directory in the Controller UI ([63e1731](https://gitlab.com/commonground/nlx/fsc-nlx/commit/63e173150651bbcca1c50fc1da46dd99bbc43228)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** remove selected Directory Peer info ([c9908b9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9908b96cd049219c8537622231eeb51bfd976d2)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** replace input list with select ([4a103c3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4a103c3ff641cf43db4bef726ec07c11d702dc39)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** replace Services count with Directory toggle ([79c607b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/79c607b681866a8e0c777947a669b9f2dba3500c)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** add roles property to peers ([89fc26f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/89fc26f951cfefc50aa485e440d8c83d160d4238)), closes [fsc-nlx#463](https://gitlab.com/commonground/fsc-nlx/issues/463)
* **manager:** use multiple directories ([469aa3e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/469aa3ef7c6ca9124d618b3933dee3e2d5f0a9bf)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)

### Bug Fixes

* **auditlog:** implement event type delete_peer ([200d7da](https://gitlab.com/commonground/nlx/fsc-nlx/commit/200d7da1961b08ea1ec1d86a75e20d705c916836)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **auditlog:** use correct numbering for migration files ([d05fcd8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d05fcd840187eb6655e14b81e8fbd7d05e515cab)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** ensure the Peer roles are shown correctly after submit ([cb0812a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cb0812acfb2f99bc2ba4ad261107ba01f57e87ff)), closes [fsc-nlx#511](https://gitlab.com/commonground/fsc-nlx/issues/511)
* **controller:** pass the Directory Peer ID to the Directory Service Detail page ([ec97fb3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ec97fb3a668076fb77b911f0250b7430d85b31e1)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** update page title Directory -> Directories ([d4f35fa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d4f35fa838da198e6a133c752fba8aeb4134bc42)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** do not delete peer roles on announce ([903956e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/903956ec8204dd7f8f2479fc997646b2b75acb72)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** extract the creation of the Directory Peer from the announce loop ([edfcded](https://gitlab.com/commonground/nlx/fsc-nlx/commit/edfcded4160917f74cd4c661ff20ebed2f14c48b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** strip slash suffix from manager address for the announce endpoint ([b538ce9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b538ce94aa29aa993ad4c42c27e4c7d06602752e)), closes [fsc-nlx#510](https://gitlab.com/commonground/fsc-nlx/issues/510)
* **manager:** use correct date format in query ([7cc1998](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7cc19980025b648caab51dd78591dc2fd84f065f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Code Refactoring

* **controller:** ensure the Controller UI can work with multiple Peer Roles ([06b7112](https://gitlab.com/commonground/nlx/fsc-nlx/commit/06b7112309ef6e0b9f00e38bf1c66e44367fb7c9)), closes [fsc-nlx#511](https://gitlab.com/commonground/fsc-nlx/issues/511)
* **controller:** remove unused POST connect to service handler ([b14fbf8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b14fbf8d7d457b75b69039c26f114deeab52a4ec)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** rename GetServices to ListServices ([531f4b7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/531f4b7187167ab265b85ec8c80f5774e4271cc9)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** rename PeerID to ServiceProviderPeerIDFilter ([c9d0b38](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9d0b381fa335355e37bf2da421087d7dfb82d2a)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** separate the default Peer roles and selected Peer roles ([a126dac](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a126daca2e0c95581be3fd86b3859e001103d2a6)), closes [fsc-nlx#511](https://gitlab.com/commonground/fsc-nlx/issues/511)
* **controller:** simplify translating the Peer Roles ([c7135a7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c7135a7df6edb8ffb93b4e250ba5e2e550662bcc)), closes [fsc-nlx#511](https://gitlab.com/commonground/fsc-nlx/issues/511)
* **manager:** extract implementation from functions ([4cb9369](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4cb9369adf19fedbfad7586ace814978fc2eadca)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** remove unused code ([d54120a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d54120a107e1a480f0aad67680bad912a4f42ea0)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** rename peer_id to directory_peer_id to improve readability ([2984287](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2984287264840f14623455ffa26c251b00911a35)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** rework the ListServices args to improve readability ([3c36c8d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3c36c8da15900f7726c00eadac57bb02a4ae4a7e)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)

### Styles

* **controller:** fix template linting ([24b021b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/24b021ba1f25340f3dab54d408ea92ad973e7041)), closes [fsc-nlx#511](https://gitlab.com/commonground/fsc-nlx/issues/511)
* **controller:** improve formatting ([0361911](https://gitlab.com/commonground/nlx/fsc-nlx/commit/036191106b8f9859b05cc3761e5be7f8ce093776)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** remove redundant comment ([070788a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/070788a31c847bf2d4a22d65d4c4b42efcaed455)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Tests

* add tests for manager rest adapter ([d74fbf1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d74fbf1c60ea5b736d146a500c58953dc17acc02)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **auditlog:** add component test for set_under_review endpoint ([87e460a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/87e460aae8b9a8a922baa4dd15312edfe23281fc)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **auditlog:** add test for unset_under_review endpoint ([5e9e9b0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5e9e9b0aca04f6f2845e1e0c68bae73fb8ae16a3)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **auditlog:** run tests for ListRecords in parallel ([a48b159](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a48b1597be5371f455d9b8afb8312cebd69a7475)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **auditlog:** test all event types in list records ([77ad33e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/77ad33e678fa14765a59ba5a804d34d01c348e89)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* fix flacky test for announce ([082e587](https://gitlab.com/commonground/nlx/fsc-nlx/commit/082e587c6c4642f84ef20df8d00d51c93255373e)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add log with response body if response status code is not as expected ([40e7678](https://gitlab.com/commonground/nlx/fsc-nlx/commit/40e76785b78e045275e594648f9c44f648ba1c48)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** add test for announcing with an empty Manager Address ([2f52a4a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2f52a4a60ae130650a9e4ec15df64f80a2782b19)), closes [fsc-nlx#510](https://gitlab.com/commonground/fsc-nlx/issues/510)
* **manager:** add tests for the ContractContent model ([2b43714](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2b43714d627884d89759b4c09e531891132c6827)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add tests for the metrics collector ([d2b38e3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d2b38e3bc5683e11413af81ecafebe829a631413)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** extend tests for the Contract Content domain model ([c329cbc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c329cbc9bbe50c9e301be079435de690cc91a55a)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** extend tests for the external GetToken endpoint ([2acb46c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2acb46c6847982fe4594e964d21cdd751824e9e7)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** replace defer with t.Cleanup and add t.Parallel for all component tests ([bf1adb3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bf1adb36a2063022e4d7842229bc8858971ff39c)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** replace global test clock with a clock per test ([1025a32](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1025a32df40513d7e7bf93c7662d5915ed46629c)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* run tests for the probes in parallel ([626015d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/626015d68e546d659cd486405505c4c8eedcba6b)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **txlog:** add test for retrieving Metadata Log Records ([5994914](https://gitlab.com/commonground/nlx/fsc-nlx/commit/59949147895d9e84eb6ca4ff68cfbec9de8c09f7)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **txlog:** add tests for the Transaction Log API probe ([fc036f0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fc036f0f483a8d09b5a1292954cde7c64a2831bf)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* update peer as directory and select peer as directory ([a0a1886](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a0a18863a9530561e84eeeec43f376db629e79ca)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)

## [1.6.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.6.1...v1.6.2) (2024-11-22)

### Bug Fixes

* **manager:** removed unused get service endpoint url handler ([cc98693](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cc9869377b57e1f2823c1b5848e6d37607d84d85)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* prevent error when updating CRLs ([05bd492](https://gitlab.com/commonground/nlx/fsc-nlx/commit/05bd492e94006154b4595924741e425a1e55c73b)), closes [fsc-nlx#509](https://gitlab.com/commonground/fsc-nlx/issues/509)

### Code Refactoring

* removed unnecessary return function in tls package ([0014720](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0014720a9d1e40d46bc59f69da97b6187ec2400d)), closes [fsc-nlx#509](https://gitlab.com/commonground/fsc-nlx/issues/509)

### Tests

* **manager:** add test for the policy decision point autosigner ([4eee359](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4eee3595d806d9a8d02febcc793c231f746c99e5)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add tests for Manager Get Tx Log records ([d82ac98](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d82ac9864c4b019b36a6613ed9159965b351973e)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

## [1.6.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.6.0...v1.6.1) (2024-11-20)

### Build System

* **manager:** remove unused mock for the Signer of cloudflare/cfssl ([6a4bdcd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6a4bdcd052621c8bee62356a3e45673180de09d3)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* use PostgreSQL DSN of Docker Compose file by default ([86b1c5f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86b1c5f52e8380a0368960f5e3436eb041f5d8bd)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Continuous Integration

* remove redundant covermode flag ([7a00be8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7a00be8f32a6ab77148128868bc8b710ccb18684)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Bug Fixes

* ensure Audit Logs of type UpdatePeer are properly returned by the Audit Log ([83a45f3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/83a45f3dbb81d54ebd189e1e79024d8d1ec5507a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** return correct value for the DirectoryPeerID of Delegated Service Publication Grants ([577d60b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/577d60b655dbe9dd391ffab082a82586af661561)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **outway:** health probe is now able to call outway with TLS ([24f5da3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/24f5da3d2960947f7128a93d76a54104ce4c49a8)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* refreshing cached CRL's caused a deadlock ([7ce3edb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7ce3edb1dba4a7d480cb1684fbc43b31817feaa8)), closes [fsc-nlx#496](https://gitlab.com/commonground/fsc-nlx/issues/496)

### Code Refactoring

* **manager:** remove unused internal GetToken endpoint ([77da767](https://gitlab.com/commonground/nlx/fsc-nlx/commit/77da767eb9063a8a94d8f72a6a16eecb57088cd9)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** removed unused get_peer internal app ([2d7c76a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2d7c76ad6704ab101f74ee0aecf5ea187af9b2c3)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Tests

* add tests for health probes ([fa0c0ad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fa0c0ade0987e9de722fff0e159d6692d73b7ebd)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add RetryContractDistribution for adding a Revoke Signature ([e021e39](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e021e3904ca0a73385400664d3e7909578c289e1)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add RetryContractDistribution for re-submitting a Contract ([d963127](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d963127b40c0e039e1cd77d9af4b5ad44061142e)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test for list Contracts filtered by Grant type DelegatedServiceConnection ([f5c4715](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f5c4715b0255e29cb108e262420f666909355c8c)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test for list Contracts filtered by Grant type ServiceConnection ([957a858](https://gitlab.com/commonground/nlx/fsc-nlx/commit/957a85833ac3f2d652fa9e1a56291b4c2e03d31d)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** rename test to improve readability ([8f6b258](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8f6b258b5cde50eaa223f897e656b94970137ddd)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

## [1.7.0-rc.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.6.0...v1.7.0-rc.1) (2024-11-18)

### Build System

* add nolint annotation ([cc15b5a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cc15b5a26bad0e2841edff7cf7d75a20c4f5dd80)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* fix keycloak error on macos 15 ([c554a26](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c554a26807c165b014cd25cabe60150cfbe06b3e)), closes [/github.com/keycloak/keycloak/issues/24821#issuecomment-2173010754](https://gitlab.com/commonground//github.com/keycloak/keycloak/issues/24821/issues/issuecomment-2173010754) [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** remove unused mock for the Signer of cloudflare/cfssl ([6a4bdcd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6a4bdcd052621c8bee62356a3e45673180de09d3)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* use PostgreSQL DSN of Docker Compose file by default ([86b1c5f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86b1c5f52e8380a0368960f5e3436eb041f5d8bd)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Continuous Integration

* enable E2E tests for the beta branch ([7263840](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7263840d7de14a57b0313e9e314be43085ef157a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* ensure creating a pre-release works as expected ([fb97492](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fb97492f4699a92fc168df4a9ee59a47c3706697)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* remove redundant covermode flag ([7a00be8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7a00be8f32a6ab77148128868bc8b710ccb18684)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Features

* add synchronize peers function ([172f2c2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/172f2c260aff83dfee504234521ce8c2549dd5eb)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** add loader indicator when loading Services of a Directory ([86ed814](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86ed8145de41a5ba47f48b29465da7c1a0bb912c)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** enable assigning the role 'Directory' to a Peer ([1ca5810](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1ca58106ede61ccd56bbe65f3583040bab7b30a2)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** enable selecting a Directory in the Controller UI ([9c50e31](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9c50e317ba7d4ba84f843f7f660b0624867c1999)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** remove selected Directory Peer info ([3c7d000](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3c7d000efe2969bca4230b585468688decbc67fe)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** replace input list with select ([9d64cf5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9d64cf554c524eb847f41133493310866f481245)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** replace Services count with Directory toggle ([807b0fb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/807b0fb80e88385c28a248361b58eef9cf853c0e)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** add roles property to peers ([528d2c2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/528d2c21c48545732fe9c7392c33a0c99e7f193d)), closes [fsc-nlx#463](https://gitlab.com/commonground/fsc-nlx/issues/463)
* **manager:** use multiple directories ([fc5e90a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fc5e90a5dc0e5a0707be24626490c8f76f165070)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)

### Bug Fixes

* **auditlog:** use correct numbering for migration files ([e577d9f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e577d9f8d8049f20e2aa42effb73b621c53853ff)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** pass the Directory Peer ID to the Directory Service Detail page ([f653442](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f6534423eeff9b146f9add3f9b3d1e93a06ed5a4)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** update page title Directory -> Directories ([eaf249e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eaf249ef271893f2e77fd100e65bbcd4128dac07)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* ensure Audit Logs of type UpdatePeer are properly returned by the Audit Log ([83a45f3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/83a45f3dbb81d54ebd189e1e79024d8d1ec5507a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** do not delete peer roles on announce ([55db78b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/55db78b2d284f62bb1ba9a0ce4a2d66400495e22)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** return correct value for the DirectoryPeerID of Delegated Service Publication Grants ([577d60b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/577d60b655dbe9dd391ffab082a82586af661561)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **outway:** health probe is now able to call outway with TLS ([24f5da3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/24f5da3d2960947f7128a93d76a54104ce4c49a8)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)

### Code Refactoring

* **controller:** remove unused POST connect to service handler ([dfa0bce](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dfa0bce3c1d8b3e1d9fb068432507220ded38fcc)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** rename GetServices to ListServices ([de371ba](https://gitlab.com/commonground/nlx/fsc-nlx/commit/de371babac964e2ae005e1b54d8d18100027386d)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **controller:** rename PeerID to ServiceProviderPeerIDFilter ([a889544](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a889544b6fe8f376ef81d5c7dd1e4440120b57e8)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** remove unused internal GetToken endpoint ([77da767](https://gitlab.com/commonground/nlx/fsc-nlx/commit/77da767eb9063a8a94d8f72a6a16eecb57088cd9)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** rename peer_id to directory_peer_id to improve readability ([c6c5387](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c6c53877bc65ae5477717fd5b2b466196714ad6c)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** rework the ListServices args to improve readability ([9a790c7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9a790c7ab859f1ec1b245e0a110a71c68386c050)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)

### Styles

* **controller:** improve formatting ([0cf2089](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0cf2089041cf96c96e6616363a2ae3ebb97ae1f5)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)

### Tests

* add tests for health probes ([fa0c0ad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fa0c0ade0987e9de722fff0e159d6692d73b7ebd)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add log with response body if response status code is not as expected ([85d8e61](https://gitlab.com/commonground/nlx/fsc-nlx/commit/85d8e61ebdd725d72770fb9f3779fccfca079624)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)
* **manager:** add RetryContractDistribution for adding a Revoke Signature ([e021e39](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e021e3904ca0a73385400664d3e7909578c289e1)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add RetryContractDistribution for re-submitting a Contract ([d963127](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d963127b40c0e039e1cd77d9af4b5ad44061142e)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test for list Contracts filtered by Grant type DelegatedServiceConnection ([f5c4715](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f5c4715b0255e29cb108e262420f666909355c8c)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** add test for list Contracts filtered by Grant type ServiceConnection ([957a858](https://gitlab.com/commonground/nlx/fsc-nlx/commit/957a85833ac3f2d652fa9e1a56291b4c2e03d31d)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* **manager:** rename test to improve readability ([8f6b258](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8f6b258b5cde50eaa223f897e656b94970137ddd)), closes [fsc-nlx#495](https://gitlab.com/commonground/fsc-nlx/issues/495)
* update peer as directory and select peer as directory ([1a8ae70](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1a8ae701d4a898cadb086d59bc1c6e1d3448875a)), closes [fsc-nlx#486](https://gitlab.com/commonground/fsc-nlx/issues/486)

## [1.6.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.5.0...v1.6.0) (2024-11-08)

### Continuous Integration

* replace deprecated linter exportloopref with copyloopvar ([7027e1c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7027e1caa67b0e88e4c96e1a272552b7007a4d5f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Features

* add logging for health probes ([d61cfdc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d61cfdc6fd8810594a52bd6bbc64adbd3ee8dbe3)), closes [fsc-nlx#502](https://gitlab.com/commonground/fsc-nlx/issues/502)
* combine logs from probes into a single log ([a3ac9a2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a3ac9a275fdc306005444ef5114849b46a79468f)), closes [fsc-nlx#502](https://gitlab.com/commonground/fsc-nlx/issues/502)

### Bug Fixes

* **auditlog:** delete records older then 30 days ([9793ec5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9793ec53fdcf75e9ac07a4149e20c7cf6dd173cd)), closes [fsc-nlx#497](https://gitlab.com/commonground/fsc-nlx/issues/497)
* **inway,outway:** fix bad key error in logs ([bea7cb8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bea7cb8caed6824bf4ace5f6a717158a4ed4fbf3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **inway:** update endpoint URL of the Inway probe ([8fd7e5a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8fd7e5afaa06a1e1bc552973c090055e3275e736)), closes [fsc-nlx#502](https://gitlab.com/commonground/fsc-nlx/issues/502)
* log settings are respected by http servers ([754c443](https://gitlab.com/commonground/nlx/fsc-nlx/commit/754c44305d836fc3c588eab765a5e54edd0e9846)), closes [fsc-nlx#490](https://gitlab.com/commonground/fsc-nlx/issues/490)

### Code Refactoring

* replace errors.Wrap with errors.Join ([23feadb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/23feadb07cedeb0b881f41ad128b214a3094d781)), closes [fsc-nlx#502](https://gitlab.com/commonground/fsc-nlx/issues/502)

## [1.5.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.4.0...v1.5.0) (2024-10-29)

### Features

* **manager:** use format uint32 for the pagination limit in Core ([d2c130c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d2c130c73f24db1efb5677b49e2c707cef8cfe45)), closes [fsc-nlx#482](https://gitlab.com/commonground/fsc-nlx/issues/482)

### Bug Fixes

* corrected typo in rego files ([13f9ff7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/13f9ff74feafb550596ece3f54321f03749de89b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** set default log level of all components to info ([1b3a69d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1b3a69d0e71778db777c9dc945caf70274497d74)), closes [fsc-nlx#488](https://gitlab.com/commonground/fsc-nlx/issues/488)
* **helm:** use quotes in inway/outway helm chart for int and bool ([aee6862](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aee6862b7736083d128c8fba4ac7cede14dfc4a5)), closes [fsc-nlx#492](https://gitlab.com/commonground/fsc-nlx/issues/492)

## [1.4.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.3.1...v1.4.0) (2024-10-24)

### Build System

* **helm:** add tokenTTL to values to configure token ttl ([ac96b2c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ac96b2cfb790b3091581b32a09a736a120743809)), closes [fsc-nlx#468](https://gitlab.com/commonground/fsc-nlx/issues/468)
* update dependency helm/helm to v3.16.2 ([44c80fe](https://gitlab.com/commonground/nlx/fsc-nlx/commit/44c80fed667bb21ec6dbd08f1b9eb194b2a5a5d2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v24.1.3 ([5785a32](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5785a324ba50e28cde5ad2448b31891f932908da)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update go and postgres images ([7fac761](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7fac761e06a19bab6760be32d22b865785d8fb69)), closes [fsc-nlx#476](https://gitlab.com/commonground/fsc-nlx/issues/476)
* update module github.com/prometheus/client_golang to v1.20.5 ([f19b88c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f19b88cd193bda5dac2921a0c2d85aeff438eed6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to cbc651c ([d9f6ddd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d9f6ddd3a9fc591670449c8e6c70923dbc5cbe9c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to fc95a04 ([b94b42b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b94b42bdc71e21bacd96389eed38f7d56def3acc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v22.10.0 ([09427d4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/09427d49bd3e95b1c2501d6912ef698f24a4dac6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Continuous Integration

* increase replicas of Manager in ACC ([6c2bcd4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6c2bcd4fa5be45ad499cba0cc80787c35e670e54)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Features

* inway and outway authz plugin can now send HTTP body ([08a78ec](https://gitlab.com/commonground/nlx/fsc-nlx/commit/08a78ec2de87b22b5ffd21e4748adc607dcddf47)), closes [fsc-nlx#457](https://gitlab.com/commonground/fsc-nlx/issues/457)

### Bug Fixes

* **controller:** fix typo in error message ([396397d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/396397d5e887007bf7aa74e54f21adfccb11ad8d)), closes [fsc-nlx#470](https://gitlab.com/commonground/fsc-nlx/issues/470)
* **controller:** re-enable Peer suggestions for the Service Publication wizard ([a8e45ca](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a8e45ca2d5665bf50d910e8aba56fc926cc949db)), closes [fsc-nlx#479](https://gitlab.com/commonground/fsc-nlx/issues/479)
* **controller:** use correct path to prevent invalid csrf token error ([1a0fe7e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1a0fe7e2fd85ddc0ce294a340afc50bccb39e395)), closes [fsc-nlx#474](https://gitlab.com/commonground/fsc-nlx/issues/474)
* **manager:** add separate health endpoint for unauthenticated server ([f1be81d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f1be81d20fb4395e09afc6888618717c47dbb921)), closes [fsc-nlx#471](https://gitlab.com/commonground/fsc-nlx/issues/471)
* removed dead code in non-test code base ([660fecb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/660fecb8da91eefb24856814c62d75f1b47f2235)), closes [fsc-nlx#477](https://gitlab.com/commonground/fsc-nlx/issues/477)
* removed deadcode from integration tests ([65e16ec](https://gitlab.com/commonground/nlx/fsc-nlx/commit/65e16ecada8628c111ef61c7ecca8def4d88c99d)), closes [fsc-nlx#477](https://gitlab.com/commonground/fsc-nlx/issues/477)
* removed non-obvious deadcode ([8c15ef0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8c15ef081a01dd06480a88a8bd887d9def2a1c9e)), closes [fsc-nlx#477](https://gitlab.com/commonground/fsc-nlx/issues/477)

### Code Refactoring

* move code to testing directory ([41f0111](https://gitlab.com/commonground/nlx/fsc-nlx/commit/41f0111344643ca1e97ed1ae5c7b41c7dcffc2b9)), closes [fsc-nlx#481](https://gitlab.com/commonground/fsc-nlx/issues/481)

### Tests

* fix tests after postgres upgrade ([9af75eb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9af75ebb86d58d6c709d4a7c905755967585e980)), closes [fsc-nlx#476](https://gitlab.com/commonground/fsc-nlx/issues/476)

## [1.3.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.3.0...v1.3.1) (2024-10-08)

### Continuous Integration

* **helm:** ensure secret name for the imagePullSecret is provided ([f72f93c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f72f93cf3d115cf47729ae9945c0b620a408dc75)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [1.3.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.2.0...v1.3.0) (2024-10-08)

### Build System

* update dependency htmx.org to v2.0.3 ([bbfc195](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bbfc1950d82e8b9cad63d9ece6236460ea4544d1)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/getkin/kin-openapi to v0.128.0 ([4e1d864](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4e1d864a3db4594ff7c4cbc90e43c057e58ab12a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.28.0 ([0a35246](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0a352467caefa6ddffce299fbeeb37a6b226245c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.30.0 ([a554094](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a5540940980273fef38b5ead724246f830558f25)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/text to v0.19.0 ([457a576](https://gitlab.com/commonground/nlx/fsc-nlx/commit/457a576e1b2741e2ed263a95885f8ac7e82bae0c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.18.0 ([e953520](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e9535205e12ea7af286213a52ca36dff944b9bf8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Continuous Integration

* use secret containing our Docker Hub credentials when deploying to demo ([71731e9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/71731e9b502089e3a1b8b3b89bfcc017fe310f19)), closes [fsc-nlx#460](https://gitlab.com/commonground/fsc-nlx/issues/460)

### Documentation

* describe certificate renewal in documentation ([8cfa760](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8cfa760807b9f5d50cbfa3a95bf41c79e6bbfb79)), closes [fsc-nlx#461](https://gitlab.com/commonground/fsc-nlx/issues/461)

### Features

* **helm:** introduce global imagePullSecrets for our Charts ([6771e8a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6771e8ad6b078f948c626d90cbe77489bcba0461)), closes [fsc-nlx#460](https://gitlab.com/commonground/fsc-nlx/issues/460)

### Bug Fixes

* **manager:** add UTC to created at parsing from cursor ([285425e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/285425ee697f2d667d9f1a856081999789befbeb)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** expired certificate changes the Contract state to expired ([7c0909f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7c0909fa4218b7ff3911e47b28508e62f0033554)), closes [fsc-nlx#461](https://gitlab.com/commonground/fsc-nlx/issues/461)
* prevent fatal error when HTTP server returns the ServerClosed error ([956cf80](https://gitlab.com/commonground/nlx/fsc-nlx/commit/956cf80b1b710a3abf8f7924ffefbcd2a5dce3d2)), closes [fsc-nlx#467](https://gitlab.com/commonground/fsc-nlx/issues/467)
* **txlog:** initialization of health probe outside closure ([0c0fad9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0c0fad9ff151ff395882e8a01a7e67986d402f78)), closes [fsc-nlx#464](https://gitlab.com/commonground/fsc-nlx/issues/464)

## [1.2.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.1.2...v1.2.0) (2024-10-04)

### Build System

* update ricardbejarano/nginx docker tag to v1.27.2 ([91f3118](https://gitlab.com/commonground/nlx/fsc-nlx/commit/91f3118480848fbd24888f75fd6ded4c0e4e7424)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Features

* add log with error when healthcheck fails ([614cc8f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/614cc8fa1d3df2d7110dd2cda289314cb1be18ec)), closes [fsc-nlx#465](https://gitlab.com/commonground/fsc-nlx/issues/465)

### Bug Fixes

* **docs:** corrected typos in the 'setup oidc' section ([750cdde](https://gitlab.com/commonground/nlx/fsc-nlx/commit/750cddefeb154ded3354a4d4d1cc7e190e772912)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [1.1.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.1.1...v1.1.2) (2024-10-02)

### Build System

* update golang docker tag to v1.23.2 ([b74a5e2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b74a5e24cc908b610aa64827e080bac96b9276f2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.69.0 ([144236a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/144236a2fffbac632586b54adab2adbab25ea128)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Documentation

* describe how to configure an OpenID Connect provider ([a5b23cd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a5b23cd15ae08764d560f5fdcced052aaa923219)), closes [fsc-nlx#453](https://gitlab.com/commonground/fsc-nlx/issues/453)

### Bug Fixes

* **manager:** fix metrics collection ([53ef04f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/53ef04f79e61cb8d08507e701e3c3cab6178edf6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [1.1.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.1.0...v1.1.1) (2024-09-30)

### Build System

* update dependency semantic-release to v24.1.2 ([72be8fb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/72be8fb333ed43537640becbc775f621ba26f0ac)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres docker tag to v17 ([f38090d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f38090d5249d4c8dfcf1538814bf7ca677b92e2c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:17.0-alpine docker digest to 14195b0 ([13bd51e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/13bd51eefa38813a9fbf1c2777a3f7e3eaf0d010)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Continuous Integration

* use atomic covermode for unit tests to ensure counting is correct ([7eb3e14](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7eb3e146ce0ec3c85fb9c2a20e2acdb6c54ba8f6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Documentation

* add a functional description of the FSC reference implementation ([512c410](https://gitlab.com/commonground/nlx/fsc-nlx/commit/512c4100d57aefb6f6755bb8993de75470d84629)), closes [fsc-nlx#451](https://gitlab.com/commonground/fsc-nlx/issues/451)
* describe how to host a directory ([7f7ba8c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7f7ba8c8f476a4dabc6d7bf3634747bf70f2d2ae)), closes [fsc-nlx#452](https://gitlab.com/commonground/fsc-nlx/issues/452)

### Bug Fixes

* **inway:** ensure clock is passed when starting an Inway ([f150d46](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f150d46bb5c985e42577ecca231f00ec4e8f74e8)), closes [fsc-nlx#458](https://gitlab.com/commonground/fsc-nlx/issues/458)
* **manager:** filter out expired certificates when retrieving certificates from a Peer ([8ba671e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8ba671e24fa08350613cfe323292a589ae6b7553)), closes [fsc-nlx#458](https://gitlab.com/commonground/fsc-nlx/issues/458)

### Code Refactoring

* **manager:** remove unused GetPeerCerts method from repository ([e17c5e2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e17c5e2c2d9bab45476419d01be03c01738c7d81)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** remove unused ListContractsForInway method from repository ([b2e88ef](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b2e88ef3431b762319db476bab42fce8880e3137)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** remove unused ListContractsForOutway method from repository ([77cd6d8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/77cd6d8146722d5c0b2d77dd0661e44cfa040fbd)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** remove unused ListServicesDetails method ([ffea27d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ffea27dcd38f0b74cb079f00f1fcc9c1feb9eba4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Styles

* **manager:** remove redundant log ([11e9cfa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/11e9cfa1c4cf34996197e29378cae6ec66ffc66e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Tests

* **controller:** resolve flaky pgsessionstore tests ([ed140db](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ed140db07ffa41d772db79e99de908dd9468a42d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** add missing test for the internal UpdatePeer API endpoint ([368dae0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/368dae0265eaceedab8ce2f0847d23ebc5197e6a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** enable the CreatePeer test again ([bbf83ca](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bbf83caf0831ac0c7535aca6177cf6729c88dfa1)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** extend ListPeers test with test for default data from the fake Directory ([fea3bc6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fea3bc6841bae1277d8480b8810a360fb268adfd)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [1.1.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v1.0.0...v1.1.0) (2024-09-24)

### Continuous Integration

* fix patterns for matching release tags ([2371b31](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2371b31056f2b5baff2a609fb2cee6d23071b8f7)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Features

* **manager:** add metrics endpoint ([34dd7e9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/34dd7e99f238dbccb085b0074be69e8a3b9d7311)), closes [fsc-nlx#447](https://gitlab.com/commonground/fsc-nlx/issues/447)

## [1.0.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.24.1...v1.0.0) (2024-09-24)

### ⚠ BREAKING CHANGES

* **docs:** We are using this commit to trigger v1.0.0

### Build System

* **controller:** rebuild i18n catalog ([72a68ae](https://gitlab.com/commonground/nlx/fsc-nlx/commit/72a68ae9ba01109cb69e65e2c16f0c90f862875e)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* update alpine docker tag to v3.20.3 ([d38088a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d38088af2ce061b725c07f2cec37a753e1296931)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update commitlint monorepo to v19.4.1 ([3390dd4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3390dd4a50a5dab14649584237926da9d1901a38)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update commitlint monorepo to v19.5.0 ([4ff2a32](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4ff2a32675f9943d36ef48c14e7502864eefa098)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @fontsource/source-sans-pro to v5.1.0 ([ce61df3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ce61df3b758b8d2d5d7d346043f89a615a3d8897)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency dayjs to v1.11.13 ([72eea3b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/72eea3bf0b0ec0e231282cf24bead9a18a2884d0)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.16.0 ([bcf09f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bcf09f623d700235a3b0542ac62ff7965e51b8a8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.16.1 ([090cc8b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/090cc8be16fe695aa4b69b5f3810f1a629c5cc68)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v24.1.1 ([3d28fa9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3d28fa91a98918fff6d407135ab6317445416bc8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docker.io/curlimages/curl docker tag to v8.10.0 ([4f1a868](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4f1a86826f549e64b3d4d7f185f77ad9e0e30858)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docker.io/curlimages/curl docker tag to v8.10.1 ([cb6579f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cb6579ff5b8b6ec24b7a8d894ce390afb6afe5ca)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update gcr.io/kaniko-project/executor docker tag to v1.23.2 ([49bfa18](https://gitlab.com/commonground/nlx/fsc-nlx/commit/49bfa1846fedf88f8659dcdec598bb5b483831ae)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.23.1 ([b314276](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b31427693443a424374b676fd28712cf5365e010)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.12.0 ([a6392ca](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a6392ca96f43030c12d21a17b2213b4e5457a2b5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.12.1 ([95e4ea8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/95e4ea832ad35a9939821d9f55520dc486cbdf91)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/jackc/pgx/v5 to v5.7.0 ([8805055](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8805055baa8ccd250731eee7af89b0b93b1325c2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/jackc/pgx/v5 to v5.7.1 ([22a318b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/22a318bbe2b8a3ca0d46d90f076991e8d4ab0ec6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.27.0 ([cadd95e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cadd95e6a3bebf9a3ee42443971dc21c6b2c3df3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.29.0 ([93ac785](https://gitlab.com/commonground/nlx/fsc-nlx/commit/93ac785ccee058e78bfa9a4de502d96f096675d2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/oauth2 to v0.23.0 ([0c973c0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0c973c01177bbd20e7586acf33679836bdeb5056)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/text to v0.18.0 ([32e8171](https://gitlab.com/commonground/nlx/fsc-nlx/commit/32e8171b379dd7b4fb9cdf79df69e40da69f5938)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 008735b ([5e2e628](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5e2e628b4ce1e78c3e53e4fbeb6d4ad314883813)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 2d07db0 ([a5c4760](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a5c4760fef08efca763cc64be025f5579540d2f5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 8329a07 ([3f9d5a4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3f9d5a4750b13d0e201e7d624904f8b2c0c0624c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to bec0ea4 ([3e0c8fe](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3e0c8fe450d8efe5d1940a52c3d91465f8f71119)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.17.0 ([56b6b39](https://gitlab.com/commonground/nlx/fsc-nlx/commit/56b6b39ccd18b2ddefcc6e39f25ac683bc9cec45)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v22.7.0 ([811d9f7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/811d9f73cca36213a5b513d288337f50f91a10e5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v22.8.0 ([ce9afb5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ce9afb562ace65ec08e7cbdf2c78e47154e35a68)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v22.9.0 ([1143035](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1143035b95b5b7f8c0d5e3b5d59b1ae0335fb448)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.68.0 ([e18c3ea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e18c3ea08acb9fdd3f4370943bc65005f4b1ac64)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.4-alpine docker digest to d898b0b ([c253dc1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c253dc11936f900670533e471b9955fda246fe9e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.4-alpine docker digest to f9ea7b4 ([b1348a8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b1348a81e5ec25fd311dbb6efdfede618ea68d59)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Continuous Integration

* deploy tags of pre-releases to Review instead of Demo ([44755c6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/44755c6c4f91f09d491706a3bfe01682e5063083)), closes [fsc-nlx#440](https://gitlab.com/commonground/fsc-nlx/issues/440)
* enable releasing from multiple release branches ([4705fca](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4705fcaefdd94306656e7d7fa5af13df19d2647e)), closes [fsc-nlx#413](https://gitlab.com/commonground/fsc-nlx/issues/413)
* run 2 instances in demo environment ([86e5ebc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86e5ebcf0cd6c8d719b74e9814294a281bed4e9b)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* use valid commit message for the release commit by Semantic Release ([d3fb22f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d3fb22fd37b59ee9b6a254ef9bf79fc62df1460f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Documentation

* add implementation choices to the read me ([2d486e4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2d486e439edab2070975ee07aa6fd7117ac7fc16)), closes [fsc-nlx#421](https://gitlab.com/commonground/fsc-nlx/issues/421)
* add release instructions to the README ([24a2a5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/24a2a5e149a46df3c5521cc7c0eacdcae64b06aa)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* add System Requirements for an FSC NLX installation ([47e0931](https://gitlab.com/commonground/nlx/fsc-nlx/commit/47e09316190be8347c6dd9763bb01fb1e5ba9c4b)), closes [fsc-nlx#450](https://gitlab.com/commonground/fsc-nlx/issues/450)
* improve release instructions for Release Candidates ([668ea44](https://gitlab.com/commonground/nlx/fsc-nlx/commit/668ea4492362fc0f9739afe8e703924024f88851)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* revisit Release Strategy ([cf8951f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cf8951f17fb4752b1a87ec9f1f21842b3c910c72)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update component overview ([9fcc210](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9fcc210a597b9540eb3f821f5c1145a418299a16)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update component overview ([6f8fbe3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6f8fbe3f60b3a7c12de954cf5e9f13ee60d57d20)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update contributing.md ([54cde91](https://gitlab.com/commonground/nlx/fsc-nlx/commit/54cde91f915b52eb0a725fac5cad48d3d6ccc572)), closes [fsc-nlx#422](https://gitlab.com/commonground/fsc-nlx/issues/422)
* update release strategy ([1b1855d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1b1855d7acc2a9a958d5ce83622145ac833c9d2f)), closes [fsc-nlx#413](https://gitlab.com/commonground/fsc-nlx/issues/413)

### Features

* **controller:** add 'Proposed' tab for the Delegated Connections ([fce703a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fce703a9da8675ff18696b8c6cb1db4ad60910a3)), closes [fsc-nlx#417](https://gitlab.com/commonground/fsc-nlx/issues/417)
* **controller:** add 'Proposed' tab for the Incoming Connections ([1217c17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1217c17d206e20a010947d261621b1fe680ba241)), closes [fsc-nlx#410](https://gitlab.com/commonground/fsc-nlx/issues/410)
* **controller:** add 'Proposed' tab for the Publications page ([4489039](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4489039be2a525ad30042b60421b7b4ece281d60)), closes [fsc-nlx#417](https://gitlab.com/commonground/fsc-nlx/issues/417)
* **controller:** add copy button for the Grant Hashes of the Contract Detail page ([1397ba2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1397ba2f1a38ac8e652b2e987c52bb9391a9489e)), closes [fsc-nlx#431](https://gitlab.com/commonground/fsc-nlx/issues/431)
* **controller:** add page for listing Registered Inways ([a50e9a6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a50e9a6f55137b2b59db41fde8b5f7f301ed8763)), closes [fsc-nlx#412](https://gitlab.com/commonground/fsc-nlx/issues/412)
* **controller:** add page listing Inways and Outways ([b87fd46](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b87fd469623d315edc272edde2a1ee7f8da8c8ff)), closes [fsc-nlx#411](https://gitlab.com/commonground/fsc-nlx/issues/411)
* **controller:** implement submenu for the Primary Navigation ([68f0355](https://gitlab.com/commonground/nlx/fsc-nlx/commit/68f035502ff9c38279a7e5127c84737e5f112bef)), closes [fsc-nlx#415](https://gitlab.com/commonground/fsc-nlx/issues/415)
* **controller:** improve labels of the transaction log table headers ([f35b4a5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f35b4a54d9c2531099f9957cdda1e4cfec3fd730)), closes [fsc-nlx#429](https://gitlab.com/commonground/fsc-nlx/issues/429)
* **controller:** improve performance directory service detail view ([aa79bba](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aa79bba1c685fa842af808b1223c9c254c0834d6)), closes [fsc-nlx#437](https://gitlab.com/commonground/fsc-nlx/issues/437)
* **controller:** improve performance of the service detail page ([c140b44](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c140b44b741aaa96a58c90277b5d46fb53615b4a)), closes [fsc-nlx#437](https://gitlab.com/commonground/fsc-nlx/issues/437)
* **controller:** improve styling of listed Outways ([441a018](https://gitlab.com/commonground/nlx/fsc-nlx/commit/441a018d33dc02d79591c0466a91d61a0bc7432f)), closes [fsc-nlx#456](https://gitlab.com/commonground/fsc-nlx/issues/456)
* **controller:** proposed tab added to delegated service publications ([70f6ca2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/70f6ca278e1dad753a90d04ee3148dbf9e5b8222)), closes [fsc-nlx#418](https://gitlab.com/commonground/fsc-nlx/issues/418)
* **controller:** remove Contract state filter for Outgoing Service Connections ([3ceacfc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3ceacfcd2d1847ea2bb4107832c1ae80bf3971fc)), closes [fsc-nlx#438](https://gitlab.com/commonground/fsc-nlx/issues/438)
* **controller:** reorder Primary Menu items ([41ae6a2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/41ae6a281aa5c5291f2b7b5431968239890ee87d)), closes [fsc-nlx#415](https://gitlab.com/commonground/fsc-nlx/issues/415)
* **controller:** retrieve outgoing connections per service ([4e2dd95](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4e2dd95c4f84869f8cf09e8951697d4ce1a405d7)), closes [fsc-nlx#438](https://gitlab.com/commonground/fsc-nlx/issues/438)
* **controller:** show most recently created Delegated Service Connections on top ([326f786](https://gitlab.com/commonground/nlx/fsc-nlx/commit/326f786a9b6f9e8ed2557b7ec6a68433b319d3ed)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **controller:** show most recently created Outgoing Service Connections on top ([66d63e5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/66d63e5b1f59ad941863cfe1da9bb3b09631c8a3)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **controller:** show most recently created Service Publications on top ([5debefb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5debefb576885d4f20cb0d504d0530082a998ea9)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **controller:** swap icon for the Actions page ([7f03e5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7f03e5e614e13d0be4842b90158c378a6f5f686e)), closes [fsc-nlx#415](https://gitlab.com/commonground/fsc-nlx/issues/415)
* **controller:** update icon for the Proposed tab ([4b20a79](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4b20a7967a44ee6780f6984900d1c24fec4bd608)), closes [fsc-nlx#417](https://gitlab.com/commonground/fsc-nlx/issues/417)
* **manager:** add auto sign functionality ([197452f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/197452f0a8ab95fe6ed4a7d2c8027a24ee036617)), closes [fsc-nlx#376](https://gitlab.com/commonground/fsc-nlx/issues/376)
* **manager:** order Delegated Service Connections on Contract creation date ([1f9bacc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f9bacc934b8832af1b567e218a94fb7b0fe5916)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **manager:** order Outgoing Service Connections on Contract creation date ([8df3210](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8df321024135f4856100a3ac0a99f3e2856bbb9d)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **manager:** order Service Publications on Contract creation date ([babd795](https://gitlab.com/commonground/nlx/fsc-nlx/commit/babd795fd1f71aae74ff3f78ddcc2061d50ef5a0)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **manager:** rename GetServicePublications to ListServicePublications ([dc61a87](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dc61a87bf38b6c8f50291a69a8432675c3c22042)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)

### Bug Fixes

* **controller:** added text to remove service confirmation ([3424ed7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3424ed70eace42ce597a352afa8743e81f68abd2)), closes [fsc-nlx#427](https://gitlab.com/commonground/fsc-nlx/issues/427)
* **controller:** back button still works after signing Contract ([c9b555b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9b555b12011da1d05808e02c5882f40b6a70930)), closes [fsc-nlx#425](https://gitlab.com/commonground/fsc-nlx/issues/425)
* **controller:** changed reset pagination text to back to top ([23b2f5c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/23b2f5cb9433aa15375f0ea2fdcc60ab336b46cd)), closes [fsc-nlx#435](https://gitlab.com/commonground/fsc-nlx/issues/435)
* **controller:** improve directory services page ([30d4919](https://gitlab.com/commonground/nlx/fsc-nlx/commit/30d49199ed8d59be67db87f476b4ef6550779ad4)), closes [fsc-nlx#428](https://gitlab.com/commonground/fsc-nlx/issues/428)
* **controller:** redirect to the Directory as default landing page ([4d7139c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4d7139cb721b60e61b92ebf13113317230a4672b)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)
* **controller:** trim spaces for peer ID inputs ([c8cec44](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c8cec447145d7a2ac3b1be457bb13fe56949041a)), closes [fsc-nlx#443](https://gitlab.com/commonground/fsc-nlx/issues/443)
* **controller:** used American spelling for organization ([ce62823](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ce628232628242077ffd81651307209a2a9122c2)), closes [fsc-nlx#432](https://gitlab.com/commonground/fsc-nlx/issues/432)
* **directory:** ensure the base URL is used for the navigation links ([667800d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/667800d53d6ae47a2f2f76b435965f3f841a112d)), closes [fsc-nlx#433](https://gitlab.com/commonground/fsc-nlx/issues/433)
* **directory:** fixed broken links and used fsc ([a123bd9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a123bd95176e6088538e9faffb4e2bf251cdc147)), closes [fsc-nlx#433](https://gitlab.com/commonground/fsc-nlx/issues/433)
* **helm:** allow using prerelease versions for Gemeente Stijns ([8ee4bd7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8ee4bd733ed48735d82904f1c663556a74fd2906)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** allow using prerelease versions for our Helm charts ([19ca3f4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/19ca3f4d08de8702997250d7751d50fa4608df4a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** update lockfile of the Gemeente Stijns Helm chart ([9810502](https://gitlab.com/commonground/nlx/fsc-nlx/commit/981050200562fec4d62adb58c92f42aa9e1f7558)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* liveness and readiness probes verify if http servers are up ([70d5657](https://gitlab.com/commonground/nlx/fsc-nlx/commit/70d56576a9c110e85da91a000caaa285bd7448d1)), closes [fsc-nlx#141](https://gitlab.com/commonground/fsc-nlx/issues/141)
* **manager:** ensure the pagination cursor for (Delegated) Service Publications is correct ([756065a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/756065a9a5d814b5478185b6ddc08bfd04fb475d)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **manager:** ensure the pagination cursor for List Incoming Connections is correct ([d718730](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d718730b4e6f5fce405b3aea8e2d5277c29b4690)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **manager:** ensure the pagination cursor for List Outgoing Connections is correct ([7668207](https://gitlab.com/commonground/nlx/fsc-nlx/commit/76682072587013cbb1a26f6e2bfc8b601adae61b)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **manager:** ensure the total count of Delegated Connections is correct ([7269fef](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7269fef622324a882e0ec48d800155e694ac55e3)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)
* **manager:** ensure the total count of Incoming Connections is correct ([cf8ee46](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cf8ee46cc9a3b24697117aa9493d2d829406e61f)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)
* **manager:** ensure the total count of Outgoing Connections is correct ([517762b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/517762b20aa0f4c5e0028e3c5794a3424c9c20b9)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)
* **manager:** ensure the total count of Service Publications is correct ([31e4794](https://gitlab.com/commonground/nlx/fsc-nlx/commit/31e4794988d7057631086c0192f8437d73674e8f)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)
* **manager:** prevent nil pointer exception when processing pdp response ([6c8613a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6c8613a6bc54d4d3bc251ac6711dd0b875fe4c42)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Performance Improvements

* add resource limits to helm charts ([59ee020](https://gitlab.com/commonground/nlx/fsc-nlx/commit/59ee0201f6380ed8fc44511b44f9aa001443b2a7)), closes [fsc-nlx#419](https://gitlab.com/commonground/fsc-nlx/issues/419)

### Code Refactoring

* **controller:** enable passing pagination sort order for the Incoming Service Connections ([aeec7c7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aeec7c74a13005406bc48f7a2cf9c48861d079b0)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **controller:** enable passing pagination sort order for the Outgoing Service Connections ([3feff85](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3feff85282f424e16eb8a519d0bada46ea52c8fa)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **controller:** enable passing pagination sort order for the Service Publications ([9879a4c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9879a4cf50c5893345d2c39c9144367e5b90f5bf)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **controller:** improve performance service overview ([3139dda](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3139dda5787651329fd00d08d6b61aa71d72dc77)), closes [fsc-nlx#437](https://gitlab.com/commonground/fsc-nlx/issues/437)
* **controller:** rename New -> newSessionStore ([250e7c6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/250e7c678779a2cbc675717c15f099e0c64e26fe)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **controller:** rename variables related to the selected tab to improve readability ([db91534](https://gitlab.com/commonground/nlx/fsc-nlx/commit/db91534dcffba23e088100c0c128df59e1ce6245)), closes [fsc-nlx#417](https://gitlab.com/commonground/fsc-nlx/issues/417)
* **controller:** update page titles & styling ([1747143](https://gitlab.com/commonground/nlx/fsc-nlx/commit/174714371c721c6fa4273dc1fdece5692ebb0524)), closes [fsc-nlx#432](https://gitlab.com/commonground/fsc-nlx/issues/432)
* **controller:** wrap menu items inside list element, so we can add submenus ([5302a05](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5302a05951af0b3bff5cb5e0031db30420ba18a6)), closes [fsc-nlx#415](https://gitlab.com/commonground/fsc-nlx/issues/415)
* **manager:** define auto-sign interface using OAS ([06ccc1a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/06ccc1aa384d6603579d0017ba0fbb0e073e11df)), closes [fsc-nlx#376](https://gitlab.com/commonground/fsc-nlx/issues/376)
* **manager:** pass logger to the ListOutgoingConnections query ([c7dbc36](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c7dbc36446eee2c32fc6cfbabff96a7508bae0c2)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **manager:** rename test to be consistent with other filenames ([fd5dd3b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fd5dd3bb27561fe53bf0a3cb486a836cb0ad39df)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)
* **manager:** replace errors package ([4ec1c4e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4ec1c4e0ddf7b893771d720d83e7d9246b361a12)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* remove unused [@only](https://gitlab.com/only) tag from the E2E tests ([c8852a0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c8852a0268b17a076f0e675287b8bf3f1718cb2c)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* use common log interface ([bb8df2a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bb8df2a96f384cf8c515caf814950f56469f9638)), closes [fsc-nlx#151](https://gitlab.com/commonground/fsc-nlx/issues/151)

### Styles

* **docs:** add new lines to the section titles of the Release Strategy ([19fad22](https://gitlab.com/commonground/nlx/fsc-nlx/commit/19fad225d5f528a32245b02bffcf02c66fd746e9)), closes [fsc-nlx#317](https://gitlab.com/commonground/fsc-nlx/issues/317)
* fix typo for the E2E tests ([5fd340b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5fd340b26e48326e2d2f3d2d33bc64c6896808b2)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)

### Tests

* add E2E test for auto-sign Contracts using OPA ([079d879](https://gitlab.com/commonground/nlx/fsc-nlx/commit/079d87904b0426adada338f60e3e9725a0c64faa)), closes [fsc-nlx#376](https://gitlab.com/commonground/fsc-nlx/issues/376)
* add E2E test for the Active Outgoing Connections page ([b1be339](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b1be3398726e6f0f05b268a5855bb49d24dec589)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* add E2E test for the Archived Outgoing Connections page ([35d8a1b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35d8a1b648f66ac9e43a55d624104451f6fa396b)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* add E2E test for the Proposed Outgoing Connections page ([0acde85](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0acde8539ad96335c743e51c23f81d5dbb8da487)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* add test for the internal Manager ListDelegatedPublications endpoint ([65f291c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/65f291c537475637008227dc0e3d000a03f5a138)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)
* add timestamp to E2E test log so we can debug ([8e268a5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8e268a5155c493c699d1decf4520976549026739)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **controller,txlog:** ensure SSL mode is disabled when running integration tests for DB ([22d1be0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/22d1be0d792a41a56a88d36bca06651d753bd860)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **controller:** improve assertion text ([15ab4f7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/15ab4f7aec186ad55b4ce120e0532be90c6880f5)), closes [fsc-nlx#417](https://gitlab.com/commonground/fsc-nlx/issues/417)
* fix incorrect grant type mapping for the DelegatedServicePublication Grant ([1dd244d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1dd244dde673c131880d952fd1612e597d534dff)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)
* make the distinction between Incoming and Outgoing Service connections clear ([4067e88](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4067e88aadcd2e47cba6f4fcff64d90b5cff1b0b)), closes [fsc-nlx#441](https://gitlab.com/commonground/fsc-nlx/issues/441)
* **manager:** add test for filtering Incoming Connections by Contract state ([e88e82f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e88e82fcc71306fe987c637e5e59a87aa80d6b2d)), closes [fsc-nlx#414](https://gitlab.com/commonground/fsc-nlx/issues/414)
* **manager:** ensure the test result is stable ([38f99d4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/38f99d4faf1872172d0bd24f744a4f77923eb0df)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)
* pagination is used in the e2e tests ([920c314](https://gitlab.com/commonground/nlx/fsc-nlx/commit/920c31466094df02487fdc7b2a9592a213484e41)), closes [fsc-nlx#423](https://gitlab.com/commonground/fsc-nlx/issues/423)
* prevent the test for ListOutgoingConnections with ContractStateFilter being flaky ([57a3eea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/57a3eea2c6036d442d4af1ea4678e633a4b8292f)), closes [fsc-nlx#430](https://gitlab.com/commonground/fsc-nlx/issues/430)

## [0.24.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.24.0...v0.24.1) (2024-08-20)

### Continuous Integration

* add trailing /v1 to the Docker Hub URL for PROD Docker images ([f475512](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f475512c342e12c9741961569e6784ef79b8563d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [0.24.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.23.1...v0.24.0) (2024-08-19)

### Build System

* **controller:** remove dependency on hyperscript.org ([93092d1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/93092d19da5d25148497808c0403c7d6cd432060)), closes [fsc-nlx#331](https://gitlab.com/commonground/fsc-nlx/issues/331)
* **controller:** remove unused custom i18n implementation ([ed3f80a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ed3f80a85e5f18d36ba04e77f2c4d4475fc7f9bf)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* downgrade replace-in-file v8 -> v7 to prevent ES Module issues ([94b01e3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/94b01e385233cf0fce8ed5367bd441370056cb99)), closes [/gitlab.com/commonground/nlx/fsc-nlx/-/jobs/7611552115#L108](https://gitlab.com/commonground//gitlab.com/commonground/nlx/fsc-nlx/-/jobs/7611552115/issues/L108) [/github.com/adamreisnz/replace-in-file/blob/main/CHANGELOG.md#800](https://gitlab.com/commonground//github.com/adamreisnz/replace-in-file/blob/main/CHANGELOG.md/issues/800) [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* enable Delve locally for the Controller of Organization B ([3e6e399](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3e6e3994d648c77676858e193caa045fb4f045af)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* ensure the order of the translations is stable ([98df61b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/98df61b3070356ec34a38ce5581c715570ee54b7)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* migrate Docker images to a new registry 'federatedserviceconnectivity' ([8b8e760](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8b8e760efaac1f1762abe79f05370062db2c6839)), closes [fsc-nlx#365](https://gitlab.com/commonground/fsc-nlx/issues/365)
* prevent delay when resolving .local vhosts on Mac OS ([ef531f8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ef531f8042616c4f1faf5e3d79c9ae7d335aed0f)), closes [fsc-nlx#408](https://gitlab.com/commonground/fsc-nlx/issues/408)
* remove deprecated Docker Compose version property ([29ea91e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/29ea91e7e63b23211796a92a32f5cf87cddf54ae)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* replace personal repository for i18n with FSC NLX repository ([60f1985](https://gitlab.com/commonground/nlx/fsc-nlx/commit/60f1985853cc555e51c96b0887be22f446b98cea)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* reuse wait-for-http script to check if Keycloack is up and running ([00f929c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/00f929c3baf0bf033c3e8bbdcb957e7a132333fb)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* update alpine docker tag to v3.20.1 ([e7bb256](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e7bb256b00c1a081e8830f165bcd88452ddd01bd)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update alpine docker tag to v3.20.2 ([16bcff4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/16bcff4b2a3f7bd94206837428c457515b0047ef)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @commitlint/cli to v19.4.0 ([c9f10ed](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9f10edcba23081db706453a99358f0bfaebda49)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @semantic-release/gitlab to v13.2.0 ([a80d04a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a80d04aa7fba169a0498011df4402284daff4baa)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @semantic-release/gitlab to v13.2.1 ([da710cf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/da710cf644302cf058ff7e8dae7549cbcb802f59)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency dayjs to v1.11.12 ([c258258](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c25825828e7fadb405c0a1939dd9844eab4a9729)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency glob to v10.4.2 ([f328145](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f3281456f2eaf180f8c53ba88e082a4f5e72d350)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency glob to v10.4.3 ([52d07f2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/52d07f28cfb24465cfaf1ba54007534a10f6c01f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency glob to v10.4.4 ([8c18964](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8c189646bf88421d160877d0fff5514adfabc2c3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency glob to v11 ([2d8c51e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2d8c51efc4d1e4ed51c61d662c084e2b2ee3b014)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.15.2 ([da549a3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/da549a33c96a3736b649b317aea1c4910bd4bef9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.15.3 ([727da7c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/727da7c3b7495e4981353bd6c923c098e2d542d0)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.15.4 ([d142923](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d1429238e4269bf570c15639a1ed886409958198)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency htmx.org to v2 ([d4dbf89](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d4dbf892b8431c20215ec0bd29d3d080eb739a99)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency htmx.org to v2.0.2 ([9bab838](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9bab83868b89e04487b0b8bf0be2d7600cc55d26)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency prettier to v3.3.2 ([2ec24ff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2ec24ff92ed70da5fa5314321df2dab0f420d2dd)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency prettier to v3.3.3 ([a9d4eb0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a9d4eb0ecf9c5eec3c505d560f2561d75f13cdbf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency replace-in-file to v8 ([a4a9e5c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a4a9e5ca20f4feef2c6f6b1417db7b7fcf34d3ed)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency replace-in-file to v8.0.1 ([e3c2f69](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e3c2f6956b6b706674a9478dbb6d8a92d41f64d6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency replace-in-file to v8.0.2 ([74db78c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/74db78c143b71f01fdc775091b2eaa7c0a353fbc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency replace-in-file to v8.1.0 ([b3a80d0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b3a80d0794c9130729644307688f6a4a60b29955)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v24.1.0 ([e5f5f8d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e5f5f8dbe7bb7f04925318a8331732a41e6a60b1)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docker.io/curlimages/curl docker tag to v8.9.0 ([726c13c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/726c13caadb6e005e36cee11fdf8294bae709743)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docker.io/curlimages/curl docker tag to v8.9.1 ([99b688b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/99b688bddedd00dec9108171cba552ba9467b5a4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docusaurus monorepo to v3.5.1 ([f138ccc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f138ccc2e31820f99705343f42f204cd85c985b7)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docusaurus monorepo to v3.5.2 ([8a8bcd2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8a8bcd20a9dfc054ef4f5912aa594b55998b9d13)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update gcr.io/kaniko-project/executor docker tag to v1.23.1 ([c5984e2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c5984e2f3bfa04496b3bf0b24ba05046fb5a7f38)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update ghcr.io/go-rod/rod docker tag to v0.116.1 ([3c1cdbf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3c1cdbf534ee76397ab290d4b24de18074140993)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update ghcr.io/go-rod/rod docker tag to v0.116.2 ([0c3ba68](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0c3ba68239f0f38ecd0dc3f9cad28a6aa5c433a4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.22.5 ([b7f6e95](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7f6e95e1a3721d893c784e22bb07570a4f1447a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.22.6 ([2ba5d50](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2ba5d5007b02e6469720a629df88b8dbaadaef19)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.23.0 ([5a65f1e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5a65f1e31b28955fae701f3f33b409ee9376c902)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.4-alpine docker digest to 32c8500 ([519a840](https://gitlab.com/commonground/nlx/fsc-nlx/commit/519a84035e8eee0f4d1ff1bcac39ca57cc0a7e60)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.4-alpine docker digest to 6522f0c ([65067d2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/65067d20b9d928e5d80d710dcebaca7fa95356df)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.4-alpine docker digest to 8b55159 ([d91e863](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d91e863bae8c13ea26a422418f61cf9a3cd5068b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.4-alpine docker digest to ace6cc3 ([64df63d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/64df63d72ffc7aef52a113b9300b8727894e1a03)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.5-alpine docker digest to 0d3653d ([cd9f270](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cd9f27095bfa8ab1c4d2bdd60ca161cbf9393fbc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.5-alpine docker digest to 8c9183f ([4db6281](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4db628145eac07d1a2a549de2c7621620770c7b6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/coreos/go-oidc/v3 to v3.11.0 ([45a7c72](https://gitlab.com/commonground/nlx/fsc-nlx/commit/45a7c729d2271bc9b676dff94913de18642492e5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/getkin/kin-openapi to v0.125.0 ([5fd7bca](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5fd7bcabee89c5e4367fe909ebd9778ac3dafad8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/getkin/kin-openapi to v0.126.0 ([2278d66](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2278d66c9def77143f541dab67a1cdf5eb6ee846)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/getkin/kin-openapi to v0.127.0 ([03b8a23](https://gitlab.com/commonground/nlx/fsc-nlx/commit/03b8a23dce411c9dad5eb4be3257275a84c9edb3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-chi/chi/v5 to v5.0.13 ([25fe087](https://gitlab.com/commonground/nlx/fsc-nlx/commit/25fe0878462dc460039bb91f428ce32b85f1c0d4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-chi/chi/v5 to v5.0.14 ([989720f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/989720fd5afa5eb895cdf55c76981da080803050)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-chi/chi/v5 to v5.1.0 ([fb34442](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fb34442ebfcb5a753837300f7df3b300d6a5e18d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.10.0 ([b468721](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b468721088ed00701be6dfa107bb5f26206d2477)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.10.1 ([fadeef2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fadeef2dfe6f5802b39c1f3f551a2ee9cf8b908c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.11.0 ([cda8569](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cda85699ace15e21e777a80d91f1dcc6ffeddc39)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.7.0 ([bc74583](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bc745836b03a7c4f4e1e057a44b878c3af696965)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.7.1 ([5ef07bb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5ef07bbec8c38a3c4ec616ab7dcefd298570628f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.8.0 ([254480d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/254480da7bc6daed5d4ac5f13974e13a2540d773)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.9.0 ([703655d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/703655d344d7509b60c2c08529a6b29603247f6e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-rod/rod to v0.116.1 ([679a6d1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/679a6d1ad8e35343fdd8d61a85ab8c980af5e1c5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-rod/rod to v0.116.2 ([a2fb20e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a2fb20e57fc2c51d226453b2e3e0f3f3922db9b3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/gorilla/sessions to v1.3.0 ([c337558](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c3375585e52ed154cc22aef0a4468eff59aae666)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/lestrrat-go/jwx/v2 to v2.1.0 ([ae61707](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ae61707d6996c25bf9aa70694983d1903fa99636)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/lestrrat-go/jwx/v2 to v2.1.1 ([45b701b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/45b701b75984f1e5159981588bb7fa01d643d575)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/spf13/cobra to v1.8.1 ([b0539be](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b0539be4b07a3b55fd0e0c2389e28783ae37d808)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.25.0 ([3def081](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3def081b13737f9bda3cc93fdc976ae16ff2517c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.26.0 ([56a80eb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/56a80eb34f1f99b7e3832ca6c75fe6376f665289)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.27.0 ([0e6ce72](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0e6ce722edc2c5e1f6762db355136935c40ee9b8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.28.0 ([12c132c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/12c132cd1773c928d0e89ca21eac4f0094b8dd68)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/oauth2 to v0.22.0 ([ad08edf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ad08edf97d64a454fbff76154d1cc4b899da7176)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/sync to v0.8.0 ([60cf6c0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/60cf6c09097ea251dde6ad18512a15c28e224c79)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node docker tag to v22.6.0 ([42865f3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/42865f3274f7d786bcba3c1e09fb96f0d0abd48a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node:22.6.0-alpine3.20 docker digest to 4162c8a ([2236a2b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2236a2b2ed8063bbde8684aa70e4c2ea0d33f272)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 04867dd ([f3a51c2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f3a51c2967bd9171e121bb7652be842e9ba3ff67)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 09dbe0a ([9232f4b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9232f4b0776aec6e57d3fada714e443ef7c0d8cd)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 0c82c39 ([b84ecf0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b84ecf0bab60b7d65b6e8e074276c5241dde85e1)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 1e847dd ([af172d6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/af172d6599227d1cd69bd67df42c197b0a0f2b2b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 21d994f ([013e90f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/013e90f9c74d313185340dbb35440beb5063c57d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 34b7aa4 ([1292a45](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1292a4582d7ce36322cd020ea685069537c7d46e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 415f321 ([0cb0d63](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0cb0d638eedaade5e3f65eb6e1711894f12daa6a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 6601779 ([c9d2390](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9d2390705a19c8eeba93d7aaa25ae8bebf79899)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 804aa6a ([f10a181](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f10a181716b9f29cf73a70e74c5ef7f35378d4f0)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 9fcc1a6 ([490200b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/490200b4f1f6050c3fc8183492e98643a41ee408)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to df01469 ([f006ed4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f006ed45129cb96b436eddbd6a18591d4744534a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to df46071 ([fca8941](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fca8941493891a3f93717828bc5925bdde7c6b39)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to eb8101c ([062782f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/062782f32fc0df3928918b2bb70d482f170e289f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.15.0 ([0c3a7a4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0c3a7a425fa6b90983dded606ae18e60ebfa3177)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.15.1 ([f1d71bf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f1d71bf7fe10d94b2e251a1a33c0df8487ec179d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.16.0 ([d41ace6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d41ace6690e55e8582f54e66e5ced8303f41cd5f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v22.3.0 ([b145956](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b145956a9d8401f378f6c3548e1744534103403c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v22.4.0 ([5bb9d40](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5bb9d40690cfd517ede1bc9432e5466f0ab7d040)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v22.4.1 ([d71b65a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d71b65a2de72902948d1ae482b855093ca73d292)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v22.5.0 ([a793b5a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a793b5a82362f3ac3ff6872bf29bf6f02e25bbd5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v22.5.1 ([44d14dd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/44d14ddccc028e9c5b2f10767270a83094d29413)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.66.0 ([b5e0530](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b5e05304eb9e3702b984fccf74ce766f1c189d61)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.67.0 ([bab0da4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bab0da487a85fee3504988d6c614f594576dc2ce)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.67.1 ([6a574bb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6a574bb4e4e1532ee8e31730050cf1d76dc41ddf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres docker tag to v16.4 ([9296603](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9296603fbf9a0df927be32020708f5d1c39f2680)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.3-alpine docker digest to 108c3ea ([a09a82f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a09a82f89ae7a0ab1d62c6ed8f94a2706c7ad8a4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.3-alpine docker digest to 36ed712 ([e945fb4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e945fb44ad4aa9971544325f2097a0191cb2f213)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.3-alpine docker digest to 3af2a1d ([f8ed83d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f8ed83df1ceb17177e568e6345caead5d1b27173)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.3-alpine docker digest to de3d7b6 ([f37b798](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f37b798f90e0ed83c10b3536842ff34c4f1f177e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update ricardbejarano/nginx docker tag to v1.27.1 ([1198e49](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1198e497980c359eb2d29abf80bba5140f1421f8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Continuous Integration

* add Gemeente Riemer to local dev environment ([df92596](https://gitlab.com/commonground/nlx/fsc-nlx/commit/df9259640841176245d00c31a6c5e2aaf05eb1b7)), closes [fsc-nlx#326](https://gitlab.com/commonground/fsc-nlx/issues/326)
* add v1 to dockerhub path ([1c6fe46](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1c6fe4671f332d818a6e550dc12fc463aa26f38b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* fix docker compose not starting correctly ([2e0c342](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2e0c342d6a23d7dae7bec5372ce496012ae771e5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* fix modd entries for ui assets ([a19e4a4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a19e4a4825612bcce0a58c6cf612a83849b85044)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* fix ui assets not building on local dev environment ([9453a98](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9453a984bb079374d51f05f355ef257bf15b1986)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** change default image used by the keycloak chart ([55de08d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/55de08da11a334c7b50c624848285b42e128b3af)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* increase postgres conn limit for local dev environment ([85e69fa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/85e69fa4ae8082b4390ba27f640d976e6ccf1c10)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* optimize start development script startup time ([160b8e7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/160b8e7505c104ab2e0c9a7b175c210939be00f6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* revert fix modd entries for ui assets ([71a8569](https://gitlab.com/commonground/nlx/fsc-nlx/commit/71a85692067e8fc5d2ed5d5d161cf2aaac90aba0)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* setup authentication to Docker Hub for the ACC and Review build step ([742c462](https://gitlab.com/commonground/nlx/fsc-nlx/commit/742c4625fe20fd531c91fd1e457cbb615f87b41f)), closes [fsc-nlx#365](https://gitlab.com/commonground/fsc-nlx/issues/365)
* use the correct image names in the container scans ([f3c501b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f3c501be2b3ccff25911536a48c8192d93a47977)), closes [fsc-nlx#365](https://gitlab.com/commonground/fsc-nlx/issues/365)

### Documentation

* add RELEASE_STRATEGY.md describing the release strategy for FSC ([c816cf1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c816cf1937505b00e3c558bceb3ce33c7fc93f85)), closes [fsc-nlx#367](https://gitlab.com/commonground/fsc-nlx/issues/367)
* update CONTRIBUTING.md for FSC-NLX ([d0e5d10](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d0e5d1014083448298f8ea0ff84a858149fe98cd)), closes [fsc-nlx#381](https://gitlab.com/commonground/fsc-nlx/issues/381)

### Features

* add v1 to api paths ([c29455a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c29455a870bdc3080d8628eb99c3b3d135a642d0)), closes [fsc-nlx#337](https://gitlab.com/commonground/fsc-nlx/issues/337)
* auto connect to database when conn is closed ([dd62ca6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dd62ca6a8bdbb3d94ebd91b68568f7fede4a635c)), closes [fsc-nlx#342](https://gitlab.com/commonground/fsc-nlx/issues/342)
* **controller:** add 'Proposed' tab for the Outgoing Connections ([06bb4ef](https://gitlab.com/commonground/nlx/fsc-nlx/commit/06bb4ef42f1d1631cc150a321c4ded5bb64d2251)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **controller:** add actions page ([7bfcc0c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7bfcc0cfa0b6d484f1f32f8b11fc45b0a22a297c)), closes [fsc-nlx#339](https://gitlab.com/commonground/fsc-nlx/issues/339)
* **controller:** add archive tab to delegated connections tab ([a039667](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a03966758795a38c2a9db636ba253ccb8aa202cd)), closes [fsc-nlx#378](https://gitlab.com/commonground/fsc-nlx/issues/378)
* **controller:** add archive tab to incoming connections page ([771cd6d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/771cd6d39edda83bb78250fd23dc98142125a45a)), closes [fsc-nlx#378](https://gitlab.com/commonground/fsc-nlx/issues/378)
* **controller:** add archive tab to outgoing connections page ([fd98cba](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fd98cbafcb5ce45e69f24484f2808eab06255e90)), closes [fsc-nlx#378](https://gitlab.com/commonground/fsc-nlx/issues/378)
* **controller:** add archive tab to publications page ([6e738d4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6e738d4e17536967b608db1d250dad5e2c5edd8f)), closes [fsc-nlx#378](https://gitlab.com/commonground/fsc-nlx/issues/378)
* **controller:** add CSRF protection to the webinterface ([0eb48fe](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0eb48fe054255fef02fea4bb355b22b2a09766a2)), closes [fsc-nlx#350](https://gitlab.com/commonground/fsc-nlx/issues/350)
* **controller:** add delegated connection wizard ([1298aff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1298affeadbe92f4a8413234472e2a2ac61c6571)), closes [fsc-nlx#393](https://gitlab.com/commonground/fsc-nlx/issues/393)
* **controller:** add delegated connections page to UI ([bf391c7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bf391c77665302e9c414c1a73a031fb971484352)), closes [fsc-nlx#370](https://gitlab.com/commonground/fsc-nlx/issues/370)
* **controller:** add delegated publications page to UI ([70ba9e0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/70ba9e01947f7d58acbb6aee4269a19d28345e5b)), closes [fsc-nlx#389](https://gitlab.com/commonground/fsc-nlx/issues/389)
* **controller:** add Dutch translations ([f7d3051](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f7d3051ee801be373e12f7c7b6f0f344b76c895e)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* **controller:** add favicon and detailed information to browser title ([b6e6b75](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b6e6b75207685d5d835382dafc32c927cdff2694)), closes [fsc-nlx#101](https://gitlab.com/commonground/fsc-nlx/issues/101)
* **controller:** add Incoming Connections page ([1886dae](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1886daeddecc124514608a55187a4b3bc1dca731)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **controller:** add New buttons in publications, outgoing connections ([7c9d682](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7c9d6825f10ff78f9e44bfd3427e387adf463948)), closes [fsc-nlx#388](https://gitlab.com/commonground/fsc-nlx/issues/388)
* **controller:** add outgoing connections page ([3b09ba4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3b09ba400d55adf9b3dd23da752f3b3d9ef54a40)), closes [fsc-nlx#366](https://gitlab.com/commonground/fsc-nlx/issues/366)
* **controller:** add pagination to action page ([d9f4018](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d9f401848c01160956ecc4d9f145b33d27d663c3)), closes [fsc-nlx#339](https://gitlab.com/commonground/fsc-nlx/issues/339)
* **controller:** add pagination to the Contracts page ([a1cb4b8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a1cb4b83269e39a739aa44752936c5a8f7bf7b73)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **controller:** add publications page to controller UI ([f9f55d0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f9f55d0661073de29834e31848e4828809c0ddf9)), closes [fsc-nlx#348](https://gitlab.com/commonground/fsc-nlx/issues/348)
* **controller:** add wizard to create a delegated service publication ([998d400](https://gitlab.com/commonground/nlx/fsc-nlx/commit/998d400bbc52cac8adef2541b4b046979befa433)), closes [fsc-nlx#393](https://gitlab.com/commonground/fsc-nlx/issues/393)
* **controller:** change default amount of contracts to show from 100 to 10 ([8e7e3b7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8e7e3b7cfff2ab0edfffe27faeaf39497ff7cdfe)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **controller:** connection wizards starts also from peers page ([cba70e2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cba70e2897f257af4501e3b4a22f8e1492907072)), closes [fsc-nlx#330](https://gitlab.com/commonground/fsc-nlx/issues/330)
* **controller:** display message 'no more contracts' when pagination ends ([9519c19](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9519c194b4cf2df4467776a2608b6723622dfe0e)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **controller:** display warning about usage for advanced users on the Contracts page ([2d9a2ba](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2d9a2ba68192bae8cf1f8a9cd97724b27fcdc824)), closes [fsc-nlx#371](https://gitlab.com/commonground/fsc-nlx/issues/371)
* **controller:** enable cursor based pagination for the Contracts page ([889a148](https://gitlab.com/commonground/nlx/fsc-nlx/commit/889a14868a296684d742f8c69c1dd2424766317e)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **controller:** enable scrolling to a specific Grant ([3cc7d17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3cc7d17b6e6da58d23f99c3110b74600aa2a2f41)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **controller:** improve translations for the Outgoing Connections ([ad94d19](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ad94d195464ceccea39859a3556eb3006084235a)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **controller:** improve validation messages for the Add Peer form ([ca54bb1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ca54bb10f7db97e67582c9a1cd24d89232c39d3a)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** improve validation messages for the Add Service form ([a484065](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a484065ed6dada9724f4b45717eb513fc435143d)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** improve validation messages for the Connect to Service form ([9e710bf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e710bf9005c490e871ed544a950afaefdacd517)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** improve validation messages for the Delegated Service connection form ([fa8b306](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fa8b3066bfecb2dde5a85105d8570bb65c386204)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** improve validation messages for the update Peer form ([ddeed61](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ddeed61f0143ce7e0c8403651f3921d69ce28645)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** log response body when retrying ContractDistribution fails ([0865d30](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0865d30d80400d9c5f7f64f807a967a5e0e52460)), closes [fsc-nlx#350](https://gitlab.com/commonground/fsc-nlx/issues/350)
* **controller:** navigate back to Incoming Connections when using the Contract Detail back button ([87d8f2b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/87d8f2b65d8eb3966e63eb20911a33afac1a2e29)), closes [fsc-nlx#399](https://gitlab.com/commonground/fsc-nlx/issues/399)
* **controller:** navigate back to previous page when using the Contract Detail back button ([56b8977](https://gitlab.com/commonground/nlx/fsc-nlx/commit/56b897772429823dbb84664c600f5b41b7fbfc3e)), closes [fsc-nlx#399](https://gitlab.com/commonground/fsc-nlx/issues/399)
* **controller:** prefill directory UI in publication grants ([3f0229a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3f0229a3eddebf5c6bd01dca6fa29f17fe86f652)), closes [fsc-nlx#78](https://gitlab.com/commonground/fsc-nlx/issues/78)
* **controller:** replace permission FscListContractsWithIncomingConnections with FscListContracts ([4172f5d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4172f5d54f6b272acb1359677e25dc57e4010c68)), closes [fsc-nlx#331](https://gitlab.com/commonground/fsc-nlx/issues/331)
* **controller:** replace query parameter with path param when toggling Outgoing Connections ([a00d95d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a00d95d1760e9bfc90b7ae9099852c51b24da73b)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **controller:** retrieve incoming connections per grant ([9e28e1b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e28e1b672d54f6591cd6e3089b199efc7a9f593)), closes [fsc-nlx#377](https://gitlab.com/commonground/fsc-nlx/issues/377)
* **controller:** reword remaining days active for the Contract status ([647cd7c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/647cd7ca1b7ab1adf2b2ae4024f572a19c485c31)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** reword validation messages for the forms ([4fa6771](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4fa6771d4773f3182b601fe0b9afbf3de6066aed)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** scroll relevant Grant into view when clicking Incoming Connection ([f0017c4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f0017c49e8a538d41ec0e0c0bffd800f0af59d9f)), closes [fsc-nlx#368](https://gitlab.com/commonground/fsc-nlx/issues/368)
* **controller:** scroll relevant Grant into view when clicking Publication ([dfe1dfa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dfe1dfaf7274f544923cc05215df1bc2c5dd43db)), closes [fsc-nlx#368](https://gitlab.com/commonground/fsc-nlx/issues/368)
* display contract states in controller UI on grants ([c5ecf74](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c5ecf74926de868a9724d4d2ec00cb69e02193b8)), closes [fsc-nlx#395](https://gitlab.com/commonground/fsc-nlx/issues/395)
* **docs:** use static compiled nginx from scratch container image ([fad90a6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fad90a6d57b5b105c5de048e8afe868e377d41c3)), closes [fsc-nlx#373](https://gitlab.com/commonground/fsc-nlx/issues/373)
* optimize container size and use scratch container for components ([3192578](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3192578aff0804eab75054a6d31b1a6e42828304)), closes [fsc-nlx#373](https://gitlab.com/commonground/fsc-nlx/issues/373)
* **outway:** add flag for enabling/disabling grant hash output ([5d900b3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5d900b37db5d475b2f15853b8aa8f09f115292b6)), closes [fsc-nlx#352](https://gitlab.com/commonground/fsc-nlx/issues/352)
* **outway:** implement fsc-transaction-id http header ([76b6333](https://gitlab.com/commonground/nlx/fsc-nlx/commit/76b6333a0939993fe41d9408f892061d1904ce3e)), closes [fsc-nlx#355](https://gitlab.com/commonground/fsc-nlx/issues/355)
* **outway:** log an error when proxy error handler is called ([c58820d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c58820d1ffdd8c021240d48c996adad927fcab97)), closes [fsc-nlx#341](https://gitlab.com/commonground/fsc-nlx/issues/341)
* **txlog:** add check on period filter ([0516058](https://gitlab.com/commonground/nlx/fsc-nlx/commit/051605848244265cb11d98cb4a7f8720fb830d79)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Bug Fixes

* controller shows count of total records ([0cd6b8d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0cd6b8d58a7a44d281eb043552e64d05ff3afac2)), closes [fsc-nlx#363](https://gitlab.com/commonground/fsc-nlx/issues/363)
* controller UI messages are formatted and translated ([e040270](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e040270db050fb6768811036e8871e406c48f3de)), closes [fsc-nlx#215](https://gitlab.com/commonground/fsc-nlx/issues/215)
* **controller:** actions page is the landing page of controller ui ([b35377f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b35377fdf9740ac518cd1bed1aa003f1d6976cab)), closes [fsc-nlx#394](https://gitlab.com/commonground/fsc-nlx/issues/394)
* **controller:** add contract state to contracts and service detail pages ([acba04c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/acba04c56ae4336d6ba5970872116c4338cccd44)), closes [fsc-nlx#395](https://gitlab.com/commonground/fsc-nlx/issues/395)
* **controller:** add missing translations ([0398883](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0398883a6a961daee5d0f06a9ee28bea0d40312f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** add translation for "Connections" ([1d530bb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1d530bb667fdd53f21475db878a2d97a5563dcd3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** added missing translations for actions and publications ([c8870f0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c8870f03e96393029e8f7ccc5eea09bac3da1174)), closes [fsc-nlx#385](https://gitlab.com/commonground/fsc-nlx/issues/385)
* **controller:** delegatedServiceConnection sends servicePeerID ([41acd7a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/41acd7a4695b3acda5a98b56d33e420cacff5af5)), closes [fsc-nlx#400](https://gitlab.com/commonground/fsc-nlx/issues/400)
* **controller:** disable contract action buttons when contract is ended ([fb5c79a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fb5c79a48577a75e0d0f532052196c141d5f2394)), closes [fsc-nlx#343](https://gitlab.com/commonground/fsc-nlx/issues/343)
* **controller:** do not display show more actions button ([1cd479c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1cd479c64579ea54b62f1295d5090ef3aee40481)), closes [fsc-nlx#390](https://gitlab.com/commonground/fsc-nlx/issues/390)
* **controller:** do not display show more publications button ([3b921a0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3b921a09ba74e5e4024eb37fb2279ed673d26d18)), closes [fsc-nlx#390](https://gitlab.com/commonground/fsc-nlx/issues/390)
* **controller:** do not show proposed contracts in publications archive tab ([70896a7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/70896a736809fea6e1519de8fce10dc7f0f194c7)), closes [fsc-nlx#378](https://gitlab.com/commonground/fsc-nlx/issues/378)
* **controller:** empty peers list after failed connect wizard ([d24754a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d24754a9587284d8d5b51890dcfca873195bfdb3)), closes [fsc-nlx#396](https://gitlab.com/commonground/fsc-nlx/issues/396)
* **controller:** ensure adding extra Outway Public Key Fingerprint does not trigger submit ([937260e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/937260e8e6414cb14bd044e1faa1321dfed59588)), closes [fsc-nlx#340](https://gitlab.com/commonground/fsc-nlx/issues/340)
* **controller:** ensure days until returns at least 1 day ([9e4af9d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e4af9d4bcabd45f98eab4dc38828dd4b4766418)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** fix empty peerID in service connection wizard ([ea4fc90](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ea4fc90c4fafea9bdbd53ebde48d0c5c861de5da)), closes [fsc-nlx#391](https://gitlab.com/commonground/fsc-nlx/issues/391)
* **controller:** fix title of service connection wizard ([d791623](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d791623542a640d6b36b0a4a2401a2d0a955c9c3)), closes [fsc-nlx#401](https://gitlab.com/commonground/fsc-nlx/issues/401)
* **controller:** fix translation connect to service from ([3ce531b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3ce531b663ac400db50a267f1f7d25cadb37da81)), closes [fsc-nlx#387](https://gitlab.com/commonground/fsc-nlx/issues/387)
* **controller:** peer details page menu highlights peers menu item ([5356c0f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5356c0f43c941ef06c65760fe4108bb404f29457)), closes [fsc-nlx#368](https://gitlab.com/commonground/fsc-nlx/issues/368)
* **controller:** publication wizard from publications page ([001686e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/001686e17d4567dbb4aaaffac9af48e7065072c9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** publications are displayed per grant ([3b7e872](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3b7e872be387236481f6afd214d45b4fc38fd734)), closes [fsc-nlx#375](https://gitlab.com/commonground/fsc-nlx/issues/375)
* **controller:** resolve validation issue for the Outway Public Key Fingerprint field ([74093e0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/74093e0ce6729dee8ed0c65087129fac95b8b77c)), closes [fsc-nlx#340](https://gitlab.com/commonground/fsc-nlx/issues/340)
* **controller:** use POST /services/publish if the service name is not known yet ([796eebd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/796eebdc32e4820f91aabc47a51944f4a313c0aa)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **manager:** added indices and fk to grant tables ([0b54b9e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0b54b9ecfa07569578d5f16e6b59711dc1c0e8e2)), closes [fsc-nlx#364](https://gitlab.com/commonground/fsc-nlx/issues/364)
* **manager:** ensure internal GET /contracts sorting is stable by explicitly passing the collation ([8604e9a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8604e9a53f259214905646582454bbdc1b169ebf)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **manager:** ensure internal GET /contracts sorting takes timezone into account ([a3764c9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a3764c9baa230949d919bfa992b336c1cc18f310)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **manager:** fix bug for not passing filter parameters to txlog api ([1fbd8be](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1fbd8be5168b3a4b83c66618954c8aa5e5e35b81)), closes [fsc-nlx#365](https://gitlab.com/commonground/fsc-nlx/issues/365)
* **manager:** rename consumer_peer_id to outway_peer_id in db view ([9f6ee06](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9f6ee06dcb89756bd25b5df6651206cb280273e4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** set default sorting of pending contracts to ascending ([d16fae1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d16fae12cfc306fd3635b1801aea6ff521f23b5f)), closes [fsc-nlx#339](https://gitlab.com/commonground/fsc-nlx/issues/339)
* **txlog:** always return transaction log records from DB in UTC ([0542d74](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0542d74cc9e050fab0eb7ace805efafcf57aa793)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)

### Code Refactoring

* **controller:** adjust manager interface to allow passing pagination options ([6b2da52](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6b2da52ea905ff036144db65b3331513266de1d5)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **controller:** allow passing extra arguments to translation method ([e074798](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e074798a1bde1d8e16d04a9b7a95bd84a22ae93c)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* **controller:** contract states determined on state from db ([0697bab](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0697bab2c57683871613cb2c746169cc742d9a64)), closes [fsc-nlx#409](https://gitlab.com/commonground/fsc-nlx/issues/409)
* **controller:** introduce x/text for i18n ([c9d6485](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9d6485fe60170b76df66f053f05a9dab39b36e3)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* **controller:** move generic locales from the implementation to interface ([b92869e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b92869ee66cebc2ae056d75a3edb43c31372be0f)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* **controller:** rename daysValid -> contractRemainingDaysValid ([1e97956](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1e97956fa67cc1f4fe14652f3003956973474df4)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** rename locales to improve consistency ([213ee9b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/213ee9b2df66384d6e9b9d0054c40cdebb19d96d)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* **controller:** reorder menu in the UI ([12b4983](https://gitlab.com/commonground/nlx/fsc-nlx/commit/12b498385c2bb399878675d70e6f238b15c5743d)), closes [fsc-nlx#317](https://gitlab.com/commonground/fsc-nlx/issues/317)
* **controller:** replace gotext with gotext-update-templates ([789b0c4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/789b0c475de120563e603af7acfcfb0fa0c8c14b)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* **controller:** replace schema package ([65e2fe2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/65e2fe26295ef4915bb9c04b4dd2622dd682997d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** reuse the common daysValid function where possible ([3af97f9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3af97f98310f90554c3739ecaf75cb02cef33119)), closes [fsc-nlx#402](https://gitlab.com/commonground/fsc-nlx/issues/402)
* **controller:** rework the ListOutgoingConnections DB handler ([17232ce](https://gitlab.com/commonground/nlx/fsc-nlx/commit/17232cec397014ed3e63e67ee47e7ff1da1fe723)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **controller:** simplify Outgoing Connections page ([7602400](https://gitlab.com/commonground/nlx/fsc-nlx/commit/760240099bc5d84ecf04980c55bd7b4bef1b9d0a)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **controller:** simplify Outgoing Connections template logic ([2737174](https://gitlab.com/commonground/nlx/fsc-nlx/commit/27371747472aba39c5b1096393804ce68006ef9f)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **controller:** use BCP-47 language tags to comply with x/text from Golang ([b117548](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b117548d5c189d8b21bd375ce5b8018d680dc379)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* **controller:** use customized fork of gotext-update-templates ([f4520ca](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f4520ca253e3c0d40942ad949eb1a5373c4b8d9a)), closes [fsc-nlx#245](https://gitlab.com/commonground/fsc-nlx/issues/245)
* **controller:** use interface as value for translation keys ([ed98171](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ed98171453b19df8c3b0ec2aea0c2b0ff312e43a)), closes [fsc-nlx#345](https://gitlab.com/commonground/fsc-nlx/issues/345)
* **controller:** use same status element for contract status ([3082d78](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3082d78618207a23eabaff81881fd5aa01e992a0)), closes [fsc-nlx#372](https://gitlab.com/commonground/fsc-nlx/issues/372)
* **manager:** extract PeerInfo struct into a separate file ([98da8a2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/98da8a29f87907a953544a3559254c150f31ef9e)), closes [fsc-nlx#329](https://gitlab.com/commonground/fsc-nlx/issues/329)
* **manager:** extract setting up PostgreSQL database in a separate function ([e4d0c10](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e4d0c1099d3eb1a018c4a14d66d10c9711d14767)), closes [fsc-nlx#329](https://gitlab.com/commonground/fsc-nlx/issues/329)
* **manager:** improve error logging for the PostgreSQL setup function ([9a2bfe1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9a2bfe1bd3fbd0c6be10668389e870e4eb7ec0a8)), closes [fsc-nlx#329](https://gitlab.com/commonground/fsc-nlx/issues/329)
* **manager:** improve test setup for customizing the Manager factory behaviour ([444ca9d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/444ca9d24a5cd4ee724f4783733d663458e4d933)), closes [fsc-nlx#329](https://gitlab.com/commonground/fsc-nlx/issues/329)
* **manager:** list publications cursor is parsed in port ([bbb7516](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bbb75167c6083bc7cda159be531a18342258d708)), closes [fsc-nlx#375](https://gitlab.com/commonground/fsc-nlx/issues/375)
* **manager:** rename column consumer_peer_id to outway_peer_id ([d5604c3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d5604c3f128d96045c4b8b9a532b04fdb73399b3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** rename test files to group related components ([2b3bea9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2b3bea926bb8fc425e10703651f34b357b997c4b)), closes [fsc-nlx#329](https://gitlab.com/commonground/fsc-nlx/issues/329)
* **manager:** rework GET /outgoing-connections to enable filter by contract state ([a8f894e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a8f894e7b056149e432c2e27aa3a0c6107ae04c7)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **manager:** rework ListContracts query to return domain objects instead of custom struct ([9de237f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9de237fa92d07a6f7b6af0c24f097daec9bd0576)), closes [fsc-nlx#331](https://gitlab.com/commonground/fsc-nlx/issues/331)
* **manager:** simplify the Outgoing Connections endpoint with count test ([5c2a302](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5c2a302bbf452ea198df1b545d38c66724bed664)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **manager:** use variable for the Peer CertBundle of the SUT ([2f48e47](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2f48e47b027290a54c836f17b76a6890ccfe73e1)), closes [fsc-nlx#329](https://gitlab.com/commonground/fsc-nlx/issues/329)
* **outway:** improve log message ([ad4a4cf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ad4a4cfd38045bc0f2bdeb2380136509c53c87fc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Styles

* **controller:** fix order of imports ([87043cd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/87043cd46c35bfdffc613ad6633b95ab01518eab)), closes [fsc-nlx#340](https://gitlab.com/commonground/fsc-nlx/issues/340)

### Tests

* add e2e test for deleting service in controller UI ([31b5873](https://gitlab.com/commonground/nlx/fsc-nlx/commit/31b5873d9d30f98215a0284d674f4336748ff6c3)), closes [fsc-nlx#354](https://gitlab.com/commonground/fsc-nlx/issues/354)
* add e2e test for incoming requests page in controller UI ([11cd6e7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/11cd6e74ee2afa6333f62998470256a37a1fd948)), closes [fsc-nlx#356](https://gitlab.com/commonground/fsc-nlx/issues/356)
* add e2e test for parkeerrechten ([830e530](https://gitlab.com/commonground/nlx/fsc-nlx/commit/830e53088255689546de9ef57b5b9800c814d399)), closes [fsc-nlx#336](https://gitlab.com/commonground/fsc-nlx/issues/336)
* add e2e test for publication page ([f2d3b65](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f2d3b658818ae4801fea2ee7079da7af810767fb)), closes [fsc-nlx#348](https://gitlab.com/commonground/fsc-nlx/issues/348)
* add e2e test setup for controller UI ([8c519ae](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8c519aed6294e6e1ed8ab55afdf8e0cdd49f84ec)), closes [fsc-nlx#354](https://gitlab.com/commonground/fsc-nlx/issues/354)
* add status code check in e2e test ([34dcb7d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/34dcb7d8e982cfd59b207a4adfd65214b11afc9e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** refactor the e2e test for the publication page ([b704248](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b70424872842fba35d03ec92e237afbba02db9d0)), closes [fsc-nlx#368](https://gitlab.com/commonground/fsc-nlx/issues/368)
* fix e2e tests being flaky and random test ID entropy ([892e6ea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/892e6ead25835c37229862b435085f529c86c170)), closes [fsc-nlx#335](https://gitlab.com/commonground/fsc-nlx/issues/335)
* make browser and backoff timeout of E2E test configurable ([85d864f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/85d864f8f255ea054165ff98fdf6269e07652196)), closes [fsc-nlx#368](https://gitlab.com/commonground/fsc-nlx/issues/368)
* **manager:** add component test for ListPendingContracts ([e140140](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e140140a1110cda40aec74db7ad6458ecc450671)), closes [fsc-nlx#339](https://gitlab.com/commonground/fsc-nlx/issues/339)
* **manager:** add test for filtering Outgoing Connections by Contract state ([25978f1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/25978f1da72d533c50f2ac6a290b559144a79f2e)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **manager:** also take filter into account when listing contracts ([e8df582](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e8df58219fac0b20410793b9744c1cb1af08b156)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **manager:** expand test for internal GET /contracts to validate the next cursor value ([c9d4f8e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9d4f8ebca3dd757bc033905579523ffd251c624)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **manager:** re-enable accidentally commented out tests ([bbc4c47](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bbc4c470d258a6fb5b4c43ccd0506ceb65ca0cc4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** remove redundant GetToken implementation ([5d7f4e4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5d7f4e44fa5761bc8fcebee41fb79091d53538a4)), closes [fsc-nlx#329](https://gitlab.com/commonground/fsc-nlx/issues/329)
* **manager:** remove redundant properties from the manager factories ([ffdc075](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ffdc0755846068943da4c0ddb63084a46f544a75)), closes [fsc-nlx#329](https://gitlab.com/commonground/fsc-nlx/issues/329)
* **manager:** rename get -> list for the Contracts handler ([c1963fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c1963fcb1574ba1fd4278d05c590370ce0888616)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **manager:** rename tests so we don't need a comment explaining them ([194a651](https://gitlab.com/commonground/nlx/fsc-nlx/commit/194a6517e3f7bb297f78fb9f26981d38240d6a07)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **manager:** rework test for Outgoing Connections endpoint ([b404c0e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b404c0e297b8dd31e443ae443419db4ac3fc8ad1)), closes [fsc-nlx#407](https://gitlab.com/commonground/fsc-nlx/issues/407)
* **manager:** rework test functions so they can be combined into a single function ([47e4fde](https://gitlab.com/commonground/nlx/fsc-nlx/commit/47e4fde8fc31993cda6f91609406df788ce743d0)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* **manager:** simplify and reorganize internal GET /contracts tests ([90a548a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/90a548a7b80ff8211c94f6a511d26eb7f15a2ace)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* use default PostgreSQL DSN with SSL disabled ([a2f1619](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a2f1619a10de898d88e95e272257b91f5ba9ec1d)), closes [fsc-nlx#319](https://gitlab.com/commonground/fsc-nlx/issues/319)
* use logging to golang test interface and use asserts for errors ([00bfe5b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/00bfe5bb12679e1565ff0c5a2428f1501c31f66d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [0.23.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.23.0...v0.23.1) (2024-06-06)

### Continuous Integration

* disable the ingresses for the controller/manager APIs ([f4cec8a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f4cec8a6e087f165f5cc2a0361350e3fca82348a)), closes [fsc-nlx#333](https://gitlab.com/commonground/fsc-nlx/issues/333)

## [0.23.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.22.0...v0.23.0) (2024-06-06)

### Build System

* pin dependency conventional-changelog-conventionalcommits to 8.0.0 ([8c68a8d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8c68a8d341994b4779cda95ad1a59d89c4912ff9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency prettier to v3.3.1 ([cb4b98a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cb4b98aefbe7edf9339f57b811b6147559abf294)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.22.4 ([21e1c11](https://gitlab.com/commonground/nlx/fsc-nlx/commit/21e1c1185e5c2e7285e2da973045ed123bb830e8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.24.0 ([bd5c400](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bd5c400e3b682ce4796b6f3f706dcf6df135d20c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.26.0 ([128caec](https://gitlab.com/commonground/nlx/fsc-nlx/commit/128caec5e994eb453e24f7eaef35ee30ba97015a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/oauth2 to v0.21.0 ([44853a1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/44853a19d49e2293166a54057af4f278d799ec7f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 66c7d98 ([f648487](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f648487d4ebf015ac8e322cab2fac94ddb3ed3bf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 8dec302 ([99d9f6a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/99d9f6a49541fab7a7a4d01955b00e5b4354b389)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.3-alpine docker digest to 7b48f84 ([7537e04](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7537e048ad4ab65898a6449f1f31f99949b29f1b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.3-alpine docker digest to d037653 ([46ff06a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/46ff06a3b1e44fe69cc2a97234f19e372b470ae0)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Continuous Integration

* replace zalando/postgres-operator with cloudnative-pg ([5817495](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5817495246b27fe81cf9322b2f63952adc2c743e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Features

* **manager:** ensure listing Peers does not error when the Directory is unavailable ([acc586d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/acc586dbd767c16f19241ed27633c3561501bec6)), closes [fsc-nlx#323](https://gitlab.com/commonground/fsc-nlx/issues/323)

### Bug Fixes

* **controller:** prevent passing an empty Peer ID when retrieving Peers from the Manager ([b7d5849](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7d58494427d14b7b0f1dab02c1da04eafe9deb0)), closes [fsc-nlx#323](https://gitlab.com/commonground/fsc-nlx/issues/323)

### Code Refactoring

* **manager:** rename PeerID filter to PeerIDs ([2cf9c22](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2cf9c225b1fe57f4845c9d116e61f1e9f65b0069)), closes [fsc-nlx#323](https://gitlab.com/commonground/fsc-nlx/issues/323)

### Styles

* **manager:** fix linting by removing redundant return parameter ([5313f18](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5313f18988187774427c8f93dcded38bb235cfcd)), closes [fsc-nlx#323](https://gitlab.com/commonground/fsc-nlx/issues/323)

### Tests

* add Integration-ConsumeDelegatedService-1 e2e test ([dbb51bd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dbb51bd15e5999bbb5ae22b83313cd70cd773c68)), closes [fsc-nlx#316](https://gitlab.com/commonground/fsc-nlx/issues/316)
* add Integration-ConsumeDelegatedService-2 e2e test ([b36645d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b36645ddcdf7d89e180b91aa5faafa1708dfacfd)), closes [fsc-nlx#324](https://gitlab.com/commonground/fsc-nlx/issues/324)
* add Integration-TransactionLog-1 e2e test ([6810567](https://gitlab.com/commonground/nlx/fsc-nlx/commit/681056706a07c7ab53e79ccc0444725ca33d41ba)), closes [fsc-nlx#325](https://gitlab.com/commonground/fsc-nlx/issues/325)
* **manager:** add test to verify PeerID filter of internal GET /peers ([d93e6f9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d93e6f9ff9800661a0bf67d136d60c60b2945932)), closes [fsc-nlx#323](https://gitlab.com/commonground/fsc-nlx/issues/323)

## [0.22.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.21.0...v0.22.0) (2024-06-03)

### Build System

* add podman compose to start development script ([c3e6f9c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c3e6f9cc249e722ab244180fb9d1528e9370773a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** regenerate code from authz oas ([073365e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/073365e090c549fec019e97e889e1c0dee97a49b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* fix Semantic Release job ([a7507c8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a7507c80e3b8423c5e3816a0c73d9f89959743d5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* replace deprecated GolangCI linters with alternatives ([30242a7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/30242a76871d52c916f1f0377a4b39eec5da0df2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update alpine docker tag to v3.20.0 ([93f796c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/93f796cfc69cf4e4cbc2e980cdf5b9b6afe40ce7)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update chromedp/headless-shell docker tag to v124.0.6367.155 ([a5f34e3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a5f34e33698e0db1d767916d11035fb331ad00a9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update chromedp/headless-shell docker tag to v126 ([5b16cca](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5b16ccac46945d74dcb18eb8c8571e515f6e1812)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @semantic-release/gitlab to v13.1.0 ([7df0c51](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7df0c51e006f915dd1ea3d06459fcb382f0ca270)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency glob to v10.3.14 ([1340d70](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1340d704380777b6987b0512e6431235d93ae6f9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency glob to v10.3.15 ([d37b12a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d37b12aa56afa81f606c0fdc05134c253d5e21f5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency glob to v10.3.16 ([e2917a6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e2917a6bfd95e8ab4c18e1e3b56ec60cbecb4406)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency glob to v10.4.1 ([17c1205](https://gitlab.com/commonground/nlx/fsc-nlx/commit/17c1205aacb22cca5b88e67642c8fe0735e78b78)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.15.0 ([9e26376](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e2637648c2a83707c4f0ab91bf1c24ec21e23f9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.15.1 ([ab24d90](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ab24d902183505940e08c0aca4cc5262c54ed671)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency prettier to v3.3.0 ([a7d59a6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a7d59a6d7ebb056374edcafe679fb58252606284)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency replace-in-file to v7.2.0 ([76c4c20](https://gitlab.com/commonground/nlx/fsc-nlx/commit/76c4c20d517acdfbc73f7ecf29dd64a2a9866c8a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23.1.1 ([55e4ac0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/55e4ac0e9d2a5dfe350fa290f2d622be57c92be4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v24 ([f6ec6c9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f6ec6c9778aabb79ce55edda199688fe5b43473e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docker.io/curlimages/curl docker tag to v8.8.0 ([14c299a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/14c299a40bfc20229ad19efd2897bc242f3e9a31)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docusaurus monorepo to v3.4.0 ([287bfff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/287bfff9785516824541d97fb7da79afb9a823b5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update gcr.io/kaniko-project/executor docker tag to v1.23.0 ([0414bf3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0414bf3a7034b63fbc4276f869a342dd4cff752e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.22.3 ([0b16490](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0b164901fad91e2e38faefdf86b24076955eee1a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.22.3 ([b042310](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b042310ad9eac7813ed5fbcc84aede193062a061)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.3-alpine docker digest to 421bc7f ([fd24b4d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fd24b4d0e96d5577d34017c4825f72f5565f0ab2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.3-alpine docker digest to 7e78833 ([6eebf28](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6eebf28626622b09703e1077390d4e7f4ea5ad1a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.3-alpine docker digest to 809aaea ([d73cd95](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d73cd9559f1710322f59d2da263096790cb93189)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.3-alpine docker digest to b8ded51 ([049510f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/049510f963f3efa160f5dcfb21d49fe03daf936c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.3-alpine docker digest to f1fe698 ([853f458](https://gitlab.com/commonground/nlx/fsc-nlx/commit/853f458d5b8c145b5aed44af5cbcd9a71a7fb08a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/data-dog/go-txdb to v0.1.9 ([ad7bd33](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ad7bd33c017fd89434e311feee70f7cecc8941ff)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/jackc/pgx/v5 to v5.6.0 ([277c637](https://gitlab.com/commonground/nlx/fsc-nlx/commit/277c6373da436dcabd19885730064ab66fbeb83f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 3bb8914 ([cf310c4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cf310c468d6d33e8d86219fffc9405f23f566d82)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 49344ed ([086d05f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/086d05fa0e560ab5ecf0dbd5764ffb99864e4219)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 928b24a ([e88f4e3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e88f4e375a2174f775f05794c07e6c15d8ba2375)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to a7b980c ([7ba529d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7ba529dc105a09fe6439fcd3bda2b877725f9a34)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.13.0 ([39935e3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/39935e317e73374aa2a7f669ae161991b502874f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.13.1 ([cf132d9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cf132d938182c4aa4b4593ff6beae7f76b005d03)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.14.0 ([00c7c5c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/00c7c5c13b2804bd370d0a98a1a6a2b80f0ee4ac)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.65.0 ([7a05d47](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7a05d47941638c71b88082e1ea1ebc1bcd965f4f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres docker tag to v16.3 ([4e56fa1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4e56fa170e0680d44ffff4511f2bc93c48745aa2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.3-alpine docker digest to e89da2c ([3574b77](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3574b77d6a8766139b715dfe4783b858814b17e8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* upgrade golangci-lint v1.57.2 to v1.58.1 ([2a449eb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2a449ebd6b6c86fbf384c13e731603ff17c7427e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Continuous Integration

* add controller and manager ingress for vergunningsoftware BV ([ab87497](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ab87497214179a08d81b158a2cce03ebee8bae6c)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* add e2e tests to linter job ([8070d55](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8070d558241a581a360ecd69309cfab737da127b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* add html exporter for e2e tests ([5331f91](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5331f9190c9c37c5c43da1dbc1f53f5a6bec48cd)), closes [fsc-nlx#307](https://gitlab.com/commonground/fsc-nlx/issues/307)
* add success check to on failure rule ([455e1bc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/455e1bcdf863e5b6033fd16dac928380dc2f1de4)), closes [fsc-nlx#302](https://gitlab.com/commonground/fsc-nlx/issues/302)
* deploy review environment after image build ([5450393](https://gitlab.com/commonground/nlx/fsc-nlx/commit/54503938e4332676c8f6db8f32d5b22907f4c2d6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* fix format of Slack notification failed pipeline ([a8cb94e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a8cb94e109110ae0c22452602629951ebc0e21bb)), closes [fsc-nlx#302](https://gitlab.com/commonground/fsc-nlx/issues/302)
* send Slack notifications directly from pipeline ([ab187bb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ab187bbf9d4bc0d7a53528f5106d37c2f569b087)), closes [fsc-nlx#302](https://gitlab.com/commonground/fsc-nlx/issues/302)
* use go cache in the go test stage ([323ac7a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/323ac7a0d6e4a5568cc9cea8837926c9af764dc7)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Documentation

* remove production directory from docs ([e7b684a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e7b684af711b0874be8e2d7334cf7db1a10849bb)), closes [fsc-nlx#308](https://gitlab.com/commonground/fsc-nlx/issues/308)

### Features

* **controller:** add Peer detail page + update Peer form ([5d0442f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5d0442fbe636b243cba41e2330ec8f5d9346af82)), closes [fsc-nlx#300](https://gitlab.com/commonground/fsc-nlx/issues/300)
* **controller:** add Peers overview to the user interface ([92c8669](https://gitlab.com/commonground/nlx/fsc-nlx/commit/92c86694b25d419a1f2166b56bc73cfbe55e61fd)), closes [fsc-nlx#300](https://gitlab.com/commonground/fsc-nlx/issues/300)
* **controller:** ensure Peer names are shown when working with Contracts ([e5555cb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e5555cb7fe94056463633809045d2486fda55247)), closes [fsc-nlx#304](https://gitlab.com/commonground/fsc-nlx/issues/304)
* **controller:** ensure the Directory page renders if the Directory is unreachable ([45c83ff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/45c83ffc27b6626446b8bac2786d5bd61f1ba96a)), closes [fsc-nlx#300](https://gitlab.com/commonground/fsc-nlx/issues/300)
* **controller:** only fetch the relevant Peer info instead of all Peers ([25936bc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/25936bcd2d1365fafcbe3db5add75ae4dbd106bc)), closes [fsc-nlx#312](https://gitlab.com/commonground/fsc-nlx/issues/312)
* enable adding new Peer via the Controller UI ([c51e083](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c51e08346e67507c925ee6c518f5005a38062e19)), closes [fsc-nlx#300](https://gitlab.com/commonground/fsc-nlx/issues/300)
* introduce e2e tests in ci ([ba10d9c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ba10d9c550365303ce22008abd74d813af274add)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* **manager:** allow contract creation when the directory is down ([e41f801](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e41f80199cb7d6a0d1124908b563fff31116c228)), closes [fsc-nlx#300](https://gitlab.com/commonground/fsc-nlx/issues/300)
* **manager:** ensure ListPeers and ListPeersByName return ordered list ([ee48a00](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ee48a00a9272fcd97d93d4d0d99ab1ae7a8fc784)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** ensure the manager will start when the announce to the directory fails ([c05ade4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c05ade40513b3a289b27a01d42b12035e4a3fd75)), closes [fsc-nlx#300](https://gitlab.com/commonground/fsc-nlx/issues/300)
* **manager:** include local Peer information in the internal Manager GET /peer handler ([05bcf6e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/05bcf6e9566a2691e42ca83b03607693d890dc1a)), closes [fsc-nlx#300](https://gitlab.com/commonground/fsc-nlx/issues/300)

### Bug Fixes

* add certificates for e2e test ingress controllers ([02a460c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/02a460c17459081948828e1d37c3e7cc7769d0ed)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* add certificates for e2e test ingress gemeente stijns ([45e5e13](https://gitlab.com/commonground/nlx/fsc-nlx/commit/45e5e1302bcf7a1550278cdde6402418305ba5d2)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* **controller:** access to service button visible with no contracts ([128a241](https://gitlab.com/commonground/nlx/fsc-nlx/commit/128a241883089a8a4bf0f93601b19347a6d6de3a)), closes [fsc-nlx#299](https://gitlab.com/commonground/fsc-nlx/issues/299)
* **controller:** adjust incorrect classname ([9b10b12](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9b10b124f2060fd88353031707b69b1f39bf3e46)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* dns names gemeente stijns acc too long ([c50fd16](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c50fd16647575cfd6e7cf7a69a82637c0a83fe9a)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* e2e test did not wait for accept signature to be processed ([8338e8e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8338e8e2bff4d94d62951c56cdcd0f1dd530d9ac)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* endpoint acc internal manager gemeente stijns e2e tests ([1ca9429](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1ca9429495365a2da156d00171b39c0242f80b7c)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* **helm:** add values for ingress on demo and acc ([8acbd86](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8acbd86ea75f97cc2f691f1aac768c774cd426a3)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* **manager:** all Peers of a Contract should be returned for the internal ListContracts ([ec271df](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ec271dff0d8a3539dad5b10e07be47cd7ada2df6)), closes [fsc-nlx#300](https://gitlab.com/commonground/fsc-nlx/issues/300)
* **outway:** prevent the usage of expired access tokens ([7c0ccc6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7c0ccc6bbcc8e06b39ab30edac9db656109b58fd)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **outway:** use correct short description serve command ([03ef91a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/03ef91a65824cb371c0b115e447b06c2494173e1)), closes [fsc-nlx#31](https://gitlab.com/commonground/fsc-nlx/issues/31)
* rename controller ports in Earthfile for OAS generation ([eb164a5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eb164a5b7320101d0d732d39ebbc4f99c2b54528)), closes [fsc-nlx#301](https://gitlab.com/commonground/fsc-nlx/issues/301)
* set group_id in acceptance e2e tests ([fbc720d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fbc720d53b52ada1a38f2fc6db15f0590c161625)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* use correct endpoints on acceptance for e2e tests ([9e252f3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e252f33d89698d9ff54e3c97f6f088a2655be8d)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* use correct namespace in e2e tests on acceptance ([f0d7428](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f0d742854a760fc30f17db4240913689eb62aba1)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)

### Code Refactoring

* **controller:** reorganized controller ports ([312d5a4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/312d5a41f51183e3a74075e432e62941a58c680a)), closes [fsc-nlx#301](https://gitlab.com/commonground/fsc-nlx/issues/301)
* **inway:** removed unused CA parameter in authorization plugin ([77f2d4d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/77f2d4df55ba6302e2b9173a4b9a268a1a19d52d)), closes [fsc-nlx#11](https://gitlab.com/commonground/fsc-nlx/issues/11)
* **manager:** improve readability of internal ListContracts handler ([9558bb8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9558bb8414affedab9194efb3b68763d78e80499)), closes [fsc-nlx#300](https://gitlab.com/commonground/fsc-nlx/issues/300)
* **manager:** mention correct repository in error messages ([6cd5be7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6cd5be799f6e17a962b11e7bf42a478a7b067cab)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **outway:** add tests as different package ([32faf33](https://gitlab.com/commonground/nlx/fsc-nlx/commit/32faf33b92bbc360a19704034b3f4fbc62ea994e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* remove peer name and manager address from contracts ([2866b5c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2866b5c336311a48ef6c98c2304f5b3496647310)), closes [fsc-nlx#304](https://gitlab.com/commonground/fsc-nlx/issues/304)

### Styles

* **manager:** remove redundant newline ([4a5c6f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4a5c6f63a0d6364e13f5f1237f4933317cda9a7c)), closes [fsc-nlx#104](https://gitlab.com/commonground/fsc-nlx/issues/104)
* **outway:** remove redundant newline ([300755b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/300755b7ff6966e219853e00728f7391072fa744)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **outway:** update copyright header to be more compact ([e276685](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e27668567fe8e3ceb0309a1487415f57a8bbdbf5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

### Tests

* add e2e test for Integration-ConsumeServiceAsDelegatee-1 ([f10d337](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f10d337492fd88debf7916189044ab973c3586d7)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* add e2e test for Integration-ConsumeServiceAsDelegatee-1 ([4c84e86](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4c84e86b651528a99f858d103705743a1ff83b19)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* add e2e test Integration-ConsumeService-2 ([6ac59d0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6ac59d0762c510a9eecfa80bc1aa6c538625a80f)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* add go rod as chrome driver ([ed108c8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ed108c8d36d4ab518c2323167468fa4fd49451e2))
* add screenshot after e2e test fail and some handy functions ([706ab77](https://gitlab.com/commonground/nlx/fsc-nlx/commit/706ab771352b647d6dccc0610268c6d3c3f45c3b)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* **controller:** add missing tests for the internal REST API ([b4e45d1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b4e45d1b55c23ac7c8afed0b35c225172e3ac778)), closes [fsc-nlx#104](https://gitlab.com/commonground/fsc-nlx/issues/104)
* **controller:** add tests for the internal REST API ([e9c513e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e9c513e3eddd8ced8e7b67e4ca0a8d7c22369583)), closes [fsc-nlx#104](https://gitlab.com/commonground/fsc-nlx/issues/104)
* fix incorrect hostname for Vergunningsoftware in E2E tests ([558c90b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/558c90bc76359cc6e031c274fd5d2eefca9ffe51)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* greate given step for verifying accepted contracts ([b7ea3d7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7ea3d75a13d7e90937ba1f1673f34bd60f2bdff)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* **manager:** add test for the internal GetFailedDistributions endpoint ([f62f8f7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f62f8f7c8f66b2a7883974288edf52d831433264)), closes [fsc-nlx#104](https://gitlab.com/commonground/fsc-nlx/issues/104)
* **manager:** add test for the internal GetLogs and GetOutwayServices endpoint ([937a3c7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/937a3c794afd2bbbfb3be80bd7cf78de73762541)), closes [fsc-nlx#104](https://gitlab.com/commonground/fsc-nlx/issues/104)
* **manager:** add test for the internal GetPeerInfo and GetServices endpoint ([288abe9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/288abe92419f75c59dc183b619ff1e6254a84673)), closes [fsc-nlx#104](https://gitlab.com/commonground/fsc-nlx/issues/104)
* **manager:** rename Submit Contract -> Create Contract for consistency ([30a12ad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/30a12adc99a1f747d0da23e7e9b56d297214dfbf)), closes [fsc-nlx#104](https://gitlab.com/commonground/fsc-nlx/issues/104)
* **outway:** add test to ensure the Outway does not reuse expired tokens ([67386dd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/67386dd1c857db1e8452e2c140071bf6810bb98c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **outway:** extract test setup for Outway to its own package ([c8bf609](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c8bf6099031b0f7b30901643e7a8c950634ba1af)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **outway:** remove unused newFakeEmptyRepository ([dc5aa60](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dc5aa60c6806cf3f320321628d48bb4f0c9fb409)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **outway:** remove unused nil check ([eaf9e76](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eaf9e761ec918bcb2dacfdde5ed51cdbaa27a943)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* perform e2e tests on acceptance ([a2531b0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a2531b0f2e5603585e22fcbe9917a7e4ed4a01db)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)
* rework E2E tests to use PeerID instead of Peer name from contracts ([42c0576](https://gitlab.com/commonground/nlx/fsc-nlx/commit/42c05767252060f93fa30be6f8093bb1e8f281fd)), closes [fsc-nlx#304](https://gitlab.com/commonground/fsc-nlx/issues/304)
* shorten common name of Manager and Controller on ACC ([91ee6bd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/91ee6bd4b87676a735bdad1b322b733820e8ea27)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* use godog logger instead of slog for logging debug messages ([074c12f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/074c12fb6959bd863a217bd280790d6ab3eebbed)), closes [fsc-nlx#285](https://gitlab.com/commonground/fsc-nlx/issues/285)

## [0.21.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.20.0...v0.21.0) (2024-05-07)


### Build System

* changed eartly base image repository ([66788d9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/66788d9ad77e6b146f7c7a098e845bfd55fd4aec)), closes [fsc-nlx#283](https://gitlab.com/commonground/fsc-nlx/issues/283)
* remove nlx- prefix from paths ([c3bf69e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c3bf69e5fc81258fa5132ed043cdf6e1d2076dfe)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @commitlint/cli to v19.3.0 ([b94af72](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b94af72dbaba9b57384eed1b43d20b305578b984)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @semantic-release/gitlab to v13.0.4 ([0d11955](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0d1195527f935cdeb1e09eab67a4ca6fc8e9b526)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency dayjs to v1.11.11 ([d6b2098](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d6b209834f87451057d3d7c3a12e19077e2ddb76)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docusaurus monorepo to v3.3.2 ([42f6fd3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/42f6fd3be7d6ecc9f45db5a2c18556d1b7df6ed3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.2.10 ([1f8393e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f8393e9b9f1ab7e2df5253944bae0f3a6039266)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.3.0 ([f5b1506](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f5b1506a6d2af6dc9fb2fcf9653e3b02b2d0c602)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.4.0 ([f379a93](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f379a938f82decb839b4eceeda53ce41b9ae6ac0)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.4.1 ([840f408](https://gitlab.com/commonground/nlx/fsc-nlx/commit/840f408df7907c095da0a9dbb1b3ccae439067b7)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.5.0 ([0ef2dbc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0ef2dbc187eeddb6930ef7c8511de7407fbd7b7c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/jmoiron/sqlx to v1.4.0 ([d89c28a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d89c28a43653354c18dd842bc56c02241b249502)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.23.0 ([c7686ad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c7686ad97172a80e05e4c4358a6561951afe8799)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.25.0 ([b14e63b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b14e63be2ccfd4950110aaa862df820e990efa09)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/oauth2 to v0.20.0 ([97d62db](https://gitlab.com/commonground/nlx/fsc-nlx/commit/97d62db1e9d6655bfe39fad3bfe5190cbc70a339)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 7a91aa3 ([6728191](https://gitlab.com/commonground/nlx/fsc-nlx/commit/672819189e2c09999c5f28d9919e9553dad50a4d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.64.0 ([0e27e40](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0e27e40b0c1c6bb169b5bd5ec5f64229dd549518)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.64.1 ([ff7008f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ff7008f714e42271f72c0cb3b099e588fb4b2b86)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update react monorepo to v18.3.0 ([dc3502e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dc3502e19d0e71396fe19726dd46470ece5fe75b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update react monorepo to v18.3.1 ([fe2ded1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fe2ded1cd8f528af9fec54ee936ce8202dfa33fc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* upgrade to go 1.22.2 ([06c928b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/06c928bc02a49bed3b008f85f44bace58577397b)), closes [fsc-nlx#294](https://gitlab.com/commonground/fsc-nlx/issues/294)


### Continuous Integration

* disable failing jobs ([51f6c55](https://gitlab.com/commonground/nlx/fsc-nlx/commit/51f6c55b22a7363bc041915911b3ac7cea4b5257)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update default database name of the txlog-api chart ([48ffea8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/48ffea86531805a7f724537401f29b231a04b85f)), closes [fsc-nlx#14](https://gitlab.com/commonground/fsc-nlx/issues/14)


### Documentation

* add auditlog to the component overview ([a7bb18f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a7bb18f71f81c5a97a6d6a85fc68b0252f784ee7)), closes [fsc-nlx#281](https://gitlab.com/commonground/fsc-nlx/issues/281)
* remove outdated documentation about the NLX environments ([5bc0cf8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5bc0cf8c5fee03471407320685864ff242fbb8b3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Features

* **controller:** add oidc logout url ([31a5645](https://gitlab.com/commonground/nlx/fsc-nlx/commit/31a564590bfcda55230010bf6190fa006c3608b2)), closes [fsc-nlx#269](https://gitlab.com/commonground/fsc-nlx/issues/269)
* **controller:** disable Submit button on submit for the Connect and Publish Wizard ([c673aa7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c673aa76b2555c6d548cdb17eee64fa1c7a154f6)), closes [fsc-nlx#278](https://gitlab.com/commonground/fsc-nlx/issues/278)
* **controller:** enable connecting to a Service for a Delegator using the Wizard ([41901d5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/41901d55e0a7a0994665e392a4ef26ac44e3256c)), closes [fsc-nlx#278](https://gitlab.com/commonground/fsc-nlx/issues/278)
* **controller:** enable publishing a Service for a Delegator using the Wizard ([81dd7b0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/81dd7b0ab3cb857b37eee6b19f7bdd66cfe39c96)), closes [fsc-nlx#277](https://gitlab.com/commonground/fsc-nlx/issues/277)
* **controller:** improve title of Service connection Wizard ([4e8dda1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4e8dda13f9f874b71ddd9f67575e4680fdc5d103)), closes [fsc-nlx#278](https://gitlab.com/commonground/fsc-nlx/issues/278)
* **controller:** prefill Outway Public Key Thumbprint when Connecting ([b9a9666](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b9a966679f814714d8a3cec7074b346cde3a7cd6)), closes [fsc-nlx#278](https://gitlab.com/commonground/fsc-nlx/issues/278)
* **manager:** log announce to directory errors ([7260d7f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7260d7f833dcecdadf17f929b8a36ffc932a63ba)), closes [fsc-nlx#259](https://gitlab.com/commonground/fsc-nlx/issues/259)


### Bug Fixes

* **auditlog:** use postgres connection pool instead of connection ([5cb6e45](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5cb6e45e4d6fbca38ba207e4ede830520d08ca35)), closes [fsc-nlx#279](https://gitlab.com/commonground/fsc-nlx/issues/279)
* **ca-certportal:** add logging for debugging ([b94de0a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b94de0a5bb2e7772cb404b0af08d240e1f04f543)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** set Directory as active page for the Service Connect Wizard ([afca015](https://gitlab.com/commonground/nlx/fsc-nlx/commit/afca01585193931c356dcefd78faeaffbcb2c01d)), closes [fsc-nlx#278](https://gitlab.com/commonground/fsc-nlx/issues/278)
* **directory:** display correct delegator and service provider ([778f020](https://gitlab.com/commonground/nlx/fsc-nlx/commit/778f020793b4e2120dc8bbe45310d920be0eaa91)), closes [fsc-nlx#286](https://gitlab.com/commonground/fsc-nlx/issues/286)
* **helm:** fix keycloak ingress template ([c709710](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c70971023b78967996a67a1fd477761017c85f0e)), closes [fsc-nlx#274](https://gitlab.com/commonground/fsc-nlx/issues/274)
* **manager:** ensure sorting is correct in internal get contracts endpoint ([0b744d6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0b744d61e1a7bdff217a17cf744ce3d258cdcf5b)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** properly check the token properties ([9744a0b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9744a0bfedce561bb992b7f4d05cdf08c1726065)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** use context from main in db connection ([cf1ddbf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cf1ddbfa6e0e69fc3fb586427e73967d4c12d525)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **outway:** get token call now works ([716017e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/716017e43d9dc5b11a39d3c8f5fd66d2edf88bdf)), closes [fsc-nlx#259](https://gitlab.com/commonground/fsc-nlx/issues/259)


### Code Refactoring

* **apps-overview:** remove NLX references ([eed6463](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eed64630f7d84e7b03628562fc3764aba1614a90)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* consolidated command line libraries ([9dd1019](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9dd101981e452edb8f1ecb51f1f1960b4699c577))
* **controller:** changed dex to keycloak as authorization server ([3cec54f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3cec54f4231a341d34e5cf42f860afa57b9d5079))
* **docs:** remove nlx references where possible ([bb723ac](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bb723ac5da55084e7e511407e230dad188c93832)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* let outway get token directly from peer ([a861ad8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a861ad8c850d036d88b00856a45c6a34c8747679)), closes [fsc-nlx#259](https://gitlab.com/commonground/fsc-nlx/issues/259)
* **manager:** removed internal get token endpoint ([b1713ef](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b1713ef4ee8feef053027437158a6fe12daf1175)), closes [fsc-nlx#259](https://gitlab.com/commonground/fsc-nlx/issues/259)
* **manager:** use different structs to indicate service type ([083513d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/083513d71cc063cc2bf662b5af7dffc7c92f2588)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **outway:** removed unused CA parameter from auth plugin ([c8be758](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c8be758ba9965058e56b160f6dc44238e8684648)), closes [fsc-nlx#11](https://gitlab.com/commonground/fsc-nlx/issues/11)


### Tests

* **manager:** add happy flow test for the internal get_token endpoint ([10f8af1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/10f8af10608b8592b4747cac9a6664cf8a8998fe)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** implement test for the internal SubmitContract endpoint ([0514d36](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0514d3627fc3c03ee71e79f820d717e164a6bf7c)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** use current date instead of a fixed date ([f420012](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f420012c7d255694fffb5157befa4db76dec67c3)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)

## [0.20.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.19.0...v0.20.0) (2024-04-22)


### Build System

* add Controller for the Directory ([c85c3a5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c85c3a5ace52be502bd5ae3c101e5d920c5ec1b6)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **txlog:** resolve leftover conflicts after rebase ([e9c8f8d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e9c8f8d0a9e70fbc933d33941df3240abeeefd03)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* update commitlint monorepo to v19.2.2 ([2fc038a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2fc038af6089db11ed4c900d6858d98cc4b2f783)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency htmx.org to v1.9.12 ([20b6489](https://gitlab.com/commonground/nlx/fsc-nlx/commit/20b6489002b5d0f7c4727823fa87d8d540df2008)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update mocks ([1a9c96f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1a9c96f6a2e9dd24a09a3290479cc3914c70fa9b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/golang-migrate/migrate/v4 to v4.17.1 ([09b45b8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/09b45b8a09e667ca54bca95f73e2859c98612f98)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update nginxinc/nginx-unprivileged:alpine3.18 docker digest to 7b43166 ([63a4f15](https://gitlab.com/commonground/nlx/fsc-nlx/commit/63a4f1599004e7764a4d7a959e4beb3ed9dd99a2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.12.2 ([115e258](https://gitlab.com/commonground/nlx/fsc-nlx/commit/115e25844cfb6c38e76d0b6d5c474421c3afb42b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.2-alpine docker digest to 951bfda ([7befec2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7befec2def1d371d43a475e5733ce182e61feb96)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* configure max_connections to 100 for PostgreSQL ([1741353](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1741353b7d0aa93ec98ccdedc14e6a58c03e9fc4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Documentation

* update FSC logo to horizontal variant for the README ([7ca345b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7ca345b8d8d156cdd2507000aee20d80d1b604b1)), closes [fsc-nlx#270](https://gitlab.com/commonground/fsc-nlx/issues/270)


### Features

* add auditlog to manager and add correlation ID to api ([fd5a6c3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fd5a6c32ba8192210ed1f0866eb942f6ca700c86)), closes [fsc-nlx#211](https://gitlab.com/commonground/fsc-nlx/issues/211)
* announce new manager address to all known peers ([40d88cb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/40d88cbe61cf49d3e177a1251911013a8144aa2c)), closes [fsc-nlx#272](https://gitlab.com/commonground/fsc-nlx/issues/272)
* **common:** log metadata in transaction log ([c11540f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c11540f94ea9042cdecd67bb39858845299ef383)), closes [fsc-nlx#268](https://gitlab.com/commonground/fsc-nlx/issues/268)
* **docs:** replace NLX logo with FSC logo ([6e0a557](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6e0a557f3673e46477321a3af65c493ab7392ef8)), closes [fsc-nlx#270](https://gitlab.com/commonground/fsc-nlx/issues/270)
* **helm:** configure txlog metadata in review cluster ([f9e74b4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f9e74b4a459db62f0523e8b585cf8a3cf399593e)), closes [fsc-nlx#268](https://gitlab.com/commonground/fsc-nlx/issues/268)
* **inway:** add option to add metadata to transaction log ([3ba603c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3ba603ce7e080682ece1bf2af2d788384d651630)), closes [fsc-nlx#268](https://gitlab.com/commonground/fsc-nlx/issues/268)
* **manager:** add db connection settings ([1ec5f47](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1ec5f47a600c24d67a912d7adffd341f28c9fc5b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** add option to retrieve metadata from internal manager ([d6b4f50](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d6b4f50848c1aa565a61435a3fea69817b82892f)), closes [fsc-nlx#268](https://gitlab.com/commonground/fsc-nlx/issues/268)
* **manager:** add service type to (delegated) service connection grants ([dee8a90](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dee8a90ab95c981f2b88c9d4fc93fe4ae104eb21)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** include grant type in the grant hash ([39025c5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/39025c5d9a1d675cb89107ef71df387c1a32813b)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** include service type and delegator in grant hash ([ba0f31e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ba0f31e389d89b39b3f889aab7fcff8c72d62980)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** validate Delegator info when retrieving a token via the internal Manager ([0a4c5be](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0a4c5be46241746c89e2beba76db204993d8786d)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **outway:** add option to add metadata to transaction log ([2d674ad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2d674ad47cb22a8618f2b844bf72457bd854d27e)), closes [fsc-nlx#268](https://gitlab.com/commonground/fsc-nlx/issues/268)
* **txlog:** add metadata endpoint for txlog-API ([b803b55](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b803b55446c4a2bba968344527e68839234ad867)), closes [fsc-nlx#268](https://gitlab.com/commonground/fsc-nlx/issues/268)


### Bug Fixes

* **manager:** disable the transaction log ([325ea17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/325ea17c8c315fcfc48de273aeb70ffb1d5891c4)), closes [fsc-nlx#276](https://gitlab.com/commonground/fsc-nlx/issues/276)
* **outway:** use correct http error func ([f86cb7d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f86cb7dfa02cd76f4c5120d8db13c4dad3b1c531)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Code Refactoring

* **manager:** replace iota with explicit map to integers for the hash types ([b9934b2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b9934b2b03f520064e80be0ee29c9e930563c85f)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* review feedback ([de0da00](https://gitlab.com/commonground/nlx/fsc-nlx/commit/de0da001e1c0a040a18021c6a516bd2889053c16)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)


### Tests

* add extra organization certificates ([335346c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/335346c6b8721268b690feb2b285bdb7ddbe0a8f)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** add test with Delegator in DelegatedServiceConnectionGrant for INT /get_contracts ([50c3b0b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/50c3b0bd66db8ae765f25515af42400d8b21c64d)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** add test with Delegator in ServiceConnectionGrant for INT /get_contracts ([c53776d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c53776d88426e5a18e3b2399b50a0ee8d3824415)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** fix order of contracts in INT list contracts ([6a745e5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6a745e5fe275a26baab87c94fde4256338c5278b)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** implement test for Manager-SubmitContract-5 ([a224043](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a224043e65f1721d4d4e4010e8fecbc510ffb12d)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** implement test for Manager-SubmitContract-6 ([f1e54c3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f1e54c3eaa727f5ed0f85108337d81ad7c2a07ac)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** re-order SubmitContract tests ([f6a8c33](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f6a8c33bcb5f12c24d87761c73c79943792e0aa9)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **manager:** verify that autosigning contracts works ([289d71e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/289d71e49010efb7710f39e65f67ba7610a72b70)), closes [fsc-nlx#267](https://gitlab.com/commonground/fsc-nlx/issues/267)

## [0.19.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.18.0...v0.19.0) (2024-04-11)


### Build System

* remove E2E tests ([9f6541e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9f6541e274bcf981373f799eeb08614023dea2c4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @axe-core/webdriverjs to v4.9.0 ([2b3dc39](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2b3dc39ab72a496bbfafc4e0100d9081fdd42c01)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @cucumber/cucumber to v10.4.0 ([d064b80](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d064b802a2e6032753fe284a0256408d308cc34a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.12 ([168e4db](https://gitlab.com/commonground/nlx/fsc-nlx/commit/168e4dbd0c5ac90621e9afd386a9cdd030f48db1)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.12.2 ([6dac996](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6dac9966f9ae258487c3ea34e17c10ba266ee492)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.12.3 ([0a78bb9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0a78bb982a80b49c485c0ce198828a2a637f98fb)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.12.4 ([ad19a78](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ad19a785a8f66fb479d8810752c6356af42fe65f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.12.5 ([dd29d5b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dd29d5bd57523093aa11714557ae024631670c66)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency glob to v10.3.12 ([fe1c6b7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fe1c6b74d1a31cb3f8c3d2f6432f0eaa99b5d7be)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.14.4 ([475fd23](https://gitlab.com/commonground/nlx/fsc-nlx/commit/475fd238843b9a468964e7012dfa603e92e68b72)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency selenium-webdriver to v4.19.0 ([9e279bd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e279bd4e5d939e8f77c625f7d878c6d8a4a119c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23.0.7 ([4f52ebd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4f52ebdbe2592d3bad4c19766ba6ad1f48e79bae)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23.0.8 ([a285029](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a285029b07af3ed1df27fbfbf7362a70e4f9137c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency typescript to v5.4.4 ([9d100af](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9d100af0813ff66eaa3f0a95dc00150738dd2d8f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dexidp/dex docker tag to v2.39.1 ([d92f0fb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d92f0fbadde541640624888e34bac01a89b1db14)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docker.io/curlimages/curl docker tag to v8.7.1 ([b6d5e4b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b6d5e4b89a9e6e8547cb040ebdd5db468e6c19fb)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docusaurus monorepo to v3.2.0 ([766ee56](https://gitlab.com/commonground/nlx/fsc-nlx/commit/766ee560554f27caee18ea38d2f43d5457c27aab)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docusaurus monorepo to v3.2.1 ([066b91f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/066b91f60ac36a28d42fbc72a1ab7b066fd9c593)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.22.2 ([39288d6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/39288d6159ea2627f743eba1ffbe59920b4ed42c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update helm release dex to v0.17.1 ([ddc626b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ddc626b35ee8cdcab860dc57a40e6e1b6fe6d699)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/getkin/kin-openapi to v0.124.0 ([cc66713](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cc66713f9f30567286256d788104450cf4756854)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.22.0 ([59c83fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/59c83fce9f887a90ca2e7d6ab5e97eb0947de811)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.23.0 ([5759fa7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5759fa7601276617f83678d7d88b99a790da4887)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.24.0 ([8e486f4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8e486f4db87fccf649eb1c65c0267a98b6604f1d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/oauth2 to v0.19.0 ([d082a7d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d082a7defc4678af02dfbdb055adcb389d9c6cb4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/sync to v0.7.0 ([4b13a28](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4b13a28585cad4c065c2872d1ebf3b6bca70d6bc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update nginxinc/nginx-unprivileged:alpine3.18 docker digest to 5b49ce2 ([5654074](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5654074eddd6d77bde9ffcdcf620395da0c1fa53)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update nginxinc/nginx-unprivileged:alpine3.18 docker digest to e5a49a7 ([c09dccf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c09dccfe6c66498955ce36f6508e4d94e52a2551)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 7e22729 ([40d8b33](https://gitlab.com/commonground/nlx/fsc-nlx/commit/40d8b33232d3bf200a7a482c668d4d33f5cfa21f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.12.0 ([90b70fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/90b70fcc674526b962493502265e3d644ea5d2e6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.12.1 ([c02e91d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c02e91d2a3328c21f3ee47460bbf005daf3f35bf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.63.0 ([05e205d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/05e205df0694c9cea94598a7b955022936925aaf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update selenium/standalone-chrome docker tag to v4.19.0 ([dbbc634](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dbbc634e0b046c653e925894ac73f589abca8a4c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update selenium/standalone-chrome docker tag to v4.19.1 ([88c4572](https://gitlab.com/commonground/nlx/fsc-nlx/commit/88c45726ba089d48f8b651fa60bdacc58d6a9c42)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v7.5.0 ([b12ed0e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b12ed0ec115dced10b873c00f6ee43ef944a3354)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Documentation

* update README change nlx to FSC ([548ed7c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/548ed7ce0b4d6534910e38cc61ff441ee4ffbb88)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Features

* **auditlog:** add api endpoints to set and unset log records under review ([bf872be](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bf872bedf89b8cc6e119e49e30a4dbf27de4c96e)), closes [fsc-nlx#242](https://gitlab.com/commonground/fsc-nlx/issues/242)
* **controller:** display Delegator ID when connecting with a Delegated Service ([383de8c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/383de8caf5cf5c1ec9b55b73e4c8a0fe7731e88e)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **controller:** replace NLX logo with FSC logo ([291ad38](https://gitlab.com/commonground/nlx/fsc-nlx/commit/291ad38c28ca38d46610c78211b77ad619fee4e3)), closes [fsc-nlx#270](https://gitlab.com/commonground/fsc-nlx/issues/270)
* **directory:** remove 'FSC NLX' from title ([c4d8e28](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c4d8e28901adc64cc4539dc3fe93b9d8dcde3ed4)), closes [fsc-nlx#270](https://gitlab.com/commonground/fsc-nlx/issues/270)
* **directory:** replace NLX logo with FSC logo ([205c622](https://gitlab.com/commonground/nlx/fsc-nlx/commit/205c622cf1e9c1f8ebe8a6b83598d83336753d5d)), closes [fsc-nlx#270](https://gitlab.com/commonground/fsc-nlx/issues/270)
* **docs:** add information about the FSC environments ([544f2fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/544f2fc0d6417ed33e4b27948e7e8484f41a4c03)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** validate public key thumbprint ([1747c43](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1747c437e886144f28b40ddd72789fd28c5a70e8)), closes [fsc-nlx#247](https://gitlab.com/commonground/fsc-nlx/issues/247)


### Bug Fixes

* **controller:** ensure Directory Service detail page can be rendered ([a37236a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a37236abab354f6823bd012260540a81c4be1e5a)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)
* **controller:** prevent showing 404 for Delegated Directory Services ([bc2a5c6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bc2a5c62593d7e52857e9170435de06519b27811)), closes [fsc-nlx#265](https://gitlab.com/commonground/fsc-nlx/issues/265)

## [0.18.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.17.0...v0.18.0) (2024-03-28)


### Build System

* rename NLX -> FSC for the OIDC flow ([63aecf5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/63aecf519fdef9e4664e65ae83149813f5ccdf37)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update .dockerignore ([877ca04](https://gitlab.com/commonground/nlx/fsc-nlx/commit/877ca043605622c4d5e95ff23af2b7a4fc15ec9a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @cucumber/cucumber to v10.3.2 ([91648bf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/91648bff545ab1d6b104910969e7796ec85a6877)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @cucumber/html-formatter to v21.3.1 ([18c6ccd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/18c6ccdd583a8e6608ab8d8e227fa656a84a0c3b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.11 ([56934a6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/56934a698e0efb7609e03cc83ec84d228b8ae62a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update gcr.io/kaniko-project/executor docker tag to v1.22.0 ([cb4db23](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cb4db23d406d7e99e5908b5c92cb8b5066219253)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/cenkalti/backoff/v4 to v4.3.0 ([2ff1007](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2ff1007eadf89a9bb07ac219030653743b2c7f14)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-co-op/gocron/v2 to v2.2.9 ([1e38b7e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1e38b7e798d88b01765533adf5c3444324f3f17f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v7.4.0 ([87f9db3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/87f9db33bcbcc10a3927beef8fa49120cc13ece1)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* move OIDC config to a secret for Gemeente Riemer ([41e6f43](https://gitlab.com/commonground/nlx/fsc-nlx/commit/41e6f4394c2a76dad4779bdc8bc2ba38689434a8)), closes [fsc-nlx#245](https://gitlab.com/commonground/fsc-nlx/issues/245)


### Documentation

* add the auditlog to the try-fsc-nlx helm guide ([f4ca8ea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f4ca8ea84a481cca6e2958756c7d83e6deca79d1)), closes [nlx-fsc#240](https://gitlab.com/commonground/nlx-fsc/issues/240)
* added audit log to try me docs ([881b8f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/881b8f61966abd5018c4e235b6fb6b93ee554a9e)), closes [fsc-nlx#240](https://gitlab.com/commonground/fsc-nlx/issues/240)


### Features

* **auditlog:** add automatic clean up of records older than the retention period ([42ed283](https://gitlab.com/commonground/nlx/fsc-nlx/commit/42ed283659470c2b7db7615b9891db6552126571)), closes [fsc-nlx#241](https://gitlab.com/commonground/fsc-nlx/issues/241)
* **auditlog:** add README ([74df566](https://gitlab.com/commonground/nlx/fsc-nlx/commit/74df56655a42159583ec255a61fcee48eac4efbf)), closes [fsc-nlx#235](https://gitlab.com/commonground/fsc-nlx/issues/235)
* **controller:** update Transaction Log icon ([0dc6158](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0dc6158e81956ab698582827aaf4db7617eb8109)), closes [fsc-nlx#251](https://gitlab.com/commonground/fsc-nlx/issues/251)
* **helm:** introduce support for using an existing secret with OIDC ([60f5ba4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/60f5ba4dc7293734229bac3cc871815925d73517)), closes [fsc-nlx#245](https://gitlab.com/commonground/fsc-nlx/issues/245)


### Bug Fixes

* **controller:** ensure Contracts & Contract Detail page renders properly ([8f883d3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8f883d3acb7737ec1373a1c78b1458dc4e455b3a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** ensure dates are shown correctly on Safari ([522839a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/522839a7b5cc54bbcdbe84ee29483045293a2a0d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** ensure Service Connection page renders properly ([7f29302](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7f29302833f5709f7b5604e1d7f671fc9155a3d7)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** use Outway Public Key fingerprint in the UI ([81acfb1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/81acfb1df990411b0edcd05fa0b8b0b4a21c757d)), closes [fsc-nlx#249](https://gitlab.com/commonground/fsc-nlx/issues/249)
* **manager:** round signature timestamp to seconds when auto signing contracts ([63b5484](https://gitlab.com/commonground/nlx/fsc-nlx/commit/63b54847fdc5ec14535af57367510f09327e370b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Code Refactoring

* **manager:** rename certificate thumbprint to public key thumbprint ([d132624](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d13262422b1042f1fb4868456ae3bc5483afeb6f)), closes [fsc-nlx#250](https://gitlab.com/commonground/fsc-nlx/issues/250)


### Tests

* **manager:** add test for internal accept, reject and revoke endpoints ([82c2120](https://gitlab.com/commonground/nlx/fsc-nlx/commit/82c21205f4d03ed1f9dde1f88390553d4569dd69)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** include nanoseconds in test clock ([069eef7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/069eef7c022b9b74efe6d87f46e0e89aeeb0039a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [0.17.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.16.0...v0.17.0) (2024-03-25)


### Build System

* pin docker images on sha256 hash ([d7f1463](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d7f14639812b78d01e4ed9f02b396d6056d0b31c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* re-enable the manager of organization B ([72b5626](https://gitlab.com/commonground/nlx/fsc-nlx/commit/72b5626f74f3359ee43cdf3eb79eb43eaf369595)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* remove /v1 from the txlog API spec ([d26d28a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d26d28a82361acff33be2d8bdb3c4f361e11436f)), closes [fsc-nlx#223](https://gitlab.com/commonground/fsc-nlx/issues/223)
* update commitlint monorepo to v19 ([d3e98f8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d3e98f8f41556b641ce05b466e4f763c9be2e0b2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update commitlint monorepo to v19.0.3 ([08cefb2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/08cefb26262ab8865262d5c4e72293f8d8eb0957)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update commitlint monorepo to v19.1.0 ([eed0e39](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eed0e39b3e75d6c59c9b7e5f0c76478b74744834)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @commitlint/cli to v19.2.0 ([c67b4c4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c67b4c48b61e5dc393ff99a617be80802755c586)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @commitlint/cli to v19.2.1 ([75c2b53](https://gitlab.com/commonground/nlx/fsc-nlx/commit/75c2b53d4858ecca7348df531638518fac221248)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @cucumber/html-formatter to v21.3.0 ([e48feca](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e48fecafd70f7981a6acce1045f7ed6e364cd305)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @semantic-release/gitlab to v13.0.3 ([5ccaa94](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5ccaa9449c60ad6330bc675191cf272179f92ba0)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.4 ([86cd177](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86cd1779d36674bc26b5b64a81325fd5e48dfdea)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.5 ([9d8b88e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9d8b88e7ffb90cf771671adc1f89150c19ac2359)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.6 ([342d955](https://gitlab.com/commonground/nlx/fsc-nlx/commit/342d955b733ee7933117cf6cc09b2fd3652f9bac)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.7 ([3918d14](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3918d148826db19ba466b2a42c17431285c651f4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.8 ([0651405](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0651405868add111bf7225171705f64c34cf289b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.20 ([73c2093](https://gitlab.com/commonground/nlx/fsc-nlx/commit/73c209352e7803c6ecd209e924c3efc067f574c1)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.21 ([c7b31e1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c7b31e1598ccef758b498c8ee134f2784ee932e2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.22 ([9344c15](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9344c1567a2a85e26d6c6ce1e1d3441619fd76f9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.24 ([6e44301](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6e44301c62b55e1e0393deadfb147ae8ac36dab4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.25 ([20dc271](https://gitlab.com/commonground/nlx/fsc-nlx/commit/20dc27139773193fddedf27299e9155c3d0afac8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.26 ([6314ec9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6314ec9b403e571f69f3af80956d5ef83e0698b9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.27 ([f9a4c86](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f9a4c8693bc9ee6b944281f1039ba37d1c63c37e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.28 ([1e1c540](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1e1c540009eec0f58138bb6d613fb2114360572a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.29 ([caaa87e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/caaa87e56b9a2f1268688b30a9327970ceff772c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.30 ([a409ad6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a409ad6b5be8938f32df9c2f7ad4057e98f75fa9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/selenium-webdriver to v4.1.22 ([3923009](https://gitlab.com/commonground/nlx/fsc-nlx/commit/392300954953a4054daed92367620a2e7d69bbae)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency eslint to v8.57.0 ([22bdff3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/22bdff3ace868ad8522f0e33b19a398b85018a8c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.14.2 ([4d2420e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4d2420e14ec98063b90a3cafd044bc6c03f65024)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.14.3 ([14c4cfd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/14c4cfd23114b87a58775b041a70529117f373e2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency htmx.org to v1.9.11 ([425fb84](https://gitlab.com/commonground/nlx/fsc-nlx/commit/425fb84af4196654a91b5dd0a690f8d9a75c1d6d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency selenium-webdriver to v4.18.1 ([2fce30a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2fce30ab35982de37ff5ccdd7fabfd87d580d051)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23.0.3 ([9326e01](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9326e0130dc9197e902c871aed5f8705790e30e8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23.0.4 ([eebb04f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eebb04fedfa161e984086e57a604c6c2166f377e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23.0.5 ([3f22114](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3f22114c9ef4d092a83d2bf3847a677fecbe9667)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23.0.6 ([a5d1d65](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a5d1d659c71a9ed6c46915ae62e5aec372772f49)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency typescript to v5.4.2 ([9de4d1a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9de4d1ac1529daa6b943afe4a627f93b9eaace01)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency typescript to v5.4.3 ([516cd74](https://gitlab.com/commonground/nlx/fsc-nlx/commit/516cd74d615325dd29dcd80fd30997dcc67af292)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dexidp/dex docker tag to v2.39.0 ([0e89576](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0e89576d89e38d92bde0831c7e43dac81908a2ee)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docker.io/cfssl/cfssl docker tag to v1.6.5 ([c5fad66](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c5fad661703bf2c2bb2e4e6fd756eca141d0fb32)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update Earthfile to be compatible with Earthly v0.8 ([8c49a51](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8c49a51f019973e4e7069f20ac4c2af00e4c33c8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.22.1 ([30eea48](https://gitlab.com/commonground/nlx/fsc-nlx/commit/30eea48d10495044cf398c7e620c7889d7e129ca)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.1-alpine docker digest to 0466223 ([ccf158c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ccf158c4467bbc3149d4da4a8682b46e3b15b440)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang:1.22.1-alpine docker digest to 6f179ec ([7cda45d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7cda45d682699fc2e69b6d5961c698e98e05d2f6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update helm release dex to v0.17.0 ([8d8e577](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8d8e57706222f424d531848c9ab13edfbf9143be)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/cloudflare/cfssl to v1.6.5 ([606bbeb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/606bbeb1217de063c4851824699f095779d77080)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/coreos/go-oidc/v3 to v3.10.0 ([b8e5ee3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b8e5ee33cd60585cbf917ba3b139f829df320dad)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-jose/go-jose/v3 to v3.0.2 ([d1a946c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d1a946cdcea242cf9c84c629ad4d17426b83c944)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-jose/go-jose/v3 to v3.0.3 ([8a6d1a2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8a6d1a2860946b580cce6fdd506aa68451866714)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-jose/go-jose/v3 to v4 ([2822da8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2822da8b18c007576e3be72e7c9ef299d192380b))
* update module github.com/golang-jwt/jwt/v5 to v5.2.1 ([3a3e74f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3a3e74fd3d64d5c9dfe9642c8dfc734e5b79d9af)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/jackc/pgx/v5 to v5.5.4 ([ad53b39](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ad53b39792df26be4761f54dff0e4bb7bb580248)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/jackc/pgx/v5 to v5.5.5 ([8fcd689](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8fcd689b14285a8ea8b55ef0e664aff33f42856a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/lestrrat-go/jwx/v2 to v2.0.21 ([d92ba8f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d92ba8f4f8c0d181b09183fddfbe846eabc95be3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/stretchr/testify to v1.9.0 ([38a4874](https://gitlab.com/commonground/nlx/fsc-nlx/commit/38a4874d88629376bf3b80bee5e2d3b03d1dbd2d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module go.uber.org/zap to v1.27.0 ([ee8ceb8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ee8ceb89af7a4fe4c2c23774629cc251c04d4d69)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.20.0 ([419e0cf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/419e0cff4244fb90b0ea0ecbe8ee21e2d016a4ba)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.22.0 ([36b2da7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/36b2da70e25e131a6e0a6fb0a2d29c237865220f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/oauth2 to v0.18.0 ([69e759b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/69e759bb0d1911daeee16eccb8b0ec3dc17ac134)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update nginxinc/nginx-unprivileged:alpine3.18 docker digest to 4883a29 ([7b0f0e8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7b0f0e885751a8cf7098608f16cd85eada88ef01)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update nginxinc/nginx-unprivileged:alpine3.18 docker digest to 7c1d415 ([49145e4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/49145e4ad803ab15091b0c5dd72c26d6b50256a5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to 842159a ([306b5d8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/306b5d8a311707721a4ed5b2c49c52cdf8c408cc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to bf77dc2 ([cd6faa9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cd6faa959ebe98ca7d96ea71ed94ece964bacba2)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.62.0 ([6083633](https://gitlab.com/commonground/nlx/fsc-nlx/commit/60836330502b8a59c26cc4714366a245f0173c9a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.62.1 ([51330b6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/51330b6926e35902937b76c584ba4122092d70b6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update parcel monorepo to v2.12.0 ([95072fd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/95072fdf83faa5a7996b2968861c3547557b2c8d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.2-alpine docker digest to 1d74239 ([fc12210](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fc122107ce582f46c1d36f9f5186828f874fb707)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.2-alpine docker digest to 49fd8c1 ([0d2403c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0d2403cd41d829c2b6fc546bcf10e05e765d8471)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.2-alpine docker digest to 8671bb4 ([3883f29](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3883f29408dcdf67f249908c4b323a6b01108451)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres:16.2-alpine docker digest to c9e7d6b ([253f9cc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/253f9ccc25e0cab759d88774f2d9be5970aa5daa)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update selenium/standalone-chrome docker tag to v4.18.0 ([3592fe2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3592fe2e1472116ccbff49c5e0912c0e7d8f7682)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update selenium/standalone-chrome docker tag to v4.18.1 ([5ed8aba](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5ed8aba5d1714d5714ae1ec58950e5c5d54f1d86)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v7.1.0 ([7e570fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7e570fcb024b22b4dec0c15475c55056026bfa18)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v7.1.1 ([d3d2e11](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d3d2e114cc3274ea0cedc8af50637652cb22c350)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v7.2.0 ([7403ea7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7403ea701f9d7a6a6a9192cdac465911e6cbcd38)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v7.3.1 ([a61a039](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a61a039aad3a2eefa586da3fa08511ed6d096500)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* add auditlog sqlc generation to earthfile ([57fa0f2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/57fa0f216293bc904387672516d439639d281ebc)), closes [fsc-nlx#207](https://gitlab.com/commonground/fsc-nlx/issues/207)
* deploy auditlog api for all organizations ([756ee1c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/756ee1cab0b3821a8f03b7876959b2f167db8a71)), closes [fsc-nlx#207](https://gitlab.com/commonground/fsc-nlx/issues/207)
* include auditlog in Go tests & linter ([a108ae2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a108ae23d59068feb0e8add82922537999911c5c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* remove goimport command from sqlc sections in the Earthfile ([a791fe2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a791fe2ed33df9ed1cf7c1ce8f3521a0c6758a8b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Documentation

* add missing certificate ([2d202d4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2d202d4e84a35044fa0b28704e17c5151d8df439)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** regenerate json docs for the controller chart ([6364b5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6364b5e5622619520a693fe0d686cf66cec48ed7)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Features

* add auditlog component ([fc89fea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fc89fea62ea19cadb259e32b2fb7abd5deb66bc3)), closes [fsc-nlx#209](https://gitlab.com/commonground/fsc-nlx/issues/209)
* add auditlog domain models and oas spec ([1afc1b7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1afc1b764196e88628d9c807ddc45c1bcd151919)), closes [fsc-nlx#208](https://gitlab.com/commonground/fsc-nlx/issues/208)
* add auditlogger to stdout adapter ([1553a90](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1553a90193badd874efcaafaef0d2a26603ee2dc)), closes [fsc-nlx#243](https://gitlab.com/commonground/fsc-nlx/issues/243)
* **auditlog:** add auditlog page ([9ebf5dd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9ebf5dd3fb86c59d0400b7b045c3b528303f55c4)), closes [fsc-nlx#233](https://gitlab.com/commonground/fsc-nlx/issues/233)
* **auditlog:** add listRecords api ([a7f2955](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a7f29551868c744f25d8648a3ecf512673b0e209)), closes [fsc-nlx#209](https://gitlab.com/commonground/fsc-nlx/issues/209)
* **common:** create common Auditlogger interface ([e454ec2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e454ec22dd945bba8ec3201d6b25ab49be60194c)), closes [fsc-nlx#207](https://gitlab.com/commonground/fsc-nlx/issues/207)
* **controller:** add Audit Log page ([7ab41ad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7ab41ad0e0be2317257483525556eee46320bf7d)), closes [fsc-nlx#233](https://gitlab.com/commonground/fsc-nlx/issues/233)
* **controller:** add error logging to the rest port ([e3d0075](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e3d007591f127c85e761436abc547eaf4afbbab6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** add missing translations ([9ddbb75](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9ddbb753d0669659cb8d80b599346eae182940cb)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* **controller:** add page header to every page ([a55fa1e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a55fa1eeff5848112183c774f0f024e29e92050a)), closes [fsc-nlx#214](https://gitlab.com/commonground/fsc-nlx/issues/214)
* **controller:** add query for retrieving audit log records ([9126bc6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9126bc63aa76f4f45657447e7d22cf01b513d98c)), closes [fsc-nlx#233](https://gitlab.com/commonground/fsc-nlx/issues/233)
* **controller:** add support for all audit log types in the Controller UI ([9b1a76f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9b1a76fd52ebed30815bb938cbb786bb17f1ae9a)), closes [fsc-nlx#233](https://gitlab.com/commonground/fsc-nlx/issues/233)
* **controller:** add support for the retry distribution audit log ([5cff987](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5cff987eb107ff48abe27e236b3dcd58397aff06)), closes [fsc-nlx#233](https://gitlab.com/commonground/fsc-nlx/issues/233)
* **controller:** controller creates Auditlog records ([b287090](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b28709077642ba5b7ca1c69317d741eaba5fdcf5)), closes [fsc-nlx#207](https://gitlab.com/commonground/fsc-nlx/issues/207)
* **controller:** disable submit buttons for the duration of the request ([778b6d5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/778b6d5ff9569794b84b94c075261ccd6f80bcb2)), closes [fsc-nlx#213](https://gitlab.com/commonground/fsc-nlx/issues/213)
* **controller:** hide retry button when next attempt is scheduled ([a509436](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a509436e1891666730612686277cc1ba7085e826)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **controller:** improve styling for no access message ([851b8f4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/851b8f47d1ee9dfa139ab4895e9762b5db8f32bb)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* **controller:** improve title for the Access section ([e96c7ea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e96c7ea78e13b6e3a292d0e1bb41986db8724422)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* **controller:** include hours, minutes and seconds in next attempt info ([0a67423](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0a674239646f1fedad73481645294a217ce1c6f4)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **controller:** remove status column title from distribution failures ([f38a233](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f38a233de597a7a41a013ca691ba800f07337586)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** reorder buttons of the Service detail page ([c9671fd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9671fd3bc0f5703a1cc44a5bdd63e15c3f31913)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* **controller:** request acces to service from the service detail page ([bfefc74](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bfefc74bf31b7282ca96c58e25c20e7c1666aa47)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* **controller:** use full width of page instead of 40 rem limit ([afc534f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/afc534fce814240018bff53a4386bc2beb33cf42)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* **controller:** write auditlog for a retry of a failed distribution ([b1db56a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b1db56a378fffb6fe3a18c5c98ed0cb11b5ecbeb)), closes [fsc-nlx#207](https://gitlab.com/commonground/fsc-nlx/issues/207)
* **manager:** add grant hash in feedback when retrieving token fails ([e4e2da0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e4e2da00f633e602a619565ce81e398322a8cdb3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** use exponential backoff when sending contracts and signatures ([3596e33](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3596e33a0c78e22e436e8bae355cc87aa7dc89f5)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)


### Bug Fixes

* add types to actor, event, source and status objects in oas spec ([930516a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/930516ad1d736a56a3300378ba28536cfce7253a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **auditlog:** add additional checks for create contract event ([3c8a2b8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3c8a2b8f10498e9620a9b0871f56bbb9f640ce8a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** add audit log source ([1911fbb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1911fbbf50e4b2ca562bfc01c77a4fdff46eccc7)), closes [fsc-nlx#207](https://gitlab.com/commonground/fsc-nlx/issues/207)
* **controller:** add auditlog type and rest address flags to helm chart ([2bb229d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2bb229db57810f6f5f313868b8cfd5b2caae8c84)), closes [fsc-nlx#243](https://gitlab.com/commonground/fsc-nlx/issues/243)
* **controller:** add missing translations ([8aa71da](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8aa71da563fde7e01a9f3898d1ceb88ddab6e3f9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** add translations for resending Signatures ([a9b702e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a9b702e67c25012a9f6a0e34c7e6567ab21f2b82)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **controller:** correctly display retried at timestamp ([2db059b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2db059b94d5daec3ae1cb0b03813b70781b80652)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **controller:** display next distribution attempt instead of last attempt ([85e3c88](https://gitlab.com/commonground/nlx/fsc-nlx/commit/85e3c88a07a787170ffe0893bc8f29df2ae7013b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** ensure next attempt is properly set ([49079a6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/49079a674c0198a8d317d5e2e34165b1688f46a7)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** make Peer info available ([7902d9e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7902d9ed96f1ec2ee94e2089f60323dacc910ece)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** make Peer info available ([1188b59](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1188b599375863e905ff3e57c02dbeb88bbf4d38)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** only show failed distributions in contract details ([39ea93f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/39ea93f81da185774d47aaf06cbdfccec6ee3167)), closes [fsc-nlx#224](https://gitlab.com/commonground/fsc-nlx/issues/224)
* **controller:** pass correct authorization information in /services endpoint ([bf5cffa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bf5cffa2f36a71cbae661c88722bc5fc2f510c1d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** remove invalid code from the shut-down-line icon ([1270701](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1270701c7bde75d3e056f4e9577ff66d5f3764bf)), closes [fsc-nlx#233](https://gitlab.com/commonground/fsc-nlx/issues/233)
* **controller:** return an error when obtaining peer info fails ([0b41569](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0b4156928db91a8587937203ad5a7e0f92df4814)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** accept, reject, revoke signature truncate timestamps ([35afcf7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35afcf7cfe1425784f771c67d3e3f0a1ffbc1153)), closes [fsc-nlx#246](https://gitlab.com/commonground/fsc-nlx/issues/246)
* **manager:** ensure the Clock and Logger are being passed to the PeersCommunicationService ([21ea04b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/21ea04b1f19bfc04498beb8ef50ce81dc237feb1)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** ensure the next_retry_at value is updated when upserting a distribution ([384379d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/384379df250d3f112eda7d1cec3d8f798289deb0)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** ensure timestamps are rounded on seconds when creating a contract ([07586eb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/07586ebca794401c2820f07fce01b37218106529)), closes [fsc-nlx#231](https://gitlab.com/commonground/fsc-nlx/issues/231)
* **manager:** revalidate contracts before issuing tokens ([55fec40](https://gitlab.com/commonground/nlx/fsc-nlx/commit/55fec4023cee5d2263fb4d15bfb317cf9a1b4321)), closes [fsc-nlx#206](https://gitlab.com/commonground/fsc-nlx/issues/206)
* **manager:** use correct type in get service listing ([d5607c8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d5607c86fdc13f3112bfc3ed306bdc3da0c9a53a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** validate contract used to generate a token from ([ff9295d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ff9295da6df57e52da17333cb6d0cee37d84ec3b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **txlog:** return error when not providing any records to create ([a7bf4ff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a7bf4ffea68eb6b9327c8952276c9ef482dc85fc)), closes [fsc-nlx#223](https://gitlab.com/commonground/fsc-nlx/issues/223)


### Code Refactoring

* **controller,manager:** rename failed distribution properties ([2b51af9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2b51af9b45fd4c7c0b8894d038929aa099db1f3a)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **controller:** use constants in the add contract form ([74b36c0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/74b36c07e890be1de23107120a80778696ccf37b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** extract domain logic from SQL to domain model ([008fe5c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/008fe5c19bbbb35c248fb02498c52843e85254a6)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** pass context to the Contract Distribution Service ([dca3dab](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dca3dab8a9f663b79a2ea8d9303be8d4db4cfb80)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** remove Get* prefix from getters ([d10b1d6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d10b1d6cb900fa41a417d5f1d9f3ec7c1e59054e)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** rename Contract Distribution properties ([e506f41](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e506f41a434ac8d14bc564fb87b4c2232a06c9d2)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** rename contract distributions table to failed_distributions ([1f4ecf0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f4ecf007b76ff70b4659b07c34e9c836545bb88)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** rename ContractDistribution -> FailedContractDistribution repository ([7f04b3b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7f04b3bb291a7fbcfe77e84a450a7ed44269bd7f)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** rename SaveContractDistribution -> UpsertContractDistribution ([3b94773](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3b947730b8f24ba3f7e137d064d28fcea968d691)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** replace SQL like with null string for retrieving distributions ([054c4e6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/054c4e6f1964b883add105ae3b4f2de0e3aee5bd)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** use errors package for joining errors ([be5c73b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/be5c73b0631214bcf440dd4319f3ce2a8177e1a5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* use public key thumbprint in contracts ([3a51187](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3a51187b87f86e1160df007a249cd5b471a45d2d)), closes [fsc-nlx#229](https://gitlab.com/commonground/fsc-nlx/issues/229)


### Styles

* **manager:** apply code style suggestions from review feedback ([f7f25dc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f7f25dc026cb3b9a0d59d069d2be90a9e7d114ee)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)


### Tests

* **manager:** add test name to logger context ([86dc274](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86dc274737af044f50f69b5b80cb63521f568322)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** fix tests ([abc8779](https://gitlab.com/commonground/nlx/fsc-nlx/commit/abc8779e3ba0a253fc13c65f6e9921b3a9a33d7d)), closes [fsc-nlx#217](https://gitlab.com/commonground/fsc-nlx/issues/217)
* **manager:** improve setup of flaky unit test ([81ada52](https://gitlab.com/commonground/nlx/fsc-nlx/commit/81ada52501fe3f77a19d50544120160b5aa80798)), closes [fsc-nlx#231](https://gitlab.com/commonground/fsc-nlx/issues/231)
* **manager:** improve setup of remaining flaky unit tests ([60eccfe](https://gitlab.com/commonground/nlx/fsc-nlx/commit/60eccfedd7fc6aec1dda9697e5e8af26b0f97413)), closes [fsc-nlx#231](https://gitlab.com/commonground/fsc-nlx/issues/231)
* **manager:** prevent nil pointer exceptions by terminating test ([56a11ec](https://gitlab.com/commonground/nlx/fsc-nlx/commit/56a11ec84d61509ed4e3499c57cc43bdbca668e3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** replace discardlogger with zaplogger for tests ([bb0453e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bb0453e4014bab9685e9c593e69c1d83868af995)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [0.16.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.15.0...v0.16.0) (2024-02-20)


### Build System

* **controller:** update package-lock.json ([831b159](https://gitlab.com/commonground/nlx/fsc-nlx/commit/831b1597648694f2945c33de954422e31d46acf3)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* generate mocks for iv generator instead of id generator ([7107842](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7107842a89b196f135cee7a0fcd805e5f7a4ce27)), closes [fsc-nlx#199](https://gitlab.com/commonground/fsc-nlx/issues/199)
* update dependency @commitlint/config-conventional to v18.6.2 ([a0d43b7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a0d43b7c23a0005cc0510f4399aab65a199b6462)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.1 ([aa54d38](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aa54d38beb2f9af1bdd3e6593ed6ca45172395a8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.2 ([f93c023](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f93c0236456dec9c28bdd8325504e456d18c2840)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.14.1 ([2de9e44](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2de9e44662743026df1215f8d3bad4b2ea2eee07)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update glauth/glauth docker tag to v2.3.2 ([265c14c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/265c14ca0999acac8812cfd45a6d667d446a4da4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/go-chi/chi/v5 to v5.0.12 ([dc6440b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dc6440b05483b11e1bb14fc67d8508999f9f9120)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/lestrrat-go/jwx/v2 to v2.0.20 ([d069e6f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d069e6f02a4005851635fdc4a4a7fe7c8e17b33c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.11.1 ([20bfd47](https://gitlab.com/commonground/nlx/fsc-nlx/commit/20bfd479c16a45b93eb01b59360edd9a69843620)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v7.0.2 ([07eead1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/07eead13f05108a84a5d3135e6253f7558ba6f47)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* rename docker-compose file to default naming ([8fd35cf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8fd35cfcdabf194e6e2a12415bc6698c7650d7c9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Features

* **controller:** add Access section to Directory Service Detail page ([39717ae](https://gitlab.com/commonground/nlx/fsc-nlx/commit/39717ae54e946e6b2d0510b4295f740740e7603f)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* **controller:** implement Directory Service Detail page ([413afdf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/413afdf171379f5358c720b4402c9978bb8ef1a1)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* **controller:** introduced the ability for retry sending contracts to peers ([bce0512](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bce05120acdb681538f4d7ad4fc2ca0781c00305)), closes [fsc-nlx#179](https://gitlab.com/commonground/fsc-nlx/issues/179)
* **manager:** add contract distribution retry endpoint ([3ed9afd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3ed9afd3254ba1b57dd4dd46f8ad64c72e88f50d)), closes [fsc-nlx#179](https://gitlab.com/commonground/fsc-nlx/issues/179)
* **manager:** return link header after receiving contract distribution retry ([4708976](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4708976e8cb7f5166e0b1ae91c62d8b6c12276c7)), closes [fsc-nlx#179](https://gitlab.com/commonground/fsc-nlx/issues/179)


### Bug Fixes

* **controller:** show correct Peer info for the Directory overview page ([885fed0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/885fed07f42d204a72e0c131887ffa39ea1770a6)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)


### Code Refactoring

* **controller:** improve readability of the Directory Service Detail page ([c4cac00](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c4cac004f48ab661eb55500888c998b4f1f54549)), closes [fsc-nlx#202](https://gitlab.com/commonground/fsc-nlx/issues/202)
* **manager:** removed http_status_code from contract distribution ([4351117](https://gitlab.com/commonground/nlx/fsc-nlx/commit/43511177a1f8e148682eb72f0cb05ace4ac2e7a7)), closes [fsc-nlx#179](https://gitlab.com/commonground/fsc-nlx/issues/179)
* rename content ID to content IV ([3bb1dcf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3bb1dcf2df8221c20d7f047fc64de73df06412f7)), closes [fsc-nlx#199](https://gitlab.com/commonground/fsc-nlx/issues/199)

## [0.15.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.14.0...v0.15.0) (2024-02-14)


### Build System

* update commitlint monorepo to v18.6.1 ([bd62b0a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bd62b0a2229986eec93a6906eb373b33ddfbbfeb)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @axe-core/webdriverjs to v4.8.5 ([f2ae0e1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f2ae0e199b3731d8722bf777a0a08e59ae152450)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.4.0 ([4299d97](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4299d97d4ce48acc29819022f07bfd4d8f8447d0)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.16 ([306dc0f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/306dc0f7c1d27fa8bdd9e96cd4de594f1b74ce8f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.17 ([0835991](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0835991205e4193ac2b4fdeccffdff5a5efb9b39)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency prettier to v3.2.5 ([b57c359](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b57c359be95a9487e79be2cdc5d74f54c7b04c31)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23.0.1 ([a7fbae2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a7fbae2d7cf0c9ae206c980bbab9cbea8d0acc72)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23.0.2 ([f67b2fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f67b2fc214ffb2064e620c89258129b905e52443)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docker.io/curlimages/curl docker tag to v8.6.0 ([f543d81](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f543d8164cb5152f6088ce2001e36fed4a9b35b4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update glauth/glauth docker tag to v2.3.1 ([e7e048d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e7e048d8888c84c2cb5c77c99e6346f8a3ed622a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.22.0 ([5cba424](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5cba424939b292d59cc08be2ec9f73e9cf81743e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang.org/x/exp digest to 2c58cdc ([0d4342e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0d4342ec4b157b9c1feb36eb9a0102bd7594dbaf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang.org/x/exp digest to ec58324 ([c505105](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c5051053ed9009a09427d8854840dbd40eb7dd88)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/getkin/kin-openapi to v0.123.0 ([64b0fe6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/64b0fe60e532f9773f0423abec3c8bee22378007)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/google/uuid to v1.6.0 ([aa510cd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aa510cddc1c76b2b228a7789b5498c9fe9d5f949)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.19.0 ([b3b9998](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b3b9998533a98760ef12ac3a9c5c1923954d1e43)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.21.0 ([5f60b07](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5f60b07a6b5828c03e9d203b765afb4995ca038b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/oauth2 to v0.17.0 ([a869fb4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a869fb43255e85b6499e75bb5c19b4e955151bdc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update openpolicyagent/opa docker tag to v0.61.0 ([80b8133](https://gitlab.com/commonground/nlx/fsc-nlx/commit/80b8133a38e7a6cd355a292fab4c9b9a80f17e4f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update postgres docker tag to v16.2 ([3bd4ef8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3bd4ef87c817421ad21d8e90c79b06366bc077f5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update selenium/standalone-chrome docker tag to v4.17.0 ([1003289](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1003289d505c2f2e65eac77c16de1f2a84101d33)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v6.20.0 ([b4b56a8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b4b56a82734662768b3ef9cdad8a92d965effe26)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v6.21.0 ([49a0a3e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/49a0a3ed6a060765ec8bca55281c495fda9e4b39)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v7 ([4e3697c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4e3697c5bdb2cc94a3f4a32ac9e13567890fd560)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* use correct mock output path for authorization mocks ([c1135ed](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c1135ed7deed52a95c0c7468d2c9b8f878a05737)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* add verbose flag to golangci-lint so we can investigate why its slow ([7d25a2d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7d25a2d9b5ae1d24ab8465bbea44d2017fde2f6a)), closes [fsc-nlx#196](https://gitlab.com/commonground/fsc-nlx/issues/196)
* export CHART_DIRECTORY so it can be accessed by the Helm post-render hook ([4ef653d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4ef653daf125cd815aea9a5855e2e1f4a37739c9)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** add glauth helm chart to deployment pipeline ([a27b17d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a27b17dccba1eec67e6642d18a7e3a41e10ea85e)), closes [fsc-nlx#161](https://gitlab.com/commonground/fsc-nlx/issues/161)
* **helm:** add kustomize post render for glauth chart ([bbcae9d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bbcae9d05a557c1bf5eb25822b0f4130107fda4b)), closes [fsc-nlx#161](https://gitlab.com/commonground/fsc-nlx/issues/161)
* **helm:** added storage to glauth values ([62ac07d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/62ac07d266ebd38a509d4060d8da5c12529d3dbb)), closes [fsc-nlx#161](https://gitlab.com/commonground/fsc-nlx/issues/161)
* **helm:** change service type of the glauth ldap deployment ([da45546](https://gitlab.com/commonground/nlx/fsc-nlx/commit/da4554627843c5c105e5de990e2c9baaa652c6ac)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** disable dex for Gemeente Stijns, RvRD and Vergunningssoftware ([ea50530](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ea50530dcbb4d9fca6f653b91d937af3278337bd)), closes [fsc-nlx#161](https://gitlab.com/commonground/fsc-nlx/issues/161) [fsc-nlx#197](https://gitlab.com/commonground/fsc-nlx/issues/197) [fsc-nlx#200](https://gitlab.com/commonground/fsc-nlx/issues/200)
* **helm:** fix create service job error check ([b29d536](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b29d536fcbccc7c50a7b469f416d8d8107ea198e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** local copy of glauth helm chart to change config ([2b8c6f9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2b8c6f99c96229cee96b1d4df3fdb3e4e40cef06)), closes [fsc-nlx#161](https://gitlab.com/commonground/fsc-nlx/issues/161)
* **helm:** remove unused value directoryManagerAddress from the Controller chart ([205126a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/205126a6b462bf337420b0016821f5bf61cc8665)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* introduce cache for the golangci-lint job ([d9d3af9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d9d3af9db7ec98c34d3adb6827f9945f20169419)), closes [fsc-nlx#196](https://gitlab.com/commonground/fsc-nlx/issues/196)
* kill leftover debug processes on development shutdown ([258c5f5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/258c5f574001d6304c6d777c1fcf43ef137109ac)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golangci-lint v1.54 -> v1.55 ([2d1cae0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2d1cae01351845fdea0338b2335139cc4e1bd75d)), closes [fsc-nlx#196](https://gitlab.com/commonground/fsc-nlx/issues/196)


### Documentation

* **controller:** improve description of the cursor of internal GET /contracts ([47d68f8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/47d68f86d286dfe8537ccf4733753bb813a8979b)), closes [fsc-nlx#193](https://gitlab.com/commonground/fsc-nlx/issues/193)


### Features

* add authorization capabilities to controller ([ecd610a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ecd610a66e01c059424157015bc180be2fb52226)), closes [fsc-nlx#161](https://gitlab.com/commonground/fsc-nlx/issues/161)
* **controller:** add Publish Service form ([9a1a83c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9a1a83ca1f35d0a0fb4793b8763724ae017c2866)), closes [fsc-nlx#186](https://gitlab.com/commonground/fsc-nlx/issues/186)
* **controller:** display date time in local timezone and localize format ([34a3f33](https://gitlab.com/commonground/nlx/fsc-nlx/commit/34a3f333198a0822d8920e3955c8c781da49eb8e)), closes [fsc-nlx#189](https://gitlab.com/commonground/fsc-nlx/issues/189)
* **controller:** display message if no contracts are available for a service ([c9dccd1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9dccd18f2be0ce69500a321420f4a87874bccf8)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **controller:** move Edit button for a Service to an actions section ([67fc9f1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/67fc9f1f41bbe6498ecb4a6a30705da01aca964b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **controller:** scroll to top of page when submitting form ([c346f4e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c346f4e5ccac3f16c4c42710b627ef6d8bfc796e)), closes [fsc-nlx#188](https://gitlab.com/commonground/fsc-nlx/issues/188)
* **manager:** add contract distribution endpoint to the internal api ([dea1453](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dea1453bdefd6a866df16fe0c9584961ba6de4de)), closes [fsc-nlx#179](https://gitlab.com/commonground/fsc-nlx/issues/179)
* **manager:** add table and upsert query for contract resend functionality ([a261b16](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a261b1646fa5815c3c6ef48321f996de134a00b4)), closes [fsc-nlx#179](https://gitlab.com/commonground/fsc-nlx/issues/179)
* **manager:** int GET /contracts with Grant type filter sorted by created_at, hash ([e46d728](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e46d72869d69d2e727cdbfaafaa74fd71a172808)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)


### Bug Fixes

* **controller:** add fsc-admin group to controller API ([e549648](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e5496485b8d9681906ef3f316891fe6c22393548)), closes [fsc-nlx#161](https://gitlab.com/commonground/fsc-nlx/issues/161)
* **controller:** add missing translations for the Contracts page ([9f7d670](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9f7d6709fb856483e6b9f53bc3604a6b6aeb7935)), closes [fsc-nlx#188](https://gitlab.com/commonground/fsc-nlx/issues/188)
* **controller:** ensure sorting of contracts for internal GET /contracts is stable ([76b10dd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/76b10ddd9d15f2119c022606907bdea2b212ccbe)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **controller:** ensure the eye icon uses the current color as fill ([229d111](https://gitlab.com/commonground/nlx/fsc-nlx/commit/229d1116b3c2b1c58ab5f144afacfbbd011b2f94)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **controller:** fix pipeline issues ([4e62050](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4e62050cc193be0d96563ed53d9499ef8dbb9fe9)), closes [fsc-nlx#161](https://gitlab.com/commonground/fsc-nlx/issues/161)
* **controller:** fixed all linter issues ([5458891](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5458891cabaf8dd970f90e720de7c304b1c3ee4a)), closes [fsc-nlx#161](https://gitlab.com/commonground/fsc-nlx/issues/161)
* **controller:** prevent duplicate error type prefixes in the feedback ([ab52207](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ab52207b3a84155c82ce3137c52fa6def99e9f2b)), closes [fsc-nlx#186](https://gitlab.com/commonground/fsc-nlx/issues/186)
* **controller:** provide peerID to ListServices call ([3230d92](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3230d924dcbaa631479e81de73f5040543db8273))
* **controller:** rename incorrect class name for the collapsible icon ([def514f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/def514f9e8ec7f913098fa94d985877cb86493ee)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **controller:** write log before rendering page ([ecab2a1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ecab2a10e4be13f6bbab3b84f3a343b4d22b7cad)), closes [fsc-nlx#186](https://gitlab.com/commonground/fsc-nlx/issues/186)
* **helm:** corrected log_truncate_on_rotation setting for Postgres ([d09b029](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d09b0297a951a71f989e2c96e2011f97d51963c3)), closes [fsc-nlx#198](https://gitlab.com/commonground/fsc-nlx/issues/198)


### Code Refactoring

* **controller:** remove hashmap from Service detail page ([a17a0eb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a17a0ebecc4360ac8271ae47c291f30b783de66a)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **controller:** remove unused ListAllContracts repository method ([441cbe1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/441cbe1079d442f2c6f0f52cc80123bd23110b6d)), closes [fsc-nlx#193](https://gitlab.com/commonground/fsc-nlx/issues/193)
* **controller:** rework int GET /contracts handler to reuse repo method to fetch contracts ([4a26daf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4a26dafb28783406011ca8d1ceee89d024fe77e2)), closes [fsc-nlx#193](https://gitlab.com/commonground/fsc-nlx/issues/193)
* **manager:** extract decoding of the pagination cursor to a function ([46f072c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/46f072c0c216a9368d50880f09696309359b5db6)), closes [fsc-nlx#193](https://gitlab.com/commonground/fsc-nlx/issues/193)
* **manager:** remove unused methods ([5afa5dc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5afa5dc03eafc3c1ea9cc064e08d2359f0f8aede)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)


### Tests

* **manager:** improve clarification of comment ([be0e664](https://gitlab.com/commonground/nlx/fsc-nlx/commit/be0e6640a9cb76991913da589143112d4fa0c197)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)

## [0.14.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.13.4...v0.14.0) (2024-02-01)


### Build System

* **controller:** define GET /service/{name} in OAS ([f9699ae](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f9699ae826270b0208e8ee8d06b6120b436c40d6)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* fix sqlc command for Earthly ([e57aa8b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e57aa8ba8699f3224cfd872bb8b141d8711a87ce)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* generate client code from controller authz OAS ([2794933](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2794933101560fb95d50a8115d63d2bbf41ffb64)), closes [fsc-nlx#160](https://gitlab.com/commonground/fsc-nlx/issues/160)
* introduce testing certificates for the Directory Peer ([22f6093](https://gitlab.com/commonground/nlx/fsc-nlx/commit/22f6093df998db259bd314b8dfbd778b538b2204)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** ensure files with build tag 'integration' are linted too ([51326c0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/51326c07f9b24cf7e6871f9267c4a85c03c48369)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* move regeneration of static assets from startup script to Modd ([d5f7915](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d5f79156c4525c8004577733b5203c3ccfacb4a7)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* push acc-latest tag to container registry ([803762a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/803762a255bedf06bf28521cff87438f88f17342)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* regenerate mocks with latest version of Mockery ([0f658ed](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0f658eddbdd0d398eeaedd1e814bc913b5f77bde)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* regenerate Sqlc files with latest Sqlc version ([5d57dab](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5d57dab10b0f5ecdf077f02cbfbd19fea964818c)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* regenerate testing certificates ([d99217b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d99217bf2f60d483556a10ea5dfd6f07eaf26407)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* split configuration of modd file into separate sections ([806ecf4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/806ecf4698b84c075abe21c64a4a8e7cd9c9b50e)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* update alpine docker tag to v3.19.1 ([206e9cd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/206e9cd0aa9a289fc777b2f884f0830a3f9f2176)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update commitlint monorepo to v18.4.4 ([5d57381](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5d57381477fcb193ce3da91a53671a1d4e186a25)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update commitlint monorepo to v18.6.0 ([4ceca1f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4ceca1fb7a46c1d6056acb2ab2aa3a24939d284d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @axe-core/webdriverjs to v4.8.3 ([6f8c804](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6f8c80407fb6cba913da1a0fe4a3909cd5677a1f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @axe-core/webdriverjs to v4.8.4 ([9de640a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9de640acb93fb7441397bfda3f00c44984ab0fc8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @cucumber/cucumber to v10.2.1 ([f85579a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f85579ae6c4f5097e8aa58d7479e21542dbeb3a5)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @cucumber/cucumber to v10.3.0 ([b586006](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b5860060081659e591dffecc716e004c763fb1a3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @cucumber/cucumber to v10.3.1 ([c3135e4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c3135e495579d0e2500cd260d00b6ff91898305e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @semantic-release/gitlab to v13 ([5dfb36b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5dfb36bc7a5d1d1bf826720f925e74fb89ea266e)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @semantic-release/gitlab to v13.0.2 ([c70f8ad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c70f8ad2c3a361d3e85ebdfca215562fa89593c3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.3.102 ([8411933](https://gitlab.com/commonground/nlx/fsc-nlx/commit/84119334497abf33e9cb2d30697c60dbc84abdce)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @swc/core to v1.3.103 ([bf6f102](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bf6f102df6ea3967d84d143ae51b45e30551b9b6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.3.104 ([3017f9d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3017f9d9be555c06116db69936228f6f9449a42a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.3.105 ([3867c84](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3867c84e5ff09a38da8df77872ea38750b876dbf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @swc/core to v1.3.107 ([da93e18](https://gitlab.com/commonground/nlx/fsc-nlx/commit/da93e183697a9b5addae2086b577ad71635bcd0f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.10.6 ([e04cb4d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e04cb4dfb57d0548c4c68e6bfd5063d7728117a5)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v20.10.8 ([50abc8c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/50abc8c928e351b2909d6198ad457cfad165da56)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.10 ([6e17766](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6e17766f492682565f7a33accdd62d2422df05f6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.13 ([09c6da0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/09c6da019491ce1bbf06343a2bdfcd96ab6501ca)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.14 ([1b0d23a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1b0d23aa09e738eeace236f04c09b66af4a6580b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.4 ([4357506](https://gitlab.com/commonground/nlx/fsc-nlx/commit/435750657426aa9e6d7865320ab48e9e70b44395)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency @types/node to v20.11.5 ([e22bd0a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e22bd0abbf846d610c106c207d9169707cf28bcf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency eslint-plugin-prettier to v5.1.2 ([09aadbb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/09aadbb8aa417f70fbd7099f3f677d42476c12d5)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-prettier to v5.1.3 ([af59bc3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/af59bc3ed7cf8dfcf5e1a63a8039f386f0b0f42a)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.13.3 ([245dc36](https://gitlab.com/commonground/nlx/fsc-nlx/commit/245dc369ad1ec23da89c2c63a92c9c15a3791f2b)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency helm/helm to v3.14.0 ([716e21b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/716e21b99d97c5744e6d42b5c5c3389a62e8b31c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency htmx.org to v1.9.10 ([9394ed7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9394ed774609ca4fe2796d3cc4b47a7920551d2e)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency prettier to v3.2.1 ([6a21e33](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6a21e33de953d41baff402b2c439131b7ec79fca)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency prettier to v3.2.2 ([e26462d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e26462d1b34fe954cab1e660024599e913cbb114)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency prettier to v3.2.4 ([8bfdfda](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8bfdfda02c7d239f0da70d4b13e52076ca65c641)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency replace-in-file to v7.1.0 ([e5a9bbe](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e5a9bbe77315d87595075a5a768567ed91da0fa9)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency selenium-webdriver to v4.17.0 ([4dd4ecf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4dd4ecf53337f92a87ecd95a804e82e6a7f5b97c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dependency semantic-release to v23 ([bb692b4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bb692b4e6ea0fc1bc973fcabe47e4cd2e256fcb3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update dexidp/dex docker tag to v2.38.0 ([6dd6930](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6dd69306d0db1ea8f5157ef430c64f031aec071c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docusaurus monorepo to v3.1.0 ([6f32dd5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6f32dd58fd88439f08934864cac99d9ec30fd011)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update docusaurus monorepo to v3.1.1 ([11c2092](https://gitlab.com/commonground/nlx/fsc-nlx/commit/11c209255811f7c7971d484ad1ac616206ee61a8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.21.6 ([16c6c84](https://gitlab.com/commonground/nlx/fsc-nlx/commit/16c6c845924ee2e6ed4caee51bdb7964759d3295)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang.org/x/exp digest to 02704c9 ([f368289](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f3682890439aecc63f68ce9528b08f6074211ad2)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to 0dcbfd6 ([d133883](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d13388390d3c9071023f504e6a77b89a5a1567af)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang.org/x/exp digest to 1b97071 ([02e4694](https://gitlab.com/commonground/nlx/fsc-nlx/commit/02e4694d8d5d6885789eb3ba1ac6da2e1c899e9e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang.org/x/exp digest to be819d1 ([d9fe44c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d9fe44cee3620a7738eb915baff27ae57ab7aea1)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to db7319d ([f1b18a6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f1b18a6fce6a08f67c69d4e32f49a510b66166dd)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update helm release dex to v0.16.0 ([0b28db2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0b28db2a341fd3f95d0bf7a1d92fdcb930d36ed4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/lestrrat-go/jwx/v2 to v2.0.19 ([0fdac4b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0fdac4b8bb79b52ec22268d0efab23465bb2b8e1)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/oapi-codegen/runtime to v1.1.1 ([1a7053e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1a7053ea5b70827adc188d935b3667cdec4afdb6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/crypto to v0.18.0 ([cdbe0da](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cdbe0da8472dd21d13fafbdd4ed293de6ed54d81)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/net to v0.20.0 ([e81e184](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e81e18405951dc3587d01d8d02bfa1be60ce4b2d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/oauth2 to v0.16.0 ([3ab3566](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3ab356602cbbc36fdac6aa2120c43fda293fd360)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module golang.org/x/sync to v0.6.0 ([f0a2d1c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f0a2d1c74bbcbba2abb5337a0255ae8f14e86779)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node.js to v20.11.0 ([3731a7b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3731a7b3f6ead24c11390dd0226553046606d570)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update parcel monorepo to v2.11.0 ([500a81d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/500a81d4ea939ca06c1ff87395f19792bdd47417)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v6.16.0 ([a64509f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a64509f27d467861101738e639b62951385e12d6)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.17.0 ([ecce5f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ecce5f6f64a1c229c05595e979d47ba242775351)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.18.1 ([ed69304](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ed693046d0959ed28549acf9573db52b49ec4cd8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update typescript-eslint monorepo to v6.19.0 ([b8a14a5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b8a14a54b263572cd7e147320db5dab4a3a0753f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* add custom renovate bot manager for CI yaml files ([c9122b5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9122b5df8226eaa0ed8ea080bb006cc561b2b52)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** fix typo certificate-internal-unauthenticated value of the Manager chart ([a5de328](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a5de3282dc426d8dc4fd6b8a6dd1df120882188f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** use the same value name for the directory manager address in all charts ([a15a24a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a15a24ae170c414c371e156e3a6f0ca816563a98)), closes [fsc-nlx#148](https://gitlab.com/commonground/fsc-nlx/issues/148)
* perform security scanning on both the acc and production images ([018d449](https://gitlab.com/commonground/nlx/fsc-nlx/commit/018d449c4a2ca44fc79b16e0545994bfc79ef9e0)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* run review build and deployment only on review branches ([1f61adf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f61adf50c3da3bfc33a16a631bac3b11ab40957)), closes [fsc-nlx#154](https://gitlab.com/commonground/fsc-nlx/issues/154)


### Documentation

* **controller:** add authz interface ([2fc7709](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2fc770957559d9d14f0d82124a4cb42b1a88a73d)), closes [fsc-nlx#160](https://gitlab.com/commonground/fsc-nlx/issues/160) [fsc-nlx#160](https://gitlab.com/commonground/fsc-nlx/issues/160)
* define resource urn prefixes in authz OAS ([ce49f27](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ce49f275035307b2c968f26d45f333156ff5b815)), closes [fsc-nlx#160](https://gitlab.com/commonground/fsc-nlx/issues/160)


### Features

* **common:** add functionality for caching Certificate Revocation Lists ([1e2d688](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1e2d688fb6f2c57d1182baeaec5b2262a0fb9bcd)), closes [fsc-nlx#146](https://gitlab.com/commonground/fsc-nlx/issues/146)
* **common:** added check against CRL in Manager, Inway, Outway ([8e64407](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8e6440705ba92a299026ba5bbdb73c63c51c240c)), closes [fsc-nlx#146](https://gitlab.com/commonground/fsc-nlx/issues/146)
* **common:** added fetch CRL on Cache miss ([eb945aa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eb945aa376e0e2042740fb498b81e035fe95a3e8)), closes [fsc-nlx#146](https://gitlab.com/commonground/fsc-nlx/issues/146)
* **common:** use CRLDistributionPoints for CRL check ([b670355](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b670355b45a8ae6fa78c14e377d5c00aadf4bee9)), closes [fsc-nlx#146](https://gitlab.com/commonground/fsc-nlx/issues/146)
* **controller:** add check for valid contract before marking a service as published ([5b96cd4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5b96cd4352ae6320cd7741bba09787ca85d8f437)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **controller:** add colum to services page with info if service is published ([8422f5d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8422f5d859e076e4a498631e9ef1e4ef83ad13a8)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **controller:** add delete service functionality ([66a5234](https://gitlab.com/commonground/nlx/fsc-nlx/commit/66a52346dc66b448f66402c1af487cc672f84ec7)), closes [fsc-nlx#173](https://gitlab.com/commonground/fsc-nlx/issues/173)
* **controller:** add Endpoint URL and Inway address to the Service detail page ([95f1951](https://gitlab.com/commonground/nlx/fsc-nlx/commit/95f195187bc2362f146c67b1a690a827475fd14c)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** add GET /inways to the REST API ([d55033b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d55033bc79d78b96d18ed51c1e2a7038e7f91833)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** add GET /outways to the REST API ([d7ec6db](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d7ec6db4147bb22eecb079d37a603924dda9b215)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** add peer names to the contract overview and contract detail view ([b0c607b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b0c607bad4dc73007ea7a7139bf7b1a0c1fe0fae)), closes [fsc-nlx#172](https://gitlab.com/commonground/fsc-nlx/issues/172)
* **controller:** add servicename drop down in publication grants ([f833021](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f8330210e397007da962474e38fa23f2cce09173)), closes [fsc-nlx#162](https://gitlab.com/commonground/fsc-nlx/issues/162)
* **controller:** display related contracts on the Service detail page ([acd98ed](https://gitlab.com/commonground/nlx/fsc-nlx/commit/acd98ed3dc03deb5d9bd33ff70a8e8181ab2dce9)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** display username from token ([423fb93](https://gitlab.com/commonground/nlx/fsc-nlx/commit/423fb93f2e8ba71a0f7c1688d6402000f19623f1)), closes [fsc-nlx#164](https://gitlab.com/commonground/fsc-nlx/issues/164)
* **controller:** do not allow service endpoint URLs with a trailing slash ([2a3e7ff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2a3e7ffbd72f914fc8413832469a695b37f732ca)), closes [fsc-nlx#178](https://gitlab.com/commonground/fsc-nlx/issues/178)
* **controller:** fetch service details from API + add tests ([028510e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/028510e2ad3cb441b0335cec399fd7fb69b0a683)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** implement 404 page for the Service detail page ([d26e3eb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d26e3eb35b9933847441c2db6ca88cb3930b7b08)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** implement Service Detail page ([df1df4d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/df1df4db9330c791900345a59634d21396dc19cc)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **controller:** implement the edit service page ([b1200de](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b1200decf77b5d65b9d2b6286d29552f056f6e18)), closes [fsc-nlx#156](https://gitlab.com/commonground/fsc-nlx/issues/156)
* **controller:** implemented user dropdown menu ([c259648](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c2596485f585067aaea49304598930dc9c1a25c8)), closes [fsc-nlx#176](https://gitlab.com/commonground/fsc-nlx/issues/176)
* **controller:** make Inway address mandatory when adding a Service ([31434c9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/31434c901e78d1d691752295887ad294305eaeaa)), closes [fsc-nlx#177](https://gitlab.com/commonground/fsc-nlx/issues/177)
* **controller:** remove support for filtering by Inway when retrieving a service ([75141cf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/75141cf6d164da6ad82475c5c00329be4e538853)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** replace Service endpoint URL with Service name for the service dropdown ([86d663d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86d663d08f75ed8dc56f53374415309196633310)), closes [fsc-nlx#162](https://gitlab.com/commonground/fsc-nlx/issues/162)
* **controller:** show specific error when the name of a service is in use ([1527fa1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1527fa185994cdc6fdf3f897670ee1bf41dbaaeb)), closes [fsc-nlx#178](https://gitlab.com/commonground/fsc-nlx/issues/178)
* **controller:** translate column name ([90d9907](https://gitlab.com/commonground/nlx/fsc-nlx/commit/90d99078b1b355cb50a9f7fbcde019ad65d8bdac)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* extend authorization interface with request query parameters en method ([3739436](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3739436b7c733ff9c65e07912ae1b93901da9723)), closes [fsc-nlx#166](https://gitlab.com/commonground/fsc-nlx/issues/166)
* **manager:** add support for pagination limit of the internal GET /contracts ([92356cc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/92356cc3a429cb8e73d2f351e053c1e9e498a2a2)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **manager:** change default sorting order to desc ([1709496](https://gitlab.com/commonground/nlx/fsc-nlx/commit/17094960b8e45c1e5489fab65379ef0be1b4ff07)), closes [fsc-nlx#171](https://gitlab.com/commonground/fsc-nlx/issues/171)
* **manager:** check outgoing request against CRL ([e7e5406](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e7e5406f98471eb9df43ad43fad720b40fd65168)), closes [fsc-nlx#146](https://gitlab.com/commonground/fsc-nlx/issues/146)
* **manager:** ensure sort order of Peers is stable for internal GET /peers ([8cff38b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8cff38b6b5c54b167cff590387b749baac2b7fed)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **manager:** include contract state in the internal GET /contracts endpoint ([b0ba37a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b0ba37a1c886e4c815271f280597a43fd2a25ee7)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **manager:** remove redundant GET service-endpoint-url endpoint of the internal Manager ([471e303](https://gitlab.com/commonground/nlx/fsc-nlx/commit/471e3037c0780332c827bbd5277220217939bc46)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **manager:** sort external GET /contracts by created_at in descending order by default ([a871ead](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a871ead971717b861d2fe01d5aba1bda06bba845)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **manager:** update internal GET /contracts to order by created_at DESC by default ([3d1731b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3d1731b4445f9e4f5a167a45bd2182cc519847e3)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* use local peers cache for retrieving peer manager addresses ([aed7cb9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aed7cb9416f04406ee521282b3c5a576465d29a5)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)


### Bug Fixes

* **controller:** always display 'provided by' in directory ([02f5007](https://gitlab.com/commonground/nlx/fsc-nlx/commit/02f5007c4acd90a9487ea7caeb6a8d7c39fe6aba)), closes [fsc-nlx#170](https://gitlab.com/commonground/fsc-nlx/issues/170)
* **controller:** correctly display contracts in the service detail view ([1061cfc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1061cfc4fa4de6c987da54e26db2965865f34c1d)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* correct package name ([66ede1a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/66ede1aa788e6568978a1104d8c34ce59c1059f6))
* **manager:** ensure the manager address is returned from the Directory ([ec8063e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ec8063e6a97c3beb615cc531f708578424764b8c)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* re-enabled OPA for Inway and Outway AuthN ([d701694](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d7016943f5e18714255ccedaf0dda62eb45a75e8)), closes [fsc-nlx#167](https://gitlab.com/commonground/fsc-nlx/issues/167)


### Code Refactoring

* **common:** changed cache and validation for CRL ([0a0712b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0a0712bf314dfd8a74f3c1ddd9f64d27a2878551)), closes [fsc-nlx#146](https://gitlab.com/commonground/fsc-nlx/issues/146)
* **common:** delete expired CRL from cache ([d2f26c6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d2f26c6e38d5271185ef2ce5ff188fb91994879f)), closes [fsc-nlx#146](https://gitlab.com/commonground/fsc-nlx/issues/146)
* **controller:** extract server setup from the REST port ([7030684](https://gitlab.com/commonground/nlx/fsc-nlx/commit/70306843e40a1454b427fa752a5379d4b83c1d40)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** improve error handling for rendering the Service detail page ([cfba20e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cfba20e449a2d2b94767970735345824e9c1615d)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** remove redundant UI application ([622f7eb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/622f7eba055550dee5cfff16b31a9513046f94c3)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** rename filterValidContracts -> retrieveValidContracts ([b67be3a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b67be3a4ffc3b616e17d9cb6b74f8d4cede34ad8)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **controller:** rename name property to serviceName ([d4c21f5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d4c21f5e4279dd9294c282ff0f58d79b945156f6)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** retrieve Inway addresses via rest app instead of the ui app ([0bf9e7d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0bf9e7dd3171231e3f61072a9c9dff94a9f76f14)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** retrieve Outway Certificate Thumbprints via rest app instead of the ui app ([f705b4f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f705b4fe64bff33d8e579fe7770dadfc33733362)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **controller:** use clock passed to handler to verify validity of contract ([3157ba9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3157ba95aa568ce11f5fef60143419b95d875b06)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **controller:** use Contract state from the internal Manager ([f9cc979](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f9cc97955385d3db2a7cfc6e39d018e5f58cbbd1)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **controller:** use interface as return type instead of struct ([27f944b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/27f944bc0b52501ef082a24461afdabba8db8c62)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **manager:** extract retrieval of contracts by grant type in function ([c3682da](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c3682da237013dd7e15553108798422ba81b4b20)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **manager:** introduce service to create test contracts in the tests ([d009769](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d009769bffbc8988565b19a4d34c78b71f034568)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **manager:** move contract generation to the contract service ([5604a3d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5604a3d545b8b129526535e8e634fa5b2056cc1e)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **manager:** move the announcement of your organization from the serve to the application ([7bea4a8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7bea4a89c0fc1f7ae770f4e717a73e352c37284e)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** remove duplication in peer communication service ([0740a8a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0740a8ac4f562f572fa102f1eb62f30a9e16a51e)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** rename actions -> endpoints ([57ff64b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/57ff64badb7ae8d2f449a7819fa612271de0085b)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** rework ListContract arguments ([89d5419](https://gitlab.com/commonground/nlx/fsc-nlx/commit/89d5419f2a78a16135d5f39edb05416f7904591e)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* refactor logic for the CRL cache ([000bd5f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/000bd5f7fef292a1341e3461cef81916713afdd0)), closes [fsc-nlx#146](https://gitlab.com/commonground/fsc-nlx/issues/146)
* replace internal/peers with PeersCommunicationService ([d7c405b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d7c405b5e876cd548c992a40116b97a0ec85302e)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)


### Styles

* **controller:** remove redundant type property from oas ([a77adda](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a77adda304efd1b5948a69a1b201395ada188515)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **controller:** rename files for consistency ([988d5dc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/988d5dc3b8b48b73e96ccf328e915aca4f44a598)), closes [fsc-nlx#155](https://gitlab.com/commonground/fsc-nlx/issues/155)
* **manager:** fix linting ([7b697ee](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7b697eeec03433a163a313eece72e875150e2aa0)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)


### Tests

* add a Certificate Revokation List(CRL) with a validity of 20 years ([5f01f5d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5f01f5d029b952d904ce5ce1b8cd3ecd702f8ac7)), closes [fsc-nlx#146](https://gitlab.com/commonground/fsc-nlx/issues/146)
* **controller:** rename tc -> tt to be consistent with other test files ([079678d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/079678d484aa3dd86facebb88007bdf9327df6e9)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* extend assertions so we get more specific feedback ([7e00fa2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7e00fa26eb48d0b2603590e5e78410f0818480f3)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **inway:** remove unnecessary test ([df0a9b6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/df0a9b6a10b8b20ae42ecfc5011018e772797ba5)), closes [#142](https://gitlab.com/commonground/nlx/fsc-nlx/issues/142)
* make tests pass and rename organization to Peer ([28d2c16](https://gitlab.com/commonground/nlx/fsc-nlx/commit/28d2c165a989274e642cb9645e2879ce75cf34c3)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** add test for Contract order of internal GET /contracts ([777e1a9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/777e1a916c11d3ad62dadc4603368e35e157ee6a)), closes [fsc-nlx#169](https://gitlab.com/commonground/fsc-nlx/issues/169)
* **manager:** add test to verify behaviour when calling Announce to other Peers ([dec5e99](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dec5e9934a22fed84d12c67d84d1ec8e0fa078be)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** add tests for the contract states of the internal GET /contracts ([1f9bce6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f9bce617dbbf0249e33102aad0cfbd4d8841ebb)), closes [fsc-nlx#150](https://gitlab.com/commonground/fsc-nlx/issues/150)
* **manager:** assert the external Manager stores its own Peer info ([1795f3a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1795f3a64cb47b61a85c0d54b136b893db95b8d8)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** ensure database names for the int and ext tests dont collide ([1d756ef](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1d756efdba1bcb490e183faf7fc85c6bae5ee138)), closes [nlx-fsc#32](https://gitlab.com/commonground/nlx-fsc/issues/32)
* **manager:** fix tests ([e32b930](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e32b930f9778df000931ad08dd2112988dc0e40f)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** generate mock for the ManagerFactory and Manager interface ([f297e93](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f297e934ed606733d8d23d495a1271a886550946)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** rename Organization -> Peer ([5c69458](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5c69458bb1dc1f1a138f56dbd150b38847333028)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **manager:** reuse existing cert bundles ([c9a85fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9a85fc0c655383899f2c359d540d7a957499d6e)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* replace custom Directory setup with dedicated Directory Organization certificates ([aa37e2d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aa37e2d7b19f8a95ec9c2d1e5f5c24847938d5eb)), closes [fsc-nlx#142](https://gitlab.com/commonground/fsc-nlx/issues/142)
* **txlog:** fix flaky tests by using unique service names ([3dfa4c7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3dfa4c7f65950ae662c91c640c345880ca5bc924)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [0.13.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.13.3...v0.13.4) (2023-12-22)


### Build System

* update dependency @cucumber/cucumber to v10.1.0 ([965e26c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/965e26c839014091c436f515bcefedfa8cfe2696)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @cucumber/html-formatter to v21.2.0 ([a3dee04](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a3dee04ae56045435a6320b926dd59810c5adbb7)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-prettier to v5.1.1 ([a58e3e6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a58e3e666a2e8e52a7228e96d30c147c1f2149d2)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)


### Continuous Integration

* provide correct image tag names to Kaniko ([ac73c84](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ac73c84df7af62167159e4ce83384855aa7c48b7)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)

## [0.13.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.13.2...v0.13.3) (2023-12-21)


### Continuous Integration

* update dockerhub repository url for login ([fb7c41f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fb7c41f0255b08ca7d508b7f0b965cfe218c8fbb)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)

## [0.13.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.13.1...v0.13.2) (2023-12-21)


### Continuous Integration

* use correct ci/cd variables when injecting dockerhub credentials ([ab90ba3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ab90ba38b02add7b0ea8941cd2358dd4ab2a5f20)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)

## [0.13.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.13.0...v0.13.1) (2023-12-21)


### Continuous Integration

* inject dockerhub credentials in production build step ([c5cf8c1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c5cf8c11b94ba7f8dc25eede47961c26f5384938)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)
* prevent success comments on gitlab by semantic release ([4d61f91](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4d61f9105a89e40a2132e32842ea040aa28a5d48)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)

## [0.13.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.12.0...v0.13.0) (2023-12-21)


### Build System

* **controller:** add internal REST API to Earthly configuration + regenerate ([8eb8f64](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8eb8f646253da3e063c8c073b2b18caf1afb66bf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **docs:** upgrade React to v18 ([65eb0ee](https://gitlab.com/commonground/nlx/fsc-nlx/commit/65eb0ee210c98032e3ea7358224910b48763ee17)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **docs:** use non privileged nginx image ([9d27f0f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9d27f0f031dcfdb90b22a5ab23d740dd321745cc)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)
* regenerate REST API models using OpenAPI generator v2 ([5e69a54](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5e69a54b36cbeac5c9591934a7b9df0e6939d8be)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* tidy and update Go dependencies ([8a37890](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8a37890689a843f729c13babde193969e2f67dd4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* tidy Go module ([ec05047](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ec050471a18e1589ac7c5c7ad4b7effc7733918f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update alpine docker tag to v3.19.0 ([b7013bd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7013bd95abd29c97a882daaeb5f62eed18a4296)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @axe-core/webdriverjs to v4.8.2 ([9457246](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9457246535bfd027b6ad89feae5cdf7858d16af8)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @cucumber/cucumber to v10 ([8f2d125](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8f2d125206c331ae81ae95750870aafcfb7ecbef)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @cucumber/html-formatter to v21 ([feda082](https://gitlab.com/commonground/nlx/fsc-nlx/commit/feda08278c5232cb2e0650010f1cc0d387599288)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @cucumber/html-formatter to v21.1.0 ([e4bcfe6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e4bcfe6efbafe18082f27f592197f95edde03344)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @swc/core to v1.3.101 ([b0a1cdb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b0a1cdb278d913bd62fd8f552b1124455c049a43)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v18.19.0 ([a1a0264](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a1a0264f78ed94c2fedb4f34775775ddd66bb2b1)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v18.19.1 ([2d5f3d0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2d5f3d063d53e4e1f7536149707e0de077a0b2af)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v20.10.3 ([21b8142](https://gitlab.com/commonground/nlx/fsc-nlx/commit/21b8142af1c58ae320bab094b57bd04d037b7644)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v20.10.4 ([5706816](https://gitlab.com/commonground/nlx/fsc-nlx/commit/570681640eed5be3829a53dbae89d745c4628f9b)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v20.10.5 ([c225c03](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c225c038d7722af9fdd906357d5a302398cb474c)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint to v8.55.0 ([4f7d4e0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4f7d4e0632d87ce7b3e17f720a772f29a6adc39d)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint to v8.56.0 ([9e74f61](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e74f61e2eab01c93803c4fa0f6e180350f510ec)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-config-prettier to v9.1.0 ([2b80c31](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2b80c31f67a8b123f2e86bccec1c3eb216148f6f)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-import to v2.29.1 ([53bfd54](https://gitlab.com/commonground/nlx/fsc-nlx/commit/53bfd541d72c744fed3d3af99139aaf8bbb799bb)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-prettier to v5.1.0 ([4f19fad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4f19fad65076b33702ba0603b2bb51392d3b5c74)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency prettier to v3.1.1 ([3b17dd3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3b17dd3e55a4bcadb6cb7956aaf308da55acdf01)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency selenium-webdriver to v4.16.0 ([5415d96](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5415d9609330fdbf7b0d1145f5992685262858fc)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency semantic-release to v22.0.10 ([fbcac82](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fbcac821cf2608eb0a207ec415c3d307b88decd1)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency semantic-release to v22.0.12 ([fe60fc8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fe60fc89356e3a9396310f8e316f7d651cc4805b)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency semantic-release to v22.0.9 ([2b4b8ab](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2b4b8ab8bce070564aeeb0a7189101bec963a2c3)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency ts-node to v10.9.2 ([9b2184b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9b2184babb51ff381f2159a3a225a3e3fcb94961)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency typescript to v5.3.3 ([d2de7f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d2de7f6b979f0e33ecb0f46895772d694a90b9e2)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docker.io/curlimages/curl docker tag to v8.5.0 ([196e560](https://gitlab.com/commonground/nlx/fsc-nlx/commit/196e56075dea7446f7666602c50a726ff293fffa)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docusaurus monorepo to v3 ([56023d6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/56023d6647c9a34998b497888f88433c8e081d2a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update Go dependencies ([efab1f4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/efab1f4417433df566b97c85c94148d08ffe7ff6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.21.5 ([883a393](https://gitlab.com/commonground/nlx/fsc-nlx/commit/883a393b7a624ee59983d173de2399727a2e5731)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to aacd6d4 ([a1150a4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a1150a45fd8b831d135aee66ca0bd86a3cc5ce0d)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to dc181d7 ([98f45c5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/98f45c510e958703b143048067040d0b2f441c57)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to f3f8817 ([f4584b3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f4584b327e3dd4359752d52dd7972f1822a10d07)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update Helm version for Semantic Release ([afb2b0d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/afb2b0d4f306228019c6ff6bff129bc44bdd4dea)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/coreos/go-oidc/v3 to v3.9.0 ([cb37e4f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cb37e4fe1df0853f75ba7f908dafe49aa8f8b4ed)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/data-dog/go-txdb to v0.1.8 ([0fdc11b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0fdc11b4f36c65147f2dd5c153d768c100e8d3d8)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/go-chi/chi/v5 to v5.0.11 ([fbd0ab8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fbd0ab8142018e9e60202050860dccf038e95747)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/golang-jwt/jwt/v5 to v5.2.0 ([be75275](https://gitlab.com/commonground/nlx/fsc-nlx/commit/be752752318bd341caddda0d60a0746a174f16ac)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/golang-migrate/migrate/v4 to v4.17.0 ([5c88a8a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5c88a8ada13d31f25ec2ab5d22d4766235931bb1)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/google/uuid to v1.5.0 ([6c729f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6c729f677a08c3ee71fc3a8eba4ee8eadad7d09f)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/lestrrat-go/jwx to v1.2.27 ([477d387](https://gitlab.com/commonground/nlx/fsc-nlx/commit/477d3873bdfef802a391a983373906f8e3dbc811)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/lestrrat-go/jwx to v2 ([f033cc9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f033cc9e5e74f4e63b90bbd1fa09cd54a3bc6890)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module golang.org/x/crypto to v0.17.0 ([bfc1ca9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bfc1ca9447e96edf117e44c6e0805039b08801f1)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update node.js to v20.10.0 ([bd3cac3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bd3cac37ec172acb3179bed831a4120a50a75f38)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update openpolicyagent/opa docker tag to v0.59.0 ([71b6e0f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/71b6e0fa0c22285e483f5faab790295dd664f1ee)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update openpolicyagent/opa docker tag to v0.60.0 ([027d6f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/027d6f6e93a3a7895e1f58238173407da503fd30)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update postgres docker tag to v16 ([de68a37](https://gitlab.com/commonground/nlx/fsc-nlx/commit/de68a372d25b54c913e47874aa7cad6ad72e15ff)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update selenium/standalone-chrome docker tag to v4.15.0 ([9feafc6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9feafc651bf58348d3d9b13110970406031adbd3)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update selenium/standalone-chrome docker tag to v4.16.0 ([f8671b6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f8671b6afacd2e00b2809d3504bef46bc5242ca5)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update selenium/standalone-chrome docker tag to v4.16.1 ([d8edecb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d8edecb4359cc8c2760ad345806a92f93a8bce09)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update semantic-release monorepo ([64f9b88](https://gitlab.com/commonground/nlx/fsc-nlx/commit/64f9b88e43a20452cf2be7787256b479b9d03bcf)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.13.1 ([fd0d740](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fd0d74086ce345586cc758fb9bb9b6bd885e2d40)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.13.2 ([d8362cc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d8362cc4e64197405c0a3081ad33b5c206f614ef)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.14.0 ([1f26a79](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f26a797c977d17914c4ea1c32e48c6d69b4dd37)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.15.0 ([b4301b2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b4301b29f7d1bc2953bbc6757dcf80ea232cc4d0)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)


### Continuous Integration

* **directory:** introduce /health endpoint for the liveness and readyness probe ([07b2c2e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/07b2c2e7f07be0497820f1b4762f1e0abf7b3586)), closes [fsc-nlx#98](https://gitlab.com/commonground/fsc-nlx/issues/98)
* **directory:** introduce startup probe for the UI ([d5fec50](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d5fec50cdd80d316186a723fabb48e7b6d5f8ee9)), closes [fsc-nlx#98](https://gitlab.com/commonground/fsc-nlx/issues/98)
* enable ci jobs for external merge requests from forks ([0e536f2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0e536f283c2b4ebdda602ce8438ca9bac78d9cd3)), closes [fsc-nlx#134](https://gitlab.com/commonground/fsc-nlx/issues/134)
* remove tx-log-api from the Directory deployment ([d4e2a1b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d4e2a1b59c4eb107cf5ecbd78674d5063f73025a)), closes [fsc-nlx#133](https://gitlab.com/commonground/fsc-nlx/issues/133)
* remove unused tags ([45dbf34](https://gitlab.com/commonground/nlx/fsc-nlx/commit/45dbf34c909624eda79cec551328fd367eeea02e)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)
* run review deployment for external merge requests of forks ([b486004](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b4860046dcaf29d2c1bd17ab4d69ce3ebb628d6f)), closes [fsc-nlx#134](https://gitlab.com/commonground/fsc-nlx/issues/134)
* update pipeline to use the kubernetes based gitlab runners ([cd1865d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cd1865d69de609a4b9c1026a1def1e6a01873f70)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)


### Documentation

* **helm:** add fsc prefix to install commands in the readme's ([3075e64](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3075e640e9d72dd68b89edf2dc525f24c6092585)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** update txlogAPIAddress flag description in the Manager chart ([59ef524](https://gitlab.com/commonground/nlx/fsc-nlx/commit/59ef5246b751c4e081ff9283aa269c82be202430)), closes [fsc-nlx#133](https://gitlab.com/commonground/fsc-nlx/issues/133)
* update issue template for user stories ([415af2f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/415af2f90634fd67f6440a90a4ca5f59ccaf29c1)), closes [fsc-nlx#118](https://gitlab.com/commonground/fsc-nlx/issues/118)


### Features

* add `delete_service` call to controller's REST API ([5e340b9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5e340b9237c4be5519c919d85aae5aae1735cd31))
* add `get_services` call to controller's REST API ([a49144e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a49144ecb19c155a2186d927731ee7ba40a1a37f))
* add `update_service` call to controller's REST API ([dde732e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dde732e8e1e2c7deaf4a4471f09a0eca1e0b2e33))
* add health endpoints to controller and manager ([35b0f8e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35b0f8ed80b9f753320ede3b7049c52dfd8ee113)), closes [fsc-nlx#99](https://gitlab.com/commonground/fsc-nlx/issues/99)
* add internal unauthenticated jwks endpoint ([7203a56](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7203a56478470696d34bd481082226ecdea087d8)), closes [fsc-nlx#42](https://gitlab.com/commonground/fsc-nlx/issues/42)
* **controller:** display Outway names besides the Certificate Thumbprint ([52e8d42](https://gitlab.com/commonground/nlx/fsc-nlx/commit/52e8d429f4b0e936b4db730e9914b1b760592455)), closes [fsc-nlx#88](https://gitlab.com/commonground/fsc-nlx/issues/88)
* **controller:** log error when the Add Contract page fails to render ([801b1f5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/801b1f504883435f119118307d2d9952334b55b0)), closes [fsc-nlx#88](https://gitlab.com/commonground/fsc-nlx/issues/88)
* **controller:** show 100 latest transaction logs in descending order ([aaf20df](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aaf20dfe4a76d72be917ea1ff79e06db3fb76e8a)), closes [fsc-nlx#129](https://gitlab.com/commonground/fsc-nlx/issues/129)
* **directory:** remove environment toggle from the UI ([d837c05](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d837c0533cf1c1a843c8095391aa88753c3c1493)), closes [fsc-nlx#143](https://gitlab.com/commonground/fsc-nlx/issues/143)
* **manager:** add logging when retrieving a certificate for a Peer fails ([77c704f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/77c704f464d560f7f5a0c31134db35481a9aa3ba)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** enable optional txlog-api when deploying as a directory ([0a75efa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0a75efaad1b79667fb1f8507366403baca162884)), closes [fsc-nlx#133](https://gitlab.com/commonground/fsc-nlx/issues/133)
* **manager:** rename transaction_id filter to transaction_ids ([c38e806](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c38e80628f5efaa656570bebe9090fc21592c9fd)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **manager:** return error if string to decode is empty ([0f7843d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0f7843dab5c0d1c55d040bfa12263cf1ce977e3a)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **manager:** surround Peer ID with quotes in error messages ([6f70a43](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6f70a430d118ebc78d9ced4de33b436c36f2e863)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** update logging endpoints OAS and implementation according to the Standard ([13c068c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/13c068c1a9f01eda4c673363101ea0d2564cc27c)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* remove PeerRegistration Grant ([fb05cb0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fb05cb06bfa7691becc29abec3baf2eee2ac450d)), closes [fsc-nlx#131](https://gitlab.com/commonground/fsc-nlx/issues/131)
* **txlog:** rename transaction_id filter to transaction_ids ([f85de34](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f85de344dff40a1eb9391b576b09aa477a818df4)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** update OAS and implementation to be consistent with the standard ([39cb5d2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/39cb5d2340b45d53b06f8a1b7c9fb72a61d82c40)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)


### Bug Fixes

* added missing logging errors to Inway and Outway ([32714e7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/32714e78bb4f8297ce51833288c4bd8ab7db63dc)), closes [fsc-nlx#122](https://gitlab.com/commonground/fsc-nlx/issues/122)
* error http status codes Inway and Outway ([4c6abb9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4c6abb9e42e6e2a1d07d2efb0a8eee43130bbd60)), closes [fsc-nlx#123](https://gitlab.com/commonground/fsc-nlx/issues/123)
* **inway:** add error code as HTTP response header ([4fb25a0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4fb25a02864e411f83fee3a84e9101a3cb6bada3)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **inway:** add missing http error TRANSACTION_LOG_WRITE_ERROR ([c7d9ab4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c7d9ab4c8d744df88d8203aef9d0036fceac62be)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **inway:** always use authorization plugin ([2f369d6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2f369d6e2696eb608d6ddc80699c241d7892b354)), closes [fsc-nlx#42](https://gitlab.com/commonground/fsc-nlx/issues/42)
* **manager:** ensure list of peers is stable sorted ([11577c1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/11577c121e324f35d29aaff156088948e5b11da4)), closes [fsc-nlx#131](https://gitlab.com/commonground/fsc-nlx/issues/131)
* **manager:** get peers default limit and tests ([75b6ddc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/75b6ddc73f053424847d5188f0802f79dfa2fcb3)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **txlog:** get_logs transactionID filter must ignore other filters ([04bc854](https://gitlab.com/commonground/nlx/fsc-nlx/commit/04bc8545cffdd383f6325e56f081c330d4b337fc)), closes [fsc-nlx#127](https://gitlab.com/commonground/fsc-nlx/issues/127)
* **txlog:** prevent panic when retrieving logs with a filter by Transaction ID ([6fd9bdf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6fd9bdf982446b2730a20a365d2aa7eca5015532)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)


### Code Refactoring

* add group ID to register Inway and Outway call ([bf29994](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bf2999402c11b9030e0e715557a8a1284d5b3c48)), closes [fsc-nlx#103](https://gitlab.com/commonground/fsc-nlx/issues/103)
* **manager:** introduce fetchLimit property per handler ([e4a9502](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e4a950212c2d4bf8154048eed1ec9b52d84b8e9d)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **manager:** replace transaction log stub with check if transaction log is disabled ([f5efe58](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f5efe586add0f7b61ec3ceebcd8b5586e6b03c83)), closes [fsc-nlx#133](https://gitlab.com/commonground/fsc-nlx/issues/133)
* **manager:** use interface as return type for the fake txlog constructor ([95d1eff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/95d1eff0684a9169cdcccec5b67cc009fbc307c8)), closes [fsc-nlx#135](https://gitlab.com/commonground/fsc-nlx/issues/135)
* merge getServiceEndpointURL and getInwayAddressForService into one api call ([0792954](https://gitlab.com/commonground/nlx/fsc-nlx/commit/07929540bafe218905e914927f3b5e48f879991a)), closes [fsc-nlx#103](https://gitlab.com/commonground/fsc-nlx/issues/103)
* remove deprecated getCertificates internal manager api call ([fd196cd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fd196cd3ead93d7fc837df6a491f99c34bccd7ee)), closes [fsc-nlx#42](https://gitlab.com/commonground/fsc-nlx/issues/42)
* use protocol enum instead of string and add protocol to manager client ([1942534](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1942534b171deef72989db00459d9c871135b02b)), closes [fsc-nlx#139](https://gitlab.com/commonground/fsc-nlx/issues/139)


### Tests

* **inway:** add transactionlog and delegation test cases to Inway tests ([45837d3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/45837d3d6960915dc6c30a40bfef69f615830e8b)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **manager:** provided missing tests from testsuite in manager ([2f7822e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2f7822e08fd76cf804ea30dd0ea60f9c4c7cc508)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **manager:** replace query tests with port tests for the internal manager ([b470162](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b470162173ddf98fecdb136bb2698df2795cd268)), closes [fsc-nlx#135](https://gitlab.com/commonground/fsc-nlx/issues/135)
* **outway:** added tests for the Outway ([aa998d4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aa998d406a0aa69d762b46a544b1f70767dc6b13)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **txlog:** add test for the Create Records usecase ([d64d87d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d64d87d85c699837f40f17c02d08c5614a382d07)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** add test for the List Records for a Delegated Service ([0dedd2b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0dedd2b35a361aaebb805375ec70377889973fc6)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** add test for the List Records for a request on behalf of another Peer ([c8ce624](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c8ce62448dc3e7f6c241ec05aae1ab76054cc8ed)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** add test for the List Records usecase ([b25d6c5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b25d6c5f13055fb5ea72d6836457ae4b912622b8)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** fix flaky test ([07f8963](https://gitlab.com/commonground/nlx/fsc-nlx/commit/07f8963d69af965bd6445497ac4adbacc6702f18)), closes [fsc-nlx#135](https://gitlab.com/commonground/fsc-nlx/issues/135)
* **txlog:** remove redundant test ([81c41ff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/81c41ff9d79c85a83f4e995f0f859883573938a6)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** remove redundant test ([57118f1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/57118f158a90aab8dc47f0a96ad03377d03882f6)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** use unique transaction ids per test ([ba175aa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ba175aab993c125e52dc02741f84d6e076af3e93)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)

## [0.12.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.11.0...v0.12.0) (2023-12-01)


### Build System

* add the Earthly Helm generate docs target to the all target ([80b4a77](https://gitlab.com/commonground/nlx/fsc-nlx/commit/80b4a775de597c5c995d2eed37b6aa51f8ac1521)), closes [fsc-nlx#120](https://gitlab.com/commonground/fsc-nlx/issues/120)
* pin dependencies ([9f96083](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9f96083ce58578a31db38a2eb8066fde52fe2d32)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update alpine docker tag to v3.18.4 ([33bca9c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/33bca9cb5d1e8bff82d3aa0758c2e5faf2bcaa64)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update alpine docker tag to v3.18.5 ([0a1cced](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0a1ccedaa960b25c871a99e5645f96fa8a7f816a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update commitlint monorepo to v18.4.3 ([f201dc9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f201dc9d361533c3bb92fdb5e8b09aa5a7e748e2)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @axe-core/webdriverjs to v4.8.1 ([b251968](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b25196844850f0607982d8817092e49a771d24de)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @cucumber/cucumber to v9.6.0 ([dfd0f3f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dfd0f3fc82ef0bcd962304419c9e18d2bc08605a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @swc/core to v1.3.100 ([f20212c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f20212c2f0e1732be669da41afad44f71e23bc0b)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @swc/core to v1.3.99 ([d7eddb4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d7eddb4236d65c8a61cf0ea87607e199bd8af9c2)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v18.18.13 ([a7356ea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a7356ea0c83e69ad49210dd41921ff3648e88e75)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v18.18.14 ([0d2b6a2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0d2b6a2ff5326d3d0a2fd6966dea945d20283fb3)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency dayjs to v1.11.10 ([35a84de](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35a84de935089a71bd049b3d4755390e40fe273f)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint to v8.54.0 ([6e73fc6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6e73fc629b6baeffa9268f380f1fd28f60330ff9)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-import to v2.29.0 ([20a2a0b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/20a2a0b2a2ba85fa582925b4ea36071f9d372fbf)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-prettier to v5.0.1 ([93da4bd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/93da4bd06c5c189839576d854b605b6467b78a16)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency htmx.org to v1.9.9 ([3d5b19c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3d5b19c8dc672f600d6e46ef5ee3ccb4fbdb6bc8)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency hyperscript.org to v0.9.12 ([250cc46](https://gitlab.com/commonground/nlx/fsc-nlx/commit/250cc4611a4fe97e77cc1430d76eb1f21079c39a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency prettier to v3.1.0 ([f4fa792](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f4fa7922567cadbc654198238a40b5767b4b3a34)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency replace-in-file to v7.0.2 ([cdb31e8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cdb31e82b27cb28864df26b55d351a61a9b8b659)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency selenium-webdriver to v4.15.0 ([930a243](https://gitlab.com/commonground/nlx/fsc-nlx/commit/930a243533c209800b30df6cf977b9d5db6fe6c5)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency typescript to v5.3.2 ([3826197](https://gitlab.com/commonground/nlx/fsc-nlx/commit/38261976b70e568f335253646861b97603dac731)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dexidp/dex docker tag to v2.37.0 ([fceff52](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fceff52e9fa45332f1a5b80535aee9956f11b298)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docker docker tag to v24.0.7 ([e53e7c4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e53e7c4ebc28e045eaad2b51237aa193ac0bfef8)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docker.io/curlimages/curl docker tag to v8.4.0 ([ac78117](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ac78117bb171fe3213cc1a2eed7be8b1f0c5d2dd)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docusaurus monorepo to v2.4.3 ([3ac416c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3ac416c34cb0a797b22dc9661557b6d07dc639b4)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang docker tag to v1.21.4 ([c279e1d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c279e1d3d562b23b2a11a8eb0f66ec073fad78ee)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to 6522937 ([b418aaf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b418aaf81792121c9d523ba16b91227f89cf6e4e)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to 9a3e603 ([101b354](https://gitlab.com/commonground/nlx/fsc-nlx/commit/101b3549bf4c7b37d2ded831710733136545558c)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update helm release basisregister-fictieve-kentekens to v0.1.1 ([36b9333](https://gitlab.com/commonground/nlx/fsc-nlx/commit/36b93330eb1308f7ab47f6188e5e311328eb4ed4)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/coreos/go-oidc/v3 to v3.8.0 ([42bb6cc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/42bb6cc4e3c252f450ac0d71d34549f048cb8d34)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/getkin/kin-openapi to v0.122.0 ([19faa9f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/19faa9f475d6ee252ec2c7a2ef459cf12bb6cbbd)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/golang-jwt/jwt/v5 to v5.1.0 ([6b93c55](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6b93c553c94ec89dcbc43bfd8d702ceb62dc7822)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/google/uuid to v1.4.0 ([c1f89cb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c1f89cbf78f716e0a66267213c19fc455c0fdbda)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/gorilla/securecookie to v1.1.2 ([dd8ffec](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dd8ffec494fb1e1319f8feebff2eb775f3bdbe92)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/gorilla/sessions to v1.2.2 ([7d59d47](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7d59d47b7460f7059b08febcb64af381b1178e97)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/oapi-codegen/runtime to v1.1.0 ([fbe3a17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fbe3a17f6b937dcf4619f717a631df31542a7338)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/spf13/cobra to v1.8.0 ([5f5cd76](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5f5cd768f57093e3336464890b460c768d23f601)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module golang.org/x/net to v0.19.0 ([5b0cf97](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5b0cf97573cf2da64767b8a5b6a80a537361fd4a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module golang.org/x/oauth2 to v0.15.0 ([b420c2d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b420c2d537aa437adbfdf2ef04a6025ec684a364)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module golang.org/x/sync to v0.5.0 ([34dc1d3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/34dc1d30e927db4a1ab1ec64dda933147923059b)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update openpolicyagent/opa docker tag to v0.58.0 ([1f2668d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f2668d80e916ad3215ec3e536fdaf948edfd926)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update parcel monorepo to v2.10.3 ([43dbf45](https://gitlab.com/commonground/nlx/fsc-nlx/commit/43dbf45a6de1e24f41528a58711348d236a10091)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update postgres docker tag to v15.5 ([5451f1c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5451f1cc6e84c6ab6608550efe1ed7703cfba54c)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)


### Continuous Integration

* enable Container Scanning using Trivy ([0ea6a5c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0ea6a5c3973e6bb6bcfcb63c6cd280a15987e37e)), closes [fsc-nlx#105](https://gitlab.com/commonground/fsc-nlx/issues/105)
* update container scanning rule so it is enabled again ([721989d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/721989d6087eb004ea17c7c9ace7ce99b58a7db6)), closes [fsc-nlx#105](https://gitlab.com/commonground/fsc-nlx/issues/105)


### Features

* add 'Bearer' scheme to Fsc-Authorization header ([6dfb126](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6dfb1263c7487295034b37907b2fe51f201f9908)), closes [fsc-nlx#49](https://gitlab.com/commonground/fsc-nlx/issues/49)
* **directory:** enable serving the Directory UI from a custom URL Path ([a9a7c77](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a9a7c77c146cdfc8588813a8d48b2974bb3abba2)), closes [fsc-nlx#120](https://gitlab.com/commonground/fsc-nlx/issues/120)
* **helm:** add serviceProxyCacheSize setting to the Inway chart ([7780c31](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7780c319f42f4f2b927ea289d369ce575004f305)), closes [fsc-nlx#107](https://gitlab.com/commonground/fsc-nlx/issues/107)
* **inway:** ensure requests from the Inway to the Service URL contain at least a root slash ([3446344](https://gitlab.com/commonground/nlx/fsc-nlx/commit/34463446d798bbe2078a58227e3553cc94ab86b6)), closes [/www.rfc-editor.org/rfc/rfc2616#section-3](https://gitlab.com/commonground//www.rfc-editor.org/rfc/rfc2616/issues/section-3) [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **manager:** add specific algorithm errors for hash and signature ([8fb686e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8fb686ea3d90bb65d713e2ae8db630adb0dc1766)), closes [fsc-nlx#97](https://gitlab.com/commonground/fsc-nlx/issues/97)
* **outway,inway,manager,controller:** respect HTTP_PROXY and HTTPS_PROXY environment variables ([7761832](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7761832528ef6b72252e0adbea76644feb3dc7d4)), closes [fsc-nlx#121](https://gitlab.com/commonground/fsc-nlx/issues/121)


### Bug Fixes

* **controller:** prevent resetting form fields when adding a new Grant to a contract ([22214b8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/22214b819b1e7bd2e87bb994cb6cb26b969adbda)), closes [fsc-nlx#111](https://gitlab.com/commonground/fsc-nlx/issues/111)
* **inway:** reuse connections to Services ([fda9ccd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fda9ccd4241951665a56a1e2f8ea0987ef5ceea6)), closes [fsc-nlx#107](https://gitlab.com/commonground/fsc-nlx/issues/107)
* permissions for OPA auth containers ([114845c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/114845c3cec6a7319cc28789a2f27933d004cb00)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Code Refactoring

* **inway:** rename MockRepository -> fakeConfigRepository ([68da028](https://gitlab.com/commonground/nlx/fsc-nlx/commit/68da0289ba49be1e83698512f50efd2c5e77b561)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** rework creating mock config repository to improve readability ([825ccd7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/825ccd7823251926696f96254a319a340e1ab09f)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **manager:** use globally available organization certificates ([ebee2ef](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ebee2ef195cc3a10ffd6edfc17e02c53ee3831e1)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **outway:** rename context to requestContext ([fc9f5bf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fc9f5bf4e95da5085975a90f1f6431f782024d6a)), closes [fsc-nlx#107](https://gitlab.com/commonground/fsc-nlx/issues/107)


### Styles

* **manager:** improve formatting ([8966c3b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8966c3b8c21d2820a9de7f27dd7462e20e5ec347)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)


### Tests

* **inway:** add assertion for the response body if the expected status code does not match ([1e25b7b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1e25b7b3bc4b305c503351bbfd44c8e50878b75a)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** enable instantiating test Inway with a fake ConfigRepository ([9cd0c2f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9cd0c2f404b0cebaf4620a03e6b502fd224d75f9)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** enable passing custom cert for the Inway -> Outway connection ([f7b965b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f7b965bc1997cb325383335c6ac9b1181cb40c04)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** extract fake implementations into separate files ([0cdbefe](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0cdbefe4335cefb1091bd397669d2b35245c460a)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** move transaction log assertions to the act phase ([1e3d9f0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1e3d9f0156ffdd315f2b775a59f03701cee84763)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** rename fscTransactionID -> validFSCTransactionID to reveal its intent ([378f6cc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/378f6ccafbc2574762c467a5caf0684d270a1fa3)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** rename variables to improve readability ([e224542](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e22454266966d6b08c0ab5e15864f7f34c13350d)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** replace mockService with serviceEndpointURL ([adcbece](https://gitlab.com/commonground/nlx/fsc-nlx/commit/adcbecee734f0183b7583470b684210a9f8e39c5)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **manager:** add test ids from FSC Test Suite to corresponding tests ([f12f0be](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f12f0bec6facb02c4d05915fc96a6246324b0dca)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)

## [0.11.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.10.0...v0.11.0) (2023-11-23)


### Features

* **helm:** add env var to inway helm chart ([cbd27fa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cbd27fa055059be28942111dc28e26b86e297463)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [0.10.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.9.0...v0.10.0) (2023-11-23)


### Features

* **helm:** add env var to outway helm chart ([ef55169](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ef5516948f0e41dccf9d09e05d757f64e6b89b3d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Bug Fixes

* **manager:** return error details according to FSC for test scenario Manager-RejectContract 1 - 6 ([5fbb5cd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5fbb5cd82b8e77737ab9c3fa022560a5cee5b91d)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-RevokeContract 1 - 6 ([1b6facb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1b6facbd04d2e76b48725128ef20f0b77143d7b9)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* permissions for testing key material more permissive ([e1cc8a9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e1cc8a9f2ef5383655ed690b1725aff0ccbac28c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Tests

* **manager:** add logging for failing assertions ([67047d3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/67047d380cf0bc753c68a507f222a762035c4d2b)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)

## [0.9.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.8.0...v0.9.0) (2023-11-22)


### Continuous Integration

* make the redeploy to ACC job independent of the previous jobs ([2828b45](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2828b45ef21ce05840590ea12687eea4c98455a3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Documentation

* corrected dns name in outway internal certificate kubernetes manifest ([35a8b40](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35a8b40dec3ec04d7c8056767df531afe69d2796)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Features

* add rest api to tx log api and let inway outway and manager use rest api ([42c2e6a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/42c2e6a40dfa4f2f3e5b98b03bccaa1bc431bf9c)), closes [fsc-nlx#91](https://gitlab.com/commonground/fsc-nlx/issues/91)
* add rest controller adapter to inway, outway and manager ([31b868d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/31b868dccea3290fdf21d6e482f830a9f950836a)), closes [fsc-nlx#90](https://gitlab.com/commonground/fsc-nlx/issues/90)
* **controller:** add internal rest port ([91b0b0a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/91b0b0a8cc5a03d884316fd03db5d32ed04a04ea)), closes [fsc-nlx#90](https://gitlab.com/commonground/fsc-nlx/issues/90)


### Bug Fixes

* **helm:** rename inconsistent volume name for the internal certificate of the Outway ([b9b25df](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b9b25df3a8ca7ff2cb68e41264fcfdf319765bf8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** error responses of external manager conforms FSC ([756b290](https://gitlab.com/commonground/nlx/fsc-nlx/commit/756b290f32ab5e2f894a9c46eeb732ef2782d4ea)), closes [fsc-nlx#73](https://gitlab.com/commonground/fsc-nlx/issues/73)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-2 ([7e2fbfb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7e2fbfb9f75ff52b3dfb2eeefba3277c853c264a)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-3 ([1b455d9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1b455d904fd50349534a316a23b8a7586523ee80)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-4 ([9b966a5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9b966a5921feee23ad5187c5c5603d047aaa2c7d)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-5 ([9e8ee5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e8ee5e2b72c78edde2a9aa1d78914d5811e6db2)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-6 ([ac5dc17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ac5dc177ef11161523c58b7b133b9cb49e640860)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** use correct error type for type casting error ([354b2d0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/354b2d0c68b8d580984c9a1156cb95ece6ca7029)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Code Refactoring

* **manager:** reduce complexity for the generation of test hashes ([821ea62](https://gitlab.com/commonground/nlx/fsc-nlx/commit/821ea6293804444fc12ff51aede33a198b490c48)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** remove unused code ([9db5c6a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9db5c6ad5fee088ffe9da430535865a9bfcb28cb)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** reuse app errors for error handling ([1ccaf6d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1ccaf6d82eaa67d54eb5bfc02a06d07104bc7162)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* remove grpc related files and adapters ([6ae883c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6ae883cc6e294803267e8311cc697fe5708b0045)), closes [fsc-nlx#90](https://gitlab.com/commonground/fsc-nlx/issues/90)
* remove grpc txlog server and adapters ([7763ba3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7763ba30fc82d4b98ad2b3cfbf58ae360a762362)), closes [fsc-nlx#91](https://gitlab.com/commonground/fsc-nlx/issues/91)


### Tests

* **manager:** add tests for the AcceptContract behaviour ([bcd3adc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bcd3adc675e2fcdb6928f36f49bac8f4363244db)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** pass the actual contract content hash when signing a contract ([e022b62](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e022b62b7233a8dfdc5780bcfea876d8aa4d3701)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** replace custom struct with command arguments to create internal contract ([b796792](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b796792073049cf66b346fa5ac06202aa992bb87)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** replace custom struct with JSON model struct to approve contract ([2039b67](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2039b67a05d434201eaa4e49651ef375290f9cb1)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)

## [0.8.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.7.1...v0.8.0) (2023-11-16)


### Build System

* **manager:** add updated FSC OAS with protocol for Service Publication Grants ([17f323e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/17f323e6d4ae7a63a47253153b0b5bf6d33fca64)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** regenerate external Manager models based on updated OAS ([1f17dc0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f17dc0a2a190e638980e91df33f0ef2854de51d)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* remove obsolete admin config from modd ([b3e85ec](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b3e85ec29abcc4b7f5dfb2983d782501eed03118)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* tidy Go mod file ([71beed2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/71beed2c6cbbd0ff3bfffd597cee5398a14afbf8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* enable Go mod tidy test if Go files change ([bc7e955](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bc7e955b478df1a6d981f41392968785c0935557)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** add default security context to charts ([c9bf3dd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9bf3dde800be113fdbabc82011b5413b874142d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* remove deployments of the preprod and prod environments ([0ff1214](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0ff1214be2a09199a3ebdabde904f1cf08765738)), closes [fsc-nlx#86](https://gitlab.com/commonground/fsc-nlx/issues/86)
* remove directory chart ([92b2b6c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/92b2b6c603f930b50239de00b6b9474d1774753f)), closes [fsc-nlx#86](https://gitlab.com/commonground/fsc-nlx/issues/86)
* remove directory chart from helm update script ([46c7afc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/46c7afc6b53b7800364eb104ed1ca2f93d6adbc4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Documentation

* add GroupID to the helm value files used in try nlx guide ([0b85af4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0b85af4163071fbf340f78460290e84aa4f0c40c)), closes [fsc-nlx#87](https://gitlab.com/commonground/fsc-nlx/issues/87)


### Features

* **controller:** simplfy service endpoint url validation ([9b5920b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9b5920b4665534ceec3e2ab52e428ddd0d4aad31)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **directory:** add preprod and prod environments ([19648a9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/19648a9daae6d05213427777c7f933db67e6e594)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **directory:** only support the demo environment ([f1db784](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f1db7848196150befc0ecacce7e890b972624257)), closes [fsc-nlx#86](https://gitlab.com/commonground/fsc-nlx/issues/86)
* implement protocol in service publication grants ([7af2c7d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7af2c7dd23995932077c2a9310c3b01f8cbc8c03)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **inway:** add specific error response for expired tokens ([a944f25](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a944f25371d64c1d88b5317574e8ca1faf3ecae9)), closes [fsc-nlx#93](https://gitlab.com/commonground/fsc-nlx/issues/93)
* **manager:** add rest json internal port ([47ab3c7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/47ab3c7863aeb137c9712f63049d5ac9bf8e6cf2)), closes [fsc-nlx#62](https://gitlab.com/commonground/fsc-nlx/issues/62)
* **manager:** implement errors for the token endpoint ([16aa062](https://gitlab.com/commonground/nlx/fsc-nlx/commit/16aa0621a7dd99bdea4b6fa8543581e33a761002)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** implement protocol in delegated service publication grant ([62eaa79](https://gitlab.com/commonground/nlx/fsc-nlx/commit/62eaa796174fde05b49abf14efab2750ea47f9e7)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** reorder fields in the delegated service connection grant ([b21685f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b21685fbaabdac192506f9b111b28666d4b3a936)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Content Hash algorithm ([e9c9784](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e9c9784c611f8f8bb2a9007b267275e0fb4654b2)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Delegated Service Connection Hash algorithm ([7b0d5b3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7b0d5b3408a87e4642c64be2005ae246dc039c02)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Delegated Service Publication Hash algorithm ([35248b3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35248b33186a94ab3ff43bf1e6de627ca59b7877)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Peer Registration Hash algorithm ([401f967](https://gitlab.com/commonground/nlx/fsc-nlx/commit/401f96795bbc7e203cf1b0e8dc0aced7248c8572)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Service Connection Hash algorithm ([eea050a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eea050a79d105ee97696363caf8f270e3157c99a)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Service Publication Hash algorithm ([1d20a8a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1d20a8ac8fb13fae3fdbfb5c04361b866d87e207)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** support protocol property for the Delegated Service Publication Grant ([8b61a73](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8b61a735db9dd49fb0b170933481a9438cbc3289)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** support protocol property for the Service Publication Grant ([315f11a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/315f11ae2369a2729acd9ff8fa51d19fff057bae)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)


### Bug Fixes

* **inway:** invalid Fsc-Transaction-Id header returned the wrong error ([08596f5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/08596f514928cd993dc6c06db0044f2541180e36)), closes [fsc-nlx#93](https://gitlab.com/commonground/fsc-nlx/issues/93)


### Code Refactoring

* **inway:** aligned bootstrap with manager for easier testing ([f9bfb82](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f9bfb82e3ce109e81999e90d939a99570e432d7f)), closes [fsc-nlx#93](https://gitlab.com/commonground/fsc-nlx/issues/93)
* **manager:** move the protocol field to the service object ([2bde72a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2bde72ac2211e00d1166d4e5c2e75a23effce06d)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** move the protocol field to the service object ([863c419](https://gitlab.com/commonground/nlx/fsc-nlx/commit/863c41989a9dd8070db5dadc2d249c0dd1f6c14f)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** remove redundant log ([cfb49e4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cfb49e43d586d3825cdf6fc8fc70f8d4a078cbbd)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rename hash test files ([f4256d5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f4256d5db241670defceb051113f914587931f8d)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** use value method of protocol domain model ([24180d4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/24180d4278ace94fcf92bbc9d1f5ad36ba7ff6a6)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* remove admin tool, not needed anymore ([3a6ac2a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3a6ac2ac8c79049c391b26aa3cf9b8cc19e8a043)), closes [fsc-nlx#62](https://gitlab.com/commonground/fsc-nlx/issues/62)
* remove grpc adapters and ports ([e924afd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e924afd41034355b072d2acb92dbdb07204bcf37)), closes [fsc-nlx#62](https://gitlab.com/commonground/fsc-nlx/issues/62)
* use manager rest endpoint in inway, outway and controller ([5f41b28](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5f41b286b8d74b95d2acd609d7573700e241239e)), closes [fsc-nlx#62](https://gitlab.com/commonground/fsc-nlx/issues/62)


### Tests

* **inway:** added Inway test suite to go tests ([dd6645b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dd6645b0d5be54873f87ad61a087468b083c3265)), closes [fsc-nlx#93](https://gitlab.com/commonground/fsc-nlx/issues/93)
* **manager:** add test suite tests for token endpoint ([74f0aaa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/74f0aaa574774ea092cea80127bf9240571e39d3)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** annotate get peer info test with testsuite test id ([7554847](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7554847de0ab12e9816584f2c6de6be02498eee7)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** fix integration tests using the missing protocol property ([3e5440a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3e5440a6f96b4cae19b3a3e75acba47ffb82981b)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** pass protocol when using (delegated) Service Publication Grants ([32bca5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/32bca5e7c36d0290da21b9d77ac49c14de45873d)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)

## [0.7.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.7.0...v0.7.1) (2023-11-06)


### Continuous Integration

* add group id to the values for the preprod and prod deploments ([3d5fca3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3d5fca3a1a3d1085eaffa368a7d7c2720dfb5e1a)), closes [fsc-nlx#27](https://gitlab.com/commonground/fsc-nlx/issues/27)

## [0.7.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.6.1...v0.7.0) (2023-11-06)


### Build System

* **manager:** add HTTP status codes from standard into the core OAS ([a691bb4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a691bb459f425a2f79d01711686d8d6599e9bb6c)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **manager:** regenerate REST server from the FSC Core OAS ([cb4ea6f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cb4ea6f3e11135295127a45e91c58553de210181)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* regenerate rest server from core OAS ([7cecb7a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7cecb7af59a368d7ecdfc0b7caea44c24d1c1d0d)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* upgrade Semantic Release & Commitlint packages ([dad6577](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dad6577b7a4ccebf58bc1d661865a4f34d4cd2cb)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* add release rule for every allowed commit type ([b7590e6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7590e63d04c8d9106d92549dc8440f6d0fc607f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* explicitly include every commit type in our CHANGELOG ([003f438](https://gitlab.com/commonground/nlx/fsc-nlx/commit/003f438bd5a41e2828c73fa8a8994c3dfcd570df)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node version ([f8e9331](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f8e9331496475947fac3827ed0734ef21f7a886f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Documentation

* reword release strategy to be more specific ([a30af9c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a30af9c9e663269f810a04bb1c756e67857e3dcc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Features

* **controller:** added validation checks when submitting service name ([e5eaa47](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e5eaa474cd31c265cd53d7c95561827b3b17b1e8)), closes [fsc-nlx#71](https://gitlab.com/commonground/fsc-nlx/issues/71)
* **controller:** peer id can be selected with a datalist ([8ecbe76](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8ecbe7696ab7dfd6135fabf488604bc3e47be501))
* **controller:** show confirmation dialog before signing a contract ([b9a5cea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b9a5cea5ebecb0eaef59cc4c756364057d7ff811)), closes [fsc-nlx#67](https://gitlab.com/commonground/fsc-nlx/issues/67)
* **controller:** use group id domain model instead of string ([ba7c419](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ba7c4197a3dd5fff56556663233bbda687b72757)), closes [fsc-nlx#80](https://gitlab.com/commonground/fsc-nlx/issues/80)
* **manager:** return error code and domain according to FSC for Manager-SubmitContract-2 ([6dde658](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6dde658f9940e74bccb31d28f66ed299f0e62af0)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **manager:** rework the SubmitContract endpoint to return 422 with the corresponding error code ([e1923e0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e1923e022a4aab93dcde2a2a01daba2e73995b77)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **manager:** use domain model to pass the group ID to the external app ([d589fad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d589fad8f411b9487aed4c55b40a8aac13c95702)), closes [fsc-nlx#80](https://gitlab.com/commonground/fsc-nlx/issues/80)


### Bug Fixes

* **manager:** add required group id to internal request to the txlog api ([1dbb278](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1dbb2789591eb6c60b1465ed687952b7bf49b936)), closes [fsc-nlx#80](https://gitlab.com/commonground/fsc-nlx/issues/80)
* **manager:** ensure decoded access tokens use UTC instead of local timezone ([3da621b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3da621b843529be4811fd33d6f1d9f95aa3ddcd6)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)


### Code Refactoring

* **common:** remove unused error Location code 'M1' ([54a8d7a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/54a8d7a404c32f1de31cd0ab86553f50499deaa6)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **controller:** move the resource ID of the contract to the URL ([d4d7de9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d4d7de955c9173c769e9e3d7ecb2c689883675c1)), closes [fsc-nlx#67](https://gitlab.com/commonground/fsc-nlx/issues/67)
* **controller:** remove unused modal styling ([b34b243](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b34b2434f46ef724fde8b23c1814d1410a39accf)), closes [fsc-nlx#67](https://gitlab.com/commonground/fsc-nlx/issues/67)
* remove redundant println statements ([0082907](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0082907887401b693e5da5634c07d88cf4dfab0e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Styles

* **controller:** fix typo's in logs ([fde6cfa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fde6cfab6df657f48359b6eefe81a783b4bdf0b9)), closes [fsc-nlx#67](https://gitlab.com/commonground/fsc-nlx/issues/67)
* **manager:** cleanup structs ([c00d52a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c00d52ac0263986aec2cf53715340e62362fc383)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)


### Tests

* **manager:** implement tests from the FSC test suite for submitting an invalid contract ([066c7be](https://gitlab.com/commonground/nlx/fsc-nlx/commit/066c7be6cfcd3c7b19119f8184eafb4fafe7a368)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** move declared variable inline ([2739b49](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2739b496caac4e91e08568c166a0ab2ca6bb1911)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **manager:** rename expected -> want to be consistent with other tests ([a11191d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a11191d1f0695adb5beb4b1774ac3600cf4c1866)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)

## [0.6.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.6.0...v0.6.1) (2023-10-30)

# [0.6.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.5.0...v0.6.0) (2023-10-27)


### Bug Fixes

* **controller:** add dummy avatar to the user menu ([23c3f59](https://gitlab.com/commonground/nlx/fsc-nlx/commit/23c3f5964dfc77059509ca24d7bcf29768267b27)), closes [fsc-nlx#64](https://gitlab.com/fsc-nlx/issues/64)
* **manager:** pass the Group ID to the Controller gRPC adapter ([9338557](https://gitlab.com/commonground/nlx/fsc-nlx/commit/93385575e65f4789728bc682dc5a53f21200e080)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **manager:** realigned OAS with FSC standard ([0eca77f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0eca77fd395efa9d8cb13d5c9cb09997ae02b8e9)), closes [fsc-nlx#61](https://gitlab.com/fsc-nlx/issues/61)
* **outway:** prevent stripping the X-NLX-Authorization header ([008f699](https://gitlab.com/commonground/nlx/fsc-nlx/commit/008f699ea1102db410db476c20915bcc8e5696c5)), closes [fsc-nlx#32](https://gitlab.com/fsc-nlx/issues/32)


### Features

* **controller:** add check for required flags on startup ([50de330](https://gitlab.com/commonground/nlx/fsc-nlx/commit/50de3303162e8a837d18990b3a4a9b5a275cfa4f)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **controller:** increase with of Outway Certificate Thumbprint field ([ef265df](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ef265dfe0a61660ae86dd4801030232c236e872d)), closes [fsc-nlx#68](https://gitlab.com/fsc-nlx/issues/68)
* **controller:** remove default value for GroupID ([d56ad40](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d56ad40924fce12752fc6841b026072d03971dec)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **controller:** remove unused Settings button from the primary navigation ([508fce3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/508fce3ff7a6f1a582caa835498e3bb847346e08)), closes [fsc-nlx#65](https://gitlab.com/fsc-nlx/issues/65)
* **controller:** rename flag manager-address to manager-address-internal ([708b9a0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/708b9a043f23d17d4ba526b41d786b15ea55e2f1)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **controller:** replace select with datalist element for Outway Cert Thumbprint ([172f6c1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/172f6c106a668451f9f436522042dfddbe80865b)), closes [fsc-nlx#68](https://gitlab.com/fsc-nlx/issues/68)
* **controller:** replace select with datalist element for Outway Cert Thumbprint ([a661121](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a661121b6cc2a7df44575b21715929914d106cc3)), closes [fsc-nlx#68](https://gitlab.com/fsc-nlx/issues/68)
* **controller:** replace the Contract modal to a Contract detail page ([610bd4e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/610bd4ef342d4dd7a1d14d1d02a835a3880e6bce)), closes [fsc-nlx#66](https://gitlab.com/fsc-nlx/issues/66)
* **controller:** use debug as default log level ([84ca50f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/84ca50f17464b75cb898fdf2038abce54182f008)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **manager:** add FSC Logging extension version to the /get_peer_info endpoint ([674689e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/674689ece181e8eacd42795b3b82c0382747198e)), closes [fsc-nlx#53](https://gitlab.com/fsc-nlx/issues/53)
* **manager:** add Peer name filter to external /peers endpoint ([ffa8edb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ffa8edbf8ba7c647e762043395683ae5b48bef86)), closes [nlx#54](https://gitlab.com/nlx/issues/54)
* **manager:** add support for other algorithms in the /get_jwks endpoint ([a0afc17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a0afc17ef9098608014ca07569fd7c9614cc6c63)), closes [fsc-nlx#55](https://gitlab.com/fsc-nlx/issues/55)
* use base64URL encoding without padding for Contract and Grant hashes ([cf37765](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cf3776570032f0ceb6c6a771787b9ee556c83403)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)

# [0.5.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.5...v0.5.0) (2023-10-11)


### Bug Fixes

* update outway and inway entries on database conflict ([9a1c1e8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9a1c1e83529ff3987e9f41cc3263034c45e2641a)), closes [fsc-nlx#40](https://gitlab.com/fsc-nlx/issues/40)


### Features

* **docs:** use introduction page as entry page ([bd00a1c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bd00a1cb73932b34d073d68e07e2195acdb75522)), closes [fsc-nlx#20](https://gitlab.com/fsc-nlx/issues/20)
* **manager:** add logging on error when fetching services from directory ([99dcff8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/99dcff857634dabc433c1e8ecef7685866b99e65)), closes [fsc-nlx#9](https://gitlab.com/fsc-nlx/issues/9)

## [0.4.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.4...v0.4.5) (2023-10-03)


### Bug Fixes

* **controller:** add AuthData to service-add-handler ([8cc4f90](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8cc4f906e665b41632fbaaefbf325bd5cf88c2c4)), closes [#32](https://gitlab.com/commonground/nlx/fsc-nlx/issues/32)

## [0.4.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.3...v0.4.4) (2023-10-02)


### Bug Fixes

* **docs:** use correct commonname for txlog api certificate ([a39db53](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a39db53461062feeea252aba3f39ae13d2bd6c85)), closes [fsc-nlx#35](https://gitlab.com/fsc-nlx/issues/35)

## [0.4.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.2...v0.4.3) (2023-09-29)


### Bug Fixes

* **controller:** pass authentication data when adding a new contract ([b7cbcf6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7cbcf63373d9f5c403b5224323f321c723ef963)), closes [fsc-nlx#32](https://gitlab.com/fsc-nlx/issues/32)

## [0.4.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.1...v0.4.2) (2023-09-28)

## [0.4.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.0...v0.4.1) (2023-09-28)

# [0.4.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.3.0...v0.4.0) (2023-09-28)


### Features

* **outway:** improve logging ([ed0a913](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ed0a91308414fe858b45a829abd2e593927666fd)), closes [fsc-nlx#32](https://gitlab.com/fsc-nlx/issues/32)

# [0.3.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.7...v0.3.0) (2023-09-27)


### Features

* **controller:** add oidc authentication mechanism ([efad656](https://gitlab.com/commonground/nlx/fsc-nlx/commit/efad6563b05a6c03bac4bd7905911e93b5f6bb8d)), closes [fsc-nlx#1795](https://gitlab.com/fsc-nlx/issues/1795)

## [0.2.7](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.6...v0.2.7) (2023-09-27)

## [0.2.6](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.5...v0.2.6) (2023-09-27)

## [0.2.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.4...v0.2.5) (2023-09-27)

## [0.2.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.3...v0.2.4) (2023-09-26)

## [0.2.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.2...v0.2.3) (2023-09-26)

## [0.2.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.1...v0.2.2) (2023-09-15)

## [0.2.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.0...v0.2.1) (2023-09-15)

# [0.2.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.1.1...v0.2.0) (2023-09-14)


### Features

* **directory:** remove preprod and prod options from the Directory UI filter list ([86e36e5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86e36e5bf24651503ff9cfe4091932fb88622799)), closes [fsc-nlx#24](https://gitlab.com/fsc-nlx/issues/24)
* **directory:** update NLX content to reflect FSC NLX ([82d4a5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/82d4a5e2ad0c4bf6eb56b42e62d73b86586df033)), closes [fsc-nlx#24](https://gitlab.com/fsc-nlx/issues/24)

## [0.1.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.1.0...v0.1.1) (2023-09-13)

# [0.1.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.5...v0.1.0) (2023-09-12)


### Features

* rename listen-address to listen-address-ui in controller ([5faf247](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5faf247dcaa39b2ca5d1ac0d2e227e1475b96882)), closes [fsc-nlx#12](https://gitlab.com/fsc-nlx/issues/12)


### Reverts

* update node.js to v20.6.0 ([803abd9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/803abd9c9ab9283f63438e196c0d95b8f87c9c5b)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)

## [0.0.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.4...v0.0.5) (2023-09-08)


### Bug Fixes

* **helm:** add fsc- prefix to hostnames for the Demo deployment ([7f718a8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7f718a8297887655aee726b976844d5effda758a)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)
* **manager:** show message if Directory Peer Manager address is not passed to Manager ([13756fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/13756fc37ac462f47d9ede167b5a13f1dda911de)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)
* **manager:** show message if Self Address is not passed to Manager ([cbb6693](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cbb6693557b7f6c49322cdc85b3ae00a3f756ff5)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)

## [0.0.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.3...v0.0.4) (2023-09-07)

## [0.0.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.2...v0.0.3) (2023-09-07)

## [0.0.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.1...v0.0.2) (2023-09-07)

## [0.0.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.0...v0.0.1) (2023-09-07)
