**As a** _user_
<br>**I want** _goal_
<br>**so** _purpose_

**:scroll: Changes to FSC standard**

1.

**:computer: Changes to FSC implementation**

1.

**:white\_check\_mark: Acceptance criteria**

1.

**:link: Reference links**

1.

**:desktop_computer: How to demonstrate / e2e test(s):**

1.

**:mailbox: Communication activities**

1.
