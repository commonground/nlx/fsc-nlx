# Copyright © VNG Realisatie 2022
# Licensed under the EUPL

services:
  - name: ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/postgres:17.0-alpine@sha256:14195b0729fce792f47ae3c3704d6fd04305826d57af3b01d5b4d004667df174
    alias: postgres
    command:
      - "postgres"
      - "-c"
      - "max_connections=500"

variables:
  POSTGRES_DB: nlx
  POSTGRES_USER: postgres
  POSTGRES_PASSWORD: ""
  POSTGRES_HOST_AUTH_METHOD: trust

Go tests (unit & integration):
  stage: Tests
  image: ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/golang:1.23.2-alpine@sha256:9dd2625a1ff2859b8d8b01d8f7822c0f528942fe56cfe7a1e7c38d3b8d72d679
  variables:
    POSTGRES_DSN: "postgres://postgres@postgres:5432/nlx?sslmode=disable"
  before_script:
    - mkdir -p tmp-coverage-reports/
  script:
    - ./pki/fix-permissions.sh
    - /usr/local/go/bin/go mod download
    - /usr/local/go/bin/go test -tags=integration -coverpkg=./... -coverprofile=tmp-coverage-reports/coverage.out.tmp ./...
    - cat tmp-coverage-reports/coverage.out.tmp | grep -v "mock" | grep -v "migrations" | grep -v "ca-certportal" | grep -v "apps-overview" | grep -v "directory-ui" > tmp-coverage-reports/coverage.out
    - rm tmp-coverage-reports/coverage.out.tmp
    - /usr/local/go/bin/go tool cover -html=tmp-coverage-reports/coverage.out -o tmp-coverage-reports/coverage.html
    - /usr/local/go/bin/go tool cover -func=tmp-coverage-reports/coverage.out
  coverage: /total:\t+\(statements\)\t+([\d\.]+?%)/
  artifacts:
    expire_in: 1 month
    paths:
      - tmp-coverage-reports/coverage.html
  needs: []
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    - if: $CI_COMMIT_BRANCH
      changes:
        compare_to: "main"
        paths:
          - .gitlab/**/*
          - apps-overview/**/*
          - auditlog/**/*
          - ca-certportal/**/*
          - common/**/*
          - controller/**/*
          - directory-ui/**/*
          - inway/**/*
          - manager/**/*
          - outway/**/*
          - testing/**/*
          - txlog-api/**/*
          - e2e-tests/**/*
          - .golang-ci.yml
          - go.mod
          - go.sum
    - if: $CI_PROJECT_PATH != "commonground/nlx/fsc-nlx" && $CI_PIPELINE_SOURCE == "merge_request_event" # run for external fork merge requests
  tags:
    - commonground-k8s-runners
  retry: 1

Go linter:
  stage: Tests
  image: ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/golangci/golangci-lint:v1.61.0-alpine@sha256:61e2d68adc792393fcb600340fe5c28059638d813869d5b4c9502392a2fb4c96
  before_script:
    - export PATH=$PATH:/usr/local/go/bin
    - apk --no-cache add jq
  script:
    - mkdir -p .go-cache .lint-cache
    - golangci-lint version
    # Command based on https://docs.gitlab.com/ee/development/go_guide/index.html#automatic-linting
    # Write the code coverage report to gl-code-quality-report.json
    # and print linting issues to stdout in the format: path/to/file:line description
    # remove `--issues-exit-code 0` or set to non-zero to fail the job if linting issues are detected
    - golangci-lint run --verbose --timeout=5m --new-from-rev $(git rev-parse origin/main) --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  variables:
    GOPATH: ${CI_PROJECT_DIR}/.go-cache
    GOLANGCI_LINT_CACHE: ${CI_PROJECT_DIR}/.lint-cache
  cache:
    key: lintercache
    paths:
      - .go-cache
      - .lint-cache
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
  needs: []
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    - if: $CI_COMMIT_BRANCH # do not run for tags
      changes:
        compare_to: "main"
        paths:
          - .gitlab/**/*
          - auditlog/**/*
          - apps-overview/**/*
          - ca-certportal/**/*
          - common/**/*
          - controller/**/*
          - directory-ui/**/*
          - inway/**/*
          - manager/**/*
          - outway/**/*
          - testing/**/*
          - txlog-api/**/*
          - e2e-tests/**/*
          - .golang-ci.yml
          - go.mod
          - go.sum
    - if: $CI_PROJECT_PATH != "commonground/nlx/fsc-nlx" && $CI_PIPELINE_SOURCE == "merge_request_event" # run for external fork merge requests
  tags:
    - commonground-k8s-runners

Go go.mod tidy test:
  stage: Tests
  image: ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/golang:1.23.2-alpine@sha256:9dd2625a1ff2859b8d8b01d8f7822c0f528942fe56cfe7a1e7c38d3b8d72d679
  script:
    - cp go.mod go.mod.orig
    - /usr/local/go/bin/go mod download
    - /usr/local/go/bin/go mod tidy
    - diff go.mod.orig go.mod
  needs: []
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    - if: $CI_COMMIT_BRANCH # do not run for tags
      changes:
        compare_to: "main"
        paths:
          - .gitlab/**/*
          - auditlog/**/*
          - apps-overview/**/*
          - ca-certportal/**/*
          - common/**/*
          - controller/**/*
          - directory-ui/**/*
          - inway/**/*
          - manager/**/*
          - outway/**/*
          - testing/**/*
          - txlog-api/**/*
          - e2e-tests/**/*
          - .golang-ci.yml
          - go.mod
          - go.sum
    - if: $CI_PROJECT_PATH != "commonground/nlx/fsc-nlx" && $CI_PIPELINE_SOURCE == "merge_request_event" # run for external fork merge requests
  tags:
    - commonground-k8s-runners
