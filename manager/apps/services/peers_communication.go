// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"errors"
	"fmt"

	"golang.org/x/sync/errgroup"
	"golang.org/x/sync/singleflight"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

const maxConcurrentCalls = 5

type LocalPeerRepository interface {
	ListPeersByID(ctx context.Context, peerIDs contract.PeersIDs) (contract.Peers, error)
	ListDirectoryPeers(ctx context.Context) ([]*contract.Peer, error)
	UpsertPeer(ctx context.Context, peer *contract.Peer) error
}

type FailedContractDistributionRepository interface {
	UpsertFailedContractDistribution(ctx context.Context, distribution *contract.FailedDistribution) error
	GetFailedContractDistribution(ctx context.Context, contentHash *contract.ContentHash, peerID contract.PeerID, action contract.FailedDistributionAction) (*contract.FailedDistribution, error)
	ListFailedContractDistributions(ctx context.Context, contentHash *contract.ContentHash) ([]*contract.FailedDistribution, error)
}

type PeersCommunicationService struct {
	ctx                         context.Context
	clock                       clock.Clock
	logger                      *logger.Logger
	contractDistributionService *ContractDistributionService
	localRepository             LocalPeerRepository
	managerFactory              manager.Factory
	selfPeerID                  contract.PeerID
	getPeersFromRepo            *singleflight.Group
	getPeersFromDirectory       *singleflight.Group
}

type NewPeersCommunicationServiceArgs struct {
	Ctx                         context.Context
	Clock                       clock.Clock
	Logger                      *logger.Logger
	LocalRepo                   LocalPeerRepository
	ManagerFactory              manager.Factory
	SelfPeerID                  contract.PeerID
	ContractDistributionService *ContractDistributionService
}

func NewPeersCommunicationService(args *NewPeersCommunicationServiceArgs) (*PeersCommunicationService, error) {
	if args.LocalRepo == nil {
		return nil, errors.New("local peer repository is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.ContractDistributionService == nil {
		return nil, errors.New("contractDistributionService is required")
	}

	return &PeersCommunicationService{
		ctx:                         args.Ctx,
		localRepository:             args.LocalRepo,
		clock:                       args.Clock,
		logger:                      args.Logger,
		managerFactory:              args.ManagerFactory,
		selfPeerID:                  args.SelfPeerID,
		getPeersFromRepo:            &singleflight.Group{},
		getPeersFromDirectory:       &singleflight.Group{},
		contractDistributionService: args.ContractDistributionService,
	}, nil
}

func (p *PeersCommunicationService) UpdateManagerAddress(ctx context.Context) error {
	peers, err := p.localRepository.ListPeersByID(ctx, contract.PeersIDs{})
	if err != nil {
		return fmt.Errorf("could not get all peers from repo: %w", err)
	}

	err = p.Announce(ctx, peers.IDs())
	if err != nil {
		return fmt.Errorf("could not announce to peers: %w", err)
	}

	return nil
}

func (p *PeersCommunicationService) getManagerClient(peerID contract.PeerID) (manager.Manager, error) {
	address, err := p.getManagerAddress(peerID)
	if err != nil {
		return nil, fmt.Errorf("could not get manager address: %w", err)
	}

	c, err := p.managerFactory.New(peerID.Value(), address)
	if err != nil {
		return nil, fmt.Errorf("could not create peer client: %w", err)
	}

	return c, nil
}

func (p *PeersCommunicationService) getManagerAddress(peerID contract.PeerID) (string, error) {
	peer, err := p.getPeerFromRepo(peerID)
	if err != nil {
		return "", fmt.Errorf("could not get manager address from peer repository: %w", err)
	}

	if peer != nil && peer.ManagerAddress().Value() != "" {
		return peer.ManagerAddress().Value(), nil
	}

	peer, err = p.getPeerFromDirectory(peerID)
	if err == nil {
		return peer.ManagerAddress().Value(), nil
	}

	return "", fmt.Errorf("could not get manager address from directory: %w", err)
}

func (p *PeersCommunicationService) getPeerFromRepo(peerID contract.PeerID) (*contract.Peer, error) {
	peer, err, _ := p.getPeersFromRepo.Do(peerID.Value(), func() (interface{}, error) {
		peers, err := p.localRepository.ListPeersByID(p.ctx, contract.PeersIDs{
			peerID: true,
		})
		if err != nil {
			return nil, fmt.Errorf("could not get peer from peer repository: %w", err)
		}

		peer, ok := peers[peerID]
		if !ok {
			return nil, nil
		}

		return peer, nil
	})
	if err != nil {
		return nil, err
	}

	if peer == nil {
		return nil, nil
	}

	return peer.(*contract.Peer), nil
}

func (p *PeersCommunicationService) getPeerFromDirectory(peerID contract.PeerID) (*contract.Peer, error) {
	peer, err, _ := p.getPeersFromDirectory.Do(peerID.Value(), func() (interface{}, error) {
		directoryPeers, err := p.localRepository.ListDirectoryPeers(context.Background())
		if err != nil {
			return nil, errors.Join(err, fmt.Errorf("could not get directory peers"))
		}

		for _, directoryPeer := range directoryPeers {
			var managerClient manager.Manager

			managerClient, err = p.managerFactory.New(directoryPeer.ID().Value(), directoryPeer.ManagerAddress().Value())
			if err != nil {
				return nil, errors.Join(err, fmt.Errorf("could create client for directory peer %q", directoryPeer.ManagerAddress().Value()))
			}

			var peers contract.Peers

			peers, err = managerClient.GetPeers(p.ctx, contract.PeersIDs{
				peerID: true,
			})
			if err != nil {
				return nil, fmt.Errorf("could not get peer from directory: %w", err)
			}

			peer, ok := peers[peerID]
			if !ok {
				continue
			}

			err = p.localRepository.UpsertPeer(p.ctx, peer)
			if err != nil {
				p.logger.Warn("could not upsert peer", err)
			}

			return peer, nil
		}

		return nil, errors.New("peer not found in directory")
	})
	if err != nil {
		return nil, err
	}

	return peer.(*contract.Peer), nil
}

func (p *PeersCommunicationService) sendToPeers(peers contract.PeersIDs, action func(peerID contract.PeerID) error) error {
	errs := errgroup.Group{}
	errs.SetLimit(maxConcurrentCalls)

	for peerID := range peers {
		peerIDReceiver := peerID

		errs.Go(func() error {
			if p.selfPeerID == peerIDReceiver {
				return nil
			}

			return action(peerIDReceiver)
		})
	}

	return errs.Wait()
}
