// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"fmt"

	"github.com/cenkalti/backoff/v4"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (p *PeersCommunicationService) Announce(ctx context.Context, peers contract.PeersIDs) error {
	return p.sendToPeers(peers, func(peerID contract.PeerID) error {
		announce := func() error {
			managerClient, err := p.getManagerClient(peerID)
			if err != nil {
				return fmt.Errorf("could not get client for peer %q: %w", peerID.Value(), err)
			}

			err = managerClient.Announce(ctx)
			if err != nil {
				p.logger.Debug(fmt.Sprintf("could not announce peer %q: %s", peerID.Value(), err.Error()))
				return fmt.Errorf("could not announce to peer %q: %w", peerID.Value(), err)
			}

			return nil
		}

		err := backoff.Retry(announce, backoff.WithContext(backoff.NewExponentialBackOff(), ctx))
		if err != nil {
			return fmt.Errorf("permanently failed to announce to peer: %w", err)
		}

		return nil
	})
}
