// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"fmt"
	"time"

	"github.com/cenkalti/backoff/v4"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (p *PeersCommunicationService) SubmitContract(_ context.Context, peerIDs contract.PeersIDs, contractContent *contract.Content, sig *contract.Signature) error {
	retryContext := context.Background()

	return p.sendToPeers(peerIDs, func(peerID contract.PeerID) error {
		performAction := func() error {
			managerClient, err := p.getManagerClient(peerID)
			if err != nil {
				return fmt.Errorf("could not get client for peer %q: %w", peerID.Value(), err)
			}

			err = managerClient.SubmitContract(retryContext, contractContent, sig)
			if err != nil {
				p.logger.Warn("failed to submit contract to peer", err)

				return fmt.Errorf("could not submit contract %q: %w", peerID.Value(), err)
			}

			return nil
		}

		notify := func(err error, delay time.Duration) {
			nextRetry := p.clock.Now().Add(delay)

			updateDistributionErr := p.contractDistributionService.UpdateNewAttempt(contract.FailedDistributionActionSubmitContract, contractContent.Hash(), peerID, sig, err, &nextRetry)
			if updateDistributionErr != nil {
				p.logger.Error("could not update contract distribution", updateDistributionErr)
			}
		}

		go func() {
			err := backoff.RetryNotify(performAction, backoff.WithContext(backoff.NewExponentialBackOff(), retryContext), notify)
			if err != nil {
				p.logger.Error("permanently failed to communicate with other Peer", err)

				updateDistributionErr := p.contractDistributionService.UpdateWithLastAttempt(contract.FailedDistributionActionSubmitContract, contractContent.Hash(), peerID, sig, err)
				if updateDistributionErr != nil {
					p.logger.Error("could not update contract distribution", updateDistributionErr)
				}
			}
		}()

		return nil
	})
}
