// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package services_test

import (
	"context"
	"crypto/x509"
	"log"
	"os"
	"path"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	mock_manager "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager/mock"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

const directoryManagerAddress = "https://directory.nlx.local:443"
const directoryPeerID = "00000000000000000001"
const peerBManagerAddress = "https://manager.org-b.nlx.local:443"

// announce should not happen if receiver is self
func TestPeers_AnnounceToSelf(t *testing.T) {
	peerACertBundle, err := testingutils.GetCertificateBundle(path.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestPeerA)
	if err != nil {
		log.Fatalf("failed to load organization a cert bundle: %v", err)
	}

	testclock := testingutils.NewMockClock(time.Now())

	repository := createRepository(t, peerACertBundle.RootCAs(), testclock)

	selfPeerID, err := contract.NewPeerID(peerACertBundle.GetPeerInfo().SerialNumber)
	assert.NoError(t, err)

	// Arrange
	managerFactory := mock_manager.NewMockFactory(t)

	testClock := testingutils.NewMockClock(time.Now())

	contractDistributionService, err := services.NewContractDistributionService(&services.NewContractDistributionServiceArgs{
		Context:                              context.Background(),
		Logger:                               discardlogger.New(),
		Clock:                                testClock,
		FailedContractDistributionRepository: repository,
	})
	assert.NoError(t, err)

	peersRepository, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
		Ctx:                         context.Background(),
		Logger:                      discardlogger.New(),
		LocalRepo:                   repository,
		ManagerFactory:              managerFactory,
		SelfPeerID:                  selfPeerID,
		ContractDistributionService: contractDistributionService,
		Clock:                       testClock,
	})
	assert.NoError(t, err)

	// Act
	err = peersRepository.Announce(context.Background(), contract.PeersIDs{
		contract.PeerID(peerACertBundle.GetPeerInfo().SerialNumber): true,
	})

	// Assert
	assert.NoError(t, err)
}

// peer address from the database should be used. no call to the directory should be made
func TestPeers_AnnounceToKnownPeer(t *testing.T) {
	peerACertBundle, err := testingutils.GetCertificateBundle(path.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestPeerA)
	if err != nil {
		log.Fatalf("failed to load organization a cert bundle: %v", err)
	}

	peerBCertBundle, err := testingutils.GetCertificateBundle(path.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestPeerB)
	if err != nil {
		log.Fatalf("failed to load organization b cert bundle: %v", err)
	}

	testClock := testingutils.NewMockClock(time.Now())

	repository := createRepository(t, peerACertBundle.RootCAs(), testClock)

	// Arrange
	peersContext := context.Background()

	manager := mock_manager.NewMockManager(t)
	manager.EXPECT().Announce(peersContext).Return(nil)

	managerFactory := mock_manager.NewMockFactory(t)
	managerFactory.EXPECT().New(peerBCertBundle.GetPeerInfo().SerialNumber, peerBManagerAddress).Return(manager, nil)

	contractDistributionService, err := services.NewContractDistributionService(&services.NewContractDistributionServiceArgs{
		Context:                              context.Background(),
		Logger:                               discardlogger.New(),
		Clock:                                testClock,
		FailedContractDistributionRepository: repository,
	})
	assert.NoError(t, err)

	p, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
		Ctx:                         context.Background(),
		Logger:                      discardlogger.New(),
		LocalRepo:                   repository,
		ContractDistributionService: contractDistributionService,
		ManagerFactory:              managerFactory,
		SelfPeerID:                  "",
		Clock:                       testClock,
	})
	assert.NoError(t, err)

	peer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerBCertBundle.GetPeerInfo().SerialNumber,
		Name:           peerBCertBundle.GetPeerInfo().Name,
		ManagerAddress: peerBManagerAddress,
	})
	assert.NoError(t, err)

	err = repository.UpsertPeer(context.Background(), peer)
	assert.NoError(t, err)

	// Act
	err = p.Announce(context.Background(), contract.PeersIDs{
		contract.PeerID(peerBCertBundle.GetPeerInfo().SerialNumber): true,
	})

	// Assert
	assert.NoError(t, err)
}

// peer address from the directory should be used
func TestPeers_AnnounceToUnknownPeer(t *testing.T) {
	peerACertBundle, err := testingutils.GetCertificateBundle(path.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestPeerA)
	if err != nil {
		log.Fatalf("failed to load peer a cert bundle: %v", err)
	}

	peerBCertBundle, err := testingutils.GetCertificateBundle(path.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestPeerB)
	if err != nil {
		log.Fatalf("failed to load peer b cert bundle: %v", err)
	}

	testclock := testingutils.NewMockClock(time.Now())

	repository := createRepository(t, peerACertBundle.RootCAs(), testclock)

	directoryPeer, err := contract.NewPeer(
		&contract.NewPeerArgs{
			ID:             directoryPeerID,
			Name:           "directory",
			ManagerAddress: directoryManagerAddress,
			Roles:          []contract.PeerRole{contract.PeerRoleDirectory},
		})
	require.NoError(t, err)

	err = repository.UpsertPeerWithRoles(context.Background(), directoryPeer)
	assert.NoError(t, err)

	peersContext := context.Background()

	// Arrange
	peerIDs, err := contract.NewPeerIDs([]string{
		peerBCertBundle.GetPeerInfo().SerialNumber,
	})
	assert.NoError(t, err)

	orgBPeerInfo, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerBCertBundle.GetPeerInfo().SerialNumber,
		Name:           peerBCertBundle.GetPeerInfo().Name,
		ManagerAddress: peerBManagerAddress,
	})
	assert.NoError(t, err)

	directoryManager := mock_manager.NewMockManager(t)
	directoryManager.
		EXPECT().
		GetPeers(peersContext, peerIDs).
		Return(contract.Peers{
			orgBPeerInfo.ID(): orgBPeerInfo,
		}, nil)

	orgBManager := mock_manager.NewMockManager(t)
	orgBManager.EXPECT().Announce(peersContext).Return(nil)

	managerFactory := mock_manager.NewMockFactory(t)
	managerFactory.EXPECT().New(directoryPeerID, directoryManagerAddress).Return(directoryManager, nil)
	managerFactory.EXPECT().New(orgBPeerInfo.ID().Value(), peerBManagerAddress).Return(orgBManager, nil)

	testClock := testingutils.NewMockClock(time.Now())

	contractDistributionService, err := services.NewContractDistributionService(&services.NewContractDistributionServiceArgs{
		Context:                              context.Background(),
		Logger:                               discardlogger.New(),
		Clock:                                testClock,
		FailedContractDistributionRepository: repository,
	})
	assert.NoError(t, err)

	peersRepository, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
		Ctx:                         context.Background(),
		Logger:                      discardlogger.New(),
		LocalRepo:                   repository,
		ContractDistributionService: contractDistributionService,
		Clock:                       testClock,
		ManagerFactory:              managerFactory,
		SelfPeerID:                  "",
	})
	assert.NoError(t, err)

	// Act
	err = peersRepository.Announce(context.Background(), contract.PeersIDs{
		contract.PeerID(peerBCertBundle.GetPeerInfo().SerialNumber): true,
	})

	// Assert
	assert.NoError(t, err)
}

func createRepository(t *testing.T, trustedRootCAs *x509.CertPool, c clock.Clock) contract.Repository {
	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(t.Name())
	dbName = strings.ReplaceAll(dbName, "/", "_")

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		log.Fatalf("failed to setup test database: %v", err)
	}

	db, err := postgresadapter.NewConnection(context.Background(), testDB)
	if err != nil {
		log.Fatalf("failed to create db connection: %v", err)
	}

	err = postgresadapter.PerformMigrations(testDB)
	if err != nil {
		log.Fatalf("failed to perform dbmigrations: %v", err)
	}

	postgresqlRepository, err := postgresadapter.New(trustedRootCAs, db, c)
	if err != nil {
		log.Fatalf("failed to setup postgresql database: %s", err)
	}

	return postgresqlRepository
}
