// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package services

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ContractDistributionService struct {
	context                              context.Context
	logger                               *logger.Logger
	clock                                clock.Clock
	failedContractDistributionRepository FailedContractDistributionRepository
}

type NewContractDistributionServiceArgs struct {
	Context                              context.Context
	Logger                               *logger.Logger
	Clock                                clock.Clock
	FailedContractDistributionRepository FailedContractDistributionRepository
}

func NewContractDistributionService(args *NewContractDistributionServiceArgs) (*ContractDistributionService, error) {
	if args.Logger == nil {
		return nil, errors.New("missing logger")
	}

	if args.Clock == nil {
		return nil, errors.New("missing clock")
	}

	if args.FailedContractDistributionRepository == nil {
		return nil, errors.New("missing contract distribution repository")
	}

	if args.Context == nil {
		return nil, errors.New("missing context")
	}

	return &ContractDistributionService{
		context:                              args.Context,
		logger:                               args.Logger,
		clock:                                args.Clock,
		failedContractDistributionRepository: args.FailedContractDistributionRepository,
	}, nil
}

func (c *ContractDistributionService) UpdateWithLastAttempt(action contract.FailedDistributionAction, contractContentHash *contract.ContentHash, peerID contract.PeerID, signature *contract.Signature, err error) error {
	return c.update(action, contractContentHash, peerID, signature, err, nil)
}

func (c *ContractDistributionService) UpdateNewAttempt(action contract.FailedDistributionAction, contractContentHash *contract.ContentHash, peerID contract.PeerID, signature *contract.Signature, err error, nextRetry *time.Time) error {
	return c.update(action, contractContentHash, peerID, signature, err, nextRetry)
}

func (c *ContractDistributionService) update(action contract.FailedDistributionAction, contractContentHash *contract.ContentHash, peerID contract.PeerID, signature *contract.Signature, err error, nextRetry *time.Time) error {
	distribution, errDistribution := c.failedContractDistributionRepository.GetFailedContractDistribution(c.context, contractContentHash, peerID, action)
	if errDistribution != nil {
		return errors.Join(errDistribution, fmt.Errorf("could not retrieve contract distribution"))
	}

	if distribution == nil {
		newDistribution, errNewDistribution := contract.NewFailedDistribution(&contract.NewFailedDistributionArgs{
			ContentHash:   contractContentHash,
			PeerID:        peerID.Value(),
			Action:        string(action),
			Signature:     signature.JWS(),
			Attempts:      1,
			NextAttemptAt: nextRetry,
			LastAttemptAt: c.clock.Now(),
			Reason:        err.Error(),
		})
		if errNewDistribution != nil {
			return errors.Join(errNewDistribution, fmt.Errorf("could not instantiate contract distribution"))
		}

		distribution = newDistribution
	} else {
		distribution.UpdateWithNewAttempt(c.clock.Now(), err.Error(), nextRetry)
	}

	errSaveDistribution := c.failedContractDistributionRepository.UpsertFailedContractDistribution(c.context, distribution)
	if errSaveDistribution != nil {
		return errors.Join(errSaveDistribution, fmt.Errorf("could not save contract distribution"))
	}

	return nil
}

func (c *ContractDistributionService) ClearAllNextAttemptTimestamps() error {
	distributions, err := c.failedContractDistributionRepository.ListFailedContractDistributions(c.context, nil)
	if err != nil {
		return errors.Join(err, fmt.Errorf("could not retrieve contract distribution"))
	}

	for _, distribution := range distributions {
		distribution.ClearNextRetry()

		errSaveDistribution := c.failedContractDistributionRepository.UpsertFailedContractDistribution(c.context, distribution)
		if errSaveDistribution != nil {
			return errors.Join(errSaveDistribution, fmt.Errorf("could not save contract distribution"))
		}
	}

	return nil
}
