// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"fmt"
	"sync"

	"golang.org/x/sync/errgroup"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (p *PeersCommunicationService) GetCertificates(ctx context.Context, peerCertificateThumbprints map[contract.PeerID][]string) (contract.PeersCertificates, error) {
	responseLock := sync.Mutex{}
	peersCertificates := contract.PeersCertificates{}

	errs := errgroup.Group{}
	errs.SetLimit(maxConcurrentCalls)

	for peerID := range peerCertificateThumbprints {
		peerID := peerID

		errs.Go(func() error {
			peer, err := p.getManagerClient(peerID)
			if err != nil {
				return fmt.Errorf("could not get client for peer %q to get certificates: %w", peerID.Value(), err)
			}

			certs, err := peer.GetCertificates(ctx)
			if err != nil {
				return err
			}

			responseLock.Lock()
			peersCertificates[peerID] = certs
			responseLock.Unlock()

			return nil
		})
	}

	err := errs.Wait()
	if err != nil {
		return nil, err
	}

	return peersCertificates, nil
}
