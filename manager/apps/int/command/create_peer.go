// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/authentication"
)

type NewCreatePeerHandlerArgs struct {
	Repository contract.Repository
	Logger     *logger.Logger
	AuditLgr   *auditlog.AuditLogger
}

type CreatePeerHandler struct {
	repository contract.Repository
	logger     *logger.Logger
	auditLgr   *auditlog.AuditLogger
}

type PeerRole string

const (
	PeerRoleDirectory PeerRole = "directory"
)

func NewCreatePeerHandler(args *NewCreatePeerHandlerArgs) (*CreatePeerHandler, error) {
	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.AuditLgr == nil {
		return nil, errors.New("audit logger is required")
	}

	return &CreatePeerHandler{
		repository: args.Repository,
		logger:     args.Logger,
		auditLgr:   args.AuditLgr,
	}, nil
}

type CreatePeerArgs struct {
	PeerID                string
	PeerName              string
	ManagerAddress        string
	Roles                 []PeerRole
	AuditLogSource        common_auditlog.Source
	AuthData              authentication.Data
	AuditlogCorrelationID string
}

func (h *CreatePeerHandler) Handle(ctx context.Context, args *CreatePeerArgs) error {
	handlerFunc := func() (any, common_auditlog.Event, error) {
		roles := make([]contract.PeerRole, 0, len(args.Roles))

		for _, role := range args.Roles {
			switch role {
			case PeerRoleDirectory:
				roles = append(roles, contract.PeerRoleDirectory)
			default:
				return nil, common_auditlog.EventCreatePeer{}, fmt.Errorf("unknown peer role: %s", role)
			}
		}

		peer, err := contract.NewPeer(
			&contract.NewPeerArgs{
				ID:             args.PeerID,
				Name:           args.PeerName,
				ManagerAddress: args.ManagerAddress,
				Roles:          roles,
			},
		)
		if err != nil {
			return "", nil, internalapp_errors.NewIncorrectInputError(err)
		}

		if args.PeerName == "" {
			return "", nil, internalapp_errors.NewIncorrectInputError(fmt.Errorf("peer name can not be empty"))
		}

		err = h.repository.UpsertPeerWithRoles(ctx, peer)
		if err != nil {
			h.logger.Error("could not upsert peer with roles", err)
			return "", nil, err
		}

		return nil, common_auditlog.EventCreatePeer{PeerID: args.PeerID}, nil
	}

	_, err := h.auditLgr.AddRecordWithResultAndEvent(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData: args.AuthData,
		Event: common_auditlog.EventCreatePeer{
			PeerID: args.PeerID,
		},
		Source:        args.AuditLogSource,
		CorrelationID: args.AuditlogCorrelationID,
	})
	if err != nil {
		return mapError(err, "could not create auditLog record for create peer")
	}

	return nil
}
