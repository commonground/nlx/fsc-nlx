// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	"github.com/lestrrat-go/jwx/v2/jws"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/authentication"
)

type RetryContractDistributionHandler struct {
	repository               contract.Repository
	peerCommunicationService *services.PeersCommunicationService
	auditLgr                 *auditlog.AuditLogger
}

type NewRetryContractDistributionHandlerArgs struct {
	Repository               contract.Repository
	PeerCommunicationService *services.PeersCommunicationService
	AuditLgr                 *auditlog.AuditLogger
}

type RetryContractDistributionHandlerArgs struct {
	ContentHash           string
	PeerID                string
	Action                Action
	AuditLogSource        common_auditlog.Source
	AuthData              authentication.Data
	AuditlogCorrelationID string
}

type Action int

const (
	DistributionActionSubmitContract Action = iota
	DistributionActionSubmitAcceptSignature
	DistributionActionSubmitRejectSignature
	DistributionActionSubmitRevokeSignature
)

func NewRetryContractDistributionHandler(args *NewRetryContractDistributionHandlerArgs) (*RetryContractDistributionHandler, error) {
	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.PeerCommunicationService == nil {
		return nil, errors.New("peerCommunicationService is required")
	}

	if args.AuditLgr == nil {
		return nil, fmt.Errorf("audit logger is required")
	}

	return &RetryContractDistributionHandler{
		repository:               args.Repository,
		peerCommunicationService: args.PeerCommunicationService,
		auditLgr:                 args.AuditLgr,
	}, nil
}

func (h *RetryContractDistributionHandler) Handle(ctx context.Context, args *RetryContractDistributionHandlerArgs) error {
	handlerFunc := func() error {
		hash, err := contract.NewContentHashFromString(args.ContentHash)
		if err != nil {
			return internalapp_errors.NewIncorrectInputError(err)
		}

		pID, err := contract.NewPeerID(args.PeerID)
		if err != nil {
			return internalapp_errors.NewIncorrectInputError(err)
		}

		distributionAction, err := convertAction(args.Action)
		if err != nil {
			return internalapp_errors.NewIncorrectInputError(err)
		}

		cd, err := h.repository.GetFailedContractDistribution(ctx, hash, pID, distributionAction)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not retrieve contract distribution"))
		}

		contractContent, err := h.repository.GetContentByHash(ctx, cd.ContentHash())
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not get contract content of hash %q", cd.ContentHash().String()))
		}

		certificateThumbprint, err := getCertificateThumbprintFromSignature(cd.Signature())
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not get certificate thumbprint from signature"))
		}

		peerCerts, err := h.repository.GetPeersCertsByCertThumbprints(ctx, contract.CertificateThumbprints{contract.CertificateThumbprint(certificateThumbprint)})
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not get peer certs by certificate thumbprint"))
		}

		err = h.retry(ctx, distributionAction, cd, contractContent, peerCerts)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not retry contract distribution"))
		}

		err = h.repository.DeleteFailedContractDistributions(ctx, pID, hash, distributionAction)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not delete contract distribution"))
		}

		return nil
	}

	err := h.auditLgr.AddRecord(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData: args.AuthData,
		Event: common_auditlog.EventFailedDistributionRetry{
			ContentHash: args.ContentHash,
			PeerID:      args.PeerID,
		},
		Source:        args.AuditLogSource,
		CorrelationID: args.AuditlogCorrelationID,
	})
	if err != nil {
		return mapError(err, "could not create auditLog record for retry contract distribution")
	}

	return nil
}

func convertAction(action Action) (contract.FailedDistributionAction, error) {
	switch action {
	case DistributionActionSubmitContract:
		return contract.FailedDistributionActionSubmitContract, nil
	case DistributionActionSubmitAcceptSignature:
		return contract.FailedDistributionActionSubmitAcceptSignature, nil
	case DistributionActionSubmitRevokeSignature:
		return contract.FailedDistributionActionSubmitRevokeSignature, nil
	case DistributionActionSubmitRejectSignature:
		return contract.FailedDistributionActionSubmitRejectSignature, nil
	default:
		return "", errors.New("provided ContractDistributionRetry Action is not valid")
	}
}

//nolint:gocyclo // retry is a complex function because the many distribution actions
func (h *RetryContractDistributionHandler) retry(ctx context.Context, a contract.FailedDistributionAction, cd *contract.FailedDistribution, contractContent *contract.Content, peerCerts *contract.PeersCertificates) error {
	peerIDs, err := contract.NewPeerIDs([]string{cd.PeerID().Value()})
	if err != nil {
		return errors.Join(err, fmt.Errorf("could not parse peer ID %q", cd.PeerID()))
	}

	switch a {
	case contract.FailedDistributionActionSubmitRejectSignature:
		rejectSignature, errCreateSig := contract.NewSignature(&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeReject,
			Signature:  cd.Signature(),
			Content:    contractContent,
			PeersCerts: *peerCerts,
		})
		if errCreateSig != nil {
			return errors.Join(errCreateSig, fmt.Errorf("could not create reject signature"))
		}

		err = h.peerCommunicationService.RejectContract(ctx, peerIDs, contractContent, rejectSignature)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not reject contract"))
		}

	case contract.FailedDistributionActionSubmitAcceptSignature:
		acceptSignature, errCreateSig := contract.NewSignature(&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeAccept,
			Signature:  cd.Signature(),
			Content:    contractContent,
			PeersCerts: *peerCerts,
		})
		if errCreateSig != nil {
			return errors.Join(errCreateSig, fmt.Errorf("could not create accept signature"))
		}

		err = h.peerCommunicationService.AcceptContract(ctx, peerIDs, contractContent, acceptSignature)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not accept contract"))
		}

	case contract.FailedDistributionActionSubmitRevokeSignature:
		revokeSignature, errCreateSig := contract.NewSignature(&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeRevoke,
			Signature:  cd.Signature(),
			Content:    contractContent,
			PeersCerts: *peerCerts,
		})
		if errCreateSig != nil {
			return errors.Join(errCreateSig, fmt.Errorf("could not create accept signature"))
		}

		err = h.peerCommunicationService.RevokeContract(ctx, peerIDs, contractContent, revokeSignature)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not revoke contract"))
		}
	case contract.FailedDistributionActionSubmitContract:
		acceptSignature, errCreateSig := contract.NewSignature(&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeAccept,
			Signature:  cd.Signature(),
			Content:    contractContent,
			PeersCerts: *peerCerts,
		})
		if errCreateSig != nil {
			return errors.Join(errCreateSig, fmt.Errorf("could not create accept signature"))
		}

		err = h.peerCommunicationService.SubmitContract(ctx, peerIDs, contractContent, acceptSignature)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not submit contract"))
		}
	default:
		return fmt.Errorf("unexpected action %s", a)
	}

	return nil
}

func getCertificateThumbprintFromSignature(signature string) (string, error) {
	parsedSignature, err := jws.ParseString(signature)
	if err != nil {
		return "", fmt.Errorf("could not parse signature: %v", err)
	}

	if len(parsedSignature.Signatures()) != 1 {
		return "", fmt.Errorf("exactly one signature is required, found either zero or multiple signatures in jws token")
	}

	headers := parsedSignature.Signatures()[0].ProtectedHeaders()
	if headers == nil {
		return "", fmt.Errorf("no headers present in jws signature")
	}

	pubKeyFingerPrintString := headers.X509CertThumbprintS256()
	if pubKeyFingerPrintString == "" {
		return "", fmt.Errorf("could not find required header in jws signature: x5t#S256")
	}

	return pubKeyFingerPrintString, nil
}
