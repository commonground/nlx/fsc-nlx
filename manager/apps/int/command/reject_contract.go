// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"time"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/authentication"
)

type RejectContractHandler struct {
	trustedExternalRootCAs    *x509.CertPool
	clock                     clock.Clock
	repository                contract.Repository
	logger                    *logger.Logger
	peersCommunicationService *services.PeersCommunicationService
	signatureCert             *tls.Certificate
	auditLgr                  *auditlog.AuditLogger
}

type NewRejectContractHandlerArgs struct {
	TrustedExternalRootCAs    *x509.CertPool
	Repository                contract.Repository
	PeersCommunicationService *services.PeersCommunicationService
	Clock                     clock.Clock
	Logger                    *logger.Logger
	SignatureCert             *tls.Certificate
	AuditLgr                  *auditlog.AuditLogger
}

func NewRejectContractHandler(args *NewRejectContractHandlerArgs) (*RejectContractHandler, error) {
	if args.TrustedExternalRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.PeersCommunicationService == nil {
		return nil, errors.New("peers communication service is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.SignatureCert == nil {
		return nil, errors.New("signature cert is required")
	}

	if args.AuditLgr == nil {
		return nil, fmt.Errorf("audit logger is required")
	}

	return &RejectContractHandler{
		trustedExternalRootCAs:    args.TrustedExternalRootCAs,
		repository:                args.Repository,
		peersCommunicationService: args.PeersCommunicationService,
		clock:                     args.Clock,
		logger:                    args.Logger,
		signatureCert:             args.SignatureCert,
		auditLgr:                  args.AuditLgr,
	}, nil
}

type RejectContractHandlerArgs struct {
	ContentHash           string
	AuditLogSource        common_auditlog.Source
	AuthData              authentication.Data
	AuditlogCorrelationID string
}

func (h *RejectContractHandler) Handle(ctx context.Context, args *RejectContractHandlerArgs) error {
	handlerFunc := func() error {
		hash, err := contract.NewContentHashFromString(args.ContentHash)
		if err != nil {
			return internalapp_errors.NewIncorrectInputError(err)
		}

		contractContent, err := h.repository.GetContentByHash(ctx, hash)
		if err != nil {
			return fmt.Errorf("could not retrieve content by hash: %w", err)
		}

		sig, err := contractContent.Reject(h.trustedExternalRootCAs, h.signatureCert, h.clock.Now().Truncate(time.Second))
		if err != nil {
			return fmt.Errorf("unable to place a reject signature on content %w", err)
		}

		err = h.repository.UpsertSignature(ctx, sig)
		if err != nil {
			h.logger.Error("could not upsert signature", err)
			return err
		}

		err = h.peersCommunicationService.RejectContract(ctx, contractContent.PeersIDs(), contractContent, sig)
		if err != nil {
			return fmt.Errorf("could not send signed rejected content to peers: %w", err)
		}

		return nil
	}

	err := h.auditLgr.AddRecord(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData:      args.AuthData,
		Event:         common_auditlog.EventRejectContract{ContractHash: args.ContentHash},
		Source:        args.AuditLogSource,
		CorrelationID: args.AuditlogCorrelationID,
	})
	if err != nil {
		return mapError(err, "could not create auditLog record for reject contract")
	}

	return nil
}
