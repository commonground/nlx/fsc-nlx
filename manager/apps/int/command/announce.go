// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/x509"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type AnnouncePeerHandler struct {
	trustedExternalRootCAs    *x509.CertPool
	logger                    *logger.Logger
	peersCommunicationService *services.PeersCommunicationService
}

func NewAnnouncePeerHandler(trustedExternalRootCAs *x509.CertPool, peersCommunicationService *services.PeersCommunicationService, lgr *logger.Logger) (*AnnouncePeerHandler, error) {
	if trustedExternalRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if peersCommunicationService == nil {
		return nil, errors.New("peers communication service is required")
	}

	if lgr == nil {
		return nil, errors.New("logger is required")
	}

	return &AnnouncePeerHandler{
		trustedExternalRootCAs:    trustedExternalRootCAs,
		peersCommunicationService: peersCommunicationService,
		logger:                    lgr,
	}, nil
}

func (h *AnnouncePeerHandler) Handle(ctx context.Context, peerIDs []string) error {
	ids, err := contract.NewPeerIDs(peerIDs)
	if err != nil {
		return internalapp_errors.NewIncorrectInputError(err)
	}

	err = h.peersCommunicationService.Announce(ctx, ids)
	if err != nil {
		return fmt.Errorf("could not announce to peers: %w", err)
	}

	return nil
}
