// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/x509"
	"github.com/pkg/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type CreateCertificateHandler struct {
	trustedExternalRootCAs *x509.CertPool
	repository             contract.Repository
	clock                  clock.Clock
}

func NewCreateCertificateHandler(trustedExternalRootCAs *x509.CertPool, repository contract.Repository, c clock.Clock) (*CreateCertificateHandler, error) {
	if trustedExternalRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if c == nil {
		return nil, errors.New("clock is required")
	}

	if repository == nil {
		return nil, errors.New("repository is required")
	}

	return &CreateCertificateHandler{
		trustedExternalRootCAs: trustedExternalRootCAs,
		repository:             repository,
		clock:                  c,
	}, nil
}

func (h *CreateCertificateHandler) Handle(ctx context.Context, certBytes [][]byte) error {
	peerCert, err := contract.NewPeerCertFromCertificate(h.clock, h.trustedExternalRootCAs, certBytes)
	if err != nil {
		return errors.Wrap(err, "could not create peer certificate")
	}

	// Upsert ourselves into the repository
	err = h.repository.UpsertCertificate(ctx, peerCert)
	if err != nil {
		return errors.Wrapf(err, "could not upsert peer certificate")
	}

	return nil
}
