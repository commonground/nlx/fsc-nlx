// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	"golang.org/x/sync/errgroup"
	"golang.org/x/sync/singleflight"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

const maxConcurrentCalls = 5

type SyncPeersHandler struct {
	managerFactory       manager.Factory
	directoryConnections *singleflight.Group
	repository           contract.Repository
	logger               *logger.Logger
}

type NewSyncPeersHandlerArgs struct {
	ManagerFactory manager.Factory
	Repository     contract.Repository
	Logger         *logger.Logger
}

func NewSyncPeersHandler(args *NewSyncPeersHandlerArgs) (*SyncPeersHandler, error) {
	if args.ManagerFactory == nil {
		return nil, errors.New("managerFactory is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	return &SyncPeersHandler{
		managerFactory:       args.ManagerFactory,
		directoryConnections: &singleflight.Group{},
		repository:           args.Repository,
		logger:               args.Logger,
	}, nil
}

type SynchronizationResult struct {
	Ok    bool
	Error error
}

func (h *SyncPeersHandler) Handle(ctx context.Context) (map[contract.PeerID]*SynchronizationResult, error) {
	directoryPeers, err := h.repository.ListDirectoryPeers(ctx)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not get Directory peers from repository"))
	}

	result := make(map[contract.PeerID]*SynchronizationResult)

	if len(directoryPeers) == 0 {
		return result, nil
	}

	errs := errgroup.Group{}
	errs.SetLimit(maxConcurrentCalls)

	for _, directoryPeer := range directoryPeers {
		var res interface{}

		errs.Go(func() error {
			res, err, _ = h.directoryConnections.Do(directoryPeer.ManagerAddress().Value(), func() (interface{}, error) {
				var directoryManagerClient manager.Manager

				directoryManagerClient, err = h.managerFactory.New(directoryPeer.ID().Value(), directoryPeer.ManagerAddress().Value())
				if err != nil {
					return nil, fmt.Errorf("could not create directory peer client: %w", err)
				}

				res, err = directoryManagerClient.GetPeers(ctx, nil)
				if err != nil {
					result[directoryPeer.ID()] = &SynchronizationResult{Ok: false, Error: err}

					return nil, fmt.Errorf("could not retrieve peers from directory: %w", err)
				}

				result[directoryPeer.ID()] = &SynchronizationResult{Ok: true}

				return res, nil
			})

			if err != nil {
				h.logger.Error("could not retrieve peers from directory", err)

				res = contract.Peers{}
			}

			peers := res.(contract.Peers)

			for _, peer := range peers {
				err = h.repository.UpsertPeer(ctx, peer)
				if err != nil {
					h.logger.Warn("could not upsert peer", err)
				}
			}

			return nil
		})
	}

	return result, errs.Wait()
}
