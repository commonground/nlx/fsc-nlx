// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"fmt"
)

func mapError(err error, message string) error {
	return fmt.Errorf("%s: %w", message, err)
}
