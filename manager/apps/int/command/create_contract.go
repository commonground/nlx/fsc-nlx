// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

type NewCreateContractHandlerArgs struct {
	TrustedExternalRootCAs    *x509.CertPool
	Repository                contract.Repository
	PeersCommunicationService *services.PeersCommunicationService
	Clock                     clock.Clock
	Logger                    *logger.Logger
	SignatureCert             *tls.Certificate
	SelfPeerID                contract.PeerID
	AuditLgr                  *auditlog.AuditLogger
}

type CreateContractHandler struct {
	trustedExternalRootCAs    *x509.CertPool
	clock                     clock.Clock
	repository                contract.Repository
	logger                    *logger.Logger
	peersCommunicationService *services.PeersCommunicationService
	signatureCert             *tls.Certificate
	selfPeerID                *contract.PeerID
	auditLgr                  *auditlog.AuditLogger
}

func NewCreateContractHandler(args *NewCreateContractHandlerArgs) (*CreateContractHandler, error) {
	if args.TrustedExternalRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.PeersCommunicationService == nil {
		return nil, errors.New("peers communication service is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.SignatureCert == nil {
		return nil, errors.New("signature cert is required")
	}

	if args.AuditLgr == nil {
		return nil, errors.New("audit logger is required")
	}

	return &CreateContractHandler{
		trustedExternalRootCAs:    args.TrustedExternalRootCAs,
		repository:                args.Repository,
		peersCommunicationService: args.PeersCommunicationService,
		clock:                     args.Clock,
		logger:                    args.Logger,
		signatureCert:             args.SignatureCert,
		selfPeerID:                &args.SelfPeerID,
		auditLgr:                  args.AuditLgr,
	}, nil
}

type GrantServiceConnectionArgs struct {
	OutwayPublicKeyThumbprint string
	OutwayPeerID              string
	Service                   interface{}
}

type NewServiceArgs struct {
	Name   string
	PeerID string
}

type NewDelegatedServiceArgs struct {
	Name                       string
	PeerID                     string
	PublicationDelegatorPeerID string
}

type GrantServicePublicationArgs struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
}

type GrantDelegatedServiceConnectionArgs struct {
	OutwayPublicKeyThumbprint string
	OutwayPeerID              string
	DelegatorPeerID           string
	Service                   interface{}
}

type GrantDelegatedServicePublicationArgs struct {
	DelegatorPeerID string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
}

type CreateContractHandlerArgs struct {
	HashAlgorithm         string
	IV                    string
	GroupID               string
	ContractNotBefore     time.Time
	ContractNotAfter      time.Time
	Grants                []interface{}
	CreatedAt             time.Time
	AuditLogSource        common_auditlog.Source
	AuthData              authentication.Data
	AuditlogCorrelationID string
}

func (h *CreateContractHandler) Handle(ctx context.Context, args *CreateContractHandlerArgs) (string, error) {
	handlerFunc := func() (any, common_auditlog.Event, error) {
		grants := make([]interface{}, len(args.Grants))

		for i, grant := range args.Grants {
			switch g := grant.(type) {
			case *GrantServicePublicationArgs:
				grants[i] = &contract.NewGrantServicePublicationArgs{
					Directory: &contract.NewGrantServicePublicationDirectoryArgs{
						Peer: &contract.NewPeerArgs{
							ID: g.DirectoryPeerID,
						},
					},
					Service: &contract.NewGrantServicePublicationServiceArgs{
						Peer: &contract.NewPeerArgs{
							ID: g.ServicePeerID,
						},
						Name:     g.ServiceName,
						Protocol: g.ServiceProtocol,
					},
				}
			case *GrantServiceConnectionArgs:
				var service interface{}
				switch s := g.Service.(type) {
				case *NewServiceArgs:
					service = &contract.NewGrantServiceConnectionServiceArgs{
						Peer: &contract.NewPeerArgs{
							ID: s.PeerID,
						},
						Name: s.Name,
					}
				case *NewDelegatedServiceArgs:
					service = &contract.NewGrantServiceConnectionDelegatedServiceArgs{
						Peer: &contract.NewPeerArgs{
							ID: s.PeerID,
						},
						Name: s.Name,
						PublicationDelegator: &contract.NewPeerArgs{
							ID: s.PublicationDelegatorPeerID,
						},
					}
				default:
					return "", nil, errors.New("invalid service type")
				}

				grants[i] = &contract.NewGrantServiceConnectionArgs{
					Outway: &contract.NewGrantServiceConnectionOutwayArgs{
						Peer: &contract.NewPeerArgs{
							ID: g.OutwayPeerID,
						},
						PublicKeyThumbprint: g.OutwayPublicKeyThumbprint,
					},
					Service: service,
				}

			case *GrantDelegatedServiceConnectionArgs:
				var service interface{}
				switch s := g.Service.(type) {
				case *NewServiceArgs:
					service = &contract.NewGrantDelegatedServiceConnectionServiceArgs{
						Peer: &contract.NewPeerArgs{
							ID: s.PeerID,
						},
						Name: s.Name,
					}
				case *NewDelegatedServiceArgs:
					service = &contract.NewGrantDelegatedServiceConnectionDelegatedServiceArgs{
						Peer: &contract.NewPeerArgs{
							ID: s.PeerID,
						},
						Name: s.Name,
						PublicationDelegator: &contract.NewPeerArgs{
							ID: s.PublicationDelegatorPeerID,
						},
					}
				default:
					return "", nil, errors.New("invalid service type")
				}

				grants[i] = &contract.NewGrantDelegatedServiceConnectionArgs{
					Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
						Peer: &contract.NewPeerArgs{
							ID: g.OutwayPeerID,
						},
						PublicKeyThumbprint: g.OutwayPublicKeyThumbprint,
					},
					Service: service,
					Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
						Peer: &contract.NewPeerArgs{
							ID: g.DelegatorPeerID,
						},
					},
				}
			case *GrantDelegatedServicePublicationArgs:
				grants[i] = &contract.NewGrantDelegatedServicePublicationArgs{
					Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
						Peer: &contract.NewPeerArgs{
							ID: g.DirectoryPeerID,
						},
					},
					Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
						Peer: &contract.NewPeerArgs{
							ID: g.ServicePeerID,
						},
						Name:     g.ServiceName,
						Protocol: g.ServiceProtocol,
					},
					Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
						Peer: &contract.NewPeerArgs{
							ID: g.DelegatorPeerID,
						},
					},
				}
			default:
				return "", nil, fmt.Errorf("unknown grant type %T", g)
			}
		}

		contractIV, err := uuid.Parse(args.IV)
		if err != nil {
			return "", nil, fmt.Errorf("could not parse contract IV. %s", err)
		}

		uuidBytes, err := contractIV.MarshalBinary()
		if err != nil {
			return "", nil, fmt.Errorf("could not get bytes from uuid. %s", err)
		}

		hashAlg, err := hashAlgToModel(args.HashAlgorithm)
		if err != nil {
			return "", nil, err
		}

		contractContent, err := contract.NewContent(
			&contract.NewContentArgs{
				HashAlgorithm: *hashAlg,
				IV:            uuidBytes,
				GroupID:       args.GroupID,
				Validity: &contract.NewValidityArgs{
					NotBefore: args.ContractNotBefore,
					NotAfter:  args.ContractNotAfter,
				},
				Grants:    grants,
				CreatedAt: args.CreatedAt,
				Clock:     h.clock,
			},
		)
		if err != nil {
			return "", nil, internalapp_errors.NewIncorrectInputError(err)
		}

		if !contractContent.ContainsPeer(*h.selfPeerID) {
			return "", nil, internalapp_errors.NewIncorrectInputError(fmt.Errorf("the peer creating contract is not part of the contract. peerID %q ", h.selfPeerID.Value()))
		}

		// sign own content
		sig, err := contractContent.Accept(h.trustedExternalRootCAs, h.signatureCert, h.clock.Now().Truncate(time.Second))
		if err != nil {
			return "", nil, fmt.Errorf("unable to sign content %v", err)
		}

		err = h.repository.UpsertContent(ctx, contractContent)
		if err != nil {
			h.logger.Error("create content", err)
			return "", nil, err
		}

		err = h.repository.UpsertSignature(ctx, sig)
		if err != nil {
			h.logger.Error("could not upsert signature", err)
			return "", nil, err
		}

		err = h.peersCommunicationService.SubmitContract(ctx, contractContent.PeersIDs(), contractContent, sig)
		if err != nil {
			h.logger.Error("could not submit contract to peers", err)
			return "", nil, err
		}

		contentHash := contractContent.Hash().String()

		return contentHash, common_auditlog.EventCreateContract{ContractHash: contentHash}, nil
	}

	res, err := h.auditLgr.AddRecordWithResultAndEvent(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData:      args.AuthData,
		Event:         common_auditlog.EventCreateContract{},
		Source:        args.AuditLogSource,
		CorrelationID: args.AuditlogCorrelationID,
	})
	if err != nil {
		return "", mapError(err, "could not create auditLog record for create contract")
	}

	return res.(string), nil
}

func hashAlgToModel(alg string) (*contract.HashAlg, error) {
	var hashAlgorithm contract.HashAlg

	switch alg {
	case string(models.HASHALGORITHMSHA3512):
		hashAlgorithm = contract.HashAlgSHA3_512
	default:
		return nil, internalapp_errors.NewErrorTypeUnknownHashAlgorithm(fmt.Errorf("unknown hash algorithm"))
	}

	return &hashAlgorithm, nil
}
