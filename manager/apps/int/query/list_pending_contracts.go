// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type PendingContracts struct {
	Count            int
	PendingContracts []*PendingContract
}

type PendingContract struct {
	ContentHash    string
	Grants         []*PendingContractGrant
	ValidNotBefore time.Time
}

type PendingContractGrant struct {
	ServiceProtocol                   contract.ServiceProtocol
	ServiceName                       string
	ServicePeerID                     string
	OutwayPeerID                      string
	OutwayThumbprint                  string
	DirectoryPeerID                   string
	DelegatorPeerID                   string
	ServicePublicationDelegatorPeerID string
	GrantType                         GrantType
}

type ListPendingContractsHandlerArgs struct {
	PaginationStartDate time.Time
	PaginationStartHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	IncludeCount        bool
}

type ListPendingContractsHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
	clock      clock.Clock
}

func NewListContractsPendingHandler(clck clock.Clock, repo contract.Repository, selfPeerID contract.PeerID) (*ListPendingContractsHandler, error) {
	if repo == nil {
		return nil, errors.New("repository is required")
	}

	if clck == nil {
		return nil, errors.New("clock is required")
	}

	if selfPeerID == "" {
		return nil, errors.New("selfPeerID is required")
	}

	return &ListPendingContractsHandler{
		repo:       repo,
		selfPeerID: selfPeerID,
		clock:      clck,
	}, nil
}

func (h *ListPendingContractsHandler) Handle(ctx context.Context, args *ListPendingContractsHandlerArgs) (*PendingContracts, error) {
	limit := defaultPaginationLimit

	if args.PaginationLimit > 0 {
		limit = args.PaginationLimit
	}

	sortOrder := contract.SortOrderAscending

	if args.PaginationSortOrder == SortOrderDescending {
		sortOrder = contract.SortOrderDescending
	}

	contractsPending, err := h.repo.ListContractsPending(
		&contract.ListContractsPendingArgs{
			Ctx:                 ctx,
			SelfPeerID:          h.selfPeerID,
			PaginationStartHash: args.PaginationStartHash,
			PaginationLimit:     limit,
			PaginationSortOrder: sortOrder,
			PaginationStartDate: args.PaginationStartDate,
			IncludeCount:        args.IncludeCount,
		})
	if err != nil {
		return nil, fmt.Errorf("could not retrieve the pending contracts, %v", err)
	}

	result := make([]*PendingContract, len(contractsPending.PendingContracts))
	for i, pendingContract := range contractsPending.PendingContracts {
		result[i] = &PendingContract{
			ContentHash:    pendingContract.ContentHash,
			Grants:         convertPendingGrant(pendingContract.Grants),
			ValidNotBefore: pendingContract.ValidNotBefore,
		}
	}

	pendingContracts := &PendingContracts{
		PendingContracts: result,
		Count:            contractsPending.Count,
	}

	return pendingContracts, nil
}

func convertPendingGrant(inputGrants []*contract.ContractPendingGrant) []*PendingContractGrant {
	pendingGrants := make([]*PendingContractGrant, len(inputGrants))
	for i, inputGrant := range inputGrants {
		pendingGrants[i] = &PendingContractGrant{
			ServiceName:                       inputGrant.ServiceName,
			ServicePeerID:                     inputGrant.ServicePeerID.Value(),
			OutwayPeerID:                      inputGrant.OutwayPeerID.Value(),
			OutwayThumbprint:                  inputGrant.OutwayThumbprint,
			DelegatorPeerID:                   inputGrant.DelegatorPeerID.Value(),
			ServicePublicationDelegatorPeerID: inputGrant.ServicePublicationDelegatorPeerID.Value(),
			DirectoryPeerID:                   inputGrant.DirectoryPeerID.Value(),
			ServiceProtocol:                   inputGrant.ServiceProtocol,
			GrantType:                         convertGrantType(inputGrant.GrantType),
		}
	}

	return pendingGrants
}

func convertGrantType(inputGrantType contract.GrantType) GrantType {
	switch inputGrantType {
	case contract.GrantTypeServicePublication:
		return GrantTypeServicePublication
	case contract.GrantTypeServiceConnection:
		return GrantTypeServiceConnection
	case contract.GrantTypeDelegatedServiceConnection:
		return GrantTypeDelegatedServiceConnection
	case contract.GrantTypeDelegatedServicePublication:
		return GrantTypeDelegatedServicePublication
	default:
		return GrantTypeUnspecified
	}
}
