// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type Publications struct {
	Count        int
	Publications []*Publication
}
type Publication struct {
	ServicePeerID   string
	ServiceName     string
	ContentHash     string
	CreatedAt       time.Time
	ValidNotBefore  time.Time
	ValidNotAfter   time.Time
	ContractState   string
	GrantHash       string
	DirectoryPeerID string
	DelegatorPeerID string
}

type ListServicePublicationsHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
	clock      clock.Clock
}

type ListServicePublicationsHandlerArgs struct {
	PaginationCreatedAt int64
	PaginationGrantHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	IncludeCount        bool
	ServiceNames        []string
	ContractStates      []contract.ContractState
}

func NewListServicePublicationsHandler(clck clock.Clock, repo contract.Repository, selfPeerID contract.PeerID) (*ListServicePublicationsHandler, error) {
	if repo == nil {
		return nil, errors.New("repository is required")
	}

	if clck == nil {
		return nil, errors.New("clock is required")
	}

	if selfPeerID == "" {
		return nil, errors.New("selfPeerID is required")
	}

	return &ListServicePublicationsHandler{
		repo:       repo,
		selfPeerID: selfPeerID,
		clock:      clck,
	}, nil
}

// nolint:dupl // similar but not the same as delegated service publications
func (h *ListServicePublicationsHandler) Handle(ctx context.Context, args *ListServicePublicationsHandlerArgs) (*Publications, error) {
	limit := defaultPaginationLimit

	if args.PaginationLimit > 0 {
		limit = args.PaginationLimit
	}

	sortOrder := contract.SortOrderDescending

	if args.PaginationSortOrder == SortOrderAscending {
		sortOrder = contract.SortOrderAscending
	}

	publishedServices, err := h.repo.ListServicePublications(
		&contract.ListPublicationsArgs{
			Ctx:                 ctx,
			PeerID:              h.selfPeerID,
			PaginationCreatedAt: args.PaginationCreatedAt,
			PaginationGrantHash: args.PaginationGrantHash,
			PaginationLimit:     limit,
			PaginationSortOrder: sortOrder,
			IncludeCount:        args.IncludeCount,
			ContractStates:      args.ContractStates,
			ServiceNames:        args.ServiceNames,
		})
	if err != nil {
		return nil, fmt.Errorf("could not retrieve the published services, %v", err)
	}

	result := make([]*Publication, len(publishedServices.Publications))
	for i, publishedService := range publishedServices.Publications {
		result[i] = &Publication{
			ServicePeerID:   publishedService.ServicePeerID.Value(),
			ServiceName:     publishedService.ServiceName,
			ContentHash:     publishedService.ContentHash,
			CreatedAt:       publishedService.CreatedAt,
			ValidNotBefore:  publishedService.ValidNotBefore,
			ValidNotAfter:   publishedService.ValidNotAfter,
			ContractState:   convertContractState(publishedService.ContractState),
			GrantHash:       publishedService.GrantHash,
			DirectoryPeerID: publishedService.DirectoryPeerID.Value(),
			DelegatorPeerID: publishedService.DelegatorPeerID.Value(),
		}
	}

	publications := &Publications{
		Publications: result,
		Count:        publishedServices.Count,
	}

	return publications, nil
}
