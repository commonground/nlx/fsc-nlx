// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type DelegatedPublications struct {
	Count        int
	Publications []*Publication
}

type ListDelegatedServicePublicationsHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
	clock      clock.Clock
}

type ListDelegatedServicePublicationsHandlerArgs struct {
	PaginationCreatedAt int64
	PaginationGrantHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	IncludeCount        bool
	ContractStates      []contract.ContractState
}

func NewListDelegatedServicePublicationsHandler(clck clock.Clock, repo contract.Repository, selfPeerID contract.PeerID) (*ListDelegatedServicePublicationsHandler, error) {
	if repo == nil {
		return nil, errors.New("repository is required")
	}

	if clck == nil {
		return nil, errors.New("clock is required")
	}

	if selfPeerID == "" {
		return nil, errors.New("selfPeerID is required")
	}

	return &ListDelegatedServicePublicationsHandler{
		repo:       repo,
		selfPeerID: selfPeerID,
		clock:      clck,
	}, nil
}

//nolint:dupl // looks the same as ListServicePublicationsHandler but is different.
func (h *ListDelegatedServicePublicationsHandler) Handle(ctx context.Context, args *ListDelegatedServicePublicationsHandlerArgs) (*DelegatedPublications, error) {
	limit := defaultPaginationLimit

	if args.PaginationLimit > 0 {
		limit = args.PaginationLimit
	}

	sortOrder := contract.SortOrderDescending

	if args.PaginationSortOrder == SortOrderAscending {
		sortOrder = contract.SortOrderAscending
	}

	publishedServices, err := h.repo.ListDelegatedServicePublications(
		&contract.ListDelegatedPublicationsArgs{
			Ctx:                 ctx,
			PeerID:              h.selfPeerID,
			PaginationCreatedAt: args.PaginationCreatedAt,
			PaginationGrantHash: args.PaginationGrantHash,
			PaginationLimit:     limit,
			PaginationSortOrder: sortOrder,
			IncludeCount:        args.IncludeCount,
			ContractStates:      args.ContractStates,
		})
	if err != nil {
		return nil, fmt.Errorf("could not retrieve the delegated published services, %v", err)
	}

	result := make([]*Publication, len(publishedServices.Publications))
	for i, publishedService := range publishedServices.Publications {
		result[i] = &Publication{
			ServicePeerID:   publishedService.ServicePeerID.Value(),
			ServiceName:     publishedService.ServiceName,
			ContentHash:     publishedService.ContentHash,
			CreatedAt:       publishedService.CreatedAt,
			ValidNotBefore:  publishedService.ValidNotBefore,
			ValidNotAfter:   publishedService.ValidNotAfter,
			ContractState:   convertContractState(publishedService.ContractState),
			GrantHash:       publishedService.GrantHash,
			DirectoryPeerID: publishedService.DirectoryPeerID.Value(),
			DelegatorPeerID: publishedService.DelegatorPeerID.Value(),
		}
	}

	publications := &DelegatedPublications{
		Publications: result,
		Count:        publishedServices.Count,
	}

	return publications, nil
}
