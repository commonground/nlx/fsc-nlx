// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import "time"

type Contract struct {
	ID                []byte
	GroupID           string
	ContractNotBefore time.Time
	ContractNotAfter  time.Time
	Grants            []*Grant
	CreatedAt         time.Time
}

type Grant struct {
	Type                   string
	ClientPeerID           string
	ProviderPeerID         string
	ServiceName            string
	CertificateThumbprints []string
}

type OutwayConfigService struct {
	Name                   string
	ServiceProviderPeerID  string
	ServiceDelegatorPeerID string
	DelegatorPeerID        string
	InwayAddress           string
	GrantHash              string
}

type OutwayConfig struct {
	Services []*OutwayConfigService
}

type InwayConfigOutway struct {
	GrantHash              string
	CertificateThumbprints []string
	PeerID                 string
	DelegatorPeerID        string // Peer ID of the delegator that provided the outway access to this service
}

type InwayConfigService struct {
	ServiceName     string
	EndpointURL     string
	Outways         []*InwayConfigOutway
	DelegatorPeerID string // Peer ID of the delegator this service is offered on behalf of
}

type InwayConfig struct {
	Services map[string]*InwayConfigService
}

type Certificate struct {
	RawDERs [][]byte
}

type SortOrder string

const (
	SortOrderAscending  SortOrder = "asc"
	SortOrderDescending SortOrder = "desc"
)
