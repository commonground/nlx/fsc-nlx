// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import "gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"

type InternalError struct {
	m string
}

func newInternalError(message string) *InternalError {
	return &InternalError{
		m: message,
	}
}

func (v *InternalError) Error() string {
	return "internal error: " + v.m
}

func convertContractState(state contract.ContractState) string {
	unspecified := "unspecified"

	switch state {
	case contract.ContractStateUnspecified:
		return unspecified
	case contract.ContractStateValid:
		return "valid"
	case contract.ContractStateExpired:
		return "expired"
	case contract.ContractStateRejected:
		return "rejected"
	case contract.ContractStateRevoked:
		return "revoked"
	case contract.ContractStateProposed:
		return "proposed"
	default:
		return unspecified
	}
}
