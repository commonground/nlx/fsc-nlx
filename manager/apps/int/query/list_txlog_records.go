// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/gofrs/uuid"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type ListTXLogRecordsHandler struct {
	tx                        txlog.TXLog
	peersCommunicationService *services.PeersCommunicationService
	groupID                   contract.GroupID
	isTxLogDisabled           bool
}

func NewListTransactionLogRecordsHandler(groupID contract.GroupID, tx txlog.TXLog, peersCommunicationService *services.PeersCommunicationService, isTxLogDisabled bool) (*ListTXLogRecordsHandler, error) {
	if !isTxLogDisabled && tx == nil {
		return nil, fmt.Errorf("txlog is required")
	}

	if peersCommunicationService == nil {
		return nil, fmt.Errorf("peers communication service is required")
	}

	return &ListTXLogRecordsHandler{
		tx:                        tx,
		peersCommunicationService: peersCommunicationService,
		groupID:                   groupID,
		isTxLogDisabled:           isTxLogDisabled,
	}, nil
}

type Pagination struct {
	StartID   string
	Limit     uint32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}

type Filter struct {
	Period        *Period
	TransactionID string
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type ListTXLogRecordsHandlerArgs struct {
	DataSourcePeerID string
	Pagination       *Pagination
	Filters          []*Filter
}

type TXLogRecord struct {
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type TXLogRecordSource struct {
	OutwayPeerID string
}

type TXLogRecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type TXLogRecordDestination struct {
	ServicePeerID string
}

type TXLogRecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

func (h *ListTXLogRecordsHandler) Handle(ctx context.Context, args *ListTXLogRecordsHandlerArgs) ([]*TXLogRecord, error) {
	if h.isTxLogDisabled {
		return []*TXLogRecord{}, nil
	}

	// Get records from our own tx log API
	if args.DataSourcePeerID == "" {
		req, err := txlogReqToRepo(args)
		if err != nil {
			return nil, errors.Join(err, fmt.Errorf("invalid request in transcation log records handler"))
		}

		req.GroupID = h.groupID.String()

		records, err := h.tx.ListRecords(ctx, req)
		if err != nil {
			return nil, errors.Join(err, fmt.Errorf("could not get records from txlog repository"))
		}

		return txlogRespToRecords(records)
	} else { // Get records from peer
		peerID, err := contract.NewPeerID(args.DataSourcePeerID)
		if err != nil {
			return nil, errors.Join(err, fmt.Errorf("invalid peer ID in args"))
		}

		resp, err := h.peersCommunicationService.GetTXLogRecords(ctx, peerID)
		if err != nil {
			return nil, errors.Join(err, fmt.Errorf("could not get tx log records from peer"))
		}

		return managerRespToRecords(resp)
	}
}

func txlogRespToRecords(recs []*txlog.Record) ([]*TXLogRecord, error) {
	records := make([]*TXLogRecord, len(recs))

	for i, r := range recs {
		var src interface{}
		switch s := r.Source.(type) {
		case *txlog.RecordSource:
			src = &TXLogRecordSource{
				OutwayPeerID: s.OutwayPeerID,
			}
		case *txlog.RecordDelegatedSource:
			src = &TXLogRecordDelegatedSource{
				OutwayPeerID:    s.OutwayPeerID,
				DelegatorPeerID: s.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		var dest interface{}
		switch d := r.Destination.(type) {
		case *txlog.RecordDestination:
			dest = &TXLogRecordDestination{
				ServicePeerID: d.ServicePeerID,
			}
		case *txlog.RecordDelegatedDestination:
			dest = &TXLogRecordDelegatedDestination{
				ServicePeerID:   d.ServicePeerID,
				DelegatorPeerID: d.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		records[i] = &TXLogRecord{
			TransactionID: r.TransactionID,
			GrantHash:     r.GrantHash,
			Direction:     r.Direction,
			ServiceName:   r.ServiceName,
			Source:        src,
			Destination:   dest,
			CreatedAt:     r.CreatedAt,
		}
	}

	return records, nil
}

func txlogReqToRepo(req *ListTXLogRecordsHandlerArgs) (*txlog.ListRecordsRequest, error) {
	var startID uuid.UUID

	if req.Pagination.StartID != "" {
		var err error

		startID, err = uuid.FromString(req.Pagination.StartID)
		if err != nil {
			return nil, fmt.Errorf("invalid startID in pagination: %w", err)
		}
	}

	filters := make([]*txlog.Filter, len(req.Filters))

	for i, f := range req.Filters {

		filter := &txlog.Filter{
			ServiceName:   f.ServiceName,
			GrantHash:     f.GrantHash,
			PeerID:        f.PeerID,
		}

		if f.TransactionID != "" {
			id, err := record.NewTransactionIDFromString(f.TransactionID)
			if err != nil {
				return nil, errors.Join(err, fmt.Errorf("invalid transaction ID in filter"))
			}

			filter.TransactionID = id
		}

		if f.Period != nil {
			filter.Period = &txlog.Period{
				Start: f.Period.Start,
				End:   f.Period.End,
			}
		}

		filters[i] = filter
	}

	return &txlog.ListRecordsRequest{
		Pagination: &txlog.Pagination{
			StartID:   startID,
			Limit:     req.Pagination.Limit,
			SortOrder: sortOrderToProto(req.Pagination.SortOrder),
		},
		Filters: filters,
	}, nil
}

func sortOrderToProto(o SortOrder) txlog.SortOrder {
	switch o {
	case SortOrderAscending:
		return txlog.SortOrderAscending
	case SortOrderDescending:
		return txlog.SortOrderDescending
	default:
		return txlog.SortOrderUnspecified
	}
}

func managerRespToRecords(recs []*contract.TXLogRecord) ([]*TXLogRecord, error) {
	records := make([]*TXLogRecord, len(recs))

	for i, r := range recs {
		var src interface{}
		switch s := r.Source.(type) {
		case *contract.TXLogRecordSource:
			src = &TXLogRecordSource{
				OutwayPeerID: s.OutwayPeerID,
			}
		case *contract.TXLogRecordDelegatedSource:
			src = &TXLogRecordDelegatedSource{
				OutwayPeerID:    s.OutwayPeerID,
				DelegatorPeerID: s.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		var dest interface{}
		switch d := r.Destination.(type) {
		case *contract.TXLogRecordDestination:
			dest = &TXLogRecordDestination{
				ServicePeerID: d.ServicePeerID,
			}
		case *contract.TXLogRecordDelegatedDestination:
			dest = &TXLogRecordDelegatedDestination{
				ServicePeerID:   d.ServicePeerID,
				DelegatorPeerID: d.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		records[i] = &TXLogRecord{
			TransactionID: r.TransactionID,
			GrantHash:     r.GrantHash,
			Direction:     mapDirection(r.Direction),
			ServiceName:   r.ServiceName,
			Source:        src,
			Destination:   dest,
			CreatedAt:     r.CreatedAt,
		}
	}

	return records, nil
}

func mapDirection(d contract.TXLogDirection) record.Direction {
	switch d {
	case contract.TXLogDirectionIn:
		return record.DirectionIn
	case contract.TXLogDirectionOut:
		return record.DirectionOut
	default:
		return record.DirectionIn
	}
}
