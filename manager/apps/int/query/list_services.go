// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"

	"golang.org/x/sync/singleflight"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListServicesHandler struct {
	managerFactory      manager.Factory
	directoryConnection *singleflight.Group
	logger              *logger.Logger
	repository          contract.Repository
}

type NewListServicesHandlerArgs struct {
	ManagerFactory manager.Factory
	Logger         *logger.Logger
	Repository     contract.Repository
}

type Services []interface{}

type Service struct {
	Name     string
	PeerID   string
	PeerName string
	Protocol contract.ServiceProtocol
}

type DelegatedService struct {
	Name          string
	PeerID        string
	PeerName      string
	DelegatorID   string
	DelegatorName string
	Protocol      contract.ServiceProtocol
}

func NewListServicesHandler(args *NewListServicesHandlerArgs) (*ListServicesHandler, error) {
	if args.ManagerFactory == nil {
		return nil, errors.New("managerFactory is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	return &ListServicesHandler{
		managerFactory:      args.ManagerFactory,
		directoryConnection: &singleflight.Group{},
		logger:              args.Logger,
		repository:          args.Repository,
	}, nil
}

type ListServicesArgs struct {
	DirectoryPeerID             string
	ServiceProviderPeerIDFilter *string
}

func (h *ListServicesHandler) Handle(ctx context.Context, args *ListServicesArgs) (Services, error) {
	directoryPeerIDs, err := contract.NewPeerIDs([]string{args.DirectoryPeerID})
	if err != nil {
		return Services{}, errors.Join(err, errors.New("could not create peer ids"))
	}

	peers, err := h.repository.ListPeersByID(ctx, directoryPeerIDs)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not list peers by id"))
	}

	directoryPeer, ok := peers[contract.PeerID(args.DirectoryPeerID)]
	if !ok {
		return nil, fmt.Errorf("directory peer not found: %s", args.DirectoryPeerID)
	}

	res, err, _ := h.directoryConnection.Do(directoryPeer.ManagerAddress().Value(), func() (interface{}, error) {
		var directoryManagerClient manager.Manager

		directoryManagerClient, err = h.managerFactory.New(directoryPeer.ID().Value(), directoryPeer.ManagerAddress().Value())
		if err != nil {
			return nil, fmt.Errorf("could not create directory manager client: %w", err)
		}

		var serviceProviderPeerIDFilter *contract.PeerID

		if args.ServiceProviderPeerIDFilter != nil {
			filterAsPeerID := contract.PeerID(*args.ServiceProviderPeerIDFilter)
			serviceProviderPeerIDFilter = &filterAsPeerID
		}

		return directoryManagerClient.GetServices(ctx, serviceProviderPeerIDFilter, "")
	})
	if err != nil {
		h.logger.Error("could not get services from directory", err)

		return Services{}, nil
	}

	svcs := res.([]*contract.Service)

	services := make(Services, len(svcs))

	for i, s := range svcs {
		if s.DelegatorPeerID != "" {
			services[i] = &DelegatedService{
				Name:          s.Name,
				PeerID:        s.PeerID,
				PeerName:      s.PeerName,
				DelegatorID:   s.DelegatorPeerID,
				DelegatorName: s.DelegatorPeerName,
				Protocol:      s.Protocol,
			}
		} else {
			services[i] = &Service{
				Name:     s.Name,
				PeerID:   s.PeerID,
				PeerName: s.PeerName,
				Protocol: s.Protocol,
			}
		}
	}

	return services, nil
}
