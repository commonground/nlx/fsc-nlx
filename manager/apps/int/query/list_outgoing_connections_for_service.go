// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListContractsWithIncomingConnections
package query

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListOutgoingConnectionsForServiceHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
}

type ListOutgoingConnectionsForServiceHandlerArgs struct {
	ServicePeerID                     contract.PeerID
	ServicePublicationDelegatorPeerID contract.PeerID
	ServiceName                       string
	PaginationSortOrder               SortOrder
}

type NewListOutgoingConnectionsForServiceHandlerArgs struct {
	Repository contract.Repository
	SelfPeerID contract.PeerID
}

func NewListOutgoingConnectionsForServiceHandler(args *NewListOutgoingConnectionsForServiceHandlerArgs) (*ListOutgoingConnectionsForServiceHandler, error) {
	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	return &ListOutgoingConnectionsForServiceHandler{
		repo:       args.Repository,
		selfPeerID: args.SelfPeerID,
	}, nil
}

func (h *ListOutgoingConnectionsForServiceHandler) Handle(ctx context.Context, args *ListOutgoingConnectionsForServiceHandlerArgs) ([]*Connection, error) {
	sortOrder := contract.SortOrderDescending

	if args.PaginationSortOrder == SortOrderAscending {
		sortOrder = contract.SortOrderAscending
	}

	outgoingConnections, err := h.repo.ListOutgoingConnectionsForService(&contract.ListOutgoingConnectionsForServiceArgs{
		Ctx:             ctx,
		ServicePeerID:   args.ServicePeerID,
		DelegatorPeerID: args.ServicePublicationDelegatorPeerID,
		ServiceName:     args.ServiceName,
		SelfPeerID:      h.selfPeerID,
		SortOrder:       sortOrder,
	})
	if err != nil {
		return nil, errors.Join(err, newInternalError("failed to get outgoing connections for service from the repository"))
	}

	result := make([]*Connection, len(outgoingConnections))

	for i, outgoingConnection := range outgoingConnections {
		outConn := &Connection{
			ServicePeerID:             outgoingConnection.ServicePeerID.Value(),
			ServiceName:               outgoingConnection.ServiceName,
			ContentHash:               outgoingConnection.ContentHash,
			CreatedAt:                 outgoingConnection.CreatedAt,
			ValidNotBefore:            outgoingConnection.ValidNotBefore,
			ValidNotAfter:             outgoingConnection.ValidNotAfter,
			ContractState:             convertContractState(outgoingConnection.ContractState),
			GrantHash:                 outgoingConnection.GrantHash,
			OutwayPeerID:              outgoingConnection.OutwayPeerID.Value(),
			OutwayPublicKeyThumbprint: outgoingConnection.OutwayPublicKeyThumbprint.Value(),
		}

		if outgoingConnection.ServicePublicationDelegatorPeerID != nil {
			ppID := outgoingConnection.ServicePublicationDelegatorPeerID.Value()
			outConn.ServicePublicationDelegatorPeerID = ppID
		}

		if outgoingConnection.DelegatorPeerID != nil {
			dpID := outgoingConnection.DelegatorPeerID.Value()
			outConn.DelegatorPeerID = dpID
		}

		result[i] = outConn
	}

	return result, nil
}
