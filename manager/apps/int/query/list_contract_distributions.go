// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"time"

	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListFailedContractDistributionsHandler struct {
	repo contract.Repository
}

type ListFailedContractDistributions []*FailedContractDistribution

type FailedContractDistribution struct {
	ContractHash  string
	PeerID        string
	Action        DistributionAction
	Signature     string
	Reason        string
	Attempts      int32
	LastAttemptAt time.Time
	NextAttemptAt *time.Time
}

type DistributionAction string

const (
	DistributionActionSubmitContract        DistributionAction = "submit_contract"
	DistributionActionSubmitRevokeSignature DistributionAction = "submit_revoke_signature"
	DistributionActionSubmitRejectSignature DistributionAction = "submit_reject_signature"
	DistributionActionSubmitAcceptSignature DistributionAction = "submit_accept_signature"
)

func NewListFailedContractDistributionsHandler(repository contract.Repository) (*ListFailedContractDistributionsHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	return &ListFailedContractDistributionsHandler{
		repo: repository,
	}, nil
}

func (h *ListFailedContractDistributionsHandler) Handle(ctx context.Context, contentHash string) (ListFailedContractDistributions, error) {
	hash, err := contract.NewContentHashFromString(contentHash)
	if err != nil {
		return nil, internalapp_errors.NewIncorrectInputError(err)
	}

	cds, err := h.repo.ListFailedContractDistributions(ctx, hash)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not retrieve contract distributions from the database"))
	}

	distributions := make(ListFailedContractDistributions, len(cds))

	for i, cd := range cds {
		distributions[i] = &FailedContractDistribution{
			ContractHash:  cd.ContentHash().String(),
			PeerID:        cd.PeerID().Value(),
			Action:        DistributionAction(cd.Action()),
			Signature:     cd.Signature(),
			Attempts:      cd.Attempts(),
			Reason:        cd.Reason(),
			LastAttemptAt: cd.LastAttemptAt(),
			NextAttemptAt: cd.NextAttemptAt(),
		}
	}

	return distributions, nil
}
