// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"sort"

	"golang.org/x/sync/singleflight"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListPeersHandler struct {
	managerFactory       manager.Factory
	directoryConnections *singleflight.Group
	repository           contract.Repository
	logger               *logger.Logger
}

type ListPeersPeers []*ListPeersPeer

type PeerRole string

const (
	PeerRoleDirectory PeerRole = "directory"
)

type ListPeersPeer struct {
	ID             string
	Name           string
	ManagerAddress string
	Roles          []PeerRole
}

type NewListPeersHandlerArgs struct {
	ManagerFactory manager.Factory
	Repository     contract.Repository
	Logger         *logger.Logger
}

func NewListPeersHandler(args *NewListPeersHandlerArgs) (*ListPeersHandler, error) {
	if args.ManagerFactory == nil {
		return nil, errors.New("managerFactory is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	return &ListPeersHandler{
		managerFactory:       args.ManagerFactory,
		directoryConnections: &singleflight.Group{},
		repository:           args.Repository,
		logger:               args.Logger,
	}, nil
}

func (h *ListPeersHandler) Handle(ctx context.Context, peerIDs *[]string) (ListPeersPeers, error) {
	result := ListPeersPeers{}

	repoPeers, err := getPeersFromLocalRepository(ctx, h.repository, peerIDs)
	if err != nil {
		return nil, errors.New("could not retrieve peers from repository")
	}

	result, err = convertMapToList(repoPeers)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not convert directory peers to list peers"))
	}

	sort.Slice(result, func(i, j int) bool {
		return result[i].ID < result[j].ID
	})

	return result, nil
}

func getPeersFromLocalRepository(ctx context.Context, repository contract.Repository, peerIDs *[]string) (contract.Peers, error) {
	const paginationLimit = 10

	var repoPeers contract.Peers

	if peerIDs != nil {
		pIDs, err := contract.NewPeerIDs(*peerIDs)
		if err != nil {
			return nil, errors.Join(err, errors.New("could not create peer IDs"))
		}

		repoPeers, err = repository.ListPeersByID(ctx, pIDs)
		if err != nil {
			return nil, errors.Join(fmt.Errorf("could not list peers from repository"), err)
		}

		return repoPeers, nil
	} else {
		var err error

		listPeers, err := repository.ListPeers(ctx, "", paginationLimit, contract.SortOrderAscending)
		if err != nil {
			return nil, errors.Join(fmt.Errorf("could not list peers from repository"), err)
		}

		repoPeers = contract.Peers{}

		for _, peer := range listPeers {
			repoPeers[peer.ID()] = peer
		}
	}

	return repoPeers, nil
}

func convertMapToList(peers contract.Peers) (ListPeersPeers, error) {
	result := ListPeersPeers{}

	for _, peer := range peers {
		roles := make([]PeerRole, 0, len(peer.Roles()))

		for _, role := range peer.Roles() {
			switch role {
			case contract.PeerRoleDirectory:
				roles = append(roles, PeerRoleDirectory)
			default:
				return nil, fmt.Errorf("unknown role '%d'", role)
			}
		}

		result = append(result, &ListPeersPeer{
			ID:             peer.ID().Value(),
			Name:           peer.Name().Value(),
			ManagerAddress: peer.ManagerAddress().Value(),
			Roles:          roles,
		})
	}

	return result, nil
}
