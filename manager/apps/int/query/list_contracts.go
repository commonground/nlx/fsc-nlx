// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"time"

	"golang.org/x/exp/slices"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListContractsHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
	clock      clock.Clock
}

type Peer struct {
	ID             string
	Name           string
	ManagerAddress string
}

type Signature struct {
	Peer     *Peer
	SignedAt time.Time
}

type GrantType string

const (
	GrantTypeUnspecified                 GrantType = ""
	GrantTypeServicePublication          GrantType = "service_publication"
	GrantTypeServiceConnection           GrantType = "service_connection"
	GrantTypeDelegatedServicePublication GrantType = "delegated_service_publication"
	GrantTypeDelegatedServiceConnection  GrantType = "delegated_service_connection"
)

type ListContractsHandlerArgs struct {
	PeerID              string
	GrantTypes          []GrantType
	ContentHashes       []string
	GrantHashes         []string
	PaginationStartDate time.Time
	PaginationStartHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
}

func NewListContractsHandler(clck clock.Clock, repository contract.Repository, selfPeerID contract.PeerID) (*ListContractsHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	if clck == nil {
		return nil, errors.New("clock is required")
	}

	return &ListContractsHandler{
		clock:      clck,
		repo:       repository,
		selfPeerID: selfPeerID,
	}, nil
}

const defaultPaginationLimit = uint32(100)

func (h *ListContractsHandler) Handle(ctx context.Context, args *ListContractsHandlerArgs) ([]*contract.Contract, error) {
	var (
		resp []*contract.Contract
		err  error
	)

	limit := defaultPaginationLimit

	if args.PaginationLimit > 0 {
		limit = args.PaginationLimit
	}

	sortOrder := contract.SortOrderDescending

	if args.PaginationSortOrder == SortOrderAscending {
		sortOrder = contract.SortOrderAscending
	}

	switch {
	case len(args.ContentHashes) > 0:
		resp, err = h.repo.ListContractsByContentHash(ctx, args.ContentHashes)
		if err != nil {
			return nil, errors.Join(err, newInternalError("failed to get contracts by content hash from repository"))
		}

		return resp, nil
	case len(args.GrantHashes) > 0:
		resp, err = h.repo.ListContractsForPeerByGrantHashes(ctx, h.selfPeerID, args.GrantHashes)
		if err != nil {
			return nil, errors.Join(err, newInternalError("failed to get contracts by grant hash from repository"))
		}

		return resp, nil
	default:
		resp, err = fetchContractsByGrantType(ctx, h.repo, limit, sortOrder, args)
		if err != nil {
			return nil, errors.Join(err, newInternalError("failed to get contracts by grant type from repository"))
		}

		return resp, nil
	}
}

func fetchContractsByGrantType(ctx context.Context, repo contract.Repository, limit uint32, sortOrder contract.SortOrder, args *ListContractsHandlerArgs) ([]*contract.Contract, error) {
	listContractsArgs := &contract.ListContractsWithGrantTypeFilterArgs{
		PaginationStartHash:                   args.PaginationStartHash,
		PaginationStartDate:                   args.PaginationStartDate,
		PaginationLimit:                       limit,
		PaginationSortOrder:                   sortOrder,
		WithServiceConnectionGrants:           true,
		WithDelegatedServiceConnectionGrants:  true,
		WithServicePublicationGrants:          true,
		WithDelegatedServicePublicationGrants: true,
	}

	if len(args.GrantTypes) > 0 {
		listContractsArgs.WithServiceConnectionGrants = slices.Contains(args.GrantTypes, GrantTypeServiceConnection)
		listContractsArgs.WithDelegatedServiceConnectionGrants = slices.Contains(args.GrantTypes, GrantTypeDelegatedServiceConnection)
		listContractsArgs.WithServicePublicationGrants = slices.Contains(args.GrantTypes, GrantTypeServicePublication)
		listContractsArgs.WithDelegatedServicePublicationGrants = slices.Contains(args.GrantTypes, GrantTypeDelegatedServicePublication)
	}

	contracts, errByGrant := repo.ListContractsWithGrantTypeFilter(ctx, listContractsArgs)
	if errByGrant != nil {
		return nil, errors.Join(errByGrant, newInternalError("could not get contracts by grant type from repository"))
	}

	return contracts, nil
}
