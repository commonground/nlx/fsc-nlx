// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListOutgoingConnections
package query

import (
	"context"
	"errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListIncomingConnectionsHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
}

type ListIncomingConnectionsHandlerArgs struct {
	PaginationStartHash      string
	PaginationStartCreatedAt int64
	PaginationLimit          uint32
	PaginationSortOrder      SortOrder
	IncludeCount             bool
	ContractStates           []contract.ContractState
	ServiceNames             []string
}

type IncomingConnections struct {
	Connections []*Connection
	Count       int
}

func NewListIncomingConnectionsHandler(repository contract.Repository, selfPeerID contract.PeerID) (*ListIncomingConnectionsHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	return &ListIncomingConnectionsHandler{
		repo:       repository,
		selfPeerID: selfPeerID,
	}, nil
}

func (h *ListIncomingConnectionsHandler) Handle(ctx context.Context, args *ListIncomingConnectionsHandlerArgs) (*IncomingConnections, error) {
	limit := defaultPaginationLimit

	if args.PaginationLimit > 0 {
		limit = args.PaginationLimit
	}

	sortOrder := contract.SortOrderDescending

	if args.PaginationSortOrder == SortOrderAscending {
		sortOrder = contract.SortOrderAscending
	}

	incomingConnections, err := h.repo.ListIncomingConnections(ctx, &contract.ListIncomingConnectionsArgs{
		Ctx:                 ctx,
		PeerID:              h.selfPeerID,
		PaginationCreatedAt: args.PaginationStartCreatedAt,
		PaginationGrantHash: args.PaginationStartHash,
		PaginationLimit:     limit,
		PaginationSortOrder: sortOrder,
		IncludeCount:        args.IncludeCount,
		ContractStates:      args.ContractStates,
		ServiceNames:        args.ServiceNames,
	})
	if err != nil {
		return nil, errors.Join(err, newInternalError("failed to get incoming connections from repository"))
	}

	result := make([]*Connection, len(incomingConnections.Connections))

	for i, connection := range incomingConnections.Connections {
		conn := &Connection{
			ServicePeerID:  connection.ServicePeerID.Value(),
			ServiceName:    connection.ServiceName,
			ContentHash:    connection.ContentHash,
			CreatedAt:      connection.CreatedAt,
			ValidNotBefore: connection.ValidNotBefore,
			ValidNotAfter:  connection.ValidNotAfter,
			ContractState:  convertContractState(connection.ContractState),
			GrantHash:      connection.GrantHash,
			OutwayPeerID:   connection.OutwayPeerID.Value(),
		}

		if connection.ServicePublicationDelegatorPeerID != nil {
			ppID := connection.ServicePublicationDelegatorPeerID.Value()
			conn.ServicePublicationDelegatorPeerID = ppID
		}

		if connection.DelegatorPeerID != nil {
			dpID := connection.DelegatorPeerID.Value()
			conn.DelegatorPeerID = dpID
		}

		result[i] = conn
	}

	incomingConnectionsResult := &IncomingConnections{
		Connections: result,
		Count:       incomingConnections.Count,
	}

	return incomingConnectionsResult, nil
}
