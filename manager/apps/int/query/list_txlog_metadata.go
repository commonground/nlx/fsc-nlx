// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
)

type ListTxLogMetadataHandler struct {
	tx      txlog.TXLog
	groupID contract.GroupID
}

func NewListTxLogMetadataHandler(groupID contract.GroupID, tx txlog.TXLog, isTxLogDisabled bool) (*ListTxLogMetadataHandler, error) {
	if !isTxLogDisabled && tx == nil {
		return nil, fmt.Errorf("txlog is required")
	}

	return &ListTxLogMetadataHandler{
		tx:      tx,
		groupID: groupID,
	}, nil
}

type MetadataRecord struct {
	TransactionID string
	Direction     metadata.Direction
	Metadata      map[string]map[string]interface{}
}

func (h *ListTxLogMetadataHandler) Handle(ctx context.Context, transactionID string) ([]*MetadataRecord, error) {
	records, err := h.tx.ListRecordMetadata(ctx, transactionID)
	if err != nil {
		return nil, err
	}

	responseRecords := make([]*MetadataRecord, len(records))
	for i, record := range records {
		responseRecords[i] = &MetadataRecord{
			TransactionID: record.TransactionID,
			Direction:     record.Direction,
			Metadata:      record.Metadata,
		}
	}

	return responseRecords, nil
}
