// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListContractsWithIncomingConnections
package query

import (
	"context"
	"errors"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListOutgoingConnectionsHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
	logger     *logger.Logger
}

type ListOutgoingConnectionsHandlerArgs struct {
	PaginationStartHash      string
	PaginationStartCreatedAt int64
	PaginationLimit          uint32
	PaginationSortOrder      SortOrder
	IncludeCount             bool
	ContractStates           []contract.ContractState
}

type Connection struct {
	ServicePeerID                     string
	ServiceName                       string
	ContentHash                       string
	CreatedAt                         time.Time
	ValidNotBefore                    time.Time
	ValidNotAfter                     time.Time
	ContractState                     string
	GrantHash                         string
	ServicePublicationDelegatorPeerID string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
	DelegatorPeerID                   string
}

type OutgoingConnections struct {
	OutgoingConnections []*Connection
	Count               int
}

type NewListOutgoingConnectionsHandlerArgs struct {
	Repository contract.Repository
	SelfPeerID contract.PeerID
	Logger     *logger.Logger
}

func NewListOutgoingConnectionsHandler(args *NewListOutgoingConnectionsHandlerArgs) (*ListOutgoingConnectionsHandler, error) {
	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	return &ListOutgoingConnectionsHandler{
		repo:       args.Repository,
		selfPeerID: args.SelfPeerID,
		logger:     args.Logger,
	}, nil
}

func (h *ListOutgoingConnectionsHandler) Handle(ctx context.Context, args *ListOutgoingConnectionsHandlerArgs) (*OutgoingConnections, error) {
	limit := defaultPaginationLimit

	if args.PaginationLimit > 0 {
		limit = args.PaginationLimit
	}

	sortOrder := contract.SortOrderDescending

	if args.PaginationSortOrder == SortOrderAscending {
		sortOrder = contract.SortOrderAscending
	}

	outgoingConnections, err := h.repo.ListOutgoingConnections(&contract.ListOutgoingConnectionsArgs{
		Ctx:                 ctx,
		PeerID:              h.selfPeerID,
		PaginationCreatedAt: args.PaginationStartCreatedAt,
		PaginationGrantHash: args.PaginationStartHash,
		PaginationLimit:     limit,
		PaginationSortOrder: sortOrder,
		IncludeCount:        args.IncludeCount,
		ContractStates:      args.ContractStates,
	})
	if err != nil {
		return nil, errors.Join(err, newInternalError("failed to get outgoing connections from repository"))
	}

	result := make([]*Connection, len(outgoingConnections.Connections))

	for i, outgoingConnection := range outgoingConnections.Connections {
		outConn := &Connection{
			ServicePeerID:             outgoingConnection.ServicePeerID.Value(),
			ServiceName:               outgoingConnection.ServiceName,
			ContentHash:               outgoingConnection.ContentHash,
			CreatedAt:                 outgoingConnection.CreatedAt,
			ValidNotBefore:            outgoingConnection.ValidNotBefore,
			ValidNotAfter:             outgoingConnection.ValidNotAfter,
			ContractState:             convertContractState(outgoingConnection.ContractState),
			GrantHash:                 outgoingConnection.GrantHash,
			OutwayPeerID:              outgoingConnection.OutwayPeerID.Value(),
			OutwayPublicKeyThumbprint: outgoingConnection.OutwayPublicKeyThumbprint.Value(),
		}

		if outgoingConnection.ServicePublicationDelegatorPeerID != nil {
			ppID := outgoingConnection.ServicePublicationDelegatorPeerID.Value()
			outConn.ServicePublicationDelegatorPeerID = ppID
		}

		if outgoingConnection.DelegatorPeerID != nil {
			dpID := outgoingConnection.DelegatorPeerID.Value()
			outConn.DelegatorPeerID = dpID
		}

		result[i] = outConn
	}

	outgoingConnectionsResult := &OutgoingConnections{
		OutgoingConnections: result,
		Count:               outgoingConnections.Count,
	}

	return outgoingConnectionsResult, nil
}
