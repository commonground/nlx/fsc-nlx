// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListContractsWithIncomingConnections
package query

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListDelegatedConnectionsHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
}

type ListDelegatedConnectionsHandlerArgs struct {
	PaginationStartHash      string
	PaginationStartCreatedAt int64
	PaginationLimit          uint32
	PaginationSortOrder      SortOrder
	IncludeCount             bool
	ContractStates           []contract.ContractState
}

type DelegatedConnections struct {
	DelegatedConnections []*Connection
	Count                int
}

func NewListDelegatedConnectionsHandler(repository contract.Repository, selfPeerID contract.PeerID) (*ListDelegatedConnectionsHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	return &ListDelegatedConnectionsHandler{
		repo:       repository,
		selfPeerID: selfPeerID,
	}, nil
}

func (h *ListDelegatedConnectionsHandler) Handle(ctx context.Context, args *ListDelegatedConnectionsHandlerArgs) (*DelegatedConnections, error) {
	limit := defaultPaginationLimit

	if args.PaginationLimit > 0 {
		limit = args.PaginationLimit
	}

	sortOrder := contract.SortOrderDescending

	if args.PaginationSortOrder == SortOrderAscending {
		sortOrder = contract.SortOrderAscending
	}

	delegatedConnections, err := h.repo.ListDelegatedConnections(ctx, &contract.ListDelegatedConnectionsArgs{
		Ctx:                 ctx,
		PeerID:              h.selfPeerID,
		PaginationCreatedAt: args.PaginationStartCreatedAt,
		PaginationGrantHash: args.PaginationStartHash,
		PaginationLimit:     limit,
		PaginationSortOrder: sortOrder,
		IncludeCount:        args.IncludeCount,
		ContractStates:      args.ContractStates,
	})
	if err != nil {
		return nil, errors.Join(err, newInternalError("failed to get delegated connections from repository"))
	}

	result := make([]*Connection, len(delegatedConnections.Connections))

	for i, delegatedConnection := range delegatedConnections.Connections {
		outConn := &Connection{
			ServicePeerID:  delegatedConnection.ServicePeerID.Value(),
			ServiceName:    delegatedConnection.ServiceName,
			ContentHash:    delegatedConnection.ContentHash,
			CreatedAt:      delegatedConnection.CreatedAt,
			ValidNotBefore: delegatedConnection.ValidNotBefore,
			ValidNotAfter:  delegatedConnection.ValidNotAfter,
			ContractState:  convertContractState(delegatedConnection.ContractState),
			GrantHash:      delegatedConnection.GrantHash,
			OutwayPeerID:   delegatedConnection.OutwayPeerID.Value(),
		}

		if delegatedConnection.ServicePublicationDelegatorPeerID != nil {
			ppID := delegatedConnection.ServicePublicationDelegatorPeerID.Value()
			outConn.ServicePublicationDelegatorPeerID = ppID
		}

		if delegatedConnection.DelegatorPeerID != nil {
			dpID := delegatedConnection.DelegatorPeerID.Value()
			outConn.DelegatorPeerID = dpID
		}

		result[i] = outConn
	}

	delegatedConnectionsResult := &DelegatedConnections{
		DelegatedConnections: result,
		Count:                delegatedConnections.Count,
	}

	return delegatedConnectionsResult, nil
}
