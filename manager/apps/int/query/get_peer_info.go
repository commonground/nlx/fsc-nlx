// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type GetPeerInfoHandler struct {
	self *contract.Peer
}

type NewGetPeerInfoHandlerArg struct {
	SelfPeerID   string
	SelfPeerName string
}

func NewGetPeerInfoHandler(selfPeerID, selfPeerName string) (*GetPeerInfoHandler, error) {
	if selfPeerID == "" {
		return nil, fmt.Errorf("SelfPeerID is required")
	}

	if selfPeerName == "" {
		return nil, fmt.Errorf("SelfPeerName is required")
	}

	p, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:   selfPeerID,
		Name: selfPeerName,
	})
	if err != nil {
		return nil, errors.Wrap(err, "unable to create peer from args")
	}

	return &GetPeerInfoHandler{
		self: p,
	}, nil
}

func (h *GetPeerInfoHandler) Handle(ctx context.Context) *Peer {
	return &Peer{
		ID:   h.self.ID().Value(),
		Name: h.self.Name().Value(),
	}
}
