// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package internalapp

import (
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
)

type Application struct {
	Queries  Queries
	Commands Commands
}

type Queries struct {
	GetServicesForOutway              *query.GetServicesForOutwayHandler
	GetCertificates                   *query.GetCertificatesHandler
	GetContracts                      *query.ListContractsHandler
	ListPendingContracts              *query.ListPendingContractsHandler
	ListOutgoingConnections           *query.ListOutgoingConnectionsHandler
	ListOutgoingConnectionsForService *query.ListOutgoingConnectionsForServiceHandler
	ListIncomingConnections           *query.ListIncomingConnectionsHandler
	ListDelegatedConnections          *query.ListDelegatedConnectionsHandler
	ListServicePublications           *query.ListServicePublicationsHandler
	GetContractDistributions          *query.ListFailedContractDistributionsHandler
	ListServices                      *query.ListServicesHandler

	GetDelegatedServicePublications *query.ListDelegatedServicePublicationsHandler
	GetTXLogRecords                 *query.ListTXLogRecordsHandler
	GetTXLogMetadata                *query.ListTxLogMetadataHandler
	GetPeerInfo                     *query.GetPeerInfoHandler
	ListPeers                       *query.ListPeersHandler
	GetHealth                       *query.GetHealthHandler
}

type Commands struct {
	AnnouncePeer              *command.AnnouncePeerHandler
	CreateContract            *command.CreateContractHandler
	AcceptContract            *command.AcceptContractHandler
	RejectContract            *command.RejectContractHandler
	RevokeContract            *command.RevokeContractHandler
	CreateCertificate         *command.CreateCertificateHandler
	RetryContractDistribution *command.RetryContractDistributionHandler
	CreatePeer                *command.CreatePeerHandler
	SyncPeers                 *command.SyncPeersHandler
}
