// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package externalapp

import (
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/query"
)

type Application struct {
	Queries  Queries
	Commands Commands
}

type Queries struct {
	ListContracts             *query.ListContractsHandler
	GetKeySet                 *query.GetKeySetHandler
	ListPeers                 *query.ListPeersHandler
	ListServices              *query.ListServicesHandler
	GetPeerInfo               *query.GetPeerInfoHandler
	ListTransactionLogRecords *query.ListTransactionLogRecordsHandler
	GetToken                  *query.GetTokenHandler
	GetHealth                 *query.GetHealthHandler
}

type Commands struct {
	SubmitContract *command.SubmitContractHandler
	AcceptContract *command.AcceptContractHandler
	RejectContract *command.RejectContractHandler
	RevokeContract *command.SubmitRevokeSignatureHandler
	Announce       *command.AnnounceHandler
}
