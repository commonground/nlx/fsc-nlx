// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type GetHealthHandler struct {
	repo contract.Repository
}

func NewGetHealthHandler(repo contract.Repository) (*GetHealthHandler, error) {
	if repo == nil {
		return nil, errors.New("repository is required")
	}

	return &GetHealthHandler{repo: repo}, nil
}

func (h *GetHealthHandler) Handle(ctx context.Context) error {
	return h.repo.Ping(ctx)
}
