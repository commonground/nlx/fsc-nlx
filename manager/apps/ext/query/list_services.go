// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"

	app_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListServicesHandler struct {
	repository contract.Repository
}

func NewListServicesHandler(repository contract.Repository) (*ListServicesHandler, error) {
	if repository == nil {
		return nil, fmt.Errorf("repository is required")
	}

	return &ListServicesHandler{
		repository: repository,
	}, nil
}

type ListServicesHandlerArgs struct {
	PaginationStartID   string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	PeerIDFilter        string
	ServiceNameFilter   string
}

func (h *ListServicesHandler) Handle(ctx context.Context, args *ListServicesHandlerArgs) ([]*Service, error) {
	if args.PaginationStartID != "" {
		_, err := uuid.Parse(args.PaginationStartID)
		if err != nil {
			return nil, app_errors.NewIncorrectInputError(fmt.Errorf("pagination start id must be a valid uuid"))
		}
	}

	sortOrder := contract.SortOrderAscending
	if args.PaginationSortOrder == SortOrderDescending {
		sortOrder = contract.SortOrderDescending
	}

	var peerID *contract.PeerID

	if args.PeerIDFilter != "" {
		var err error

		pID, err := contract.NewPeerID(args.PeerIDFilter)
		if err != nil {
			return nil, err
		}

		peerID = &pID
	}

	limit := defaultLimit
	if args.PaginationLimit > 0 {
		limit = args.PaginationLimit
	}

	services, err := h.repository.ListServices(
		ctx,
		peerID,
		args.ServiceNameFilter,
		args.PaginationStartID,
		limit,
		sortOrder,
	)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not retrieve services from repository"))
	}

	result := make([]*Service, len(services))

	for i, s := range services {
		result[i] = &Service{
			PeerID:             s.PeerID,
			PeerManagerAddress: s.PeerManagerAddress,
			PeerName:           s.PeerName,
			Name:               s.Name,
			DelegatorPeerID:    s.DelegatorPeerID,
			DelegatorPeerName:  s.DelegatorPeerName,
			ContractIV:         s.ContractIV,
			Protocol:           s.Protocol,
		}
	}

	return result, nil
}
