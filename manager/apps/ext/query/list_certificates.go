// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type GetKeySetHandler struct {
	repository contract.Repository
	selfPeerID contract.PeerID
}

func NewGetKeySetHandler(repository contract.Repository, selfPeerID contract.PeerID) (*GetKeySetHandler, error) {
	if repository == nil {
		return nil, fmt.Errorf("repository is required")
	}

	return &GetKeySetHandler{
		repository: repository,
		selfPeerID: selfPeerID,
	}, nil
}

// Returns a list of certificate keys used by this peer
// these certificates can then be used to verify signatures
func (h *GetKeySetHandler) Handle(ctx context.Context) (Certificates, error) {
	certs, err := h.repository.GetPeerCerts(ctx, h.selfPeerID)
	if err != nil {
		return nil, errors.Wrap(err, "could not retrieve certificates from repository")
	}

	results := make(Certificates, 0)

	for _, cert := range *certs {
		results[cert.CertificateThumbprint().Value()] = &Certificate{
			KeyType: keyTypeToQuery(cert.KeyType()),
			RawDERs: cert.RawDERs(),
		}
	}

	return results, nil
}

func keyTypeToQuery(c contract.CertKeyType) CertKeyType {
	switch c {
	case contract.CertKeyTypeEC:
		return CertKeyTypeEC
	case contract.CertKeyTypeRSA:
		return CertKeyTypeRSA
	default:
		return CertKeyTypeUnspecified
	}
}
