// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/controller"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// OAuthGrantType The grant_type of an access token request. FSC only uses client_credentials
type OAuthGrantType string

// OAuthTokenType The type of token. FSC only uses Bearer
type OAuthTokenType string

// Defines values for OAuthGrantType.
const (
	OAuthGrantTypeInvalid           OAuthGrantType = ""
	OAuthGrantTypeClientCredentials OAuthGrantType = "client_credentials"
)

// Defines values for OAuthTokenType.
const (
	OAuthTokenTypeInvalid OAuthTokenType = ""
	OAuthTokenTypeBearer  OAuthTokenType = "bearer"
)

var ErrNoValidContractForGrantHash = errors.New("no valid contract for grant hash")
var ErrUnsupportedGrantType = errors.New("unsupported grant type")
var ErrInvalidGrantInScope = errors.New("invalid grant in scope")

type GetTokenHandler struct {
	clock            clock.Clock
	repo             contract.Repository
	signTokenCert    *tls.Certificate
	tokenCert        *contract.PeerCertificate
	tokenTTL         time.Duration
	controllerClient controller.Controller
}

type GetTokenHandlerArgs struct {
	ConnectingPeerID  string
	Scope             string
	OAuthGrantType    OAuthGrantType
	OutwayCertificate *x509.Certificate
}

type NewGetTokenHandlerArgs struct {
	Clock            clock.Clock
	Repo             contract.Repository
	SignTokenCert    *tls.Certificate
	TrustedRootCAs   *x509.CertPool
	TokenTTL         time.Duration
	ManagementClient controller.Controller
}

func NewGetTokenHandler(args *NewGetTokenHandlerArgs) (*GetTokenHandler, error) {
	if args.Clock == nil {
		return nil, fmt.Errorf("clock is required")
	}

	if args.Repo == nil {
		return nil, fmt.Errorf("repo is required")
	}

	if args.SignTokenCert == nil {
		return nil, fmt.Errorf("signTokenWith is required")
	}

	if args.TrustedRootCAs == nil {
		return nil, fmt.Errorf("trustedRootCAs is required")
	}

	if args.ManagementClient == nil {
		return nil, fmt.Errorf("management client is required")
	}

	tokenCert, err := contract.NewPeerCertFromCertificate(args.Clock, args.TrustedRootCAs, args.SignTokenCert.Certificate)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create peer cert, invalid certificate chain"))
	}

	return &GetTokenHandler{
		clock:            args.Clock,
		repo:             args.Repo,
		signTokenCert:    args.SignTokenCert,
		tokenCert:        tokenCert,
		tokenTTL:         args.TokenTTL,
		controllerClient: args.ManagementClient,
	}, nil
}

func (h *GetTokenHandler) Handle(ctx context.Context, args *GetTokenHandlerArgs) (*GetTokenResponse, error) {
	connPeer, err := contract.NewPeerID(args.ConnectingPeerID)
	if err != nil {
		return nil, errors.Join(err, errors.New("invalid connecting peer ID in token handler args"))
	}

	if args.OAuthGrantType != OAuthGrantTypeClientCredentials {
		return nil, ErrUnsupportedGrantType
	}

	grantHash, err := contract.DecodeHashFromString(args.Scope)
	if err != nil {
		return nil, errors.Join(err, ErrInvalidGrantInScope)
	}

	if grantHash.Type() != contract.HashTypeGrantServiceConnection && grantHash.Type() != contract.HashTypeGrantDelegatedServiceConnection {
		return nil, fmt.Errorf("invalid grant hash type, only hashes of type service connection or delegated service connection are allowed")
	}

	contracts, err := h.repo.ListContractsForPeerByGrantHashes(ctx, connPeer, []string{grantHash.String()})
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not list contracts for grant hash %s", grantHash.String()))
	}

	if len(contracts) == 0 {
		return nil, ErrNoValidContractForGrantHash
	}

	for _, c := range contracts {
		state := c.State(h.clock.Now())
		if state != contract.ContractStateValid {
			return nil, ErrNoValidContractForGrantHash
		}
	}

	tokenInfo, err := h.repo.GetTokenInfo(ctx, connPeer, h.tokenCert.Peer().ID(), grantHash)
	if err != nil {
		if errors.Is(err, contract.ErrTokenInfoNotFound) {
			return nil, ErrNoValidContractForGrantHash
		}

		return nil, errors.Join(err, errors.New("could not get token info from repository"))
	}

	if common_tls.PublicKeyThumbprint(args.OutwayCertificate) != tokenInfo.PublicKeyThumbprint {
		return nil, errors.New("public key thumbprint of Outway certificate does not match public key thumbprint in contract")
	}

	service, err := h.controllerClient.GetService(ctx, tokenInfo.ServiceName)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not get service from Controller API"))
	}

	now := h.clock.Now()

	token, err := accesstoken.New(&accesstoken.NewTokenArgs{
		GroupID:                     tokenInfo.GroupID,
		GrantHash:                   grantHash.String(),
		OutwayPeerID:                args.ConnectingPeerID,
		OutwayDelegatorPeerID:       tokenInfo.OutwayDelegatorPeerID,
		OutwayCertificateThumbprint: common_tls.X509CertificateThumbprint(args.OutwayCertificate),
		ServicePeerID:               h.tokenCert.Peer().ID().Value(),
		ServiceName:                 tokenInfo.ServiceName,
		ServiceInwayAddress:         service.InwayAddress,
		ServiceDelegatorPeerID:      tokenInfo.ServiceDelegatorPeerID,
		ExpiryDate:                  now.Add(h.tokenTTL),
		NotBefore:                   now,
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: h.tokenCert.CertificateThumbprint().Value(),
			PrivateKey:            h.signTokenCert.PrivateKey,
		},
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not sign token"))
	}

	return &GetTokenResponse{
		Token:     token.Value(),
		TokenType: OAuthTokenTypeBearer,
	}, nil
}
