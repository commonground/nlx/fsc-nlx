// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	app_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type AnnounceHandler struct {
	repository contract.Repository
	logger     *logger.Logger
}

type NewAnnounceHandlerArgs struct {
	Repository contract.Repository
	Logger     *logger.Logger
}

func NewAnnounceHandler(args *NewAnnounceHandlerArgs) (*AnnounceHandler, error) {
	if args.Repository == nil {
		return nil, fmt.Errorf("repository is required")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &AnnounceHandler{
		repository: args.Repository,
		logger:     args.Logger,
	}, nil
}

func (h *AnnounceHandler) Handle(ctx context.Context, peer *PeerArgs) error {
	peer.ManagerAddress = strings.TrimSuffix(peer.ManagerAddress, "/")

	announcingPeer, err := peerToModel(peer)
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	err = h.repository.UpsertPeer(ctx, announcingPeer)
	if err != nil {
		return fmt.Errorf("failed to upsert peer: %w", err)
	}

	return nil
}
