// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/errors"
	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

type GrantServiceConnectionArgs struct {
	OutwayPublicKeyThumbprint string
	OutwayPeerID              string
	Service                   interface{}
}

type ServiceArgs struct {
	PeerID string
	Name   string
}

type DelegatedServiceArgs struct {
	PeerID                            string
	Name                              string
	ServicePublicationDelegatorPeerID string
}

type GrantServicePublicationArgs struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
}

type GrantDelegatedServiceConnectionArgs struct {
	OutwayPublicKeyThumbprint string
	OutwayPeerID              string
	Service                   interface{}
	DelegatorPeerID           string
}

type GrantDelegatedServicePublicationArgs struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
	DelegatorPeerID string
}

type ContractContentArgs struct {
	HashAlgorithm string
	IV            []byte
	GroupID       string
	NotBefore     time.Time
	NotAfter      time.Time
	Grants        []interface{}
	CreatedAt     time.Time
}

type PeerArgs struct {
	ID             string
	Name           string
	ManagerAddress string
}

// nolint:funlen,gocyclo // unable to shorten
func contentToModel(c *ContractContentArgs, clck clock.Clock) (*contract.Content, error) {
	contractContentGrants := make([]interface{}, len(c.Grants))

	for i, grant := range c.Grants {
		switch g := grant.(type) {
		case *GrantServicePublicationArgs:
			contractContentGrants[i] = &contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DirectoryPeerID,
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
					Name:     g.ServiceName,
					Protocol: g.ServiceProtocol,
				},
			}
		case *GrantServiceConnectionArgs:
			var service interface{}

			switch s := g.Service.(type) {
			case *ServiceArgs:
				service = &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: s.PeerID,
					},
					Name: s.Name,
				}
			case *DelegatedServiceArgs:
				service = &contract.NewGrantServiceConnectionDelegatedServiceArgs{
					Name: s.Name,
					Peer: &contract.NewPeerArgs{
						ID: s.PeerID,
					},
					PublicationDelegator: &contract.NewPeerArgs{
						ID: s.ServicePublicationDelegatorPeerID,
					},
				}
			}

			contractContentGrants[i] = &contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.OutwayPeerID,
					},
					PublicKeyThumbprint: g.OutwayPublicKeyThumbprint,
				},
				Service: service,
			}
		case *GrantDelegatedServiceConnectionArgs:
			var service interface{}

			switch s := g.Service.(type) {
			case *ServiceArgs:
				service = &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Name: s.Name,
					Peer: &contract.NewPeerArgs{
						ID: s.PeerID,
					},
				}
			case *DelegatedServiceArgs:
				service = &contract.NewGrantDelegatedServiceConnectionDelegatedServiceArgs{
					Name: s.Name,
					Peer: &contract.NewPeerArgs{
						ID: s.PeerID,
					},
					PublicationDelegator: &contract.NewPeerArgs{
						ID: s.ServicePublicationDelegatorPeerID,
					},
				}
			}
			contractContentGrants[i] = &contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.OutwayPeerID,
					},
					PublicKeyThumbprint: g.OutwayPublicKeyThumbprint,
				},
				Service: service,
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DelegatorPeerID,
					},
				},
			}
		case *GrantDelegatedServicePublicationArgs:
			contractContentGrants[i] = &contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DirectoryPeerID,
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Name: g.ServiceName,
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
					Protocol: g.ServiceProtocol,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DelegatorPeerID,
					},
				},
			}
		default:
			return nil, fmt.Errorf("unknown grant type %T", g)
		}
	}

	hashAlg, err := hashAlgToModel(c.HashAlgorithm)
	if err != nil {
		return nil, err
	}

	model, err := contract.NewContent(
		&contract.NewContentArgs{
			Clock:         clck,
			HashAlgorithm: *hashAlg,
			IV:            c.IV,
			GroupID:       c.GroupID,
			Validity: &contract.NewValidityArgs{
				NotBefore: c.NotBefore,
				NotAfter:  c.NotAfter,
			},
			Grants:    contractContentGrants,
			CreatedAt: c.CreatedAt,
		},
	)
	if err != nil {
		return nil, err
	}

	return model, nil
}

func peerToModel(p *PeerArgs) (*contract.Peer, error) {
	peer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             p.ID,
		Name:           p.Name,
		ManagerAddress: p.ManagerAddress,
	})
	if err != nil {
		return nil, internalapp_errors.NewIncorrectInputError(err)
	}

	if peer.ManagerAddress() == "" {
		return nil, internalapp_errors.NewIncorrectInputError(fmt.Errorf("peer manager address cannot be empty"))
	}

	return peer, nil
}

func hashAlgToModel(alg string) (*contract.HashAlg, error) {
	var hashAlgorithm contract.HashAlg

	switch alg {
	case string(models.HASHALGORITHMSHA3512):
		hashAlgorithm = contract.HashAlgSHA3_512
	default:
		return nil, errors.NewErrorTypeUnknownHashAlgorithm(fmt.Errorf("unknown hash algorithm %q", alg))
	}

	return &hashAlgorithm, nil
}
