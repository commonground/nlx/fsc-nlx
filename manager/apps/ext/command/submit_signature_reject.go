// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/x509"
	"fmt"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	app_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type RejectContractHandler struct {
	groupID                   contract.GroupID
	selfPeerID                contract.PeerID
	trustedRootCAs            *x509.CertPool
	clock                     clock.Clock
	repository                contract.Repository
	logger                    *logger.Logger
	peersCommunicationService *services.PeersCommunicationService
}

type NewRejectContractHandlerArgs struct {
	GroupID                   contract.GroupID
	SelfPeerID                contract.PeerID
	TrustedRootCAs            *x509.CertPool
	Repository                contract.Repository
	PeersCommunicationService *services.PeersCommunicationService
	Logger                    *logger.Logger
	Clock                     clock.Clock
}

func NewRejectContractHandler(args *NewRejectContractHandlerArgs) (*RejectContractHandler, error) {
	if args.GroupID == "" {
		return nil, fmt.Errorf("groupID is required")
	}

	if args.TrustedRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.PeersCommunicationService == nil {
		return nil, errors.New("peers communication service is required")
	}

	return &RejectContractHandler{
		groupID:                   args.GroupID,
		selfPeerID:                args.SelfPeerID,
		trustedRootCAs:            args.TrustedRootCAs,
		repository:                args.Repository,
		clock:                     args.Clock,
		logger:                    args.Logger,
		peersCommunicationService: args.PeersCommunicationService,
	}, nil
}

type HandleRejectContractArgs struct {
	ContractContentHash string
	ContractContent     *ContractContentArgs
	Signature           string
	SubmittedByPeer     *PeerArgs
}

func (h *RejectContractHandler) Handle(ctx context.Context, args *HandleRejectContractArgs) error {
	submittedByPeer, err := peerToModel(args.SubmittedByPeer)
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	err = h.repository.UpsertPeer(ctx, submittedByPeer)
	if err != nil {
		return errors.Wrapf(err, "could not upsert peer: %s into repository", submittedByPeer.ID())
	}

	content, err := contentToModel(args.ContractContent, h.clock)
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	if args.ContractContentHash != content.Hash().String() {
		err = fmt.Errorf("the contract content hash in the url %q does not match the contract content hash generated from the request body %q", args.ContractContentHash, content.Hash().String())
		return app_errors.NewPathContentHashMismatchError(err)
	}

	if content.GroupID() != h.groupID.String() {
		return app_errors.NewIncorrectInputError(fmt.Errorf("wrong group ID in contract, want: %q, got: %q", h.groupID, content.GroupID()))
	}

	if !content.ContainsPeer(submittedByPeer.ID()) {
		return app_errors.NewPeerNotPartOfContractError(fmt.Errorf("submitting peer: %s is not found in contract", submittedByPeer.ID()))
	}

	if !content.ContainsPeer(h.selfPeerID) {
		return app_errors.NewPeerNotPartOfContractError(fmt.Errorf("self peer: %s is not found in contract", h.selfPeerID))
	}

	CertificateThumbprint, err := getCertificateThumbprintFromSignature(args.Signature)
	if err != nil {
		return app_errors.NewSignatureVerificationFailedError(fmt.Errorf("could not get certificate thumbprint from signature: %s.%s", args.Signature, err))
	}

	certsToRequest := make(map[contract.PeerID][]string)
	certsToRequest[submittedByPeer.ID()] = []string{CertificateThumbprint}

	certs, err := h.peersCommunicationService.GetCertificates(ctx, certsToRequest)
	if err != nil {
		h.logger.Error("unable to retrieve certificate from Peer", err)
		return err
	}

	sig, err := contract.NewSignature(
		&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeReject,
			Signature:  args.Signature,
			Content:    content,
			PeersCerts: certs,
		})
	if err != nil {
		return app_errors.FromDomainValidationError(err)
	}

	if !sig.Peer().IsEqual(submittedByPeer) {
		return app_errors.NewSignatureMismatchError(fmt.Errorf("signature submitted by peer: %s, but signature is signed by peer: %s", submittedByPeer.ID(), sig.Peer().ID().Value()))
	}

	err = h.repository.UpsertContent(ctx, content)
	if err != nil {
		h.logger.Error("could not upsert contract", err)
		return err
	}

	err = h.repository.UpsertSignature(ctx, sig)
	if err != nil {
		h.logger.Error("could not upsert signature", err)
		return err
	}

	return nil
}
