// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package errors

import (
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type Type struct {
	t string
}

var (
	ErrorTypeIncorrectInput                = Type{"incorrect-input"}
	ErrorTypeIncorrectGroupID              = Type{"incorrect-group-id"}
	ErrorTypePeerNotPartOfContact          = Type{"peer-not-part-of-contract"}
	ErrorTypeContractContentHashMismatch   = Type{"contract-content-hash-mismatch"}
	ErrorTypeCertificateVerificationFailed = Type{"certificate-verification-failed"}
	ErrorTypeSignatureMismatch             = Type{"signature-mismatch"}
	ErrorTypeSignatureVerificationFailed   = Type{"signature-verification-failed"}
	ErrorTypeIncorrectGrantCombination     = Type{"incorrect-grant-combination"}
	ErrorTypePathContentHashMismatch       = Type{"path-content-hash-mismatch"}
	ErrorTypeSignatureUnknownAlgorithm     = Type{"signature-algorithm-unknown"}
	ErrorTypeUnknownHashAlgorithm          = Type{"hash-algorithm-unknown"}
	ErrorTypeCertificateRevoked            = Type{"certificate-revoked"}
	ErrorTypePublicKeyThumbprintInvalid    = Type{"incorrect-public-key-thumbprint"}
)

type Error struct {
	message   string
	errorType Type
}

func (s Error) Error() string {
	return s.message
}

func (s Error) ErrorType() Type {
	return s.errorType
}

func NewIncorrectInputError(err error) Error {
	return Error{
		message:   fmt.Sprintf("invalid input: %v", err),
		errorType: ErrorTypeIncorrectInput,
	}
}

func NewIncorrectGroupIDError(err error) Error {
	return Error{
		message:   fmt.Sprintf("invalid group id: %v", err),
		errorType: ErrorTypeIncorrectGroupID,
	}
}

func NewPeerNotPartOfContractError(err error) Error {
	return Error{
		message:   fmt.Sprintf("peer is not part of the contract: %v", err),
		errorType: ErrorTypePeerNotPartOfContact,
	}
}

func NewContractContentHashMismatchError(err error) Error {
	return Error{
		message:   fmt.Sprintf("contract content hash mismatch : %v", err),
		errorType: ErrorTypeContractContentHashMismatch,
	}
}

func NewCertificateVerificationFailedError(err error) Error {
	return Error{
		message:   fmt.Sprintf("certificate verification failed: %v", err),
		errorType: ErrorTypeCertificateVerificationFailed,
	}
}

func NewSignatureMismatchError(err error) Error {
	return Error{
		message:   fmt.Sprintf("signature mismatch: %v", err),
		errorType: ErrorTypeSignatureMismatch,
	}
}

func NewSignatureVerificationFailedError(err error) Error {
	return Error{
		message:   fmt.Sprintf("signature verification failed: %v", err),
		errorType: ErrorTypeSignatureVerificationFailed,
	}
}

func NewIncorrectGrantCombinationError(err error) Error {
	return Error{
		message:   fmt.Sprintf("invalid grant combination: %v", err),
		errorType: ErrorTypeIncorrectGrantCombination,
	}
}

func NewPathContentHashMismatchError(err error) Error {
	return Error{
		message:   fmt.Sprintf("path content hash mismatch: %v", err),
		errorType: ErrorTypePathContentHashMismatch,
	}
}

func NewSignatureAlgorithmError(err error) Error {
	return Error{
		message:   fmt.Sprintf("algorithm on signature is not supported: %v", err),
		errorType: ErrorTypeSignatureUnknownAlgorithm,
	}
}

func NewErrorTypeUnknownHashAlgorithm(err error) Error {
	return Error{
		message:   fmt.Sprintf("hash algorithm unsupported: %v", err),
		errorType: ErrorTypeUnknownHashAlgorithm,
	}
}

func NewErrorTypeCertificateRevoked(err error, certificateSerialnumber string) Error {
	return Error{
		message:   "client used a revoked certificate: " + err.Error(),
		errorType: ErrorTypeCertificateRevoked,
	}
}

func FromDomainValidationError(err error) Error {
	var appError Error
	var domainError *contract.ValidationError

	switch {
	case errors.As(err, &appError):
		return err.(Error)
	case !errors.As(err, &domainError):
		return NewIncorrectInputError(err)
	}

	switch domainError.Code() {
	case string(contract.ErrorCodePeerNotPartOfContract):
		return NewPeerNotPartOfContractError(err)
	case string(contract.ErrorCodeGrantCombinationNotAllowed):
		return NewIncorrectGrantCombinationError(err)
	case string(contract.ErrorCodeSignatureContractContentHashMismatch):
		return NewContractContentHashMismatchError(err)
	case string(contract.ErrorCodeSignatureVerificationFailed):
		return NewSignatureVerificationFailedError(err)
	case string(contract.ErrorCodeSignatureUnknownAlgorithm):
		return NewSignatureAlgorithmError(err)
	default:
		return NewIncorrectInputError(err)
	}
}
