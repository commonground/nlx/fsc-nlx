// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package cmd

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/spf13/cobra"

	api_auditlog "gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/server"
	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/cmd"
	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logoptions"
	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	"gitlab.com/commonground/nlx/fsc-nlx/common/process"
	"gitlab.com/commonground/nlx/fsc-nlx/common/slogerrorwriter"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/version"
	controllerapi "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/server"
	restcontroller "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/controller/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager/rest"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	resttxlog "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/metrics"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner/granttype"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner/neverallow"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner/pdp"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/health"
	externalrest "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest"
	internalrest "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest"
	internalunauthenticatedrest "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/intunauthenticated/rest"
	externalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/ext"
	internalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/int"
	txlogapi "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

var grantTypes = []string{"servicePublication", "serviceConnection", "delegatedServicePublication", "delegatedServiceConnection"}

type TLSInternalOptions struct {
	InternalRootCertFile string
	InternalCertFile     string
	InternalKeyFile      string
}

var serveOpts struct {
	ListenAddressInternal                string
	ListenAddressInternalUnauthenticated string
	ListenAddressExternal                string
	MonitoringAddress                    string
	SelfAddress                          string
	ControllerAPIAddress                 string
	StoragePostgresDSN                   string
	GroupID                              string
	DirectoryManagerAddress              string
	DirectoryPeerID                      string
	AutoSignGrants                       []string
	TxLogAPIAddress                      string
	TokenTTL                             string
	CRLURLs                              []string
	AutoSignPDPAddress                   string
	AutoSignPDPCA                        string

	AuditlogType        string
	AuditlogRESTAddress string

	logoptions.LogOptions
	cmd.TLSGroupOptions
	cmd.TLSOptions
	TLSInternalOptions

	GroupTokenCertFile string
	GroupTokenKeyFile  string

	GroupContractCertFile string
	GroupContractKeyFile  string
}

//nolint:gochecknoinits,funlen,gocyclo // this is the recommended way to use cobra
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressInternal, "listen-address-internal", "", "127.0.0.1:443", "Address for the internal api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressInternalUnauthenticated, "listen-address-internal-unauthenticated", "", "127.0.0.1:444", "Address for the internal unauthenticated api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressExternal, "listen-address-external", "", "127.0.0.1:8443", "Address for the external api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.MonitoringAddress, "monitoring-address", "", "127.0.0.1:8081", "Address for the monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.SelfAddress, "self-address", "", "", "Manager address of this instance, must be publicly routable and either on port 443 or port 8443, e.g. 'https://manager.example.com:443'")
	serveCommand.Flags().StringVarP(&serveOpts.StoragePostgresDSN, "storage-postgres-dsn", "", "", "Postgres Connection URL")
	serveCommand.Flags().StringVarP(&serveOpts.GroupID, "group-id", "", "", "Group ID of the FSC Group")
	serveCommand.Flags().StringVarP(&serveOpts.DirectoryManagerAddress, "directory-manager-address", "", "", "Manager address of directory peer")
	serveCommand.Flags().StringVarP(&serveOpts.DirectoryPeerID, "directory-peer-id", "", "", "ID of directory peer")
	serveCommand.Flags().StringVarP(&serveOpts.ControllerAPIAddress, "controller-api-address", "", "", "Address of the Controller API")
	serveCommand.Flags().StringVarP(&serveOpts.TxLogAPIAddress, "tx-log-api-address", "", "", "Address of the Transaction Log API")
	serveCommand.Flags().StringSliceVar(&serveOpts.AutoSignGrants, "auto-sign-grants", []string{}, fmt.Sprintf("Auto sign grants determines which contraces containing these grants should be automatically signed, possible values are: %s", strings.Join(grantTypes, ", ")))
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "info", "Set loglevel")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSInternalOptions.InternalRootCertFile, "tls-internal-unauthenticated-root-cert", "", "", "Absolute or relative path to the CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSInternalOptions.InternalCertFile, "tls-internal-unauthenticated-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSInternalOptions.InternalKeyFile, "tls-internal-unauthenticated-key", "", "", "Absolute or relative path to the key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupRootCert, "tls-group-root-cert", "", "", "Absolute or relative path to the NLX CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupCertFile, "tls-group-cert", "", "", "Absolute or relative path to the FSC Group cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupKeyFile, "tls-group-key", "", "", "Absolute or relative path to the FSC Group key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.GroupTokenCertFile, "tls-group-token-cert", "", "", "Absolute or relative path to the FSC Group token cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.GroupTokenKeyFile, "tls-group-token-key", "", "", "Absolute or relative path to the FSC Group token key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.GroupContractCertFile, "tls-group-contract-cert", "", "", "Absolute or relative path to the FSC Group contract cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.GroupContractKeyFile, "tls-group-contract-key", "", "", "Absolute or relative path to the FSC Group contract key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TokenTTL, "token-ttl", "", "1h", "Duration of token validity, how long is the time to live for the generated tokens, format is specified in string, e.g. '1h', '300s', or '5m'")
	serveCommand.Flags().StringSliceVar(&serveOpts.CRLURLs, "crl-urls", []string{}, "List of URL's of Certificate Revocation Lists used to retrieve Certificate Revocation Lists")
	serveCommand.Flags().StringVarP(&serveOpts.AuditlogType, "auditlog-type", "", "stdout", "Type of the authentication adapter, valid values: rest, stdout")
	serveCommand.Flags().StringVarP(&serveOpts.AuditlogRESTAddress, "auditlog-rest-address", "", "", "Address for the Auditlog API endpoint")
	serveCommand.Flags().StringVarP(&serveOpts.AutoSignPDPAddress, "auto-sign-pdp-address", "", "", "Address of the policy decision point which decides if a submitted Contract should get an automatic accept signature")
	serveCommand.Flags().StringVarP(&serveOpts.AutoSignPDPCA, "auto-sign-pdp-root-ca", "", "", "absolute path to root CA used to verify auto-sign policy decision point certificate")

	if err := serveCommand.MarkFlagRequired("listen-address-internal"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("listen-address-internal-unauthenticated"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("listen-address-external"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("storage-postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-root-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-internal-unauthenticated-root-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-internal-unauthenticated-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-internal-unauthenticated-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-root-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-token-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-token-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-contract-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-contract-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("directory-manager-address"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("self-address"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the API",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		logger, err := zaplogger.New(serveOpts.LogOptions.LogLevel, serveOpts.LogOptions.LogType)
		if err != nil {
			log.Fatalf("failed to create logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version info: version: %s source-hash: %s", version.BuildVersion, version.BuildSourceHash))

		internalCertificate, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("loading internal TLS files", err)
		}

		externalCertificate, err := common_tls.NewBundleFromFiles(serveOpts.GroupCertFile, serveOpts.GroupKeyFile, serveOpts.GroupRootCert)
		if err != nil {
			logger.Fatal("loading external TLS files", err)
		}

		livenessProbeInternalManagerAPI := health.ManagerAPIProbe(internalCertificate, serveOpts.ListenAddressInternal)
		livenessProbeExternalManagerAPI := health.ManagerAPIProbe(externalCertificate, serveOpts.ListenAddressExternal)
		livenessProbeInternalUnauthenticatedProbe := health.InternalUnauthenticatedProbe(serveOpts.ListenAddressInternalUnauthenticated)

		readinessProbes := map[string]monitoring.Check{
			"Readiness Probe Manager Internal Unauthenticated": livenessProbeInternalUnauthenticatedProbe,
			"Readiness Probe Manager External API":             livenessProbeExternalManagerAPI,
			"Readiness Probe Manager Internal API":             livenessProbeInternalManagerAPI,
		}

		livenessProbes := map[string]monitoring.Check{
			"Liveness Probe Manager Internal Unauthenticated": livenessProbeInternalUnauthenticatedProbe,
			"Liveness Probe Manager External API":             livenessProbeExternalManagerAPI,
			"Liveness Probe Manager Internal API":             livenessProbeInternalManagerAPI,
		}

		// manager uses the same probes as liveness and readiness
		monitoringService, err := monitoring.NewMonitoringService(serveOpts.MonitoringAddress, logger, readinessProbes, livenessProbes)
		if err != nil {
			logger.Fatal("unable to create monitoring service", err)
		}

		go func() {
			if err = monitoringService.Start(); err != nil {
				if !errors.Is(err, http.ErrServerClosed) {
					logger.Fatal("error listening on monitoring service", err)
				}

				logger.Fatal("cannot start monitoringService", err)
			}
		}()

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn(fmt.Sprintf("invalid internal PKI key permissions: file-path: %s", serveOpts.KeyFile), err)
		}

		internalUnauthenticatedCertificate, err := common_tls.NewBundleFromFiles(serveOpts.InternalCertFile, serveOpts.InternalKeyFile, serveOpts.InternalRootCertFile)
		if err != nil {
			logger.Fatal("loading internal unauthenticated TLS files", err)
		}

		tokenCertificate, err := common_tls.NewBundleFromFiles(serveOpts.GroupTokenCertFile, serveOpts.GroupTokenKeyFile, serveOpts.GroupRootCert)
		if err != nil {
			logger.Fatal("loading token TLS files", err)
		}

		signatureCertificate, err := common_tls.NewBundleFromFiles(serveOpts.GroupContractCertFile, serveOpts.GroupContractKeyFile, serveOpts.GroupRootCert)
		if err != nil {
			logger.Fatal("loading contract signature TLS files", err)
		}

		appClock := clock.New()

		ctx := context.Background()

		db, err := postgresadapter.NewConnection(ctx, serveOpts.StoragePostgresDSN)
		if err != nil {
			logger.Fatal("can not create db connection:", err)
		}

		storage, err := postgresadapter.New(externalCertificate.RootCAs(), db, appClock)
		if err != nil {
			logger.Fatal("failed to setup postgresql database", err)
		}

		if serveOpts.SelfAddress != serveOpts.DirectoryManagerAddress && serveOpts.TxLogAPIAddress == "" {
			logger.Fatal("tx-log-api-address is required when the manager does not function as the directory", errors.New("tx-log-api-address not set"))
		}

		crl, err := common_tls.NewCRL(serveOpts.CRLURLs)
		if err != nil {
			logger.Fatal("could not create CRLsCache", err)
		}

		managerFactory := rest.NewFactory(externalCertificate, serveOpts.SelfAddress, crl, appClock)

		var auditAdapter common_auditlog.AuditLogger

		switch serveOpts.AuditlogType {
		case "stdout":
			auditAdapter, err = common_auditlog.NewStdoutAuditlogger()
			if err != nil {
				logger.Fatal("could not create stdout Audit logger", err)
			}
		case "rest":
			var auditLogClient *api_auditlog.ClientWithResponses

			auditLogClient, err = api_auditlog.NewClientWithResponses(serveOpts.AuditlogRESTAddress, func(c *api_auditlog.Client) error {
				t := http.DefaultTransport.(*http.Transport).Clone()
				t.TLSClientConfig = internalCertificate.TLSConfig()

				c.Client = &http.Client{Transport: t}

				return nil
			})
			if err != nil {
				logger.Fatal("could not create AuditLog client", err)
			}

			auditAdapter, err = common_auditlog.NewRestAPIAuditLogger(auditLogClient)
			if err != nil {
				logger.Fatal("could not create REST Audit logger", err)
			}
		default:
			logger.Fatal(fmt.Sprintf("unknown auditlog type: %q", serveOpts.AuditlogType), nil)
		}

		auditLogger, err := auditlog.New(&auditlog.NewArgs{
			AuditLogger: auditAdapter,
			GroupID:     serveOpts.GroupID,
			Clock:       appClock,
			IDGenerator: uuidgenerator.New(),
		})
		if err != nil {
			logger.Fatal("cannot create Audit log client", err)
		}

		controllerClient, err := controllerapi.NewClientWithResponses(serveOpts.ControllerAPIAddress, func(c *controllerapi.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = internalCertificate.TLSConfig()

			c.Client = &http.Client{Transport: t}
			return nil
		})
		if err != nil {
			logger.Fatal("could not create rest controller client", err)
		}

		controllerRepository, err := restcontroller.New(ctx, serveOpts.GroupID, controllerClient)
		if err != nil {
			logger.Fatal("could not create rest controller repository", err)
		}

		var txLogRepository txlog.TXLog

		if serveOpts.TxLogAPIAddress != "" {
			txLogRepository, err = createTxLogRepository(serveOpts.TxLogAPIAddress, internalCertificate.TLSConfig())
			if err != nil {
				logger.Fatal("could not create txlog repository", err)
			}
		}

		selfPeer, err := contract.NewPeer(&contract.NewPeerArgs{
			ID:             externalCertificate.GetPeerInfo().SerialNumber,
			Name:           externalCertificate.GetPeerInfo().Name,
			ManagerAddress: serveOpts.SelfAddress,
		})
		if err != nil {
			logger.Fatal("could not create self peer", err)
		}

		tokenTTL, err := time.ParseDuration(serveOpts.TokenTTL)
		if err != nil {
			logger.Fatal("could not parse token-ttl duration", err)
		}

		groupID, err := contract.NewGroupID(serveOpts.GroupID)
		if err != nil {
			logger.Fatal(fmt.Sprintf("invalid group ID provided '%s'", serveOpts.GroupID), err)
		}

		contractDistributionService, err := services.NewContractDistributionService(&services.NewContractDistributionServiceArgs{
			Context:                              ctx,
			Logger:                               logger,
			Clock:                                appClock,
			FailedContractDistributionRepository: storage,
		})
		if err != nil {
			logger.Fatal("could not create contract distribution service", err)
		}

		autoSigner := neverallow.New()

		if len(serveOpts.AutoSignGrants) > 0 && serveOpts.AutoSignPDPAddress != "" {
			logger.Fatal("cannot combine the auto sign grants option with an auto-sign policy decision point", errors.New("invalid serve opts combination"))

		}

		if serveOpts.AutoSignPDPAddress != "" {
			ca, err := common_tls.NewCertPoolFromFile(serveOpts.AutoSignPDPCA)
			if err != nil {
				logger.Fatal("could not load auto-sign policy decision point ca", err)
			}

			tlsConfig := common_tls.NewConfig(common_tls.WithTLS12())
			tlsConfig.RootCAs = ca

			autoSigner, err = pdp.New(serveOpts.AutoSignPDPAddress, logger, http.Client{
				Transport: createHTTPTransport(tlsConfig),
			})
			if err != nil {
				logger.Fatal("could not create policy decision point auto-signer", err)
			}
		}

		if len(serveOpts.AutoSignGrants) > 0 {
			autoSigner, err = granttype.New(serveOpts.AutoSignGrants)
			if err != nil {
				logger.Fatal("could not create grant type auto signer", err)
			}
		}

		peersCommunicationService, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
			Ctx:                         ctx,
			Clock:                       appClock,
			Logger:                      logger,
			LocalRepo:                   storage,
			ContractDistributionService: contractDistributionService,
			ManagerFactory:              managerFactory,
			SelfPeerID:                  selfPeer.ID(),
		})
		if err != nil {
			logger.Fatal("could not create peers communication service", err)
		}

		internalApp, err := internalservice.NewApplication(&internalservice.NewApplicationArgs{
			Context:                     ctx,
			Logger:                      logger,
			GroupID:                     groupID,
			Repository:                  storage,
			ControllerRepository:        controllerRepository,
			PeersCommunicationService:   peersCommunicationService,
			ContractDistributionService: contractDistributionService,
			TXLog:                       txLogRepository,
			Clock:                       appClock,
			SelfPeer:                    selfPeer,
			TrustedExternalRootCAs:      externalCertificate.RootCAs(),
			SignatureCertificate:        signatureCertificate.Cert(),
			ManagerFactory:              managerFactory,
			IsTxlogDisabled:             txLogRepository == nil,
			DirectoryPeerID:             serveOpts.DirectoryPeerID,
			DirectoryPeerManagerAddress: serveOpts.DirectoryManagerAddress,
			Auditlogger:                 auditLogger,
			InternalCertificate:         internalCertificate,
		})
		if err != nil {
			logger.Fatal("could not create application internal", err)
		}

		err = internalApp.Commands.CreateCertificate.Handle(ctx, tokenCertificate.Cert().Certificate)
		if err != nil {
			logger.Fatal("could not create token certificate", err)
		}

		err = internalApp.Commands.CreateCertificate.Handle(ctx, signatureCertificate.Cert().Certificate)
		if err != nil {
			logger.Fatal("could not create signature certificate", err)
		}

		externalApp, err := externalservice.NewApplication(&externalservice.NewApplicationArgs{
			Context:                   ctx,
			Logger:                    logger,
			Repository:                storage,
			ControllerRepository:      controllerRepository,
			TXLogRepository:           txLogRepository,
			Clock:                     appClock,
			PeersCommunicationService: peersCommunicationService,
			GroupID:                   groupID,
			SelfPeer:                  selfPeer,
			TrustedRootCAs:            externalCertificate.RootCAs(),
			AutoSignCertificate:       signatureCertificate.Cert(),
			TokenSignCertificate:      tokenCertificate.Cert(),
			TokenTTL:                  tokenTTL,
			IsTxlogDisabled:           txLogRepository == nil,
			AutoSigner:                autoSigner,
		})
		if err != nil {
			logger.Fatal("could not create application external", err)
		}

		restServerExternal, err := externalrest.New(&externalrest.NewArgs{
			Logger:      logger,
			App:         externalApp,
			Cert:        externalCertificate,
			SelfAddress: serveOpts.SelfAddress,
			CRLCache:    crl,
		})
		if err != nil {
			logger.Fatal("could not setup external rest server", err)
		}

		metricsCollector, err := metrics.NewCollector(logger, storage, []metrics.Metric{
			metrics.NewNumberOfContractsWaitingForSignature(ctx, storage),
			metrics.NewNumberOfContractsWaitingForSignatureInLast72Hours(ctx, storage, appClock),
			metrics.NewNumberOfContractsDistributionFailures(ctx, storage)})
		if err != nil {
			logger.Fatal("could not create metrics collector", err)
		}

		go func() {
			var collectionInterval = 30 * time.Second
			metricsCollector.Start(collectionInterval)
		}()

		var readHeaderTimeout = 5 * time.Second
		srv := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restServerExternal.Handler(),
			Addr:              serveOpts.ListenAddressExternal,
			TLSConfig:         externalCertificate.TLSConfig(externalCertificate.WithTLSClientAuth()),
			ErrorLog:          log.New(slogerrorwriter.New(logger.Logger), "", 0),
		}

		go func() {
			err = srv.ListenAndServeTLS(serveOpts.GroupCertFile, serveOpts.GroupKeyFile)
			if err != nil && !errors.Is(err, http.ErrServerClosed) {
				logger.Fatal("could not listen and serve external rest server", err)
			}
		}()

		restServerInternal, err := internalrest.New(&internalrest.NewArgs{
			Logger:     logger,
			App:        internalApp,
			Cert:       internalCertificate,
			SelfPeerID: selfPeer.ID(),
			Clock:      appClock,
		})
		if err != nil {
			logger.Fatal("could not create internal rest server", err)
		}

		srvInternal := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restServerInternal.Handler(),
			Addr:              serveOpts.ListenAddressInternal,
			TLSConfig:         internalCertificate.TLSConfig(internalCertificate.WithTLSClientAuth()),
			ErrorLog:          log.New(slogerrorwriter.New(logger.Logger), "", 0),
		}

		go func() {
			err = srvInternal.ListenAndServeTLS(serveOpts.CertFile, serveOpts.KeyFile)
			if err != nil && !errors.Is(err, http.ErrServerClosed) {
				logger.Fatal("could not listen and serve internal rest server", err)
			}
		}()

		restServerInternalUnauthenticated, err := internalunauthenticatedrest.New(&internalunauthenticatedrest.NewArgs{
			Logger: logger,
			App:    internalApp,
		})
		if err != nil {
			logger.Fatal("could not create internal unauthenticated rest server", err)
		}

		srvInternalUnauthenticated := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restServerInternalUnauthenticated.Handler(),
			Addr:              serveOpts.ListenAddressInternalUnauthenticated,
			TLSConfig:         internalUnauthenticatedCertificate.TLSConfig(),
			ErrorLog:          log.New(slogerrorwriter.New(logger.Logger), "", 0),
		}

		go func() {
			err = srvInternalUnauthenticated.ListenAndServeTLS(serveOpts.InternalCertFile, serveOpts.InternalKeyFile)
			if err != nil && !errors.Is(err, http.ErrServerClosed) {
				logger.Fatal("could not listen and serve internal unauthenticated rest server", err)
			}
		}()

		go func() {
			err = peersCommunicationService.UpdateManagerAddress(ctx)
			if err != nil {
				logger.Error("could not update manager address and announce to all peers", err)
			}
		}()

		p.Wait()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		err = srvInternal.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown internal rest server", err)
		}

		err = srvInternalUnauthenticated.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown internal unauthenticated rest server", err)
		}

		err = srv.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown external rest server", err)
		}

		db.Close()

		metricsCollector.Stop()

		if err := monitoringService.Stop(); err != nil {
			logger.Error("could not shutdown monitoringService", err)
		}
	},
}

func createTxLogRepository(txLogAPIAddress string, tlsConfig *tls.Config) (txlog.TXLog, error) {
	txLogClient, err := txlogapi.NewClientWithResponses(txLogAPIAddress, func(c *txlogapi.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = tlsConfig

		c.Client = &http.Client{Transport: t}

		return nil
	})
	if err != nil {
		return nil, err
	}

	txLogRepository, err := resttxlog.New(txLogClient)
	if err != nil {
		return nil, err
	}

	return txLogRepository, nil
}

func createHTTPTransport(tlsConfig *tls.Config) *http.Transport {
	const (
		timeOut               = 30 * time.Second
		keepAlive             = 30 * time.Second
		maxIdleCons           = 100
		idleConnTimeout       = 20 * time.Second
		tlsHandshakeTimeout   = 10 * time.Second
		expectContinueTimeout = 1 * time.Second
	)

	return &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			a := &net.Dialer{
				Timeout:   timeOut,
				KeepAlive: keepAlive,
			}

			return a.DialContext(ctx, network, addr)
		},
		MaxIdleConns:          maxIdleCons,
		IdleConnTimeout:       idleConnTimeout,
		TLSHandshakeTimeout:   tlsHandshakeTimeout,
		ExpectContinueTimeout: expectContinueTimeout,
		TLSClientConfig:       tlsConfig,
	}
}
