// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package prometheus

import (
	"errors"
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type Metric struct {
	g prometheus.Gauge
	c func() (int, error)
	v any
}

func NewMetric(name, help string, collect func() (int, error)) *Metric {
	g := promauto.NewGauge(prometheus.GaugeOpts{
		Name: name,
		Help: help,
	})

	return &Metric{
		g: g,
		c: collect,
	}
}

func (m *Metric) Collect() error {
	value, err := m.c()
	if err != nil {
		return errors.Join(err, fmt.Errorf("failed to collect metric: %s", m.g.Desc().String()))
	}

	m.g.Set(float64(value))

	m.v = value

	return nil
}

func (m *Metric) Value() any {
	return m.v
}
