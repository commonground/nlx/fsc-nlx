// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package metrics

import (
	"context"
	"errors"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/metrics/prometheus"
)

type Collector struct {
	l           *logger.Logger
	metrics     []Metric
	stopChannel chan bool
}

type Metric interface {
	Collect() error
	Value() any
}

func NewCollector(l *logger.Logger, repository contract.Repository, metrics []Metric) (*Collector, error) {
	if l == nil {
		return nil, errors.New("logger required")
	}

	if repository == nil {
		return nil, errors.New("repository required")
	}

	c := &Collector{l: l, stopChannel: make(chan bool)}

	c.metrics = metrics

	return c, nil
}

func (b *Collector) Start(collectEvery time.Duration) {
	ticker := time.NewTicker(collectEvery)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			for _, m := range b.metrics {
				err := m.Collect()
				if err != nil {
					b.l.Error("could not collect metric", err)
				}
			}
		case <-b.stopChannel:
			return
		}
	}
}

func (b *Collector) Stop() {
	b.stopChannel <- true
}

func NewNumberOfContractsWaitingForSignature(ctx context.Context, storage contract.Repository) Metric {
	return prometheus.NewMetric("number_of_contracts_waiting_for_signature", "Contracts that are awaiting a signature", func() (int, error) {
		count, err := storage.GetContractsWaitingForSignatureCount(ctx, nil)
		if err != nil {
			return 0, nil
		}

		return count, nil
	})
}

func NewNumberOfContractsWaitingForSignatureInLast72Hours(ctx context.Context, storage contract.Repository, c clock.Clock) Metric {
	return prometheus.NewMetric("number_of_contracts_waiting_for_signature_72h", "Contracts that have been awaiting a signature for more than 72 hours.", func() (int, error) {
		before := c.Now().Add(-72 * time.Hour)

		count, err := storage.GetContractsWaitingForSignatureCount(ctx, &before)
		if err != nil {
			return 0, nil
		}

		return count, nil
	})
}

func NewNumberOfContractsDistributionFailures(ctx context.Context, storage contract.Repository) Metric {
	return prometheus.NewMetric("number_of_contracts_distribution_failures", "Contracts that could not be sent to other Peers", func() (int, error) {
		count, err := storage.GetContractFailedDistributionCount(ctx)
		if err != nil {
			return 0, nil
		}

		return count, nil
	})
}
