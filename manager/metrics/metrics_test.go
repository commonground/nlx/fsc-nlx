// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package metrics_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	mock_repository "gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/metrics"
)

type mockMetric struct {
	collectCount int
}

func (m *mockMetric) Collect() error {
	m.collectCount++
	return nil
}

func (m *mockMetric) Value() any {
	return m.collectCount
}

func TestNewCollector_NilLogger(t *testing.T) {
	repository := &mock_repository.MockRepository{}

	collector, err := metrics.NewCollector(nil, repository, []metrics.Metric{})

	assert.Nil(t, collector)
	assert.EqualError(t, err, "logger required")
}

func TestNewCollector_NilRepository(t *testing.T) {
	l := &logger.Logger{}

	collector, err := metrics.NewCollector(l, nil, []metrics.Metric{})

	assert.Nil(t, collector)
	assert.EqualError(t, err, "repository required")
}

func TestCollector_Start(t *testing.T) {
	mockLogger := &logger.Logger{}
	mockRepository := &mock_repository.MockRepository{}
	mMetric := &mockMetric{}

	collector, err := metrics.NewCollector(mockLogger, mockRepository, []metrics.Metric{mMetric})
	assert.NoError(t, err)

	collectInterval := 10 * time.Millisecond

	go collector.Start(collectInterval)

	time.Sleep(25 * time.Millisecond)

	collector.Stop()

	assert.GreaterOrEqual(t, mMetric.collectCount, 2)
}
