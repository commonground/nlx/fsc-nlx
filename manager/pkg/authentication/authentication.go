// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package authentication

type Data interface{}

type CertificateData struct {
	SubjectCommonName   string
	PublicKeyThumbprint string
}
