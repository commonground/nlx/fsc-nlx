// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package health_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/health"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestInternalUnauthenticatedProbeFailing(t *testing.T) {
	t.Parallel()

	ts := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusServiceUnavailable)
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.InternalUnauthenticatedProbe(testProbeURL.Host)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("unexpected response status code 503. url: https://%s/v1/health body: ", testProbeURL.Host))
}

func TestInternalUnauthenticatedProbeError(t *testing.T) {
	t.Parallel()

	ts := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("kaboom!")
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.InternalUnauthenticatedProbe(testProbeURL.Host)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("Get \"https://%s/v1/health\": EOF\nhealth check failed. url https://%s/v1/health", testProbeURL.Host, testProbeURL.Host))
}

func TestInternalUnauthenticatedProbeSuccess(t *testing.T) {
	t.Parallel()

	ts := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.InternalUnauthenticatedProbe(testProbeURL.Host)

	err = probe()
	assert.NoError(t, err)
}

func TestManagerAPIProbeError(t *testing.T) {
	t.Parallel()

	ts := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("kaboom!")
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.ManagerAPIProbe(orgCertBundle, testProbeURL.Host)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("Get \"https://%s/v1/health\": EOF\nhealth check failed. url https://%s/v1/health", testProbeURL.Host, testProbeURL.Host))
}

func TestManagerAPIProbeFailing(t *testing.T) {
	t.Parallel()

	ts := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusServiceUnavailable)
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.ManagerAPIProbe(orgCertBundle, testProbeURL.Host)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("unexpected response status code 503. url: https://%s/v1/health body: ", testProbeURL.Host))
}

func TestManagerAPIProbeSuccess(t *testing.T) {
	t.Parallel()

	ts := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.ManagerAPIProbe(orgCertBundle, testProbeURL.Host)

	err = probe()
	assert.NoError(t, err)
}
