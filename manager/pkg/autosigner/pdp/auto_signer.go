// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package pdp

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner/pdp/client"
)

type AutoSignerPDP struct {
	c         *client.ClientWithResponses
	logger    *logger.Logger
	reqEditor client.RequestEditorFn
}

func New(url string, l *logger.Logger, httpClient http.Client) (autosigner.AutoSigner, error) {
	if l == nil {
		return nil, errors.New("logger is required")
	}

	if url == "" {
		return nil, errors.New("url is required")
	}

	c, err := client.NewClientWithResponses(url, func(c *client.Client) error {
		c.Client = &httpClient

		return nil
	})
	if err != nil {
		return nil, err
	}

	a := &AutoSignerPDP{
		c:      c,
		logger: l,
	}

	if !strings.HasSuffix(url, "/") {
		a.reqEditor = trimTrailingSuffix
	}

	return a, nil
}

func (a *AutoSignerPDP) ShouldAutoSignContract(ctx context.Context, contractInfo *autosigner.ContractInfo) (bool, error) {
	i := client.Input{
		ContractContentHash: contractInfo.ContractContentHash.String(),
		GrantTypes:          convertGrantTypes(contractInfo.GrantTypes),
		PeerIds:             convertPeerIds(contractInfo.PeerIDs),
	}

	reqEditors := []client.RequestEditorFn{}
	if a.reqEditor != nil {
		reqEditors = append(reqEditors, a.reqEditor)
	}

	resp, err := a.c.ShouldAutoSignWithResponse(context.Background(), client.ShouldAutoSignJSONRequestBody(client.ShouldAutoSignJSONBody{
		Input: i,
	}), reqEditors...)
	if err != nil {
		return false, errors.Join(err, errors.New("failed to execute request"))
	}

	if resp.StatusCode() == http.StatusOK {
		if resp.JSON200 == nil {
			a.logger.Log(ctx, slog.LevelWarn, fmt.Sprintf("unexpected pdp response, no response body present: %+v", resp.JSON200))
			return false, nil
		}

		if !resp.JSON200.Result.Allowed {
			a.logger.Log(ctx, slog.LevelWarn, fmt.Sprintf("auto-sign not approved. pdp response: %+v", resp.JSON200.Result))
		}

		return resp.JSON200.Result.Allowed, nil
	}

	return false, nil
}

func trimTrailingSuffix(ctx context.Context, req *http.Request) error {
	req.URL.Path = strings.TrimSuffix(req.URL.Path, "/")

	return nil
}

func convertGrantTypes(grantTypes []contract.GrantType) []client.GrantType {
	var grantTypeStrings []client.GrantType

	for _, grantType := range grantTypes {
		switch grantType {
		case contract.GrantTypeServiceConnection:
			grantTypeStrings = append(grantTypeStrings, client.ServiceConnection)
		case contract.GrantTypeDelegatedServiceConnection:
			grantTypeStrings = append(grantTypeStrings, client.DelegatedServiceConnection)
		case contract.GrantTypeServicePublication:
			grantTypeStrings = append(grantTypeStrings, client.ServicePublication)
		case contract.GrantTypeDelegatedServicePublication:
			grantTypeStrings = append(grantTypeStrings, client.DelegatedServicePublication)
		}
	}

	return grantTypeStrings
}

func convertPeerIds(peerIDs []contract.PeerID) []client.PeerID {
	peerIDStrings := []client.PeerID{}

	for _, peerID := range peerIDs {
		peerIDStrings = append(peerIDStrings, peerID.Value())
	}

	return peerIDStrings
}
