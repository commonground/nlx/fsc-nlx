// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package neverallow

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner"
)

type AutoSignerNeverAllow struct{}

func New() autosigner.AutoSigner {
	return &AutoSignerNeverAllow{}
}

func (a *AutoSignerNeverAllow) ShouldAutoSignContract(_ context.Context, _ *autosigner.ContractInfo) (bool, error) {
	return false, nil
}
