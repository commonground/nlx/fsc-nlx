// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package granttype

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner"
)

type AutoSignerGrantType struct {
	typesToSign []contract.GrantType
}

func New(grantTypesToSign []string) (autosigner.AutoSigner, error) {
	gtToSign := []contract.GrantType{}

	for _, t := range grantTypesToSign {
		gt, err := contract.NewGrantTypeFromString(t)
		if err != nil {
			return nil, err
		}

		gtToSign = append(gtToSign, gt)
	}

	return &AutoSignerGrantType{
		typesToSign: gtToSign,
	}, nil
}

func (a *AutoSignerGrantType) ShouldAutoSignContract(_ context.Context, contractInfo *autosigner.ContractInfo) (bool, error) {
	if len(contractInfo.GrantTypes) == 0 {
		return false, nil
	}

	for _, grantType := range contractInfo.GrantTypes {
		autoSignGrant := false

		for _, grantTypeToSign := range a.typesToSign {
			if grantType == grantTypeToSign {
				autoSignGrant = true
				break
			}
		}

		if !autoSignGrant {
			return false, nil
		}
	}

	return true, nil
}
