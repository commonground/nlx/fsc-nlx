// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package autosigner

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type AutoSigner interface {
	ShouldAutoSignContract(ctx context.Context, contractInfo *ContractInfo) (bool, error)
}

type ContractInfo struct {
	ContractContentHash contract.ContentHash
	GrantTypes          []contract.GrantType
	PeerIDs             []contract.PeerID
}
