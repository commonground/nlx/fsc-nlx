// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:dupl,funlen // this is a test
func TestCreateContractContent_InvalidArguments(t *testing.T) {
	tests := map[string]struct {
		Args    *contract.NewContentArgs
		WantErr string
	}{
		"without_arguments": {
			Args:    nil,
			WantErr: "new content args cannot be nil",
		},
		"without_clock": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.Clock = nil
				return args
			}(),
			WantErr: "clock cannot be nil",
		},
		"without_group_id": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.GroupID = ""
				return args
			}(),
			WantErr: "group ID cannot be empty",
		},
		"invalid_group_id": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.GroupID = "with-special-characters!@#$%"
				return args
			}(),
			WantErr: "not a valid group ID",
		},
		"without_grants": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.Grants = nil
				return args
			}(),
			WantErr: "grants cannot be nil",
		},
		"empty_created_at": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.CreatedAt = time.Time{}
				return args
			}(),
			WantErr: "createdAt is required",
		},
		"created_at_in_the_future": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.CreatedAt = time.Now().Add(2 * time.Second).Truncate(time.Second)
				return args
			}(),
			WantErr: "createdAt cannot be in the future",
		},
		"created_at_contains_nanoseconds": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.CreatedAt = time.Now()
				return args
			}(),
			WantErr: "createdAt must be rounded on seconds",
		},
		"without_validity": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.Validity = nil
				return args
			}(),
			WantErr: "new validity args cannot be nil",
		},
		"invalid_validity_notbefore_not_rounded_on_seconds": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.Validity.NotBefore = time.Now()
				return args
			}(),
			WantErr: "NotBefore must be rounded on seconds",
		},
		"invalid_validity_notafter_not_rounded_on_seconds": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.Validity.NotAfter = time.Now()
				return args
			}(),
			WantErr: "NotAfter must be rounded on seconds",
		},
		"invalid_validity_notafter_invalid": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.Validity.NotAfter = time.Time{}
				return args
			}(),
			WantErr: "NotAfter cannot be zero",
		},
		"invalid_validity_notbefore_invalid": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.Validity.NotBefore = time.Time{}
				return args
			}(),
			WantErr: "NotBefore cannot be zero",
		},
		"invalid_iv": {
			Args: func() *contract.NewContentArgs {
				args := getValidNewContentArgs(t)
				args.IV = []byte("foo")
				return args
			}(),
			WantErr: "could not create content IV: uuid: UUID must be exactly 16 bytes long, got 3 bytes",
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			contractContent, err := contract.NewContent(test.Args)
			assert.Nil(t, contractContent)

			assert.NotNil(t, err)
			assert.Equal(t, test.WantErr, err.Error())
		})
	}
}

// nolint:dupl // this is a test
func getValidNewContentArgs(t *testing.T) *contract.NewContentArgs {
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	result := contract.NewContentArgs{
		Clock:         clock.New(),
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567890",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567891",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	}

	return &result
}

//nolint:dupl // this is a test
func TestCreateContractContent(t *testing.T) {
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	mockClock := testingutils.NewMockClock(time.Now())

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         mockClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     mockClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: mockClock.Now().Truncate(time.Second),
			NotAfter:  mockClock.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567890",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567891",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})

	assert.NoError(t, err)
	assert.NotNil(t, contractContent)
	assert.Len(t, contractContent.Grants().ServiceConnectionGrants(), 1)

	s, ok := contractContent.Grants().ServiceConnectionGrants()[0].Service().(*contract.GrantServiceConnectionService)
	require.True(t, ok)

	assert.Equal(t, "parkeerrechten", s.Name())
	assert.Equal(t, "12345678901234567891", s.Peer().ID().Value())
	assert.Equal(t, "12345678901234567890", contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value())
	assert.Equal(t, "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae", contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value())
	assert.Equal(t, contractIV, contractContent.IV().Bytes())
	assert.Equal(t, "fsc-local", contractContent.GroupID())
	assert.Equal(t, mockClock.Now().Truncate(time.Second), contractContent.CreatedAt().Truncate(time.Second))
}

//nolint:dupl // this is a test
func TestCreateContractContentWithInvalidPublicKeyThumbprint(t *testing.T) {
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         clock.New(),
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567890",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "invalid thumbprint",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567891",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})

	assert.Nil(t, contractContent)

	wantErr := &contract.ValidationError{}
	assert.ErrorAs(t, err, &wantErr)
	assert.ErrorContains(t, err, "the public key thumbprint is not valid. It must be a SHA-256 HEX encoded string")
}

//nolint:dupl // this is a test
func TestContract(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	testClock := testingutils.NewMockClock(now)

	c, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     now,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "Gemeente Stijns",
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, "$1$1$ESk3JfBB3ESqYLTWkE78zeKSNNhCRHibXLKqM9Clg26pUBwsCG_UQxsjgwtrHU6apVrBfrhOsSQMvjzpzb5Pkw", c.Hash().String())
	assert.Equal(t, "$1$2$s5MKxRh4KSN6Bm7wNK96ouaCUlvLUbqBzxD83eeFHWD8Cfr_Rn4mPbWOLoBoNmGAm5ntCN6zhrzHHvDa378Klg", c.Grants().ServicePublicationGrants()[0].Hash().String())
}

//nolint:dupl // this is a test
func TestValidity_NotBefore(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)
	nowWithNanoseconds := time.Date(2023, 1, 1, 1, 42, 1, 2, time.UTC)

	c, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: nowWithNanoseconds,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "Gemeente Stijns",
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})

	assert.Nil(t, c)
	assert.Error(t, err, "NotBefore must be rounded on seconds")
}

//nolint:dupl // this is a test
func TestValidity_NotAfter(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)
	nowWithNanoseconds := time.Date(2023, 1, 1, 1, 42, 1, 2, time.UTC)

	c, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  nowWithNanoseconds,
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "Gemeente Stijns",
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})

	assert.Nil(t, c)
	assert.Error(t, err, "NotAfter must be rounded on seconds")
}

//nolint:dupl // this is a test
func TestValidity_CreatedAt(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)
	nowWithNanoseconds := time.Date(2023, 1, 1, 1, 42, 1, 2, time.UTC)

	c, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     nowWithNanoseconds,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "Gemeente Stijns",
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})

	assert.Nil(t, c)
	assert.Error(t, err, "createdAt must be rounded on seconds")
}
