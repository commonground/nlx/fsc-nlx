// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract_test

import (
	"crypto/x509"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestNewPeerCertificatesWithExpiredCertificate(t *testing.T) {
	bundle, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-lower-expiration-chain.pem",
		"../../../testing/pki/org-nlx-test-b-lower-expiration-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	trustedRootCert := bundle.RootCAs()
	peerCertsRaw := bundle.Cert().Certificate

	testClock := testingutils.NewMockClock(time.Date(2125, 11, 6, 14, 10, 5, 0, time.UTC))

	_, err = contract.NewPeerCertFromCertificate(testClock, trustedRootCert, peerCertsRaw)
	assert.ErrorAs(t, err, &x509.CertificateInvalidError{})

	invalidCertErr := &x509.CertificateInvalidError{}
	errors.As(err, invalidCertErr)

	assert.Equal(t, x509.Expired, invalidCertErr.Reason)
}

//nolint:funlen // table tests are long
func TestNewPeerCertificates(t *testing.T) {
	t.Parallel()

	type args struct {
		clock          clock.Clock
		trustedRootCAs *x509.CertPool
		keys           contract.NewPeersCertificatesArgs
		certBundle     *tls.CertificateBundle
	}

	testCases := map[string]struct {
		setup   func() args
		wantErr error
	}{
		"invalid_peer_id": {
			setup: func() args {
				bundle, err := tls.NewBundleFromFiles(
					"../../../testing/pki/org-nlx-test-a-chain.pem",
					"../../../testing/pki/org-nlx-test-a-key.pem",
					"../../../testing/pki/ca-root.pem",
				)
				require.NoError(t, err)

				return args{
					clock:          testingutils.NewMockClock(time.Now()),
					trustedRootCAs: bundle.RootCAs(),
					keys: contract.NewPeersCertificatesArgs{
						"invalid peer id": bundle.Cert().Certificate,
					},
				}
			},
			wantErr: errors.New("could not create peers certificates, invalid peer id: peerID cannot be smaller than 20 characters"),
		},
		"invalid_cert_bundle": {
			setup: func() args {
				bundle, err := tls.NewBundleFromFiles(
					"../../../testing/pki/org-without-name-chain.pem",
					"../../../testing/pki/org-without-name-key.pem",
					"../../../testing/pki/ca-root.pem",
				)
				require.NoError(t, err)

				return args{
					clock:          testingutils.NewMockClock(time.Now()),
					trustedRootCAs: bundle.RootCAs(),
					keys: contract.NewPeersCertificatesArgs{
						"00000000000000000002": bundle.Cert().Certificate,
					},
					certBundle: bundle,
				}
			},
			wantErr: errors.New("could not create peers certificates, invalid peer certs: could not create peer certificates: could not verify peer certificate: x509: certificate signed by unknown authority"),
		},
		"invalid_root_ca": {
			setup: func() args {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				bundle2, err := tls.NewBundleFromFiles(
					"../../../testing/pki/org-without-name-chain.pem",
					"../../../testing/pki/org-without-name-key.pem",
					"../../../testing/pki/ca-root.pem",
				)
				require.NoError(t, err)

				return args{
					clock:          testingutils.NewMockClock(time.Now()),
					trustedRootCAs: bundle2.RootCAs(),
					keys: contract.NewPeersCertificatesArgs{
						"00000000000000000001": contract.RawDERPeerCertificates{
							bundle.Certificate().Raw,
						},
					},
					certBundle: bundle,
				}
			},
			wantErr: errors.New("could not create peers certificates, invalid peer certs: could not create peer certificates: could not verify peer certificate: x509: certificate signed by unknown authority"),
		},
		"missing_root_ca": {
			setup: func() args {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				return args{
					clock:          testingutils.NewMockClock(time.Now()),
					trustedRootCAs: nil,
					keys: contract.NewPeersCertificatesArgs{
						"00000000000000000001": contract.RawDERPeerCertificates{
							bundle.Certificate().Raw,
						},
					},
					certBundle: bundle,
				}
			},
			wantErr: errors.New("could not create peers certificates, invalid peer certs: could not create peer certificates: could not create peer certificate, missing trusted root CAs"),
		},
		"invalid_raw_certificate": {
			setup: func() args {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				return args{
					clock:          testingutils.NewMockClock(time.Now()),
					trustedRootCAs: bundle.RootCAs(),
					keys: contract.NewPeersCertificatesArgs{
						"00000000000000000001": contract.RawDERPeerCertificates{
							[]byte("invalid DER certificate"),
						},
					},
					certBundle: bundle,
				}
			},
			wantErr: errors.New("could not create peers certificates, invalid peer certs: could not create peer certificates: could not create peer certificate, invalid certificate bytes: x509: malformed certificate"),
		},
		"happy_flow": {
			setup: func() args {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				return args{
					clock:          testingutils.NewMockClock(time.Now()),
					trustedRootCAs: bundle.RootCAs(),
					keys: contract.NewPeersCertificatesArgs{
						"00000000000000000001": contract.RawDERPeerCertificates{
							bundle.Certificate().Raw,
						},
					},
					certBundle: bundle,
				}
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			a := tc.setup()
			peersCertificates, err := contract.NewPeersCertificates(a.clock, a.trustedRootCAs, a.keys)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.NotNil(t, peersCertificates)
				peerCertificate, err := peersCertificates.GetPeerCertificate(contract.CertificateThumbprint(a.certBundle.CertificateThumbprint()))
				assert.NoError(t, err)
				assert.Equal(t, a.certBundle.CertificateThumbprint(), peerCertificate.CertificateThumbprint().Value())
				assert.True(t, peerCertificate.CertificateThumbprint().IsEqual(contract.CertificateThumbprint(a.certBundle.CertificateThumbprint())))
				assert.Equal(t, a.certBundle.GetPeerInfo().SerialNumber, peerCertificate.Peer().ID().Value())
				assert.Equal(t, a.certBundle.Certificate().Raw, peerCertificate.Raw())
				assert.Equal(t, a.certBundle.Certificate().SignatureAlgorithm, peerCertificate.SignatureAlgorithm())
				assert.Equal(t, contract.CertKeyTypeRSA, peerCertificate.KeyType())
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}

func TestNewCertificateThumbprint(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		args    string
		want    contract.CertificateThumbprint
		wantErr error
	}{
		"missing_thumbprint": {
			args:    "",
			wantErr: errors.New("certificate thumbprint cannot be empty"),
		},
		"invalid_thumbprint": {
			args:    "invalid thumbprint",
			wantErr: errors.New("could not decode base64 encoded certificate thumbprint: illegal base64 data at input byte 7"),
		},
		"invalid_thumbprint_length": {
			args:    "dGhpcyB0aHVtYnByaW50IGlzIGludmFsaWQgYW5kIHdheSB0b28gbG9uZw",
			wantErr: errors.New("decoded certificate thumbprint must be exactly 32 bytes long"),
		},
		"happy_flow": {
			args: "20iQeqBFQx0Yx3hVvlOiPiRWQvxxfD7FL2u8dAmJTOc",
			want: contract.CertificateThumbprint("20iQeqBFQx0Yx3hVvlOiPiRWQvxxfD7FL2u8dAmJTOc"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			thumbprint, err := contract.NewCertificateThumbprint(tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, thumbprint)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
