// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestServiceConnectionHash(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	testClock := testingutils.NewMockClock(now)

	c, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     now,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "fcde2b2edba56bf408601fb721fe9b5c338d10ee429ea04fae5511b68fbf8fb9",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, "$1$3$Ne0JqAmaJjjNa33P_g2H_SS0vbtf1iyE40gprl6dMhIIUGyknfj2JLdiBGFWnFL-eJSZ8ifR-Eg7uzKgG7ZG3w", c.Grants().ServiceConnectionGrants()[0].Hash().String())
}
