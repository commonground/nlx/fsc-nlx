// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gopkg.in/square/go-jose.v2"
)

func (c *Content) Accept(trustedRootCAs *x509.CertPool, signWithCert *tls.Certificate, signedAt time.Time) (*Signature, error) {
	return c.createSignature(SignatureTypeAccept, trustedRootCAs, signWithCert, signedAt)
}

func (c *Content) Reject(trustedRootCAs *x509.CertPool, signWithCert *tls.Certificate, signedAt time.Time) (*Signature, error) {
	return c.createSignature(SignatureTypeReject, trustedRootCAs, signWithCert, signedAt)
}

func (c *Content) Revoke(trustedRootCAs *x509.CertPool, signWithCert *tls.Certificate, signedAt time.Time) (*Signature, error) {
	return c.createSignature(SignatureTypeRevoke, trustedRootCAs, signWithCert, signedAt)
}

func (c *Content) createSignature(signatureType SignatureType, trustedRootCAs *x509.CertPool, signWithCert *tls.Certificate, signedAt time.Time) (*Signature, error) {
	if signWithCert == nil {
		return nil, fmt.Errorf("cannot sign content, missing certificate")
	}

	if signedAt.Nanosecond() != 0 {
		return nil, fmt.Errorf("signed timestamp must be rounded on seconds")
	}

	peerCert, err := NewPeerCertFromCertificate(c.clock, trustedRootCAs, signWithCert.Certificate)
	if err != nil {
		return nil, errors.Wrapf(err, "could not create signature, invalid certificate chain")
	}

	options := &jose.SignerOptions{}
	options.WithHeader(jwsX5tHeader, peerCert.CertificateThumbprint())

	signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: signWithCert.PrivateKey}, options)
	if err != nil {
		return nil, fmt.Errorf("error creating new signer: %v", err)
	}

	payload := &SignaturePayload{
		ContentHash: c.Hash().String(),
		SigType:     signatureType.String(),
		SignedAt:    signedAt.Unix(),
	}

	payloadData, err := json.Marshal(payload)
	if err != nil {
		return nil, fmt.Errorf("error while marshaling signature payload: %v", err)
	}

	jws, err := signer.Sign(payloadData)
	if err != nil {
		return nil, fmt.Errorf("error while signing signature payload: %v", err)
	}

	signatureJWS, err := jws.CompactSerialize()
	if err != nil {
		return nil, fmt.Errorf("cannot marshal signature: %v", err)
	}

	return &Signature{
		contentHash:           c.Hash(),
		peer:                  peerCert.Peer(),
		sigType:               signatureType,
		certificateThumbprint: peerCert.CertificateThumbprint(),
		jws:                   signatureJWS,
		cert:                  peerCert,
		signedAt:              signedAt,
	}, nil
}
