// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package contract

import (
	"fmt"
	"time"
)

type NewContractArgs struct {
	Content            *NewContentArgs
	PeersCerts         PeersCertificates
	SignaturesAccepted map[string]string
	SignaturesRejected map[string]string
	SignaturesRevoked  map[string]string
}

type Contract struct {
	content            *Content
	signaturesAccepted signaturesAccepted
	signaturesRejected signaturesRejected
	signaturesRevoked  signaturesRevoked
}

func NewContract(args *NewContractArgs) (*Contract, error) {
	if args == nil {
		return nil, fmt.Errorf("new contract args cannot be nil")
	}

	content, err := NewContent(args.Content)
	if err != nil {
		return nil, err
	}

	signaturesAcc, err := newSignaturesAccepted(&newSignaturesArgs{
		Sigs:       mapSignatureArgs(args.SignaturesAccepted),
		Content:    content,
		PeersCerts: args.PeersCerts,
	})
	if err != nil {
		return nil, err
	}

	signaturesRej, err := newSignaturesRejected(&newSignaturesArgs{
		Sigs:       mapSignatureArgs(args.SignaturesRejected),
		Content:    content,
		PeersCerts: args.PeersCerts,
	})
	if err != nil {
		return nil, err
	}

	signaturesRev, err := newSignaturesRevoked(&newSignaturesArgs{
		Sigs:       mapSignatureArgs(args.SignaturesRevoked),
		Content:    content,
		PeersCerts: args.PeersCerts,
	})
	if err != nil {
		return nil, err
	}

	return &Contract{
		content:            content,
		signaturesAccepted: signaturesAcc,
		signaturesRejected: signaturesRej,
		signaturesRevoked:  signaturesRev,
	}, nil
}

func (c *Contract) SignaturesAccepted() signaturesAccepted {
	return c.signaturesAccepted
}

func (c *Contract) SignaturesRejected() signaturesRejected {
	return c.signaturesRejected
}

func (c *Contract) SignaturesRevoked() signaturesRevoked {
	return c.signaturesRevoked
}

func (c *Contract) IsAcceptedBy(p PeerID) bool {
	return c.signaturesAccepted.contains(p)
}

func (c *Contract) IsRejectedBy(p PeerID) bool {
	return c.signaturesRejected.contains(p)
}

func (c *Contract) IsRevokedBy(p PeerID) bool {
	return c.signaturesRevoked.contains(p)
}

func (c *Contract) Content() *Content {
	return c.content
}

type ContractState int

const (
	ContractStateUnspecified ContractState = iota
	ContractStateValid
	ContractStateProposed
	ContractStateRejected
	ContractStateRevoked
	ContractStateNotStarted
	ContractStateExpired
)

// State checks the state of the contract.
// e.g. checks for valid signatures and expiry dates
func (c *Contract) State(now time.Time) ContractState {
	sigsAccepted := c.SignaturesAccepted()
	sigsRejected := c.SignaturesRejected()
	sigsRevoked := c.SignaturesRevoked()

	for p := range c.Content().PeersIDs() {
		if sigsRejected.contains(p) {
			return ContractStateRejected
		}

		if sigsRevoked.contains(p) {
			return ContractStateRevoked
		}
	}

	for p := range c.Content().PeersIDs() {
		if !sigsAccepted.contains(p) {
			return ContractStateProposed
		}

		if sigsAccepted.contains(p) && (sigsAccepted[p].CertificateExpired(now) || sigsAccepted[p].CertificateNotActive(now)) {
			return ContractStateExpired
		}
	}

	if c.content.NotBefore().After(now) {
		return ContractStateNotStarted
	}

	if c.content.NotAfter().Before(now) {
		return ContractStateExpired
	}

	return ContractStateValid
}

func mapSignatureArgs(input map[string]string) map[string]*newSignatureArgs {
	result := make(map[string]*newSignatureArgs)

	if input == nil {
		return result
	}

	for peerID, signature := range input {
		result[peerID] = &newSignatureArgs{
			JWS: signature,
		}
	}

	return result
}
