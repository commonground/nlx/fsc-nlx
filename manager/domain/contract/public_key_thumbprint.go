// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract

import "regexp"

var isSHA = regexp.MustCompile(`^[a-fA-F0-9]{64}$`)

type PublicKeyThumbprint string

func NewPublicKeyThumbprint(thumbprint string) (*PublicKeyThumbprint, error) {
	if !isSHA.MatchString(thumbprint) {
		return nil, ErrorPublicKeyThumbprint()
	}

	publicKeyThumbprint := PublicKeyThumbprint(thumbprint)

	return &publicKeyThumbprint, nil
}

func (p *PublicKeyThumbprint) Value() string {
	return string(*p)
}
