// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract

type ServiceType int32

const (
	ServiceTypeService          ServiceType = 1
	ServiceTypeDelegatedService ServiceType = 2
)
