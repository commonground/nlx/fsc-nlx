// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract_test

import (
	"errors"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func TestNewGrantTypeFromString(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		args    string
		want    contract.GrantType
		wantErr error
	}{
		"empty_grant_type": {
			args:    "",
			wantErr: errors.New("grant type cannot be empty"),
			want:    contract.GrantTypeUnspecified,
		},
		"unspecified_grant_type": {
			args:    "idonotexists",
			wantErr: errors.New("invalid grant type: idonotexists"),
			want:    contract.GrantTypeUnspecified,
		},
		"service_publication": {
			args: "servicePublication",
			want: contract.GrantTypeServicePublication,
		},
		"service_connection": {
			args: "serviceConnection",
			want: contract.GrantTypeServiceConnection,
		},
		"delegated_service_connection": {
			args: "delegatedServiceConnection",
			want: contract.GrantTypeDelegatedServiceConnection,
		},
		"delegated_service_publication": {
			args: "delegatedServicePublication",
			want: contract.GrantTypeDelegatedServicePublication,
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			grantType, err := contract.NewGrantTypeFromString(tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}

			assert.Equal(t, tc.want, grantType)
		})
	}
}

//nolint:funlen // table tests are long
func TestGrantTypes(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		setup func() *contract.Content
		want  []contract.GrantType
	}{
		"service_connection_grants": {
			setup: func() *contract.Content {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				content, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         clock.New(),
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     time.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: time.Now().Truncate(time.Second),
						NotAfter:  time.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567890",
								},
								PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
							},
							Service: &contract.NewGrantServiceConnectionServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name: "parkeerrechten",
							},
						},
						&contract.NewGrantDelegatedServiceConnectionArgs{
							Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567890",
								},
								PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
							},
							Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name: "parkeerrechten",
							},
							Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
						},
						&contract.NewGrantDelegatedServiceConnectionArgs{
							Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567890",
								},
								PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
							},
							Service: &contract.NewGrantDelegatedServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
							Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
						},
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567890",
								},
								PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
							},
							Service: &contract.NewGrantServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
						},
					},
				})
				require.NoError(t, err)

				return content
			},
			want: []contract.GrantType{
				contract.GrantTypeServiceConnection,
				contract.GrantTypeDelegatedServiceConnection,
			},
		},
		"service_publication_grants": {
			setup: func() *contract.Content {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				content, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         clock.New(),
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     time.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: time.Now().Truncate(time.Second),
						NotAfter:  time.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServicePublicationArgs{
							Directory: &contract.NewGrantServicePublicationDirectoryArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
							Service: &contract.NewGrantServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name:     "test-service",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
					},
				})
				require.NoError(t, err)

				return content
			},
			want: []contract.GrantType{
				contract.GrantTypeServicePublication,
			},
		},
		"delegated_service_publication_grants": {
			setup: func() *contract.Content {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				content, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         clock.New(),
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     time.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: time.Now().Truncate(time.Second),
						NotAfter:  time.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantDelegatedServicePublicationArgs{
							Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
							Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name:     "test-service",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
							Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
						},
					},
				})
				require.NoError(t, err)

				return content
			},
			want: []contract.GrantType{
				contract.GrantTypeDelegatedServicePublication,
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			contractContent := tc.setup()
			grants := contractContent.Grants()
			grantTypes := grants.Types()

			for _, grant := range tc.want {
				assert.True(t, grantTypes[grant])
			}
		})
	}
}

//nolint:funlen // table tests are long
func TestGrantCombinations(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		setup   func() *contract.NewContentArgs
		wantErr error
	}{
		"service_connection_with_delegated_service_connection": {
			setup: func() *contract.NewContentArgs {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				return &contract.NewContentArgs{
					Clock:         clock.New(),
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     time.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: time.Now().Truncate(time.Second),
						NotAfter:  time.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantDelegatedServiceConnectionArgs{
							Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567890",
								},
								PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
							},
							Service: &contract.NewGrantDelegatedServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
							Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
						},
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567890",
								},
								PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
							},
							Service: &contract.NewGrantServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
						},
					},
				}
			},
		},
		"service_publication_with_delegated_service_publication": {
			setup: func() *contract.NewContentArgs {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				return &contract.NewContentArgs{
					Clock:         clock.New(),
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     time.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: time.Now().Truncate(time.Second),
						NotAfter:  time.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantDelegatedServicePublicationArgs{
							Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
							Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name:     "test-service",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
							Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
						},
						&contract.NewGrantServicePublicationArgs{
							Directory: &contract.NewGrantServicePublicationDirectoryArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
							},
							Service: &contract.NewGrantServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: "12345678901234567891",
								},
								Name:     "test-service",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
					},
				}
			},
			wantErr: contract.ErrorServicePublicationGrantCanNotBeMixedWithOtherGrants(),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			content, err := contract.NewContent(tc.setup())

			if tc.wantErr != nil {
				require.Error(t, err)
				assert.EqualError(t, err, tc.wantErr.Error())
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, content)
			}
		})
	}
}
