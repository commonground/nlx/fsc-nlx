// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"github.com/pkg/errors"
)

type grantPublicationHash Hash

func newGrantServicePublicationHash(gID GroupID, cID contentIV, alg HashAlg, gc *GrantServicePublication) (*grantPublicationHash, error) {
	h, err := newHash(alg, HashTypeGrantServicePublication, getSortedGrantPublicationBytes(gID, cID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant service publication hash")
	}

	return (*grantPublicationHash)(h), nil
}

func getSortedGrantPublicationBytes(gID GroupID, cID contentIV, gc *GrantServicePublication) []byte {
	grantBytes := make([]byte, 0)

	grantBytes = append(grantBytes, []byte(gID)...)
	grantBytes = append(grantBytes, cID.Bytes()...)
	grantBytes = append(grantBytes, bytesFromInt32(int32(GrantTypeServicePublication))...)
	grantBytes = append(grantBytes, []byte(gc.Directory().Peer().ID().Value())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Peer().ID())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Name())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Protocol().String())...)

	return grantBytes
}

func (h grantPublicationHash) String() string {
	return Hash(h).String()
}
