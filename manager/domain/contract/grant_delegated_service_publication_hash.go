// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"github.com/pkg/errors"
)

type grantDelegatedServicePublicationHash Hash

func newGrantDelegatedServicePublicationHash(gID GroupID, cID contentIV, alg HashAlg, gc *GrantDelegatedServicePublication) (*grantDelegatedServicePublicationHash, error) {
	h, err := newHash(alg, HashTypeGrantDelegatedServicePublication, getSortedGrantDelegatedPublicationBytes(gID, cID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant delegated service connection hash")
	}

	return (*grantDelegatedServicePublicationHash)(h), nil
}

func getSortedGrantDelegatedPublicationBytes(gID GroupID, cID contentIV, gc *GrantDelegatedServicePublication) []byte {
	grantBytes := make([]byte, 0)

	grantBytes = append(grantBytes, []byte(gID)...)
	grantBytes = append(grantBytes, cID.Bytes()...)
	grantBytes = append(grantBytes, bytesFromInt32(int32(GrantTypeDelegatedServicePublication))...)
	grantBytes = append(grantBytes, []byte(gc.Directory().Peer().ID().Value())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Peer().ID())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Name())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Protocol().String())...)
	grantBytes = append(grantBytes, []byte(gc.Delegator().Peer().ID().Value())...)

	return grantBytes
}

func (h grantDelegatedServicePublicationHash) String() string {
	return Hash(h).String()
}
