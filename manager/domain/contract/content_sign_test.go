// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract_test

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	internal_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:funlen,dupl // table tests are long and share similar setup
func TestSignContent(t *testing.T) {
	t.Parallel()

	type args struct {
		content        *contract.Content
		trustedRootCAs *x509.CertPool
		signWithCert   *tls.Certificate
		signedAt       time.Time
		signatureType  contract.SignatureType
	}

	testCases := map[string]struct {
		setup   func() *args
		execute func(a *args) (*contract.Signature, error)
		wantErr error
	}{
		"accept_signature": {
			setup: func() *args {
				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := internal_tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				return &args{
					content:        content,
					trustedRootCAs: bundle.RootCAs(),
					signWithCert:   bundle.Cert(),
					signedAt:       mockClock.Now().Truncate(time.Second),
					signatureType:  contract.SignatureTypeAccept,
				}
			},
			execute: func(a *args) (*contract.Signature, error) {
				return a.content.Accept(a.trustedRootCAs, a.signWithCert, a.signedAt)
			},
		},
		"reject_signature": {
			setup: func() *args {
				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := internal_tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				return &args{
					content:        content,
					trustedRootCAs: bundle.RootCAs(),
					signWithCert:   bundle.Cert(),
					signedAt:       mockClock.Now().Truncate(time.Second),
					signatureType:  contract.SignatureTypeReject,
				}
			},
			execute: func(a *args) (*contract.Signature, error) {
				return a.content.Reject(a.trustedRootCAs, a.signWithCert, a.signedAt)
			},
		},
		"revoke_signature": {
			setup: func() *args {
				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := internal_tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				return &args{
					content:        content,
					trustedRootCAs: bundle.RootCAs(),
					signWithCert:   bundle.Cert(),
					signedAt:       mockClock.Now().Truncate(time.Second),
					signatureType:  contract.SignatureTypeRevoke,
				}
			},
			execute: func(a *args) (*contract.Signature, error) {
				return a.content.Revoke(a.trustedRootCAs, a.signWithCert, a.signedAt)
			},
		},
		"missing_sign_with_cert": {
			setup: func() *args {
				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := internal_tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				return &args{
					content:        content,
					trustedRootCAs: bundle.RootCAs(),
					signWithCert:   nil,
					signedAt:       mockClock.Now().Truncate(time.Second),
				}
			},
			execute: func(a *args) (*contract.Signature, error) {
				return a.content.Revoke(a.trustedRootCAs, a.signWithCert, a.signedAt)
			},
			wantErr: errors.New("cannot sign content, missing certificate"),
		},
		"invalid_signed_at": {
			setup: func() *args {
				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := internal_tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				return &args{
					content:        content,
					trustedRootCAs: bundle.RootCAs(),
					signWithCert:   bundle.Cert(),
					signedAt:       mockClock.Now(),
				}
			},
			execute: func(a *args) (*contract.Signature, error) {
				return a.content.Accept(a.trustedRootCAs, a.signWithCert, a.signedAt)
			},
			wantErr: errors.New("signed timestamp must be rounded on seconds"),
		},
		"invalid_peer_certificate": {
			setup: func() *args {
				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := internal_tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				return &args{
					content:        content,
					trustedRootCAs: nil,
					signWithCert:   bundle.Cert(),
					signedAt:       mockClock.Now().Truncate(time.Second),
				}
			},
			execute: func(a *args) (*contract.Signature, error) {
				return a.content.Accept(a.trustedRootCAs, a.signWithCert, a.signedAt)
			},
			wantErr: errors.New("could not create signature, invalid certificate chain: could not create peer certificate, missing trusted root CAs"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			testCaseArgs := tc.setup()
			signature, err := tc.execute(testCaseArgs)

			if tc.wantErr == nil {
				require.NoError(t, err)
				assert.NotNil(t, signature)
				assert.Equal(t, testCaseArgs.content.Hash(), signature.ContentHash())
				assert.Equal(t, testCaseArgs.signedAt.Unix(), signature.SignedAt().Unix())
				assert.Equal(t, testCaseArgs.signWithCert.Leaf.PublicKey, signature.PeerCert().PublicKey())
				assert.Equal(t, testCaseArgs.signatureType, signature.Type())
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
