// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func TestDecodeHashFromString(t *testing.T) {
	t.Parallel()

	testcases := map[string]struct {
		args string
		want struct {
			hashType  contract.HashType
			hashAlg   contract.HashAlg
			hashValue string
		}
		wantErr error
	}{
		"empty_hash": {
			args:    "",
			wantErr: errors.New("hash cannot be empty"),
		},
		"too_long_hash": {
			args:    "thishashistoolongsinceitexceedsonehundredandelevencharactersthishashistoolongsinceitexceedsonehundredandelevencharacters",
			wantErr: errors.New("hash cannot be longer than 111 characters"),
		},
		"invalid_prefix_hash": {
			args:    "invalidHash",
			wantErr: errors.New("invalid hash prefix, must start with '$'"),
		},
		"invalid_hash_alg": {
			args:    "$3$1$UHl-GSepnzG1hTLywVps4W3BdfkCzvVxKI8mYEBaSTGR2mNkd3q2JYH3mn51B2TwQC2HqUTxuIcI9YFObJ3Zag",
			wantErr: errors.New("unsupported hash algorithm: 3"),
		},
		"invalid_hash_type": {
			args:    "$1$7$UHl-GSepnzG1hTLywVps4W3BdfkCzvVxKI8mYEBaSTGR2mNkd3q2JYH3mn51B2TwQC2HqUTxuIcI9YFObJ3Zag",
			wantErr: errors.New("unsupported hash type: 7"),
		},
		"empty_hash_value": {
			args:    "$1$2$",
			wantErr: errors.New("hash value cannot be empty"),
		},
		"happy_flow": {
			args: "$1$1$UHl-GSepnzG1hTLywVps4W3BdfkCzvVxKI8mYEBaSTGR2mNkd3q2JYH3mn51B2TwQC2HqUTxuIcI9YFObJ3Zag",
			want: struct {
				hashType  contract.HashType
				hashAlg   contract.HashAlg
				hashValue string
			}{hashType: contract.HashTypeContent, hashAlg: contract.HashAlgSHA3_512, hashValue: "$1$1$UHl-GSepnzG1hTLywVps4W3BdfkCzvVxKI8mYEBaSTGR2mNkd3q2JYH3mn51B2TwQC2HqUTxuIcI9YFObJ3Zag"},
		},
	}

	for name, tc := range testcases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			hash, err := contract.DecodeHashFromString(tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.NotNil(t, hash)
				assert.Equal(t, tc.want.hashType, hash.Type())
				assert.Equal(t, tc.want.hashAlg, hash.Algorithm())
				assert.Equal(t, tc.want.hashValue, hash.String())
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
