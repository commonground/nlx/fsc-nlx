// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package contract

import (
	"context"
	"errors"
	"time"
)

var ErrTokenInfoNotFound = errors.New("could not find token info for grant hash")

type Repository interface {
	UpsertContent(ctx context.Context, c *Content) error
	UpsertSignature(ctx context.Context, s *Signature) error
	UpsertCertificate(ctx context.Context, certificate *PeerCertificate) error
	GetContentByHash(ctx context.Context, hash *ContentHash) (*Content, error)
	GetPeersCertsByCertThumbprints(ctx context.Context, thumbprints CertificateThumbprints) (*PeersCertificates, error)
	GetPeerCerts(ctx context.Context, id PeerID) (*PeerCertificates, error)
	GetContractFailedDistributionCount(ctx context.Context) (int, error)
	GetContractsWaitingForSignatureCount(ctx context.Context, createdBefore *time.Time) (int, error)
	ListContractsForPeerByGrantHashes(ctx context.Context, peerID PeerID, grantHashes []string) ([]*Contract, error)
	ListContractsForPeer(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsForPeerWithServiceConnectionGrant(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsForPeerWithServicePublicationGrant(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsForPeerWithDelegatedServiceConnectionGrant(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsForPeerWithDelegatedServicePublicationGrant(ctx context.Context, peerID PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Contract, error)
	ListContractsPending(args *ListContractsPendingArgs) (*PendingContracts, error)
	ListContractsWithGrantTypeFilter(ctx context.Context, args *ListContractsWithGrantTypeFilterArgs) ([]*Contract, error)
	ListOutgoingConnections(args *ListOutgoingConnectionsArgs) (*OutgoingConnections, error)
	ListOutgoingConnectionsForService(args *ListOutgoingConnectionsForServiceArgs) ([]*Connection, error)
	ListIncomingConnections(ctx context.Context, args *ListIncomingConnectionsArgs) (*IncomingConnections, error)
	ListDelegatedConnections(ctx context.Context, args *ListDelegatedConnectionsArgs) (*DelegatedConnections, error)
	ListPeers(ctx context.Context, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Peer, error)
	ListPeersByID(ctx context.Context, peerIDs PeersIDs) (Peers, error)
	ListPeersByName(ctx context.Context, peerName string, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Peer, error)
	ListDirectoryPeers(ctx context.Context) ([]*Peer, error)
	ListServices(ctx context.Context, peertID *PeerID, serviceName string, paginationStartID string, paginationLimit uint32, paginationSortOrder SortOrder) ([]*Service, error)
	ListServicePublications(args *ListPublicationsArgs) (*Publications, error)
	ListDelegatedServicePublications(args *ListDelegatedPublicationsArgs) (*Publications, error)
	UpsertPeer(ctx context.Context, p *Peer) error
	UpsertPeerWithRoles(ctx context.Context, p *Peer) error
	GetTokenInfo(ctx context.Context, outwayPeer PeerID, selfPeer PeerID, grantHash *Hash) (*TokenInfo, error)
	GetServicesForOutway(ctx context.Context, outwayCertThumbprint string) (OutwayServices, error)
	ListContractsByContentHash(ctx context.Context, contentHashes []string) ([]*Contract, error)
	UpsertFailedContractDistribution(ctx context.Context, distribution *FailedDistribution) error
	ListFailedContractDistributions(ctx context.Context, contentHash *ContentHash) ([]*FailedDistribution, error)
	GetFailedContractDistribution(ctx context.Context, contentHash *ContentHash, peerID PeerID, action FailedDistributionAction) (*FailedDistribution, error)
	DeleteFailedContractDistributions(ctx context.Context, peerID PeerID, contentHash *ContentHash, action FailedDistributionAction) error
	Ping(ctx context.Context) error
}

type ListContractsWithGrantTypeFilterArgs struct {
	PaginationStartDate                   time.Time
	PaginationStartHash                   string
	PaginationLimit                       uint32
	PaginationSortOrder                   SortOrder
	WithServiceConnectionGrants           bool
	WithDelegatedServiceConnectionGrants  bool
	WithServicePublicationGrants          bool
	WithDelegatedServicePublicationGrants bool
}

type Service struct {
	ContractIV         string
	PeerID             string
	PeerManagerAddress string
	PeerName           string
	DelegatorPeerID    string
	DelegatorPeerName  string
	Name               string
	Protocol           ServiceProtocol
}

type TokenInfo struct {
	GroupID                string
	PublicKeyThumbprint    string
	ServiceName            string
	ServiceDelegatorPeerID string
	OutwayDelegatorPeerID  string
}

type OutwayServices map[string]*OutwayService

type OutwayService struct {
	PeerID string
	Name   string
}

type TXLogRecords []*TXLogRecord

type TXLogRecord struct {
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     TXLogDirection
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type TXLogDirection int32

const (
	TXLogDirectionUnspecified TXLogDirection = iota
	TXLogDirectionIn
	TXLogDirectionOut
)

type TXLogRecordSource struct {
	OutwayPeerID string
}

type TXLogRecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type TXLogRecordDestination struct {
	ServicePeerID string
}

type TXLogRecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

type SortOrder string

const (
	SortOrderAscending  SortOrder = "asc"
	SortOrderDescending SortOrder = "desc"
)

type Publications struct {
	Count        int
	Publications []*Publication
}

type Publication struct {
	ServicePeerID   PeerID
	ServiceName     string
	ContentHash     string
	CreatedAt       time.Time
	ValidNotBefore  time.Time
	ValidNotAfter   time.Time
	ContractState   ContractState
	GrantHash       string
	DirectoryPeerID PeerID
	DelegatorPeerID PeerID
}

type ListPublicationsArgs struct {
	Ctx                 context.Context
	PeerID              PeerID
	PaginationCreatedAt int64
	PaginationGrantHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	IncludeCount        bool
	ContractStates      []ContractState
	ServiceNames        []string
}

type ListDelegatedPublicationsArgs struct {
	Ctx                 context.Context
	PeerID              PeerID
	PaginationCreatedAt int64
	PaginationGrantHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	IncludeCount        bool
	ContractStates      []ContractState
}

type PendingContracts struct {
	Count            int
	PendingContracts []*PendingContract
}

type Connection struct {
	ServicePeerID                     PeerID
	ServiceName                       string
	ContentHash                       string
	CreatedAt                         time.Time
	ValidNotBefore                    time.Time
	ValidNotAfter                     time.Time
	ContractState                     ContractState
	GrantHash                         string
	ServicePublicationDelegatorPeerID *PeerID
	OutwayPeerID                      PeerID
	OutwayPublicKeyThumbprint         PublicKeyThumbprint
	DelegatorPeerID                   *PeerID
}

type PendingContract struct {
	ContentHash    string
	Grants         []*ContractPendingGrant
	ValidNotBefore time.Time
}

type ContractPendingGrant struct {
	ServiceProtocol                   ServiceProtocol
	ServiceName                       string
	DirectoryPeerID                   PeerID
	ServicePeerID                     PeerID
	OutwayPeerID                      PeerID
	OutwayThumbprint                  string
	DelegatorPeerID                   PeerID
	ServicePublicationDelegatorPeerID PeerID
	GrantType                         GrantType
}

type ListContractsPendingArgs struct {
	Ctx                 context.Context
	SelfPeerID          PeerID
	PaginationStartHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	PaginationStartDate time.Time
	IncludeCount        bool
}

type ListContractsWithIncomingConnectionsArgs struct {
	SelfPeerID          string
	PaginationStartDate time.Time
	PaginationStartHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	IncludeCount        bool
}

type ListContractsWithOutgoingConnectionsArgs struct {
	SelfPeerID          string
	PaginationStartDate time.Time
	PaginationStartHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
}

type ListOutgoingConnectionsArgs struct {
	Ctx                 context.Context
	PeerID              PeerID
	PaginationCreatedAt int64
	PaginationGrantHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	IncludeCount        bool
	ContractStates      []ContractState
}

type ListOutgoingConnectionsForServiceArgs struct {
	Ctx             context.Context
	SelfPeerID      PeerID
	ServicePeerID   PeerID
	ServiceName     string
	DelegatorPeerID PeerID
	SortOrder       SortOrder
}

type OutgoingConnections struct {
	Connections []*Connection
	Count       int
}

type ListIncomingConnectionsArgs struct {
	Ctx                 context.Context
	PeerID              PeerID
	PaginationCreatedAt int64
	PaginationGrantHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	IncludeCount        bool
	ContractStates      []ContractState
	ServiceNames        []string
}

type IncomingConnections struct {
	Connections []*Connection
	Count       int
}

type ListDelegatedConnectionsArgs struct {
	Ctx                 context.Context
	PeerID              PeerID
	PaginationCreatedAt int64
	PaginationGrantHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	IncludeCount        bool
	ContractStates      []ContractState
}

type DelegatedConnections struct {
	Connections []*Connection
	Count       int
}
