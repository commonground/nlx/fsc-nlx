// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract_test

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:funlen // table tests are long
func TestNewFailedDistribution(t *testing.T) {
	t.Parallel()

	mockClock := testingutils.NewMockClock(time.Now())

	testCases := map[string]struct {
		args    *contract.NewFailedDistributionArgs
		wantErr error
		want    *contract.FailedDistribution
	}{
		"new_failed_distribution_without_content_hash": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   nil,
				PeerID:        "1",
				Action:        string(contract.FailedDistributionActionSubmitAcceptSignature),
				Signature:     "test-signature",
				Attempts:      0,
				LastAttemptAt: mockClock.Now(),
				Reason:        "test-reason",
			},
			wantErr: errors.New("missing content hash"),
		},
		"new_failed_distribution_without_peer_id": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   &contract.ContentHash{},
				PeerID:        "",
				Action:        string(contract.FailedDistributionActionSubmitAcceptSignature),
				Signature:     "test-signature",
				Attempts:      0,
				LastAttemptAt: mockClock.Now(),
				Reason:        "test-reason",
			},
			wantErr: errors.New("missing peer id"),
		},
		"new_failed_distribution_invalid_peer_id": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   &contract.ContentHash{},
				PeerID:        "invalid peer id",
				Action:        string(contract.FailedDistributionActionSubmitAcceptSignature),
				Signature:     "test-signature",
				Attempts:      0,
				LastAttemptAt: mockClock.Now(),
				Reason:        "test-reason",
			},
			wantErr: errors.New("invalid peer id: peerID cannot be smaller than 20 characters"),
		},
		"new_failed_distribution_missing_action": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   &contract.ContentHash{},
				PeerID:        "00000000000000000001",
				Action:        "",
				Signature:     "test-signature",
				Attempts:      0,
				LastAttemptAt: mockClock.Now(),
				Reason:        "test-reason",
			},
			wantErr: errors.New("unsupported action \"\", must be one of submit_contract, submit_accept_signature, submit_revoke_signature, submit_reject_signature"),
		},
		"new_failed_distribution_invalid_action": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   &contract.ContentHash{},
				PeerID:        "00000000000000000001",
				Action:        "invalid action",
				Signature:     "test-signature",
				Attempts:      0,
				LastAttemptAt: mockClock.Now(),
				Reason:        "test-reason",
			},
			wantErr: errors.New("unsupported action \"invalid action\", must be one of submit_contract, submit_accept_signature, submit_revoke_signature, submit_reject_signature"),
		},
		"happy_flow_submit_contract": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   &contract.ContentHash{},
				PeerID:        "00000000000000000001",
				Action:        "submit_contract",
				Signature:     "test-signature",
				Attempts:      0,
				LastAttemptAt: mockClock.Now(),
				Reason:        "test-reason",
			},
		},
		"happy_flow_accept_contract": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   &contract.ContentHash{},
				PeerID:        "00000000000000000001",
				Action:        "submit_accept_signature",
				Signature:     "test-signature",
				Attempts:      0,
				LastAttemptAt: mockClock.Now(),
				Reason:        "test-reason",
			},
		},
		"happy_flow_revoke_contract": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   &contract.ContentHash{},
				PeerID:        "00000000000000000001",
				Action:        "submit_revoke_signature",
				Signature:     "test-signature",
				Attempts:      0,
				LastAttemptAt: mockClock.Now(),
				Reason:        "test-reason",
			},
		},
		"happy_flow_reject_contract": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   &contract.ContentHash{},
				PeerID:        "00000000000000000001",
				Action:        "submit_reject_signature",
				Signature:     "test-signature",
				Attempts:      0,
				LastAttemptAt: mockClock.Now(),
				Reason:        "test-reason",
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			failedDistribution, err := contract.NewFailedDistribution(tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.NotNil(t, failedDistribution)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}

//nolint:funlen // table tests are long
func TestFailedDistribution(t *testing.T) {
	t.Parallel()

	mockClock := testingutils.NewMockClock(time.Now())

	contentHashString := "$1$1$Emm9bnohULAEIVuAt3VMC5bc5AJnU8XY7CxAZ39LKq-CT8r2L_WZTWsdqHEyRQlFQU3-IIY2m3V92dEBqqasiw"
	contentHash, _ := contract.NewContentHashFromString(contentHashString)

	nextAttemptAt := mockClock.Now().Add(1 * time.Minute)
	newNextAttemptAt := mockClock.Now().Add(1 * time.Minute)

	type want struct {
		signature     string
		action        contract.FailedDistributionAction
		contentHash   *contract.ContentHash
		peerID        contract.PeerID
		lastAttemptAt time.Time
		nextAttemptAt *time.Time
		attempts      int32
		reason        string
	}

	testCases := map[string]struct {
		args  *contract.NewFailedDistributionArgs
		setup func(distribution *contract.FailedDistribution)
		want  want
	}{
		"happy_flow": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   contentHash,
				PeerID:        "00000000000000000001",
				Action:        "submit_accept_signature",
				Signature:     "test-signature",
				Attempts:      3,
				LastAttemptAt: mockClock.Now(),
				NextAttemptAt: &nextAttemptAt,
				Reason:        "test-reason",
			},
			want: want{
				signature:     "test-signature",
				action:        contract.FailedDistributionActionSubmitAcceptSignature,
				contentHash:   contentHash,
				peerID:        "00000000000000000001",
				lastAttemptAt: mockClock.Now(),
				nextAttemptAt: &nextAttemptAt,
				attempts:      3,
				reason:        "test-reason",
			},
		},
		"happy_flow_clear_next_retry": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   contentHash,
				PeerID:        "00000000000000000001",
				Action:        "submit_revoke_signature",
				Signature:     "test-signature",
				Attempts:      3,
				LastAttemptAt: mockClock.Now(),
				NextAttemptAt: &nextAttemptAt,
				Reason:        "test-reason",
			},
			setup: func(distribution *contract.FailedDistribution) {
				distribution.ClearNextRetry()
			},
			want: want{
				signature:     "test-signature",
				action:        contract.FailedDistributionActionSubmitRevokeSignature,
				contentHash:   contentHash,
				peerID:        "00000000000000000001",
				lastAttemptAt: mockClock.Now(),
				nextAttemptAt: nil,
				attempts:      3,
				reason:        "test-reason",
			},
		},
		"happy_flow_update_with_new_attempt": {
			args: &contract.NewFailedDistributionArgs{
				ContentHash:   contentHash,
				PeerID:        "00000000000000000001",
				Action:        "submit_accept_signature",
				Signature:     "test-signature",
				Attempts:      3,
				LastAttemptAt: mockClock.Now(),
				NextAttemptAt: &newNextAttemptAt,
				Reason:        "test-reason",
			},
			setup: func(distribution *contract.FailedDistribution) {
				distribution.UpdateWithNewAttempt(nextAttemptAt, "updated reason", &newNextAttemptAt)
			},
			want: want{
				signature:     "test-signature",
				action:        contract.FailedDistributionActionSubmitAcceptSignature,
				contentHash:   contentHash,
				peerID:        "00000000000000000001",
				lastAttemptAt: nextAttemptAt,
				nextAttemptAt: &newNextAttemptAt,
				attempts:      4,
				reason:        "updated reason",
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			failedDistribution, err := contract.NewFailedDistribution(tc.args)
			assert.NoError(t, err)

			if tc.setup != nil {
				tc.setup(failedDistribution)
			}

			assert.Equal(t, tc.want.signature, failedDistribution.Signature())
			assert.Equal(t, tc.want.action, failedDistribution.Action())
			assert.Equal(t, tc.want.contentHash, failedDistribution.ContentHash())
			assert.Equal(t, tc.want.peerID, failedDistribution.PeerID())
			assert.Equal(t, tc.want.lastAttemptAt, failedDistribution.LastAttemptAt())
			assert.Equal(t, tc.want.nextAttemptAt, failedDistribution.NextAttemptAt())
			assert.Equal(t, tc.want.attempts, failedDistribution.Attempts())
			assert.Equal(t, tc.want.reason, failedDistribution.Reason())
		})
	}
}
