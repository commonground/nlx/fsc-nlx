// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import "fmt"

type ErrorCode string

const (
	ErrorCodePeerNotPartOfContract                ErrorCode = "ERROR_CODE_PEER_NOT_PART_OF_CONTRACT"
	ErrorCodeGrantCombinationNotAllowed           ErrorCode = "ERROR_CODE_GRANT_COMBINATION_NOT_ALLOWED"
	ErrorCodeSignatureVerificationFailed          ErrorCode = "ERROR_CODE_SIGNATURE_VERIFICATION_FAILED"
	ErrorCodeSignatureContractContentHashMismatch ErrorCode = "ERROR_CODE_SIGNATURE_CONTRACT_CONTENT_HASH_MISMATCH"
	ErrorCodeSignatureUnknownAlgorithm            ErrorCode = "ERROR_CODE_UNKNOWN_ALGORITHM_SIGNATURE"
	ErrorCodePublicKeyThumbprintInvalid           ErrorCode = "ERROR_CODE_PUBLIC_KEY_THUMBPRINT_INVALID"
)

type ValidationError struct {
	message string
	code    ErrorCode
}

func (e *ValidationError) Error() string {
	return e.message
}

func (e *ValidationError) Code() string {
	return string(e.code)
}

func ErrorSubmittingPeerNotPartOfContract(submittingPeerID string) error {
	return &ValidationError{
		code:    ErrorCodePeerNotPartOfContract,
		message: fmt.Sprintf("submitting peer %s is not found in contract", submittingPeerID),
	}
}

func ErrorSignatureMustBeOfTypeAccepted(actualSignatureType string) error {
	return &ValidationError{
		code:    ErrorCodeSignatureVerificationFailed,
		message: fmt.Sprintf("signature must be of type expected. received signature is of type %q", actualSignatureType),
	}
}

func ErrorReceivingPeerNotPartOfContract(receivingPeerID string) error {
	return &ValidationError{
		code:    ErrorCodePeerNotPartOfContract,
		message: fmt.Sprintf("receiving %s is not found in contract", receivingPeerID),
	}
}

func ErrorSignatureContractContentHashDoesNotMatchTheContract(signatureContractContentHash, contractContentHash string) error {
	return &ValidationError{
		code:    ErrorCodeSignatureContractContentHashMismatch,
		message: fmt.Sprintf("the signature contract content hash %q does not match the contract content hash you are accepting %q", signatureContractContentHash, contractContentHash),
	}
}

func ErrorServicePublicationGrantCanNotBeMixedWithOtherGrants() error {
	return &ValidationError{
		code:    ErrorCodeGrantCombinationNotAllowed,
		message: "a ServicePublicationGrant cannot be mixed with other grants",
	}
}

func ErrorSignatureUnknownAlgorithm(submittedAlg string) error {
	return &ValidationError{
		code:    ErrorCodeSignatureUnknownAlgorithm,
		message: fmt.Sprintf("the algorithm %s in the signature is not supported", submittedAlg),
	}
}

func ErrorPublicKeyThumbprint() error {
	return &ValidationError{
		code:    ErrorCodePublicKeyThumbprintInvalid,
		message: "the public key thumbprint is not valid. It must be a SHA-256 HEX encoded string",
	}
}
