// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func TestPeerIDs(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		args    []string
		wantErr error
	}{
		"invalid_peer_id": {
			args:    []string{"invalid_peer_id"},
			wantErr: errors.New("invalid peer id in peer IDs: peerID cannot be smaller than 20 characters"),
		},
		"list_with_invalid_peer_id": {
			args:    []string{"00000000000000000001", "invalid_peer_id"},
			wantErr: errors.New("invalid peer id in peer IDs: peerID cannot be smaller than 20 characters"),
		},
		"happy_flow": {
			args: []string{"00000000000000000001", "12345678901234567890"},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			peerIDs, err := contract.NewPeerIDs(tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.NotNil(t, peerIDs)
				assert.Equal(t, tc.args, peerIDs.Value())
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}

func TestNewPeerID(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		args    string
		want    contract.PeerID
		wantErr error
	}{
		"empty_peer_id": {
			args:    "",
			wantErr: errors.New("peerID cannot be empty"),
		},
		"invalid_peer_id_too_long": {
			args:    "123456789012345678901",
			wantErr: errors.New("peerID cannot be longer than 20 characters"),
		},
		"invalid_peer_id_too_short": {
			args:    "1234567890123456789",
			wantErr: errors.New("peerID cannot be smaller than 20 characters"),
		},
		"happy_flow": {
			args: "12345678901234567890",
			want: contract.PeerID("12345678901234567890"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			peerID, err := contract.NewPeerID(tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, peerID)
				assert.Equal(t, tc.args, peerID.Value())
				assert.True(t, tc.want.IsEqualString(tc.args))
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}

func TestNewPeer(t *testing.T) {
	t.Parallel()

	type want struct {
		name           string
		managerAddress string
		roles          []contract.PeerRole
		peerID         string
	}

	testCases := map[string]struct {
		args    *contract.NewPeerArgs
		want    want
		wantErr error
	}{
		"invalid_peer_empty_args": {
			args:    nil,
			wantErr: errors.New("new peer args cannot be nil"),
		},
		"invalid_peer_id": {
			args: &contract.NewPeerArgs{
				ID:             "invalid peer id",
				Name:           "test peer",
				ManagerAddress: "https://example.com:443",
				Roles:          nil,
			},
			wantErr: errors.New("peerID cannot be smaller than 20 characters"),
		},
		"invalid_peer_name": {
			args: &contract.NewPeerArgs{
				ID:             "00000000000000000001",
				Name:           "test invalid peer name this peer name exceeds 64 characters..............",
				ManagerAddress: "https://example.com:443",
				Roles:          nil,
			},
			wantErr: errors.New("peer invalid peer name"),
		},
		"invalid_peer_manager_address": {
			args: &contract.NewPeerArgs{
				ID:             "00000000000000000001",
				Name:           "test-peer",
				ManagerAddress: "http://example.com:443",
				Roles:          nil,
			},
			wantErr: errors.New("peer invalid manager address: missing https prefix in manager address \"http://example.com:443\""),
		},
		"happy_flow": {
			args: &contract.NewPeerArgs{
				ID:             "00000000000000000001",
				Name:           "test-peer",
				ManagerAddress: "https://example.com:443",
				Roles:          nil,
			},
			want: want{
				name:           "test-peer",
				managerAddress: "https://example.com:443",
				roles:          nil,
				peerID:         "00000000000000000001",
			},
		},
		"happy_flow_with_roles": {
			args: &contract.NewPeerArgs{
				ID:             "00000000000000000001",
				Name:           "test-peer",
				ManagerAddress: "https://example.com:443",
				Roles:          []contract.PeerRole{contract.PeerRoleDirectory},
			},
			want: want{
				name:           "test-peer",
				managerAddress: "https://example.com:443",
				roles:          []contract.PeerRole{contract.PeerRoleDirectory},
				peerID:         "00000000000000000001",
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			peer, err := contract.NewPeer(tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want.peerID, peer.ID().Value())
				assert.Equal(t, tc.want.name, peer.Name().Value())
				assert.Equal(t, tc.want.managerAddress, peer.ManagerAddress().Value())
				assert.Equal(t, tc.want.roles, peer.Roles())
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
