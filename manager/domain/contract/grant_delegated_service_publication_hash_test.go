// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

// nolint:dupl // this is a test
package contract_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestDelegatedServicePublicationHash(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	testClock := testingutils.NewMockClock(now)

	c, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     now,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP2,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000003",
						Name:           "",
						ManagerAddress: "",
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, "$1$5$sIZ7BgV-lops2SHQS2xYwYhXfC3juNvJYnSKPta6aBBEngR9rJsbj01idsFJJ6ybbgnWUuGMoJkyogCIPP3vyA", c.Grants().DelegatedServicePublicationGrants()[0].Hash().String())
}

func TestDelegatedServicePublicationHashWithoutProtocol(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	testClock := testingutils.NewMockClock(now)

	c, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     now,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000003",
						Name:           "",
						ManagerAddress: "",
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, "$1$5$LlI8TYwWVmhhYGBtuICwU24Jn5abKV5KIzRJaoN4xVW07QajhWLdxEIRpzu_6tKgsshXM85zQWBpi9drcP3x8Q", c.Grants().DelegatedServicePublicationGrants()[0].Hash().String())
}
