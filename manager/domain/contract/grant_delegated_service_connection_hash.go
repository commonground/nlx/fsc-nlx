// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"github.com/pkg/errors"
)

type grantDelegatedServiceConnectionHash Hash

func newGrantDelegatedServiceConnectionHash(gID GroupID, cID contentIV, alg HashAlg, gc *GrantDelegatedServiceConnection) (*grantDelegatedServiceConnectionHash, error) {
	h, err := newHash(alg, HashTypeGrantDelegatedServiceConnection, getSortedGrantDelegatedConnectionBytes(gID, cID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant delegated service connection hash")
	}

	return (*grantDelegatedServiceConnectionHash)(h), nil
}

func getSortedGrantDelegatedConnectionBytes(gID GroupID, cIV contentIV, gc *GrantDelegatedServiceConnection) []byte {
	grantBytes := make([]byte, 0)

	grantBytes = append(grantBytes, []byte(gID)...)
	grantBytes = append(grantBytes, cIV.Bytes()...)
	grantBytes = append(grantBytes, bytesFromInt32(int32(GrantTypeDelegatedServiceConnection))...)
	grantBytes = append(grantBytes, []byte(gc.Outway().Peer().ID().Value())...)
	grantBytes = append(grantBytes, []byte(gc.Outway().PublicKeyThumbprint().Value())...)

	// nolint:dupl // looks the same but is a different grant
	switch s := gc.Service().(type) {
	case *GrantDelegatedServiceConnectionService:
		grantBytes = append(grantBytes, bytesFromInt32(int32(ServiceTypeService))...)
		grantBytes = append(grantBytes, []byte(s.Peer().ID())...)
		grantBytes = append(grantBytes, []byte(s.Name())...)
	case *GrantDelegatedServiceConnectionDelegatedService:
		grantBytes = append(grantBytes, bytesFromInt32(int32(ServiceTypeDelegatedService))...)
		grantBytes = append(grantBytes, []byte(s.Peer().ID())...)
		grantBytes = append(grantBytes, []byte(s.Name())...)
		grantBytes = append(grantBytes, []byte(s.PublicationDelegator().ID().Value())...)
	}

	grantBytes = append(grantBytes, []byte(gc.Delegator().Peer().ID().Value())...)

	return grantBytes
}

func (h grantDelegatedServiceConnectionHash) String() string {
	return Hash(h).String()
}
