// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"fmt"
	"slices"
)

type NewGrantServicePublicationArgs struct {
	Directory *NewGrantServicePublicationDirectoryArgs
	Service   *NewGrantServicePublicationServiceArgs
}

type NewGrantServicePublicationDirectoryArgs struct {
	Peer *NewPeerArgs
}

type NewGrantServicePublicationServiceArgs struct {
	Peer     *NewPeerArgs
	Name     string
	Protocol ServiceProtocol
}

type GrantServicePublication struct {
	hash      *grantPublicationHash
	directory *grantServicePublicationDirectory
	service   *grantServicePublicationService
}

type grantServicePublicationDirectory struct {
	peer *Peer
}

type serviceName string

type grantServicePublicationService struct {
	peer     *Peer
	name     serviceName
	protocol ServiceProtocol
}

const (
	serviceNameMaxLength  = 255
	inwayAddressMaxLength = 255 // valid DNS hostnames can't be longer than 255 characters: https://www.rfc-editor.org/rfc/rfc3986#section-3.2.2
)

func (g *GrantServicePublication) Directory() *grantServicePublicationDirectory {
	return g.directory
}

func (g *GrantServicePublication) Hash() *grantPublicationHash {
	return g.hash
}

func (g *GrantServicePublication) Service() *grantServicePublicationService {
	return g.service
}

func (s *grantServicePublicationService) Peer() *Peer {
	return s.peer
}

func (s *grantServicePublicationService) Name() string {
	return string(s.name)
}

func (s *grantServicePublicationService) Protocol() ServiceProtocol {
	return s.protocol
}

func (c *grantServicePublicationDirectory) Peer() *Peer {
	return c.peer
}

// nolint:dupl // grants are definitively similar but still have distinctive properties
func newGrantServicePublication(gID GroupID, cID contentIV, alg HashAlg, args *NewGrantServicePublicationArgs) (*GrantServicePublication, error) {
	if args == nil {
		return nil, fmt.Errorf("new service publication args cannot be nil")
	}

	directory, err := newGrantServicePublicationDirectory(args.Directory)
	if err != nil {
		return nil, err
	}

	service, err := newGrantServicePublicationService(args.Service)
	if err != nil {
		return nil, err
	}

	h, err := newGrantServicePublicationHash(gID, cID, alg, &GrantServicePublication{
		directory: directory,
		service:   service,
	})
	if err != nil {
		return nil, err
	}

	return &GrantServicePublication{
		hash:      h,
		directory: directory,
		service:   service,
	}, nil
}

func newGrantServicePublicationService(args *NewGrantServicePublicationServiceArgs) (*grantServicePublicationService, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant service publication args cannot be nil")
	}

	if !slices.Contains(ServiceProtocols, args.Protocol) {
		return nil, fmt.Errorf("invalid protocol %q. possible values: %s", args.Protocol, ServiceProtocols)
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	name, err := newServiceName(args.Name)
	if err != nil {
		return nil, err
	}

	return &grantServicePublicationService{
		peer:     p,
		name:     name,
		protocol: args.Protocol,
	}, nil
}

func newServiceName(s string) (serviceName, error) {
	if len(s) > serviceNameMaxLength {
		return "", fmt.Errorf("service name cannot be longer than %d characters", serviceNameMaxLength)
	}

	if !alphaNumericRegex.MatchString(s) {
		return serviceName(""), fmt.Errorf("not a valid service name")
	}

	return serviceName(s), nil
}

func newGrantServicePublicationDirectory(args *NewGrantServicePublicationDirectoryArgs) (*grantServicePublicationDirectory, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant service publication directory args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	return &grantServicePublicationDirectory{
		peer: p,
	}, nil
}
