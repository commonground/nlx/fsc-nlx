// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

// nolint:dupl // similar but too soon to abstract
package contract_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestCreateContractNoSignatures(t *testing.T) {
	testClock := testingutils.NewMockClock(time.Date(2124, 10, 1, 14, 10, 5, 0, time.UTC))
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContentArgs := &contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567890",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567891",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	}

	contractContent, err := contract.NewContent(contractContentArgs)
	assert.NoError(t, err)

	assert.NoError(t, err)
	assert.NotNil(t, contractContent)
	assert.Len(t, contractContent.Grants().ServiceConnectionGrants(), 1)

	s, ok := contractContent.Grants().ServiceConnectionGrants()[0].Service().(*contract.GrantServiceConnectionService)
	require.True(t, ok)

	assert.Equal(t, "parkeerrechten", s.Name())
	assert.Equal(t, "12345678901234567891", s.Peer().ID().Value())
	assert.Equal(t, "12345678901234567890", contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value())
	assert.Equal(t, "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae", contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value())

	bundle, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-chain.pem",
		"../../../testing/pki/org-nlx-test-b-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	trustedRootCert := bundle.RootCAs()

	peerCertificate, err := contract.NewPeerCertFromCertificate(testClock, trustedRootCert, bundle.Cert().Certificate)

	assert.NoError(t, err)

	peerCerts := contract.PeersCertificates{
		contract.PeerID("12345678901234567890"): &contract.PeerCertificates{
			contract.CertificateThumbprint(bundle.CertificateThumbprint()): peerCertificate,
		},
	}
	newContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: peerCerts,
	}

	testContract, err := contract.NewContract(newContractArgs)
	assert.NoError(t, err)

	assert.NotNil(t, testContract)
	assert.Len(t, testContract.SignaturesAccepted(), 0)
	assert.Len(t, testContract.SignaturesRejected(), 0)
	assert.Len(t, testContract.SignaturesRevoked(), 0)
}

func TestSignaturePeerIsNotOnContract(t *testing.T) {
	testClock := testingutils.NewMockClock(time.Date(2124, 10, 1, 14, 10, 5, 0, time.UTC))
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	bundle, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-chain.pem",
		"../../../testing/pki/org-nlx-test-b-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	trustedRootCert := bundle.RootCAs()

	peerCertificate, err := contract.NewPeerCertFromCertificate(testClock, trustedRootCert, bundle.Cert().Certificate)
	assert.NoError(t, err)

	peerCerts := contract.PeersCertificates{
		contract.PeerID("12345678901234567890"): &contract.PeerCertificates{
			contract.CertificateThumbprint(bundle.CertificateThumbprint()): peerCertificate,
		},
	}

	contractContentArgs := &contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567890",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567891",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	}

	contractContent, err := contract.NewContent(contractContentArgs)
	assert.NoError(t, err)

	acceptSig, err := contractContent.Accept(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	assert.Equal(t, contract.SignatureTypeAccept, acceptSig.Type())

	newContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: peerCerts,
		SignaturesAccepted: map[string]string{
			bundle.GetPeerInfo().SerialNumber: acceptSig.JWS(),
		},
	}

	_, err = contract.NewContract(newContractArgs)
	assert.ErrorContains(t, err, "could not create signatures: could not create signature: signature is from a peer that is not found in the content")
}

func TestContractWithAcceptSignature(t *testing.T) {
	testClock := testingutils.NewMockClock(time.Date(2124, 10, 1, 14, 10, 5, 0, time.UTC))
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	bundle, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-chain.pem",
		"../../../testing/pki/org-nlx-test-b-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	trustedRootCert := bundle.RootCAs()

	peerCertificate, err := contract.NewPeerCertFromCertificate(testClock, trustedRootCert, bundle.Cert().Certificate)
	assert.NoError(t, err)

	peerCerts := contract.PeersCertificates{
		contract.PeerID("12345678901234567890"): &contract.PeerCertificates{
			contract.CertificateThumbprint(bundle.CertificateThumbprint()): peerCertificate,
		},
	}

	contractContentArgs := &contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567891",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	}

	contractContent, err := contract.NewContent(contractContentArgs)
	assert.NoError(t, err)

	acceptSig, err := contractContent.Accept(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	assert.Equal(t, contract.SignatureTypeAccept, acceptSig.Type())

	newContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: peerCerts,
		SignaturesAccepted: map[string]string{
			bundle.GetPeerInfo().SerialNumber: acceptSig.JWS(),
		},
	}

	testContract, err := contract.NewContract(newContractArgs)
	assert.NoError(t, err)

	assert.Len(t, testContract.SignaturesAccepted(), 1)

	assert.Equal(t, contract.ContractStateProposed, testContract.State(testClock.Now()))
}

func TestAcceptedContract(t *testing.T) {
	testClock := testingutils.NewMockClock(time.Date(2124, 10, 1, 14, 10, 5, 0, time.UTC))
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	bundle, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-chain.pem",
		"../../../testing/pki/org-nlx-test-b-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	trustedRootCert := bundle.RootCAs()

	peerCertificate, err := contract.NewPeerCertFromCertificate(testClock, trustedRootCert, bundle.Cert().Certificate)
	assert.NoError(t, err)

	peerCerts := contract.PeersCertificates{
		contract.PeerID("00000000000000000002"): &contract.PeerCertificates{
			contract.CertificateThumbprint(bundle.CertificateThumbprint()): peerCertificate,
		},
	}

	contractContentArgs := &contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	}

	contractContent, err := contract.NewContent(contractContentArgs)
	assert.NoError(t, err)

	acceptSig, err := contractContent.Accept(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	assert.Equal(t, contract.SignatureTypeAccept, acceptSig.Type())

	newContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: peerCerts,
		SignaturesAccepted: map[string]string{
			bundle.GetPeerInfo().SerialNumber: acceptSig.JWS(),
		},
	}

	testContract, err := contract.NewContract(newContractArgs)
	assert.NoError(t, err)

	assert.Len(t, testContract.SignaturesAccepted(), 1)

	assert.Equal(t, contract.ContractStateValid, testContract.State(testClock.Now()))
}

func TestRevokedContract(t *testing.T) {
	testClock := testingutils.NewMockClock(time.Date(2124, 10, 1, 14, 10, 5, 0, time.UTC))
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	bundle, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-chain.pem",
		"../../../testing/pki/org-nlx-test-b-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	trustedRootCert := bundle.RootCAs()

	peerCertificate, err := contract.NewPeerCertFromCertificate(testClock, trustedRootCert, bundle.Cert().Certificate)
	assert.NoError(t, err)

	peerCerts := contract.PeersCertificates{
		contract.PeerID("00000000000000000002"): &contract.PeerCertificates{
			contract.CertificateThumbprint(bundle.CertificateThumbprint()): peerCertificate,
		},
	}

	contractContentArgs := &contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	}

	contractContent, err := contract.NewContent(contractContentArgs)
	assert.NoError(t, err)

	acceptSig, err := contractContent.Accept(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	assert.Equal(t, contract.SignatureTypeAccept, acceptSig.Type())

	revokeSig, err := contractContent.Revoke(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	assert.Equal(t, contract.SignatureTypeRevoke, revokeSig.Type())

	newContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: peerCerts,
		SignaturesAccepted: map[string]string{
			bundle.GetPeerInfo().SerialNumber: acceptSig.JWS(),
		},
		SignaturesRevoked: map[string]string{
			bundle.GetPeerInfo().SerialNumber: revokeSig.JWS(),
		},
	}

	testContract, err := contract.NewContract(newContractArgs)
	assert.NoError(t, err)

	assert.Len(t, testContract.SignaturesAccepted(), 1)

	assert.Equal(t, contract.ContractStateRevoked, testContract.State(testClock.Now()))
}

//nolint:funlen // test scenario requires a lot of setup
func TestAcceptSignatureOnRevokedContract(t *testing.T) {
	testClock := testingutils.NewMockClock(time.Date(2124, 10, 1, 14, 10, 5, 0, time.UTC))
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	bundle, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-chain.pem",
		"../../../testing/pki/org-nlx-test-b-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	trustedRootCert := bundle.RootCAs()

	peerCertificate, err := contract.NewPeerCertFromCertificate(testClock, trustedRootCert, bundle.Cert().Certificate)
	assert.NoError(t, err)

	peerCerts := contract.PeersCertificates{
		contract.PeerID("00000000000000000002"): &contract.PeerCertificates{
			contract.CertificateThumbprint(bundle.CertificateThumbprint()): peerCertificate,
		},
	}

	contractContentArgs := &contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	}

	contractContent, err := contract.NewContent(contractContentArgs)
	assert.NoError(t, err)

	acceptSig, err := contractContent.Accept(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	assert.Equal(t, contract.SignatureTypeAccept, acceptSig.Type())

	revokeSig, err := contractContent.Revoke(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	assert.Equal(t, contract.SignatureTypeRevoke, revokeSig.Type())

	newContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: peerCerts,
		SignaturesAccepted: map[string]string{
			bundle.GetPeerInfo().SerialNumber: acceptSig.JWS(),
		},
		SignaturesRevoked: map[string]string{
			bundle.GetPeerInfo().SerialNumber: revokeSig.JWS(),
		},
	}

	testContract, err := contract.NewContract(newContractArgs)
	assert.NoError(t, err)

	assert.Len(t, testContract.SignaturesAccepted(), 1)

	assert.Equal(t, contract.ContractStateRevoked, testContract.State(testClock.Now()))

	// place a new accept signature on the contract should not change state
	renewSig, err := testContract.Content().Accept(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	renewContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: peerCerts,
		SignaturesRevoked: map[string]string{
			bundle.GetPeerInfo().SerialNumber: revokeSig.JWS(),
		},
		SignaturesAccepted: map[string]string{
			bundle.GetPeerInfo().SerialNumber: renewSig.JWS(),
		},
	}

	renewedContract, err := contract.NewContract(renewContractArgs)
	assert.NoError(t, err)

	assert.Equal(t, contract.ContractStateRevoked, renewedContract.State(testClock.Now()))
}

func TestContractStateWithExpiredCertificate(t *testing.T) {
	testClock := testingutils.NewMockClock(time.Date(2124, 10, 1, 14, 10, 5, 0, time.UTC))
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	bundle, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-lower-expiration-chain.pem",
		"../../../testing/pki/org-nlx-test-b-lower-expiration-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	trustedRootCert := bundle.RootCAs()

	peerCertificate, err := contract.NewPeerCertFromCertificate(testClock, trustedRootCert, bundle.Cert().Certificate)
	assert.NoError(t, err)

	peerCerts := contract.PeersCertificates{
		contract.PeerID("00000000000000000002"): &contract.PeerCertificates{
			contract.CertificateThumbprint(bundle.CertificateThumbprint()): peerCertificate,
		},
	}

	contractContentArgs := &contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().AddDate(1, 2, 0).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	}

	contractContent, err := contract.NewContent(contractContentArgs)
	assert.NoError(t, err)

	acceptSig, err := contractContent.Accept(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	testClock.UpdateTime(time.Date(2125, 11, 6, 14, 10, 5, 0, time.UTC)) // advance time so cert used for accept signature is expired

	assert.Equal(t, contract.SignatureTypeAccept, acceptSig.Type())

	assert.NoError(t, err)

	newContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: peerCerts,
		SignaturesAccepted: map[string]string{
			bundle.GetPeerInfo().SerialNumber: acceptSig.JWS(),
		},
	}

	testContract, err := contract.NewContract(newContractArgs)
	assert.NoError(t, err)

	assert.Len(t, testContract.SignaturesAccepted(), 1)

	assert.Equal(t, contract.ContractStateExpired, testContract.State(testClock.Now()))
}

//nolint:funlen // a lot of test setup for this scenario
func TestContractStateSignWithRenewedCertificate(t *testing.T) {
	testClock := testingutils.NewMockClock(time.Date(2124, 10, 1, 14, 10, 5, 0, time.UTC))
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	bundleExpired, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-lower-expiration-chain.pem",
		"../../../testing/pki/org-nlx-test-b-lower-expiration-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	trustedRootCert := bundleExpired.RootCAs()

	peerCertificate, err := contract.NewPeerCertFromCertificate(testClock, trustedRootCert, bundleExpired.Cert().Certificate)
	assert.NoError(t, err)

	peerCerts := contract.PeersCertificates{
		contract.PeerID("00000000000000000002"): &contract.PeerCertificates{
			contract.CertificateThumbprint(bundleExpired.CertificateThumbprint()): peerCertificate,
		},
	}

	contractContentArgs := &contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().AddDate(1, 2, 0).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	}

	contractContent, err := contract.NewContent(contractContentArgs)
	assert.NoError(t, err)

	acceptSig, err := contractContent.Accept(trustedRootCert, bundleExpired.Cert(), testClock.Now())
	assert.NoError(t, err)

	testClock.UpdateTime(time.Date(2125, 11, 6, 14, 10, 5, 0, time.UTC)) // advance time so cert used for accept signature is expired

	assert.Equal(t, contract.SignatureTypeAccept, acceptSig.Type())

	assert.NoError(t, err)

	newContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: peerCerts,
		SignaturesAccepted: map[string]string{
			bundleExpired.GetPeerInfo().SerialNumber: acceptSig.JWS(),
		},
	}

	testContract, err := contract.NewContract(newContractArgs)
	assert.NoError(t, err)

	assert.Len(t, testContract.SignaturesAccepted(), 1)

	assert.Equal(t, contract.ContractStateExpired, testContract.State(testClock.Now()))

	//place a new Accept signature with a new valid certificate
	bundle, err := tls.NewBundleFromFiles(
		"../../../testing/pki/org-nlx-test-b-chain.pem",
		"../../../testing/pki/org-nlx-test-b-key.pem",
		"../../../testing/pki/ca-root.pem",
	)
	assert.NoError(t, err)

	renewedPeerCertificate, err := contract.NewPeerCertFromCertificate(testClock, trustedRootCert, bundle.Cert().Certificate)
	assert.NoError(t, err)

	renewedPeerCerts := contract.PeersCertificates{
		contract.PeerID("00000000000000000002"): &contract.PeerCertificates{
			contract.CertificateThumbprint(bundle.CertificateThumbprint()): renewedPeerCertificate,
		},
	}

	renewSignature, err := testContract.Content().Accept(trustedRootCert, bundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	renewContractArgs := &contract.NewContractArgs{
		Content:    contractContentArgs,
		PeersCerts: renewedPeerCerts,
		SignaturesAccepted: map[string]string{
			bundleExpired.GetPeerInfo().SerialNumber: renewSignature.JWS(),
		},
	}

	renewedContract, err := contract.NewContract(renewContractArgs)
	assert.NoError(t, err)

	assert.Equal(t, contract.ContractStateValid, renewedContract.State(testClock.Now()))
}
