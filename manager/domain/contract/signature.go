// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package contract

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"golang.org/x/exp/slices"
	"gopkg.in/square/go-jose.v2"
)

const (
	jwsX5tHeader = "x5t#S256"
)

type SignatureType string

const (
	SignatureTypeAccept SignatureType = "accept"
	SignatureTypeReject SignatureType = "reject"
	SignatureTypeRevoke SignatureType = "revoke"
)

var signatureTypes = []SignatureType{SignatureTypeAccept, SignatureTypeReject, SignatureTypeRevoke}

type signatures map[PeerID]*Signature
type signaturesAccepted signatures
type signaturesRejected signatures
type signaturesRevoked signatures

type Signature struct {
	contentHash           *ContentHash
	peer                  *Peer
	sigType               SignatureType
	certificateThumbprint CertificateThumbprint
	jws                   string
	cert                  *PeerCertificate
	signedAt              time.Time
}

type SignaturePayload struct {
	ContentHash string `json:"contentHash"`
	SigType     string `json:"type"`
	SignedAt    int64  `json:"signedAt"`
}

type newSignatureArgs struct {
	JWS string
}

type newSignaturesArgs struct {
	Sigs       map[string]*newSignatureArgs
	Content    *Content
	PeersCerts PeersCertificates
}

func newSignaturesAccepted(args *newSignaturesArgs) (signaturesAccepted, error) {
	models, err := newSignatures(SignatureTypeAccept, args)
	if err != nil {
		return nil, fmt.Errorf("could not create signatures: %v", err)
	}

	return signaturesAccepted(models), nil
}

func newSignaturesRejected(args *newSignaturesArgs) (signaturesRejected, error) {
	models, err := newSignatures(SignatureTypeReject, args)
	if err != nil {
		return nil, fmt.Errorf("could not create signatures")
	}

	return signaturesRejected(models), nil
}

func newSignaturesRevoked(args *newSignaturesArgs) (signaturesRevoked, error) {
	models, err := newSignatures(SignatureTypeRevoke, args)
	if err != nil {
		return nil, fmt.Errorf("could not create signatures")
	}

	return signaturesRevoked(models), nil
}

func newSignatures(sigType SignatureType, args *newSignaturesArgs) (signatures, error) {
	models := make(signatures, len(args.Sigs))

	for peer, signature := range args.Sigs {
		p, err := NewPeerID(peer)
		if err != nil {
			return nil, fmt.Errorf("invalid peer id in signatures: %v", err)
		}

		sig, err := NewSignature(&NewSignatureArgs{
			SigType:    sigType,
			Signature:  signature.JWS,
			Content:    args.Content,
			PeersCerts: args.PeersCerts,
		})
		if err != nil {
			return nil, fmt.Errorf("could not create signature: %v", err)
		}

		models[p] = sig
	}

	return models, nil
}

type NewSignatureArgs struct {
	SigType    SignatureType
	Signature  string
	Content    *Content
	PeersCerts PeersCertificates
}

func NewSignature(args *NewSignatureArgs) (*Signature, error) {
	jws, err := jose.ParseSigned(args.Signature)
	if err != nil {
		return nil, fmt.Errorf("could not parse signature: %v", err)
	}

	err = verifySignatureAlg(jws.Signatures)
	if err != nil {
		return nil, err
	}

	if len(jws.Signatures) != 1 {
		return nil, fmt.Errorf("exactly one signature is required, found either zero or multiple signatures in jws token")
	}

	header, ok := jws.Signatures[0].Header.ExtraHeaders[jwsX5tHeader]
	if !ok {
		return nil, fmt.Errorf("could not find required header in jws signature: %s", jwsX5tHeader)
	}

	pubKey, ok := header.(string)
	if !ok {
		return nil, fmt.Errorf("certificate thumbprint must be of type string")
	}

	certificateThumbprint, err := NewCertificateThumbprint(pubKey)
	if err != nil {
		return nil, fmt.Errorf("invalid certificate thumbprint in jws header: %v", err)
	}

	peerCert, err := args.PeersCerts.GetPeerCertificate(certificateThumbprint)
	if err != nil {
		return nil, errors.Wrap(err, "could not find certificate for certificate thumbprint in jws signature")
	}

	if !args.Content.ContainsPeer(peerCert.Peer().ID()) {
		return nil, fmt.Errorf("signature is from a peer that is not found in the content")
	}

	jsonPayload, err := jws.Verify(peerCert.PublicKey())
	if err != nil {
		return nil, errors.Wrap(err, "could not verify jws token in signature")
	}

	payload := &SignaturePayload{}

	err = json.Unmarshal(jsonPayload, payload)
	if err != nil {
		return nil, fmt.Errorf("could not parse jws signature: %v", err)
	}

	payloadSigType, err := newSignatureType(payload.SigType)
	if err != nil {
		return nil, fmt.Errorf("invalid signature type in jws signature payload: %v", err)
	}

	if payloadSigType != args.SigType {
		return nil, ErrorSignatureMustBeOfTypeAccepted(payloadSigType.String())
	}

	if payload.SignedAt == 0 {
		return nil, fmt.Errorf("invalid signed at datetime for signature: %d", payload.SignedAt)
	}

	signedAt := time.Unix(payload.SignedAt, 0)

	if signedAt.IsZero() || signedAt.Before(time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC)) {
		return nil, fmt.Errorf("invalid signed at datetime for signature: %d", payload.SignedAt)
	}

	hash, err := NewContentHashFromString(payload.ContentHash)
	if err != nil {
		return nil, fmt.Errorf("invalid content hash in jws signature payload: %v", err)
	}

	if !args.Content.Hash().isEqual(hash) {
		return nil, ErrorSignatureContractContentHashDoesNotMatchTheContract(hash.String(), args.Content.Hash().String())
	}

	return &Signature{
		contentHash:           args.Content.Hash(),
		peer:                  peerCert.Peer(),
		sigType:               payloadSigType,
		certificateThumbprint: certificateThumbprint,
		jws:                   args.Signature,
		cert:                  peerCert,
		signedAt:              signedAt,
	}, nil
}

func newSignatureType(t string) (SignatureType, error) {
	if !slices.Contains(signatureTypes, SignatureType(t)) {
		return "", fmt.Errorf("invalid signature type: %s", t)
	}

	return SignatureType(t), nil
}

func (sigs signaturesAccepted) contains(p PeerID) bool {
	return signatures(sigs).contains(p)
}

func (sigs signaturesRejected) contains(p PeerID) bool {
	return signatures(sigs).contains(p)
}

func (sigs signaturesRevoked) contains(p PeerID) bool {
	return signatures(sigs).contains(p)
}

func (sigs signatures) contains(p PeerID) bool {
	_, ok := sigs[p]

	return ok
}

func (s *Signature) Type() SignatureType {
	return s.sigType
}

func (s *Signature) JWS() string {
	return s.jws
}

func (s *Signature) CertificateThumbprint() string {
	return s.certificateThumbprint.Value()
}

func (s *Signature) ContentHash() *ContentHash {
	return s.contentHash
}

func (s *Signature) Peer() *Peer {
	return s.peer
}

func (s *Signature) PeerCert() *PeerCertificate {
	return s.cert
}

func (s *Signature) SignedAt() time.Time {
	return s.signedAt
}

func (s *Signature) CertificateExpired(now time.Time) bool {
	return now.After(s.cert.cert.NotAfter)
}

func (s *Signature) CertificateNotActive(now time.Time) bool {
	return now.Before(s.cert.cert.NotBefore)
}

func (s *SignatureType) String() string {
	return string(*s)
}

func (sigs signaturesAccepted) Value() map[string]string {
	return signatures(sigs).Value()
}

func (sigs signaturesRejected) Value() map[string]string {
	return signatures(sigs).Value()
}

func (sigs signaturesRevoked) Value() map[string]string {
	return signatures(sigs).Value()
}

func (sigs signatures) Value() map[string]string {
	list := make(map[string]string, len(sigs))
	for i, sig := range sigs {
		list[i.Value()] = sig.JWS()
	}

	return list
}

func verifySignatureAlg(signatures []jose.Signature) error {
	for i := range signatures {
		alg := jose.SignatureAlgorithm(signatures[i].Header.Algorithm)
		switch alg {
		case jose.RS512:
			return nil
		case jose.RS384:
			return nil
		case jose.RS256:
			return nil
		case jose.ES512:
			return nil
		case jose.ES384:
			return nil
		case jose.ES256:
			return nil
		default:
			return ErrorSignatureUnknownAlgorithm(string(alg))
		}
	}

	return ErrorSignatureUnknownAlgorithm("UNKNOWN")
}
