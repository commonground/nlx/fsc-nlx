// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract

import (
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
)

type FailedDistributionAction string

const (
	FailedDistributionActionSubmitContract        FailedDistributionAction = "submit_contract"
	FailedDistributionActionSubmitAcceptSignature FailedDistributionAction = "submit_accept_signature"
	FailedDistributionActionSubmitRevokeSignature FailedDistributionAction = "submit_revoke_signature"
	FailedDistributionActionSubmitRejectSignature FailedDistributionAction = "submit_reject_signature"
)

var supportedActions = []string{
	string(FailedDistributionActionSubmitContract),
	string(FailedDistributionActionSubmitAcceptSignature),
	string(FailedDistributionActionSubmitRevokeSignature),
	string(FailedDistributionActionSubmitRejectSignature),
}

type FailedDistribution struct {
	contentHash   *ContentHash
	peerID        PeerID
	action        FailedDistributionAction
	signature     string
	attempts      int32
	nextAttemptAt *time.Time
	lastAttemptAt time.Time
	reason        string
}

type NewFailedDistributionArgs struct {
	ContentHash   *ContentHash
	PeerID        string
	Action        string
	Signature     string
	Attempts      int32
	NextAttemptAt *time.Time
	LastAttemptAt time.Time
	Reason        string
}

func NewFailedDistribution(args *NewFailedDistributionArgs) (*FailedDistribution, error) {
	if args.ContentHash == nil {
		return nil, errors.New("missing content hash")
	}

	if args.PeerID == "" {
		return nil, errors.New("missing peer id")
	}

	peerID, err := NewPeerID(args.PeerID)
	if err != nil {
		return nil, fmt.Errorf("invalid peer id: %v", err)
	}

	action, err := actionArgsToDistributedAction(args.Action)
	if err != nil {
		return nil, err
	}

	result := &FailedDistribution{
		contentHash:   args.ContentHash,
		peerID:        peerID,
		action:        action,
		signature:     args.Signature,
		attempts:      args.Attempts,
		nextAttemptAt: args.NextAttemptAt,
		lastAttemptAt: args.LastAttemptAt,
		reason:        args.Reason,
	}

	return result, nil
}

func actionArgsToDistributedAction(arg string) (FailedDistributionAction, error) {
	switch arg {
	case "submit_contract":
		return FailedDistributionActionSubmitContract, nil
	case "submit_accept_signature":
		return FailedDistributionActionSubmitAcceptSignature, nil
	case "submit_revoke_signature":
		return FailedDistributionActionSubmitRevokeSignature, nil
	case "submit_reject_signature":
		return FailedDistributionActionSubmitRejectSignature, nil
	default:
		return "", fmt.Errorf("unsupported action %q, must be one of %s", arg, strings.Join(supportedActions, ", "))
	}
}

func (d *FailedDistribution) UpdateWithNewAttempt(attemptedAt time.Time, errorMessage string, nextAttemptAt *time.Time) {
	d.lastAttemptAt = attemptedAt
	d.attempts += 1
	d.reason = errorMessage
	d.nextAttemptAt = nextAttemptAt
}

func (d *FailedDistribution) ClearNextRetry() {
	d.nextAttemptAt = nil
}

func (d *FailedDistribution) NextAttemptAt() *time.Time {
	return d.nextAttemptAt
}

func (d *FailedDistribution) Reason() string {
	return d.reason
}

func (d *FailedDistribution) Attempts() int32 {
	return d.attempts
}

func (d *FailedDistribution) LastAttemptAt() time.Time {
	return d.lastAttemptAt
}

func (d *FailedDistribution) PeerID() PeerID {
	return d.peerID
}

func (d *FailedDistribution) ContentHash() *ContentHash {
	return d.contentHash
}

func (d *FailedDistribution) Action() FailedDistributionAction {
	return d.action
}

func (d *FailedDistribution) Signature() string {
	return d.signature
}
