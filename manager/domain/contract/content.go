// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"fmt"
	"regexp"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
)

var (
	alphaNumericRegex = regexp.MustCompile(`^[a-zA-Z0-9-._]{1,100}$`)
	groupIDRegex      = regexp.MustCompile(`^[a-zA-Z0-9./_-]{1,100}$`)
)

type contentIV uuid.UUID

type NewContentArgs struct {
	Validity      *NewValidityArgs
	GroupID       string
	Grants        []interface{}
	HashAlgorithm HashAlg
	IV            []byte
	CreatedAt     time.Time
	Clock         clock.Clock
}

type NewValidityArgs struct {
	NotBefore time.Time
	NotAfter  time.Time
}

type GroupID string

type Content struct {
	hash      *ContentHash
	validity  *validity
	groupID   GroupID
	grants    grants
	iv        contentIV
	createdAt time.Time
	clock     clock.Clock
}

type validity struct {
	notBefore time.Time
	notAfter  time.Time
}

func NewContent(args *NewContentArgs) (*Content, error) {
	if args == nil {
		return nil, fmt.Errorf("new content args cannot be nil")
	}

	if args.Clock == nil {
		return nil, fmt.Errorf("clock cannot be nil")
	}

	gID, err := NewGroupID(args.GroupID)
	if err != nil {
		return nil, err
	}

	cIV, err := newContentIV(args.IV)
	if err != nil {
		return nil, err
	}

	modelGrants, err := newGrants(gID, cIV, args.HashAlgorithm, args.Grants)
	if err != nil {
		return nil, err
	}

	v, err := newValidity(args.Validity)
	if err != nil {
		return nil, err
	}

	if args.CreatedAt.IsZero() {
		return nil, fmt.Errorf("createdAt is required")
	}

	if args.CreatedAt.Nanosecond() != 0 {
		return nil, fmt.Errorf("createdAt must be rounded on seconds")
	}

	if args.CreatedAt.After(time.Now()) {
		return nil, fmt.Errorf("createdAt cannot be in the future")
	}

	hash, err := newContentHash(&Content{
		iv:        cIV,
		groupID:   gID,
		validity:  v,
		grants:    modelGrants,
		createdAt: args.CreatedAt,
	}, args.HashAlgorithm)
	if err != nil {
		return nil, err
	}

	return &Content{
		hash:      hash,
		iv:        cIV,
		groupID:   gID,
		validity:  v,
		grants:    modelGrants,
		createdAt: args.CreatedAt,
		clock:     args.Clock,
	}, nil
}

func newValidity(args *NewValidityArgs) (*validity, error) {
	if args == nil {
		return nil, fmt.Errorf("new validity args cannot be nil")
	}

	if args.NotBefore.Nanosecond() != 0 {
		return nil, fmt.Errorf("NotBefore must be rounded on seconds")
	}

	if args.NotAfter.Nanosecond() != 0 {
		return nil, fmt.Errorf("NotAfter must be rounded on seconds")
	}

	if args.NotBefore.IsZero() {
		return nil, fmt.Errorf("NotBefore cannot be zero")
	}

	if args.NotAfter.IsZero() {
		return nil, fmt.Errorf("NotAfter cannot be zero")
	}

	if args.NotBefore.After(args.NotAfter) {
		return nil, fmt.Errorf("NotBefore cannot be after NotAfter")
	}

	return &validity{
		notBefore: args.NotBefore,
		notAfter:  args.NotAfter,
	}, nil
}

func newContentIV(bytes []byte) (contentIV, error) {
	iv, err := uuid.FromBytes(bytes)
	if err != nil {
		return contentIV{}, errors.Wrap(err, "could not create content IV")
	}

	return contentIV(iv), nil
}

func NewGroupID(g string) (GroupID, error) {
	if g == "" {
		return "", fmt.Errorf("group ID cannot be empty")
	}

	if !groupIDRegex.MatchString(g) {
		return "", fmt.Errorf("not a valid group ID")
	}

	return GroupID(g), nil
}

func (g GroupID) String() string {
	return string(g)
}

func (g GroupID) IsEqual(s string) bool {
	return g == GroupID(s)
}

func (c *Content) Hash() *ContentHash {
	return c.hash
}

func (c *Content) NotBefore() time.Time {
	return c.validity.notBefore
}

func (c *Content) NotAfter() time.Time {
	return c.validity.notAfter
}

func (c *Content) Grants() grants {
	return c.grants
}

func (c *Content) IV() contentIV {
	return c.iv
}

func (c *Content) GroupID() string {
	return string(c.groupID)
}

func (c *Content) CreatedAt() time.Time {
	return c.createdAt
}

func (c *Content) PeersIDs() PeersIDs {
	peers := make(PeersIDs)

	for _, grant := range c.Grants() {
		switch g := grant.(type) {
		case *GrantServicePublication:
			peers[g.directory.peer.id] = true
			peers[g.service.peer.id] = true
		case *GrantServiceConnection:
			switch s := g.service.(type) {
			case *GrantServiceConnectionService:
				peers[s.Peer().ID()] = true
			case *GrantServiceConnectionDelegatedService:
				peers[s.Peer().ID()] = true
				peers[s.PublicationDelegator().ID()] = true
			}

			peers[g.outway.peer.id] = true
		case *GrantDelegatedServiceConnection:
			switch s := g.service.(type) {
			case *GrantDelegatedServiceConnectionService:
				peers[s.Peer().ID()] = true
			case *GrantDelegatedServiceConnectionDelegatedService:
				peers[s.Peer().ID()] = true
				peers[s.PublicationDelegator().ID()] = true
			}

			peers[g.delegator.peer.id] = true
			peers[g.outway.peer.id] = true
		case *GrantDelegatedServicePublication:
			peers[g.directory.peer.id] = true
			peers[g.service.peer.id] = true
			peers[g.delegator.peer.id] = true
		}
	}

	return peers
}

func (c *Content) ContainsPeer(id PeerID) bool {
	_, ok := c.PeersIDs()[id]

	return ok
}

func (iv contentIV) Bytes() []byte {
	return iv[:]
}

func (iv contentIV) Value() uuid.UUID {
	return uuid.UUID(iv)
}

func (iv contentIV) String() string {
	uid := uuid.UUID(iv)
	return uid.String()
}
