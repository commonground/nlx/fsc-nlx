// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"fmt"
)

type grants []interface{}

// type of single grant
type GrantType int32

const (
	GrantTypeUnspecified                 GrantType = 0
	GrantTypeServicePublication          GrantType = 1
	GrantTypeServiceConnection           GrantType = 2
	GrantTypeDelegatedServiceConnection  GrantType = 3
	GrantTypeDelegatedServicePublication GrantType = 4
)

const (
	minAmountOfGrants = 1
	maxAmountOfGrants = 100
)

func NewGrantTypeFromString(g string) (GrantType, error) {
	if g == "" {
		return GrantTypeUnspecified, fmt.Errorf("grant type cannot be empty")
	}

	switch g {
	case "servicePublication":
		return GrantTypeServicePublication, nil
	case "serviceConnection":
		return GrantTypeServiceConnection, nil
	case "delegatedServiceConnection":
		return GrantTypeDelegatedServiceConnection, nil
	case "delegatedServicePublication":
		return GrantTypeDelegatedServicePublication, nil
	default:
		return GrantTypeUnspecified, fmt.Errorf("invalid grant type: %s", g)
	}
}

func newGrants(gID GroupID, cID contentIV, alg HashAlg, g []interface{}) (grants, error) {
	if g == nil {
		return nil, fmt.Errorf("grants cannot be nil")
	}

	if len(g) < minAmountOfGrants {
		return nil, fmt.Errorf("amount of grants must be at least %d", minAmountOfGrants)
	}

	if len(g) > maxAmountOfGrants {
		return nil, fmt.Errorf("amount of grants cannot be greater than %d grants", maxAmountOfGrants)
	}

	modelGrants := make([]interface{}, 0)

	gTypes := make(map[GrantType]uint, 0)

	for _, grant := range g {
		t, m, err := newGrant(gID, cID, alg, grant)
		if err != nil {
			return nil, err
		}

		modelGrants = append(modelGrants, m)
		gTypes[t] += 1
	}

	err := validateGrantTypeCombinations(gTypes)
	if err != nil {
		return nil, err
	}

	return modelGrants, nil
}

func newGrant(gID GroupID, cID contentIV, alg HashAlg, g interface{}) (GrantType, interface{}, error) {
	var (
		t   GrantType
		m   interface{}
		err error
	)

	switch v := g.(type) {
	case *NewGrantServicePublicationArgs:
		t = GrantTypeServicePublication
		m, err = newGrantServicePublication(gID, cID, alg, v)
	case *NewGrantServiceConnectionArgs:
		t = GrantTypeServiceConnection
		m, err = newGrantServiceConnection(gID, cID, alg, v)
	case *NewGrantDelegatedServiceConnectionArgs:
		t = GrantTypeDelegatedServiceConnection
		m, err = newGrantDelegatedServiceConnection(gID, cID, alg, v)
	case *NewGrantDelegatedServicePublicationArgs:
		t = GrantTypeDelegatedServicePublication
		m, err = newGrantDelegatedServicePublication(gID, cID, alg, v)
	default:
		return GrantTypeUnspecified, nil, fmt.Errorf("unknown grant type: %T", v)
	}

	return t, m, err
}

func validateGrantTypeCombinations(t map[GrantType]uint) error {
	servicePublicationGrants := t[GrantTypeServicePublication] > 0
	delegatedServicePublicationGrants := t[GrantTypeDelegatedServicePublication] > 0
	serviceConnectionGrants := t[GrantTypeServiceConnection] > 0
	delegatedServiceConnectionGrants := t[GrantTypeDelegatedServiceConnection] > 0

	if servicePublicationGrants && (delegatedServicePublicationGrants || serviceConnectionGrants || delegatedServiceConnectionGrants) {
		return ErrorServicePublicationGrantCanNotBeMixedWithOtherGrants()
	}

	return nil
}

func (g grants) Types() map[GrantType]bool {
	t := make(map[GrantType]bool)

	for _, grant := range g {
		switch grant.(type) {
		case *GrantServicePublication:
			t[GrantTypeServicePublication] = true
		case *GrantServiceConnection:
			t[GrantTypeServiceConnection] = true
		case *GrantDelegatedServiceConnection:
			t[GrantTypeDelegatedServiceConnection] = true
		case *GrantDelegatedServicePublication:
			t[GrantTypeDelegatedServicePublication] = true
		}
	}

	return t
}

func (g grants) ServiceConnectionGrants() []*GrantServiceConnection {
	result := []*GrantServiceConnection{}

	for _, grant := range g {
		serviceConnectionGrant, ok := grant.(*GrantServiceConnection)
		if !ok {
			continue
		}

		result = append(result, serviceConnectionGrant)
	}

	return result
}

func (g grants) DelegatedServiceConnectionGrants() []*GrantDelegatedServiceConnection {
	result := []*GrantDelegatedServiceConnection{}

	for _, grant := range g {
		g, ok := grant.(*GrantDelegatedServiceConnection)
		if !ok {
			continue
		}

		result = append(result, g)
	}

	return result
}

func (g grants) ServicePublicationGrants() []*GrantServicePublication {
	result := []*GrantServicePublication{}

	for _, grant := range g {
		serviceServicePublication, ok := grant.(*GrantServicePublication)
		if !ok {
			continue
		}

		result = append(result, serviceServicePublication)
	}

	return result
}

func (g grants) DelegatedServicePublicationGrants() []*GrantDelegatedServicePublication {
	result := []*GrantDelegatedServicePublication{}

	for _, grant := range g {
		delegatedServiceServicePublication, ok := grant.(*GrantDelegatedServicePublication)
		if !ok {
			continue
		}

		result = append(result, delegatedServiceServicePublication)
	}

	return result
}
