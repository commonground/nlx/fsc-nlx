// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import "fmt"

type NewGrantServiceConnectionArgs struct {
	Outway  *NewGrantServiceConnectionOutwayArgs
	Service interface{}
}

type NewGrantServiceConnectionServiceArgs struct {
	Peer *NewPeerArgs
	Name string
}

type NewGrantServiceConnectionDelegatedServiceArgs struct {
	Peer                 *NewPeerArgs
	Name                 string
	PublicationDelegator *NewPeerArgs
}

type NewGrantServiceConnectionOutwayArgs struct {
	Peer                *NewPeerArgs
	PublicKeyThumbprint string
}

type GrantServiceConnection struct {
	hash    *grantServiceConnectionHash
	outway  *grantServiceConnectionOutway
	service interface{}
}

type grantServiceConnectionOutway struct {
	peer                *Peer
	publicKeyThumbprint *PublicKeyThumbprint
}

type GrantServiceConnectionService struct {
	peer *Peer
	name serviceName
}

type GrantServiceConnectionDelegatedService struct {
	peer                 *Peer
	name                 serviceName
	publicationDelegator *Peer
}

func (g *GrantServiceConnection) Outway() *grantServiceConnectionOutway {
	return g.outway
}

func (g *GrantServiceConnection) Hash() *grantServiceConnectionHash {
	return g.hash
}

func (g *GrantServiceConnection) Service() interface{} {
	return g.service
}

func (s *GrantServiceConnectionService) Peer() *Peer {
	return s.peer
}

func (s *GrantServiceConnectionService) Name() string {
	return string(s.name)
}

func (c *grantServiceConnectionOutway) Peer() *Peer {
	return c.peer
}

func (s *GrantServiceConnectionDelegatedService) Peer() *Peer {
	return s.peer
}

func (s *GrantServiceConnectionDelegatedService) Name() string {
	return string(s.name)
}

func (s *GrantServiceConnectionDelegatedService) PublicationDelegator() *Peer {
	return s.publicationDelegator
}

func (c *grantServiceConnectionOutway) PublicKeyThumbprint() *PublicKeyThumbprint {
	return c.publicKeyThumbprint
}

// nolint:dupl // grants are definitively similar but still have distinctive properties
func newGrantServiceConnection(gID GroupID, cID contentIV, alg HashAlg, args *NewGrantServiceConnectionArgs) (*GrantServiceConnection, error) {
	if args == nil {
		return nil, fmt.Errorf("new service connection args cannot be nil")
	}

	outway, err := newGrantServiceConnectionOutway(args.Outway)
	if err != nil {
		return nil, err
	}

	var service any
	switch s := args.Service.(type) {
	case *NewGrantServiceConnectionServiceArgs:
		service, err = newGrantServiceConnectionService(s)
		if err != nil {
			return nil, err
		}
	case *NewGrantServiceConnectionDelegatedServiceArgs:
		service, err = newGrantServiceConnectionDelegatedService(s)
		if err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("unknown service type: %T", args.Service)
	}

	h, err := newGrantServiceConnectionHash(gID, cID, alg, &GrantServiceConnection{
		outway:  outway,
		service: service,
	})
	if err != nil {
		return nil, err
	}

	return &GrantServiceConnection{
		hash:    h,
		outway:  outway,
		service: service,
	}, nil
}

func newGrantServiceConnectionService(args *NewGrantServiceConnectionServiceArgs) (*GrantServiceConnectionService, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant service connection service args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	name, err := newServiceName(args.Name)
	if err != nil {
		return nil, err
	}

	return &GrantServiceConnectionService{
		peer: p,
		name: name,
	}, nil
}

func newGrantServiceConnectionDelegatedService(args *NewGrantServiceConnectionDelegatedServiceArgs) (*GrantServiceConnectionDelegatedService, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant service connection delegated service args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	dp, err := NewPeer(args.PublicationDelegator)
	if err != nil {
		return nil, err
	}

	name, err := newServiceName(args.Name)
	if err != nil {
		return nil, err
	}

	return &GrantServiceConnectionDelegatedService{
		peer:                 p,
		name:                 name,
		publicationDelegator: dp,
	}, nil
}

func newGrantServiceConnectionOutway(args *NewGrantServiceConnectionOutwayArgs) (*grantServiceConnectionOutway, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant service connection outway args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	pubKeyThumbprint, err := NewPublicKeyThumbprint(args.PublicKeyThumbprint)
	if err != nil {
		return nil, err
	}

	return &grantServiceConnectionOutway{
		peer:                p,
		publicKeyThumbprint: pubKeyThumbprint,
	}, nil
}
