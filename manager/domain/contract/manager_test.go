// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func TestNewManagerAddress(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		args    string
		wantErr error
	}{
		"empty_manager_address": {
			args:    "",
			wantErr: errors.New("manager address is required"),
		},
		"invalid_prefix_manager_address": {
			args:    "invalid:manager-address",
			wantErr: errors.New("missing https prefix in manager address \"invalid:manager-address\""),
		},
		"invalid_prefix_http_manager_address": {
			args:    "http://manager-address.com",
			wantErr: errors.New("missing https prefix in manager address \"http://manager-address.com\""),
		},
		"invalid_url_manager_address": {
			args:    "https://manager address.com",
			wantErr: errors.New("unable to parse manager address \"https://manager address.com\" as URL: parse \"https://manager address.com\": invalid character \" \" in host name"),
		},
		"invalid_url_missing_port_manager_address": {
			args:    "https://manager-address.com",
			wantErr: errors.New("missing port in manager address \"https://manager-address.com\""),
		},
		"invalid_url_invalid_port_manager_address": {
			args:    "https://manager-address.com:1234",
			wantErr: errors.New("invalid port in manager address \"https://manager-address.com:1234\". Must be 8443 or 443"),
		},
		"happy_flow_8443": {
			args: "https://manager-address.com:8443",
		},
		"happy_flow_443": {
			args: "https://manager-address.com:443",
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			managerAddress, err := contract.NewManagerAddress(tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.NotNil(t, managerAddress)
				assert.Equal(t, tc.args, managerAddress.Value())
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
