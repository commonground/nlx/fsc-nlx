// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract

type PeerRole int32

const (
	PeerRoleDirectory PeerRole = 1
)
