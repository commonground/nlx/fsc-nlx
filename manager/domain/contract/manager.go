// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"fmt"
	"net/url"
	"strings"
)

type ManagerAddress string

func NewManagerAddress(s string) (ManagerAddress, error) {
	if s == "" {
		return "", fmt.Errorf("manager address is required")
	}

	if !strings.HasPrefix(s, "https://") {
		return "", fmt.Errorf("missing https prefix in manager address %q", s)
	}

	parsedURL, err := url.Parse(s)
	if err != nil {
		return "", fmt.Errorf("unable to parse manager address %q as URL: %w", s, err)
	}

	port := parsedURL.Port()
	if port == "" {
		return "", fmt.Errorf("missing port in manager address %q", s)
	}

	if port != "443" && port != "8443" {
		return "", fmt.Errorf("invalid port in manager address %q. Must be 8443 or 443", s)
	}

	return ManagerAddress(parsedURL.String()), nil
}

func (a ManagerAddress) Value() string {
	return string(a)
}
