// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import "fmt"

type NewGrantDelegatedServiceConnectionArgs struct {
	Outway    *NewGrantDelegatedServiceConnectionOutwayArgs
	Service   interface{}
	Delegator *NewGrantDelegatedServiceConnectionDelegatorArgs
}

type NewGrantDelegatedServiceConnectionOutwayArgs struct {
	Peer                *NewPeerArgs
	PublicKeyThumbprint string
}

type NewGrantDelegatedServiceConnectionServiceArgs struct {
	Peer *NewPeerArgs
	Name string
}

type NewGrantDelegatedServiceConnectionDelegatedServiceArgs struct {
	Peer                 *NewPeerArgs
	Name                 string
	PublicationDelegator *NewPeerArgs
}

type NewGrantDelegatedServiceConnectionDelegatorArgs struct {
	Peer *NewPeerArgs
}

type GrantDelegatedServiceConnection struct {
	hash      *grantDelegatedServiceConnectionHash
	outway    *grantDelegatedServiceConnectionOutway
	service   interface{}
	delegator *grantDelegatedServiceConnectionDelegator
}

type grantDelegatedServiceConnectionOutway struct {
	peer                *Peer
	publicKeyThumbprint *PublicKeyThumbprint
}

type GrantDelegatedServiceConnectionService struct {
	peer *Peer
	name serviceName
}

type GrantDelegatedServiceConnectionDelegatedService struct {
	peer                 *Peer
	name                 serviceName
	publicationDelegator *Peer
}

type grantDelegatedServiceConnectionDelegator struct {
	peer *Peer
}

func (g *GrantDelegatedServiceConnection) Outway() *grantDelegatedServiceConnectionOutway {
	return g.outway
}

func (g *GrantDelegatedServiceConnection) Hash() *grantDelegatedServiceConnectionHash {
	return g.hash
}

func (g *GrantDelegatedServiceConnection) Service() interface{} {
	return g.service
}

func (g *GrantDelegatedServiceConnection) Delegator() *grantDelegatedServiceConnectionDelegator {
	return g.delegator
}

func (s *GrantDelegatedServiceConnectionService) Peer() *Peer {
	return s.peer
}

func (s *GrantDelegatedServiceConnectionService) Name() string {
	return string(s.name)
}

func (s *GrantDelegatedServiceConnectionDelegatedService) Peer() *Peer {
	return s.peer
}

func (s *GrantDelegatedServiceConnectionDelegatedService) Name() string {
	return string(s.name)
}

func (s *GrantDelegatedServiceConnectionDelegatedService) PublicationDelegator() *Peer {
	return s.publicationDelegator
}

func (c *grantDelegatedServiceConnectionOutway) Peer() *Peer {
	return c.peer
}

func (s *grantDelegatedServiceConnectionDelegator) Peer() *Peer {
	return s.peer
}

func (c *grantDelegatedServiceConnectionOutway) PublicKeyThumbprint() *PublicKeyThumbprint {
	return c.publicKeyThumbprint
}

// nolint:dupl // grants are definitively similar but still have distinctive properties
func newGrantDelegatedServiceConnection(gID GroupID, cID contentIV, alg HashAlg, args *NewGrantDelegatedServiceConnectionArgs) (*GrantDelegatedServiceConnection, error) {
	if args == nil {
		return nil, fmt.Errorf("new delegated service connection args cannot be nil")
	}

	outway, err := newGrantDelegatedServiceConnectionOutway(args.Outway)
	if err != nil {
		return nil, err
	}

	var service any
	switch s := args.Service.(type) {
	case *NewGrantDelegatedServiceConnectionServiceArgs:
		service, err = newGrantDelegatedServiceConnectionService(s)
		if err != nil {
			return nil, err
		}
	case *NewGrantDelegatedServiceConnectionDelegatedServiceArgs:
		service, err = newGrantDelegatedServiceConnectionDelegatedService(s)
		if err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("unknown service type: %T", args.Service)
	}

	delegator, err := newGrantDelegatedServiceConnectionDelegator(args.Delegator)
	if err != nil {
		return nil, err
	}

	h, err := newGrantDelegatedServiceConnectionHash(gID, cID, alg, &GrantDelegatedServiceConnection{
		outway:    outway,
		service:   service,
		delegator: delegator,
	})
	if err != nil {
		return nil, err
	}

	return &GrantDelegatedServiceConnection{
		hash:      h,
		outway:    outway,
		service:   service,
		delegator: delegator,
	}, nil
}

func newGrantDelegatedServiceConnectionService(args *NewGrantDelegatedServiceConnectionServiceArgs) (*GrantDelegatedServiceConnectionService, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service connection service args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	name, err := newServiceName(args.Name)
	if err != nil {
		return nil, err
	}

	return &GrantDelegatedServiceConnectionService{
		peer: p,
		name: name,
	}, nil
}

func newGrantDelegatedServiceConnectionDelegatedService(args *NewGrantDelegatedServiceConnectionDelegatedServiceArgs) (*GrantDelegatedServiceConnectionDelegatedService, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service connection service args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	name, err := newServiceName(args.Name)
	if err != nil {
		return nil, err
	}

	publicationDelegator, err := NewPeer(args.PublicationDelegator)
	if err != nil {
		return nil, err
	}

	return &GrantDelegatedServiceConnectionDelegatedService{
		peer:                 p,
		name:                 name,
		publicationDelegator: publicationDelegator,
	}, nil
}

func newGrantDelegatedServiceConnectionOutway(args *NewGrantDelegatedServiceConnectionOutwayArgs) (*grantDelegatedServiceConnectionOutway, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service connection outway args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	pubKeyThumbprint, err := NewPublicKeyThumbprint(args.PublicKeyThumbprint)
	if err != nil {
		return nil, err
	}

	return &grantDelegatedServiceConnectionOutway{
		peer:                p,
		publicKeyThumbprint: pubKeyThumbprint,
	}, nil
}

func newGrantDelegatedServiceConnectionDelegator(args *NewGrantDelegatedServiceConnectionDelegatorArgs) (*grantDelegatedServiceConnectionDelegator, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service connection delegator args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	return &grantDelegatedServiceConnectionDelegator{
		peer: p,
	}, nil
}
