// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"fmt"
	"sort"
)

type NewPeerArgs struct {
	ID             string
	Name           string
	ManagerAddress string
	Roles          []PeerRole
}

type PeerID string
type peerName string

type Peer struct {
	id             PeerID
	name           peerName
	managerAddress ManagerAddress
	roles          []PeerRole
}

type Peers map[PeerID]*Peer
type PeersIDs map[PeerID]bool

func (p PeersIDs) Value() []string {
	result := make([]string, 0, len(p))

	for peerID := range p {
		result = append(result, peerID.Value())
	}

	sort.Slice(result, func(i, j int) bool {
		return result[i] < result[j]
	})

	return result
}

const (
	peerIDMaxLength   = 20
	peerNameMaxLength = 64
)

func NewPeer(args *NewPeerArgs) (*Peer, error) {
	if args == nil {
		return nil, fmt.Errorf("new peer args cannot be nil")
	}

	s, err := NewPeerID(args.ID)
	if err != nil {
		return nil, err
	}

	var pName peerName
	if args.Name != "" {
		pName, err = newPeerName(args.Name)
		if err != nil {
			return nil, fmt.Errorf("peer invalid peer name")
		}
	}

	var mAddress ManagerAddress
	if args.ManagerAddress != "" {
		mAddress, err = NewManagerAddress(args.ManagerAddress)
		if err != nil {
			return nil, fmt.Errorf("peer invalid manager address: %w", err)
		}
	}

	return &Peer{
		id:             s,
		name:           pName,
		managerAddress: mAddress,
		roles:          args.Roles,
	}, nil
}

func NewPeerIDs(ids []string) (PeersIDs, error) {
	pIDs := make(PeersIDs)

	for _, id := range ids {
		p, err := NewPeerID(id)
		if err != nil {
			return nil, fmt.Errorf("invalid peer id in peer IDs: %w", err)
		}

		pIDs[p] = true
	}

	return pIDs, nil
}

func NewPeerID(s string) (PeerID, error) {
	if s == "" {
		return PeerID(""), fmt.Errorf("peerID cannot be empty")
	}

	if len(s) > peerIDMaxLength {
		return PeerID(""), fmt.Errorf("peerID cannot be longer than %d characters", peerIDMaxLength)
	}

	if len(s) < peerIDMaxLength {
		return "", fmt.Errorf("peerID cannot be smaller than %d characters", peerIDMaxLength)
	}

	return PeerID(s), nil
}

func newPeerName(s string) (peerName, error) {
	if s == "" {
		return peerName(""), fmt.Errorf("peerName cannot be empty")
	}

	if len(s) > peerNameMaxLength {
		return peerName(""), fmt.Errorf("peerName cannot be longer than %d characters", peerNameMaxLength)
	}

	return peerName(s), nil
}

func (s peerName) Value() string {
	return string(s)
}

func (s PeerID) Value() string {
	return string(s)
}

func (p *Peer) ID() PeerID {
	return p.id
}

func (p *Peer) Name() peerName {
	return p.name
}

func (p *Peer) ManagerAddress() ManagerAddress {
	return p.managerAddress
}

func (p *Peer) Roles() []PeerRole {
	return p.roles
}

func (p *Peer) IsEqual(other *Peer) bool {
	return p.id == other.id
}

func (s PeerID) IsEqualString(otherPeerID string) bool {
	return s == PeerID(otherPeerID)
}

func (p *Peers) IDs() PeersIDs {
	ids := make(PeersIDs, len(*p))

	for _, peer := range *p {
		ids[peer.ID()] = true
	}

	return ids
}
