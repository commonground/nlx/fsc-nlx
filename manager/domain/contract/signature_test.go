// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package contract_test

import (
	"encoding/json"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gopkg.in/square/go-jose.v2"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:dupl,funlen // table tests are long and share setup
func TestNewSignature(t *testing.T) {
	t.Parallel()

	type expected struct {
		bundle      *tls.CertificateBundle
		contentHash *contract.ContentHash
		clock       clock.Clock
	}

	testCases := map[string]struct {
		setup   func() (*contract.NewSignatureArgs, *expected)
		wantErr error
	}{
		"parse_error": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				return &contract.NewSignatureArgs{
					SigType:    "",
					Signature:  "",
					Content:    nil,
					PeersCerts: nil,
				}, nil
			},
			wantErr: errors.New("could not parse signature: square/go-jose: compact JWS format must have three parts"),
		},
		"missing_x5t_header": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, nil)
				require.NoError(t, err)

				object, err := signer.Sign([]byte("test"))
				require.NoError(t, err)

				serialized := object.FullSerialize()

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: nil,
					Content:    nil,
				}

				return testArgs, nil
			},
			wantErr: errors.New("could not find required header in jws signature: x5t#S256"),
		},
		"invalid_x5t_header_type": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", 1)

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				object, err := signer.Sign([]byte("test"))
				require.NoError(t, err)

				serialized := object.FullSerialize()

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: nil,
					Content:    nil,
				}

				return testArgs, nil
			},
			wantErr: errors.New("certificate thumbprint must be of type string"),
		},
		"invalid_x5t_header": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", "invalid thumbprint")

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				object, err := signer.Sign([]byte("test"))
				require.NoError(t, err)

				serialized := object.FullSerialize()

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: nil,
					Content:    nil,
				}

				return testArgs, nil
			},
			wantErr: errors.New("invalid certificate thumbprint in jws header: could not decode base64 encoded certificate thumbprint: illegal base64 data at input byte 7"),
		},
		"wrong_x5t_header": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				bundle2, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-b/manager.organization-b.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-b/manager.organization-b.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", bundle2.CertificateThumbprint())

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				object, err := signer.Sign([]byte("test"))
				require.NoError(t, err)

				serialized := object.FullSerialize()

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: nil,
					Content:    nil,
				}

				return testArgs, nil
			},
			wantErr: errors.New("could not find certificate for certificate thumbprint in jws signature: no peer certificate found for certificate thumbprint:"),
		},
		"signed_with_different_key": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				bundle2, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-b/manager.organization-b.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-b/manager.organization-b.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", bundle.CertificateThumbprint())

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle2.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				object, err := signer.Sign([]byte("test"))
				require.NoError(t, err)

				serialized := object.FullSerialize()

				mockClock := testingutils.NewMockClock(time.Now())
				certificates, err := contract.NewPeersCertificates(mockClock, bundle.RootCAs(), contract.NewPeersCertificatesArgs{
					"00000000000000000001": contract.RawDERPeerCertificates{
						bundle.Certificate().Raw,
					},
				})
				require.NoError(t, err)

				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: certificates,
					Content:    content,
				}

				return testArgs, nil
			},
			wantErr: errors.New("could not verify jws token in signature"),
		},
		"invalid_signature_payload": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", bundle.CertificateThumbprint())

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				object, err := signer.Sign([]byte("invalid payload"))
				require.NoError(t, err)

				serialized := object.FullSerialize()

				mockClock := testingutils.NewMockClock(time.Now())
				certificates, err := contract.NewPeersCertificates(mockClock, bundle.RootCAs(), contract.NewPeersCertificatesArgs{
					"00000000000000000001": contract.RawDERPeerCertificates{
						bundle.Certificate().Raw,
					},
				})
				require.NoError(t, err)

				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: certificates,
					Content:    content,
				}

				return testArgs, nil
			},
			wantErr: errors.New("could not parse jws signature:"),
		},
		"invalid_signature_type": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", bundle.CertificateThumbprint())

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				sigPayload, err := json.Marshal(contract.SignaturePayload{
					ContentHash: content.Hash().String(),
					SigType:     string(contract.SignatureTypeAccept),
					SignedAt:    mockClock.Now().Unix(),
				})
				require.NoError(t, err)

				object, err := signer.Sign(sigPayload)
				require.NoError(t, err)

				serialized := object.FullSerialize()

				certificates, err := contract.NewPeersCertificates(mockClock, bundle.RootCAs(), contract.NewPeersCertificatesArgs{
					"00000000000000000001": contract.RawDERPeerCertificates{
						bundle.Certificate().Raw,
					},
				})
				require.NoError(t, err)

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureType("invalid_signature_type"),
					Signature:  serialized,
					PeersCerts: certificates,
					Content:    content,
				}

				expectedValues := &expected{
					bundle:      bundle,
					contentHash: content.Hash(),
					clock:       mockClock,
				}

				return testArgs, expectedValues
			},
			wantErr: errors.New("signature must be of type expected."),
		},
		"invalid_content_hash": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", bundle.CertificateThumbprint())

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				sigPayload, err := json.Marshal(contract.SignaturePayload{
					ContentHash: "invalid content hash",
					SigType:     string(contract.SignatureTypeAccept),
					SignedAt:    mockClock.Now().Unix(),
				})
				require.NoError(t, err)

				object, err := signer.Sign(sigPayload)
				require.NoError(t, err)

				serialized := object.FullSerialize()

				certificates, err := contract.NewPeersCertificates(mockClock, bundle.RootCAs(), contract.NewPeersCertificatesArgs{
					"00000000000000000001": contract.RawDERPeerCertificates{
						bundle.Certificate().Raw,
					},
				})
				require.NoError(t, err)

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: certificates,
					Content:    content,
				}

				expectedValues := &expected{
					bundle:      bundle,
					contentHash: content.Hash(),
					clock:       mockClock,
				}

				return testArgs, expectedValues
			},
			wantErr: errors.New("invalid content hash in jws signature payload"),
		},
		"mismatch_content_hash": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", bundle.CertificateThumbprint())

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				sigPayload, err := json.Marshal(contract.SignaturePayload{
					ContentHash: "$1$1$UHl-GSepnzG1hTLywVps4W3BdfkCzvVxKI8mYEBaSTGR2mNkd3q2JYH3mn51B2TwQC2HqUTxuIcI9YFObJ3Zag",
					SigType:     string(contract.SignatureTypeAccept),
					SignedAt:    mockClock.Now().Unix(),
				})
				require.NoError(t, err)

				object, err := signer.Sign(sigPayload)
				require.NoError(t, err)

				serialized := object.FullSerialize()

				certificates, err := contract.NewPeersCertificates(mockClock, bundle.RootCAs(), contract.NewPeersCertificatesArgs{
					"00000000000000000001": contract.RawDERPeerCertificates{
						bundle.Certificate().Raw,
					},
				})
				require.NoError(t, err)

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: certificates,
					Content:    content,
				}

				expectedValues := &expected{
					bundle:      bundle,
					contentHash: content.Hash(),
					clock:       mockClock,
				}

				return testArgs, expectedValues
			},
			wantErr: errors.New("the signature contract content hash \"$1$1$UHl-GSepnzG1hTLywVps4W3BdfkCzvVxKI8mYEBaSTGR2mNkd3q2JYH3mn51B2TwQC2HqUTxuIcI9YFObJ3Zag\" does not match the contract content hash you are accepting"),
		},
		"invalid_signed_at": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", bundle.CertificateThumbprint())

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				sigPayload, err := json.Marshal(contract.SignaturePayload{
					ContentHash: content.Hash().String(),
					SigType:     string(contract.SignatureTypeAccept),
					SignedAt:    0,
				})
				require.NoError(t, err)

				object, err := signer.Sign(sigPayload)
				require.NoError(t, err)

				serialized := object.FullSerialize()

				certificates, err := contract.NewPeersCertificates(mockClock, bundle.RootCAs(), contract.NewPeersCertificatesArgs{
					"00000000000000000001": contract.RawDERPeerCertificates{
						bundle.Certificate().Raw,
					},
				})
				require.NoError(t, err)

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: certificates,
					Content:    content,
				}

				expectedValues := &expected{
					bundle:      bundle,
					contentHash: content.Hash(),
					clock:       mockClock,
				}

				return testArgs, expectedValues
			},
			wantErr: errors.New("invalid signed at datetime for signature: 0"),
		},
		"invalid_signed_at_before": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", bundle.CertificateThumbprint())

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				sigPayload, err := json.Marshal(contract.SignaturePayload{
					ContentHash: content.Hash().String(),
					SigType:     string(contract.SignatureTypeAccept),
					SignedAt:    time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC).Unix(),
				})
				require.NoError(t, err)

				object, err := signer.Sign(sigPayload)
				require.NoError(t, err)

				serialized := object.FullSerialize()

				certificates, err := contract.NewPeersCertificates(mockClock, bundle.RootCAs(), contract.NewPeersCertificatesArgs{
					"00000000000000000001": contract.RawDERPeerCertificates{
						bundle.Certificate().Raw,
					},
				})
				require.NoError(t, err)

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: certificates,
					Content:    content,
				}

				expectedValues := &expected{
					bundle:      bundle,
					contentHash: content.Hash(),
					clock:       mockClock,
				}

				return testArgs, expectedValues
			},
			wantErr: errors.New("invalid signed at datetime for signature: 1640995200"),
		},
		"unsupported_algorithm": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				// Signing with HS256 which is not supported
				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.HS256, Key: []byte("testkey")}, nil)
				assert.NoError(t, err)

				object, err := signer.Sign([]byte("Lorem ipsum dolor sit amet"))
				assert.NoError(t, err)

				serialized := object.FullSerialize()

				return &contract.NewSignatureArgs{
					SigType:   contract.SignatureTypeAccept,
					Signature: serialized,
				}, nil
			},
			wantErr: errors.New("the algorithm HS256 in the signature is not supported"),
		},
		"happy_flow": {
			setup: func() (*contract.NewSignatureArgs, *expected) {
				mockClock := testingutils.NewMockClock(time.Now())

				bundle, err := tls.NewBundleFromFiles(
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/cert.pem",
					"../../../pki/external/certs/organization-a/manager.organization-a.nlx.localhost/key.pem",
					"../../../pki/external/ca/intermediate-1.pem",
				)
				require.NoError(t, err)

				signerOpts := &jose.SignerOptions{}
				signerOpts.WithHeader("x5t#S256", bundle.CertificateThumbprint())

				signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: bundle.PrivateKey()}, signerOpts)
				require.NoError(t, err)

				contentArgs := getValidNewContentArgs(t)
				content, err := contract.NewContent(contentArgs)
				require.NoError(t, err)

				sigPayload, err := json.Marshal(contract.SignaturePayload{
					ContentHash: content.Hash().String(),
					SigType:     string(contract.SignatureTypeAccept),
					SignedAt:    mockClock.Now().Unix(),
				})
				require.NoError(t, err)

				object, err := signer.Sign(sigPayload)
				require.NoError(t, err)

				serialized := object.FullSerialize()

				certificates, err := contract.NewPeersCertificates(mockClock, bundle.RootCAs(), contract.NewPeersCertificatesArgs{
					"00000000000000000001": contract.RawDERPeerCertificates{
						bundle.Certificate().Raw,
					},
				})
				require.NoError(t, err)

				testArgs := &contract.NewSignatureArgs{
					SigType:    contract.SignatureTypeAccept,
					Signature:  serialized,
					PeersCerts: certificates,
					Content:    content,
				}

				expectedValues := &expected{
					bundle:      bundle,
					contentHash: content.Hash(),
					clock:       mockClock,
				}

				return testArgs, expectedValues
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			var args *contract.NewSignatureArgs

			var expectedValues *expected

			if tc.setup != nil {
				args, expectedValues = tc.setup()
			}

			signature, err := contract.NewSignature(args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.NotEmpty(t, signature)
				assert.Equal(t, expectedValues.bundle.GetPeerInfo().SerialNumber, signature.Peer().ID().Value())
				assert.Equal(t, expectedValues.contentHash, signature.ContentHash())
				assert.Equal(t, expectedValues.clock.Now().Unix(), signature.SignedAt().Unix())
				assert.Equal(t, expectedValues.bundle.CertificateThumbprint(), signature.CertificateThumbprint())
			} else {
				assert.ErrorContains(t, err, tc.wantErr.Error())
			}
		})
	}
}
