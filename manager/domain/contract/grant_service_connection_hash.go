// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"github.com/pkg/errors"
)

type grantServiceConnectionHash Hash

func newGrantServiceConnectionHash(gID GroupID, cID contentIV, alg HashAlg, gc *GrantServiceConnection) (*grantServiceConnectionHash, error) {
	h, err := newHash(alg, HashTypeGrantServiceConnection, getSortedGrantConnectionBytes(gID, cID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant service connection hash")
	}

	return (*grantServiceConnectionHash)(h), nil
}

func getSortedGrantConnectionBytes(gID GroupID, cID contentIV, gc *GrantServiceConnection) []byte {
	grantBytes := make([]byte, 0)

	grantBytes = append(grantBytes, []byte(gID)...)
	grantBytes = append(grantBytes, cID.Bytes()...)
	grantBytes = append(grantBytes, bytesFromInt32(int32(GrantTypeServiceConnection))...)
	grantBytes = append(grantBytes, []byte(gc.Outway().Peer().ID().Value())...)
	grantBytes = append(grantBytes, []byte(gc.Outway().PublicKeyThumbprint().Value())...)

	// nolint:dupl // looks the same but is a different grant
	switch s := gc.Service().(type) {
	case *GrantServiceConnectionService:
		grantBytes = append(grantBytes, bytesFromInt32(int32(ServiceTypeService))...)
		grantBytes = append(grantBytes, []byte(s.Peer().ID())...)
		grantBytes = append(grantBytes, []byte(s.Name())...)
	case *GrantServiceConnectionDelegatedService:
		grantBytes = append(grantBytes, bytesFromInt32(int32(ServiceTypeDelegatedService))...)
		grantBytes = append(grantBytes, []byte(s.Peer().ID())...)
		grantBytes = append(grantBytes, []byte(s.Name())...)
		grantBytes = append(grantBytes, []byte(s.PublicationDelegator().ID().Value())...)
	}

	return grantBytes
}

func (h grantServiceConnectionHash) String() string {
	return Hash(h).String()
}
