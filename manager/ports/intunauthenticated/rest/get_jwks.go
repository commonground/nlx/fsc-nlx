// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"crypto/x509"
	"encoding/base64"
	"errors"
	"fmt"

	"github.com/lestrrat-go/jwx/v2/cert"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/intunauthenticated/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/intunauthenticated/rest/api/server"
)

func (s *Server) GetJSONWebKeySet(ctx context.Context, _ api.GetJSONWebKeySetRequestObject) (api.GetJSONWebKeySetResponseObject, error) {
	certs, err := s.app.Queries.GetCertificates.Handle(ctx)
	if err != nil {
		s.logger.Error("error executing GetJSONWebKeySet query", err)
		return nil, err
	}

	keys := make([]models.Jwk, 0, len(*certs))

	for thumbprint, c := range *certs {
		key, _ := peerCertToKey(thumbprint, c)

		keys = append(keys, *key)
	}

	jwks := models.Jwks{
		Keys: keys,
	}

	return api.GetJSONWebKeySet200JSONResponse(jwks), nil
}

func peerCertToKey(thumbprint contract.CertificateThumbprint, c *contract.PeerCertificate) (*models.Jwk, error) {
	key, err := jwk.FromRaw(c.PublicKey())
	if err != nil {
		return nil, fmt.Errorf("failed to create jwk key: %w", err)
	}

	rawDERs := rawDERstoBase64(c.RawDERs())

	certChain := &cert.Chain{}
	for _, src := range rawDERs {
		_ = certChain.AddString(src)
	}

	err = key.Set(jwk.X509CertChainKey, certChain)
	if err != nil {
		return nil, fmt.Errorf("cannot set x5c on JWK: %w", err)
	}

	err = key.Set(jwk.X509CertThumbprintS256Key, thumbprint.Value())
	if err != nil {
		return nil, errors.New("cannot set x5t#s256 on JWK")
	}

	err = key.Set(jwk.KeyIDKey, thumbprint.Value())
	if err != nil {
		return nil, errors.New("cannot set kid on JWK")
	}

	err = key.Set(jwk.KeyUsageKey, "sig")
	if err != nil {
		return nil, errors.New("cannot set use on JWK")
	}

	alg, err := x509SignatureAlgorithmToJwkAlg(c.SignatureAlgorithm())
	if err != nil {
		return nil, fmt.Errorf("could not convert x509 signature alg to jwk alg: %w", err)
	}

	err = key.Set(jwk.AlgorithmKey, alg)
	if err != nil {
		return nil, errors.New("cannot set alg on JWK")
	}

	keyItem, err := convertKey(key, &rawDERs)
	if err != nil {
		return nil, fmt.Errorf("could not convert JWK to response format: %w", err)
	}

	return &keyItem, nil
}

// Cannot use jwt.Key as response object in models.gen.go since jwt.Key is an interface, so transforming to struct
func convertKey(key jwk.Key, x5c *[]string) (models.Jwk, error) {
	keyItem := models.Jwk{}

	keyItem.Alg = ptr(models.JwkAlg(key.Algorithm().String()))
	keyItem.Kid = ptr(key.KeyID())
	keyItem.Use = ptr(models.JwkUse(key.KeyUsage()))
	keyItem.Kid = ptr(key.KeyID())
	keyItem.X5c = x5c
	keyItem.X5tS256 = ptr(key.X509CertThumbprintS256())
	keyItem.Kty = models.JwkKty(key.KeyType())

	err := convertPublicKey(&keyItem, key)
	if err != nil {
		return models.Jwk{}, errors.New("could not convert public key: " + err.Error())
	}

	return keyItem, nil
}

func ptr[T any](value T) *T {
	return &value
}

func rawDERstoBase64(rawDERs [][]byte) []string {
	certs := make([]string, len(rawDERs))

	for i, c := range rawDERs {
		certs[i] = base64.StdEncoding.EncodeToString(c)
	}

	return certs
}

func convertPublicKey(responseKey *models.Jwk, key jwk.Key) error {
	// Get key type specific values
	rawKeys, err := key.AsMap(context.Background())
	if err != nil {
		return errors.New("could not retrieve jwk values")
	}

	switch key.KeyType() {
	case jwa.RSA:
		err := convertRSAPublicKey(responseKey, rawKeys)
		if err != nil {
			return err
		}
	case jwa.EC:
		err := convertECPublicKey(responseKey, rawKeys)
		if err != nil {
			return err
		}
	default:
		return errors.New("unknown 'JWK.typ' only RSA and EC keys are supported")
	}

	return nil
}

func convertECPublicKey(responseKey *models.Jwk, rawKeys map[string]interface{}) error {
	ecPublicKey := models.EcPublicKey{}

	if x, ok := rawKeys["x"]; ok {
		xBytes := x.([]byte)
		ecPublicKey.X = &xBytes
	}

	if y, ok := rawKeys["y"]; ok {
		yBytes := y.([]byte)
		ecPublicKey.Y = &yBytes
	}

	if crv, ok := rawKeys["crv"]; ok {
		ecCrv, ok := crv.(jwa.EllipticCurveAlgorithm)
		if !ok {
			return errors.New("could not convert elliptic curve algorithm")
		}

		crvString := ecCrv.String()

		ecPublicKey.Crv = &crvString
	}

	err := responseKey.FromEcPublicKey(ecPublicKey)
	if err != nil {
		return errors.New("could not create EC public key from jwk")
	}

	return nil
}

func convertRSAPublicKey(responseKey *models.Jwk, rawKeys map[string]interface{}) error {
	rsaPublicKey := models.RsaPublicKey{}

	if n, ok := rawKeys["n"]; ok {
		nBytes := n.([]byte)
		rsaPublicKey.N = &nBytes
	}

	if e, ok := rawKeys["e"]; ok {
		eBytes := e.([]byte)
		rsaPublicKey.E = &eBytes
	}

	err := responseKey.FromRsaPublicKey(rsaPublicKey)
	if err != nil {
		return errors.New("could not create RSA public key from jwk")
	}

	return nil
}

func x509SignatureAlgorithmToJwkAlg(algorithm x509.SignatureAlgorithm) (string, error) {
	switch algorithm {
	case x509.SHA256WithRSA:
		return "RS256", nil
	case x509.SHA384WithRSA:
		return "RS384", nil
	case x509.SHA512WithRSA:
		return "RS512", nil
	case x509.ECDSAWithSHA256:
		return "ES256", nil
	case x509.ECDSAWithSHA384:
		return "ES384", nil
	case x509.ECDSAWithSHA512:
		return "ES512", nil
	case x509.SHA256WithRSAPSS:
		return "PS256", nil
	case x509.SHA384WithRSAPSS:
		return "PS384", nil
	case x509.SHA512WithRSAPSS:
		return "PS512", nil
	default:
		return "", errors.New("algorithm not supported in JWK")
	}
}
