// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetPeerInfo(ctx context.Context, _ api.GetPeerInfoRequestObject) (api.GetPeerInfoResponseObject, error) {
	info, err := s.app.Queries.GetPeerInfo.Handle(ctx, &query.GetPeerInfoHandlerArgs{
		SelfPeerID:             s.cert.GetPeerInfo().SerialNumber,
		SelfPeerName:           s.cert.GetPeerInfo().Name,
		SelfPeerManagerAddress: s.selfAddress,
	})
	if err != nil {
		s.logger.Error("error executing get peer info query", err)
		return nil, err
	}

	enabledExtension := models.FSCCoreEnabledExtensions{}

	for _, e := range info.EnabledExtensions {
		switch e.Type {
		case query.ExtensionTypeDelegation:
			version := e.Version.(query.FSCDelegationVersion)

			var versionString models.FSCCoreDelegationVersion

			switch version {
			case query.FSCDelegationVersion1_0_0:
				versionString = models.FSCCoreDelegationVersionN100
			default:
				return nil, fmt.Errorf("unknown extension version: %d", e.Version)
			}

			enabledExtension.EXTENSIONDELEGATION = &versionString
		case query.ExtensionTypeLogging:
			version := e.Version.(query.FSCLoggingVersion)

			var versionString models.FSCCoreLoggingVersion

			switch version {
			case query.FSCLoggingVersion1_0_0:
				versionString = models.N100
			default:
				return nil, fmt.Errorf("unknown extension version: %d", e.Version)
			}

			enabledExtension.EXTENSIONTRANSACTIONLOGGING = &versionString
		default:
			return nil, fmt.Errorf("unknown extension type: %T", e.Type)
		}
	}

	return api.GetPeerInfo200JSONResponse{
		FscVersion:        models.FSCCoreFscVersionN100,
		PeerId:            info.Peer.ID,
		PeerName:          info.Peer.Name,
		EnabledExtensions: enabledExtension,
	}, nil
}
