// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"crypto/x509"
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	externalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext"
	external_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
)

type Server struct {
	app         *externalapp.Application
	logger      *logger.Logger
	cert        *common_tls.CertificateBundle
	selfAddress string
	handler     http.Handler
}

type connectingPeer struct {
	id             string
	name           string
	managerAddress string
	cert           *x509.Certificate
}

type contextConnectingPeerKey struct{}

const (
	HeaderManagerAddress string = "fsc-manager-address"
)

type NewArgs struct {
	Logger      *logger.Logger
	App         *externalapp.Application
	Cert        *common_tls.CertificateBundle
	SelfAddress string
	CRLCache    *common_tls.CRLsCache
}

func New(args *NewArgs) (*Server, error) {
	if args.SelfAddress == "" {
		return nil, errors.New("self address is required")
	}

	crl := args.CRLCache

	s := &Server{
		app:         args.App,
		logger:      args.Logger,
		cert:        args.Cert,
		selfAddress: args.SelfAddress,
	}

	strict := api.NewStrictHandlerWithOptions(s, []api.StrictMiddlewareFunc{
		func(f api.StrictHandlerFunc, operationID string) api.StrictHandlerFunc {
			return func(ctx context.Context, w http.ResponseWriter, r *http.Request, args interface{}) (interface{}, error) {
				if len(r.TLS.PeerCertificates) == 0 {
					return nil, fmt.Errorf("client certificate missing")
				}

				peerCert := r.TLS.PeerCertificates[0]

				if len(peerCert.Subject.Organization) == 0 || peerCert.Subject.Organization[0] == "" {
					return nil, fmt.Errorf("missing organization in subject of client certifcate")
				}

				if peerCert.Subject.SerialNumber == "" {
					return nil, fmt.Errorf("missing subject serial number in client certificate")
				}

				err := crl.CheckCertificate(peerCert)
				if err != nil {
					return nil, external_errors.NewErrorTypeCertificateRevoked(err, peerCert.SerialNumber.String())
				}

				if r.Header.Get(HeaderManagerAddress) == "" && r.Method != http.MethodGet && r.URL.Path != "/v1/token" {
					return nil, fmt.Errorf("missing fsc-manager-address header")
				}

				ctx = withConnectingPeer(ctx, &connectingPeer{
					id:             peerCert.Subject.SerialNumber,
					name:           peerCert.Subject.Organization[0],
					managerAddress: r.Header.Get(HeaderManagerAddress),
					cert:           peerCert,
				})

				return f(ctx, w, r, args)
			}
		},
	}, api.StrictHTTPServerOptions{
		RequestErrorHandlerFunc:  RequestErrorHandler,
		ResponseErrorHandlerFunc: ResponseErrorHandler,
	})

	r := chi.NewRouter()
	api.HandlerFromMux(strict, r)

	s.handler = r

	return s, nil
}

func (s *Server) Handler() http.Handler {
	return s.handler
}

func withConnectingPeer(ctx context.Context, peer *connectingPeer) context.Context {
	return context.WithValue(ctx, contextConnectingPeerKey{}, peer)
}

func getConnectingPeerFromCtx(ctx context.Context) (*connectingPeer, error) {
	peer, ok := ctx.Value(contextConnectingPeerKey{}).(*connectingPeer)
	if !ok {
		return nil, fmt.Errorf("invalid connecting peer type in context, must be type *connectingPeer")
	}

	return peer, nil
}
