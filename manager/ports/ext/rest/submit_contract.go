// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) SubmitContract(ctx context.Context, request api.SubmitContractRequestObject) (api.SubmitContractResponseObject, error) {
	peer, err := getConnectingPeerFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	contractContent, err := contractContentToArgs(&request.Body.ContractContent)
	if err != nil {
		return nil, err
	}

	err = s.app.Commands.SubmitContract.Handle(ctx, &command.HandleSubmitContractArgs{
		ContractContent: contractContent,
		Signature:       request.Body.Signature,
		SubmittedByPeer: &command.PeerArgs{
			ID:             peer.id,
			Name:           peer.name,
			ManagerAddress: peer.managerAddress,
		},
	})
	if err != nil {
		s.logger.Error("could not submit contract", err)

		return nil, err
	}

	return api.SubmitContract201Response{}, nil
}

func contractContentToArgs(content *models.FSCCoreContractContent) (*command.ContractContentArgs, error) {
	grants, err := mapRestToCommandGrants(content.Grants)
	if err != nil {
		return nil, fmt.Errorf("could not map grants: %w", err)
	}

	contractIV, err := uuid.Parse(content.Iv)
	if err != nil {
		return nil, fmt.Errorf("could not parse contract IV. %s", err)
	}

	uuidBytes, err := contractIV.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("could not get bytes from uuid. %s", err)
	}

	return &command.ContractContentArgs{
		HashAlgorithm: string(content.HashAlgorithm),
		IV:            uuidBytes,
		GroupID:       content.GroupId,
		NotBefore:     time.Unix(content.Validity.NotBefore, 0),
		NotAfter:      time.Unix(content.Validity.NotAfter, 0),
		Grants:        grants,
		CreatedAt:     time.Unix(content.CreatedAt, 0),
	}, nil
}

//nolint:gocyclo // difficult to improve complexity without effecting the readability
func mapRestToCommandGrants(modelGrants []models.FSCCoreGrant) ([]interface{}, error) {
	grants := make([]interface{}, 0)
	for _, grant := range modelGrants {

		discriminator, err := grant.Data.ValueByDiscriminator()
		if err != nil {
			return nil, err
		}

		switch convertedModel := discriminator.(type) {
		case models.FSCCoreGrantServicePublication:
			var protocol contract.ServiceProtocol

			protocol, err = mapServiceProtocolToContract(convertedModel.Service.Protocol)
			if err != nil {
				return nil, fmt.Errorf("invalid protocol in request: %w", err)
			}

			grants = append(grants, &command.GrantServicePublicationArgs{
				DirectoryPeerID: convertedModel.Directory.PeerId,
				ServicePeerID:   convertedModel.Service.PeerId,
				ServiceName:     convertedModel.Service.Name,
				ServiceProtocol: protocol,
			})
		case models.FSCCoreGrantServiceConnection:
			serviceDiscriminator, errServiceDiscriminator := convertedModel.Service.Discriminator()
			if errServiceDiscriminator != nil {
				return nil, errServiceDiscriminator
			}

			var service interface{}

			switch serviceDiscriminator {
			case string(models.SERVICETYPESERVICE):
				s, errService := convertedModel.Service.AsFSCCoreService()
				if errService != nil {
					return nil, fmt.Errorf("invalid service in request: %w", errService)
				}

				service = &command.ServiceArgs{
					PeerID: s.PeerId,
					Name:   s.Name,
				}
			case string(models.SERVICETYPEDELEGATEDSERVICE):
				delegatedService, errService := convertedModel.Service.AsFSCCoreDelegatedService()
				if errService != nil {
					return nil, fmt.Errorf("invalid service in request: %w", errService)
				}

				service = &command.DelegatedServiceArgs{
					PeerID:                            delegatedService.PeerId,
					Name:                              delegatedService.Name,
					ServicePublicationDelegatorPeerID: delegatedService.Delegator.PeerId,
				}

			default:
				return nil, fmt.Errorf("invalid service type: %s", serviceDiscriminator)
			}

			grants = append(grants, &command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: convertedModel.Outway.PublicKeyThumbprint,
				OutwayPeerID:              convertedModel.Outway.PeerId,
				Service:                   service,
			})
		case models.FSCCoreGrantDelegatedServiceConnection:
			var service interface{}

			serviceDiscriminator, errServiceDiscriminator := convertedModel.Service.Discriminator()
			if errServiceDiscriminator != nil {
				return nil, errServiceDiscriminator
			}

			switch serviceDiscriminator {
			case string(models.SERVICETYPESERVICE):
				s, errService := convertedModel.Service.AsFSCCoreService()
				if errService != nil {
					return nil, fmt.Errorf("invalid service in request: %w", errService)
				}

				service = &command.ServiceArgs{
					PeerID: s.PeerId,
					Name:   s.Name,
				}
			case string(models.SERVICETYPEDELEGATEDSERVICE):
				delegatedService, errService := convertedModel.Service.AsFSCCoreDelegatedService()
				if errService != nil {
					return nil, fmt.Errorf("invalid service in request: %w", errService)
				}

				service = &command.DelegatedServiceArgs{
					PeerID:                            delegatedService.PeerId,
					Name:                              delegatedService.Name,
					ServicePublicationDelegatorPeerID: delegatedService.Delegator.PeerId,
				}
			default:
				return nil, fmt.Errorf("invalid service type: %s", serviceDiscriminator)
			}

			grants = append(grants, &command.GrantDelegatedServiceConnectionArgs{
				OutwayPublicKeyThumbprint: convertedModel.Outway.PublicKeyThumbprint,
				OutwayPeerID:              convertedModel.Outway.PeerId,
				Service:                   service,
				DelegatorPeerID:           convertedModel.Delegator.PeerId,
			})
		case models.FSCCoreGrantDelegatedServicePublication:
			var protocol contract.ServiceProtocol

			protocol, err = mapServiceProtocolToContract(convertedModel.Service.Protocol)
			if err != nil {
				return nil, fmt.Errorf("invalid protocol in request: %w", err)
			}

			grants = append(grants, &command.GrantDelegatedServicePublicationArgs{
				DirectoryPeerID: convertedModel.Directory.PeerId,
				ServicePeerID:   convertedModel.Service.PeerId,
				ServiceName:     convertedModel.Service.Name,
				ServiceProtocol: protocol,
				DelegatorPeerID: convertedModel.Delegator.PeerId,
			})
		default:
			return nil, fmt.Errorf("invalid grant type: %T", convertedModel)
		}
	}

	return grants, nil
}

func mapServiceProtocolToContract(p models.FSCCoreProtocol) (contract.ServiceProtocol, error) {
	switch p {
	case models.PROTOCOLTCPHTTP11:
		return contract.ServiceProtocolTCPHTTP1_1, nil
	case models.PROTOCOLTCPHTTP2:
		return contract.ServiceProtocolTCPHTTP2, nil
	default:
		return contract.ServiceProtocolUnspecified, fmt.Errorf("unknown protocol: %s", p)
	}
}
