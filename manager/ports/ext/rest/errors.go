/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package rest

import (
	"encoding/json"
	"errors"
	"net/http"

	ext_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/errors"
	oas_models "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

type fscHTTPError struct {
	status    int
	title     string
	errorCode oas_models.FSCCoreManagerErrorCode
}

var errorCodes = map[ext_errors.Type]fscHTTPError{
	ext_errors.ErrorTypeIncorrectInput: {
		title:  "invalid arguments",
		status: http.StatusUnprocessableEntity,
	},
	ext_errors.ErrorTypeIncorrectGroupID: {
		title:     "incorrect group id",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODEINCORRECTGROUPID,
	},
	ext_errors.ErrorTypePeerNotPartOfContact: {
		title:     "peer not part of contract",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODEPEERNOTPARTOFCONTRACT,
	},
	ext_errors.ErrorTypeContractContentHashMismatch: {
		title:     "signature contract content hash mismatch",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODESIGNATURECONTRACTCONTENTHASHMISMATCH,
	},
	ext_errors.ErrorTypeCertificateVerificationFailed: {
		title:     "peer certificate verification failed",
		status:    http.StatusBadRequest,
		errorCode: oas_models.ERRORCODEPEERCERTIFICATEVERIFICATIONFAILED,
	},
	ext_errors.ErrorTypeSignatureMismatch: {
		title:     "peer id signature mismatch",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODEPEERIDSIGNATUREMISMATCH,
	},
	ext_errors.ErrorTypeSignatureVerificationFailed: {
		title:     "signature verification failed",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODESIGNATUREVERIFICATIONFAILED,
	},
	ext_errors.ErrorTypeIncorrectGrantCombination: {
		title:     "grant combination not allowed",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODEGRANTCOMBINATIONNOTALLOWED,
	},
	ext_errors.ErrorTypePathContentHashMismatch: {
		title:     "URL path content hash mismatch",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODEURLPATHCONTENTHASHMISMATCH,
	},
	ext_errors.ErrorTypeSignatureUnknownAlgorithm: {
		title:     "signature algorithm is not supported",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODEUNKNOWNALGORITHMSIGNATURE,
	},
	ext_errors.ErrorTypeUnknownHashAlgorithm: {
		title:     "hashing algorithm is not supported",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODEUNKNOWNHASHALGORITHMHASH,
	},
	ext_errors.ErrorTypeCertificateRevoked: {
		title:     "certificate is revoked",
		status:    http.StatusBadRequest,
		errorCode: oas_models.ERRORCODEPEERCERTIFICATEVERIFICATIONFAILED,
	},
	ext_errors.ErrorTypePublicKeyThumbprintInvalid: {
		title:     "public key thumbprint is not formatted as a SHA256 HEX encoded string",
		status:    http.StatusUnprocessableEntity,
		errorCode: oas_models.ERRORCODEINCORRECTPUBLICKEYTHUMBPRINT,
	},
}

func RequestErrorHandler(w http.ResponseWriter, r *http.Request, err error) {
	http.Error(w, err.Error(), http.StatusBadRequest)
}

func ResponseErrorHandler(w http.ResponseWriter, r *http.Request, err error) {
	var appError ext_errors.Error
	ok := errors.As(err, &appError)

	if !ok {
		errToHTTP(w, http.StatusInternalServerError, models.Error{
			Title:   "internal server error",
			Details: err.Error(),
		})

		return
	}

	httpError, match := errorCodes[appError.ErrorType()]

	if !match {
		errToHTTP(w, http.StatusInternalServerError, models.Error{
			Title:   "internal server error",
			Details: appError.Error(),
		})

		return
	}

	errToHTTP(w, httpError.status, models.Error{
		Details: appError.Error(),
		Title:   httpError.title,
		Type:    string(httpError.errorCode),
	})
}

func errToHTTP(w http.ResponseWriter, statusCode int, e models.Error) {
	w.Header().Set("Fsc-Error-Code", e.Type)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	e.Status = statusCode

	// Temporary translation between error models defined in OAS and internal, can be removed if the OAS uses the RFC 9457 error response
	_ = json.NewEncoder(w).Encode(oas_models.FSCCoreError{
		Code:    e.Type,
		Domain:  oas_models.ERRORDOMAINMANAGER,
		Message: e.Details,
	})
}
