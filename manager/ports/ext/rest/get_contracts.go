// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetContracts(ctx context.Context, request api.GetContractsRequestObject) (api.GetContractsResponseObject, error) {
	peer, err := getConnectingPeerFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	var limit uint32

	if request.Params.Limit != nil {
		limit = *request.Params.Limit
	}

	var queryGrantType *query.GrantType

	if request.Params.GrantType != nil {
		var gt query.GrantType

		gt, err = convertGrantType(*request.Params.GrantType)
		if err != nil {
			return nil, err
		}

		queryGrantType = &gt
	}

	grantHashes := make([]string, 0)
	if request.Params.GrantHash != nil {
		grantHashes = *request.Params.GrantHash
	}

	var cursor string
	if request.Params.Cursor != nil {
		cursor = *request.Params.Cursor
	}

	querySortOrder := query.SortOrderDescending

	if request.Params.SortOrder != nil {
		var so query.SortOrder

		so, err = convertSortOrder(*request.Params.SortOrder)
		if err != nil {
			return nil, err
		}

		querySortOrder = so
	}

	contracts, err := s.app.Queries.ListContracts.Handle(ctx, &query.ListContractsHandlerArgs{
		PeerID:              peer.id,
		GrantType:           queryGrantType,
		PaginationLimit:     limit,
		GrantHashes:         grantHashes,
		PaginationStartID:   cursor,
		PaginationSortOrder: querySortOrder,
	})
	if err != nil {
		return nil, err
	}

	response := api.GetContracts200JSONResponse{
		Contracts: make([]models.FSCCoreContract, len(contracts)),
	}

	for i, c := range contracts {
		var convertedContract *models.FSCCoreContract

		convertedContract, err = convertContract(c)
		if err != nil {
			return nil, err
		}

		response.Contracts[i] = *convertedContract
	}

	nextCursor := determineNextCursorForContracts(response.Contracts)

	response.Pagination = models.FSCCorePaginationResult{NextCursor: &nextCursor}

	return response, nil
}

func convertContract(c *query.Contract) (*models.FSCCoreContract, error) {
	notAfter := c.ContractNotAfter.Unix()
	notBefore := c.ContractNotBefore.Unix()
	createdAt := c.CreatedAt.Unix()

	grants, err := convertGrants(c)
	if err != nil {
		return nil, err
	}

	content := models.FSCCoreContractContent{
		Iv:            c.IV,
		CreatedAt:     createdAt,
		Grants:        grants,
		GroupId:       c.GroupID,
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Validity:      models.FSCCoreValidity{NotAfter: notAfter, NotBefore: notBefore},
	}

	return &models.FSCCoreContract{
		Content: content,
		Signatures: models.FSCCoreSignatures{
			Accept: c.SignaturesAccepted,
			Reject: c.SignaturesRejected,
			Revoke: c.SignaturesRevoked,
		},
	}, nil
}

//nolint:gocyclo // complex function because of the number of grant types
func convertGrants(c *query.Contract) ([]models.FSCCoreGrant, error) {
	grants := make([]models.FSCCoreGrant, 0)

	for _, g := range c.ServiceConnectionGrants {
		grant, err := convertServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range c.ServicePublicationGrants {
		grant, err := convertServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range c.DelegatedServiceConnectionGrants {
		grant, err := convertDelegatedServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range c.DelegatedServicePublicationGrants {
		grant, err := convertDelegatedServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	return grants, nil
}

func convertServicePublicationGrant(grant *query.ServicePublicationGrant) (*models.FSCCoreGrant, error) {
	data := models.FSCCoreGrant_Data{}
	err := data.FromFSCCoreGrantServicePublication(models.FSCCoreGrantServicePublication{
		Directory: models.FSCCoreDirectory{
			PeerId: grant.DirectoryPeerID,
		},
		Service: models.FSCCoreServicePublication{
			PeerId:   grant.ServicePeerID,
			Name:     grant.ServiceName,
			Protocol: mapServiceProtocolToRest(grant.ServiceProtocol),
		},
	})

	if err != nil {
		return nil, err
	}

	return &models.FSCCoreGrant{
		Data: data,
	}, nil
}

func convertServiceConnectionGrant(grant *query.ServiceConnectionGrant) (*models.FSCCoreGrant, error) {
	service := models.FSCCoreGrantServiceConnection_Service{}

	if grant.ServiceIsDelegatedPublication {
		err := service.FromFSCCoreDelegatedService(models.FSCCoreDelegatedService{
			Name:   grant.ServiceName,
			PeerId: grant.ServicePeerID,
			Delegator: models.FSCCoreDelegator{
				PeerId: grant.ServicePublicationDelegatorPeerID,
			},
		})
		if err != nil {
			return nil, err
		}
	} else {
		err := service.FromFSCCoreService(models.FSCCoreService{
			Name:   grant.ServiceName,
			PeerId: grant.ServicePeerID,
		})
		if err != nil {
			return nil, err
		}
	}

	data := models.FSCCoreGrant_Data{}
	err := data.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
		Outway: models.FSCCoreOutway{
			PeerId:              grant.OutwayPeerID,
			PublicKeyThumbprint: grant.OutwayPublicKeyThumbprint,
		},
		Service: service,
	})

	if err != nil {
		return nil, err
	}

	return &models.FSCCoreGrant{
		Data: data,
	}, nil
}

func convertDelegatedServiceConnectionGrant(grant *query.DelegatedServiceConnectionGrant) (*models.FSCCoreGrant, error) {
	service := models.FSCCoreGrantDelegatedServiceConnection_Service{}

	if grant.ServiceIsDelegatedPublication {
		err := service.FromFSCCoreDelegatedService(models.FSCCoreDelegatedService{
			Name:   grant.ServiceName,
			PeerId: grant.ServicePeerID,
			Delegator: models.FSCCoreDelegator{
				PeerId: grant.ServicePublicationDelegatorPeerID,
			},
		})
		if err != nil {
			return nil, err
		}
	} else {
		err := service.FromFSCCoreService(models.FSCCoreService{
			Name:   grant.ServiceName,
			PeerId: grant.ServicePeerID,
		})
		if err != nil {
			return nil, err
		}
	}

	data := models.FSCCoreGrant_Data{}
	err := data.FromFSCCoreGrantDelegatedServiceConnection(models.FSCCoreGrantDelegatedServiceConnection{
		Delegator: models.FSCCoreDelegator{PeerId: grant.DelegatorPeerID},
		Service:   service,
		Outway: models.FSCCoreOutway{
			PeerId:              grant.OutwayPeerID,
			PublicKeyThumbprint: grant.OutwayPublicKeyThumbprint,
		},
	})

	if err != nil {
		return nil, err
	}

	return &models.FSCCoreGrant{
		Data: data,
	}, nil
}

func convertDelegatedServicePublicationGrant(grant *query.DelegatedServicePublicationGrant) (*models.FSCCoreGrant, error) {
	data := models.FSCCoreGrant_Data{}
	err := data.FromFSCCoreGrantDelegatedServicePublication(models.FSCCoreGrantDelegatedServicePublication{
		Delegator: models.FSCCoreDelegator{PeerId: grant.DelegatorPeerID},
		Service: models.FSCCoreServicePublication{
			PeerId:   grant.ServicePeerID,
			Name:     grant.ServiceName,
			Protocol: mapServiceProtocolToRest(grant.ServiceProtocol),
		},
		Directory: models.FSCCoreDirectory{
			PeerId: grant.DirectoryPeerID,
		},
	})

	if err != nil {
		return nil, err
	}

	return &models.FSCCoreGrant{
		Data: data,
	}, nil
}

func determineNextCursorForContracts(contracts []models.FSCCoreContract) string {
	if len(contracts) == 0 {
		return ""
	}

	return contracts[len(contracts)-1].Content.Iv
}

func convertGrantType(grantType models.FSCCoreGrantType) (query.GrantType, error) {
	switch grantType {
	case models.GRANTTYPESERVICECONNECTION:
		return query.GrantTypeServiceConnection, nil
	case models.GRANTTYPESERVICEPUBLICATION:
		return query.GrantTypeServicePublication, nil
	case models.GRANTTYPEDELEGATEDSERVICECONNECTION:
		return query.GrantTypeDelegatedServiceConnection, nil
	case models.GRANTTYPEDELEGATEDSERVICEPUBLICATION:
		return query.GrantTypeDelegatedServicePublication, nil
	default:
		return query.GrantTypeUnspecified, fmt.Errorf("unknown grant type. %q", grantType)
	}
}

func convertSortOrder(sortOrder models.FSCCoreSortOrder) (query.SortOrder, error) {
	switch sortOrder {
	case models.FSCCoreSortOrderSORTORDERASCENDING:
		return query.SortOrderAscending, nil
	case models.FSCCoreSortOrderSORTORDERDESCENDING:
		return query.SortOrderDescending, nil
	default:
		return "", fmt.Errorf("unknown sort order %q", sortOrder)
	}
}

func mapServiceProtocolToRest(p contract.ServiceProtocol) models.FSCCoreProtocol {
	switch p {
	case contract.ServiceProtocolTCPHTTP1_1:
		return models.PROTOCOLTCPHTTP11
	case contract.ServiceProtocolTCPHTTP2:
		return models.PROTOCOLTCPHTTP2
	default:
		return ""
	}
}
