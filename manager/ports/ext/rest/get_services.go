// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetServices(ctx context.Context, request api.GetServicesRequestObject) (api.GetServicesResponseObject, error) {
	sortOrder := query.SortOrderDescending

	if request.Params.SortOrder != nil {
		switch *request.Params.SortOrder {
		case models.FSCCoreSortOrderSORTORDERASCENDING:
			sortOrder = query.SortOrderAscending
		case models.FSCCoreSortOrderSORTORDERDESCENDING:
			sortOrder = query.SortOrderDescending
		}
	}

	var cursor string
	if request.Params.Cursor != nil {
		cursor = *request.Params.Cursor
	}

	var limit uint32

	if request.Params.Limit != nil {
		limit = *request.Params.Limit
	}

	var peerIDFilter string

	if request.Params.PeerId != nil {
		peerIDFilter = *request.Params.PeerId
	}

	var serviceNameFilter string

	if request.Params.ServiceName != nil {
		serviceNameFilter = *request.Params.ServiceName
	}

	records, err := s.app.Queries.ListServices.Handle(ctx, &query.ListServicesHandlerArgs{
		PaginationStartID:   cursor,
		PaginationLimit:     limit,
		PaginationSortOrder: sortOrder,
		PeerIDFilter:        peerIDFilter,
		ServiceNameFilter:   serviceNameFilter,
	})
	if err != nil {
		s.logger.Error("error executing list services query", err)
		return nil, err
	}

	services := make([]models.FSCCoreServiceListing, len(records))

	for i, serviceRecord := range records {
		data := models.FSCCoreServiceListing_Data{}

		if serviceRecord.DelegatorPeerID == "" {
			err = data.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
				Name: serviceRecord.Name,
				Peer: models.FSCCorePeer{
					Id:             serviceRecord.PeerID,
					Name:           serviceRecord.PeerName,
					ManagerAddress: serviceRecord.PeerManagerAddress,
				},
				Protocol: mapServiceProtocolToRest(serviceRecord.Protocol),
			})
		} else {
			err = data.FromFSCCoreServiceListingDelegatedService(models.FSCCoreServiceListingDelegatedService{
				Name: serviceRecord.Name,
				Peer: models.FSCCorePeer{
					Id:             serviceRecord.PeerID,
					Name:           serviceRecord.PeerName,
					ManagerAddress: serviceRecord.PeerManagerAddress,
				},
				Delegator: models.FSCCoreDelegatorServiceListing{
					PeerId:   serviceRecord.DelegatorPeerID,
					PeerName: serviceRecord.DelegatorPeerName,
				},
				Protocol: mapServiceProtocolToRest(serviceRecord.Protocol),
			})
		}

		if err != nil {
			return nil, err
		}

		services[i] = models.FSCCoreServiceListing{
			Data: data,
		}
	}

	nextCursor := determineNextCursorForServices(records)

	return api.GetServices200JSONResponse{
		Pagination: models.FSCCorePaginationResult{
			NextCursor: &nextCursor,
		},
		Services: services,
	}, nil
}

func determineNextCursorForServices(services []*query.Service) string {
	if len(services) == 0 {
		return ""
	}

	return services[len(services)-1].ContractIV
}
