// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

//nolint:gocyclo // requires a lot of setup due to the different query parameters, splitting this method would result in less readability
func (s *Server) ListPendingContracts(ctx context.Context, req api.ListPendingContractsRequestObject) (api.ListPendingContractsResponseObject, error) {
	s.logger.Info("rest request ListPendingContracts")

	sortOrder := query.SortOrderAscending

	if req.Params.SortOrder != nil && *req.Params.SortOrder == models.SORTORDERDESCENDING {
		sortOrder = query.SortOrderDescending
	}

	limit := uint32(0)

	if req.Params.Limit != nil {
		limit = *req.Params.Limit
	}

	paginationCursorStartDate := time.Time{}
	paginationCursorStartHash := ""

	if req.Params.Cursor != nil {
		startDate, hash, err := decodePaginationCursor(*req.Params.Cursor)
		if err != nil {
			return nil, err
		}

		paginationCursorStartDate = startDate
		paginationCursorStartHash = hash
	}

	includeCount := false
	if req.Params.IncludeCount != nil {
		includeCount = *req.Params.IncludeCount
	}

	res, err := s.app.Queries.ListPendingContracts.Handle(ctx, &query.ListPendingContractsHandlerArgs{
		PaginationStartDate: paginationCursorStartDate,
		PaginationStartHash: paginationCursorStartHash,
		PaginationLimit:     limit,
		PaginationSortOrder: sortOrder,
		IncludeCount:        includeCount,
	})
	if err != nil {
		s.logger.Error("get contracts pending query", err)
		return nil, err
	}

	response := api.ListPendingContracts200JSONResponse{
		PendingContracts: []models.PendingContract{},
		Pagination:       models.PaginationResult{},
	}

	for _, c := range res.PendingContracts {
		var convertedContract *models.PendingContract

		convertedContract, err = convertContractPending(c)
		if err != nil {
			s.logger.Error("get contracts pending converting contract", err)
			return nil, err
		}

		response.PendingContracts = append(response.PendingContracts, *convertedContract)
	}

	if len(res.PendingContracts) == 0 {
		return response, nil
	}

	lastContract := res.PendingContracts[len(res.PendingContracts)-1]
	nextCursor := createNextCursorForContract(lastContract.ValidNotBefore, lastContract.ContentHash)

	response.Pagination.NextCursor = nextCursor

	response.TotalCount = &res.Count

	return response, nil
}

func convertContractPending(c *query.PendingContract) (*models.PendingContract, error) {
	pendingContract := &models.PendingContract{
		ValidNotBefore: c.ValidNotBefore.Unix(),
		Hash:           c.ContentHash,
	}

	pendingContract.Grants = make([]models.Grant, 0, len(c.Grants))

	for _, grants := range c.Grants {
		switch grants.GrantType {
		case query.GrantTypeServicePublication:
			grant, err := convertServicePublicationPendingGrant(grants)
			if err != nil {
				return nil, err
			}

			pendingContract.Grants = append(pendingContract.Grants, *grant)
		case query.GrantTypeDelegatedServicePublication:
			grant, err := convertDelegatedServicePublicationPendingGrant(grants)
			if err != nil {
				return nil, err
			}

			pendingContract.Grants = append(pendingContract.Grants, *grant)
		case query.GrantTypeServiceConnection:
			grant, err := convertServiceConnectionPendingGrant(grants)
			if err != nil {
				return nil, err
			}

			pendingContract.Grants = append(pendingContract.Grants, *grant)
		case query.GrantTypeDelegatedServiceConnection:
			grant, err := convertDelegatedServiceConnectionPendingGrant(grants)
			if err != nil {
				return nil, err
			}

			pendingContract.Grants = append(pendingContract.Grants, *grant)
		}
	}

	return pendingContract, nil
}

func convertServicePublicationPendingGrant(pendingGrant *query.PendingContractGrant) (*models.Grant, error) {
	grant := &models.Grant{}
	err := grant.FromGrantServicePublication(models.GrantServicePublication{
		Directory: models.DirectoryPeer{
			PeerId: pendingGrant.DirectoryPeerID,
		},
		Hash: "",
		Service: models.ServicePublication{
			Name:     pendingGrant.ServiceName,
			PeerId:   pendingGrant.ServicePeerID,
			Protocol: mapServiceProtocolToRest(pendingGrant.ServiceProtocol),
		},
	})

	if err != nil {
		return nil, err
	}

	return grant, nil
}

func convertDelegatedServicePublicationPendingGrant(pendingGrant *query.PendingContractGrant) (*models.Grant, error) {
	grant := &models.Grant{}
	err := grant.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
		Directory: models.DirectoryPeer{
			PeerId: pendingGrant.DirectoryPeerID,
		},
		Delegator: models.DelegatorPeer{
			PeerId: pendingGrant.DelegatorPeerID,
		},
		Hash: "",
		Service: models.ServicePublication{
			Name:     pendingGrant.ServiceName,
			PeerId:   pendingGrant.ServicePeerID,
			Protocol: mapServiceProtocolToRest(pendingGrant.ServiceProtocol),
		},
	})

	if err != nil {
		return nil, err
	}

	return grant, nil
}
func convertServiceConnectionPendingGrant(pendingGrant *query.PendingContractGrant) (*models.Grant, error) {
	var err error

	grant := &models.Grant{}

	s := models.GrantServiceConnection_Service{}
	if pendingGrant.ServicePublicationDelegatorPeerID != "" {
		err = s.FromDelegatedService(models.DelegatedService{
			Delegator: models.Peer{
				Id: pendingGrant.ServicePublicationDelegatorPeerID,
			},
			Name: pendingGrant.ServiceName,
			Peer: models.Peer{
				Id: pendingGrant.ServicePeerID,
			},
		})
		if err != nil {
			return nil, err
		}
	} else {
		err = s.FromService(models.Service{
			Name: pendingGrant.ServiceName,
			Peer: models.Peer{
				Id: pendingGrant.ServicePeerID,
			},
		})
		if err != nil {
			return nil, err
		}
	}

	err = grant.FromGrantServiceConnection(models.GrantServiceConnection{
		Hash: "",
		Outway: models.OutwayPeer{
			PeerId:              pendingGrant.OutwayPeerID,
			PublicKeyThumbprint: pendingGrant.OutwayThumbprint,
		},
		Service: s,
	})
	if err != nil {
		return nil, err
	}

	return grant, nil
}

func convertDelegatedServiceConnectionPendingGrant(pendingGrant *query.PendingContractGrant) (*models.Grant, error) {
	var err error

	grant := &models.Grant{}

	s := models.GrantDelegatedServiceConnection_Service{}
	if pendingGrant.ServicePublicationDelegatorPeerID != "" {
		err = s.FromDelegatedService(models.DelegatedService{
			Delegator: models.Peer{
				Id: pendingGrant.ServicePublicationDelegatorPeerID,
			},
			Name: pendingGrant.ServiceName,
			Peer: models.Peer{
				Id: pendingGrant.ServicePeerID,
			},
		})
		if err != nil {
			return nil, err
		}
	} else {
		err = s.FromService(models.Service{
			Name: pendingGrant.ServiceName,
			Peer: models.Peer{
				Id: pendingGrant.ServicePeerID,
			},
		})
		if err != nil {
			return nil, err
		}
	}

	err = grant.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
		Hash: "",
		Delegator: models.DelegatorPeer{
			PeerId: pendingGrant.DelegatorPeerID,
		},
		Outway: models.OutwayPeer{
			PeerId:              pendingGrant.OutwayPeerID,
			PublicKeyThumbprint: pendingGrant.OutwayThumbprint,
		},
		Service: s,
	})
	if err != nil {
		return nil, err
	}

	return grant, nil
}
