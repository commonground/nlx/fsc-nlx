// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

//nolint:gocyclo // requires a lot of setup due to the different query parameters, splitting this method would result in less readability
func (s *Server) ListIncomingConnections(ctx context.Context, req api.ListIncomingConnectionsRequestObject) (api.ListIncomingConnectionsResponseObject, error) {
	s.logger.Info("rest request ListIncomingConnections")

	sortOrder := query.SortOrderDescending

	if req.Params.SortOrder != nil && *req.Params.SortOrder == models.SORTORDERASCENDING {
		sortOrder = query.SortOrderAscending
	}

	const defaultPaginationLimit = 25
	limit := uint32(defaultPaginationLimit)

	if req.Params.Limit != nil {
		limit = *req.Params.Limit
	}

	paginationCursorCreatedAt := int64(0)
	paginationCursorStartHash := ""

	if req.Params.Cursor != nil {
		pagination, err := decodePaginationCursorForIncomingConnections(req.Params.Cursor)
		if err != nil {
			return nil, err
		}

		paginationCursorCreatedAt = pagination.CreatedAt
		paginationCursorStartHash = pagination.GrantHash
	}

	includeCount := false
	if req.Params.IncludeCount != nil {
		includeCount = *req.Params.IncludeCount
	}

	contractStatesFilter := []contract.ContractState{}

	if req.Params.ContractStates != nil {
		for _, state := range *req.Params.ContractStates {
			if state == "" {
				continue
			}

			domainState, err := convertAPIStateToDomainModel(state)
			if err != nil {
				return nil, fmt.Errorf("invalid contract state filter %q", state)
			}

			contractStatesFilter = append(contractStatesFilter, domainState)
		}
	}

	serviceNames := make([]string, 0)

	if req.Params.ServiceNames != nil {
		for _, s := range *req.Params.ServiceNames {
			if s == "" {
				continue
			}

			serviceNames = append(serviceNames, s)
		}
	}

	res, err := s.app.Queries.ListIncomingConnections.Handle(ctx, &query.ListIncomingConnectionsHandlerArgs{
		PaginationStartCreatedAt: paginationCursorCreatedAt,
		PaginationStartHash:      paginationCursorStartHash,
		PaginationLimit:          limit,
		PaginationSortOrder:      sortOrder,
		IncludeCount:             includeCount,
		ContractStates:           contractStatesFilter,
		ServiceNames:             serviceNames,
	})
	if err != nil {
		s.logger.Error("list incoming connections query", err)
		return nil, err
	}

	response := api.ListIncomingConnections200JSONResponse{
		Connections: make([]models.Connection, len(res.Connections)),
		Pagination:  models.PaginationResult{},
	}

	if len(res.Connections) == 0 {
		return response, nil
	}

	for i, c := range res.Connections {
		response.Connections[i] = convertConnection(c)
	}

	response.TotalCount = &res.Count

	lastConnection := res.Connections[len(res.Connections)-1]

	nextCursor, err := createNextCursorForIncomingConnection(lastConnection.CreatedAt.Unix(), lastConnection.GrantHash)
	if err != nil {
		s.logger.Error("could not create cursor for next incoming connection page", err)

		return nil, err
	}

	response.Pagination.NextCursor = nextCursor

	return response, nil
}

type incomingConnectionspaginationCursor struct {
	CreatedAt int64  `json:"created_at"`
	GrantHash string `json:"grant_hash"`
}

// nolint:dupl // duplicated to decouple pagination per page
func decodePaginationCursorForIncomingConnections(input *models.QueryPaginationCursor) (*incomingConnectionspaginationCursor, error) {
	if input == nil {
		return &incomingConnectionspaginationCursor{
			CreatedAt: 0,
			GrantHash: "",
		}, nil
	}

	decodedCursor, err := base64.RawStdEncoding.DecodeString(*input)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not decode cursor from base64 string: %s", *input))
	}

	var cursor incomingConnectionspaginationCursor

	err = json.Unmarshal(decodedCursor, &cursor)
	if err != nil {
		return nil, errors.Join(err, errors.New("cursor does not contain required format"))
	}

	if cursor.GrantHash == "" {
		return nil, fmt.Errorf("invalid cursor, missing GrantHash: %v", err)
	}

	if cursor.CreatedAt == 0 {
		return nil, fmt.Errorf("invalid cursor, missing CreatedAt: %v", err)
	}

	return &cursor, nil
}

func createNextCursorForIncomingConnection(createdAt int64, grantHash string) (string, error) {
	jsonCursor, err := json.Marshal(incomingConnectionspaginationCursor{
		CreatedAt: createdAt,
		GrantHash: grantHash,
	})

	if err != nil {
		return "", err
	}

	return base64.RawURLEncoding.EncodeToString(jsonCursor), nil
}
