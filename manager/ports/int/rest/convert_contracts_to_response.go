// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func convertContract(c *contract.Contract, selfPeerID contract.PeerID, now time.Time) (*models.Contract, error) {
	grants, err := convertContractGrants(c)
	if err != nil {
		return nil, err
	}

	content := models.ContractContent{
		Iv:            c.Content().IV().String(),
		CreatedAt:     c.Content().CreatedAt().Unix(),
		Grants:        grants,
		GroupId:       c.Content().GroupID(),
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Validity: models.Validity{
			NotAfter:  c.Content().NotAfter().Unix(),
			NotBefore: c.Content().NotBefore().Unix(),
		},
	}

	rejectSignatures := models.SignatureMap{}

	for _, s := range c.SignaturesRejected() {
		rejectSignatures[s.Peer().ID().Value()] = models.Signature{
			Peer: models.Peer{
				Id:   s.Peer().ID().Value(),
				Name: s.Peer().Name().Value(),
			},
			SignedAt: s.SignedAt().Unix(),
		}
	}

	acceptSignatures := models.SignatureMap{}

	for _, s := range c.SignaturesAccepted() {
		acceptSignatures[s.Peer().ID().Value()] = models.Signature{
			Peer: models.Peer{
				Id:   s.Peer().ID().Value(),
				Name: s.Peer().Name().Value(),
			},
			SignedAt: s.SignedAt().Unix(),
		}
	}

	revokeSignatures := models.SignatureMap{}

	for _, s := range c.SignaturesRevoked() {
		revokeSignatures[s.Peer().ID().Value()] = models.Signature{
			Peer: models.Peer{
				Id:   s.Peer().ID().Value(),
				Name: s.Peer().Name().Value(),
			},
			SignedAt: s.SignedAt().Unix(),
		}
	}

	return &models.Contract{
		Hash:        c.Content().Hash().String(),
		Content:     content,
		Peers:       c.Content().PeersIDs().Value(),
		HasAccepted: c.IsAcceptedBy(selfPeerID),
		HasRejected: c.IsRejectedBy(selfPeerID),
		HasRevoked:  c.IsRevokedBy(selfPeerID),
		Signatures: models.Signatures{
			Accept: acceptSignatures,
			Reject: rejectSignatures,
			Revoke: revokeSignatures,
		},
		State: mapContractState(c.State(now)),
	}, nil
}

func convertContractGrants(c *contract.Contract) ([]models.Grant, error) {
	grants := make([]models.Grant, 0)

	for _, g := range c.Content().Grants().ServiceConnectionGrants() {
		grant, err := convertContractServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range c.Content().Grants().ServicePublicationGrants() {
		grant, err := convertContractServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range c.Content().Grants().DelegatedServiceConnectionGrants() {
		grant, err := convertContractDelegatedServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range c.Content().Grants().DelegatedServicePublicationGrants() {
		grant, err := convertContractDelegatedServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	return grants, nil
}

func convertContractServicePublicationGrant(grant *contract.GrantServicePublication) (*models.Grant, error) {
	data := models.Grant{}

	err := data.FromGrantServicePublication(models.GrantServicePublication{
		Hash: grant.Hash().String(),
		Directory: models.DirectoryPeer{
			PeerId: grant.Directory().Peer().ID().Value(),
		},
		Service: models.ServicePublication{
			Name:     grant.Service().Name(),
			PeerId:   grant.Service().Peer().ID().Value(),
			Protocol: mapServiceProtocolToRest(grant.Service().Protocol()),
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertContractServiceConnectionGrant(grant *contract.GrantServiceConnection) (*models.Grant, error) {
	service := models.GrantServiceConnection_Service{}

	switch s := grant.Service().(type) {
	case *contract.GrantServiceConnectionService:
		err := service.FromService(models.Service{
			Name: s.Name(),
			Peer: models.Peer{
				Id:   s.Peer().ID().Value(),
				Name: s.Peer().Name().Value(),
			},
		})
		if err != nil {
			return nil, err
		}
	case *contract.GrantServiceConnectionDelegatedService:
		err := service.FromDelegatedService(models.DelegatedService{
			Delegator: models.Peer{
				Id:   s.PublicationDelegator().ID().Value(),
				Name: s.PublicationDelegator().Name().Value(),
			},
			Name: s.Name(),
			Peer: models.Peer{
				Id:   s.Peer().ID().Value(),
				Name: s.Peer().Name().Value(),
			},
		})
		if err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("unknown service type '%T'", s)
	}

	data := models.Grant{}

	err := data.FromGrantServiceConnection(models.GrantServiceConnection{
		Hash: grant.Hash().String(),
		Outway: models.OutwayPeer{
			PeerId:              grant.Outway().Peer().ID().Value(),
			PublicKeyThumbprint: grant.Outway().PublicKeyThumbprint().Value(),
		},
		Service: service,
	})
	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertContractDelegatedServiceConnectionGrant(grant *contract.GrantDelegatedServiceConnection) (*models.Grant, error) {
	service := models.GrantDelegatedServiceConnection_Service{}

	switch s := grant.Service().(type) {
	case *contract.GrantDelegatedServiceConnectionService:
		err := service.FromService(models.Service{
			Name: s.Name(),
			Peer: models.Peer{
				Id: s.Peer().ID().Value(),
			},
		})
		if err != nil {
			return nil, err
		}
	case *contract.GrantDelegatedServiceConnectionDelegatedService:
		err := service.FromDelegatedService(models.DelegatedService{
			Delegator: models.Peer{
				Id: s.PublicationDelegator().ID().Value(),
			},
			Name: s.Name(),
			Peer: models.Peer{
				Id: s.Peer().ID().Value(),
			},
		})
		if err != nil {
			return nil, err
		}
	}

	data := models.Grant{}

	err := data.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
		Hash: grant.Hash().String(),
		Delegator: models.DelegatorPeer{
			PeerId: grant.Delegator().Peer().ID().Value(),
		},
		Service: service,
		Outway: models.OutwayPeer{
			PeerId:              grant.Outway().Peer().ID().Value(),
			PublicKeyThumbprint: grant.Outway().PublicKeyThumbprint().Value(),
		},
	})
	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertContractDelegatedServicePublicationGrant(grant *contract.GrantDelegatedServicePublication) (*models.Grant, error) {
	data := models.Grant{}

	err := data.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
		Hash: grant.Hash().String(),
		Delegator: models.DelegatorPeer{
			PeerId: grant.Delegator().Peer().ID().Value(),
		},
		Service: models.ServicePublication{
			Name:     grant.Service().Name(),
			PeerId:   grant.Service().Peer().ID().Value(),
			Protocol: mapServiceProtocolToRest(grant.Service().Protocol()),
		},
		Directory: models.DirectoryPeer{
			PeerId: grant.Directory().Peer().ID().Value(),
		},
	})
	if err != nil {
		return nil, err
	}

	return &data, nil
}
