// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) SyncPeers(ctx context.Context, _ api.SyncPeersRequestObject) (api.SyncPeersResponseObject, error) {
	s.logger.Info("rest request SyncPeers")

	res, err := s.app.Commands.SyncPeers.Handle(ctx)
	if err != nil {
		s.logger.Error("error syncing peers", err)
		return nil, err
	}

	result := make([]models.PeerSynchronizationResult, 0, len(res))

	for peerID, syncResult := range res {
		var errorMessage *string

		if syncResult.Error != nil {
			errorMsg := syncResult.Error.Error()
			errorMessage = &errorMsg
		}

		result = append(result, models.PeerSynchronizationResult{
			Ok:     syncResult.Ok,
			PeerId: peerID.Value(),
			Error:  errorMessage,
		})
	}

	return api.SyncPeers200JSONResponse{
		Result: result,
	}, nil
}
