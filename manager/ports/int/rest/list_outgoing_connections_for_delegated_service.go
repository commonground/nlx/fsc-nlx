// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) ListOutgoingConnectionsForDelegatedService(ctx context.Context, req api.ListOutgoingConnectionsForDelegatedServiceRequestObject) (api.ListOutgoingConnectionsForDelegatedServiceResponseObject, error) {
	s.logger.Info("rest request ListOutgoingConnectionsForDelegatedService")

	sortOrder := query.SortOrderDescending

	if req.Params.SortOrder != nil && *req.Params.SortOrder == models.SORTORDERASCENDING {
		sortOrder = query.SortOrderAscending
	}

	res, err := s.app.Queries.ListOutgoingConnectionsForService.Handle(ctx, &query.ListOutgoingConnectionsForServiceHandlerArgs{
		PaginationSortOrder:               sortOrder,
		ServiceName:                       req.ServiceName,
		ServicePeerID:                     contract.PeerID(req.PeerID),
		ServicePublicationDelegatorPeerID: contract.PeerID(req.DelegatorPeerID),
	})
	if err != nil {
		s.logger.Error("list outgoing connections for service query", err)
		return nil, err
	}

	response := api.ListOutgoingConnectionsForDelegatedService200JSONResponse{
		Connections: make([]models.Connection, len(res)),
	}

	for i, c := range res {
		response.Connections[i] = convertConnection(c)
	}

	return response, nil
}
