// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) UpdatePeer(ctx context.Context, req api.UpdatePeerRequestObject) (api.UpdatePeerResponseObject, error) {
	s.logger.Info("rest request UpdatePeer")

	roles := make([]command.PeerRole, 0)

	for _, role := range req.Body.Peer.Roles {
		switch role {
		case models.PEERROLEDIRECTORY:
			roles = append(roles, command.PeerRoleDirectory)
		default:
			return nil, internalapp_errors.NewIncorrectInputError(fmt.Errorf("unknown peer role %q. valid roles: %s", role, models.PEERROLEDIRECTORY))
		}
	}

	err := s.app.Commands.CreatePeer.Handle(ctx, &command.CreatePeerArgs{
		PeerID:                req.Body.Peer.Id,
		PeerName:              req.Body.Peer.Name,
		ManagerAddress:        req.Body.Peer.ManagerAddress,
		Roles:                 roles,
		AuditLogSource:        getSourceFromCtx(ctx),
		AuthData:              getAuthDataFromCtx(ctx),
		AuditlogCorrelationID: getCorrelationIDFromCtx(ctx),
	})
	if err != nil {
		s.logger.Error("error executing update peer command", err)
		return nil, err
	}

	return api.UpdatePeer204Response{}, nil
}
