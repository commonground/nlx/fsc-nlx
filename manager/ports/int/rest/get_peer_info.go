// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetPeerInfo(ctx context.Context, _ api.GetPeerInfoRequestObject) (api.GetPeerInfoResponseObject, error) {
	s.logger.Info("rest request GetPeerInfo")

	peer := s.app.Queries.GetPeerInfo.Handle(ctx)

	return api.GetPeerInfo200JSONResponse{
		Peer: models.Peer{
			Id:   peer.ID,
			Name: peer.Name,
		},
	}, nil
}
