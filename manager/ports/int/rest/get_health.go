// copyright © vng realisatie 2024
// licensed under the eupl

package rest

import (
	"context"

	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetHealth(ctx context.Context, _ api.GetHealthRequestObject) (api.GetHealthResponseObject, error) {
	err := s.app.Queries.GetHealth.Handle(ctx)
	if err != nil {
		s.logger.Error("health check failed", err)

		return api.GetHealth503Response{}, nil
	}

	return api.GetHealth200Response{}, nil
}
