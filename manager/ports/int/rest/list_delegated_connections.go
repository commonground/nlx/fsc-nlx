// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

//nolint:gocyclo // requires a lot of setup due to the different query parameters, splitting this method would result in less readability
func (s *Server) ListDelegatedConnections(ctx context.Context, req api.ListDelegatedConnectionsRequestObject) (api.ListDelegatedConnectionsResponseObject, error) {
	s.logger.Info("rest request ListDelegatedConnections")

	sortOrder := query.SortOrderDescending

	if req.Params.SortOrder != nil && *req.Params.SortOrder == models.SORTORDERASCENDING {
		sortOrder = query.SortOrderAscending
	}

	const defaultPaginationLimit = 25
	limit := uint32(defaultPaginationLimit)

	if req.Params.Limit != nil {
		limit = *req.Params.Limit
	}

	paginationCursorCreatedAt := int64(0)
	paginationCursorStartHash := ""

	if req.Params.Cursor != nil {
		pagination, err := decodePaginationCursorForDelegatedConnection(req.Params.Cursor)
		if err != nil {
			return nil, err
		}

		paginationCursorCreatedAt = pagination.CreatedAt
		paginationCursorStartHash = pagination.GrantHash
	}

	includeCount := false
	if req.Params.IncludeCount != nil {
		includeCount = *req.Params.IncludeCount
	}

	contractStatesFilter := []contract.ContractState{}

	if req.Params.ContractStates != nil {
		for _, state := range *req.Params.ContractStates {
			if state == "" {
				continue
			}

			domainState, err := convertAPIStateToDomainModel(state)
			if err != nil {
				return nil, fmt.Errorf("invalid contract state filter %q", state)
			}

			contractStatesFilter = append(contractStatesFilter, domainState)
		}
	}

	res, err := s.app.Queries.ListDelegatedConnections.Handle(ctx, &query.ListDelegatedConnectionsHandlerArgs{
		PaginationStartCreatedAt: paginationCursorCreatedAt,
		PaginationStartHash:      paginationCursorStartHash,
		PaginationLimit:          limit,
		PaginationSortOrder:      sortOrder,
		IncludeCount:             includeCount,
		ContractStates:           contractStatesFilter,
	})
	if err != nil {
		s.logger.Error("list delegated connections query", err)
		return nil, err
	}

	response := api.ListDelegatedConnections200JSONResponse{
		Connections: make([]models.Connection, len(res.DelegatedConnections)),
		Pagination:  models.PaginationResult{},
	}

	for i, c := range res.DelegatedConnections {
		response.Connections[i] = convertConnection(c)
	}

	if len(res.DelegatedConnections) == 0 {
		return response, nil
	}

	response.TotalCount = &res.Count

	lastConnection := res.DelegatedConnections[len(res.DelegatedConnections)-1]

	nextCursor, err := createNextCursorForDelegatedConnection(lastConnection.CreatedAt.Unix(), lastConnection.GrantHash)
	if err != nil {
		s.logger.Error("could not create cursor for next delegated connection page", err)

		return nil, err
	}

	response.Pagination.NextCursor = nextCursor

	return response, nil
}

type delegatedConnectionsaginationCursor struct {
	CreatedAt int64  `json:"created_at"`
	GrantHash string `json:"grant_hash"`
}

// nolint:dupl // duplicated to decouple pagination per page
func createNextCursorForDelegatedConnection(createdAt int64, grantHash string) (string, error) {
	jsonCursor, err := json.Marshal(delegatedConnectionsaginationCursor{
		CreatedAt: createdAt,
		GrantHash: grantHash,
	})

	if err != nil {
		return "", err
	}

	return base64.RawURLEncoding.EncodeToString(jsonCursor), nil
}

// nolint:dupl // duplicated to decouple pagination per page
func decodePaginationCursorForDelegatedConnection(input *models.QueryPaginationCursor) (*delegatedConnectionsaginationCursor, error) {
	if input == nil {
		return &delegatedConnectionsaginationCursor{
			CreatedAt: 0,
			GrantHash: "",
		}, nil
	}

	decodedCursor, err := base64.RawStdEncoding.DecodeString(*input)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not decode cursor from base64 string: %s", *input))
	}

	var cursor delegatedConnectionsaginationCursor

	err = json.Unmarshal(decodedCursor, &cursor)
	if err != nil {
		return nil, errors.Join(err, errors.New("cursor does not contain required format"))
	}

	if cursor.GrantHash == "" {
		return nil, fmt.Errorf("invalid cursor, missing GrantHash: %v", err)
	}

	if cursor.CreatedAt == 0 {
		return nil, fmt.Errorf("invalid cursor, missing CreatedAt: %v", err)
	}

	return &cursor, nil
}
