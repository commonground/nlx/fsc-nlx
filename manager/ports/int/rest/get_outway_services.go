// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetOutwayServices(ctx context.Context, req api.GetOutwayServicesRequestObject) (api.GetOutwayServicesResponseObject, error) {
	s.logger.Info("rest request GetServicesForOutway")

	res, err := s.app.Queries.GetServicesForOutway.Handle(ctx, req.OutwayPublicKeyThumbprint)
	if err != nil {
		return nil, err
	}

	services := make(models.OutwayServices)
	for grantHash, service := range res {
		services[grantHash] = models.OutwayService{
			PeerId: service.PeerID,
			Name:   service.Name,
		}
	}

	return api.GetOutwayServices200JSONResponse{Services: services}, nil
}
