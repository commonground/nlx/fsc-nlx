// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) ListPeers(ctx context.Context, req api.ListPeersRequestObject) (api.ListPeersResponseObject, error) {
	s.logger.Info("rest request ListPeers")

	res, err := s.app.Queries.ListPeers.Handle(ctx, req.Params.PeerIds)
	if err != nil {
		s.logger.Error("error listing peers from query", err)
		return nil, err
	}

	peers := make([]models.Peer, 0, len(res))

	for _, p := range res {
		roles := make([]models.PeerRole, 0, len(p.Roles))

		for _, role := range p.Roles {
			switch role {
			case query.PeerRoleDirectory:
				roles = append(roles, models.PEERROLEDIRECTORY)

			default:
				err := fmt.Errorf("unknown peer role: %s", role)

				s.logger.Error("invalid peer role", err)

				return nil, err
			}
		}

		peers = append(peers, models.Peer{
			Id:             p.ID,
			Name:           p.Name,
			ManagerAddress: p.ManagerAddress,
			Roles:          roles,
		})
	}

	return api.ListPeers200JSONResponse{
		Peers: peers,
	}, nil
}
