// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) CreateContract(ctx context.Context, req api.CreateContractRequestObject) (api.CreateContractResponseObject, error) {
	s.logger.Info("rest request CreateContract")

	args, err := contractContentToArgs(&req.Body.ContractContent)
	if err != nil {
		return nil, fmt.Errorf("invalid args in body: %w", err)
	}

	args.AuditLogSource = getSourceFromCtx(ctx)
	args.AuthData = getAuthDataFromCtx(ctx)
	args.AuditlogCorrelationID = getCorrelationIDFromCtx(ctx)

	contentHash, err := s.app.Commands.CreateContract.Handle(ctx, args)
	if err != nil {
		s.logger.Error("error executing create contract command", err)

		return nil, err
	}

	return api.CreateContract201JSONResponse{
		ContentHash: contentHash,
	}, nil
}

func contractContentToArgs(content *models.ContractContent) (*command.CreateContractHandlerArgs, error) {
	grants, err := mapRestToCommandGrants(content.Grants)
	if err != nil {
		return nil, fmt.Errorf("could not map grants: %w", err)
	}

	return &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(content.HashAlgorithm),
		IV:                content.Iv,
		GroupID:           content.GroupId,
		ContractNotBefore: time.Unix(content.Validity.NotBefore, 0),
		ContractNotAfter:  time.Unix(content.Validity.NotAfter, 0),
		Grants:            grants,
		CreatedAt:         time.Unix(content.CreatedAt, 0),
	}, nil
}

//nolint:gocyclo // difficult to improve complexity without effecting the readability
func mapRestToCommandGrants(modelGrants []models.Grant) ([]interface{}, error) {
	grants := make([]interface{}, 0)

	for _, grant := range modelGrants {
		discriminator, err := grant.ValueByDiscriminator()
		if err != nil {
			return nil, err
		}

		switch convertedModel := discriminator.(type) {
		case models.GrantServicePublication:
			p, errMapServiceProtocol := mapServiceProtocolToContract(convertedModel.Service.Protocol)
			if errMapServiceProtocol != nil {
				return nil, fmt.Errorf("invalid protocol in request: %w", errMapServiceProtocol)
			}

			grants = append(grants, &command.GrantServicePublicationArgs{
				DirectoryPeerID: convertedModel.Directory.PeerId,
				ServicePeerID:   convertedModel.Service.PeerId,
				ServiceName:     convertedModel.Service.Name,
				ServiceProtocol: p,
			})
		case models.GrantServiceConnection:
			serviceDiscriminator, errServiceDiscriminator := convertedModel.Service.Discriminator()
			if errServiceDiscriminator != nil {
				return nil, errServiceDiscriminator
			}

			var service interface{}

			//nolint:dupl // looks the same but is a different grant
			switch serviceDiscriminator {
			case string(models.SERVICETYPESERVICE):
				s, errService := convertedModel.Service.AsService()
				if errService != nil {
					return nil, fmt.Errorf("invalid service in request: %w", errService)
				}

				service = &command.NewServiceArgs{
					Name:   s.Name,
					PeerID: s.Peer.Id,
				}
			case string(models.SERVICETYPEDELEGATEDSERVICE):
				delegatedService, errService := convertedModel.Service.AsDelegatedService()
				if errService != nil {
					return nil, fmt.Errorf("invalid delegated service in request: %w", errService)
				}

				service = &command.NewDelegatedServiceArgs{
					Name:                       delegatedService.Name,
					PeerID:                     delegatedService.Peer.Id,
					PublicationDelegatorPeerID: delegatedService.Delegator.Id,
				}
			default:
				return nil, fmt.Errorf("invalid service type: %s", serviceDiscriminator)
			}

			grants = append(grants, &command.GrantServiceConnectionArgs{
				OutwayPeerID:              convertedModel.Outway.PeerId,
				OutwayPublicKeyThumbprint: convertedModel.Outway.PublicKeyThumbprint,
				Service:                   service,
			})
		case models.GrantDelegatedServiceConnection:
			serviceDiscriminator, errServiceDiscriminator := convertedModel.Service.Discriminator()
			if errServiceDiscriminator != nil {
				return nil, errServiceDiscriminator
			}

			var service interface{}

			//nolint:dupl // looks the same but is a different grant
			switch serviceDiscriminator {
			case string(models.SERVICETYPESERVICE):
				s, errService := convertedModel.Service.AsService()
				if errService != nil {
					return nil, fmt.Errorf("invalid service in request: %w", errService)
				}

				service = &command.NewServiceArgs{
					Name:   s.Name,
					PeerID: s.Peer.Id,
				}
			case string(models.SERVICETYPEDELEGATEDSERVICE):
				delegatedService, errService := convertedModel.Service.AsDelegatedService()
				if errService != nil {
					return nil, fmt.Errorf("invalid delegated service in request: %w", errService)
				}

				service = &command.NewDelegatedServiceArgs{
					Name:                       delegatedService.Name,
					PeerID:                     delegatedService.Peer.Id,
					PublicationDelegatorPeerID: delegatedService.Delegator.Id,
				}
			default:
				return nil, fmt.Errorf("invalid service type: %s", serviceDiscriminator)
			}

			grants = append(grants, &command.GrantDelegatedServiceConnectionArgs{
				OutwayPublicKeyThumbprint: convertedModel.Outway.PublicKeyThumbprint,
				OutwayPeerID:              convertedModel.Outway.PeerId,
				Service:                   service,
				DelegatorPeerID:           convertedModel.Delegator.PeerId,
			})
		case models.GrantDelegatedServicePublication:
			p, errMapProtocol := mapServiceProtocolToContract(convertedModel.Service.Protocol)
			if errMapProtocol != nil {
				return nil, fmt.Errorf("invalid protocol in request: %w", errMapProtocol)
			}

			grants = append(grants, &command.GrantDelegatedServicePublicationArgs{
				DirectoryPeerID: convertedModel.Directory.PeerId,
				ServicePeerID:   convertedModel.Service.PeerId,
				ServiceName:     convertedModel.Service.Name,
				ServiceProtocol: p,
				DelegatorPeerID: convertedModel.Delegator.PeerId,
			})
		default:
			return nil, fmt.Errorf("invalid grant type: %T", convertedModel)
		}
	}

	return grants, nil
}

func mapServiceProtocolToContract(p models.Protocol) (contract.ServiceProtocol, error) {
	switch p {
	case models.PROTOCOLTCPHTTP11:
		return contract.ServiceProtocolTCPHTTP1_1, nil
	case models.PROTOCOLTCPHTTP2:
		return contract.ServiceProtocolTCPHTTP2, nil
	default:
		return contract.ServiceProtocolUnspecified, fmt.Errorf("unknown protocol: %s", p)
	}
}
