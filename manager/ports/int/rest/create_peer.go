// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) CreatePeer(ctx context.Context, req api.CreatePeerRequestObject) (api.CreatePeerResponseObject, error) {
	s.logger.Info("rest request CreatePeer")

	auditLogCorrelationID := ""

	if req.Params.AuditlogCorrelationId != nil {
		auditLogCorrelationID = *req.Params.AuditlogCorrelationId
	}

	roles := make([]command.PeerRole, 0)

	for _, role := range req.Body.Peer.Roles {
		switch role {
		case models.PEERROLEDIRECTORY:
			roles = append(roles, command.PeerRoleDirectory)
		default:
			err := fmt.Errorf("unknown peer role: %s", role)

			s.logger.Error("invalid peer role", err)

			return nil, err
		}
	}

	err := s.app.Commands.CreatePeer.Handle(ctx, &command.CreatePeerArgs{
		PeerID:                req.Body.Peer.Id,
		PeerName:              req.Body.Peer.Name,
		ManagerAddress:        req.Body.Peer.ManagerAddress,
		Roles:                 roles,
		AuditLogSource:        getSourceFromCtx(ctx),
		AuthData:              getAuthDataFromCtx(ctx),
		AuditlogCorrelationID: auditLogCorrelationID,
	})
	if err != nil {
		s.logger.Error("error executing create peer command", err)
		return nil, err
	}

	return &api.CreatePeer201Response{}, nil
}
