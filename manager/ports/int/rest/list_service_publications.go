// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

//nolint:gocyclo,funlen // requires a lot of setup due to the different query parameters, splitting this method would result in less readability
func (s *Server) ListServicePublications(ctx context.Context, req api.ListServicePublicationsRequestObject) (api.ListServicePublicationsResponseObject, error) {
	s.logger.Info("rest request ListServicePublications")

	sortOrder := query.SortOrderDescending

	if req.Params.SortOrder != nil && *req.Params.SortOrder == models.SORTORDERASCENDING {
		sortOrder = query.SortOrderAscending
	}

	const defaultPaginationLimit = 25
	limit := uint32(defaultPaginationLimit)

	if req.Params.Limit != nil {
		limit = *req.Params.Limit
	}

	includeCount := false
	if req.Params.IncludeCount != nil {
		includeCount = *req.Params.IncludeCount
	}

	serviceNames := make([]string, 0)

	if req.Params.ServiceNames != nil {
		for _, s := range *req.Params.ServiceNames {
			if s == "" {
				continue
			}

			serviceNames = append(serviceNames, s)
		}
	}

	contractStatesFilter := []contract.ContractState{}

	if req.Params.ContractStates != nil {
		for _, state := range *req.Params.ContractStates {
			if state == "" {
				continue
			}

			domainState, err := convertAPIStateToDomainModel(state)
			if err != nil {
				return nil, fmt.Errorf("invalid contract state filter %q", state)
			}

			contractStatesFilter = append(contractStatesFilter, domainState)
		}
	}

	cursor, err := decodePaginationCursorForServicePublication(req.Params.Cursor)
	if err != nil {
		s.logger.Error("invalid cursor used in get service publications", err)
		return nil, err
	}

	res, err := s.app.Queries.ListServicePublications.Handle(ctx, &query.ListServicePublicationsHandlerArgs{
		PaginationCreatedAt: cursor.CreatedAt,
		PaginationGrantHash: cursor.GrantHash,
		PaginationLimit:     limit,
		PaginationSortOrder: sortOrder,
		IncludeCount:        includeCount,
		ContractStates:      contractStatesFilter,
		ServiceNames:        serviceNames,
	})
	if err != nil {
		s.logger.Error("list service publications", err)
		return nil, err
	}

	publications := make([]models.Publication, len(res.Publications))

	for i, servicePublication := range res.Publications {
		createdAt := servicePublication.CreatedAt.Unix()
		notValidBefore := servicePublication.ValidNotBefore.Unix()
		notValidAfter := servicePublication.ValidNotAfter.Unix()
		p := models.Publication{
			ContractHash:    servicePublication.ContentHash,
			CreatedAt:       createdAt,
			DirectoryPeerId: servicePublication.DirectoryPeerID,
			GrantHash:       servicePublication.GrantHash,
			ServicePeerId:   servicePublication.ServicePeerID,
			ServiceName:     servicePublication.ServiceName,
			NotAfter:        notValidAfter,
			NotBefore:       notValidBefore,
		}

		if servicePublication.DelegatorPeerID != "" {
			p.DelegatorPeerId = &servicePublication.DelegatorPeerID
		}

		state, err := convertState(servicePublication.ContractState)
		if err == nil {
			p.State = &state
		}

		publications[i] = p
	}

	response := api.ListServicePublications200JSONResponse{
		Publications: publications,
		Pagination:   models.PaginationResult{},
	}

	if len(res.Publications) == 0 {
		return response, nil
	}

	response.TotalCount = &res.Count

	lastContract := res.Publications[len(res.Publications)-1]

	nextCursor, err := createNextCursorForServicePublication(lastContract.CreatedAt.Unix(), lastContract.GrantHash)
	if err != nil {
		s.logger.Error("could not create cursor", err)
		return nil, err
	}

	response.Pagination.NextCursor = nextCursor

	return response, nil
}

type servicePublicationsPaginationCursor struct {
	CreatedAt int64  `json:"created_at"`
	GrantHash string `json:"grant_hash"`
}

// nolint:dupl // similar but not the same
func decodePaginationCursorForServicePublication(input *models.QueryPaginationCursor) (*servicePublicationsPaginationCursor, error) {
	if input == nil {
		return &servicePublicationsPaginationCursor{
			CreatedAt: 0,
			GrantHash: "",
		}, nil
	}

	decodedCursor, err := base64.RawURLEncoding.DecodeString(*input)
	if err != nil {
		return nil, errors.Wrapf(err, "could not decode cursor from base64 string: %s", *input)
	}

	var cursor servicePublicationsPaginationCursor

	err = json.Unmarshal(decodedCursor, &cursor)
	if err != nil {
		return nil, errors.Wrap(err, "cursor does not contain required format")
	}

	if cursor.GrantHash == "" {
		return nil, errors.New(fmt.Sprintf("invalid cursor, missing GrantHash: %v", err))
	}

	if cursor.CreatedAt == 0 {
		return nil, errors.New(fmt.Sprintf("invalid cursor, missing CreatedAt: %v", err))
	}

	return &cursor, nil
}

func createNextCursorForServicePublication(createdAt int64, grantHash string) (string, error) {
	jsonCursor, err := json.Marshal(servicePublicationsPaginationCursor{
		CreatedAt: createdAt,
		GrantHash: grantHash,
	})

	if err != nil {
		return "", err
	}

	return base64.RawURLEncoding.EncodeToString(jsonCursor), nil
}
