// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) ListServices(ctx context.Context, req api.ListServicesRequestObject) (api.ListServicesResponseObject, error) {
	s.logger.Info("rest request ListServices")

	res, err := s.app.Queries.ListServices.Handle(ctx, &query.ListServicesArgs{
		DirectoryPeerID:             req.DirectoryPeerId,
		ServiceProviderPeerIDFilter: req.Params.PeerId,
	})
	if err != nil {
		s.logger.Error("get services query failed", err)
		return nil, err
	}

	services := make(models.Services, 0, len(res))

	for _, svc := range res {
		data := models.ServiceListing{}

		switch s := svc.(type) {
		case *query.Service:
			err = data.FromService(models.Service{
				Type: models.SERVICETYPESERVICE,
				Name: s.Name,
				Peer: models.Peer{
					Id:   s.PeerID,
					Name: s.PeerName,
				},
			})
		case *query.DelegatedService:
			err = data.FromDelegatedService(models.DelegatedService{
				Type: models.SERVICETYPEDELEGATEDSERVICE,
				Name: s.Name,
				Peer: models.Peer{
					Id:   s.PeerID,
					Name: s.PeerName,
				},
				Delegator: models.Peer{
					Id:   s.DelegatorID,
					Name: s.DelegatorName,
				},
			})
		}

		if err != nil {
			return nil, err
		}

		services = append(services, data)
	}

	return api.ListServices200JSONResponse{
		Services: services,
	}, nil
}
