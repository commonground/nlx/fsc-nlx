// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"net/http"
	"net/netip"

	"github.com/go-chi/chi/v5"
	"github.com/pkg/errors"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/authentication"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

type contextKey string

const (
	SourceKey                contextKey = "Source"
	AuthenticationDataKey    contextKey = "AuthenticationData"
	AuditlogCorrelationIDKey contextKey = "AuditlogCorrelationID"
)

const AuditlogCorrelationIDHeader = "Auditlog-Correlation-Id"

type Server struct {
	app        *internalapp.Application
	logger     *logger.Logger
	cert       *common_tls.CertificateBundle
	handler    http.Handler
	selfPeerID contract.PeerID
	clock      clock.Clock
}

type NewArgs struct {
	Logger     *logger.Logger
	App        *internalapp.Application
	Cert       *common_tls.CertificateBundle
	SelfPeerID contract.PeerID
	Clock      clock.Clock
}

func New(args *NewArgs) (*Server, error) {
	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.App == nil {
		return nil, errors.New("app is required")
	}

	if args.Cert == nil {
		return nil, errors.New("cert is required")
	}

	if args.SelfPeerID == "" {
		return nil, errors.New("self peer id is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	s := &Server{
		app:        args.App,
		logger:     args.Logger,
		cert:       args.Cert,
		selfPeerID: args.SelfPeerID,
		clock:      args.Clock,
	}

	strict := api.NewStrictHandlerWithOptions(s, []api.StrictMiddlewareFunc{
		func(f api.StrictHandlerFunc, operationID string) api.StrictHandlerFunc {
			return func(ctx context.Context, w http.ResponseWriter, r *http.Request, args interface{}) (interface{}, error) {
				if len(r.TLS.PeerCertificates) == 0 {
					return nil, fmt.Errorf("client certificate missing")
				}

				peerCert := r.TLS.PeerCertificates[0]

				if peerCert.Subject.CommonName == "" {
					return nil, fmt.Errorf("missing common name in subject of client certifcate")
				}

				if peerCert.PublicKey == "" {
					return nil, fmt.Errorf("missing public key in client certificate")
				}

				ctx = context.WithValue(ctx, AuthenticationDataKey, &authentication.CertificateData{
					SubjectCommonName:   peerCert.Subject.CommonName,
					PublicKeyThumbprint: common_tls.PublicKeyThumbprint(peerCert),
				})

				addr, err := netip.ParseAddrPort(r.RemoteAddr)
				if err != nil {
					return nil, fmt.Errorf("cannot obtain client IP Address from HTTP request: %w", err)
				}

				ctx = context.WithValue(ctx, SourceKey, common_auditlog.SourceHTTP{
					UserAgent: r.UserAgent(),
					IPAddress: addr.Addr(),
				})

				ctx = context.WithValue(ctx, AuditlogCorrelationIDKey, r.Header.Get(AuditlogCorrelationIDHeader))

				return f(ctx, w, r, args)
			}
		},
	}, api.StrictHTTPServerOptions{
		RequestErrorHandlerFunc:  RequestErrorHandler,
		ResponseErrorHandlerFunc: ResponseErrorHandler,
	})

	r := chi.NewRouter()
	api.HandlerFromMux(strict, r)

	s.handler = r

	return s, nil
}

func (s *Server) Handler() http.Handler {
	return s.handler
}

func getSourceFromCtx(c context.Context) common_auditlog.Source {
	return c.Value(SourceKey)
}

func getAuthDataFromCtx(ctx context.Context) authentication.Data {
	return ctx.Value(AuthenticationDataKey)
}

func getCorrelationIDFromCtx(ctx context.Context) string {
	return ctx.Value(AuditlogCorrelationIDKey).(string)
}
