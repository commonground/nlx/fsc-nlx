// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
)

func (s *Server) GetMetadataRecords(ctx context.Context, request api.GetMetadataRecordsRequestObject) (api.GetMetadataRecordsResponseObject, error) {
	s.logger.Info("rest request GetMetadataRecords")

	records, err := s.app.Queries.GetTXLogMetadata.Handle(ctx, request.TransactionID)
	if err != nil {
		s.logger.Error("error getting tx log metadata from query", err)
		return nil, err
	}

	return api.GetMetadataRecords200JSONResponse{
		Records: mapMetadataToRest(records),
	}, nil
}

func mapMetadataToRest(records []*query.MetadataRecord) []models.Metadata {
	recs := make([]models.Metadata, len(records))

	for i, record := range records {
		recs[i] = models.Metadata{
			Direction:            mapMetadataDirection(record.Direction),
			TransactionId:        record.TransactionID,
			AdditionalProperties: record.Metadata,
		}
	}

	return recs
}

func mapMetadataDirection(direction metadata.Direction) models.DirectionType {
	switch direction {
	case metadata.DirectionIn:
		return models.DIRECTIONINCOMING
	case metadata.DirectionOut:
		return models.DIRECTIONOUTGOING
	default:
		return models.DIRECTIONINCOMING
	}
}
