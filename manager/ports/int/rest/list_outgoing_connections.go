// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

//nolint:gocyclo,funlen // requires a lot of setup due to the different query parameters, splitting this method would result in less readability
func (s *Server) ListOutgoingConnections(ctx context.Context, req api.ListOutgoingConnectionsRequestObject) (api.ListOutgoingConnectionsResponseObject, error) {
	s.logger.Info("rest request ListOutgoingConnections")

	sortOrder := query.SortOrderDescending

	if req.Params.SortOrder != nil && *req.Params.SortOrder == models.SORTORDERASCENDING {
		sortOrder = query.SortOrderAscending
	}

	const defaultPaginationLimit = 25
	limit := uint32(defaultPaginationLimit)

	if req.Params.Limit != nil {
		limit = *req.Params.Limit
	}

	paginationCursorCreatedAt := int64(0)
	paginationCursorStartHash := ""

	contractStatesFilter := []contract.ContractState{}

	if req.Params.ContractStates != nil {
		for _, state := range *req.Params.ContractStates {
			if state == "" {
				continue
			}

			domainState, err := convertAPIStateToDomainModel(state)
			if err != nil {
				return nil, fmt.Errorf("invalid contract state filter %q", state)
			}

			contractStatesFilter = append(contractStatesFilter, domainState)
		}
	}

	if req.Params.Cursor != nil {
		pagination, err := decodePaginationCursorForOutgoingConnections(req.Params.Cursor)
		if err != nil {
			return nil, err
		}

		paginationCursorCreatedAt = pagination.CreatedAt
		paginationCursorStartHash = pagination.GrantHash
	}

	includeCount := false
	if req.Params.IncludeCount != nil {
		includeCount = *req.Params.IncludeCount
	}

	res, err := s.app.Queries.ListOutgoingConnections.Handle(ctx, &query.ListOutgoingConnectionsHandlerArgs{
		PaginationStartCreatedAt: paginationCursorCreatedAt,
		PaginationStartHash:      paginationCursorStartHash,
		PaginationLimit:          limit,
		PaginationSortOrder:      sortOrder,
		IncludeCount:             includeCount,
		ContractStates:           contractStatesFilter,
	})
	if err != nil {
		s.logger.Error("list outgoing connections query", err)
		return nil, err
	}

	response := api.ListOutgoingConnections200JSONResponse{
		Connections: make([]models.Connection, len(res.OutgoingConnections)),
		Pagination:  models.PaginationResult{},
	}

	for i, c := range res.OutgoingConnections {
		response.Connections[i] = convertConnection(c)
	}

	if len(res.OutgoingConnections) == 0 {
		return response, nil
	}

	response.TotalCount = &res.Count

	lastConnection := res.OutgoingConnections[len(res.OutgoingConnections)-1]

	nextCursor, err := createNextCursorForOutgoingConnection(lastConnection.CreatedAt.Unix(), lastConnection.GrantHash)
	if err != nil {
		s.logger.Error("could not create cursor for next outgoing connection page", err)

		return nil, err
	}

	response.Pagination.NextCursor = nextCursor

	return response, nil
}

func convertConnection(c *query.Connection) models.Connection {
	connection := models.Connection{
		ContractHash:              c.ContentHash,
		CreatedAt:                 c.CreatedAt.Unix(),
		GrantHash:                 c.GrantHash,
		NotAfter:                  c.ValidNotAfter.Unix(),
		NotBefore:                 c.ValidNotBefore.Unix(),
		OutwayPeerId:              c.OutwayPeerID,
		ServiceName:               c.ServiceName,
		ServicePeerId:             c.ServicePeerID,
		OutwayPublicKeyThumbprint: c.OutwayPublicKeyThumbprint,
	}

	if c.DelegatorPeerID != "" {
		connection.DelegatorPeerId = &c.DelegatorPeerID
	}

	if c.ServicePublicationDelegatorPeerID != "" {
		connection.ServicePublicationDelegatorPeerId = &c.ServicePublicationDelegatorPeerID
	}

	state, err := convertState(c.ContractState)
	if err == nil {
		connection.State = &state
	}

	return connection
}

func convertState(state string) (models.ContractState, error) {
	switch state {
	case "valid":
		return models.CONTRACTSTATEVALID, nil
	case "rejected":
		return models.CONTRACTSTATEREJECTED, nil
	case "revoked":
		return models.CONTRACTSTATEREVOKED, nil
	case "expired":
		return models.CONTRACTSTATEEXPIRED, nil
	case "proposed":
		return models.CONTRACTSTATEPROPOSED, nil
	default:
		return "", errors.New("invalid contract state")
	}
}

func convertAPIStateToDomainModel(state models.ContractState) (contract.ContractState, error) {
	switch state {
	case models.CONTRACTSTATEVALID:
		return contract.ContractStateValid, nil
	case models.CONTRACTSTATEREJECTED:
		return contract.ContractStateRejected, nil
	case models.CONTRACTSTATEREVOKED:
		return contract.ContractStateRevoked, nil
	case models.CONTRACTSTATEEXPIRED:
		return contract.ContractStateExpired, nil
	case models.CONTRACTSTATEPROPOSED:
		return contract.ContractStateProposed, nil
	default:
		return contract.ContractStateUnspecified, errors.New("invalid contract state")
	}
}

type outgoingConnectionsPaginationCursor struct {
	CreatedAt int64  `json:"created_at"`
	GrantHash string `json:"grant_hash"`
}

// nolint:dupl // duplicated to decouple pagination per page
func decodePaginationCursorForOutgoingConnections(input *models.QueryPaginationCursor) (*outgoingConnectionsPaginationCursor, error) {
	if input == nil {
		return &outgoingConnectionsPaginationCursor{
			CreatedAt: 0,
			GrantHash: "",
		}, nil
	}

	decodedCursor, err := base64.RawStdEncoding.DecodeString(*input)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not decode cursor from base64 string: %s", *input))
	}

	var cursor outgoingConnectionsPaginationCursor

	err = json.Unmarshal(decodedCursor, &cursor)
	if err != nil {
		return nil, errors.Join(err, errors.New("cursor does not contain required format"))
	}

	if cursor.GrantHash == "" {
		return nil, fmt.Errorf("invalid cursor, missing GrantHash: %v", err)
	}

	if cursor.CreatedAt == 0 {
		return nil, fmt.Errorf("invalid cursor, missing CreatedAt: %v", err)
	}

	return &cursor, nil
}

func createNextCursorForOutgoingConnection(createdAt int64, grantHash string) (string, error) {
	jsonCursor, err := json.Marshal(outgoingConnectionsPaginationCursor{
		CreatedAt: createdAt,
		GrantHash: grantHash,
	})

	if err != nil {
		return "", err
	}

	return base64.RawURLEncoding.EncodeToString(jsonCursor), nil
}
