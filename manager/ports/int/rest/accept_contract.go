// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) AcceptContract(ctx context.Context, req api.AcceptContractRequestObject) (api.AcceptContractResponseObject, error) {
	s.logger.Info("rest request AcceptContract")

	err := s.app.Commands.AcceptContract.Handle(ctx, &command.AcceptContractHandlerArgs{
		AuditLogSource:        getSourceFromCtx(ctx),
		AuthData:              getAuthDataFromCtx(ctx),
		AuditlogCorrelationID: getCorrelationIDFromCtx(ctx),
		ContentHash:           req.Hash,
	})
	if err != nil {
		s.logger.Error("error executing accept contract command", err)
		return nil, err
	}

	return api.AcceptContract204Response{}, nil
}
