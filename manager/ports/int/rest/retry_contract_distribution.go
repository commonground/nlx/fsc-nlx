// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) RetryContractDistribution(ctx context.Context, req api.RetryContractDistributionRequestObject) (api.RetryContractDistributionResponseObject, error) {
	s.logger.Info("rest request RetryContractDistributionRequestObject")

	err := s.app.Commands.RetryContractDistribution.Handle(ctx, &command.RetryContractDistributionHandlerArgs{
		ContentHash:           req.Hash,
		PeerID:                req.PeerId,
		Action:                convertAction(req.DistributionAction),
		AuditLogSource:        getSourceFromCtx(ctx),
		AuthData:              getAuthDataFromCtx(ctx),
		AuditlogCorrelationID: getCorrelationIDFromCtx(ctx),
	})
	if err != nil {
		s.logger.Error("error executing retry contract distribution command", err)
		return nil, err
	}

	return api.RetryContractDistribution201Response{
		Headers: api.RetryContractDistribution201ResponseHeaders{
			Link: fmt.Sprintf("</api/v1/contracts/%s/distributions>; rel=\"status\"", req.Hash),
		},
	}, nil
}

func convertAction(action models.DistributionAction) command.Action {
	switch action {
	case models.DISTRIBUTIONACTIONSUBMITCONTRACT:
		return command.DistributionActionSubmitContract
	case models.DISTRIBUTIONACTIONSUBMITACCEPTSIGNATURE:
		return command.DistributionActionSubmitAcceptSignature
	case models.DISTRIBUTIONACTIONSUBMITREJECTSIGNATURE:
		return command.DistributionActionSubmitRejectSignature
	case models.DISTRIBUTIONACTIONSUBMITREVOKESIGNATURE:
		return command.DistributionActionSubmitRevokeSignature
	default:
		return -1
	}
}
