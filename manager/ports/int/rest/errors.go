// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package rest

import (
	"encoding/json"
	"errors"
	"net/http"

	internal_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func RequestErrorHandler(w http.ResponseWriter, r *http.Request, err error) {
	http.Error(w, err.Error(), http.StatusBadRequest)
}

func ResponseErrorHandler(w http.ResponseWriter, r *http.Request, err error) {
	w.Header().Set("Content-Type", "application/problem+json")

	var appError internal_errors.Error
	ok := errors.As(err, &appError)
	if !ok {
		errToHTTP(w, http.StatusInternalServerError, models.Error{
			Title:   "internal server error",
			Details: err.Error(),
		})

		return
	}

	switch appError.ErrorType() {
	case internal_errors.ErrorTypeIncorrectInput:
		errToHTTP(w, http.StatusUnprocessableEntity, models.Error{
			Title:   "invalid arguments",
			Details: appError.Error(),
		})

		return
	default:
		errToHTTP(w, http.StatusInternalServerError, models.Error{
			Title:   "internal server error",
			Details: appError.Error(),
		})

		return
	}
}

func errToHTTP(w http.ResponseWriter, statusCode int, e models.Error) {
	w.WriteHeader(statusCode)

	e.Status = statusCode

	_ = json.NewEncoder(w).Encode(e)
}
