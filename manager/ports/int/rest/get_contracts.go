// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

// nolint:funlen,gocyclo // unable to make shorter
func (s *Server) GetContracts(ctx context.Context, req api.GetContractsRequestObject) (api.GetContractsResponseObject, error) {
	s.logger.Info("rest request GetContracts")

	grantTypes := []query.GrantType{}

	if req.Params.GrantType != nil {
		for _, h := range *req.Params.GrantType {
			if h == "" {
				continue
			}

			gt := convertGrantType(h)
			grantTypes = append(grantTypes, gt)
		}
	}

	contentHashes := make([]string, 0)

	if req.Params.ContentHash != nil {
		for _, h := range *req.Params.ContentHash {
			if h == "" {
				continue
			}

			contentHashes = append(contentHashes, h)
		}
	}

	grantHashes := make([]string, 0)

	if req.Params.GrantHash != nil {
		for _, h := range *req.Params.GrantHash {
			if h == "" {
				continue
			}

			grantHashes = append(grantHashes, h)
		}
	}

	sortOrder := query.SortOrderDescending

	if req.Params.SortOrder != nil && *req.Params.SortOrder == models.SORTORDERASCENDING {
		sortOrder = query.SortOrderAscending
	}

	limit := uint32(0)

	if req.Params.Limit != nil {
		limit = *req.Params.Limit
	}

	paginationCursorStartDate := time.Time{}
	paginationCursorStartHash := ""

	if req.Params.Cursor != nil {
		startDate, hash, err := decodePaginationCursor(*req.Params.Cursor)
		if err != nil {
			return nil, err
		}

		paginationCursorStartDate = startDate
		paginationCursorStartHash = hash
	}

	res, err := s.app.Queries.GetContracts.Handle(ctx, &query.ListContractsHandlerArgs{
		GrantTypes:          grantTypes,
		ContentHashes:       contentHashes,
		GrantHashes:         grantHashes,
		PaginationStartDate: paginationCursorStartDate,
		PaginationStartHash: paginationCursorStartHash,
		PaginationLimit:     limit,
		PaginationSortOrder: sortOrder,
	})
	if err != nil {
		s.logger.Error("get contracts query", err)
		return nil, err
	}

	response := api.GetContracts200JSONResponse{
		Contracts:  make([]models.Contract, len(res)),
		Pagination: models.PaginationResult{},
	}

	for i, c := range res {
		convertedContract, convertErr := convertContract(c, s.selfPeerID, s.clock.Now())
		if convertErr != nil {
			s.logger.Error("list contracts response", convertErr)

			return nil, convertErr
		}

		response.Contracts[i] = *convertedContract
	}

	if len(res) == 0 {
		return response, nil
	}

	lastContract := res[len(res)-1]
	nextCursor := createNextCursorForContract(lastContract.Content().CreatedAt(), lastContract.Content().Hash().String())

	response.Pagination.NextCursor = nextCursor

	return response, nil
}

func decodePaginationCursor(input string) (time.Time, string, error) {
	decodedCursor, err := base64.RawURLEncoding.DecodeString(input)
	if err != nil {
		return time.Now(), "", errors.Wrapf(err, "could not decode cursor from base64 string: %s", input)
	}

	parts := strings.Split(string(decodedCursor), ",")

	const partsInCursor = 2

	if len(parts) != partsInCursor {
		return time.Now(), "", fmt.Errorf("cursor should consist of two parts. found %d", len(parts))
	}

	integerDatetime, err := strconv.Atoi(parts[0])
	if err != nil {
		return time.Now(), "", fmt.Errorf("datetime is not a valid integer: %q", parts[0])
	}

	return time.Unix(int64(integerDatetime), 0).UTC(), parts[1], nil
}

func createNextCursorForContract(createdAt time.Time, contentHash string) string {
	cursor := fmt.Sprintf("%d,%s", createdAt.Unix(), contentHash)

	return base64.RawURLEncoding.EncodeToString([]byte(cursor))
}

func mapContractState(c contract.ContractState) models.ContractState {
	switch c {
	case contract.ContractStateExpired:
		return models.CONTRACTSTATEEXPIRED
	case contract.ContractStateNotStarted:
		return models.CONTRACTSTATEPROPOSED
	case contract.ContractStateProposed:
		return models.CONTRACTSTATEPROPOSED
	case contract.ContractStateRejected:
		return models.CONTRACTSTATEREJECTED
	case contract.ContractStateRevoked:
		return models.CONTRACTSTATEREVOKED
	case contract.ContractStateValid:
		return models.CONTRACTSTATEVALID
	default:
		return ""
	}
}

func convertGrantType(grantType models.GrantType) query.GrantType {
	switch grantType {
	case models.GRANTTYPESERVICECONNECTION:
		return query.GrantTypeServiceConnection
	case models.GRANTTYPESERVICEPUBLICATION:
		return query.GrantTypeServicePublication
	case models.GRANTTYPEDELEGATEDSERVICECONNECTION:
		return query.GrantTypeDelegatedServiceConnection
	case models.GRANTTYPEDELEGATEDSERVICEPUBLICATION:
		return query.GrantTypeDelegatedServicePublication
	default:
		return query.GrantTypeUnspecified
	}
}

func mapServiceProtocolToRest(p contract.ServiceProtocol) models.Protocol {
	switch p {
	case contract.ServiceProtocolTCPHTTP1_1:
		return models.PROTOCOLTCPHTTP11
	case contract.ServiceProtocolTCPHTTP2:
		return models.PROTOCOLTCPHTTP2
	default:
		return ""
	}
}
