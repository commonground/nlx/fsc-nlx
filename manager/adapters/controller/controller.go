// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package controller

import (
	"context"
	"net/url"
)

type Controller interface {
	GetService(ctx context.Context, serviceName string) (*Service, error)
}

type Service struct {
	Name         string
	InwayAddress string
	EndpointURL  *url.URL
}
