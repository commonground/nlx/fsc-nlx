// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"fmt"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListOutgoingConnectionsForService(args *contract.ListOutgoingConnectionsForServiceArgs) ([]*contract.Connection, error) {
	delegatorPeerID := pgtype.Text{
		Valid: false,
	}

	if args.DelegatorPeerID != "" {
		delegatorPeerID.Valid = true
		delegatorPeerID.String = string(args.DelegatorPeerID)
	}

	outgoingConnectionRows, err := r.queries.ListOutgoingConnectionsForService(args.Ctx, &queries.ListOutgoingConnectionsForServiceParams{
		SelfPeerID:                        string(args.SelfPeerID),
		ServiceName:                       args.ServiceName,
		ServicePeerID:                     string(args.ServicePeerID),
		ServicePublicationDelegatorPeerID: delegatorPeerID,
		OrderDirection:                    string(args.SortOrder),
	})
	if err != nil {
		return nil, fmt.Errorf("could not retrieve outgoing connection for service from database: %v", err)
	}

	outgoingConnections := make([]*contract.Connection, len(outgoingConnectionRows))

	//nolint:dupl // is a duplication but the enclosing function is different. we like to keep it separate.
	for i, cr := range outgoingConnectionRows {
		c := &contract.Connection{
			ServicePeerID:             contract.PeerID(cr.ServicePeerID),
			ServiceName:               cr.ServiceName,
			ContentHash:               cr.ContentHash,
			CreatedAt:                 cr.CreatedAt.Time,
			ValidNotBefore:            cr.ValidNotBefore.Time,
			ValidNotAfter:             cr.ValidNotAfter.Time,
			ContractState:             convertDBContractStateToDomain(cr.State),
			GrantHash:                 cr.Hash,
			OutwayPeerID:              contract.PeerID(cr.OutwayPeerID),
			OutwayPublicKeyThumbprint: contract.PublicKeyThumbprint(cr.PublicKeyThumbprint),
		}

		if cr.ServicePublicationDelegatorPeerID.Valid {
			pID := contract.PeerID(cr.ServicePublicationDelegatorPeerID.String)
			c.ServicePublicationDelegatorPeerID = &pID
		}

		if cr.DelegatorPeerID != nil {
			dpID, errConv := contract.NewPeerID(cr.DelegatorPeerID.(string))
			if errConv != nil {
				return nil, fmt.Errorf("could not convert delegator peer ID: %v", errConv)
			}

			c.DelegatorPeerID = &dpID
		}

		outgoingConnections[i] = c
	}

	return outgoingConnections, nil
}
