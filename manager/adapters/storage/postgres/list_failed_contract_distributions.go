// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListFailedContractDistributions(ctx context.Context, contentHash *contract.ContentHash) ([]*contract.FailedDistribution, error) {
	var contentHashFilter = pgtype.Text{}

	if contentHash != nil {
		contentHashFilter.String = contentHash.String()
		contentHashFilter.Valid = true
	}

	rows, err := r.queries.ListFailedContractDistributions(ctx, contentHashFilter)
	if err != nil {
		return nil, err
	}

	distributions := make([]*contract.FailedDistribution, len(rows))

	for i, row := range rows {
		var nextRetryAt *time.Time

		if row.NextAttemptAt.Valid {
			nextRetryAt = &row.NextAttemptAt.Time
		}

		var hash *contract.ContentHash

		hash, err = contract.NewContentHashFromString(row.ContentHash)
		if err != nil {
			return nil, fmt.Errorf("invalid content hash in db: %s", err)
		}

		var distribution *contract.FailedDistribution

		distribution, err = contract.NewFailedDistribution(&contract.NewFailedDistributionArgs{
			ContentHash:   hash,
			PeerID:        row.PeerID,
			Action:        string(row.Action),
			Signature:     row.Signature,
			Attempts:      row.Attempts,
			NextAttemptAt: nextRetryAt,
			LastAttemptAt: row.LastAttemptAt.Time,
			Reason:        row.Reason,
		})
		if err != nil {
			return nil, fmt.Errorf("invalid failed contract distribution in db: %s", err)
		}

		distributions[i] = distribution
	}

	return distributions, nil
}
