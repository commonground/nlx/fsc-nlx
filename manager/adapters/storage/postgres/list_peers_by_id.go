// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListPeersByID(ctx context.Context, peerIDs contract.PeersIDs) (contract.Peers, error) {
	peerIDsAsString := make([]string, 0)

	for peerID := range peerIDs {
		peerIDsAsString = append(peerIDsAsString, peerID.Value())
	}

	rows, err := r.queries.ListPeersByID(ctx, peerIDsAsString)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not list peers by id from database"))
	}

	peers := make(contract.Peers, 0)

	if len(rows) == 0 {
		return peers, nil
	}

	for _, row := range rows {
		var p *contract.Peer

		var roles []contract.PeerRole

		roles, err = convertRoles(row.Roles)
		if err != nil {
			return nil, errors.Join(err, errors.New("could not convert roles"))
		}

		p, err = contract.NewPeer(&contract.NewPeerArgs{
			ID:             row.ID,
			Name:           row.Name.String,
			ManagerAddress: row.ManagerAddress.String,
			Roles:          roles,
		})
		if err != nil {
			return nil, errors.Join(err, errors.New("invalid peer in database"))
		}

		peers[p.ID()] = p
	}

	return peers, nil
}
