// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListPeersByName(ctx context.Context, peerName, paginationStartID string, paginationLimit uint32, paginationSortOrder contract.SortOrder) ([]*contract.Peer, error) {
	peerName = fmt.Sprintf("%%%s%%", peerName) // sqlc does not support LIKE with %

	rows, err := r.queries.ListPeersByName(ctx, &queries.ListPeersByNameParams{
		Name:              peerName,
		Limit:             int32(paginationLimit),
		PaginationStartID: paginationStartID,
		OrderDirection:    string(paginationSortOrder),
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not list peers by name from database"))
	}

	peers := make([]*contract.Peer, len(rows))

	if len(rows) == 0 {
		return peers, nil
	}

	for i, row := range rows {
		var roles []contract.PeerRole

		roles, err = convertRoles(row.Roles)
		if err != nil {
			return nil, errors.Join(err, errors.New("could not convert roles"))
		}

		p, errNewPeer := contract.NewPeer(&contract.NewPeerArgs{
			ID:             row.ID,
			Name:           row.Name.String,
			ManagerAddress: row.ManagerAddress.String,
			Roles:          roles,
		})
		if errNewPeer != nil {
			return nil, errors.Join(errNewPeer, errors.New("invalid peer in database"))
		}

		peers[i] = p
	}

	return peers, nil
}
