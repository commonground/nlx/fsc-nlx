// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) UpsertPeer(ctx context.Context, p *contract.Peer) error {
	name := p.Name().Value()
	if name == "" {
		return fmt.Errorf("could not upsert peer into database: name is required")
	}

	managerAddress := p.ManagerAddress().Value()
	if managerAddress == "" {
		return fmt.Errorf("could not upsert peer into database: manager address is required")
	}

	err := r.queries.UpsertPeer(ctx, &queries.UpsertPeerParams{
		ID:             p.ID().Value(),
		Name:           pgtype.Text{Valid: true, String: name},
		ManagerAddress: pgtype.Text{Valid: true, String: managerAddress},
	})
	if err != nil {
		return errors.Join(err, errors.New("could not upsert peer into database"))
	}

	return nil
}
