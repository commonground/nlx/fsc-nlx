// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:dupl // similar but not duplicate
func (r *PostgreSQLRepository) ListContractsForPeerWithDelegatedServiceConnectionGrant(ctx context.Context, peerID contract.PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder contract.SortOrder) ([]*contract.Contract, error) {
	contractRows, err := r.queries.ListContractsForPeerWithDelegatedServiceConnectionGrant(ctx, &queries.ListContractsForPeerWithDelegatedServiceConnectionGrantParams{
		PaginationStartID: paginationStartID,
		PeerID:            string(peerID),
		Limit:             int32(paginationLimit),
		OrderDirection:    string(paginationSortOrder),
	})
	if err != nil {
		return nil, fmt.Errorf("could not list contracts for peer with service connection grant from database: %v", err)
	}

	if len(contractRows) == 0 {
		return []*contract.Contract{}, nil
	}

	result := make([]*contract.Contract, len(contractRows))

	for i, contractRow := range contractRows {
		model, contractErr := getContractFromDB(ctx, r.clock, r.queries, r.trustedRootCAs, &createContractArgs{
			IV:            contractRow.ContentIv,
			contentHash:   contractRow.ContentHash,
			groupID:       contractRow.ContentGroupID,
			hashAlgorithm: contractRow.ContentHashAlgorithm,
			createdAt:     contractRow.ContentCreatedAt.Time,
			notBefore:     contractRow.ContentValidNotBefore.Time,
			notAfter:      contractRow.ContentValidNotAfter.Time,
		})
		if contractErr != nil {
			return nil, fmt.Errorf("invalid contract in database: %v", contractErr)
		}

		result[i] = model
	}

	return result, nil
}
