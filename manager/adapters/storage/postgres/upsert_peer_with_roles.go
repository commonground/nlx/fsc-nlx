// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

var ErrCodePrimaryKeyConstraint = "23505"

//nolint:gocyclo // complex because of the transaction
func (r *PostgreSQLRepository) UpsertPeerWithRoles(ctx context.Context, p *contract.Peer) error {
	name := p.Name().Value()
	if name == "" {
		return fmt.Errorf("could not upsert peer into database: name is required")
	}

	managerAddress := p.ManagerAddress().Value()
	if managerAddress == "" {
		return fmt.Errorf("could not upsert peer into database: manager address is required")
	}

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return err
	}

	defer func() {
		err = tx.Rollback(ctx)
		if err != nil {
			if errors.Is(err, pgx.ErrTxClosed) {
				return
			}

			fmt.Printf("cannot rollback database transaction while creating record: %e", err)
		}
	}()

	qtx := r.queries.WithTx(tx)

	err = qtx.UpsertPeer(ctx, &queries.UpsertPeerParams{
		ID:             p.ID().Value(),
		Name:           pgtype.Text{Valid: true, String: name},
		ManagerAddress: pgtype.Text{Valid: true, String: managerAddress},
	})
	if err != nil {
		return errors.Join(err, errors.New("could not upsert peer into database"))
	}

	err = qtx.DeletePeerRoles(ctx, p.ID().Value())
	if err != nil {
		return errors.Join(err, errors.New("could not delete peer roles from database"))
	}

	for _, role := range p.Roles() {
		var dbRole queries.PeersRole

		switch role {
		case contract.PeerRoleDirectory:
			dbRole = queries.PeersRoleROLEDIRECTORY
		default:
			return fmt.Errorf("unsupported peer role: %d", role)
		}

		err = qtx.UpsertPeerRole(ctx, &queries.UpsertPeerRoleParams{
			PeerID: p.ID().Value(),
			Role:   dbRole,
		})
		if err != nil {
			var pgErr *pgconn.PgError
			if errors.As(err, &pgErr) {
				if pgErr.Code == ErrCodePrimaryKeyConstraint {
					continue
				}

				return errors.Join(err, errors.New("could not upsert peer role into database"))
			}

			return errors.Join(err, errors.New("could not upsert peer role into database"))
		}
	}

	return tx.Commit(ctx)
}
