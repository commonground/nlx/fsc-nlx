// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetContentByHash(ctx context.Context, contentHash *contract.ContentHash) (*contract.Content, error) {
	row, err := r.queries.GetContentByHash(ctx, contentHash.String())
	if err != nil {
		return nil, fmt.Errorf("could not get content by hash from database: %v", err)
	}

	hashAlg := row.ContentHashAlgorithm
	notBefore := row.ContentValidNotBefore
	notAfter := row.ContentValidNotAfter
	groupID := row.ContentGroupID
	iv := row.ContentIv
	createdAt := row.ContentCreatedAt

	grants, err := getGrantsByContractHash(ctx, r.queries, row.ContentHash)
	if err != nil {
		return nil, fmt.Errorf("could not get gratns for contract: %v", err)
	}

	model, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         r.clock,
		HashAlgorithm: hashAlgToModel(hashAlg),
		IV:            iv.Bytes(),
		GroupID:       groupID,
		Validity: &contract.NewValidityArgs{
			NotBefore: notBefore.Time,
			NotAfter:  notAfter.Time,
		},
		Grants:    grants,
		CreatedAt: createdAt.Time,
	})
	if err != nil {
		return nil, errors.Wrap(err, "invalid content in database")
	}

	return model, nil
}
