// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListPeers(ctx context.Context, paginationStartID string, paginationLimit uint32, paginationSortOrder contract.SortOrder) ([]*contract.Peer, error) {
	var rows []*queries.ListPeersRow

	var err error

	rows, err = r.queries.ListPeers(ctx, &queries.ListPeersParams{
		Limit:             int32(paginationLimit),
		PaginationStartID: paginationStartID,
		OrderDirection:    string(paginationSortOrder),
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not list peers from database"))
	}

	peers := make([]*contract.Peer, 0, len(rows))

	if len(rows) == 0 {
		return peers, nil
	}

	for _, row := range rows {
		var roles []contract.PeerRole

		roles, err = convertRoles(row.Roles)
		if err != nil {
			return nil, errors.Join(err, errors.New("could not convert roles"))
		}

		p, errNewPeer := contract.NewPeer(&contract.NewPeerArgs{
			ID:             row.ID,
			Name:           row.Name.String,
			ManagerAddress: row.ManagerAddress.String,
			Roles:          roles,
		})
		if errNewPeer != nil {
			return nil, errors.Join(errNewPeer, errors.New("invalid peer in database"))
		}

		peers = append(peers, p)
	}

	return peers, nil
}

func convertRoles(rolesString string) ([]contract.PeerRole, error) {
	if rolesString == "" {
		return []contract.PeerRole{}, nil
	}

	roleStrings := strings.Split(rolesString, ",")

	roles := make([]contract.PeerRole, 0, len(roleStrings))

	for _, roleString := range roleStrings {
		switch roleString {
		case string(queries.PeersRoleROLEDIRECTORY):
			roles = append(roles, contract.PeerRoleDirectory)
		default:
			return nil, fmt.Errorf("unknown role '%s' ", roleString)
		}
	}

	return roles, nil
}
