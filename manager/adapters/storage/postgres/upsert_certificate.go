// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) UpsertCertificate(ctx context.Context, certificate *contract.PeerCertificate) error {
	return r.queries.UpsertCertificate(ctx, &queries.UpsertCertificateParams{
		PeerID:                certificate.Peer().ID().Value(),
		CertificateThumbprint: certificate.CertificateThumbprint().Value(),
		Certificate:           certificate.Raw(),
	})
}
