// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) UpsertFailedContractDistribution(ctx context.Context, distribution *contract.FailedDistribution) error {
	nextAttemptAt := pgtype.Timestamptz{}

	if distribution.NextAttemptAt() != nil {
		nextAttemptAt.Valid = true
		nextAttemptAt.Time = *distribution.NextAttemptAt()
	}

	return r.queries.UpsertFailedContractDistribution(ctx, &queries.UpsertFailedContractDistributionParams{
		PeerID:        distribution.PeerID().Value(),
		ContentHash:   distribution.ContentHash().String(),
		Action:        queries.ContractsDistributionAction(distribution.Action()),
		Signature:     distribution.Signature(),
		NextAttemptAt: nextAttemptAt,
		Reason:        distribution.Reason(),
		Attempts:      distribution.Attempts(),
		LastAttemptAt: pgtype.Timestamptz{
			Time:  distribution.LastAttemptAt(),
			Valid: true,
		},
	})
}
