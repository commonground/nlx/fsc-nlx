// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListDirectoryPeers(ctx context.Context) ([]*contract.Peer, error) {
	var rows []*queries.ListDirectoryPeersRow

	var err error

	rows, err = r.queries.ListDirectoryPeers(ctx)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not list peers from database"))
	}

	peers := make([]*contract.Peer, 0, len(rows))

	if len(rows) == 0 {
		return peers, nil
	}

	for _, row := range rows {
		var roles []contract.PeerRole

		roles, err = convertRoles(row.Roles)
		if err != nil {
			return nil, errors.Join(err, errors.New("could not list peers from database"))
		}

		p, errNewPeer := contract.NewPeer(&contract.NewPeerArgs{
			ID:             row.ID,
			Name:           row.Name.String,
			ManagerAddress: row.ManagerAddress.String,
			Roles:          roles,
		})
		if errNewPeer != nil {
			return nil, errors.Join(err, errors.New("could not list peers from database"))
		}

		peers = append(peers, p)
	}

	return peers, nil
}
