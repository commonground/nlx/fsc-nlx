-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListSignaturesForContract :many
SELECT
    signature_type as type,
    peer_id,
    p.name as peer_name,
    certificate_thumbprint,
    signature as jws,
    cs.signed_at
FROM
    contracts.contract_signatures as cs
LEFT JOIN
    peers.peers as p
ON
    p.id = cs.peer_id
WHERE
    cs.content_hash = $1
;

-- name: ListServiceConnectionGrantsForContract :many
SELECT
    gc.outway_peer_id as outway_peer_id,
    gc.service_type as service_type,
    gc.service_peer_id as service_peer_id,
    gc.service_name as service_name,
    gc.service_publication_delegator_peer_id as service_publication_delegator_peer_id,
    gc.public_key_thumbprint as public_key_thumbprint
FROM
    contracts.grants_service_connection as gc
WHERE
        gc.content_hash = $1
GROUP BY
    gc.hash
;

-- name: ListDelegatedServiceConnectionGrantsForContract :many
SELECT
    gdsc.outway_peer_id as outway_peer_id,
    gdsc.service_type as service_type,
    gdsc.service_peer_id as service_peer_id,
    gdsc.service_name as service_name,
    gdsc.service_publication_delegator_peer_id as service_publication_delegator_peer_id,
    gdsc.delegator_peer_id as delegator_peer_id,
    gdsc.public_key_thumbprint as public_key_thumbprint
FROM
    contracts.grants_delegated_service_connection as gdsc
WHERE
        gdsc.content_hash = $1
GROUP BY
    gdsc.hash
;

-- name: ListDelegatedServicePublicationGrantsForContract :many
SELECT
    gdsp.directory_peer_id as directory_peer_id,
    gdsp.service_peer_id as service_peer_id,
    gdsp.service_name as service_name,
    gdsp.service_protocol as service_protocol,
    gdsp.delegator_peer_id as delegator_peer_id
FROM
    contracts.grants_delegated_service_publication as gdsp
WHERE
    gdsp.content_hash = $1
GROUP BY
    gdsp.hash
;

-- name: ListServicePublicationGrantsForContract :many
SELECT
    gp.hash as hash,
    gp.directory_peer_id as directory_peer_id,
    gp.service_peer_id as service_peer_id,
    gp.service_name as service_name,
    gp.service_protocol as service_protocol
FROM
    contracts.grants_service_publication as gp
WHERE
    gp.content_hash = $1
;
