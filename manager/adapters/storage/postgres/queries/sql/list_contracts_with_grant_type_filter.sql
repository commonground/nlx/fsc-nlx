-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: ListContractsWithGrantTypeFilter :many
SELECT
    c.iv AS content_iv,
    c.hash as content_hash,
    c.hash_algorithm as content_hash_algorithm,
    c.group_id as content_group_id,
    c.valid_not_before as content_valid_not_before,
    c.valid_not_after as content_valid_not_after,
    c.created_at as content_created_at
FROM
    contracts.with_grant_type as c
WHERE
    (
        @pagination_start_hash = ''
        or
        (
            @order_direction::text = 'asc'
            and
            (
                c.created_at::timestamp > sqlc.arg(pagination_start_date)::timestamp with time zone
                or
                (
                    c.created_at::timestamp = sqlc.arg(pagination_start_date)::timestamp with time zone
                    and
                    c.hash > sqlc.arg(pagination_start_hash)::text
                )
            )
        )
        or
        (
            @order_direction::text = 'desc'
            and
                (
                c.created_at::timestamp < sqlc.arg(pagination_start_date)::timestamp with time zone
                or
                (
                    c.created_at::timestamp = sqlc.arg(pagination_start_date)::timestamp with time zone
                    and
                    c.hash < sqlc.arg(pagination_start_hash)::text
                )
            )
        )
    ) and (
        (
            c.has_service_connection_grant =
            CASE WHEN @with_service_connection_grants = true THEN true ELSE NULL END
        ) OR
        (
            c.has_delegated_service_connection_grant =
            CASE
                WHEN @with_delegated_service_connection_grants = true
                THEN true
            ELSE NULL END
        ) OR
        (
            c.has_service_publication_grant =
            CASE
                WHEN @with_service_publication_grants = true
                    THEN true
                ELSE NULL
                END
        ) OR
        (
            c.has_delegated_service_publication_grant =
            CASE
                WHEN @with_delegated_service_publication_grants = true
                    THEN true
                ELSE NULL
                END
        )
    )
ORDER BY
-- NOTE: unable to use ASC/DESC dynamically so we use a switch case
CASE
    WHEN @order_direction::text = 'asc' THEN c.created_at END ASC,
CASE
    WHEN @order_direction::text = 'asc' THEN c.hash COLLATE "C" END ASC,
CASE
    WHEN @order_direction::text = 'desc' THEN c.created_at END DESC,
CASE
    WHEN @order_direction::text = 'desc' THEN c.hash COLLATE "C" END DESC
LIMIT $1;
