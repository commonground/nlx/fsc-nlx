-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: DeleteFailedContractDistributions :exec
DELETE FROM
    contracts.failed_distributions
WHERE
    peer_id =  $1 AND content_hash = $2 AND action = $3;
