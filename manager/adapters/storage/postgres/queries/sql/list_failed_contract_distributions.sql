-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: ListFailedContractDistributions :many
SELECT
    peer_id,
    content_hash,
    signature,
    action,
    next_attempt_at,
    reason,
    attempts,
    last_attempt_at
FROM
    contracts.failed_distributions
WHERE
    sqlc.narg('content_hash')::text IS NULL OR content_hash = sqlc.narg('content_hash')::text;
