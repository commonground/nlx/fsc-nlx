-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: ListContractsForPeerWithServiceConnectionGrant :many
SELECT
    c.iv AS content_iv,
    c.hash as content_hash,
    c.hash_algorithm as content_hash_algorithm,
    c.group_id as content_group_id,
    c.valid_not_before as content_valid_not_before,
    c.valid_not_after as content_valid_not_after,
    c.created_at as content_created_at
FROM contracts.content as c
     INNER JOIN contracts.grants_service_connection as gc
        ON gc.content_hash = c.hash
WHERE
    (
        gc.service_peer_id = sqlc.arg(peer_id)::text OR
        gc.outway_peer_id = sqlc.arg(peer_id)::text
    ) AND
    (
                @pagination_start_id::text = ''
            OR
                (@order_direction::text = 'asc' AND c.iv > @pagination_start_id::uuid)
            OR
                (@order_direction::text = 'desc' AND c.iv < @pagination_start_id::uuid)
        )
ORDER BY
-- NOTE: unable to use ASC/DESC dynamically so we use a switch case
CASE
    WHEN @order_direction::text = 'asc' THEN c.iv  END ASC,
CASE
    WHEN @order_direction::text = 'desc' THEN c.iv END DESC
LIMIT $1;
