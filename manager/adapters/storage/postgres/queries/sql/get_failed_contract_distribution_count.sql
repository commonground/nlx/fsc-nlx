-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: GetFailedDistributionCount :one
SELECT
    COUNT(*) as failed_distribution_count
FROM
    contracts.failed_distributions;
