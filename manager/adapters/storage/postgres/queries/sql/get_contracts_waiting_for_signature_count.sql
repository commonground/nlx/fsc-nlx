-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: GetContractsWaitingForSignatureCount :one
SELECT
    count(*) as contract_waiting_for_signature
FROM contracts.contracts_with_state AS c
WHERE
    c.state = 'proposed' AND (sqlc.arg(created_before_date)::timestamp with time zone IS NULL OR c.created_at < sqlc.arg(created_before_date)::timestamp with time zone);
