-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: UpsertFailedContractDistribution :exec
insert into
    contracts.failed_distributions
(
    peer_id,
    content_hash,
    action,
    signature,
    next_attempt_at,
    reason,
    attempts,
    last_attempt_at
) values (
    $1,
    $2,
    $3,
    $4,
    $5,
    $6,
    $7,
    $8
) on conflict (
    peer_id,
    content_hash,
    action
) do update set
    signature = excluded.signature,
    next_attempt_at = excluded.next_attempt_at,
    reason = excluded.reason,
    attempts = excluded.attempts,
    last_attempt_at = excluded.last_attempt_at
;
