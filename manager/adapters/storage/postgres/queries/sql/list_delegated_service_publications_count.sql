-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: ListDelegatedServicePublicationsCount :one
SELECT
    count(*) as total_count
FROM (
    SELECT service_peer_id, service_name, content_hash, hash, directory_peer_id, delegator_peer_id
    FROM contracts.grants_delegated_service_publication
    WHERE delegator_peer_id = @self_peer_id::text
) AS p
JOIN contracts.contracts_with_state AS c ON c.hash = p.content_hash
    WHERE
    CASE
        when cardinality(@contract_states::text[]) = 0 THEN
            content_hash IN (SELECT hash AS content_hash FROM contracts.contracts_with_state)
        else
            content_hash IN (SELECT hash AS content_hash FROM contracts.contracts_with_state where contracts_with_state.state = ANY (@contract_states::text[]))
        end;
