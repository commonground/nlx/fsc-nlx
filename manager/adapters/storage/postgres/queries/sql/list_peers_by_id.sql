-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListPeersByID :many
SELECT
    p.id,
    p.name,
    p.manager_address,
    -- coalesce is used to return an empty string if the value is null. the code generate by sqlc is unable to handle null values in this context.
    coalesce( STRING_AGG(pr.role::text, ', '),'')::text AS roles
FROM peers.peers as p
LEFT JOIN peers.peers_roles pr ON p.id = pr.peer_id
WHERE
    p.name IS NOT NULL AND
    p.manager_address IS NOT NULL AND
    (cardinality(sqlc.arg(PeerIDs)::varchar[]) = 0 OR p.id = ANY(sqlc.arg(PeerIDs)::varchar[]))
GROUP BY p.id;
