-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: ListIncomingConnections :many
SELECT
    p.service_peer_id,
    p.service_name,
    p.content_hash,
    c.created_at,
    c.valid_not_after,
    c.valid_not_before,
    c.state,
    p.hash,
    p.service_publication_delegator_peer_id,
    p.delegator_peer_id,
    p.outway_peer_id
FROM (
         SELECT outway_peer_id, service_peer_id, service_name, content_hash, hash, service_publication_delegator_peer_id, null as delegator_peer_id
         FROM contracts.grants_service_connection
         WHERE service_peer_id = @self_peer_id::text
         UNION DISTINCT
         SELECT outway_peer_id,service_peer_id, service_name, content_hash, hash, service_publication_delegator_peer_id, delegator_peer_id as delegator_peer_id
         FROM contracts.grants_delegated_service_connection
         WHERE service_peer_id = @self_peer_id::text
     ) AS p
         JOIN contracts.contracts_with_state AS c ON c.hash = p.content_hash
WHERE
    CASE
        when cardinality(@contract_states::text[]) = 0 THEN
            content_hash IN (SELECT hash AS content_hash FROM contracts.contracts_with_state)
        else
            content_hash IN (SELECT hash AS content_hash FROM contracts.contracts_with_state where contracts_with_state.state = ANY (@contract_states::text[]))
        end
  AND
    (@pagination_created_at::timestamp is NULL AND @pagination_grant_hash::text = '')
   OR (
    @order_direction::text = 'asc'
            AND
            (c.created_at >= @pagination_created_at::timestamp AND (c.created_at > @pagination_created_at::timestamp OR p.hash > @pagination_grant_hash::text)
        )
        OR (
            @order_direction::text = 'desc'
            AND
            (c.created_at <= @pagination_created_at::timestamp AND (c.created_at < @pagination_created_at::timestamp OR p.hash < @pagination_grant_hash::text))
        )
    )
ORDER BY
-- NOTE: unable to use ASC/DESC dynamically so we use a switch case
CASE
    WHEN @order_direction::text = 'asc' THEN  (c.created_at, p.hash COLLATE "C") END ASC,
CASE
    WHEN @order_direction::text = 'desc' THEN (c.created_at, p.hash COLLATE "C") END DESC
LIMIT $1;

-- name: ListIncomingConnectionsForServices :many
SELECT
    p.service_peer_id,
    p.service_name,
    p.content_hash,
    c.created_at,
    c.valid_not_after,
    c.valid_not_before,
    c.state,
    p.hash,
    p.service_publication_delegator_peer_id,
    p.delegator_peer_id,
    p.outway_peer_id
FROM (
         SELECT outway_peer_id, service_peer_id, service_name, content_hash, hash, service_publication_delegator_peer_id, null as delegator_peer_id
         FROM contracts.grants_service_connection
         WHERE service_peer_id = @self_peer_id::text
         UNION DISTINCT
         SELECT outway_peer_id,service_peer_id, service_name, content_hash, hash, service_publication_delegator_peer_id, delegator_peer_id as delegator_peer_id
         FROM contracts.grants_delegated_service_connection
         WHERE service_peer_id = @self_peer_id::text
     ) AS p
         JOIN contracts.contracts_with_state AS c ON c.hash = p.content_hash
WHERE
    CASE
        when cardinality(@contract_states::text[]) = 0 THEN
            content_hash IN (SELECT hash AS content_hash FROM contracts.contracts_with_state)
        else
            content_hash IN (SELECT hash AS content_hash FROM contracts.contracts_with_state where contracts_with_state.state = ANY (@contract_states::text[]))
        end
  AND
   p.service_name = ANY(sqlc.arg(service_names)::varchar[])
ORDER BY
-- NOTE: unable to use ASC/DESC dynamically so we use a switch case
CASE
    WHEN @order_direction::text = 'asc' THEN  (c.created_at, p.hash COLLATE "C") END ASC,
CASE
    WHEN @order_direction::text = 'desc' THEN (c.created_at, p.hash COLLATE "C") END DESC;
