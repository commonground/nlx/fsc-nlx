-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: GetOutwayServices :many
SELECT
    gsc.hash as grant_hash,
    gsc.service_peer_id as peer_id,
    gsc.service_name
FROM
    contracts.grants_service_connection as gsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gsc.content_hash
WHERE
    gsc.public_key_thumbprint = $1
;

-- name: GetOutwayDelegatedServices :many
SELECT
    gsc.hash as grant_hash,
    gsc.service_peer_id as peer_id,
    gsc.service_name
FROM
    contracts.grants_delegated_service_connection as gsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gsc.content_hash
WHERE
    gsc.public_key_thumbprint = $1
;
