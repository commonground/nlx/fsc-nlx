-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: UpsertContent :one
INSERT INTO contracts.content (
    hash,
    hash_algorithm,
    iv,
    group_id,
    valid_not_before,
    valid_not_after,
    created_at
)
VALUES ($1, $2, $3, $4, $5, $6, $7)
ON CONFLICT DO NOTHING
RETURNING true; -- Return true when content is new, if not, it will return ErrNoRows

-- name: CreateConnectionGrant :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
    VALUES ($1), ($2)
    ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_service_connection (
    outway_peer_id,
    service_peer_id,
    hash,
    content_hash,
    service_name,
    public_key_thumbprint,
    service_type
)
VALUES ($1, $2, $3, $4, $5, $6,'service_type_service');

-- name: CreateConnectionGrantDelegatedService :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
        VALUES ($1), ($2), ($3)
        ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_service_connection (
    outway_peer_id,
    service_peer_id,
    service_publication_delegator_peer_id,
    hash,
    content_hash,
    service_name,
    public_key_thumbprint,
    service_type
)
VALUES ($1, $2, $3, $4, $5, $6, $7,'service_type_delegated_service');


-- name: CreateDelegatedConnectionGrant :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
    VALUES ($1), ($2), ($3)
    ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_delegated_service_connection (
    outway_peer_id,
    delegator_peer_id,
    service_peer_id,
    service_name,
    hash,
    content_hash,
    public_key_thumbprint,
    service_type
)
VALUES ($1, $2, $3, $4, $5, $6, $7, 'service_type_service');

-- name: CreateDelegatedConnectionGrantDelegatedService :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
        VALUES ($1), ($2), ($3), ($4)
        ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_delegated_service_connection (
    outway_peer_id,
    delegator_peer_id,
    service_peer_id,
    service_publication_delegator_peer_id,
    service_name,
    hash,
    content_hash,
    public_key_thumbprint,
    service_type
)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, 'service_type_delegated_service');


-- name: CreateDelegatedPublicationGrant :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
        VALUES ($1), ($2), ($3)
        ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_delegated_service_publication (
    directory_peer_id,
    delegator_peer_id,
    service_peer_id,
    service_name,
    service_protocol,
    hash,
    content_hash
)
VALUES ($1, $2, $3, $4, $5, $6, $7);

-- name: CreatePublicationGrant :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
    VALUES ($1), ($2)
    ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_service_publication (
    directory_peer_id,
    service_peer_id,
    hash,
    content_hash,
    service_name,
    service_protocol
)
VALUES ($1, $2, $3, $4, $5, $6);
