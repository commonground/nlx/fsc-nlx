-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListPeersByName :many
SELECT
    p.id,
    p.name,
    p.manager_address,
    -- coalesce is used to return an empty string if the value is null. the code generate by sqlc is unable to handle null values in this context.
    coalesce( STRING_AGG(pr.role::text, ', '),'')::text AS roles
FROM peers.peers as p
LEFT JOIN peers.peers_roles pr ON p.id = pr.peer_id
WHERE
    p.name IS NOT NULL AND
    p.manager_address IS NOT NULL AND
    lower(p.name) like lower(@name::text) AND
    (
             @pagination_start_id::text = ''
        OR
            (@order_direction::text = 'asc' AND p.id > @pagination_start_id::text)
        OR
            (@order_direction::text = 'desc' AND p.id < @pagination_start_id::text)
    )
GROUP BY p.id
ORDER BY
    CASE
        WHEN @order_direction::text = 'asc' THEN p.id END ASC,
    CASE
        WHEN @order_direction::text = 'desc' THEN p.id END DESC
LIMIT $1;
