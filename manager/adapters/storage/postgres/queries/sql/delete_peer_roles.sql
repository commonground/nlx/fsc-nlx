-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: DeletePeerRoles :exec
DELETE FROM peers.peers_roles WHERE peer_id = $1;
