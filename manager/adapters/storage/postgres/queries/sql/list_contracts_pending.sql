-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: ListContractsPending :many
SELECT
    c.hash as content_hash,
    c.valid_not_before,
    sp.service_peer_id as sp_service_peer_id,
    sp.service_name as sp_service_name,
    sp.service_protocol as sp_service_protocol,
    sp.directory_peer_id as sp_directory_peer_id,
    dsp.service_peer_id as dsp_service_peer_id,
    dsp.service_name as dsp_service_name,
    dsp.service_protocol as dsp_service_protocol,
    dsp.directory_peer_id as dsp_directory_peer_id,
    dsp.delegator_peer_id as dsp_delegator_peer_id,
    sc.service_name as sc_service_name,
    sc.service_peer_id as sc_service_peer_id,
    sc.outway_peer_id as sc_outway_peer_id,
    sc.public_key_thumbprint as sc_public_key_thumbrint,
    sc.service_publication_delegator_peer_id as sc_service_publication_delegator_id,
    dsc.service_peer_id as dsc_service_peer_id,
    dsc.service_name as dsc_service_name,
    dsc.service_publication_delegator_peer_id as dsc_service_public_delegator_peer_id,
    dsc.public_key_thumbprint as dsc_public_key_thumbrint,
    dsc.outway_peer_id as dsc_outway_peer_id,
    dsc.delegator_peer_id as dsc_delegator_peer_id,
    CASE
        WHEN @include_count::boolean THEN count(*) OVER()
    END as total_count
FROM contracts.content AS c
         LEFT JOIN contracts.grants_delegated_service_connection as dsc on c.hash = dsc.content_hash
         LEFT JOIN contracts.grants_service_connection as sc on c.hash = sc.content_hash
         LEFT JOIN contracts.grants_delegated_service_publication as dsp on c.hash = dsp.content_hash
         LEFT JOIN contracts.grants_service_publication as sp on c.hash = sp.content_hash
WHERE
    c.valid_not_after > now()
  AND
    (
        @pagination_start_hash = ''
            OR
        (
            @order_direction::text = 'asc'
                AND
            (
                c.valid_not_before::timestamp > sqlc.arg(pagination_start_date)::timestamp with time zone
                    OR
                (
                    c.valid_not_before::timestamp = sqlc.arg(pagination_start_date)::timestamp with time zone
                        AND
                    c.hash > sqlc.arg(pagination_start_hash)::text
                    )
                )
            )
            OR
        (
            @order_direction::text = 'desc'
                AND
            (
                c.valid_not_before::timestamp < sqlc.arg(pagination_start_date)::timestamp with time zone
                    OR
                (
                    c.valid_not_before::timestamp = sqlc.arg(pagination_start_date)::timestamp with time zone
                        AND
                    c.hash < sqlc.arg(pagination_start_hash)::text
                    )
                )
            )
        )
  AND c.hash NOT IN (SELECT content_hash FROM contracts.contract_signatures WHERE peer_id = sqlc.arg(self_peer_id)::text)
ORDER BY
-- NOTE: unable to use ASC/DESC dynamically so we use a switch case
CASE
    WHEN @order_direction::text = 'asc' THEN c.valid_not_before END ASC,
CASE
    WHEN @order_direction::text = 'asc' THEN c.hash COLLATE "C" END ASC,
CASE
    WHEN @order_direction::text = 'desc' THEN c.valid_not_before END DESC,
CASE
    WHEN @order_direction::text = 'desc' THEN c.hash COLLATE "C" END DESC
LIMIT $1;
