-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: GetTokenInfoServiceConnection :one
SELECT
    c.group_id,
    gsc.service_name as service_name,
    gsc.public_key_thumbprint as public_key_thumbprint,
    gsc.service_publication_delegator_peer_id as service_publication_delegator_peer_id
FROM
    contracts.grants_service_connection as gsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gsc.content_hash
    JOIN contracts.content as c
        ON c.hash = vc.hash
WHERE
    gsc.hash = @hash::text
    AND gsc.outway_peer_id = @outway_peer_id::text
    AND gsc.service_peer_id = @self_peer_id::text;

-- name: GetTokenInfoDelegatedServiceConnection :one
SELECT
    c.group_id,
    gdsc.service_name as service_name,
    gdsc.public_key_thumbprint as public_key_thumbprint,
    gdsc.delegator_peer_id as outway_delegator_peer_id,
    gdsc.service_publication_delegator_peer_id as service_publication_delegator_peer_id
FROM
    contracts.grants_delegated_service_connection as gdsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gdsc.content_hash
    JOIN contracts.content as c
        ON c.hash = vc.hash
WHERE
    gdsc.hash = @hash::text
    AND gdsc.outway_peer_id = @outway_peer_id::text
    AND gdsc.service_peer_id = @self_peer_id::text;
