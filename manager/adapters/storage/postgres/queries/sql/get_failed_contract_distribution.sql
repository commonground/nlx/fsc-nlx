-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: GetFailedContractDistribution :one
SELECT
    peer_id,
    content_hash,
    signature,
    action,
    next_attempt_at,
    reason,
    attempts,
    last_attempt_at
FROM
    contracts.failed_distributions
WHERE
    content_hash = $1
AND
    peer_id = $2
AND
    action = $3;
