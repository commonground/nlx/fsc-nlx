// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: get_certificates_for_contract.sql

package queries

import (
	"context"
)

const listPeerCertificatesForContract = `-- name: ListPeerCertificatesForContract :many

SELECT
    peer_id,
    certificate_thumbprint,
    certificate
FROM peers.certificates
WHERE certificate_thumbprint in (
    SELECT
        certificate_thumbprint
    FROM contracts.contract_signatures
    WHERE
        content_hash = $1
    )
ORDER by peer_id DESC
`

type ListPeerCertificatesForContractRow struct {
	PeerID                string
	CertificateThumbprint string
	Certificate           []byte
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
func (q *Queries) ListPeerCertificatesForContract(ctx context.Context, contentHash string) ([]*ListPeerCertificatesForContractRow, error) {
	rows, err := q.db.Query(ctx, listPeerCertificatesForContract, contentHash)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*ListPeerCertificatesForContractRow{}
	for rows.Next() {
		var i ListPeerCertificatesForContractRow
		if err := rows.Scan(&i.PeerID, &i.CertificateThumbprint, &i.Certificate); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
