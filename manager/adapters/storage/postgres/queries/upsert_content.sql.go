// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: upsert_content.sql

package queries

import (
	"context"

	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v5/pgtype"
)

const createConnectionGrant = `-- name: CreateConnectionGrant :exec

WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
    VALUES ($1), ($2)
    ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_service_connection (
    outway_peer_id,
    service_peer_id,
    hash,
    content_hash,
    service_name,
    public_key_thumbprint,
    service_type
)
VALUES ($1, $2, $3, $4, $5, $6,'service_type_service')
`

type CreateConnectionGrantParams struct {
	OutwayPeerID        string
	ServicePeerID       string
	Hash                string
	ContentHash         string
	ServiceName         string
	PublicKeyThumbprint string
}

// Return true when content is new, if not, it will return ErrNoRows
func (q *Queries) CreateConnectionGrant(ctx context.Context, arg *CreateConnectionGrantParams) error {
	_, err := q.db.Exec(ctx, createConnectionGrant,
		arg.OutwayPeerID,
		arg.ServicePeerID,
		arg.Hash,
		arg.ContentHash,
		arg.ServiceName,
		arg.PublicKeyThumbprint,
	)
	return err
}

const createConnectionGrantDelegatedService = `-- name: CreateConnectionGrantDelegatedService :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
        VALUES ($1), ($2), ($3)
        ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_service_connection (
    outway_peer_id,
    service_peer_id,
    service_publication_delegator_peer_id,
    hash,
    content_hash,
    service_name,
    public_key_thumbprint,
    service_type
)
VALUES ($1, $2, $3, $4, $5, $6, $7,'service_type_delegated_service')
`

type CreateConnectionGrantDelegatedServiceParams struct {
	OutwayPeerID                      string
	ServicePeerID                     string
	ServicePublicationDelegatorPeerID pgtype.Text
	Hash                              string
	ContentHash                       string
	ServiceName                       string
	PublicKeyThumbprint               string
}

func (q *Queries) CreateConnectionGrantDelegatedService(ctx context.Context, arg *CreateConnectionGrantDelegatedServiceParams) error {
	_, err := q.db.Exec(ctx, createConnectionGrantDelegatedService,
		arg.OutwayPeerID,
		arg.ServicePeerID,
		arg.ServicePublicationDelegatorPeerID,
		arg.Hash,
		arg.ContentHash,
		arg.ServiceName,
		arg.PublicKeyThumbprint,
	)
	return err
}

const createDelegatedConnectionGrant = `-- name: CreateDelegatedConnectionGrant :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
    VALUES ($1), ($2), ($3)
    ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_delegated_service_connection (
    outway_peer_id,
    delegator_peer_id,
    service_peer_id,
    service_name,
    hash,
    content_hash,
    public_key_thumbprint,
    service_type
)
VALUES ($1, $2, $3, $4, $5, $6, $7, 'service_type_service')
`

type CreateDelegatedConnectionGrantParams struct {
	OutwayPeerID        string
	DelegatorPeerID     string
	ServicePeerID       string
	ServiceName         string
	Hash                string
	ContentHash         string
	PublicKeyThumbprint string
}

func (q *Queries) CreateDelegatedConnectionGrant(ctx context.Context, arg *CreateDelegatedConnectionGrantParams) error {
	_, err := q.db.Exec(ctx, createDelegatedConnectionGrant,
		arg.OutwayPeerID,
		arg.DelegatorPeerID,
		arg.ServicePeerID,
		arg.ServiceName,
		arg.Hash,
		arg.ContentHash,
		arg.PublicKeyThumbprint,
	)
	return err
}

const createDelegatedConnectionGrantDelegatedService = `-- name: CreateDelegatedConnectionGrantDelegatedService :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
        VALUES ($1), ($2), ($3), ($4)
        ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_delegated_service_connection (
    outway_peer_id,
    delegator_peer_id,
    service_peer_id,
    service_publication_delegator_peer_id,
    service_name,
    hash,
    content_hash,
    public_key_thumbprint,
    service_type
)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, 'service_type_delegated_service')
`

type CreateDelegatedConnectionGrantDelegatedServiceParams struct {
	OutwayPeerID                      string
	DelegatorPeerID                   string
	ServicePeerID                     string
	ServicePublicationDelegatorPeerID pgtype.Text
	ServiceName                       string
	Hash                              string
	ContentHash                       string
	PublicKeyThumbprint               string
}

func (q *Queries) CreateDelegatedConnectionGrantDelegatedService(ctx context.Context, arg *CreateDelegatedConnectionGrantDelegatedServiceParams) error {
	_, err := q.db.Exec(ctx, createDelegatedConnectionGrantDelegatedService,
		arg.OutwayPeerID,
		arg.DelegatorPeerID,
		arg.ServicePeerID,
		arg.ServicePublicationDelegatorPeerID,
		arg.ServiceName,
		arg.Hash,
		arg.ContentHash,
		arg.PublicKeyThumbprint,
	)
	return err
}

const createDelegatedPublicationGrant = `-- name: CreateDelegatedPublicationGrant :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
        VALUES ($1), ($2), ($3)
        ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_delegated_service_publication (
    directory_peer_id,
    delegator_peer_id,
    service_peer_id,
    service_name,
    service_protocol,
    hash,
    content_hash
)
VALUES ($1, $2, $3, $4, $5, $6, $7)
`

type CreateDelegatedPublicationGrantParams struct {
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol ContractsServiceProtocolType
	Hash            string
	ContentHash     string
}

func (q *Queries) CreateDelegatedPublicationGrant(ctx context.Context, arg *CreateDelegatedPublicationGrantParams) error {
	_, err := q.db.Exec(ctx, createDelegatedPublicationGrant,
		arg.DirectoryPeerID,
		arg.DelegatorPeerID,
		arg.ServicePeerID,
		arg.ServiceName,
		arg.ServiceProtocol,
		arg.Hash,
		arg.ContentHash,
	)
	return err
}

const createPublicationGrant = `-- name: CreatePublicationGrant :exec
WITH upsert_peers AS (
    INSERT INTO peers.peers (id)
    VALUES ($1), ($2)
    ON CONFLICT DO NOTHING
)
INSERT INTO contracts.grants_service_publication (
    directory_peer_id,
    service_peer_id,
    hash,
    content_hash,
    service_name,
    service_protocol
)
VALUES ($1, $2, $3, $4, $5, $6)
`

type CreatePublicationGrantParams struct {
	DirectoryPeerID string
	ServicePeerID   string
	Hash            string
	ContentHash     string
	ServiceName     string
	ServiceProtocol ContractsServiceProtocolType
}

func (q *Queries) CreatePublicationGrant(ctx context.Context, arg *CreatePublicationGrantParams) error {
	_, err := q.db.Exec(ctx, createPublicationGrant,
		arg.DirectoryPeerID,
		arg.ServicePeerID,
		arg.Hash,
		arg.ContentHash,
		arg.ServiceName,
		arg.ServiceProtocol,
	)
	return err
}

const upsertContent = `-- name: UpsertContent :one

INSERT INTO contracts.content (
    hash,
    hash_algorithm,
    iv,
    group_id,
    valid_not_before,
    valid_not_after,
    created_at
)
VALUES ($1, $2, $3, $4, $5, $6, $7)
ON CONFLICT DO NOTHING
RETURNING true
`

type UpsertContentParams struct {
	Hash           string
	HashAlgorithm  ContractsContentHashAlgorithm
	Iv             uuid.UUID
	GroupID        string
	ValidNotBefore pgtype.Timestamptz
	ValidNotAfter  pgtype.Timestamptz
	CreatedAt      pgtype.Timestamptz
}

// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
func (q *Queries) UpsertContent(ctx context.Context, arg *UpsertContentParams) (bool, error) {
	row := q.db.QueryRow(ctx, upsertContent,
		arg.Hash,
		arg.HashAlgorithm,
		arg.Iv,
		arg.GroupID,
		arg.ValidNotBefore,
		arg.ValidNotAfter,
		arg.CreatedAt,
	)
	var column_1 bool
	err := row.Scan(&column_1)
	return column_1, err
}
