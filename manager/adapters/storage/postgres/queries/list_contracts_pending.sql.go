// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: list_contracts_pending.sql

package queries

import (
	"context"

	"github.com/jackc/pgx/v5/pgtype"
)

const listContractsPending = `-- name: ListContractsPending :many

SELECT
    c.hash as content_hash,
    c.valid_not_before,
    sp.service_peer_id as sp_service_peer_id,
    sp.service_name as sp_service_name,
    sp.service_protocol as sp_service_protocol,
    sp.directory_peer_id as sp_directory_peer_id,
    dsp.service_peer_id as dsp_service_peer_id,
    dsp.service_name as dsp_service_name,
    dsp.service_protocol as dsp_service_protocol,
    dsp.directory_peer_id as dsp_directory_peer_id,
    dsp.delegator_peer_id as dsp_delegator_peer_id,
    sc.service_name as sc_service_name,
    sc.service_peer_id as sc_service_peer_id,
    sc.outway_peer_id as sc_outway_peer_id,
    sc.public_key_thumbprint as sc_public_key_thumbrint,
    sc.service_publication_delegator_peer_id as sc_service_publication_delegator_id,
    dsc.service_peer_id as dsc_service_peer_id,
    dsc.service_name as dsc_service_name,
    dsc.service_publication_delegator_peer_id as dsc_service_public_delegator_peer_id,
    dsc.public_key_thumbprint as dsc_public_key_thumbrint,
    dsc.outway_peer_id as dsc_outway_peer_id,
    dsc.delegator_peer_id as dsc_delegator_peer_id,
    CASE
        WHEN $2::boolean THEN count(*) OVER()
    END as total_count
FROM contracts.content AS c
         LEFT JOIN contracts.grants_delegated_service_connection as dsc on c.hash = dsc.content_hash
         LEFT JOIN contracts.grants_service_connection as sc on c.hash = sc.content_hash
         LEFT JOIN contracts.grants_delegated_service_publication as dsp on c.hash = dsp.content_hash
         LEFT JOIN contracts.grants_service_publication as sp on c.hash = sp.content_hash
WHERE
    c.valid_not_after > now()
  AND
    (
        $3 = ''
            OR
        (
            $4::text = 'asc'
                AND
            (
                c.valid_not_before::timestamp > $5::timestamp with time zone
                    OR
                (
                    c.valid_not_before::timestamp = $5::timestamp with time zone
                        AND
                    c.hash > $3::text
                    )
                )
            )
            OR
        (
            $4::text = 'desc'
                AND
            (
                c.valid_not_before::timestamp < $5::timestamp with time zone
                    OR
                (
                    c.valid_not_before::timestamp = $5::timestamp with time zone
                        AND
                    c.hash < $3::text
                    )
                )
            )
        )
  AND c.hash NOT IN (SELECT content_hash FROM contracts.contract_signatures WHERE peer_id = $6::text)
ORDER BY
CASE
    WHEN $4::text = 'asc' THEN c.valid_not_before END ASC,
CASE
    WHEN $4::text = 'asc' THEN c.hash COLLATE "C" END ASC,
CASE
    WHEN $4::text = 'desc' THEN c.valid_not_before END DESC,
CASE
    WHEN $4::text = 'desc' THEN c.hash COLLATE "C" END DESC
LIMIT $1
`

type ListContractsPendingParams struct {
	Limit               int32
	IncludeCount        bool
	PaginationStartHash interface{}
	OrderDirection      string
	PaginationStartDate pgtype.Timestamptz
	SelfPeerID          string
}

type ListContractsPendingRow struct {
	ContentHash                     string
	ValidNotBefore                  pgtype.Timestamptz
	SpServicePeerID                 pgtype.Text
	SpServiceName                   pgtype.Text
	SpServiceProtocol               NullContractsServiceProtocolType
	SpDirectoryPeerID               pgtype.Text
	DspServicePeerID                pgtype.Text
	DspServiceName                  pgtype.Text
	DspServiceProtocol              NullContractsServiceProtocolType
	DspDirectoryPeerID              pgtype.Text
	DspDelegatorPeerID              pgtype.Text
	ScServiceName                   pgtype.Text
	ScServicePeerID                 pgtype.Text
	ScOutwayPeerID                  pgtype.Text
	ScPublicKeyThumbrint            pgtype.Text
	ScServicePublicationDelegatorID pgtype.Text
	DscServicePeerID                pgtype.Text
	DscServiceName                  pgtype.Text
	DscServicePublicDelegatorPeerID pgtype.Text
	DscPublicKeyThumbrint           pgtype.Text
	DscOutwayPeerID                 pgtype.Text
	DscDelegatorPeerID              pgtype.Text
	TotalCount                      interface{}
}

// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
// NOTE: unable to use ASC/DESC dynamically so we use a switch case
func (q *Queries) ListContractsPending(ctx context.Context, arg *ListContractsPendingParams) ([]*ListContractsPendingRow, error) {
	rows, err := q.db.Query(ctx, listContractsPending,
		arg.Limit,
		arg.IncludeCount,
		arg.PaginationStartHash,
		arg.OrderDirection,
		arg.PaginationStartDate,
		arg.SelfPeerID,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*ListContractsPendingRow{}
	for rows.Next() {
		var i ListContractsPendingRow
		if err := rows.Scan(
			&i.ContentHash,
			&i.ValidNotBefore,
			&i.SpServicePeerID,
			&i.SpServiceName,
			&i.SpServiceProtocol,
			&i.SpDirectoryPeerID,
			&i.DspServicePeerID,
			&i.DspServiceName,
			&i.DspServiceProtocol,
			&i.DspDirectoryPeerID,
			&i.DspDelegatorPeerID,
			&i.ScServiceName,
			&i.ScServicePeerID,
			&i.ScOutwayPeerID,
			&i.ScPublicKeyThumbrint,
			&i.ScServicePublicationDelegatorID,
			&i.DscServicePeerID,
			&i.DscServiceName,
			&i.DscServicePublicDelegatorPeerID,
			&i.DscPublicKeyThumbrint,
			&i.DscOutwayPeerID,
			&i.DscDelegatorPeerID,
			&i.TotalCount,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
