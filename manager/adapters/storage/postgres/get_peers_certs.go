// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetPeersCertsByCertThumbprints(ctx context.Context, thumbprints contract.CertificateThumbprints) (*contract.PeersCertificates, error) {
	rows, err := r.queries.GetCertificatesForPeersByCertThumbprints(ctx, thumbprints.Value())
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificates from database")
	}

	peersCertificates := make(contract.NewPeersCertificatesArgs, 0)

	for _, r := range rows {
		if peersCertificates[r.PeerID] == nil {
			peersCertificates[r.PeerID] = make([][]byte, 0)
		}

		peersCertificates[r.PeerID] = append(peersCertificates[r.PeerID], r.Certificate)
	}

	model, err := contract.NewPeersCertificates(r.clock, r.trustedRootCAs, peersCertificates)
	if err != nil {
		return nil, errors.Wrap(err, "could not create contract certificates from database certificates")
	}

	return &model, nil
}
