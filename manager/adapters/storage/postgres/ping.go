// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"errors"

	"github.com/jackc/pgx/v5/pgxpool"
	"golang.org/x/net/context"
)

func (p *PostgreSQLRepository) Ping(ctx context.Context) error {
	pool, ok := p.db.(*pgxpool.Pool)
	if !ok {
		return errors.New("could not retrieve database connection")
	}

	return pool.Ping(ctx)
}
