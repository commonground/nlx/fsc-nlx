// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"fmt"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

//nolint:gocyclo,funlen // complex mapping due to improper support coalesce in sqlc
func (r *PostgreSQLRepository) ListContractsPending(args *contract.ListContractsPendingArgs) (*contract.PendingContracts, error) {
	pendingContractRows, err := r.queries.ListContractsPending(args.Ctx, &queries.ListContractsPendingParams{
		Limit:               int32(args.PaginationLimit),
		SelfPeerID:          string(args.SelfPeerID),
		PaginationStartHash: args.PaginationStartHash,
		OrderDirection:      string(args.PaginationSortOrder),
		PaginationStartDate: pgtype.Timestamptz{
			Time:  args.PaginationStartDate,
			Valid: true,
		},
		IncludeCount: args.IncludeCount,
	})
	if err != nil {
		return nil, fmt.Errorf("could not retrieve pending contracts from database: %v", err)
	}

	pendingContracts := make(map[string]*contract.PendingContract)

	if len(pendingContractRows) == 0 {
		return &contract.PendingContracts{}, nil
	}

	var contracts []*contract.PendingContract

	for _, row := range pendingContractRows {
		pc, ok := pendingContracts[row.ContentHash]
		if !ok {
			pc = &contract.PendingContract{
				ContentHash:    row.ContentHash,
				ValidNotBefore: row.ValidNotBefore.Time,
				Grants:         make([]*contract.ContractPendingGrant, 0),
			}

			contracts = append(contracts, pc)
		}

		if row.DscOutwayPeerID.Valid {
			pc.Grants = append(pc.Grants, &contract.ContractPendingGrant{
				ServiceName:                       row.DscServiceName.String,
				ServicePeerID:                     contract.PeerID(row.DscServicePeerID.String),
				OutwayPeerID:                      contract.PeerID(row.DscOutwayPeerID.String),
				OutwayThumbprint:                  row.DscPublicKeyThumbrint.String,
				DelegatorPeerID:                   contract.PeerID(row.DscDelegatorPeerID.String),
				ServicePublicationDelegatorPeerID: contract.PeerID(row.DscServicePublicDelegatorPeerID.String),
				GrantType:                         contract.GrantTypeDelegatedServiceConnection,
			})
		}

		if row.ScOutwayPeerID.Valid {
			pc.Grants = append(pc.Grants, &contract.ContractPendingGrant{
				ServiceName:                       row.ScServiceName.String,
				ServicePeerID:                     contract.PeerID(row.ScServicePeerID.String),
				OutwayPeerID:                      contract.PeerID(row.ScOutwayPeerID.String),
				OutwayThumbprint:                  row.ScPublicKeyThumbrint.String,
				ServicePublicationDelegatorPeerID: contract.PeerID(row.ScServicePublicationDelegatorID.String),
				GrantType:                         contract.GrantTypeServiceConnection,
			})
		}

		if row.DspServiceName.Valid {
			v, err := row.DspServiceProtocol.Value()
			if err != nil {
				return nil, fmt.Errorf("could not retrieve service protocol from database: %v", err)
			}

			var sp contract.ServiceProtocol

			switch v {
			case string(queries.ContractsServiceProtocolTypePROTOCOLTCPHTTP11):
				sp = contract.ServiceProtocolTCPHTTP1_1
			case string(queries.ContractsServiceProtocolTypePROTOCOLTCPHTTP2):
				sp = contract.ServiceProtocolTCPHTTP2
			default:
				sp = contract.ServiceProtocolUnspecified
			}

			pc.Grants = append(pc.Grants, &contract.ContractPendingGrant{
				ServiceName:     row.DspServiceName.String,
				ServicePeerID:   contract.PeerID(row.DspServicePeerID.String),
				ServiceProtocol: sp,
				DelegatorPeerID: contract.PeerID(row.DspDelegatorPeerID.String),
				DirectoryPeerID: contract.PeerID(row.DspDirectoryPeerID.String),
				GrantType:       contract.GrantTypeDelegatedServicePublication,
			})
		}

		if row.SpServiceName.Valid {
			v, err := row.SpServiceProtocol.Value()
			if err != nil {
				return nil, fmt.Errorf("could not retrieve service protocol from database: %v", err)
			}

			var sp contract.ServiceProtocol

			switch v {
			case string(queries.ContractsServiceProtocolTypePROTOCOLTCPHTTP11):
				sp = contract.ServiceProtocolTCPHTTP1_1
			case string(queries.ContractsServiceProtocolTypePROTOCOLTCPHTTP2):
				sp = contract.ServiceProtocolTCPHTTP2
			default:
				sp = contract.ServiceProtocolUnspecified
			}

			pc.Grants = append(pc.Grants, &contract.ContractPendingGrant{
				ServiceName:                       row.SpServiceName.String,
				ServicePeerID:                     contract.PeerID(row.SpServicePeerID.String),
				ServicePublicationDelegatorPeerID: contract.PeerID(row.ScServicePublicationDelegatorID.String),
				ServiceProtocol:                   sp,
				DirectoryPeerID:                   contract.PeerID(row.SpDirectoryPeerID.String),
				GrantType:                         contract.GrantTypeServicePublication,
			})
		}

		pendingContracts[row.ContentHash] = pc
	}

	contractsPending := &contract.PendingContracts{
		PendingContracts: contracts,
	}

	count, ok := (pendingContractRows[0].TotalCount).(int64)
	if ok {
		contractsPending.Count = int(count)
	}

	return contractsPending, nil
}
