// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"fmt"
	"time"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListServicePublications(args *contract.ListPublicationsArgs) (*contract.Publications, error) {
	contractStatesFilter := make([]string, len(args.ContractStates))

	for i, cState := range args.ContractStates {
		contractStatesFilter[i] = convertContractStateToDB(cState)
	}

	if len(args.ServiceNames) > 0 {
		servicePublicationRows, err := r.queries.ListServicePublicationsForServices(args.Ctx, &queries.ListServicePublicationsForServicesParams{
			SelfPeerID:     string(args.PeerID),
			OrderDirection: string(args.PaginationSortOrder),
			ContractStates: contractStatesFilter,
			ServiceNames:   args.ServiceNames,
		})
		if err != nil {
			return nil, fmt.Errorf("could not retrieve service publications from database: %v", err)
		}

		publications := make([]*contract.Publication, len(servicePublicationRows))

		for i, spr := range servicePublicationRows {
			publication := &contract.Publication{
				ServicePeerID:   args.PeerID,
				ServiceName:     spr.ServiceName,
				ContentHash:     spr.ContentHash,
				CreatedAt:       spr.CreatedAt.Time,
				ValidNotBefore:  spr.ValidNotBefore.Time,
				ValidNotAfter:   spr.ValidNotAfter.Time,
				ContractState:   convertDBContractStateToDomain(spr.State),
				GrantHash:       spr.Hash,
				DirectoryPeerID: contract.PeerID(spr.DirectoryPeerID),
				DelegatorPeerID: convertDelegatorPeerID(spr.DelegatorPeerID),
			}

			publications[i] = publication
		}

		return &contract.Publications{
			Publications: publications,
		}, nil
	} else {
		paginationCreatedAt := pgtype.Timestamp{
			Time:             time.Time{},
			InfinityModifier: 0,
			Valid:            false,
		}

		if args.PaginationCreatedAt != 0 {
			paginationCreatedAt.Time = time.Unix(args.PaginationCreatedAt, 0).UTC()
			paginationCreatedAt.Valid = true
		}

		servicePublicationRows, err := r.queries.ListServicePublications(args.Ctx, &queries.ListServicePublicationsParams{
			Limit:               int32(args.PaginationLimit),
			SelfPeerID:          string(args.PeerID),
			PaginationCreatedAt: paginationCreatedAt,
			PaginationGrantHash: args.PaginationGrantHash,
			OrderDirection:      string(args.PaginationSortOrder),
			ContractStates:      contractStatesFilter,
		})
		if err != nil {
			return nil, fmt.Errorf("could not retrieve service publications from database: %v", err)
		}

		if len(servicePublicationRows) == 0 {
			return &contract.Publications{}, nil
		}

		totalAmount := 0

		if args.IncludeCount {
			count, err := r.queries.ListServicePublicationsCount(args.Ctx, &queries.ListServicePublicationsCountParams{
				SelfPeerID:     string(args.PeerID),
				ContractStates: contractStatesFilter,
			})
			if err != nil {
				return nil, fmt.Errorf("could not retrieve service publications count from database: %v", err)
			}

			totalAmount = int(count)
		}

		publications := make([]*contract.Publication, len(servicePublicationRows))

		for i, spr := range servicePublicationRows {
			publication := &contract.Publication{
				ServicePeerID:   args.PeerID,
				ServiceName:     spr.ServiceName,
				ContentHash:     spr.ContentHash,
				CreatedAt:       spr.CreatedAt.Time,
				ValidNotBefore:  spr.ValidNotBefore.Time,
				ValidNotAfter:   spr.ValidNotAfter.Time,
				ContractState:   convertDBContractStateToDomain(spr.State),
				GrantHash:       spr.Hash,
				DirectoryPeerID: contract.PeerID(spr.DirectoryPeerID),
				DelegatorPeerID: convertDelegatorPeerID(spr.DelegatorPeerID),
			}

			publications[i] = publication
		}

		res := &contract.Publications{
			Publications: publications,
			Count:        totalAmount,
		}

		return res, nil
	}
}

func convertDelegatorPeerID(rowDelegatorPeerID interface{}) contract.PeerID {
	if rowDelegatorPeerID == nil {
		return ""
	}

	delegatorPeerID, err := contract.NewPeerID(rowDelegatorPeerID.(string))
	if err != nil {
		return ""
	}

	return delegatorPeerID
}
