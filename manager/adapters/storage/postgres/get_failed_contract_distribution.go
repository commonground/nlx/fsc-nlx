// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetFailedContractDistribution(ctx context.Context, contentHash *contract.ContentHash, peerID contract.PeerID, action contract.FailedDistributionAction) (*contract.FailedDistribution, error) {
	row, err := r.queries.GetFailedContractDistribution(ctx, &queries.GetFailedContractDistributionParams{
		ContentHash: contentHash.String(),
		PeerID:      peerID.Value(),
		Action:      queries.ContractsDistributionAction(action),
	})
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}

		return nil, err
	}

	var nextRetryAt *time.Time
	if row.NextAttemptAt.Valid {
		nextRetryAt = &row.NextAttemptAt.Time
	}

	result, err := contract.NewFailedDistribution(&contract.NewFailedDistributionArgs{
		ContentHash:   contentHash,
		PeerID:        peerID.Value(),
		Action:        string(action),
		Signature:     row.Signature,
		Attempts:      row.Attempts,
		NextAttemptAt: nextRetryAt,
		LastAttemptAt: row.LastAttemptAt.Time,
		Reason:        row.Reason,
	})
	if err != nil {
		return nil, fmt.Errorf("invalid failed distribution in db: %s", err)
	}

	return result, nil
}
