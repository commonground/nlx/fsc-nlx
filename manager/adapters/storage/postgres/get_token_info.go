// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetTokenInfo(ctx context.Context, outwayPeer, selfPeer contract.PeerID, grantHash *contract.Hash) (*contract.TokenInfo, error) {
	switch grantHash.Type() {
	case contract.HashTypeGrantServiceConnection:
		infoRow, err := r.queries.GetTokenInfoServiceConnection(ctx, &queries.GetTokenInfoServiceConnectionParams{
			Hash:         grantHash.String(),
			OutwayPeerID: outwayPeer.Value(),
			SelfPeerID:   selfPeer.Value(),
		})
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				return nil, contract.ErrTokenInfoNotFound
			}

			return nil, errors.Wrap(err, "could not get service connection info from database")
		}

		return &contract.TokenInfo{
			GroupID:                infoRow.GroupID,
			ServiceName:            infoRow.ServiceName,
			ServiceDelegatorPeerID: infoRow.ServicePublicationDelegatorPeerID.String,
			PublicKeyThumbprint:    infoRow.PublicKeyThumbprint,
		}, nil
	case contract.HashTypeGrantDelegatedServiceConnection:
		infoRow, err := r.queries.GetTokenInfoDelegatedServiceConnection(ctx, &queries.GetTokenInfoDelegatedServiceConnectionParams{
			Hash:         grantHash.String(),
			OutwayPeerID: outwayPeer.Value(),
			SelfPeerID:   selfPeer.Value(),
		})
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				return nil, contract.ErrTokenInfoNotFound
			}

			return nil, errors.Wrap(err, "could not get delegated service connection info from database")
		}

		return &contract.TokenInfo{
			GroupID:                infoRow.GroupID,
			ServiceName:            infoRow.ServiceName,
			OutwayDelegatorPeerID:  infoRow.OutwayDelegatorPeerID,
			ServiceDelegatorPeerID: infoRow.ServicePublicationDelegatorPeerID.String,
			PublicKeyThumbprint:    infoRow.PublicKeyThumbprint,
		}, nil
	default:
		return nil, fmt.Errorf("invalid grant hash type, only service connection and delegated service connection types are allowed")
	}
}
