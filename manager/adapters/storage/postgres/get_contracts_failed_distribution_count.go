// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
)

func (r *PostgreSQLRepository) GetContractFailedDistributionCount(ctx context.Context) (int, error) {
	c, err := r.queries.GetFailedDistributionCount(ctx)
	if err != nil {
		return 0, err
	}

	return int(c), nil
}
