// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"time"

	"github.com/jackc/pgx/v5/pgtype"
)

func (r *PostgreSQLRepository) GetContractsWaitingForSignatureCount(ctx context.Context, createdBeforeDate *time.Time) (int, error) {
	createdBefore := pgtype.Timestamptz{
		Valid: false,
	}

	if createdBeforeDate != nil {
		createdBefore.Time = *createdBeforeDate
		createdBefore.Valid = true
	}

	c, err := r.queries.GetContractsWaitingForSignatureCount(ctx, createdBefore)
	if err != nil {
		return 0, err
	}

	return int(c), nil
}
