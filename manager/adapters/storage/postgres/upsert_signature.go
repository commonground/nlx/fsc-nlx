// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"github.com/jackc/pgx/v5/pgtype"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) UpsertSignature(ctx context.Context, s *contract.Signature) error {
	err := r.queries.UpsertSignature(ctx, &queries.UpsertSignatureParams{
		ContentHash:           s.ContentHash().String(),
		SignatureType:         mapSignatureType(s.Type()),
		PeerID:                s.Peer().ID().Value(),
		CertificateThumbprint: s.CertificateThumbprint(),
		Signature:             s.JWS(),
		Certificate:           s.PeerCert().Raw(),
		SignedAt:              pgtype.Timestamptz{Time: s.SignedAt(), Valid: true},
	})
	if err != nil {
		return errors.Wrap(err, "failed to create signature")
	}

	return nil
}

func mapSignatureType(t contract.SignatureType) queries.ContractsSignatureType {
	switch t {
	case contract.SignatureTypeAccept:
		return queries.ContractsSignatureTypeAccept
	case contract.SignatureTypeReject:
		return queries.ContractsSignatureTypeReject
	case contract.SignatureTypeRevoke:
		return queries.ContractsSignatureTypeRevoke
	default:
		return ""
	}
}
