// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"crypto/x509"
	"fmt"
	"time"

	"github.com/gofrs/uuid"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func getContractFromDB(ctx context.Context, c clock.Clock, q *queries.Queries, certs *x509.CertPool, args *createContractArgs) (*contract.Contract, error) {
	grants, err := getGrantsByContractHash(ctx, q, args.contentHash)
	if err != nil {
		return nil, err
	}

	signatures, err := getSignaturesByContractHash(ctx, q, args.contentHash)
	if err != nil {
		return nil, err
	}

	peersCerts, err := getPeersCertsByContractHash(ctx, c, certs, q, args.contentHash)
	if err != nil {
		return nil, err
	}

	model, err := contract.NewContract(&contract.NewContractArgs{
		Content: &contract.NewContentArgs{
			Clock:         c,
			IV:            args.IV.Bytes(),
			HashAlgorithm: hashAlgToModel(args.hashAlgorithm),
			GroupID:       args.groupID,
			Validity: &contract.NewValidityArgs{
				NotBefore: args.notBefore,
				NotAfter:  args.notAfter,
			},
			Grants:    grants,
			CreatedAt: args.createdAt,
		},
		SignaturesAccepted: signatures.accepted,
		SignaturesRejected: signatures.rejected,
		SignaturesRevoked:  signatures.revoked,
		PeersCerts:         peersCerts,
	})
	if err != nil {
		return nil, fmt.Errorf("invalid contract in database: %v", err)
	}

	return model, nil
}

func hashAlgToModel(a queries.ContractsContentHashAlgorithm) contract.HashAlg {
	switch a {
	case queries.ContractsContentHashAlgorithmSha3512:
		return contract.HashAlgSHA3_512
	default:
		return contract.HashAlg(0)
	}
}

type createContractArgs struct {
	IV            uuid.UUID
	contentHash   string
	groupID       string
	hashAlgorithm queries.ContractsContentHashAlgorithm
	createdAt     time.Time
	notBefore     time.Time
	notAfter      time.Time
}

func getPeersCertsByContractHash(ctx context.Context, c clock.Clock, trustedRootCAs *x509.CertPool, q *queries.Queries, contentHash string) (contract.PeersCertificates, error) {
	certificateRows, err := q.ListPeerCertificatesForContract(ctx, contentHash)
	if err != nil {
		return nil, err
	}

	peerCertificatesMap := make(contract.NewPeersCertificatesArgs)

	for _, certificateRow := range certificateRows {
		certificateBytes := make([][]byte, 0)
		certificateBytes = append(certificateBytes, certificateRow.Certificate)
		peerCertificatesMap[certificateRow.PeerID] = certificateBytes
	}

	peersCerts, err := contract.NewPeersCertificates(c, trustedRootCAs, peerCertificatesMap)
	if err != nil {
		return nil, err
	}

	return peersCerts, nil
}
