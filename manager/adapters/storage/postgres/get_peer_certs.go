// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"crypto/x509"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetPeerCerts(ctx context.Context, id contract.PeerID) (*contract.PeerCertificates, error) {
	rows, err := r.queries.GetCertificatesForPeer(ctx, id.Value())
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificates database")
	}

	peerCertificates := make(contract.RawDERPeerCertificates, len(rows))

	count := copy(peerCertificates, rows)
	if count != len(rows) {
		return nil, fmt.Errorf("could not copy rows to peer certificates, invalid amount copied, got: %d, want %d", count, len(rows))
	}

	// filter out expired certificates
	nonExpiredPeerCertificates, err := filterOutNotStartedAndExpiredCertificates(r.clock, peerCertificates)
	if err != nil {
		return nil, errors.Wrap(err, "failed to filter out not started and expired certificates from database certificates")
	}

	model, err := contract.NewPeerCertificates(r.clock, r.trustedRootCAs, nonExpiredPeerCertificates)
	if err != nil {
		return nil, errors.Wrap(err, "could not create contract certificates from database certificates")
	}

	return model, nil
}

func filterOutNotStartedAndExpiredCertificates(c clock.Clock, peerCerts contract.RawDERPeerCertificates) (contract.RawDERPeerCertificates, error) {
	result := contract.RawDERPeerCertificates{}

	for _, peerCert := range peerCerts {
		certificates, err := x509.ParseCertificates(peerCert)
		if err != nil {
			return result, errors.Wrap(err, "could not create peer certificate, invalid certificate bytes")
		}

		if len(certificates) == 0 {
			continue
		}

		atLeastOneCertificateHasExpired := false

		for _, cert := range certificates {
			if c.Now().Before(cert.NotBefore) || cert.NotAfter.Before(c.Now()) {
				atLeastOneCertificateHasExpired = true
			}
		}

		if atLeastOneCertificateHasExpired {
			continue
		}

		result = append(result, peerCert)
	}

	return result, nil
}
