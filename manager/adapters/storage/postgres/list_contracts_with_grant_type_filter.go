// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:dupl // similar but not duplicate
func (r *PostgreSQLRepository) ListContractsWithGrantTypeFilter(ctx context.Context, args *contract.ListContractsWithGrantTypeFilterArgs) ([]*contract.Contract, error) {
	contractRows, err := r.queries.ListContractsWithGrantTypeFilter(ctx, &queries.ListContractsWithGrantTypeFilterParams{
		Limit: int32(args.PaginationLimit),
		PaginationStartDate: pgtype.Timestamptz{
			Time:  args.PaginationStartDate,
			Valid: true,
		},
		PaginationStartHash:                   args.PaginationStartHash,
		OrderDirection:                        string(args.PaginationSortOrder),
		WithServiceConnectionGrants:           args.WithServiceConnectionGrants,
		WithDelegatedServiceConnectionGrants:  args.WithDelegatedServiceConnectionGrants,
		WithServicePublicationGrants:          args.WithServicePublicationGrants,
		WithDelegatedServicePublicationGrants: args.WithDelegatedServicePublicationGrants,
	})
	if err != nil {
		return nil, fmt.Errorf("could not list contracts with grant type filter from database: %v", err)
	}

	if len(contractRows) == 0 {
		return []*contract.Contract{}, nil
	}

	result := make([]*contract.Contract, len(contractRows))

	for i, contractRow := range contractRows {
		model, contractErr := getContractFromDB(ctx, r.clock, r.queries, r.trustedRootCAs, &createContractArgs{
			IV:            contractRow.ContentIv,
			contentHash:   contractRow.ContentHash,
			groupID:       contractRow.ContentGroupID,
			hashAlgorithm: contractRow.ContentHashAlgorithm,
			createdAt:     contractRow.ContentCreatedAt.Time,
			notBefore:     contractRow.ContentValidNotBefore.Time,
			notAfter:      contractRow.ContentValidNotAfter.Time,
		})
		if contractErr != nil {
			return nil, fmt.Errorf("invalid contract in database: %v", contractErr)
		}

		result[i] = model
	}

	return result, nil
}
