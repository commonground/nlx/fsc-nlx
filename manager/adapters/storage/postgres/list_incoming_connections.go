// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListIncomingConnections(ctx context.Context, args *contract.ListIncomingConnectionsArgs) (*contract.IncomingConnections, error) {
	contractStatesFilter := make([]string, len(args.ContractStates))

	for i, cState := range args.ContractStates {
		contractStatesFilter[i] = convertContractStateToDB(cState)
	}

	if len(args.ServiceNames) > 0 {
		incomingConnectionRows, err := r.queries.ListIncomingConnectionsForServices(ctx, &queries.ListIncomingConnectionsForServicesParams{
			SelfPeerID:     string(args.PeerID),
			ContractStates: contractStatesFilter,
			ServiceNames:   args.ServiceNames,
		})
		if err != nil {
			return nil, fmt.Errorf("could not retrieve incoming connection for services from database: %v", err)
		}

		connections := make([]*contract.Connection, len(incomingConnectionRows))

		//nolint:dupl // looks the same as ListOutgoingConnections but is different
		for i, cr := range incomingConnectionRows {
			c := &contract.Connection{
				ServicePeerID:  contract.PeerID(cr.ServicePeerID),
				ServiceName:    cr.ServiceName,
				ContentHash:    cr.ContentHash,
				CreatedAt:      cr.CreatedAt.Time,
				ValidNotBefore: cr.ValidNotBefore.Time,
				ValidNotAfter:  cr.ValidNotAfter.Time,
				ContractState:  convertDBContractStateToDomain(cr.State),
				GrantHash:      cr.Hash,
				OutwayPeerID:   contract.PeerID(cr.OutwayPeerID),
			}

			if cr.ServicePublicationDelegatorPeerID.Valid {
				pID := contract.PeerID(cr.ServicePublicationDelegatorPeerID.String)
				c.ServicePublicationDelegatorPeerID = &pID
			}

			if cr.DelegatorPeerID != nil {
				dpID, errConv := contract.NewPeerID(cr.DelegatorPeerID.(string))
				if errConv != nil {
					return nil, fmt.Errorf("could not convert delegator peer ID: %v", errConv)
				}

				c.DelegatorPeerID = &dpID
			}

			connections[i] = c
		}

		return &contract.IncomingConnections{Connections: connections}, nil
	} else {
		paginationCreatedAt := pgtype.Timestamp{
			Time:             time.Time{},
			InfinityModifier: 0,
			Valid:            false,
		}

		if args.PaginationCreatedAt != 0 {
			paginationCreatedAt.Time = time.Unix(args.PaginationCreatedAt, 0).UTC()
			paginationCreatedAt.Valid = true
		}

		incomingConnectionRows, err := r.queries.ListIncomingConnections(ctx, &queries.ListIncomingConnectionsParams{
			Limit:               int32(args.PaginationLimit),
			SelfPeerID:          string(args.PeerID),
			PaginationCreatedAt: paginationCreatedAt,
			PaginationGrantHash: args.PaginationGrantHash,
			OrderDirection:      string(args.PaginationSortOrder),
			ContractStates:      contractStatesFilter,
		})
		if err != nil {
			return nil, fmt.Errorf("could not retrieve incoming connection from database: %v", err)
		}

		if len(incomingConnectionRows) == 0 {
			return &contract.IncomingConnections{}, nil
		}

		totalAmount := 0

		if args.IncludeCount {
			count, err := r.queries.ListIncomingConnectionsCount(args.Ctx, &queries.ListIncomingConnectionsCountParams{
				SelfPeerID:     string(args.PeerID),
				ContractStates: contractStatesFilter,
			})
			if err != nil {
				return nil, fmt.Errorf("could not retrieve incoming connections count from database: %v", err)
			}

			totalAmount = int(count)
		}

		connections := make([]*contract.Connection, len(incomingConnectionRows))

		//nolint:dupl // looks the same as ListOutgoingConnections but is different
		for i, cr := range incomingConnectionRows {
			c := &contract.Connection{
				ServicePeerID:  contract.PeerID(cr.ServicePeerID),
				ServiceName:    cr.ServiceName,
				ContentHash:    cr.ContentHash,
				CreatedAt:      cr.CreatedAt.Time,
				ValidNotBefore: cr.ValidNotBefore.Time,
				ValidNotAfter:  cr.ValidNotAfter.Time,
				ContractState:  convertDBContractStateToDomain(cr.State),
				GrantHash:      cr.Hash,
				OutwayPeerID:   contract.PeerID(cr.OutwayPeerID),
			}

			if cr.ServicePublicationDelegatorPeerID.Valid {
				pID := contract.PeerID(cr.ServicePublicationDelegatorPeerID.String)
				c.ServicePublicationDelegatorPeerID = &pID
			}

			if cr.DelegatorPeerID != nil {
				dpID, errConv := contract.NewPeerID(cr.DelegatorPeerID.(string))
				if errConv != nil {
					return nil, fmt.Errorf("could not convert delegator peer ID: %v", errConv)
				}

				c.DelegatorPeerID = &dpID
			}

			connections[i] = c
		}

		return &contract.IncomingConnections{
			Count:       totalAmount,
			Connections: connections,
		}, nil
	}
}
