// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:dupl // similar but not duplicate
func (r *PostgreSQLRepository) ListContractsForPeer(ctx context.Context, peerID contract.PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder contract.SortOrder) ([]*contract.Contract, error) {
	contractRows, err := r.queries.ListContractsForPeer(ctx, &queries.ListContractsForPeerParams{
		PaginationStartID: paginationStartID,
		PeerID:            string(peerID),
		Limit:             int32(paginationLimit),
		OrderDirection:    string(paginationSortOrder),
	})
	if err != nil {
		return nil, fmt.Errorf("could not list contracts for peer with service connection grant from database: %v", err)
	}

	if len(contractRows) == 0 {
		return []*contract.Contract{}, nil
	}

	result := make([]*contract.Contract, len(contractRows))

	for i, contractRow := range contractRows {
		model, contractErr := getContractFromDB(ctx, r.clock, r.queries, r.trustedRootCAs, &createContractArgs{
			IV:            contractRow.ContentIv,
			contentHash:   contractRow.ContentHash,
			groupID:       contractRow.ContentGroupID,
			hashAlgorithm: contractRow.ContentHashAlgorithm,
			createdAt:     contractRow.ContentCreatedAt.Time,
			notBefore:     contractRow.ContentValidNotBefore.Time,
			notAfter:      contractRow.ContentValidNotAfter.Time,
		})
		if contractErr != nil {
			return nil, fmt.Errorf("invalid contract in database: %v", contractErr)
		}

		result[i] = model
	}

	return result, nil
}

func getGrantsByContractHash(ctx context.Context, q *queries.Queries, hash string) ([]interface{}, error) {
	result := make([]interface{}, 0)

	serviceConnectionGrants, err := getServiceConnectionGrantsByContractHash(ctx, q, hash)
	if err != nil {
		return nil, err
	}

	for _, serviceConnectionGrant := range serviceConnectionGrants {
		result = append(result, serviceConnectionGrant)
	}

	servicePublicationGrants, err := getServicePublicationGrantsByContractHash(ctx, q, hash)
	if err != nil {
		return nil, err
	}

	for _, servicePublicationGrant := range servicePublicationGrants {
		result = append(result, servicePublicationGrant)
	}

	delegatedServiceConnectionGrants, err := getDelegatedServiceConnectionGrantsByContractHash(ctx, q, hash)
	if err != nil {
		return nil, err
	}

	for _, delegatedServiceConnectionGrant := range delegatedServiceConnectionGrants {
		result = append(result, delegatedServiceConnectionGrant)
	}

	delegatedServicePublicationGrants, err := getDelegatedServicePublicationGrantsByContractHash(ctx, q, hash)
	if err != nil {
		return nil, err
	}

	for _, delegatedServicePublicationGrant := range delegatedServicePublicationGrants {
		result = append(result, delegatedServicePublicationGrant)
	}

	return result, nil
}

func getServiceConnectionGrantsByContractHash(ctx context.Context, q *queries.Queries, hash string) ([]*contract.NewGrantServiceConnectionArgs, error) {
	grantRows, err := q.ListServiceConnectionGrantsForContract(ctx, hash)
	if err != nil {
		return nil, fmt.Errorf("could not list service connection grants for contract from database: %v", err)
	}

	result := make([]*contract.NewGrantServiceConnectionArgs, len(grantRows))

	for j, grantRow := range grantRows {
		var service interface{}

		switch grantRow.ServiceType {
		case queries.ContractsServiceTypeServiceTypeService:
			service = &contract.NewGrantServiceConnectionServiceArgs{
				Name: grantRow.ServiceName,
				Peer: &contract.NewPeerArgs{
					ID: grantRow.ServicePeerID,
				},
			}
		case queries.ContractsServiceTypeServiceTypeDelegatedService:
			service = &contract.NewGrantServiceConnectionDelegatedServiceArgs{
				Name: grantRow.ServiceName,
				Peer: &contract.NewPeerArgs{
					ID: grantRow.ServicePeerID,
				},
				PublicationDelegator: &contract.NewPeerArgs{
					ID: grantRow.ServicePublicationDelegatorPeerID.String,
				},
			}
		default:
			return nil, fmt.Errorf("invalid service type in database: %v", grantRow.ServiceType)
		}

		result[j] = &contract.NewGrantServiceConnectionArgs{
			Outway: &contract.NewGrantServiceConnectionOutwayArgs{
				Peer: &contract.NewPeerArgs{
					ID: grantRow.OutwayPeerID,
				},
				PublicKeyThumbprint: grantRow.PublicKeyThumbprint,
			},
			Service: service,
		}
	}

	return result, nil
}

func getServicePublicationGrantsByContractHash(ctx context.Context, q *queries.Queries, hash string) ([]*contract.NewGrantServicePublicationArgs, error) {
	grantRows, err := q.ListServicePublicationGrantsForContract(ctx, hash)
	if err != nil {
		return nil, fmt.Errorf("could not list service publication grants for contract from database: %v", err)
	}

	result := make([]*contract.NewGrantServicePublicationArgs, len(grantRows))

	for j, grantRow := range grantRows {
		result[j] = &contract.NewGrantServicePublicationArgs{
			Directory: &contract.NewGrantServicePublicationDirectoryArgs{
				Peer: &contract.NewPeerArgs{
					ID: grantRow.DirectoryPeerID,
				},
			},
			Service: &contract.NewGrantServicePublicationServiceArgs{
				Name: grantRow.ServiceName,
				Peer: &contract.NewPeerArgs{
					ID: grantRow.ServicePeerID,
				},
				Protocol: mapServiceProtocolToContract(grantRow.ServiceProtocol),
			},
		}
	}

	return result, nil
}

func getDelegatedServiceConnectionGrantsByContractHash(ctx context.Context, q *queries.Queries, hash string) ([]*contract.NewGrantDelegatedServiceConnectionArgs, error) {
	grantRows, err := q.ListDelegatedServiceConnectionGrantsForContract(ctx, hash)
	if err != nil {
		return nil, fmt.Errorf("could not list service connection grants for contract from database: %v", err)
	}

	result := make([]*contract.NewGrantDelegatedServiceConnectionArgs, len(grantRows))

	for j, grantRow := range grantRows {
		var service interface{}

		switch grantRow.ServiceType {
		case queries.ContractsServiceTypeServiceTypeService:
			service = &contract.NewGrantDelegatedServiceConnectionServiceArgs{
				Name: grantRow.ServiceName,
				Peer: &contract.NewPeerArgs{
					ID: grantRow.ServicePeerID,
				},
			}
		case queries.ContractsServiceTypeServiceTypeDelegatedService:
			service = &contract.NewGrantDelegatedServiceConnectionDelegatedServiceArgs{
				Name: grantRow.ServiceName,
				Peer: &contract.NewPeerArgs{
					ID: grantRow.ServicePeerID,
				},
				PublicationDelegator: &contract.NewPeerArgs{
					ID: grantRow.ServicePublicationDelegatorPeerID.String,
				},
			}
		default:
			return nil, fmt.Errorf("invalid service type in database: %v", grantRow.ServiceType)
		}

		result[j] = &contract.NewGrantDelegatedServiceConnectionArgs{
			Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
				Peer: &contract.NewPeerArgs{
					ID: grantRow.OutwayPeerID,
				},
				PublicKeyThumbprint: grantRow.PublicKeyThumbprint,
			},
			Service: service,
			Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
				Peer: &contract.NewPeerArgs{
					ID: grantRow.DelegatorPeerID,
				},
			},
		}
	}

	return result, nil
}

func getDelegatedServicePublicationGrantsByContractHash(ctx context.Context, q *queries.Queries, hash string) ([]*contract.NewGrantDelegatedServicePublicationArgs, error) {
	grantRows, err := q.ListDelegatedServicePublicationGrantsForContract(ctx, hash)
	if err != nil {
		return nil, fmt.Errorf("could not list service connection grants for contract from database: %v", err)
	}

	result := make([]*contract.NewGrantDelegatedServicePublicationArgs, len(grantRows))

	for j, grantRow := range grantRows {
		result[j] = &contract.NewGrantDelegatedServicePublicationArgs{
			Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
				Peer: &contract.NewPeerArgs{
					ID: grantRow.DirectoryPeerID,
				},
			},
			Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
				Name: grantRow.ServiceName,
				Peer: &contract.NewPeerArgs{
					ID: grantRow.ServicePeerID,
				},
				Protocol: mapServiceProtocolToContract(grantRow.ServiceProtocol),
			},
			Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
				Peer: &contract.NewPeerArgs{
					ID: grantRow.DelegatorPeerID,
				},
			},
		}
	}

	return result, nil
}

type signaturesMap struct {
	accepted map[string]string
	rejected map[string]string
	revoked  map[string]string
}

func getSignaturesByContractHash(ctx context.Context, q *queries.Queries, hash string) (*signaturesMap, error) {
	signatureRows, err := q.ListSignaturesForContract(ctx, hash)
	if err != nil {
		return nil, fmt.Errorf("could not list signatures for contract from database: %v", err)
	}

	result := &signaturesMap{
		accepted: map[string]string{},
		rejected: map[string]string{},
		revoked:  map[string]string{},
	}

	for _, signatureRow := range signatureRows {
		switch signatureRow.Type {
		case queries.ContractsSignatureTypeAccept:
			result.accepted[signatureRow.PeerID] = signatureRow.Jws
		case queries.ContractsSignatureTypeReject:
			result.rejected[signatureRow.PeerID] = signatureRow.Jws
		case queries.ContractsSignatureTypeRevoke:
			result.revoked[signatureRow.PeerID] = signatureRow.Jws
		default:
			return nil, fmt.Errorf("invalid signature type in database: %s", signatureRow.Type)
		}
	}

	return result, nil
}
