// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"fmt"
	"time"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListOutgoingConnections(args *contract.ListOutgoingConnectionsArgs) (*contract.OutgoingConnections, error) {
	contractStatesFilter := make([]string, len(args.ContractStates))

	for i, cState := range args.ContractStates {
		contractStatesFilter[i] = convertContractStateToDB(cState)
	}

	paginationCreatedAt := pgtype.Timestamp{
		Time:             time.Time{},
		InfinityModifier: 0,
		Valid:            false,
	}

	if args.PaginationCreatedAt != 0 {
		paginationCreatedAt.Time = time.Unix(args.PaginationCreatedAt, 0).UTC()
		paginationCreatedAt.Valid = true
	}

	outgoingConnectionRows, err := r.queries.ListOutgoingConnections(args.Ctx, &queries.ListOutgoingConnectionsParams{
		Limit:               int32(args.PaginationLimit),
		SelfPeerID:          string(args.PeerID),
		ContractStates:      contractStatesFilter,
		PaginationCreatedAt: paginationCreatedAt,
		PaginationGrantHash: args.PaginationGrantHash,
		OrderDirection:      string(args.PaginationSortOrder),
	})
	if err != nil {
		return nil, fmt.Errorf("could not retrieve outgoing connection from database: %v", err)
	}

	totalAmount := 0

	if args.IncludeCount {
		count, err := r.queries.ListOutgoingConnectionsCount(args.Ctx, &queries.ListOutgoingConnectionsCountParams{
			SelfPeerID:     string(args.PeerID),
			ContractStates: contractStatesFilter,
		})
		if err != nil {
			return nil, fmt.Errorf("could not retrieve outgoing connections count from database: %v", err)
		}

		totalAmount = int(count)
	}

	outgoingConnections := make([]*contract.Connection, len(outgoingConnectionRows))

	if len(outgoingConnectionRows) == 0 {
		return &contract.OutgoingConnections{}, nil
	}

	for i, cr := range outgoingConnectionRows {
		c := &contract.Connection{
			ServicePeerID:             contract.PeerID(cr.ServicePeerID),
			ServiceName:               cr.ServiceName,
			ContentHash:               cr.ContentHash,
			CreatedAt:                 cr.CreatedAt.Time,
			ValidNotBefore:            cr.ValidNotBefore.Time,
			ValidNotAfter:             cr.ValidNotAfter.Time,
			ContractState:             convertDBContractStateToDomain(cr.State),
			GrantHash:                 cr.Hash,
			OutwayPeerID:              contract.PeerID(cr.OutwayPeerID),
			OutwayPublicKeyThumbprint: contract.PublicKeyThumbprint(cr.PublicKeyThumbprint),
		}

		if cr.ServicePublicationDelegatorPeerID.Valid {
			pID := contract.PeerID(cr.ServicePublicationDelegatorPeerID.String)
			c.ServicePublicationDelegatorPeerID = &pID
		}

		if cr.DelegatorPeerID != nil {
			dpID, errConv := contract.NewPeerID(cr.DelegatorPeerID.(string))
			if errConv != nil {
				return nil, fmt.Errorf("could not convert delegator peer ID: %v", errConv)
			}

			c.DelegatorPeerID = &dpID
		}

		outgoingConnections[i] = c
	}

	outgoingConnectionsResult := &contract.OutgoingConnections{
		Count:       totalAmount,
		Connections: outgoingConnections,
	}

	return outgoingConnectionsResult, nil
}

const (
	dbContractStateValid    = "valid"
	dbContractStateExpired  = "expired"
	dbContractStateRejected = "rejected"
	dbContractStateRevoked  = "revoked"
	dbContractStateProposed = "proposed"
)

func convertDBContractStateToDomain(state interface{}) contract.ContractState {
	if state == nil {
		return contract.ContractStateUnspecified
	}

	switch state.(string) {
	case dbContractStateValid:
		return contract.ContractStateValid
	case dbContractStateExpired:
		return contract.ContractStateExpired
	case dbContractStateRejected:
		return contract.ContractStateRejected
	case dbContractStateRevoked:
		return contract.ContractStateRevoked
	case dbContractStateProposed:
		return contract.ContractStateProposed
	default:
		return contract.ContractStateUnspecified
	}
}

func convertContractStateToDB(state contract.ContractState) string {
	switch state {
	case contract.ContractStateValid:
		return dbContractStateValid
	case contract.ContractStateExpired:
		return dbContractStateExpired
	case contract.ContractStateRejected:
		return dbContractStateRejected
	case contract.ContractStateProposed:
		return dbContractStateProposed
	case contract.ContractStateRevoked:
		return dbContractStateRevoked
	default:
		return ""
	}
}
