-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

ALTER TABLE contracts.grants_service_publication ADD CONSTRAINT grants_service_publication_content_hash_fk FOREIGN KEY (content_hash)
    REFERENCES contracts.content (hash) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE INDEX grants_service_publication_content_hash_idx ON contracts.grants_service_publication
    USING btree
    (
     content_hash
    );

ALTER TABLE contracts.grants_delegated_service_publication ADD CONSTRAINT grants_delegated_service_publication_content_hash_fk FOREIGN KEY (content_hash)
    REFERENCES contracts.content (hash) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE INDEX grants_delegated_service_publication_content_hash_idx ON contracts.grants_delegated_service_publication
    USING btree
    (
     content_hash
    );

ALTER TABLE contracts.grants_delegated_service_publication ADD CONSTRAINT grants_delegated_service_publication_service_peer_id_fk FOREIGN KEY (service_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE INDEX grants_delegated_service_publication_service_peer_id_idx ON contracts.grants_delegated_service_publication
    USING btree
    (
     service_peer_id
    );

ALTER TABLE contracts.grants_delegated_service_publication ADD CONSTRAINT grants_delegated_service_publication_directory_peer_id_fk FOREIGN KEY (directory_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE INDEX grants_delegated_service_publication_directory_peer_id_idx ON contracts.grants_delegated_service_publication
    USING btree
    (
     directory_peer_id
    );

ALTER TABLE contracts.grants_delegated_service_publication ADD CONSTRAINT grants_delegated_service_publication_delegator_peer_id_fk FOREIGN KEY (delegator_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE INDEX grants_delegated_service_publication_delegator_peer_id_idx ON contracts.grants_delegated_service_publication
    USING btree
    (
     delegator_peer_id
    );

ALTER TABLE contracts.grants_service_connection ADD CONSTRAINT grants_service_connection_outway_peer_id_fk FOREIGN KEY (outway_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE contracts.grants_service_connection
    DROP CONSTRAINT grants_service_connections_consumer_peer_id_fk,
    ADD CONSTRAINT grants_service_connections_outway_peer_id_fk FOREIGN KEY (outway_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE contracts.grants_service_connection
    DROP CONSTRAINT grants_service_connections_service_peer_id_fk,
    ADD CONSTRAINT grants_service_connections_service_peer_id_fk FOREIGN KEY (service_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE contracts.grants_delegated_service_connection
    DROP CONSTRAINT grants_delegated_service_connection_delegator_peer_id_fk,
    ADD CONSTRAINT grants_delegated_service_connection_delegator_peer_id_fk FOREIGN KEY (delegator_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE contracts.grants_delegated_service_connection
    DROP CONSTRAINT grants_delegated_service_connection_outway_peer_id_fk,
    ADD CONSTRAINT grants_delegated_service_connection_outway_peer_id_fk FOREIGN KEY (outway_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE contracts.grants_delegated_service_connection
    DROP CONSTRAINT grants_delegated_service_connection_service_peer_id_fk,
    ADD CONSTRAINT grants_delegated_service_connection_service_peer_id_fk FOREIGN KEY (service_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE contracts.grants_service_publication
    DROP CONSTRAINT grants_service_publication_service_peer_id_fk,
    ADD CONSTRAINT grants_service_publication_service_peer_id_fk FOREIGN KEY (service_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE contracts.grants_service_publication
    DROP CONSTRAINT grants_service_publication_directory_peer_id_fk,
    ADD CONSTRAINT grants_service_publication_directory_peer_id_fk FOREIGN KEY (directory_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE contracts.contract_signatures
    DROP CONSTRAINT contract_signatures_peer_id_fk,
    ADD CONSTRAINT contract_signatures_peer_id_fk FOREIGN KEY (peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE RESTRICT ON UPDATE CASCADE;

COMMIT;
