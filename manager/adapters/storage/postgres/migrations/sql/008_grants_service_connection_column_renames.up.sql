-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

ALTER TABLE contracts.grants_service_connection ALTER COLUMN certificate_thumbprint TYPE VARCHAR(64);
ALTER TABLE contracts.grants_service_connection RENAME COLUMN certificate_thumbprint TO public_key_thumbprint;
ALTER TABLE contracts.grants_delegated_service_connection ALTER COLUMN certificate_thumbprint TYPE VARCHAR(64);
ALTER TABLE contracts.grants_delegated_service_connection RENAME COLUMN certificate_thumbprint TO public_key_thumbprint;

COMMIT;
