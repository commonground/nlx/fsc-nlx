-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

DROP VIEW contracts.valid_contracts;

CREATE VIEW contracts.valid_contracts AS
SELECT content.hash
FROM contracts.content as content
WHERE
    content.valid_not_before < NOW() AND
    content.valid_not_after > NOW() AND

  -- contract is invalid when it contains a reject signature
    (SELECT COUNT(*) FROM contracts.contract_signatures as s WHERE s.signature_type = 'reject' AND s.content_hash = content.hash) = 0 AND

  -- contract is invalid when it contains a revoke signature
    (SELECT COUNT(*) FROM contracts.contract_signatures as s WHERE s.signature_type = 'revoke' AND s.content_hash = content.hash) = 0 AND

  -- contract is invalid when the contract is missing an accept signature from a peer
    (SELECT COUNT(DISTINCT id)
     FROM (
              -- the UNIONs will put all peer ids in the same column named 'id'
              SELECT directory_peer_id as id FROM contracts.grants_service_publication WHERE content_hash = content.hash
              UNION
              SELECT service_peer_id as id FROM contracts.grants_service_publication WHERE content_hash = content.hash
              UNION
              SELECT outway_peer_id as id FROM contracts.grants_service_connection WHERE content_hash = content.hash
              UNION
              SELECT service_peer_id as id FROM contracts.grants_service_connection WHERE content_hash = content.hash
              UNION
              SELECT service_peer_id as id FROM contracts.grants_service_connection WHERE content_hash = content.hash
              UNION
              SELECT service_publication_delegator_peer_id as id FROM contracts.grants_service_connection WHERE content_hash = content.hash
              UNION
              SELECT service_peer_id as id FROM contracts.grants_delegated_service_connection WHERE content_hash = content.hash
              UNION
              SELECT outway_peer_id as id FROM contracts.grants_delegated_service_connection WHERE content_hash = content.hash
              UNION
              SELECT service_publication_delegator_peer_id as id FROM contracts.grants_delegated_service_connection WHERE content_hash = content.hash
              UNION
              SELECT delegator_peer_id as id FROM contracts.grants_delegated_service_connection WHERE content_hash = content.hash
              UNION
              SELECT service_peer_id as id FROM contracts.grants_delegated_service_publication WHERE content_hash = content.hash
              UNION
              SELECT directory_peer_id as id FROM contracts.grants_delegated_service_publication WHERE content_hash = content.hash
              UNION
              SELECT delegator_peer_id as id FROM contracts.grants_delegated_service_publication WHERE content_hash = content.hash
          ) as p
    ) = (SELECT COUNT(*) FROM contracts.contract_signatures as s WHERE s.signature_type = 'accept' AND s.content_hash = content.hash)
;

COMMIT;
