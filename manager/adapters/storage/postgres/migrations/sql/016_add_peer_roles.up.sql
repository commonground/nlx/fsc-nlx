-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

CREATE TYPE peers.role AS ENUM ('ROLE_DIRECTORY');

CREATE TABLE peers.peers_roles (
    peer_id VARCHAR(20) NOT NULL,
    role peers.role NOT NULL,
    PRIMARY KEY (peer_id, role),
    FOREIGN KEY (peer_id) REFERENCES peers.peers (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE INDEX peers_roles_peer_id_idx ON peers.peers_roles USING btree (
 peer_id
);

COMMIT;
