-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

CREATE TYPE contracts.service_type AS ENUM ('service_type_service', 'service_type_delegated_service');

ALTER TABLE contracts.grants_service_connection ADD COLUMN service_type contracts.service_type NOT NULL;
ALTER TABLE contracts.grants_service_connection ADD COLUMN service_publication_delegator_peer_id VARCHAR(20);

ALTER TABLE contracts.grants_service_connection ADD CONSTRAINT check_service_type CHECK (
        (
            service_type = 'service_type_service'
            AND
            service_publication_delegator_peer_id IS NULL
        ) OR
        (
            service_type = 'service_type_delegated_service'
             AND
            service_publication_delegator_peer_id IS NOT NULL
        )
    );


ALTER TABLE contracts.grants_delegated_service_connection ADD COLUMN service_type contracts.service_type NOT NULL;
ALTER TABLE contracts.grants_delegated_service_connection ADD COLUMN service_publication_delegator_peer_id VARCHAR(20);

ALTER TABLE contracts.grants_delegated_service_connection ADD CONSTRAINT check_service_type CHECK (
    (
        service_type = 'service_type_service'
        AND
        service_publication_delegator_peer_id IS NULL
        ) OR
    (
        service_type = 'service_type_delegated_service'
            AND
        service_publication_delegator_peer_id IS NOT NULL
        )
    );



COMMIT;
