-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

UPDATE
    contracts.content
SET
    valid_not_before = date_trunc('second', valid_not_before),
    valid_not_after = date_trunc('second', valid_not_after),
    created_at = date_trunc('second', created_at)
;

UPDATE
    contracts.contract_signatures
SET
    signed_at = date_trunc('second', signed_at)
;

COMMIT;
