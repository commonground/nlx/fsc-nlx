-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN TRANSACTION;

CREATE VIEW contracts.contracts_with_state AS
SELECT *,
       CASE
           WHEN (SELECT COUNT(*) FROM contracts.contract_signatures AS s WHERE s.signature_type = 'reject' AND s.content_hash = c.hash) > 0 THEN 'rejected'
           WHEN (SELECT COUNT(*) FROM contracts.contract_signatures AS s WHERE s.signature_type = 'revoke' AND s.content_hash = c.hash) > 0 THEN 'revoked'
           WHEN c.valid_not_before > NOW()  OR c.valid_not_after < NOW() THEN 'expired'
           WHEN
               ((SELECT COUNT(DISTINCT p.id)
                 FROM (
                          SELECT directory_peer_id AS id FROM contracts.grants_service_publication WHERE content_hash = c.hash
                          UNION
                          SELECT service_peer_id AS id FROM contracts.grants_service_publication WHERE content_hash = c.hash
                          UNION
                          SELECT outway_peer_id AS id FROM contracts.grants_service_connection WHERE content_hash = c.hash
                          UNION
                          SELECT service_peer_id AS id FROM contracts.grants_service_connection WHERE content_hash = c.hash
                          UNION
                          SELECT service_peer_id AS id FROM contracts.grants_service_connection WHERE content_hash =c.hash
                          UNION
                          SELECT service_publication_delegator_peer_id AS id FROM contracts.grants_service_connection WHERE content_hash = c.hash
                          UNION
                          SELECT service_peer_id AS id FROM contracts.grants_delegated_service_connection WHERE content_hash =c.hash
                          UNION
                          SELECT outway_peer_id AS id FROM contracts.grants_delegated_service_connection WHERE content_hash = c.hash
                          UNION
                          SELECT service_publication_delegator_peer_id AS id FROM contracts.grants_delegated_service_connection WHERE content_hash = c.hash
                          UNION
                          SELECT delegator_peer_id AS id FROM contracts.grants_delegated_service_connection WHERE content_hash = c.hash
                          UNION
                          SELECT service_peer_id AS id FROM contracts.grants_delegated_service_publication WHERE content_hash = c.hash
                          UNION
                          SELECT directory_peer_id AS id FROM contracts.grants_delegated_service_publication WHERE content_hash = c.hash
                          UNION
                          SELECT delegator_peer_id AS id FROM contracts.grants_delegated_service_publication WHERE content_hash = c.hash
                      ) AS p) =  (SELECT COUNT(*) FROM contracts.contract_signatures AS s WHERE s.signature_type = 'accept' AND s.content_hash = c.hash))
               THEN 'valid'
           WHEN ((SELECT COUNT(DISTINCT p.id)
                  FROM (
                           SELECT directory_peer_id AS id FROM contracts.grants_service_publication WHERE content_hash = c.hash
                           UNION
                           SELECT service_peer_id AS id FROM contracts.grants_service_publication WHERE content_hash = c.hash
                           UNION
                           SELECT outway_peer_id AS id FROM contracts.grants_service_connection WHERE content_hash = c.hash
                           UNION
                           SELECT service_peer_id AS id FROM contracts.grants_service_connection WHERE content_hash = c.hash
                           UNION
                           SELECT service_peer_id AS id FROM contracts.grants_service_connection WHERE content_hash =c.hash
                           UNION
                           SELECT service_publication_delegator_peer_id AS id FROM contracts.grants_service_connection WHERE content_hash = c.hash
                           UNION
                           SELECT service_peer_id AS id FROM contracts.grants_delegated_service_connection WHERE content_hash =c.hash
                           UNION
                           SELECT outway_peer_id AS id FROM contracts.grants_delegated_service_connection WHERE content_hash = c.hash
                           UNION
                           SELECT service_publication_delegator_peer_id AS id FROM contracts.grants_delegated_service_connection WHERE content_hash = c.hash
                           UNION
                           SELECT delegator_peer_id AS id FROM contracts.grants_delegated_service_connection WHERE content_hash = c.hash
                           UNION
                           SELECT service_peer_id AS id FROM contracts.grants_delegated_service_publication WHERE content_hash = c.hash
                           UNION
                           SELECT directory_peer_id AS id FROM contracts.grants_delegated_service_publication WHERE content_hash = c.hash
                           UNION
                           SELECT delegator_peer_id AS id FROM contracts.grants_delegated_service_publication WHERE content_hash = c.hash
                       ) AS p) > (SELECT COUNT(*) FROM contracts.contract_signatures AS s WHERE s.signature_type = 'accept' AND s.content_hash = c.hash))
               THEN 'proposed'
           end
           as state
FROM contracts.content AS c;

COMMIT;
