-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

ALTER TABLE contracts.failed_distributions RENAME COLUMN retries TO attempts;
ALTER TABLE contracts.failed_distributions RENAME COLUMN last_retry_at TO last_attempt_at;
ALTER TABLE contracts.failed_distributions RENAME COLUMN next_retry_at TO next_attempt_at;
ALTER TABLE contracts.failed_distributions RENAME COLUMN error_message TO reason;

COMMIT;
