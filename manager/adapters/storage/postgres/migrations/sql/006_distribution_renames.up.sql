-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

ALTER TABLE contracts.distributions RENAME TO failed_distributions;

COMMIT;
