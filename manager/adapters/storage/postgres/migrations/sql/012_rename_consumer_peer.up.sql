-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

ALTER TABLE
    contracts.grants_service_connection
RENAME COLUMN
    consumer_peer_id TO outway_peer_id;
COMMIT;
