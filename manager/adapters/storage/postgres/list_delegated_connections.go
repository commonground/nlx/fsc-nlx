// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListDelegatedConnections(ctx context.Context, args *contract.ListDelegatedConnectionsArgs) (*contract.DelegatedConnections, error) {
	contractStatesFilter := make([]string, len(args.ContractStates))

	for i, cState := range args.ContractStates {
		contractStatesFilter[i] = convertContractStateToDB(cState)
	}

	paginationCreatedAt := pgtype.Timestamp{
		Time:             time.Time{},
		InfinityModifier: 0,
		Valid:            false,
	}

	if args.PaginationCreatedAt != 0 {
		paginationCreatedAt.Time = time.Unix(args.PaginationCreatedAt, 0).UTC()
		paginationCreatedAt.Valid = true
	}

	delegatedConnectionRows, err := r.queries.ListDelegatedConnections(ctx, &queries.ListDelegatedConnectionsParams{
		Limit:               int32(args.PaginationLimit),
		SelfPeerID:          string(args.PeerID),
		PaginationCreatedAt: paginationCreatedAt,
		PaginationGrantHash: args.PaginationGrantHash,
		OrderDirection:      string(args.PaginationSortOrder),
		ContractStates:      contractStatesFilter,
	})
	if err != nil {
		return nil, fmt.Errorf("could not retrieve delegated connections from database: %v", err)
	}

	totalAmount := 0

	if args.IncludeCount {
		count, err := r.queries.ListIncomingConnectionsCount(args.Ctx, &queries.ListIncomingConnectionsCountParams{
			SelfPeerID:     string(args.PeerID),
			ContractStates: contractStatesFilter,
		})
		if err != nil {
			return nil, fmt.Errorf("could not retrieve incoming connections count from database: %v", err)
		}

		totalAmount = int(count)
	}

	connections := make([]*contract.Connection, len(delegatedConnectionRows))

	if len(delegatedConnectionRows) == 0 {
		return &contract.DelegatedConnections{}, nil
	}

	//nolint:dupl // looks the same as ListOutgoingConnections but is different
	for i, cr := range delegatedConnectionRows {
		c := &contract.Connection{
			ServicePeerID:  contract.PeerID(cr.ServicePeerID),
			ServiceName:    cr.ServiceName,
			ContentHash:    cr.ContentHash,
			CreatedAt:      cr.CreatedAt.Time,
			ValidNotBefore: cr.ValidNotBefore.Time,
			ValidNotAfter:  cr.ValidNotAfter.Time,
			ContractState:  convertDBContractStateToDomain(cr.State),
			GrantHash:      cr.Hash,
			OutwayPeerID:   contract.PeerID(cr.OutwayPeerID),
		}

		if cr.ServicePublicationDelegatorPeerID.Valid {
			pID := contract.PeerID(cr.ServicePublicationDelegatorPeerID.String)
			c.ServicePublicationDelegatorPeerID = &pID
		}

		dpID, errConv := contract.NewPeerID(cr.DelegatorPeerID)
		if errConv != nil {
			return nil, fmt.Errorf("could not convert delegator peer ID: %v", errConv)
		}

		c.DelegatorPeerID = &dpID

		connections[i] = c
	}

	outgoingConnectionsResult := &contract.DelegatedConnections{
		Count:       totalAmount,
		Connections: connections,
	}

	return outgoingConnectionsResult, nil
}
