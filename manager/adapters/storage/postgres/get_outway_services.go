// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetServicesForOutway(ctx context.Context, outwayCertThumbprint string) (contract.OutwayServices, error) {
	services := make(contract.OutwayServices)

	dbServices, err := r.queries.GetOutwayServices(ctx, outwayCertThumbprint)
	if err != nil {
		return nil, errors.Wrap(err, "could not get outway services from database")
	}

	dbDelegatedServices, err := r.queries.GetOutwayDelegatedServices(ctx, outwayCertThumbprint)
	if err != nil {
		return nil, errors.Wrap(err, "could not get outway delegated services from database")
	}

	for _, s := range dbServices {
		services[s.GrantHash] = &contract.OutwayService{
			PeerID: s.PeerID,
			Name:   s.ServiceName,
		}
	}

	for _, s := range dbDelegatedServices {
		services[s.GrantHash] = &contract.OutwayService{
			PeerID: s.PeerID,
			Name:   s.ServiceName,
		}
	}

	return services, nil
}
