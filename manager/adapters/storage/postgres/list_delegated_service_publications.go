// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"fmt"
	"time"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListDelegatedServicePublications(args *contract.ListDelegatedPublicationsArgs) (*contract.Publications, error) {
	contractStatesFilter := make([]string, len(args.ContractStates))

	for i, cState := range args.ContractStates {
		contractStatesFilter[i] = convertContractStateToDB(cState)
	}

	paginationCreatedAt := pgtype.Timestamp{
		Time:             time.Time{},
		InfinityModifier: 0,
		Valid:            false,
	}

	if args.PaginationCreatedAt != 0 {
		paginationCreatedAt.Time = time.Unix(args.PaginationCreatedAt, 0).UTC()
		paginationCreatedAt.Valid = true
	}

	delegatedServicePublicationRows, err := r.queries.ListDelegatedServicePublications(args.Ctx, &queries.ListDelegatedServicePublicationsParams{
		Limit:               int32(args.PaginationLimit),
		SelfPeerID:          string(args.PeerID),
		PaginationCreatedAt: paginationCreatedAt,
		PaginationGrantHash: args.PaginationGrantHash,
		OrderDirection:      string(args.PaginationSortOrder),
		ContractStates:      contractStatesFilter,
	})
	if err != nil {
		return nil, fmt.Errorf("could not retrieve delegated service publications from database: %v", err)
	}

	totalAmount := 0

	if args.IncludeCount {
		count, err := r.queries.ListDelegatedServicePublicationsCount(args.Ctx, &queries.ListDelegatedServicePublicationsCountParams{
			SelfPeerID:     string(args.PeerID),
			ContractStates: contractStatesFilter,
		})
		if err != nil {
			return nil, fmt.Errorf("could not retrieve delegated service publications count from database: %v", err)
		}

		totalAmount = int(count)
	}

	if len(delegatedServicePublicationRows) == 0 {
		return &contract.Publications{}, nil
	}

	result := make([]*contract.Publication, len(delegatedServicePublicationRows))

	for i, spr := range delegatedServicePublicationRows {
		publication := &contract.Publication{
			ServicePeerID:   contract.PeerID(spr.ServicePeerID),
			ServiceName:     spr.ServiceName,
			ContentHash:     spr.ContentHash,
			CreatedAt:       spr.CreatedAt.Time,
			ValidNotBefore:  spr.ValidNotBefore.Time,
			ValidNotAfter:   spr.ValidNotAfter.Time,
			ContractState:   convertDBContractStateToDomain(spr.State),
			GrantHash:       spr.Hash,
			DirectoryPeerID: contract.PeerID(spr.DirectoryPeerID),
			DelegatorPeerID: convertDelegatorPeerID(spr.DelegatorPeerID),
		}

		result[i] = publication
	}

	publications := &contract.Publications{
		Count:        totalAmount,
		Publications: result,
	}

	return publications, nil
}
