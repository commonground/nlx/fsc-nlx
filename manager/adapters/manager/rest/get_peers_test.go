// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:dupl,funlen,errcheck // table tests are long and have a similar structure
func TestGetPeers(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(context.Context, *httptest.Server) contract.Peers
		args    contract.PeersIDs
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, ts *httptest.Server) contract.Peers {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					responseBody := []byte("{\"peers\": [{\"Id\": \"00000000000000000001\", \"Name\": \"test-peer\"}], \"pagination\": {\"next_cursor\": \"test\"}}")
					w.Header().Add("Content-Type", "application/json")
					w.Write(responseBody)
					w.WriteHeader(http.StatusOK)
				})

				expectedResponsePeer, _ := contract.NewPeer(&contract.NewPeerArgs{
					ID:             "00000000000000000001",
					Name:           "test-peer",
					ManagerAddress: "",
				})

				peers := contract.Peers{expectedResponsePeer.ID(): expectedResponsePeer}

				return peers
			},
		},
		"happy_flow_peerID": {
			setup: func(ctx context.Context, ts *httptest.Server) contract.Peers {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					responseBody := []byte("{\"peers\": [{\"Id\": \"00000000000000000001\", \"Name\": \"test-peer\"}], \"pagination\": {\"next_cursor\": \"test\"}}")
					w.Header().Add("Content-Type", "application/json")
					w.Write(responseBody)
					w.WriteHeader(http.StatusOK)
				})

				expectedResponsePeer, _ := contract.NewPeer(&contract.NewPeerArgs{
					ID:             "00000000000000000001",
					Name:           "test-peer",
					ManagerAddress: "",
				})

				peers := contract.Peers{expectedResponsePeer.ID(): expectedResponsePeer}

				return peers
			},
			args: contract.PeersIDs{
				contract.PeerID("00000000000000000001"): true,
			},
		},
		"error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) contract.Peers {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					panic("Kaboom!")
				})

				return nil
			},
			wantErr: errors.New("could not get Peers"),
		},
		"invalid_status_code": {
			setup: func(ctx context.Context, ts *httptest.Server) contract.Peers {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusInternalServerError)
				})
				return nil
			},
			wantErr: errors.New("received invalid status code 500:"),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
			t.Cleanup(ts.Close)

			client := getTestClient(ts)
			ctx := context.Background()

			var peers contract.Peers
			if tt.setup != nil {
				peers = tt.setup(ctx, ts)
			}

			peersResponse, err := client.GetPeers(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, peers.IDs().Value()[0], peersResponse.IDs().Value()[0])
			} else {
				assert.ErrorContains(t, err, tt.wantErr.Error())
			}
		})
	}
}
