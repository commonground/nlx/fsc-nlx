// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest_test

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

//nolint:funlen,errcheck // table tests are long
func TestGetCertificate(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(context.Context, *httptest.Server)
		wantErr error
	}{
		// Happy flow is tested in integration tests to allow more flexibility in the certificates used in the JWKS endpoint.
		"internal_server_error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusInternalServerError)
				})
			},
			wantErr: errors.New("could not get certificates, received invalid status code 500"),
		},
		"error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					panic("Kaboom!")
				})
			},
			wantErr: errors.New("could not get certificates"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, ts *httptest.Server) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					responseBody, err := json.Marshal(&models.FSCCoreJwks{
						Keys: []models.FSCCoreJwk{
							{
								Alg:     ptr(models.RS512),
								Kid:     ptr("test-key"),
								Kty:     "RSA",
								Use:     ptr(models.Sig),
								X5c:     ptr([]string{"invalid"}),
								X5t:     nil,
								X5tS256: ptr(orgACertBundle.CertificateThumbprint()),
								X5u:     nil,
							},
						},
					})
					if err != nil {
						w.WriteHeader(http.StatusInternalServerError)
						return
					}

					w.Header().Set("Content-Type", "application/json")
					w.Write(responseBody)
				})
			},
			wantErr: errors.New("illegal base64 data at input byte 4\nunable to decode certificate base64 string"),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
			t.Cleanup(ts.Close)

			client := getTestClient(ts)
			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, ts)
			}

			_, err := client.GetCertificates(ctx)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorContains(t, err, tt.wantErr.Error())
			}
		})
	}
}
