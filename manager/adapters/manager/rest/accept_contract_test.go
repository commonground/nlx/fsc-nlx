// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
package rest_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:dupl,funlen,errcheck // table tests are long and have a similar structure
func TestAcceptContract(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(ctx context.Context, server *httptest.Server) (*contract.Content, *contract.Signature)
		want    []*contract.Contract
		wantErr error
	}{
		"no_contract_content": {
			wantErr: errors.New("contract content required"),
		},
		"no_contract_signature": {
			setup: func(ctx context.Context, server *httptest.Server) (*contract.Content, *contract.Signature) {
				return &contract.Content{}, nil
			},
			wantErr: errors.New("contract signature required"),
		},
		"internal_server_error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) (*contract.Content, *contract.Signature) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusInternalServerError)
				})

				grants := []interface{}{
					&contract.NewGrantServicePublicationArgs{
						Directory: &contract.NewGrantServicePublicationDirectoryArgs{
							Peer: &contract.NewPeerArgs{
								ID:             "12345678901234567891",
								Name:           "",
								ManagerAddress: "",
							},
						},
						Service: &contract.NewGrantServicePublicationServiceArgs{
							Peer: &contract.NewPeerArgs{
								ID:             "00000000000000000002",
								Name:           "",
								ManagerAddress: "",
							},
							Name:     "test-service",
							Protocol: contract.ServiceProtocolTCPHTTP2,
						},
					},
				}

				contractContent, _ := createContract(t, grants)
				return contractContent, &contract.Signature{}
			},
			wantErr: errors.New("received invalid status code 500:"),
		},
		"error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) (*contract.Content, *contract.Signature) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					panic("kaboom!")
				})

				grants := []interface{}{
					&contract.NewGrantServicePublicationArgs{
						Directory: &contract.NewGrantServicePublicationDirectoryArgs{
							Peer: &contract.NewPeerArgs{
								ID:             "12345678901234567891",
								Name:           "",
								ManagerAddress: "",
							},
						},
						Service: &contract.NewGrantServicePublicationServiceArgs{
							Peer: &contract.NewPeerArgs{
								ID:             "00000000000000000002",
								Name:           "",
								ManagerAddress: "",
							},
							Name:     "test-service",
							Protocol: contract.ServiceProtocolTCPHTTP2,
						},
					},
				}

				contractContent, _ := createContract(t, grants)
				return contractContent, &contract.Signature{}
			},
			wantErr: errors.New("could not accept contract"),
		},
		"invalid_response_code": {
			setup: func(ctx context.Context, ts *httptest.Server) (*contract.Content, *contract.Signature) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusOK)
				})

				grants := []interface{}{
					&contract.NewGrantServicePublicationArgs{
						Directory: &contract.NewGrantServicePublicationDirectoryArgs{
							Peer: &contract.NewPeerArgs{
								ID:             "12345678901234567891",
								Name:           "",
								ManagerAddress: "",
							},
						},
						Service: &contract.NewGrantServicePublicationServiceArgs{
							Peer: &contract.NewPeerArgs{
								ID:             "00000000000000000002",
								Name:           "",
								ManagerAddress: "",
							},
							Name:     "test-service",
							Protocol: contract.ServiceProtocolTCPHTTP2,
						},
					},
				}

				return createContract(t, grants)
			},
			wantErr: errors.New("received invalid status code 200:"),
		},
		"happy_flow": {
			setup: func(ctx context.Context, ts *httptest.Server) (*contract.Content, *contract.Signature) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusCreated)
				})

				grants := []interface{}{
					&contract.NewGrantDelegatedServiceConnectionArgs{
						Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
							Peer: &contract.NewPeerArgs{
								ID:             "00000000000000000002",
								Name:           "",
								ManagerAddress: "",
							},
							PublicKeyThumbprint: orgACertBundle.PublicKeyThumbprint(),
						},
						Service: &contract.NewGrantDelegatedServiceConnectionDelegatedServiceArgs{
							Peer: &contract.NewPeerArgs{
								ID:             "12345678901234567891",
								Name:           "",
								ManagerAddress: "",
							},
							Name: "test-service",
							PublicationDelegator: &contract.NewPeerArgs{
								ID:             "00000000000000000004",
								Name:           "",
								ManagerAddress: "",
							},
						},
						Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
							Peer: &contract.NewPeerArgs{
								ID:             "00000000000000000003",
								Name:           "",
								ManagerAddress: "",
							}},
					},
				}

				return createContract(t, grants)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
			t.Cleanup(ts.Close)

			client := getTestClient(ts)

			ctx := context.Background()

			var contractContent *contract.Content

			var signature *contract.Signature

			if tt.setup != nil {
				contractContent, signature = tt.setup(ctx, ts)
			}

			err := client.AcceptContract(ctx, contractContent, signature)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorContains(t, err, tt.wantErr.Error())
			}
		})
	}
}
