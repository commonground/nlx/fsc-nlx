// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:dupl,funlen,errcheck // table tests are long and all have similar setup
func TestGetServices(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup func(context.Context, *httptest.Server) []*contract.Service
		args  struct {
			PeerID      contract.PeerID
			ServiceName string
		}
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, ts *httptest.Server) []*contract.Service {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					responseBody := []byte("{\"services\":[{\"data\":{\"type\":\"TYPE_SERVICE\",\"peer\":{\"id\":\"00000000000000000001\",\"name\":\"test-peer\",\"manager_address\":\"https://test-address.org:443\"},\"name\":\"test-service\",\"protocol\":\"PROTOCOL_TCP_HTTP_1.1\"}}], \"pagination\": {\"next_cursor\": \"test\"}}")
					w.Header().Add("Content-Type", "application/json")
					w.Write(responseBody)
					w.WriteHeader(http.StatusOK)
				})

				expectedService := &contract.Service{
					ContractIV:         "dcf61726-6ef3-4ede-805e-4d80323af118",
					PeerID:             "00000000000000000001",
					PeerManagerAddress: "https://test-address.org:443",
					PeerName:           "test-peer",
					Name:               "test-service",
					Protocol:           contract.ServiceProtocolTCPHTTP1_1,
				}
				expectedServices := []*contract.Service{
					expectedService,
				}

				return expectedServices
			},
		},
		"happy_flow_args": {
			setup: func(ctx context.Context, ts *httptest.Server) []*contract.Service {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					responseBody := []byte("{\"services\":[{\"data\":{\"type\":\"TYPE_DELEGATED_SERVICE\",\"peer\":{\"id\":\"00000000000000000001\",\"name\":\"test-peer\",\"manager_address\":\"https://test-address.org:443\"},\"delegator\": {\"peer_id\": \"00000000000000000002\", \"peer_name\": \"test-delegator-peer\"},\"name\":\"test-service\",\"protocol\":\"PROTOCOL_TCP_HTTP_2\"}}], \"pagination\": {\"next_cursor\": \"test\"}}")
					w.Header().Add("Content-Type", "application/json")
					w.Write(responseBody)
					w.WriteHeader(http.StatusOK)
				})

				expectedService := &contract.Service{
					ContractIV:         "dcf61726-6ef3-4ede-805e-4d80323af118",
					PeerID:             "00000000000000000001",
					PeerManagerAddress: "https://test-address.org:443",
					PeerName:           "test-peer",
					DelegatorPeerID:    "00000000000000000002",
					DelegatorPeerName:  "test-delegator-peer",
					Name:               "test-service",
					Protocol:           contract.ServiceProtocolTCPHTTP2,
				}
				expectedServices := []*contract.Service{
					expectedService,
				}

				return expectedServices
			},
			args: struct {
				PeerID      contract.PeerID
				ServiceName string
			}{PeerID: contract.PeerID("00000000000000000001"), ServiceName: "test-service"},
		},
		"unknown_protocol": {
			setup: func(ctx context.Context, ts *httptest.Server) []*contract.Service {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					responseBody := []byte("{\"services\":[{\"data\":{\"type\":\"TYPE_SERVICE\",\"peer\":{\"id\":\"00000000000000000001\",\"name\":\"test-peer\",\"manager_address\":\"https://test-address.org:443\"},\"name\":\"test-service\",\"protocol\":\"unknown-protocol\"}}], \"pagination\": {\"next_cursor\": \"test\"}}")
					w.Header().Add("Content-Type", "application/json")
					w.Write(responseBody)
					w.WriteHeader(http.StatusOK)
				})

				return nil
			},
			wantErr: errors.New("unknown protocol"),
		},
		"error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) []*contract.Service {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					panic("Kaboom!")
				})

				return nil
			},
			wantErr: errors.New("could not get Services"),
		},
		"invalid_status_code": {
			setup: func(ctx context.Context, ts *httptest.Server) []*contract.Service {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusInternalServerError)
				})
				return nil
			},
			wantErr: errors.New("received invalid status code 500:"),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
			t.Cleanup(ts.Close)

			client := getTestClient(ts)
			ctx := context.Background()

			var expectedServices []*contract.Service
			if tt.setup != nil {
				expectedServices = tt.setup(ctx, ts)
			}

			servicesResponse, err := client.GetServices(ctx, &tt.args.PeerID, tt.args.ServiceName)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Len(t, servicesResponse, len(expectedServices))
				assert.Equal(t, expectedServices[0].PeerID, servicesResponse[0].PeerID)
				assert.Equal(t, expectedServices[0].PeerName, servicesResponse[0].PeerName)
				assert.Equal(t, expectedServices[0].PeerManagerAddress, servicesResponse[0].PeerManagerAddress)
				assert.Equal(t, expectedServices[0].Name, servicesResponse[0].Name)
				assert.Equal(t, expectedServices[0].Protocol, servicesResponse[0].Protocol)
			} else {
				assert.ErrorContains(t, err, tt.wantErr.Error())
			}
		})
	}
}
