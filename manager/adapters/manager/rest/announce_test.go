// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
package rest_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func TestAnnounce(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(ctx context.Context, server *httptest.Server)
		want    []*contract.Contract
		wantErr error
	}{
		"internal_server_error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusInternalServerError)
				})
			},
			wantErr: errors.New("received invalid status code 500:"),
		},
		"error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					panic("kaboom!")
				})
			},
			wantErr: errors.New("could not announce to peer"),
		},
		"happy_flow": {
			setup: func(ctx context.Context, ts *httptest.Server) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusOK)
				})
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
			t.Cleanup(ts.Close)

			client := getTestClient(ts)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, ts)
			}

			err := client.Announce(ctx)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorContains(t, err, tt.wantErr.Error())
			}
		})
	}
}
