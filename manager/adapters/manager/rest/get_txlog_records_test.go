// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:dupl,funlen,errcheck // table tests are long and all have similar setup
func TestTXLogRecords(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(context.Context, *httptest.Server) contract.TXLogRecords
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, ts *httptest.Server) contract.TXLogRecords {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					responseBody := []byte("{\"records\":[{\"transaction_id\":\"0189f7e5-c883-7106-8272-ccb7fcba0575\",\"direction\":\"DIRECTION_INCOMING\",\"grant_hash\":\"$1$4$+PQI7we01qIfEwq4O5UioLKzjGBgRva6F5+bUfDlKxUjcY5yX1MRsn6NKquDbL8VcklhYO9sk18rHD6La3w/mg\",\"source\":{\"type\":\"SOURCE_TYPE_SOURCE\",\"outway_peer_id\":\"12345678901234567891\"},\"destination\":{\"type\":\"DESTINATION_TYPE_DESTINATION\",\"service_peer_id\":\"12345678901234567891\"},\"service_name\":\"random_service_name\",\"created_at\":1672527600}, {\"transaction_id\":\"d6c01e87-88be-4030-99dc-e6f310362152\",\"direction\":\"DIRECTION_OUTGOING\",\"grant_hash\":\"$1$4$+PQI7we01qIfEwq4O5UioLKzjGBgRva6F5+bUfDlKxUjcY5yX1MRsn6NKquDbL8VcklhYO9sk18rHD6La3w/mg\",\"source\":{\"type\":\"SOURCE_TYPE_DELEGATED_SOURCE\",\"outway_peer_id\":\"00000000000000000001\", \"delegator_peer_id\":\"00000000000000000002\"},\"destination\":{\"type\":\"DESTINATION_TYPE_DELEGATED_DESTINATION\",\"service_peer_id\":\"00000000000000000003\", \"delegator_peer_id\":\"00000000000000000004\"},\"service_name\":\"random_service_name\",\"created_at\":1672527600}], \"pagination\": {\"next_cursor\": \"test\"}}")
					w.Header().Add("Content-Type", "application/json")
					w.Write(responseBody)
					w.WriteHeader(http.StatusOK)
				})

				records := contract.TXLogRecords{
					{
						TransactionID: "0189f7e5-c883-7106-8272-ccb7fcba0575",
						GrantHash:     "$1$4$+PQI7we01qIfEwq4O5UioLKzjGBgRva6F5+bUfDlKxUjcY5yX1MRsn6NKquDbL8VcklhYO9sk18rHD6La3w/mg",
						ServiceName:   "random_service_name",
						Direction:     contract.TXLogDirectionIn,
						Source: &contract.TXLogRecordSource{
							OutwayPeerID: "12345678901234567891",
						},
						Destination: &contract.TXLogRecordDestination{
							ServicePeerID: "12345678901234567891",
						},
						CreatedAt: time.Unix(1672527600, 0),
					},
					{
						TransactionID: "d6c01e87-88be-4030-99dc-e6f310362152",
						GrantHash:     "$1$4$+PQI7we01qIfEwq4O5UioLKzjGBgRva6F5+bUfDlKxUjcY5yX1MRsn6NKquDbL8VcklhYO9sk18rHD6La3w/mg",
						ServiceName:   "test-service",
						Direction:     contract.TXLogDirectionOut,
						Source: &contract.TXLogRecordDelegatedSource{
							OutwayPeerID:    "00000000000000000001",
							DelegatorPeerID: "00000000000000000002",
						},
						Destination: &contract.TXLogRecordDelegatedDestination{
							ServicePeerID:   "00000000000000000003",
							DelegatorPeerID: "00000000000000000004",
						},
						CreatedAt: time.Unix(1672527600, 0),
					},
				}

				return records
			},
		},
		"error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) contract.TXLogRecords {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					panic("Kaboom!")
				})

				return nil
			},
			wantErr: errors.New("could not get TXLog records"),
		},
		"invalid_status_code": {
			setup: func(ctx context.Context, ts *httptest.Server) contract.TXLogRecords {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusInternalServerError)
				})
				return nil
			},
			wantErr: errors.New("received invalid status code 500:"),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
			t.Cleanup(ts.Close)

			client := getTestClient(ts)
			ctx := context.Background()

			var expectedRecords contract.TXLogRecords
			if tt.setup != nil {
				expectedRecords = tt.setup(ctx, ts)
			}

			recordsResponse, err := client.GetTXLogRecords(ctx)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Len(t, recordsResponse, len(expectedRecords))
				assert.Equal(t, expectedRecords[0].ServiceName, recordsResponse[0].ServiceName)
				assert.Equal(t, expectedRecords[0].GrantHash, recordsResponse[0].GrantHash)
				assert.Equal(t, expectedRecords[0].Direction, recordsResponse[0].Direction)
				assert.Equal(t, expectedRecords[0].TransactionID, recordsResponse[0].TransactionID)
				assert.Equal(t, expectedRecords[0].CreatedAt, recordsResponse[0].CreatedAt)
				assert.Equal(t, expectedRecords[0].Source, recordsResponse[0].Source)
				assert.Equal(t, expectedRecords[0].Destination, recordsResponse[0].Destination)
				assert.Equal(t, expectedRecords[1].Source, recordsResponse[1].Source)
				assert.Equal(t, expectedRecords[1].Destination, recordsResponse[1].Destination)
			} else {
				assert.ErrorContains(t, err, tt.wantErr.Error())
			}
		})
	}
}
