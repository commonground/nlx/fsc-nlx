// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest_test

import (
	"net/http/httptest"
	"path/filepath"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var (
	orgACertBundle, _ = testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	testClock         = testingutils.NewMockClock(time.Now().Truncate(time.Second))
)

func ptr[T any](v T) *T {
	return &v
}

func getTestClient(ts *httptest.Server) *rest.Client {
	client, _ := rest.NewClient(ts.URL, "00000000000000000002", ts.URL, orgACertBundle, nil, testClock)
	return client
}

func createContract(t *testing.T, grants []interface{}) (*contract.Content, *contract.Signature) {
	trustedRootCert := orgACertBundle.RootCAs()

	contractIV, _ := uuid.New().MarshalBinary()
	contractContentArgs := &contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: grants,
	}

	contractContent, err := contract.NewContent(contractContentArgs)
	assert.NoError(t, err)

	acceptSig, err := contractContent.Accept(trustedRootCert, orgACertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	return contractContent, acceptSig
}

func TestNewClientWithoutSelfAddress(t *testing.T) {
	_, err := rest.NewClient("", "00000000000000000001", "", nil, nil, nil)

	assert.ErrorContains(t, err, "self address required")
}

func TestNewClientWithoutPeerID(t *testing.T) {
	_, err := rest.NewClient("http://localhost", "", "", nil, nil, nil)

	assert.ErrorContains(t, err, "peerID required")
}

func TestNewClientWithoutAddress(t *testing.T) {
	_, err := rest.NewClient("http://localhost", "00000000000000000002", "", nil, nil, nil)

	assert.ErrorContains(t, err, "address required")
}

func TestNewClientWithoutCert(t *testing.T) {
	_, err := rest.NewClient("http://localhost", "00000000000000000001", "http://localhost", nil, nil, nil)

	assert.ErrorContains(t, err, "cert required")
}

func TestNewClientWithoutClock(t *testing.T) {
	_, err := rest.NewClient("http://localhost", "00000000000000000001", "http://localhost", orgACertBundle, nil, nil)

	assert.ErrorContains(t, err, "clock required")
}
