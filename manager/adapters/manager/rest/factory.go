// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
)

type Factory struct {
	certs       *common_tls.CertificateBundle
	selfAddress string
	crlCache    *common_tls.CRLsCache
	clock       clock.Clock
}

func NewFactory(certs *common_tls.CertificateBundle, selfAddress string, crlCache *common_tls.CRLsCache, c clock.Clock) *Factory {
	return &Factory{certs: certs, selfAddress: selfAddress, crlCache: crlCache, clock: c}
}

type NewManagerArgs struct {
}

func (f *Factory) New(peerID, address string) (manager.Manager, error) {
	return NewClient(f.selfAddress, peerID, address, f.certs, f.crlCache, f.clock)
}
