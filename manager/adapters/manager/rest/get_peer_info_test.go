// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

// nolint:dupl,funlen,errcheck // table tests are long and all have similar setup
func TestGetPeerInfo(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(context.Context, *httptest.Server)
		wantErr error
		want    struct {
			PeerID   string
			PeerName string
		}
	}{
		"happy_flow": {
			setup: func(ctx context.Context, ts *httptest.Server) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					responseBody := []byte("{\"peer_id\": \"00000000000000000001\", \"peer_name\": \"test-peer\"}")
					w.Header().Add("Content-Type", "application/json")
					w.Write(responseBody)
					w.WriteHeader(http.StatusOK)
				})
			},
			want: struct {
				PeerID   string
				PeerName string
			}{"00000000000000000001", "test-peer"},
		},
		"error_response": {
			setup: func(ctx context.Context, ts *httptest.Server) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					panic("Kaboom!")
				})
			},
			wantErr: errors.New("could not get Peer info"),
		},
		"invalid_status_code": {
			setup: func(ctx context.Context, ts *httptest.Server) {
				ts.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusInternalServerError)
				})
			},
			wantErr: errors.New("received invalid status code 500:"),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
			t.Cleanup(ts.Close)

			client := getTestClient(ts)
			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, ts)
			}

			peerInfo, err := client.GetPeerInfo(ctx)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want.PeerID, peerInfo.ID().Value())
				assert.Equal(t, tt.want.PeerName, peerInfo.Name().Value())
			} else {
				assert.ErrorContains(t, err, tt.wantErr.Error())
			}
		})
	}
}
