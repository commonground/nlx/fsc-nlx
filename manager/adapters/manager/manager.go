// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package manager

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type Manager interface {
	Announce(ctx context.Context) error
	GetCertificates(ctx context.Context) (*contract.PeerCertificates, error)
	SubmitContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error
	AcceptContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error
	RejectContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error
	RevokeContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error
	GetPeerInfo(ctx context.Context) (*contract.Peer, error)
	GetPeers(ctx context.Context, peerIDs contract.PeersIDs) (contract.Peers, error)
	GetServices(ctx context.Context, peerIDs *contract.PeerID, services string) ([]*contract.Service, error)
	GetTXLogRecords(ctx context.Context) (contract.TXLogRecords, error)
}

type Factory interface {
	New(peerID, address string) (Manager, error)
}
