// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package resttxlog

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

type RestTXLog struct {
	client api.ClientWithResponsesInterface
}

func New(client api.ClientWithResponsesInterface) (*RestTXLog, error) {
	if client == nil {
		return nil, fmt.Errorf("tx log rest client is required")
	}

	return &RestTXLog{
		client: client,
	}, nil
}

func (g *RestTXLog) ListRecords(ctx context.Context, req *txlog.ListRecordsRequest) ([]*txlog.Record, error) {
	res, err := g.client.GetLogRecordsWithResponse(ctx, reqToRest(req))
	if err != nil {
		return nil, mapError(err, "could not get tx log records from rest txlog")
	}

	err = mapResponse(res, http.StatusOK, "could not get tx log records", res.Body)
	if err != nil {
		return nil, err
	}

	return restToRecords(res.JSON200.Records)
}

func restToRecords(res []models.LogRecord) ([]*txlog.Record, error) {
	records := make([]*txlog.Record, len(res))

	for i := range res {
		source, destination, err := mapSourceAndDestinationToModel(&res[i])
		if err != nil {
			return nil, fmt.Errorf("could not map source and destination: %w", err)
		}

		records[i] = &txlog.Record{
			TransactionID: res[i].TransactionId,
			GroupID:       res[i].GroupId,
			GrantHash:     res[i].GrantHash,
			ServiceName:   res[i].ServiceName,
			Direction:     mapDirection(res[i].Direction),
			Source:        source,
			Destination:   destination,
			CreatedAt:     time.Unix(res[i].CreatedAt, 0),
		}
	}

	return records, nil
}

func mapSourceAndDestinationToModel(r *models.LogRecord) (source, destination interface{}, err error) {
	sourceDiscriminator, err := r.Source.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedSourceModel := sourceDiscriminator.(type) {
	case models.Source:
		source = &txlog.RecordSource{
			OutwayPeerID: convertedSourceModel.OutwayPeerId,
		}
	case models.SourceDelegated:
		source = &txlog.RecordDelegatedSource{
			OutwayPeerID:    convertedSourceModel.OutwayPeerId,
			DelegatorPeerID: convertedSourceModel.DelegatorPeerId,
		}
	}

	destinationDiscriminator, err := r.Destination.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedDestinationModel := destinationDiscriminator.(type) {
	case models.Destination:
		destination = &txlog.RecordDestination{
			ServicePeerID: convertedDestinationModel.ServicePeerId,
		}
	case models.DestinationDelegated:
		destination = &txlog.RecordDelegatedDestination{
			ServicePeerID:   convertedDestinationModel.ServicePeerId,
			DelegatorPeerID: convertedDestinationModel.DelegatorPeerId,
		}
	}

	return source, destination, nil
}

func mapDirection(d models.LogRecordDirection) record.Direction {
	switch d {
	case models.LogRecordDirectionDIRECTIONINCOMING:
		return record.DirectionIn
	case models.LogRecordDirectionDIRECTIONOUTGOING:
		return record.DirectionOut
	default:
		return record.DirectionUnspecified
	}
}

func reqToRest(req *txlog.ListRecordsRequest) *models.GetLogRecordsParams {
	sortOrder := sortOrderToRest(req.Pagination.SortOrder)

	startID := req.Pagination.StartID.String()

	grantHashes := make([]models.GrantHash, 0)
	transactionIDs := make([]models.Uuid, 0)
	serviceNames := make([]models.ServiceName, 0)
	peerIDs := make([]models.Uuid, 0)

	var before models.Timestamp

	var after models.Timestamp

	for _, filter := range req.Filters {
		if filter.GrantHash != "" {
			grantHashes = append(grantHashes, filter.GrantHash)
		}

		if filter.TransactionID != nil {
			transactionIDs = append(transactionIDs, filter.TransactionID.String())
		}

		if filter.ServiceName != "" {
			serviceNames = append(serviceNames, filter.ServiceName)
		}

		if filter.PeerID != "" {
			peerIDs = append(peerIDs, filter.PeerID)
		}

		// Theoretically multiple filter periods can be provided, now we only take the last one
		if filter.Period != nil {
			before = filter.Period.End.Unix()
			after = filter.Period.Start.Unix()
		}
	}

	return &models.GetLogRecordsParams{
		GroupId:        req.GroupID,
		Cursor:         &startID,
		Limit:          &req.Pagination.Limit,
		GrantHash:      &grantHashes,
		TransactionIds: &transactionIDs,
		ServiceName:    &serviceNames,
		PeerId:         &peerIDs,
		After:          &after,
		Before:         &before,
		SortOrder:      &sortOrder,
	}
}

func sortOrderToRest(o txlog.SortOrder) models.SortOrder {
	switch o {
	case txlog.SortOrderAscending:
		return models.SORTORDERASCENDING
	case txlog.SortOrderDescending:
		return models.SORTORDERDESCENDING
	default:
		return models.SORTORDERASCENDING
	}
}

func (g *RestTXLog) ListRecordMetadata(ctx context.Context, transactionID string) ([]*txlog.MetadataRecord, error) {
	res, err := g.client.ListMetadataRecordsWithResponse(ctx, transactionID)
	if err != nil {
		return nil, mapError(err, "could not get tx log metadata records from rest txlog")
	}

	err = mapResponse(res, http.StatusOK, "could not get tx log metadata records", res.Body)
	if err != nil {
		return nil, err
	}

	return restToMetadataRecords(res.JSON200.Records)
}

func restToMetadataRecords(res []models.Metadata) ([]*txlog.MetadataRecord, error) {
	records := make([]*txlog.MetadataRecord, len(res))

	for i, rec := range res {
		records[i] = &txlog.MetadataRecord{
			TransactionID: rec.TransactionId,
			Direction:     mapMetadataDirection(rec.Direction),
			Metadata:      rec.AdditionalProperties,
		}
	}

	return records, nil
}

func mapMetadataDirection(d models.MetadataDirection) metadata.Direction {
	switch d {
	case models.MetadataDirectionDIRECTIONINCOMING:
		return metadata.DirectionIn
	case models.MetadataDirectionDIRECTIONOUTGOING:
		return metadata.DirectionOut
	default:
		return metadata.DirectionUnspecified
	}
}
