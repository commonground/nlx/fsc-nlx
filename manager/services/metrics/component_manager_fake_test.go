// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

//nolint:unused // most methods are unused but added to comply with the Manager interface
package metrics_test

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type fakeManager struct {
	address string
	orgCert *tls.CertificateBundle
	clock   clock.Clock
}

func (m *fakeManager) GetCertificates(_ context.Context) (*contract.PeerCertificates, error) {
	peerCert, err := contract.NewPeerCertFromCertificate(m.clock, m.orgCert.RootCAs(), m.orgCert.Cert().Certificate)
	if err != nil {
		log.Printf("failed to create peer cert: %v", err)
	}

	peerCerts := &contract.PeerCertificates{
		peerCert.CertificateThumbprint(): peerCert,
	}

	return peerCerts, nil
}

func (m *fakeManager) GetPeers(_ context.Context, _ contract.PeersIDs) (contract.Peers, error) {
	result := contract.Peers{}

	return result, nil
}

func (m *fakeManager) Announce(_ context.Context) error {
	panic(fmt.Sprintf("unintended call to announce %s", m.address))
}

func (m *fakeManager) GetContracts(_ context.Context, _ *contract.GrantType, _ *[]string) ([]*contract.Contract, error) {
	panic(fmt.Sprintf("unintended call to get contracts %s", m.address))
}

func (m *fakeManager) SubmitContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeManager) AcceptContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeManager) RejectContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeManager) RevokeContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeManager) GetPeerInfo(_ context.Context) (*contract.Peer, error) {
	panic(fmt.Sprintf("unintended call to get peer info %s", m.address))
}

func (m *fakeManager) GetServices(_ context.Context, _ *contract.PeerID, _ string) ([]*contract.Service, error) {
	return []*contract.Service{}, nil
}

func (m *fakeManager) GetToken(context.Context, string) (string, error) {
	panic("unintended call to get token")
}

func (m *fakeManager) GetTXLogRecords(_ context.Context) (contract.TXLogRecords, error) {
	panic(fmt.Sprintf("unintended call to get txlog records %s", m.address))
}
