// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package metrics_test

import (
	"context"
	"fmt"
	"log"
	"net/netip"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/authentication"
	internalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/int"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var nowInUTC = time.Now().UTC()

type fakeManagerFactory struct {
	Clock clock.Clock
}

var (
	authData = &authentication.CertificateData{
		SubjectCommonName:   "test-subject",
		PublicKeyThumbprint: "test-fingerprint",
	}

	source = common_auditlog.SourceHTTP{
		IPAddress: netip.AddrFrom4([4]byte{127, 0, 0, 1}),
		UserAgent: "test-useragent",
	}
)

func (f *fakeManagerFactory) New(_, address string) (manager.Manager, error) {
	return &fakeManager{
		address: address,
		orgCert: peerCertBundle,
		clock:   f.Clock,
	}, nil
}

var (
	testClock      = testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	peerCertBundle *common_tls.CertificateBundle
)

func TestMain(m *testing.M) {
	var err error

	peerCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

//nolint:funlen,gocyclo,gocritic // this is a test
func newService(testName string) (*internalapp.Application, contract.Repository, error) {
	return newServiceWithCustomManagerFactory(testName, &fakeManagerFactory{})
}

//nolint:funlen,gocritic // this is a long function because there is a lot to set up
func newServiceWithCustomManagerFactory(testName string, managerFactory manager.Factory) (*internalapp.Application, contract.Repository, error) {
	logger := discardlogger.New()

	postgresqlRepository, err := setupPostgresqlRepository(testName)
	if err != nil {
		return nil, nil, err
	}

	selfPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerCertBundle.GetPeerInfo().SerialNumber,
		Name:           peerCertBundle.GetPeerInfo().Name,
		ManagerAddress: "https://manager.org-a.nlx.local:443",
	})
	if err != nil {
		return nil, nil, err
	}

	controllerRepository := newFakeController()

	auditLogger, err := auditlog.New(&auditlog.NewArgs{
		AuditLogger: newFakeAuditlog(),
		GroupID:     "fsc-local",
		Clock:       testClock,
		IDGenerator: uuidgenerator.New(),
	})
	if err != nil {
		return nil, nil, err
	}

	contractDistributionService, err := services.NewContractDistributionService(&services.NewContractDistributionServiceArgs{
		Context:                              context.Background(),
		Logger:                               logger,
		Clock:                                testClock,
		FailedContractDistributionRepository: postgresqlRepository,
	})
	if err != nil {
		return nil, nil, err
	}

	peersCommunicationService, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
		Ctx:                         context.Background(),
		Logger:                      logger,
		LocalRepo:                   postgresqlRepository,
		ManagerFactory:              managerFactory,
		SelfPeerID:                  selfPeer.ID(),
		ContractDistributionService: contractDistributionService,
		Clock:                       testClock,
	})
	if err != nil {
		return nil, nil, err
	}

	internalApp, err := internalservice.NewApplication(&internalservice.NewApplicationArgs{
		Context:                     context.Background(),
		Clock:                       testClock,
		Logger:                      logger,
		Repository:                  postgresqlRepository,
		PeersCommunicationService:   peersCommunicationService,
		ContractDistributionService: contractDistributionService,
		SelfPeer:                    selfPeer,
		TrustedExternalRootCAs:      peerCertBundle.RootCAs(),
		SignatureCertificate:        peerCertBundle.Cert(),
		ControllerRepository:        controllerRepository,
		ManagerFactory:              managerFactory,
		GroupID:                     "fsc-local",
		DirectoryPeerID:             selfPeer.ID().Value(),
		DirectoryPeerManagerAddress: "https://directory.nlx.local:443",
		DirectoryPeerName:           "",
		IsTxlogDisabled:             true,
		Auditlogger:                 auditLogger,
		InternalCertificate:         peerCertBundle,
	})
	if err != nil {
		return nil, nil, fmt.Errorf("failed to create application: %w", err)
	}

	return internalApp, postgresqlRepository, nil
}

func setupPostgresqlRepository(testName string) (contract.Repository, error) {
	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)
	dbName = strings.Replace(dbName, "test", "", 1) // strip 'test'-prefix
	dbName = fmt.Sprintf("int_unauthenticated_%s", dbName)
	dbName = strings.ReplaceAll(dbName, "/", "_")

	// via https://stackoverflow.com/a/27865772/363448
	const maxLengthDatabaseName = 63

	if len(dbName) > maxLengthDatabaseName {
		return nil, fmt.Errorf("database name too long (%d > 63 characters): %q", len(dbName), dbName)
	}

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		return nil, fmt.Errorf("failed to setup test database: %v", err)
	}

	db, err := postgresadapter.NewConnection(context.Background(), testDB)
	if err != nil {
		return nil, fmt.Errorf("failed to create db connection: %v", err)
	}

	err = postgresadapter.PerformMigrations(testDB)
	if err != nil {
		return nil, fmt.Errorf("failed to perform dbmigrations: %v", err)
	}

	postgresqlRepository, err := postgresadapter.New(peerCertBundle.RootCAs(), db, testClock)
	if err != nil {
		return nil, fmt.Errorf("failed to setup postgresql database: %s", err)
	}

	return postgresqlRepository, nil
}
