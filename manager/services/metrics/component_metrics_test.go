// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package metrics_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/metrics"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

//nolint:dupl // looks the same but is a different test case
func TestMetricNumberOfContractsDistributionFailures(t *testing.T) {
	internalApp, repo, err := newService(t.Name())
	require.NoError(t, err)

	m := metrics.NewNumberOfContractsDistributionFailures(context.Background(), repo)

	_, err = internalApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerCertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerCertBundle.GetPeerInfo().SerialNumber,
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: "12345678901234567890",
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	expiration := 1 * time.Second
	timeStarted := time.Now()

	// since sending the Contract to the other organization happens in a go-routine, the distribution is
	// not executed immediately. So, we poll the failed distributions until we have one.
	for {
		err = m.Collect()
		require.NoError(t, err)

		if m.Value().(int) > 0 {
			break
		}

		shouldExpire := timeStarted.After(timeStarted.Add(expiration))

		if shouldExpire {
			require.Fail(t, fmt.Sprintf("forced failure, since test took longer than expected %q", expiration))
		}
	}

	assert.Equal(t, 1, m.Value())
}

//nolint:dupl // looks the same but is a different test case
func TestMetricNumberOfContractsWaitingForSignature(t *testing.T) {
	internalApp, repo, err := newService(t.Name())
	require.NoError(t, err)

	m := metrics.NewNumberOfContractsWaitingForSignature(context.Background(), repo)

	_, err = internalApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerCertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerCertBundle.GetPeerInfo().SerialNumber,
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: "12345678901234567890",
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	_, err = internalApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerCertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerCertBundle.GetPeerInfo().SerialNumber,
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: "12345678901234567892",
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	err = m.Collect()
	require.NoError(t, err)

	assert.Equal(t, 2, m.Value())
}

//nolint:dupl // looks the same but is a different test case
func TestMetricContractsWaitingForSignature72Hours(t *testing.T) {
	internalApp, repo, err := newService(t.Name())
	require.NoError(t, err)

	m := metrics.NewNumberOfContractsWaitingForSignatureInLast72Hours(context.Background(), repo, testClock)

	_, err = internalApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerCertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerCertBundle.GetPeerInfo().SerialNumber,
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: "12345678901234567890",
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	_, err = internalApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerCertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerCertBundle.GetPeerInfo().SerialNumber,
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: "12345678901234567892",
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second).Add(-72*time.Hour - time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	err = m.Collect()
	require.NoError(t, err)

	assert.Equal(t, 1, m.Value())
}
