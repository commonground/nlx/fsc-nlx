// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package metrics_test

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/controller"
)

type FakeController struct{}

func newFakeController() controller.Controller {
	return &FakeController{}
}

func (c *FakeController) GetService(_ context.Context, _ string) (*controller.Service, error) {
	return nil, nil
}
