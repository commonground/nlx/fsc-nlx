// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

// nolint:funlen // this is a test
func TestGetLogs(t *testing.T) {
	t.Parallel()

	sortOrderAscending := models.FSCLoggingSortOrderSORTORDERASCENDING
	sortOrderDescending := models.FSCLoggingSortOrderSORTORDERDESCENDING
	limit := models.FSCLoggingQueryPaginationLimit(10)
	timestamp := time.Now().Unix()
	cursor := models.FSCLoggingQueryPaginationCursor("01899c62-eba5-7b58-b68d-000000000002")

	logRecordsFromFakeTransactionLog := getLogRecordsFromFakeTransactionLog(t)

	tests := map[string]struct {
		Params         *models.GetLogsParams
		WantStatusCode int
		WantResponse   []models.FSCLoggingLogRecord
	}{
		"with_sort_ascending": {
			Params: &models.GetLogsParams{
				SortOrder: &sortOrderAscending,
			},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		"with_sort_descending": {
			Params: &models.GetLogsParams{
				SortOrder: &sortOrderDescending,
			},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		"with_limit": {
			Params: &models.GetLogsParams{
				Limit: &limit,
			},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		"with_cursor": {
			Params: &models.GetLogsParams{
				Cursor: &cursor,
			},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		"filter_by_start_date": {
			Params: &models.GetLogsParams{
				After: &timestamp,
			},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		"filter_by_end_date": {
			Params: &models.GetLogsParams{
				Before: &timestamp,
			},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		"filter_by_grant_hash": {
			Params: &models.GetLogsParams{
				GrantHash: &[]models.FSCLoggingGrantHash{"arbitrary-grant-hash"},
			},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		"filter_by_service_name": {
			Params: &models.GetLogsParams{
				ServiceName: &[]models.FSCLoggingServiceName{"arbitrary-service-name"},
			},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		"filter_by_transaction_ids_invalid_id": {
			Params: &models.GetLogsParams{
				TransactionIds: &[]models.FSCLoggingUuid{"arbitrary-id"},
			},
			WantStatusCode: http.StatusInternalServerError,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		"filter_by_transaction_ids": {
			Params: &models.GetLogsParams{
				TransactionIds: &[]models.FSCLoggingUuid{"01899c62-eba5-7b58-b68d-000000000002"},
			},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
		// Manager-TransactionLogRecords-1
		"happy_flow": {
			Params:         &models.GetLogsParams{},
			WantStatusCode: http.StatusOK,
			WantResponse:   logRecordsFromFakeTransactionLog,
		},
	}

	// Arrange
	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			// Act
			res, err := client.GetLogsWithResponse(context.Background(), tc.Params)
			require.NoError(t, err)

			// Assert
			if !assert.Equal(t, tc.WantStatusCode, res.StatusCode()) {
				t.Errorf("response body: %s", res.Body)
			}

			if tc.WantStatusCode == http.StatusOK {
				assert.Len(t, res.JSON200.Records, len(tc.WantResponse))

				for i, log := range tc.WantResponse {
					assert.Equal(t, log, res.JSON200.Records[i])
				}
			}
		})
	}
}

func getLogRecordsFromFakeTransactionLog(t *testing.T) []models.FSCLoggingLogRecord {
	logRecordDelegatedOutgoing := models.FSCLoggingLogRecord{
		CreatedAt:     time.Date(2023, 1, 1, 1, 42, 0, 0, time.Local).Unix(),
		Destination:   models.FSCLoggingLogRecord_Destination{},
		Direction:     models.DIRECTIONOUTGOING,
		GrantHash:     "$1$4$testhash",
		ServiceName:   "test-service",
		TransactionId: "01899c62-eba5-7b58-b68d-000000000001",
	}

	err := logRecordDelegatedOutgoing.Source.FromFSCLoggingSourceDelegated(models.FSCLoggingSourceDelegated{
		DelegatorPeerId: "3",
		OutwayPeerId:    "1",
		Type:            models.SOURCETYPEDELEGATEDSOURCE,
	})
	require.NoError(t, err)

	err = logRecordDelegatedOutgoing.Destination.FromFSCLoggingDestinationDelegated(models.FSCLoggingDestinationDelegated{
		ServicePeerId:   "2",
		DelegatorPeerId: "3",
		Type:            models.DESTINATIONTYPEDELEGATEDDESTINATION,
	})
	require.NoError(t, err)

	logRecordDelegatedIncoming := models.FSCLoggingLogRecord{
		CreatedAt:     time.Date(2023, 1, 1, 1, 42, 0, 0, time.Local).Unix(),
		Destination:   models.FSCLoggingLogRecord_Destination{},
		Direction:     models.DIRECTIONINCOMING,
		GrantHash:     "$1$4$testhash",
		ServiceName:   "test-service",
		TransactionId: "01899c62-eba5-7b58-b68d-000000000001",
	}

	err = logRecordDelegatedIncoming.Source.FromFSCLoggingSourceDelegated(models.FSCLoggingSourceDelegated{
		OutwayPeerId:    "1",
		DelegatorPeerId: "3",
		Type:            models.SOURCETYPEDELEGATEDSOURCE,
	})
	require.NoError(t, err)

	err = logRecordDelegatedIncoming.Destination.FromFSCLoggingDestinationDelegated(models.FSCLoggingDestinationDelegated{
		ServicePeerId:   "2",
		DelegatorPeerId: "3",
		Type:            models.DESTINATIONTYPEDELEGATEDDESTINATION,
	})
	require.NoError(t, err)

	logRecordOutgoing := models.FSCLoggingLogRecord{
		CreatedAt:     time.Date(2023, 1, 1, 1, 42, 0, 0, time.Local).Unix(),
		Destination:   models.FSCLoggingLogRecord_Destination{},
		Direction:     models.DIRECTIONOUTGOING,
		GrantHash:     "$1$4$testhash2",
		ServiceName:   "test-service",
		TransactionId: "01899c62-eba5-7b58-b68d-000000000002",
	}

	err = logRecordOutgoing.Source.FromFSCLoggingSource(models.FSCLoggingSource{
		OutwayPeerId: "1",
		Type:         models.SOURCETYPESOURCE,
	})
	require.NoError(t, err)

	err = logRecordOutgoing.Destination.FromFSCLoggingDestination(models.FSCLoggingDestination{
		ServicePeerId: "2",
		Type:          models.DESTINATIONTYPEDESTINATION,
	})
	require.NoError(t, err)

	logRecordIncoming := models.FSCLoggingLogRecord{
		CreatedAt:     time.Date(2023, 1, 1, 1, 42, 0, 0, time.Local).Unix(),
		Destination:   models.FSCLoggingLogRecord_Destination{},
		Direction:     models.DIRECTIONINCOMING,
		GrantHash:     "$1$4$testhash2",
		ServiceName:   "test-service",
		TransactionId: "01899c62-eba5-7b58-b68d-000000000002",
	}

	err = logRecordIncoming.Source.FromFSCLoggingSource(models.FSCLoggingSource{
		OutwayPeerId: "1",
		Type:         models.SOURCETYPESOURCE,
	})
	require.NoError(t, err)

	err = logRecordIncoming.Destination.FromFSCLoggingDestination(models.FSCLoggingDestination{
		ServicePeerId: "2",
		Type:          models.DESTINATIONTYPEDESTINATION,
	})
	require.NoError(t, err)

	return []models.FSCLoggingLogRecord{
		logRecordDelegatedOutgoing,
		logRecordDelegatedIncoming,
		logRecordOutgoing,
		logRecordIncoming,
	}
}
