// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	internal_models "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
	testsignature "gitlab.com/commonground/nlx/fsc-nlx/testing/signature"
)

//nolint:funlen // this is a test
func TestSuiteRejectContact(t *testing.T) {
	testCases := map[string]struct {
		Description           string
		ContractToSignArgs    *command.CreateContractHandlerArgs
		RequestURLHash        string
		RejectContractArgs    models.RejectContractJSONRequestBody
		WantStatusCode        int
		WantResponseDomain    models.FSCCoreErrorDomain
		WantResponseErrorCode string
	}{
		"Manager-RejectContract-1": {
			Description: "Place reject signature on a Contract",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							PeerID: peerB.GetPeerID(),
							Name:   "parkeerrechten",
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RejectContractArgs: func() models.RejectContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateReject(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RejectContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusCreated,
			WantResponseDomain:    "",
			WantResponseErrorCode: "",
		},
		"Manager-RejectContract-2": {
			Description: "Place accept signature on a Contract without being a Peer on the Contract",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							PeerID: peerA.GetPeerID(),
							Name:   "parkeerrechten",
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerA.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RejectContractArgs: func() models.RejectContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerA.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerA.GetPeerID(),
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RejectContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODEPEERNOTPARTOFCONTRACT),
		},
		"Manager-RejectContract-3": {
			Description: "Place reject signature which is not a valid JWS",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							PeerID: peerB.GetPeerID(),
							Name:   "parkeerrechten",
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RejectContractArgs: func() models.RejectContractJSONRequestBody {
				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RejectContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: "not-a-valid-jws",
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODESIGNATUREVERIFICATIONFAILED),
		},
		"Manager-RejectContract-4": {
			Description: "Place a reject signature of which the content hash does not match the contract content you are accepting",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							PeerID: peerB.GetPeerID(),
							Name:   "parkeerrechten",
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RejectContractArgs: func() models.RejectContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "a-different-group-id",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				rejectSig, err := testsignature.CreateReject(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RejectContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: rejectSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODESIGNATURECONTRACTCONTENTHASHMISMATCH),
		},
		"Manager-RejectContract-5": {
			Description: "Place reject signature on a Contract with a signature type that is not reject",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							PeerID: peerB.GetPeerID(),
							Name:   "parkeerrechten",
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RejectContractArgs: func() models.RejectContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RejectContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODESIGNATUREVERIFICATIONFAILED),
		},
		"Manager-RejectContract-6": {
			Description: "The hash in the request URL does not match with the contract hash of the contract in the request body",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							PeerID: peerB.GetPeerID(),
							Name:   "parkeerrechten",
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20830",
					GroupID:        "fsc-local-different-group-id",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RejectContractArgs: func() models.RejectContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RejectContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODEURLPATHCONTENTHASHMISMATCH),
		},
		"Manager-RejectContract-7": {
			Description: "Place reject signature with a unsupported algorithm on a Contract",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							PeerID: peerB.GetPeerID(),
							Name:   "parkeerrechten",
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RejectContractArgs: func() models.RejectContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateHMACReject(contentHash, peerB.CertBundle.CertificateThumbprint(), testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RejectContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: "ERROR_CODE_UNKNOWN_ALGORITHM_SIGNATURE",
		},
	}

	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})

	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	for name, testCase := range testCases {
		tc := testCase

		t.Run(name, func(t *testing.T) {
			if tc.ContractToSignArgs != nil {
				_, err := intApp.Commands.CreateContract.Handle(context.Background(), tc.ContractToSignArgs)
				assert.NoError(t, err)
			}

			resp, errSubmit := client.RejectContractWithResponse(context.Background(), tc.RequestURLHash, &models.RejectContractParams{
				FscManagerAddress: peerB.ManagerAddress,
			}, tc.RejectContractArgs)
			assert.NoError(t, errSubmit)

			if !assert.Equal(t, testCase.WantStatusCode, resp.StatusCode()) {
				t.Errorf("response body: %s", resp.Body)
			}

			if testCase.WantResponseDomain != "" || testCase.WantResponseErrorCode != "" {
				switch testCase.WantStatusCode {
				case http.StatusBadRequest:
					assert.Equal(t, testCase.WantResponseErrorCode, resp.JSON400.Code)
					assert.Equal(t, testCase.WantResponseDomain, resp.JSON400.Domain)

				case http.StatusUnprocessableEntity:
					assert.Equal(t, testCase.WantResponseErrorCode, resp.JSON422.Code)
					assert.Equal(t, testCase.WantResponseDomain, resp.JSON422.Domain)

				default:
					t.Fatalf("unexpected status code %d", resp.StatusCode())
				}
			}

			if testCase.WantStatusCode != http.StatusCreated {
				return
			}

			var limit = models.FSCCoreQueryPaginationLimit(20)

			contracts, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
				Limit: &limit,
			})
			assert.NoError(t, err)

			var rejectedContract models.FSCCoreContract

			for _, c := range contracts.JSON200.Contracts {
				if c.Content.Iv != testCase.RejectContractArgs.ContractContent.Iv {
					continue
				}

				rejectedContract = c
			}

			assert.NotNil(t, rejectedContract)
			assert.Contains(t, rejectedContract.Signatures.Reject, peerB.GetPeerID())
			assert.Equal(t, testCase.RejectContractArgs.Signature, rejectedContract.Signatures.Reject[peerB.GetPeerID()])
		})
	}
}
