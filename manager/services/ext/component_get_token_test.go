// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"fmt"
	"math/rand/v2"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	internal_models "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// Manager-GetToken-1
func TestGetTokenConnectionGrant(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	peerCert, err := contract.NewPeerCertFromCertificate(testClock, peerA.CertBundle.RootCAs(), peerA.CertBundle.Cert().Certificate)
	require.NoError(t, err)

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	require.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	require.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     testClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	require.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              peerB.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
	require.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	require.NoError(t, err)

	resp, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})
	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, resp.StatusCode()) {
		t.Errorf("response body: %s", resp.Body)
	}

	tokenExpirationDate := testClock.Now().Add(accessTokenTTL)

	clientID := peerB.GetPeerID()

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		ClientId:  clientID,
	})

	// Assert
	require.NoError(t, errReq)

	if !assert.Equal(t, http.StatusOK, tokenResp.StatusCode()) {
		t.Errorf("response body: %s", tokenResp.Body)
	}

	receivedToken, errTokenDecode := accesstoken.DecodeFromString(testClock, peerCert, tokenResp.JSON200.AccessToken)
	require.NoError(t, errTokenDecode)

	require.Equal(t, &accesstoken.DecodedToken{
		GroupID:                     contractContent.GroupID(),
		GrantHash:                   contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		OutwayPeerID:                peerB.CertBundle.GetPeerInfo().SerialNumber,
		OutwayCertificateThumbprint: peerB.CertBundle.CertificateThumbprint(),
		ServiceName:                 "parkeerrechten",
		ServiceInwayAddress:         "https://inway.address.com:443",
		ServicePeerID:               peerA.CertBundle.GetPeerInfo().SerialNumber,
		ExpiryDate:                  tokenExpirationDate.Add(time.Duration(-1 * tokenExpirationDate.Nanosecond())), // since unix timestamps do not support nanoseconds
	}, receivedToken)
}

// Manager-GetToken-2
func TestGetTokenInvalidContract(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	require.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	require.NoError(t, err)

	unsignedContractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     testClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	require.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                unsignedContractContent.IV().String(),
		GroupID:           unsignedContractContent.GroupID(),
		ContractNotBefore: unsignedContractContent.NotBefore(),
		ContractNotAfter:  unsignedContractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: unsignedContractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              unsignedContractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      unsignedContractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	clientID := peerB.GetPeerID()

	grantHash := unsignedContractContent.Grants().ServiceConnectionGrants()[0].Hash().String()

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     grantHash,
		ClientId:  clientID,
	})

	// Assert
	require.NoError(t, errReq)
	require.Equal(t, http.StatusBadRequest, tokenResp.StatusCode())

	if !assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode()) {
		t.Errorf("response body: %s", tokenResp.Body)
	}

	require.Equal(t, "invalid_scope", string(tokenResp.JSON400.Error))
	require.Equal(t, fmt.Sprintf("no valid contract found for grant hash %q", grantHash), *tokenResp.JSON400.ErrorDescription)
}

// Manager-GetToken-3
func TestGetTokenRevokedContract(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	require.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	require.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     testClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	require.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
	require.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	require.NoError(t, err)

	respAccept, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})

	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, respAccept.StatusCode()) {
		t.Errorf("response body: %s", respAccept.Body)
	}

	revokedSignatureOrgB, err := contractContent.Revoke(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
	require.NoError(t, err)

	respRevoke, err := clientOrgB.RevokeContractWithResponse(context.Background(), contractContent.Hash().String(), &models.RevokeContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.RevokeContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       revokedSignatureOrgB.JWS(),
	})

	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, respRevoke.StatusCode()) {
		t.Errorf("token response body: %s", respRevoke.Body)
	}

	clientID := peerB.GetPeerID()

	grantHash := contractContent.Grants().ServiceConnectionGrants()[0].Hash().String()

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     grantHash,
		ClientId:  clientID,
	})

	// Assert
	require.NoError(t, errReq)

	if !assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode()) {
		t.Errorf("token response body: %s", tokenResp.Body)
	}

	require.Equal(t, "invalid_scope", string(tokenResp.JSON400.Error))
	require.Equal(t, fmt.Sprintf("no valid contract found for grant hash %q", grantHash), *tokenResp.JSON400.ErrorDescription)
}

// Manager-GetToken-4
func TestGetTokenInvalidGrantInScope(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	require.NoError(t, err)

	clientID := peerB.GetPeerID()

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     "invalid scope",
		ClientId:  clientID,
	})

	// Assert
	require.NoError(t, errReq)
	require.Equal(t, http.StatusBadRequest, tokenResp.StatusCode())

	if !assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode()) {
		t.Errorf("response body: %s", tokenResp.Body)
	}

	assert.Equal(t, "invalid_scope", string(tokenResp.JSON400.Error))
	assert.Equal(t, "invalid grant in scope", *tokenResp.JSON400.ErrorDescription)
}

// Manager-GetToken-5
func TestGetTokenUnsupportedGrantType(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	require.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	require.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     testClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	require.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
	require.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	require.NoError(t, err)

	respAccept, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})

	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, respAccept.StatusCode()) {
		t.Errorf("response body: %s", respAccept.Body)
	}

	clientID := peerB.GetPeerID()

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: "authorization_code",
		Scope:     contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		ClientId:  clientID,
	})

	// Assert
	require.NoError(t, errReq)

	if !assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode()) {
		t.Errorf("response body: %s", tokenResp.Body)
	}

	assert.Equal(t, "unsupported_grant_type", string(tokenResp.JSON400.Error))
	assert.Equal(t, "provided grant type is not supported. only client_credentials is supported", *tokenResp.JSON400.ErrorDescription)
}

// Manager-GetToken-6
func TestGetTokenDelegatedConnectionGrant(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	peerCert, err := contract.NewPeerCertFromCertificate(testClock, peerA.CertBundle.RootCAs(), peerA.CertBundle.Cert().Certificate)
	require.NoError(t, err)

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	require.NoError(t, err)

	clientOrgC, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerC.CertBundle)
	require.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	require.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     testClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerC.GetPeerID(),
					},
				},
			},
		},
	})
	require.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
				DelegatorPeerID: contractContent.Grants().DelegatedServiceConnectionGrants()[0].Delegator().Peer().ID().Value(),
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
	require.NoError(t, err)

	acceptSignatureOrgC, err := contractContent.Accept(peerC.CertBundle.RootCAs(), peerC.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
	require.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	require.NoError(t, err)

	respAcceptOrgB, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})
	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, respAcceptOrgB.StatusCode()) {
		t.Errorf("response body: %s", respAcceptOrgB.Body)
	}

	respAcceptOrgC, err := clientOrgC.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerC.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgC.JWS(),
	})
	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, respAcceptOrgC.StatusCode()) {
		t.Errorf("response body: %s", respAcceptOrgC.Body)
	}

	tokenExpirationDate := testClock.Now().Add(accessTokenTTL)

	clientID := peerB.GetPeerID()

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     contractContent.Grants().DelegatedServiceConnectionGrants()[0].Hash().String(),
		ClientId:  clientID,
	})

	// Assert
	require.NoError(t, errReq)

	if !assert.Equal(t, http.StatusOK, tokenResp.StatusCode()) {
		t.Fatalf("response body: %s", tokenResp.Body)
	}

	receivedToken, errTokenDecode := accesstoken.DecodeFromString(testClock, peerCert, tokenResp.JSON200.AccessToken)
	require.NoError(t, errTokenDecode)

	require.Equal(t, &accesstoken.DecodedToken{
		GroupID:                     contractContent.GroupID(),
		GrantHash:                   contractContent.Grants().DelegatedServiceConnectionGrants()[0].Hash().String(),
		OutwayPeerID:                peerB.CertBundle.GetPeerInfo().SerialNumber,
		OutwayCertificateThumbprint: peerB.CertBundle.CertificateThumbprint(),
		OutwayDelegatorPeerID:       peerC.CertBundle.GetPeerInfo().SerialNumber,
		ServiceName:                 "parkeerrechten",
		ServiceInwayAddress:         "https://inway.address.com:443",
		ServicePeerID:               peerA.CertBundle.GetPeerInfo().SerialNumber,
		ExpiryDate:                  tokenExpirationDate.Add(time.Duration(-1 * tokenExpirationDate.Nanosecond())), // since unix timestamps do not support nanoseconds
	}, receivedToken)
}

// Manager-GetToken-7
func TestGetTokenConnectionGrantDelegatedPublication(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	peerCert, err := contract.NewPeerCertFromCertificate(testClock, peerA.CertBundle.RootCAs(), peerA.CertBundle.Cert().Certificate)
	require.NoError(t, err)

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	require.NoError(t, err)

	clientOrgC, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerC.CertBundle)
	require.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	require.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     testClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionDelegatedServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
					PublicationDelegator: &contract.NewPeerArgs{
						ID: peerC.GetPeerID(),
					},
				},
			},
		},
	})
	require.NoError(t, err)

	service, ok := contractContent.Grants().ServiceConnectionGrants()[0].Service().(*contract.GrantServiceConnectionDelegatedService)
	require.True(t, ok)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewDelegatedServiceArgs{
					Name:                       service.Name(),
					PeerID:                     service.Peer().ID().Value(),
					PublicationDelegatorPeerID: service.PublicationDelegator().ID().Value(),
				},
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
	require.NoError(t, err)

	acceptSignatureOrgC, err := contractContent.Accept(peerC.CertBundle.RootCAs(), peerC.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
	require.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	require.NoError(t, err)

	resp, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})
	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, resp.StatusCode()) {
		t.Errorf("response body: %s", resp.Body)
	}

	resp, err = clientOrgC.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerC.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgC.JWS(),
	})
	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, resp.StatusCode()) {
		t.Errorf("response body: %s", resp.Body)
	}

	tokenExpirationDate := testClock.Now().Add(accessTokenTTL)

	clientID := peerB.GetPeerID()

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		ClientId:  clientID,
	})

	// Assert
	require.NoError(t, errReq)

	if !assert.Equal(t, http.StatusOK, tokenResp.StatusCode()) {
		t.Errorf("response body: %s", tokenResp.Body)
	}

	receivedToken, errTokenDecode := accesstoken.DecodeFromString(testClock, peerCert, tokenResp.JSON200.AccessToken)
	require.NoError(t, errTokenDecode)

	require.Equal(t, &accesstoken.DecodedToken{
		GroupID:                     contractContent.GroupID(),
		GrantHash:                   contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		OutwayPeerID:                peerB.CertBundle.GetPeerInfo().SerialNumber,
		OutwayCertificateThumbprint: peerB.CertBundle.CertificateThumbprint(),
		ServiceName:                 "parkeerrechten",
		ServiceInwayAddress:         "https://inway.address.com:443",
		ServicePeerID:               peerA.CertBundle.GetPeerInfo().SerialNumber,
		ServiceDelegatorPeerID:      peerC.CertBundle.GetPeerInfo().SerialNumber,
		ExpiryDate:                  tokenExpirationDate.Add(time.Duration(-1 * tokenExpirationDate.Nanosecond())), // since unix timestamps do not support nanoseconds
	}, receivedToken)
}

// Manager-GetToken-8
// nolint:dupl // structure is similar but the details differ
func TestGetTokenNoClientID(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	unsignedContractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     testClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                unsignedContractContent.IV().String(),
		GroupID:           unsignedContractContent.GroupID(),
		ContractNotBefore: unsignedContractContent.NotBefore(),
		ContractNotAfter:  unsignedContractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: unsignedContractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              unsignedContractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      unsignedContractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     unsignedContractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
	})

	// Assert
	assert.NoError(t, errReq)
	assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode())
	assert.Equal(t, "invalid_request", string(tokenResp.JSON400.Error))
	assert.Equal(t, "client_id is required in Token request", *tokenResp.JSON400.ErrorDescription)
}

// Manager-GetToken-9
// nolint:dupl // structure is similar but the details differ
func TestGetTokenInvalidClientID(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	unsignedContractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     testClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                unsignedContractContent.IV().String(),
		GroupID:           unsignedContractContent.GroupID(),
		ContractNotBefore: unsignedContractContent.NotBefore(),
		ContractNotAfter:  unsignedContractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: unsignedContractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              unsignedContractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      unsignedContractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	invalidClientID := "this is not a valid client_id"

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     unsignedContractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		ClientId:  invalidClientID,
	})

	// Assert
	assert.NoError(t, errReq)
	assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode())
	assert.Equal(t, "invalid_request", string(tokenResp.JSON400.Error))
	assert.Equal(t, "client_id does not contain a valid value, must match the ID of the client Peer", *tokenResp.JSON400.ErrorDescription)
}

// nolint:funlen,dupl // this is a test
func TestGetToken_InvalidArguments(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})

	t.Cleanup(func() {
		defer externalHTTPServer.Close()
	})

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	require.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	require.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     testClock.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now().Truncate(time.Second),
			NotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	require.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
	require.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	require.NoError(t, err)

	respAcceptOrgB, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})
	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, respAcceptOrgB.StatusCode()) {
		t.Errorf("response body: %s", respAcceptOrgB.Body)
	}

	tests := map[string]struct {
		RequestBody models.GetTokenFormdataRequestBody
		WantStatus  int
		WantError   string
	}{
		"empty_scope": {
			RequestBody: models.GetTokenFormdataRequestBody{
				GrantType: models.ClientCredentials,
				Scope:     "",
				ClientId:  peerB.GetPeerID(),
			},
			WantStatus: http.StatusBadRequest,
			WantError:  "invalid grant in scope",
		},
		"scope_too_long": {
			RequestBody: models.GetTokenFormdataRequestBody{
				GrantType: models.ClientCredentials,
				Scope:     randomString(112),
				ClientId:  peerB.GetPeerID(),
			},
			WantStatus: http.StatusBadRequest,
			WantError:  "invalid grant in scope",
		},
		"hash_does_not_start_with_$": {
			RequestBody: models.GetTokenFormdataRequestBody{
				GrantType: models.ClientCredentials,
				Scope:     "foobar",
				ClientId:  peerB.GetPeerID(),
			},
			WantStatus: http.StatusBadRequest,
			WantError:  "invalid grant in scope",
		},
		"hash_consist_of_one_part_only": {
			RequestBody: models.GetTokenFormdataRequestBody{
				GrantType: models.ClientCredentials,
				Scope:     "$",
				ClientId:  peerB.GetPeerID(),
			},
			WantStatus: http.StatusBadRequest,
			WantError:  "invalid grant in scope",
		},
		"invalid_client_id": {
			RequestBody: models.GetTokenFormdataRequestBody{
				GrantType: models.ClientCredentials,
				Scope:     contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
				ClientId:  "arbitrary",
			},
			WantStatus: http.StatusBadRequest,
			WantError:  "client_id does not contain a valid value, must match the ID of the client Peer",
		},
		"invalid_grant_type": {
			RequestBody: models.GetTokenFormdataRequestBody{
				GrantType: "arbitrary",
				Scope:     contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
				ClientId:  peerB.GetPeerID(),
			},
			WantStatus: http.StatusBadRequest,
			WantError:  "provided grant type is not supported. only client_credentials is supported",
		},
	}

	for name, tc := range tests {
		testCase := tc

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			// Act
			tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), testCase.RequestBody)

			// Assert
			require.NoError(t, errReq)

			if !assert.Equal(t, testCase.WantStatus, tokenResp.StatusCode()) {
				t.Fatalf("response body: %s", tokenResp.Body)
			}

			if testCase.WantStatus == http.StatusBadRequest {
				assert.Equal(t, tc.WantError, *tokenResp.JSON400.ErrorDescription)
			}
		})
	}
}

func randomString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.IntN(len(letters))] // nolint:gosec // it is ok to use a weak random generator here
	}

	return string(s)
}
