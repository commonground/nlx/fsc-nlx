// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	internal_models "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// nolint:funlen,dupl // this is a test
func TestGetServices(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		Params         *models.GetServicesParams
		Prepare        func(*testing.T, *api.ClientWithResponses, *internalapp.Application)
		WantStatusCode int
		WantResponse   []models.FSCCoreServiceListing
	}{
		"with_invalid_cursor": {
			Params: func() *models.GetServicesParams {
				peerID := peerA.GetPeerID()
				serviceName := "parkeerrechten"
				cursor := "invalid"

				return &models.GetServicesParams{
					Cursor:      &cursor,
					Limit:       nil,
					SortOrder:   nil,
					PeerId:      &peerID,
					ServiceName: &serviceName,
				}
			}(),
			Prepare: func(t *testing.T, client *api.ClientWithResponses, intApp *internalapp.Application) {
				createContractWithServicePublicationGrant(t, client, intApp, uuid.New(), "parkeerrechten")
			},
			WantStatusCode: http.StatusUnprocessableEntity,
			WantResponse:   nil,
		},
		"with_cursor": {
			Params: func() *models.GetServicesParams {
				cursor := "102635ef-7053-4151-8fd1-2537f99bee79"

				return &models.GetServicesParams{
					Cursor:      &cursor,
					Limit:       nil,
					SortOrder:   nil,
					PeerId:      nil,
					ServiceName: nil,
				}
			}(),
			Prepare: func(t *testing.T, client *api.ClientWithResponses, intApp *internalapp.Application) {
				createContractWithServicePublicationGrant(t, client, intApp, uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79"), "parkeerrechten")
			},
			WantStatusCode: http.StatusOK,
			WantResponse: func() []models.FSCCoreServiceListing {
				return []models.FSCCoreServiceListing{}
			}(),
		},
		"with_limit": {
			Params: func() *models.GetServicesParams {
				serviceName := "basisregister-fictieve-personen"
				limit := models.FSCCoreQueryPaginationLimit(1)

				return &models.GetServicesParams{
					Cursor:      nil,
					Limit:       &limit,
					SortOrder:   nil,
					PeerId:      nil,
					ServiceName: &serviceName,
				}
			}(),
			Prepare: func(t *testing.T, client *api.ClientWithResponses, intApp *internalapp.Application) {
				createContractWithServicePublicationGrant(t, client, intApp, uuid.New(), "basisregister-fictieve-personen")
			},
			WantStatusCode: http.StatusOK,
			WantResponse: func() []models.FSCCoreServiceListing {
				data := models.FSCCoreServiceListing_Data{}

				_ = data.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
					Type: models.SERVICETYPESERVICE,
					Name: "basisregister-fictieve-personen",
					Peer: models.FSCCorePeer{
						Id:             peerA.GetPeerID(),
						ManagerAddress: peerA.ManagerAddress,
						Name:           peerA.GetName(),
					},
					Protocol: models.PROTOCOLTCPHTTP11,
				})

				return []models.FSCCoreServiceListing{
					{
						Data: data,
					},
				}
			}(),
		},
		"with_peer_id": {
			Params: func() *models.GetServicesParams {
				peerID := peerA.GetPeerID()

				return &models.GetServicesParams{
					Cursor:      nil,
					Limit:       nil,
					SortOrder:   nil,
					PeerId:      &peerID,
					ServiceName: nil,
				}
			}(),
			Prepare: func(t *testing.T, client *api.ClientWithResponses, intApp *internalapp.Application) {
				createContractWithServicePublicationGrant(t, client, intApp, uuid.New(), "basisregister-fictieve-kentekens")
			},
			WantStatusCode: http.StatusOK,
			WantResponse: func() []models.FSCCoreServiceListing {
				data := models.FSCCoreServiceListing_Data{}

				_ = data.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
					Type: models.SERVICETYPESERVICE,
					Name: "basisregister-fictieve-kentekens",
					Peer: models.FSCCorePeer{
						Id:             peerA.GetPeerID(),
						ManagerAddress: peerA.ManagerAddress,
						Name:           peerA.GetName(),
					},
					Protocol: models.PROTOCOLTCPHTTP11,
				})

				return []models.FSCCoreServiceListing{
					{
						Data: data,
					},
				}
			}(),
		},
		"with_sort_order_ascending": {
			Params: func() *models.GetServicesParams {
				sortOrder := models.FSCCoreSortOrderSORTORDERASCENDING

				return &models.GetServicesParams{
					Cursor:      nil,
					Limit:       nil,
					SortOrder:   &sortOrder,
					PeerId:      nil,
					ServiceName: nil,
				}
			}(),
			Prepare: func(t *testing.T, client *api.ClientWithResponses, intApp *internalapp.Application) {
				createContractWithServicePublicationGrant(t, client, intApp, uuid.MustParse("177543cd-ada2-47a9-9137-f42c6fef41dd"), "basisregister-fictieve-kentekens")
				createContractWithServicePublicationGrant(t, client, intApp, uuid.MustParse("177543cd-ada2-47a9-9137-f42c6fef41da"), "basisregister-fictieve-personen")
			},
			WantStatusCode: http.StatusOK,
			WantResponse: func() []models.FSCCoreServiceListing {
				serviceOne := models.FSCCoreServiceListing_Data{}

				_ = serviceOne.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
					Type: models.SERVICETYPESERVICE,
					Name: "basisregister-fictieve-kentekens",
					Peer: models.FSCCorePeer{
						Id:             peerA.GetPeerID(),
						ManagerAddress: peerA.ManagerAddress,
						Name:           peerA.GetName(),
					},
					Protocol: models.PROTOCOLTCPHTTP11,
				})

				serviceTwo := models.FSCCoreServiceListing_Data{}

				_ = serviceTwo.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
					Type: models.SERVICETYPESERVICE,
					Name: "basisregister-fictieve-personen",
					Peer: models.FSCCorePeer{
						Id:             peerA.GetPeerID(),
						ManagerAddress: peerA.ManagerAddress,
						Name:           peerA.GetName(),
					},
					Protocol: models.PROTOCOLTCPHTTP11,
				})

				return []models.FSCCoreServiceListing{
					{
						Data: serviceTwo,
					},
					{
						Data: serviceOne,
					},
				}
			}(),
		},
		"with_sort_order_desc": {
			Params: func() *models.GetServicesParams {
				sortOrder := models.FSCCoreSortOrderSORTORDERDESCENDING

				return &models.GetServicesParams{
					Cursor:      nil,
					Limit:       nil,
					SortOrder:   &sortOrder,
					PeerId:      nil,
					ServiceName: nil,
				}
			}(),
			Prepare: func(t *testing.T, client *api.ClientWithResponses, intApp *internalapp.Application) {
				createContractWithServicePublicationGrant(t, client, intApp, uuid.MustParse("177543cd-ada2-47a9-9137-f42c6fef41dd"), "basisregister-fictieve-kentekens")
				createContractWithServicePublicationGrant(t, client, intApp, uuid.MustParse("177543cd-ada2-47a9-9137-f42c6fef41da"), "basisregister-fictieve-personen")
			},
			WantStatusCode: http.StatusOK,
			WantResponse: func() []models.FSCCoreServiceListing {
				serviceOne := models.FSCCoreServiceListing_Data{}

				_ = serviceOne.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
					Type: models.SERVICETYPESERVICE,
					Name: "basisregister-fictieve-kentekens",
					Peer: models.FSCCorePeer{
						Id:             peerA.GetPeerID(),
						ManagerAddress: peerA.ManagerAddress,
						Name:           peerA.GetName(),
					},
					Protocol: models.PROTOCOLTCPHTTP11,
				})

				serviceTwo := models.FSCCoreServiceListing_Data{}

				_ = serviceTwo.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
					Type: models.SERVICETYPESERVICE,
					Name: "basisregister-fictieve-personen",
					Peer: models.FSCCorePeer{
						Id:             peerA.GetPeerID(),
						ManagerAddress: peerA.ManagerAddress,
						Name:           peerA.GetName(),
					},
					Protocol: models.PROTOCOLTCPHTTP11,
				})

				return []models.FSCCoreServiceListing{
					{
						Data: serviceOne,
					},
					{
						Data: serviceTwo,
					},
				}
			}(),
		},
		"happy_flow": {
			Params: func() *models.GetServicesParams {
				return &models.GetServicesParams{
					Cursor:      nil,
					Limit:       nil,
					SortOrder:   nil,
					PeerId:      nil,
					ServiceName: nil,
				}
			}(),
			Prepare: func(t *testing.T, client *api.ClientWithResponses, intApp *internalapp.Application) {
				createContractWithServicePublicationGrant(t, client, intApp, uuid.New(), "basisregister-fictieve-kentekens")
			},
			WantStatusCode: http.StatusOK,
			WantResponse: func() []models.FSCCoreServiceListing {
				data := models.FSCCoreServiceListing_Data{}

				_ = data.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
					Type: models.SERVICETYPESERVICE,
					Name: "basisregister-fictieve-kentekens",
					Peer: models.FSCCorePeer{
						Id:             peerA.GetPeerID(),
						ManagerAddress: peerA.ManagerAddress,
						Name:           peerA.GetName(),
					},
					Protocol: models.PROTOCOLTCPHTTP11,
				})

				return []models.FSCCoreServiceListing{
					{
						Data: data,
					},
				}
			}(),
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			// Arrange
			externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})

			t.Cleanup(func() {
				externalHTTPServer.Close()
			})

			client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
			assert.NoError(t, err)

			tc.Prepare(t, client, intApp)

			// Act
			res, err := client.GetServicesWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			// Assert
			if !assert.Equal(t, tc.WantStatusCode, res.StatusCode()) {
				t.Errorf("got status %d, want %d: %s", res.StatusCode(), tc.WantStatusCode, res.Body)
			}

			if tc.WantStatusCode == http.StatusOK {
				assert.Equal(t, tc.WantResponse, res.JSON200.Services)
			}
		})
	}
}

func createContractWithServicePublicationGrant(t *testing.T, client *api.ClientWithResponses, intApp *internalapp.Application, iv uuid.UUID, serviceName string) {
	ivAsBytes, err := iv.MarshalBinary()
	require.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            ivAsBytes,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerB.GetPeerID(),
						Name:           peerB.GetName(),
						ManagerAddress: peerB.ManagerAddress,
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           peerA.GetName(),
						ManagerAddress: peerA.ManagerAddress,
					},
					Name:     serviceName,
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})
	require.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().ServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().ServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().ServicePublicationGrants()[0].Service().Name(),
				ServiceProtocol: contractContent.Grants().ServicePublicationGrants()[0].Service().Protocol(),
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), time.Now().Truncate(time.Second))
	require.NoError(t, err)

	grantData := models.FSCCoreGrant_Data{}
	_ = grantData.FromFSCCoreGrantServicePublication(models.FSCCoreGrantServicePublication{
		Type: models.GRANTTYPESERVICEPUBLICATION,
		Directory: models.FSCCoreDirectory{
			PeerId: peerB.GetPeerID(),
		},
		Service: models.FSCCoreServicePublication{
			Name:     serviceName,
			PeerId:   peerA.GetPeerID(),
			Protocol: models.PROTOCOLTCPHTTP11,
		},
	})

	acceptContractResp, err := client.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: models.FSCCoreContractContent{
			CreatedAt: contractContent.CreatedAt().Unix(),
			Grants: []models.FSCCoreGrant{
				{
					Data: grantData,
				},
			},
			GroupId:       contractContent.GroupID(),
			HashAlgorithm: models.HASHALGORITHMSHA3512,
			Iv:            contractContent.IV().String(),
			Validity: models.FSCCoreValidity{
				NotAfter:  contractContent.NotAfter().Unix(),
				NotBefore: contractContent.NotBefore().Unix(),
			},
		},
		Signature: acceptSignatureOrgB.JWS(),
	})
	require.NoError(t, err)
	require.Equal(t, http.StatusCreated, acceptContractResp.StatusCode())
	require.Equal(t, "", string(acceptContractResp.Body))
}
