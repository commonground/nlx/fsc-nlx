// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"crypto/ecdsa"
	"crypto/rsa"
	"encoding/base64"
	"math/big"
	"net/http"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

// Manager-JSONWebKeySet-1
func TestGetJWKS(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	err = intApp.Commands.CreateCertificate.Handle(context.Background(), peerA.CertBundle.Cert().Certificate)
	assert.NoError(t, err)

	// Act
	res, err := client.GetJSONWebKeySetWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())

	assert.Len(t, res.JSON200.Keys, 1)

	assert.Equal(t, models.RS512, *res.JSON200.Keys[0].Alg)
	assert.Equal(t, models.RSA, res.JSON200.Keys[0].Kty)
	assert.Equal(t, peerA.CertBundle.CertificateThumbprint(), *res.JSON200.Keys[0].Kid)
	assert.Equal(t, peerA.CertBundle.CertificateThumbprint(), *res.JSON200.Keys[0].X5tS256)

	x5c := *res.JSON200.Keys[0].X5c

	for i, cert := range x5c {
		pubKeyPem := base64.StdEncoding.EncodeToString(peerA.CertBundle.Cert().Certificate[i])
		assert.Equal(t, pubKeyPem, cert)
	}

	// Assert RSA Public key specific fields
	jwkRsaPublicKey, err := res.JSON200.Keys[0].AsFSCCoreRsaPublicKey()
	assert.NoError(t, err)

	orgACertBundleRsaPublicKey := peerA.CertBundle.Certificate().PublicKey.(*rsa.PublicKey)
	assert.Equal(t, big.NewInt(int64(orgACertBundleRsaPublicKey.E)).Bytes(), *jwkRsaPublicKey.E)
	assert.Equal(t, orgACertBundleRsaPublicKey.N.Bytes(), *jwkRsaPublicKey.N)
}

func TestGetJWKSECCertificate(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), "org-nlx-test-a-ec")
	require.NoError(t, err)

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgCertBundle)
	require.NoError(t, err)

	err = intApp.Commands.CreateCertificate.Handle(context.Background(), orgCertBundle.Cert().Certificate)
	require.NoError(t, err)

	// Act
	res, err := client.GetJSONWebKeySetWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())

	assert.Len(t, res.JSON200.Keys, 1)

	assert.Equal(t, models.RS512, *res.JSON200.Keys[0].Alg)
	assert.Equal(t, models.EC, res.JSON200.Keys[0].Kty)
	assert.Equal(t, orgCertBundle.CertificateThumbprint(), *res.JSON200.Keys[0].Kid)
	assert.Equal(t, orgCertBundle.CertificateThumbprint(), *res.JSON200.Keys[0].X5tS256)

	x5c := *res.JSON200.Keys[0].X5c

	for i, cert := range x5c {
		pubKeyPem := base64.StdEncoding.EncodeToString(orgCertBundle.Cert().Certificate[i])
		assert.Equal(t, pubKeyPem, cert)
	}

	// Assert RSA Public key specific fields
	jwkECPublicKey, err := res.JSON200.Keys[0].AsFSCCoreEcPublicKey()
	assert.NoError(t, err)

	orgCertBundleECPublicKey := orgCertBundle.Certificate().PublicKey.(*ecdsa.PublicKey)
	assert.Equal(t, orgCertBundleECPublicKey.Y.Bytes(), *jwkECPublicKey.Y)
	assert.Equal(t, orgCertBundleECPublicKey.X.Bytes(), *jwkECPublicKey.X)
}
