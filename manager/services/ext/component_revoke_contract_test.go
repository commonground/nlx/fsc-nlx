// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	internal_models "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
	testsignature "gitlab.com/commonground/nlx/fsc-nlx/testing/signature"
)

//nolint:funlen // this is a test
func TestSuiteRevokeContact(t *testing.T) {
	testCases := map[string]struct {
		Description           string
		ContractToSignArgs    *command.CreateContractHandlerArgs
		RequestURLHash        string
		RevokeContractArgs    models.RevokeContractJSONRequestBody
		WantStatusCode        int
		WantResponseDomain    models.FSCCoreErrorDomain
		WantResponseErrorCode string
	}{
		"Manager-RevokeContract-1": {
			Description: "Place revoke signature on a Contract",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							Name:   "parkeerrechten",
							PeerID: peerB.GetPeerID(),
						},
					},
				},
				CreatedAt: testClock.Now().Truncate(time.Second), AuditLogSource: source,
				AuthData: authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RevokeContractArgs: func() models.RevokeContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				revokeSig, err := testsignature.CreateRevoke(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
					Type:   models.SERVICETYPESERVICE,
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RevokeContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: revokeSig,
				}
			}(),
			WantStatusCode:        http.StatusCreated,
			WantResponseDomain:    "",
			WantResponseErrorCode: "",
		},
		"Manager-RevokeContract-2": {
			Description: "Place accept signature on a Contract without being a Peer on the Contract",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							Name:   "parkeerrechten",
							PeerID: peerA.GetPeerID(),
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerA.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RevokeContractArgs: func() models.RevokeContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerA.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerA.GetPeerID(),
					Type:   models.SERVICETYPESERVICE,
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RevokeContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODEPEERNOTPARTOFCONTRACT),
		},
		"Manager-RevokeContract-3": {
			Description: "Place reject signature which is not a valid JWS",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							Name:   "parkeerrechten",
							PeerID: peerB.GetPeerID(),
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RevokeContractArgs: func() models.RevokeContractJSONRequestBody {
				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
					Type:   models.SERVICETYPESERVICE,
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RevokeContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: "not-a-valid-jws",
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODESIGNATUREVERIFICATIONFAILED),
		},
		"Manager-RevokeContract-4": {
			Description: "Place a revoke signature of which the content hash does not match the contract content you are revoking",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							Name:   "parkeerrechten",
							PeerID: peerB.GetPeerID(),
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RevokeContractArgs: func() models.RevokeContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "a-different-group-id",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				revokeSig, err := testsignature.CreateRevoke(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
					Type:   models.SERVICETYPESERVICE,
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RevokeContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: revokeSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODESIGNATURECONTRACTCONTENTHASHMISMATCH),
		},
		"Manager-RevokeContract-5": {
			Description: "Place reject signature on a Contract with a signature type that is not reject",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							Name:   "parkeerrechten",
							PeerID: peerB.GetPeerID(),
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RevokeContractArgs: func() models.RevokeContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
					Type:   models.SERVICETYPESERVICE,
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RevokeContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODESIGNATUREVERIFICATIONFAILED),
		},
		"Manager-RevokeContract-6": {
			Description: "The hash in the request URL does not match with the contract hash of the contract in the request body",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							Name:   "parkeerrechten",
							PeerID: peerB.GetPeerID(),
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20830",
					GroupID:        "fsc-local-different-group-id",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RevokeContractArgs: func() models.RevokeContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, peerB.CertBundle.CertificateThumbprint(), peerB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
					Type:   models.SERVICETYPESERVICE,
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RevokeContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODEURLPATHCONTENTHASHMISMATCH),
		},
		"Manager-RevokeContract-7": {
			Description: "Place revoke signature with an unsupported algorithm on a Contract",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				IV:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now().Truncate(time.Second),
				ContractNotAfter:  testClock.Now().Truncate(time.Second),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &command.NewServiceArgs{
							Name:   "parkeerrechten",
							PeerID: peerB.GetPeerID(),
						},
					},
				},
				CreatedAt:      testClock.Now().Truncate(time.Second),
				AuditLogSource: source,
				AuthData:       authData,
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			RevokeContractArgs: func() models.RevokeContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:              peerA.GetPeerID(),
							OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							ServicePeerID:             peerB.GetPeerID(),
							ServiceName:               "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				revokeSig, err := testsignature.CreateHMACRevoke(contentHash, peerB.CertBundle.CertificateThumbprint(), testClock.Now())
				assert.NoError(t, err)

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerB.GetPeerID(),
					Type:   models.SERVICETYPESERVICE,
				})

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerA.GetPeerID(),
					},
					Service: service,
				})

				return models.RevokeContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: revokeSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: "ERROR_CODE_UNKNOWN_ALGORITHM_SIGNATURE",
		},
	}

	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})

	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	for name, testCase := range testCases {
		tc := testCase

		t.Run(name, func(t *testing.T) {
			if tc.ContractToSignArgs != nil {
				_, err := intApp.Commands.CreateContract.Handle(context.Background(), tc.ContractToSignArgs)
				assert.NoError(t, err)
			}

			resp, errSubmit := client.RevokeContractWithResponse(context.Background(), tc.RequestURLHash, &models.RevokeContractParams{
				FscManagerAddress: peerB.ManagerAddress,
			}, tc.RevokeContractArgs)
			assert.NoError(t, errSubmit)

			if !assert.Equal(t, testCase.WantStatusCode, resp.StatusCode()) {
				t.Errorf("response body: %s", resp.Body)
			}

			if testCase.WantResponseDomain != "" || testCase.WantResponseErrorCode != "" {
				switch testCase.WantStatusCode {
				case http.StatusBadRequest:
					assert.Equal(t, testCase.WantResponseErrorCode, resp.JSON400.Code)
					assert.Equal(t, testCase.WantResponseDomain, resp.JSON400.Domain)

				case http.StatusUnprocessableEntity:
					assert.Equal(t, testCase.WantResponseErrorCode, resp.JSON422.Code)
					assert.Equal(t, testCase.WantResponseDomain, resp.JSON422.Domain)

				default:
					t.Fatalf("unexpected status code %d", resp.StatusCode())
				}
			}

			if testCase.WantStatusCode != http.StatusCreated {
				return
			}

			var limit = models.FSCCoreQueryPaginationLimit(20)

			contracts, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
				Limit: &limit,
			})
			assert.NoError(t, err)

			var rejectedContract models.FSCCoreContract

			for _, c := range contracts.JSON200.Contracts {
				if c.Content.Iv != testCase.RevokeContractArgs.ContractContent.Iv {
					continue
				}

				rejectedContract = c
			}

			assert.NotNil(t, rejectedContract)
			assert.Contains(t, rejectedContract.Signatures.Revoke, peerB.GetPeerID())
			assert.Equal(t, testCase.RevokeContractArgs.Signature, rejectedContract.Signatures.Revoke[peerB.GetPeerID()])
		})
	}
}
