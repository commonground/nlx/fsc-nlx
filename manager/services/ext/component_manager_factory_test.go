// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

//nolint:unused // most methods are unused but added to comply with the Manager interface
package externalservice_test

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type testManagerFactory struct {
	Clock clock.Clock
}

type testManager struct {
	address string
	orgCert *tls.CertificateBundle
	clock   clock.Clock
}

func (f *testManagerFactory) New(_, address string) (manager.Manager, error) {
	if address == peerA.ManagerAddress {
		return &testManager{
			address: address,
			orgCert: peerA.CertBundle,
			clock:   f.Clock,
		}, nil
	}

	if address == peerB.ManagerAddress {
		return &testManager{
			address: address,
			orgCert: peerB.CertBundle,
			clock:   f.Clock,
		}, nil
	}

	if address == peerC.ManagerAddress {
		return &testManager{
			address: address,
			orgCert: peerC.CertBundle,
			clock:   f.Clock,
		}, nil
	}

	if address == peerDirectory.ManagerAddress {
		return &testManager{
			address: address,
			orgCert: peerDirectory.CertBundle,
			clock:   f.Clock,
		}, nil
	}

	return nil, fmt.Errorf("unknown address '%s', unable to setup manager client", address)
}

func (m *testManager) GetCertificates(_ context.Context) (*contract.PeerCertificates, error) {
	peerCert, err := contract.NewPeerCertFromCertificate(m.clock, m.orgCert.RootCAs(), m.orgCert.Cert().Certificate)
	if err != nil {
		log.Printf("failed to create peer cert: %v", err)
	}

	peerCerts := &contract.PeerCertificates{
		peerCert.CertificateThumbprint(): peerCert,
	}

	return peerCerts, nil
}

func (m *testManager) GetPeers(_ context.Context, _ contract.PeersIDs) (contract.Peers, error) {
	orgAPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerA.GetPeerID(),
		Name:           peerA.GetName(),
		ManagerAddress: peerA.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to crate new test peer %s", err)
	}

	orgBPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerB.GetPeerID(),
		Name:           peerB.GetName(),
		ManagerAddress: peerB.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to crate new test peer %s", err)
	}

	orgCPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerC.GetPeerID(),
		Name:           peerC.GetName(),
		ManagerAddress: peerC.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	orgDirectoryPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerDirectory.GetPeerID(),
		Name:           peerDirectory.GetName(),
		ManagerAddress: peerDirectory.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	return contract.Peers{
		orgAPeer.ID():         orgAPeer,
		orgBPeer.ID():         orgBPeer,
		orgCPeer.ID():         orgCPeer,
		orgDirectoryPeer.ID(): orgDirectoryPeer,
	}, nil
}

func (m *testManager) Announce(_ context.Context) error {
	return nil
}

func (m *testManager) GetContracts(_ context.Context, _ *contract.GrantType, _ *[]string) ([]*contract.Contract, error) {
	panic(fmt.Sprintf("unintended call to get contracts %s", m.address))
}

func (m *testManager) SubmitContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *testManager) AcceptContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *testManager) RejectContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	panic(fmt.Sprintf("unintended call to reject contract %s", m.address))
}

func (m *testManager) RevokeContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	panic(fmt.Sprintf("unintended call to revoke contract %s", m.address))
}

func (m *testManager) GetPeerInfo(_ context.Context) (*contract.Peer, error) {
	panic(fmt.Sprintf("unintended call to get peer info %s", m.address))
}

func (m *testManager) GetServices(_ context.Context, _ *contract.PeerID, _ string) ([]*contract.Service, error) {
	panic(fmt.Sprintf("unintended call to get services %s", m.address))
}

func (m *testManager) GetToken(_ context.Context, _ contract.PeerID, _ string) (string, error) {
	panic(fmt.Sprintf("unintended call to get token %s", m.address))
}

func (m *testManager) GetTXLogRecords(_ context.Context) (contract.TXLogRecords, error) {
	panic(fmt.Sprintf("unintended call to get txlog records %s", m.address))
}
