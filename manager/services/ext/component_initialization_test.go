// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

// the Manager should know its own Peer info
func TestInitialization(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	// Act
	getPeersRes, err := client.GetPeersWithResponse(context.Background(), &models.GetPeersParams{
		Cursor:    nil,
		Limit:     nil,
		SortOrder: nil,
		PeerName:  nil,
		PeerId: &[]models.FSCCorePeerID{
			peerA.GetPeerID(),
		},
	})
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, []models.FSCCorePeer{
		{
			Id:             peerA.GetPeerID(),
			ManagerAddress: peerA.ManagerAddress,
			Name:           peerA.GetName(),
		},
	}, getPeersRes.JSON200.Peers)
}
