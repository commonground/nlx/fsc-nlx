// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

// testcase: Manager-Peers-1
func TestGetPeers(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	clientOrgC, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerC.CertBundle)
	assert.NoError(t, err)

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	respAnnounce, err := clientOrgC.AnnounceWithResponse(context.Background(), &models.AnnounceParams{
		FscManagerAddress: peerC.ManagerAddress,
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respAnnounce.StatusCode())

	respAnnounce, err = clientOrgB.AnnounceWithResponse(context.Background(), &models.AnnounceParams{
		FscManagerAddress: peerB.ManagerAddress,
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respAnnounce.StatusCode())

	limit := models.FSCCoreQueryPaginationLimit(10)
	sortOrder := models.FSCCoreSortOrderSORTORDERASCENDING
	peerName := peerB.GetName()

	tests := map[string]struct {
		GetPeersParams *models.GetPeersParams
		ExpectedPeers  []models.FSCCorePeer
	}{
		"no_filters": {
			GetPeersParams: &models.GetPeersParams{
				Cursor:    nil,
				Limit:     &limit,
				SortOrder: &sortOrder,
			},
			ExpectedPeers: []models.FSCCorePeer{
				{
					Id:             peerA.GetPeerID(),
					ManagerAddress: peerA.ManagerAddress,
					Name:           peerA.GetName(),
				}, {
					Id:             peerB.GetPeerID(),
					ManagerAddress: peerB.ManagerAddress,
					Name:           peerB.GetName(),
				}, {
					Id:             peerC.GetPeerID(),
					ManagerAddress: peerC.ManagerAddress,
					Name:           peerC.GetName(),
				}, {
					Id:             peerDirectory.GetPeerID(),
					ManagerAddress: peerDirectory.ManagerAddress,
					Name:           peerDirectory.GetName(),
				},
			},
		},
		"peer_id_filter": {
			GetPeersParams: &models.GetPeersParams{
				Cursor:    nil,
				Limit:     &limit,
				SortOrder: &sortOrder,
				PeerId: &[]models.FSCCorePeerID{
					peerC.GetPeerID(),
				},
			},
			ExpectedPeers: []models.FSCCorePeer{
				{
					Id:             peerC.GetPeerID(),
					ManagerAddress: peerC.ManagerAddress,
					Name:           peerC.GetName(),
				},
			},
		},
		"peer_name_filter": {
			GetPeersParams: &models.GetPeersParams{
				Cursor:    nil,
				Limit:     &limit,
				SortOrder: &sortOrder,
				PeerName:  &peerName,
			},
			ExpectedPeers: []models.FSCCorePeer{
				{
					Id:             peerB.GetPeerID(),
					ManagerAddress: peerB.ManagerAddress,
					Name:           peerB.GetName(),
				},
			},
		},
		"default_sorting": {
			GetPeersParams: &models.GetPeersParams{
				Cursor:    nil,
				Limit:     &limit,
				SortOrder: nil,
			},
			ExpectedPeers: []models.FSCCorePeer{
				{
					Id:             peerDirectory.GetPeerID(),
					ManagerAddress: peerDirectory.ManagerAddress,
					Name:           peerDirectory.GetName(),
				},
				{
					Id:             peerC.GetPeerID(),
					ManagerAddress: peerC.ManagerAddress,
					Name:           peerC.GetName(),
				},
				{
					Id:             peerB.GetPeerID(),
					ManagerAddress: peerB.ManagerAddress,
					Name:           peerB.GetName(),
				},
				{
					Id:             peerA.GetPeerID(),
					ManagerAddress: peerA.ManagerAddress,
					Name:           peerA.GetName(),
				},
			},
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			// Act
			respPeers, errReq := clientOrgC.GetPeersWithResponse(context.Background(), test.GetPeersParams)

			// Assert
			assert.NoError(t, errReq)
			assert.Equal(t, http.StatusOK, respPeers.StatusCode())
			assert.Equal(t, test.ExpectedPeers, respPeers.JSON200.Peers)
		})
	}
}
