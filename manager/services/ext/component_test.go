// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"crypto/x509"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"net/netip"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"
	"time"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner/neverallow"
	restport "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	externalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/ext"
	internalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/int"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var nowInUTC = time.Now().UTC()

var (
	testClock      = testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	accessTokenTTL = time.Minute
	peerA          *peerInfo
	peerB          *peerInfo
	peerC          *peerInfo
	peerDirectory  *peerInfo
	peerOnCRL      *peerInfo

	authData = &authentication.CertificateData{
		SubjectCommonName:   "test-subject",
		PublicKeyThumbprint: "test-fingerprint",
	}

	source = common_auditlog.SourceHTTP{
		IPAddress: netip.AddrFrom4([4]byte{127, 0, 0, 1}),
		UserAgent: "test-useragent",
	}
)

func TestMain(m *testing.M) {
	var err error

	peerA, err = newPeerInfo(testingutils.NLXTestPeerA, "https://manager.org-a.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerB, err = newPeerInfo(testingutils.NLXTestPeerB, "https://manager.org-b.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerC, err = newPeerInfo(testingutils.NLXTestPeerC, "https://manager.org-c.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerDirectory, err = newPeerInfo(testingutils.NLXTestOrgDirectory, "https://directory.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerOnCRL, err = newPeerInfo(testingutils.OrgOnCRL, "https://manager.org-on-crl.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

type peerInfo struct {
	CertBundle     *tls.CertificateBundle
	ManagerAddress string
}

func newPeerInfo(organisationName testingutils.CertificateBundlePeerName, managerAddress string) (*peerInfo, error) {
	peerCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "../", "testing", "pki"), organisationName)
	if err != nil {
		return nil, err
	}

	return &peerInfo{
		CertBundle:     peerCertBundle,
		ManagerAddress: managerAddress,
	}, nil
}

func (o *peerInfo) GetName() string {
	return o.CertBundle.GetPeerInfo().Name
}

func (o *peerInfo) GetPeerID() string {
	return o.CertBundle.GetPeerInfo().SerialNumber
}

type newServiceArgs struct {
	AutoSigner autosigner.AutoSigner
}

//nolint:funlen,gocyclo // this is a test
func newService(testName string, args *newServiceArgs) (*httptest.Server, *internalapp.Application) {
	if args == nil {
		log.Fatalf("args is required")
	}

	logger, err := zaplogger.New("info", "live")
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	logger = logger.With("test", testName)

	orgACertBundle, err := testingutils.GetCertificateBundle(path.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestPeerA)
	if err != nil {
		log.Fatalf("failed to load organization a cert bundle: %v", err)
	}

	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)
	dbName = strings.Replace(dbName, "test", "", 1) // strip 'test'-prefix
	dbName = fmt.Sprintf("ext_%s", dbName)
	dbName = strings.ReplaceAll(dbName, "/", "_")

	// via https://stackoverflow.com/a/27865772/363448
	const maxLengthDatabaseName = 63

	if len(dbName) > maxLengthDatabaseName {
		log.Fatalf("database name too long (%d > 63 characters): %q", len(dbName), dbName)
	}

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		log.Fatalf("failed to setup test database: %v", err)
	}

	db, err := postgresadapter.NewConnection(context.Background(), testDB)
	if err != nil {
		log.Fatalf("failed to create db connection: %v", err)
	}

	err = postgresadapter.PerformMigrations(testDB)
	if err != nil {
		log.Fatalf("failed to perform dbmigrations: %v", err)
	}

	postgresqlRepository, err := postgresadapter.New(orgACertBundle.RootCAs(), db, testClock)
	if err != nil {
		log.Fatalf("failed to setup postgresql database: %s", err)
	}

	controllerRepository := newFakeController()

	selfPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             orgACertBundle.GetPeerInfo().SerialNumber,
		Name:           orgACertBundle.GetPeerInfo().Name,
		ManagerAddress: "https://manager.org-a.nlx.local:443",
	})
	if err != nil {
		log.Fatalf("failed to create self peer: %v", err)
	}

	managerFactory := testManagerFactory{
		Clock: testClock,
	}

	crlFileDer, err := os.ReadFile(filepath.Join("..", "..", "..", "testing", "pki", "ca.crl"))
	if err != nil {
		log.Fatal(err)
	}

	crlFile, err := x509.ParseRevocationList(crlFileDer)
	if err != nil {
		log.Fatal(err)
	}

	crlCache, err := tls.NewCRL(nil)
	if err != nil {
		log.Fatal("cannot create CRLsCache Cache")
	}

	err = crlCache.AddRevocationList("http://localhost", crlFile)
	if err != nil {
		log.Fatal("cannot add CRL to cache")
	}

	contractDistributionService, err := services.NewContractDistributionService(&services.NewContractDistributionServiceArgs{
		Context:                              context.Background(),
		Logger:                               logger,
		Clock:                                testClock,
		FailedContractDistributionRepository: postgresqlRepository,
	})
	if err != nil {
		log.Fatalf("failed to setup contract distribution service: %s", err)
	}

	peersCommunicationService, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
		Ctx:                         context.Background(),
		Logger:                      logger,
		LocalRepo:                   postgresqlRepository,
		ManagerFactory:              &managerFactory,
		SelfPeerID:                  contract.PeerID(peerA.GetPeerID()),
		ContractDistributionService: contractDistributionService,
		Clock:                       testClock,
	})
	if err != nil {
		log.Fatalf("failed to setup peers communication service: %s", err)
	}

	txlogRepository := newFakeTxLog()

	autoSigner := neverallow.New()
	if args.AutoSigner != nil {
		autoSigner = args.AutoSigner
	}

	app, err := externalservice.NewApplication(&externalservice.NewApplicationArgs{
		Context:                   context.Background(),
		Clock:                     testClock,
		Logger:                    logger,
		Repository:                postgresqlRepository,
		ControllerRepository:      controllerRepository,
		TXLogRepository:           txlogRepository,
		PeersCommunicationService: peersCommunicationService,
		GroupID:                   "fsc-local",
		SelfPeer:                  selfPeer,
		TrustedRootCAs:            orgACertBundle.RootCAs(),
		AutoSignCertificate:       orgACertBundle.Cert(),
		TokenSignCertificate:      orgACertBundle.Cert(),
		TokenTTL:                  accessTokenTTL,
		IsTxlogDisabled:           false,
		AutoSigner:                autoSigner,
	})
	if err != nil {
		log.Fatalf("failed to setup application: %s", err)
	}

	server, err := restport.New(&restport.NewArgs{
		Logger:      logger,
		App:         app,
		Cert:        orgACertBundle,
		SelfAddress: peerA.ManagerAddress,
		CRLCache:    crlCache,
	})
	if err != nil {
		log.Fatalf("failed to setup rest port: %s", err)
	}

	auditlogger, err := auditlog.New(&auditlog.NewArgs{
		AuditLogger: newFakeAuditlog(),
		GroupID:     "fsc-local",
		Clock:       testClock,
		IDGenerator: uuidgenerator.New(),
	})
	if err != nil {
		log.Fatalf("failed to create auditlogger: %v", err)
	}

	intApp, err := internalservice.NewApplication(&internalservice.NewApplicationArgs{
		Context:                     context.Background(),
		Clock:                       testClock,
		Logger:                      logger,
		Repository:                  postgresqlRepository,
		PeersCommunicationService:   peersCommunicationService,
		ContractDistributionService: contractDistributionService,
		TXLog:                       txlogRepository,
		SelfPeer:                    selfPeer,
		TrustedExternalRootCAs:      orgACertBundle.RootCAs(),
		SignatureCertificate:        orgACertBundle.Cert(),
		ControllerRepository:        controllerRepository,
		ManagerFactory:              &managerFactory,
		DirectoryPeerID:             peerDirectory.GetPeerID(),
		DirectoryPeerManagerAddress: peerDirectory.ManagerAddress,
		DirectoryPeerName:           peerDirectory.GetName(),
		Auditlogger:                 auditlogger,
		InternalCertificate:         orgACertBundle,
	})
	if err != nil {
		log.Fatalf("failed to setup internal app: %s", err)
	}

	srv := httptest.NewUnstartedServer(server.Handler())
	srv.TLS = orgACertBundle.TLSConfig(orgACertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv, intApp
}

func createExternalManagerAPIClient(managerURL string, certBundle *tls.CertificateBundle) (*api.ClientWithResponses, error) {
	return api.NewClientWithResponses(managerURL, func(c *api.Client) error {
		t := &http.Transport{
			TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
		}
		c.Client = &http.Client{
			Transport: t,
		}

		return nil
	})
}
