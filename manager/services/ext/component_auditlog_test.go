// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
)

type FakeAuditlog struct{}

func newFakeAuditlog() *FakeAuditlog {
	return &FakeAuditlog{}
}

func (c *FakeAuditlog) CreateRecords(_ context.Context, _ auditlog.Records) error {
	return nil
}

func (c *FakeAuditlog) ListRecords(_ context.Context, _ *auditlog.ListRecordsArgs) (auditlog.Records, error) {
	return nil, nil
}
