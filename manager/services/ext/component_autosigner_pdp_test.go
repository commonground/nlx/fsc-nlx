// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager/rest"
	query_int "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner/pdp"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

// nolint:funlen,dupl // this is a test
func TestAutosignerPDP(t *testing.T) {
	t.Parallel()

	l, err := zaplogger.New("info", "local")
	require.NoError(t, err)

	testCases := map[string]struct {
		PDPServer            *httptest.Server
		ContractContent      *contract.Content
		WantAcceptSignatures contract.PeersIDs
	}{
		"pdp_that_always_allows": {
			PDPServer: createPDPServerWhichAlwaysAllows(t),
			ContractContent: func() *contract.Content {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     time.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: time.Now().Truncate(time.Second),
						NotAfter:  time.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerB.GetPeerID(),
								},
								PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantServiceConnectionServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerA.GetPeerID(),
								},
								Name: "parkeerrechten",
							},
						},
					},
				})
				assert.NoError(t, err)

				return contractContent
			}(),
			WantAcceptSignatures: contract.PeersIDs{
				contract.PeerID(peerA.GetPeerID()): true,
				contract.PeerID(peerB.GetPeerID()): true,
			},
		},
		"pdp_that_never_allows": {
			PDPServer: createPDPServerWhichNeverAllows(t),
			ContractContent: func() *contract.Content {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     time.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: time.Now().Truncate(time.Second),
						NotAfter:  time.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServicePublicationArgs{
							Directory: &contract.NewGrantServicePublicationDirectoryArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerA.GetPeerID(),
								},
							},
							Service: &contract.NewGrantServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerB.GetPeerID(),
								},
								Name:     "parkeerrechten",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
					},
				})
				assert.NoError(t, err)

				return contractContent
			}(),
			WantAcceptSignatures: contract.PeersIDs{
				contract.PeerID(peerB.GetPeerID()): true,
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			// Arrange
			tc.PDPServer.Start()

			defer tc.PDPServer.Close()

			autosigner, err := pdp.New(tc.PDPServer.URL, l, *tc.PDPServer.Client())
			require.NoError(t, err)

			externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{
				AutoSigner: autosigner,
			})

			defer externalHTTPServer.Close()

			client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
			assert.NoError(t, err)

			acceptSignature, err := tc.ContractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), time.Now().Truncate(time.Second))
			assert.NoError(t, err)

			cc, err := rest.ContractContentToAPIModel(tc.ContractContent)
			assert.NoError(t, err)

			// Act
			res, err := client.SubmitContractWithResponse(context.Background(), &models.SubmitContractParams{
				FscManagerAddress: peerB.ManagerAddress,
			}, models.SubmitContractJSONRequestBody(
				models.SubmitContractJSONBody{
					ContractContent: *cc,
					Signature:       acceptSignature.JWS(),
				}),
			)
			assert.NoError(t, err)

			if !assert.Equal(t, http.StatusCreated, res.StatusCode()) {
				t.Errorf("unexpected submit contract status code: %s", res.Body)
			}

			contracts, err := intApp.Queries.GetContracts.Handle(context.Background(), &query_int.ListContractsHandlerArgs{
				ContentHashes: []string{tc.ContractContent.Hash().String()},
			})
			require.NoError(t, err)

			// Assert
			require.Len(t, contracts, 1)

			assert.Len(t, contracts[0].SignaturesAccepted().Value(), len(tc.WantAcceptSignatures))

			for _, peerID := range tc.WantAcceptSignatures.Value() {
				require.True(t, contracts[0].IsAcceptedBy(contract.PeerID(peerID)))
			}
		})
	}
}

func createPDPServerWhichAlwaysAllows(t *testing.T) *httptest.Server {
	return createPDPServer(t, true)
}

func createPDPServerWhichNeverAllows(t *testing.T) *httptest.Server {
	return createPDPServer(t, false)
}

func createPDPServer(t *testing.T, allows bool) *httptest.Server {
	type result struct {
		Allowed bool `json:"allowed"`
		Status  *struct {
			Reason string `json:"reason"`
		} `json:"status,omitempty"`
	}

	type response struct {
		Result *result `json:"result"`
	}

	pdpServer := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		res, err := json.Marshal(response{
			Result: &result{
				Allowed: allows,
			},
		})
		require.NoError(t, err)

		w.Header().Set("Content-Type", "application/json")

		_, err = w.Write(res)
		require.NoError(t, err)
	}))

	return pdpServer
}
