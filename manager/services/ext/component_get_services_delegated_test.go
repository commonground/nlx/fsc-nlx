// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	internal_models "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// 1. Create the contract as domain model
// 2. Create that contract for Organization A using the internal app
// 3. Create signatures for the contract using the domain model for Organization B and C
// 4. Organization B sends the Accept Contract request to Organization A
// 5. Organization C sends the Accept Contract request to Organization A
// 6. Fetch the services 'parkeerrechten' at Organization A and expect the created service to be present in the list

func TestGetServicesDelegated(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	clientOrgC, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerC.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Add(time.Hour).Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerB.GetPeerID(),
						Name:           peerB.GetName(),
						ManagerAddress: peerB.ManagerAddress,
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           peerA.GetName(),
						ManagerAddress: peerA.ManagerAddress,
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerC.GetPeerID(),
						Name:           peerC.GetName(),
						ManagerAddress: peerC.ManagerAddress,
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Delegator().Peer().ID().Value(),
				DirectoryPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Name(),
				ServiceProtocol: contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Protocol(),
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), time.Now().Truncate(time.Second))
	assert.NoError(t, err)

	acceptSignatureOrgC, err := contractContent.Accept(peerC.CertBundle.RootCAs(), peerC.CertBundle.Cert(), time.Now().Truncate(time.Second))
	assert.NoError(t, err)

	grantData := models.FSCCoreGrant_Data{}
	_ = grantData.FromFSCCoreGrantDelegatedServicePublication(models.FSCCoreGrantDelegatedServicePublication{
		Type: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
		Delegator: models.FSCCoreDelegator{
			PeerId: peerC.GetPeerID(),
		},
		Directory: models.FSCCoreDirectory{
			PeerId: peerB.GetPeerID(),
		},
		Service: models.FSCCoreServicePublication{
			Name:     "parkeerrechten",
			PeerId:   peerA.GetPeerID(),
			Protocol: models.PROTOCOLTCPHTTP11,
		},
	})

	orgBAcceptContractRes, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: models.FSCCoreContractContent{
			CreatedAt: contractContent.CreatedAt().Unix(),
			Grants: []models.FSCCoreGrant{
				{
					Data: grantData,
				},
			},
			GroupId:       contractContent.GroupID(),
			HashAlgorithm: models.HASHALGORITHMSHA3512,
			Iv:            contractContent.IV().String(),
			Validity: models.FSCCoreValidity{
				NotAfter:  contractContent.NotAfter().Unix(),
				NotBefore: contractContent.NotBefore().Unix(),
			},
		},
		Signature: acceptSignatureOrgB.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, orgBAcceptContractRes.StatusCode())
	assert.Equal(t, "", string(orgBAcceptContractRes.Body))

	orgCAcceptContractRes, err := clientOrgC.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerC.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: models.FSCCoreContractContent{
			CreatedAt: contractContent.CreatedAt().Unix(),
			Grants: []models.FSCCoreGrant{
				{
					Data: grantData,
				},
			},
			GroupId:       contractContent.GroupID(),
			HashAlgorithm: models.HASHALGORITHMSHA3512,
			Iv:            contractContent.IV().String(),
			Validity: models.FSCCoreValidity{
				NotAfter:  contractContent.NotAfter().Unix(),
				NotBefore: contractContent.NotBefore().Unix(),
			},
		},
		Signature: acceptSignatureOrgC.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, orgCAcceptContractRes.StatusCode())
	assert.Equal(t, "", string(orgCAcceptContractRes.Body))

	// Act
	peerID := peerA.GetPeerID()
	serviceName := "parkeerrechten"

	res, err := clientOrgB.GetServicesWithResponse(context.Background(), &models.GetServicesParams{
		Cursor:      nil,
		Limit:       nil,
		SortOrder:   nil,
		PeerId:      &peerID,
		ServiceName: &serviceName,
	})
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())

	data := models.FSCCoreServiceListing_Data{}

	protocol := models.PROTOCOLTCPHTTP11

	_ = data.FromFSCCoreServiceListingDelegatedService(models.FSCCoreServiceListingDelegatedService{
		Type: models.SERVICETYPEDELEGATEDSERVICE,
		Delegator: models.FSCCoreDelegatorServiceListing{
			PeerId:   peerC.GetPeerID(),
			PeerName: peerC.GetName(),
		},
		Name: "parkeerrechten",
		Peer: models.FSCCorePeer{
			Id:             peerA.GetPeerID(),
			ManagerAddress: peerA.ManagerAddress,
			Name:           peerA.GetName(),
		},
		Protocol: protocol,
	})

	expectedServices := []models.FSCCoreServiceListing{
		{
			Data: data,
		},
	}

	assert.Len(t, res.JSON200.Services, len(expectedServices))

	for i, expectedService := range expectedServices {
		serviceData, err := expectedService.Data.AsFSCCoreServiceListingDelegatedService()
		assert.NoError(t, err)

		a, err := res.JSON200.Services[i].Data.AsFSCCoreServiceListingDelegatedService()
		assert.NoError(t, err)

		assert.Equalf(t, serviceData, a, "grant number %d does not match", i+1)
	}
}
