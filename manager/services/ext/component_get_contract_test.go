// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	internal_models "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// Manager-Contracts-1
//
//nolint:funlen // this is a test
func TestGetContractServiceConnectionGrant(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	service, ok := contractContent.Grants().ServiceConnectionGrants()[0].Service().(*contract.GrantServiceConnectionService)
	require.True(t, ok)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   service.Name(),
					PeerID: service.Peer().ID().Value(),
				},
			},
		},
		CreatedAt:      contractContent.CreatedAt().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	// Act
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantHash: &[]string{contractContent.Grants().ServiceConnectionGrants()[0].Hash().String()},
	})

	// Assert
	// assert all field in the contract, see submit_contract test for asserting 1 field
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.IV().String(), contractResponse.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	// Asserting grants
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	serviceConnectionGrantType, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.Discriminator()
	assert.NoError(t, err)
	assert.Equal(t, "GRANT_TYPE_SERVICE_CONNECTION", serviceConnectionGrantType)

	serviceConnectionGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantServiceConnection()
	assert.NoError(t, err)

	grantService, err := serviceConnectionGrant.Service.AsFSCCoreService()
	assert.NoError(t, err)

	assert.Equal(t, peerB.GetPeerID(), serviceConnectionGrant.Outway.PeerId)
	assert.Equal(t, peerB.CertBundle.PublicKeyThumbprint(), serviceConnectionGrant.Outway.PublicKeyThumbprint)
	assert.Equal(t, peerA.GetPeerID(), grantService.PeerId)
	assert.Equal(t, service.Name(), grantService.Name)

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))
}

// Manager-Contracts-2
//
//nolint:funlen,dupl // this is a test
func TestGetContractWithPeerNotInContract(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerC.CertBundle) // client with peer not part of contract
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	service, ok := contractContent.Grants().ServiceConnectionGrants()[0].Service().(*contract.GrantServiceConnectionService)
	require.True(t, ok)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   service.Name(),
					PeerID: service.Peer().ID().Value(),
				},
			},
		},
		CreatedAt:      time.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	// Act
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{})

	// Assert
	// assert that no contracts are returned since the peer requesting the contracts is not part of the contract
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts))
}

//nolint:funlen // this is a test
func TestGetContractMultipleGrants(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           "test-directory",
						ManagerAddress: "https://test-directory.example.com:443",
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Name: "test-service",
					Peer: &contract.NewPeerArgs{
						ID:   peerA.GetPeerID(),
						Name: peerA.GetName(),
					},
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:   peerB.GetPeerID(),
						Name: peerB.GetName(),
					},
				},
			},
			&contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:   peerC.GetPeerID(),
						Name: peerC.GetName(),
					},
					PublicKeyThumbprint: peerC.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:   peerA.GetPeerID(),
						Name: peerA.GetName(),
					},
					Name: "test-service",
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:   peerB.GetPeerID(),
						Name: peerB.GetName(),
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	service, ok := contractContent.Grants().DelegatedServiceConnectionGrants()[0].Service().(*contract.GrantDelegatedServiceConnectionService)
	require.True(t, ok)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Name(),
				ServicePeerID:   contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceProtocol: contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Protocol(),
				DelegatorPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Delegator().Peer().ID().Value(),
			},
			&command.GrantDelegatedServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					PeerID: service.Peer().ID().Value(),
					Name:   service.Name(),
				},
				DelegatorPeerID: contractContent.Grants().DelegatedServiceConnectionGrants()[0].Delegator().Peer().ID().Value(),
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	// Act
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{})

	// Assert
	// assert all field in the contract, see submit_contract test for asserting 1 field
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.IV().String(), contractResponse.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	// Asserting grants
	assert.Equal(t, 2, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	delegatedServiceConnectionGrantType, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.Discriminator()
	assert.NoError(t, err)
	assert.Equal(t, "GRANT_TYPE_DELEGATED_SERVICE_CONNECTION", delegatedServiceConnectionGrantType)

	delegatedServicePublicationGrantType, err := contractResponse.JSON200.Contracts[0].Content.Grants[1].Data.Discriminator()
	assert.NoError(t, err)
	assert.Equal(t, "GRANT_TYPE_DELEGATED_SERVICE_PUBLICATION", delegatedServicePublicationGrantType)

	delegatedServiceConnectionGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantDelegatedServiceConnection()
	assert.NoError(t, err)

	grantService, err := delegatedServiceConnectionGrant.Service.AsFSCCoreService()
	assert.NoError(t, err)

	assert.Equal(t, peerA.GetPeerID(), grantService.PeerId)
	assert.Equal(t, service.Name(), grantService.Name)
	assert.Equal(t, contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().Peer().ID().Value(), delegatedServiceConnectionGrant.Outway.PeerId)
	assert.Equal(t, contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(), delegatedServiceConnectionGrant.Outway.PublicKeyThumbprint)
	assert.Equal(t, contractContent.Grants().DelegatedServiceConnectionGrants()[0].Delegator().Peer().ID().Value(), delegatedServiceConnectionGrant.Delegator.PeerId)

	serviceDelegatedPublicationGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[1].Data.AsFSCCoreGrantDelegatedServicePublication()
	assert.NoError(t, err)

	assert.Equal(t, peerB.GetPeerID(), serviceDelegatedPublicationGrant.Delegator.PeerId)
	assert.Equal(t, peerA.GetPeerID(), serviceDelegatedPublicationGrant.Service.PeerId)
	assert.Equal(t, contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Name(), serviceDelegatedPublicationGrant.Service.Name)
	assert.Equal(t, peerA.GetPeerID(), serviceDelegatedPublicationGrant.Directory.PeerId)

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))

}

//nolint:funlen // this is a test
func TestGetContractWithGrantType_ServicePublication(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerB.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           peerA.GetName(),
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().ServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().ServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().ServicePublicationGrants()[0].Service().Name(),
				ServiceProtocol: contractContent.Grants().ServicePublicationGrants()[0].Service().Protocol(),
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	// Act
	grantType := models.FSCCoreGrantType("GRANT_TYPE_SERVICE_PUBLICATION")
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantType: &grantType,
	})

	// Assert
	// assert all field in the contract, see submit_contract test for asserting 1 field
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.IV().String(), contractResponse.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	// Asserting grants
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	assert.NoError(t, err)

	servicePublicationGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantServicePublication()
	assert.NoError(t, err)

	assert.Equal(t, contractContent.Grants().ServicePublicationGrants()[0].Directory().Peer().ID().Value(), servicePublicationGrant.Directory.PeerId)
	assert.Equal(t, contractContent.Grants().ServicePublicationGrants()[0].Service().Name(), servicePublicationGrant.Service.Name)
	assert.Equal(t, contractContent.Grants().ServicePublicationGrants()[0].Service().Peer().ID().Value(), servicePublicationGrant.Service.PeerId)
	assert.Equal(t, contractContent.Grants().ServicePublicationGrants()[0].Service().Protocol().String(), string(servicePublicationGrant.Service.Protocol))

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))
}

//nolint:funlen,dupl // this is a test
func TestGetContractWithGrantType_ServiceConnection(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           peerA.GetName(),
						ManagerAddress: "",
					},
					PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:   peerB.GetPeerID(),
						Name: peerB.GetName(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerB.GetPeerID(),
				},
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	// Act
	grantType := models.FSCCoreGrantType("GRANT_TYPE_SERVICE_CONNECTION")
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantType: &grantType,
	})

	// Assert
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.IV().String(), contractResponse.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	assert.NoError(t, err)

	serviceConnectionGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantServiceConnection()
	assert.NoError(t, err)

	service, err := serviceConnectionGrant.Service.AsFSCCoreService()
	require.NoError(t, err)

	assert.Equal(t, "parkeerrechten", service.Name)
	assert.Equal(t, peerB.GetPeerID(), service.PeerId)
	assert.Equal(t, contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(), serviceConnectionGrant.Outway.PeerId)
	assert.Equal(t, contractContent.Grants().ServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(), serviceConnectionGrant.Outway.PublicKeyThumbprint)

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))
}

//nolint:funlen,dupl // this is a test
func TestGetContractWithGrantType_DelegatedServiceConnection(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           peerA.GetName(),
						ManagerAddress: "",
					},
					PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:   peerB.GetPeerID(),
						Name: peerB.GetName(),
					},
					Name: "parkeerrechten",
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:   peerC.GetPeerID(),
						Name: peerC.GetName(),
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServiceConnectionArgs{
				OutwayPublicKeyThumbprint: contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().PublicKeyThumbprint().Value(),
				OutwayPeerID:              contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				DelegatorPeerID:           contractContent.Grants().DelegatedServiceConnectionGrants()[0].Delegator().Peer().ID().Value(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerB.GetPeerID(),
				},
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	// Act
	grantType := models.FSCCoreGrantType("GRANT_TYPE_DELEGATED_SERVICE_CONNECTION")
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantType: &grantType,
	})

	// Assert
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.IV().String(), contractResponse.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	assert.NoError(t, err)

	serviceConnectionGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantDelegatedServiceConnection()
	assert.NoError(t, err)

	service, err := serviceConnectionGrant.Service.AsFSCCoreService()
	require.NoError(t, err)

	assert.Equal(t, "parkeerrechten", service.Name)
	assert.Equal(t, peerB.GetPeerID(), service.PeerId)
	assert.Equal(t, peerA.GetPeerID(), serviceConnectionGrant.Outway.PeerId)
	assert.Equal(t, peerA.CertBundle.PublicKeyThumbprint(), serviceConnectionGrant.Outway.PublicKeyThumbprint)
	assert.Equal(t, peerC.GetPeerID(), serviceConnectionGrant.Delegator.PeerId)

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))
}

//nolint:funlen,dupl // this is a test
func TestGetContractWithGrantType_DelegatedServicePublication(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerDirectory.GetPeerID(),
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:   peerA.GetPeerID(),
						Name: peerA.GetName(),
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:   peerB.GetPeerID(),
						Name: peerB.GetName(),
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Delegator().Peer().ID().Value(),
				DirectoryPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	// Act
	grantType := models.FSCCoreGrantType("GRANT_TYPE_DELEGATED_SERVICE_PUBLICATION")
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantType: &grantType,
	})

	// Assert
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.IV().String(), contractResponse.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	assert.NoError(t, err)

	servicePublicationGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantDelegatedServicePublication()
	assert.NoError(t, err)

	assert.Equal(t, "parkeerrechten", servicePublicationGrant.Service.Name)
	assert.Equal(t, peerA.GetPeerID(), servicePublicationGrant.Service.PeerId)
	assert.Equal(t, peerDirectory.GetPeerID(), servicePublicationGrant.Directory.PeerId)
	assert.Equal(t, peerB.GetPeerID(), servicePublicationGrant.Delegator.PeerId)

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))
}

//nolint:funlen // this is a test
func TestGetContractOrdering(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	createContract(t, application, testClock.Now().Add(-5*time.Minute).Truncate(time.Second))
	createContract(t, application, testClock.Now().Add(-48*time.Hour).Truncate(time.Second))
	createContract(t, application, testClock.Now().Truncate(time.Second))

	// Act
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{})
	assert.NoError(t, err)

	// Assert default sorting
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 3, len(contractResponse.JSON200.Contracts))

	assert.Equal(t, testClock.Now().Unix(), contractResponse.JSON200.Contracts[0].Content.CreatedAt)
	assert.Equal(t, testClock.Now().Add(-5*time.Minute).Unix(), contractResponse.JSON200.Contracts[1].Content.CreatedAt)
	assert.Equal(t, testClock.Now().Add(-48*time.Hour).Unix(), contractResponse.JSON200.Contracts[2].Content.CreatedAt)

	sortOrderAscending := models.FSCCoreSortOrderSORTORDERASCENDING

	contractResponseSorted, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		SortOrder: &sortOrderAscending,
	})
	assert.NoError(t, err)

	// Assert
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponseSorted.StatusCode())
	assert.Equal(t, 3, len(contractResponseSorted.JSON200.Contracts))

	assert.Equal(t, testClock.Now().Add(-48*time.Hour).Unix(), contractResponseSorted.JSON200.Contracts[0].Content.CreatedAt)
	assert.Equal(t, testClock.Now().Add(-5*time.Minute).Unix(), contractResponseSorted.JSON200.Contracts[1].Content.CreatedAt)
	assert.Equal(t, testClock.Now().Unix(), contractResponseSorted.JSON200.Contracts[2].Content.CreatedAt)
}

// nolint:dupl // too soon to create abstraction
func createContract(t *testing.T, app *internalapp.Application, createdAt time.Time) {
	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     createdAt,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerB.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           peerA.GetName(),
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().ServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().ServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().ServicePublicationGrants()[0].Service().Name(),
				ServiceProtocol: contractContent.Grants().ServicePublicationGrants()[0].Service().Protocol(),
			},
		},
		CreatedAt:      contractContent.CreatedAt(),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)
}
