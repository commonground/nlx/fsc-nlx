// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	txlogrecord "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type fakeTxLogAPI struct{}

func newFakeTxLog() txlog.TXLog {
	return &fakeTxLogAPI{}
}

func (t *fakeTxLogAPI) ListRecords(_ context.Context, request *txlog.ListRecordsRequest) ([]*txlog.Record, error) {
	// At least 1 filter must contain the PeerID of the connecting Peer
	var connectingPeerID string
	for _, filter := range request.Filters {
		connectingPeerID = filter.PeerID
	}

	if connectingPeerID == "" {
		return nil, errors.New("missing connecting PeerID")
	}

	if request == nil {
		return nil, errors.New("missing request parameters")
	}

	mockTime := time.Date(2023, 1, 1, 1, 42, 0, 0, time.Local)
	recordDelegatedOut := &txlog.Record{
		GroupID:     "fsc-local",
		GrantHash:   "$1$4$testhash",
		ServiceName: "test-service",
		CreatedAt:   mockTime,
		Source: &txlog.RecordDelegatedSource{
			OutwayPeerID:    "1",
			DelegatorPeerID: "3",
		},
		Destination: &txlog.RecordDelegatedDestination{
			ServicePeerID:   "2",
			DelegatorPeerID: "3",
		},
		Direction:     txlogrecord.DirectionOut,
		TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
	}
	recordDelegatedIn := &txlog.Record{
		GroupID:     "fsc-local",
		GrantHash:   "$1$4$testhash",
		ServiceName: "test-service",
		CreatedAt:   mockTime,
		Source: &txlog.RecordDelegatedSource{
			OutwayPeerID:    "1",
			DelegatorPeerID: "3",
		},
		Destination: &txlog.RecordDelegatedDestination{
			ServicePeerID:   "2",
			DelegatorPeerID: "3",
		},
		Direction:     txlogrecord.DirectionIn,
		TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
	}

	recordOut := &txlog.Record{
		GroupID:     "fsc-local",
		GrantHash:   "$1$4$testhash2",
		ServiceName: "test-service",
		CreatedAt:   mockTime,
		Source: &txlog.RecordSource{
			OutwayPeerID: "1",
		},
		Destination: &txlog.RecordDestination{
			ServicePeerID: "2",
		},
		Direction:     txlogrecord.DirectionOut,
		TransactionID: "01899c62-eba5-7b58-b68d-000000000002",
	}
	recordIn := &txlog.Record{
		GroupID:     "fsc-local",
		GrantHash:   "$1$4$testhash2",
		ServiceName: "test-service",
		CreatedAt:   mockTime,
		Source: &txlog.RecordSource{
			OutwayPeerID: "1",
		},
		Destination: &txlog.RecordDestination{
			ServicePeerID: "2",
		},
		Direction:     txlogrecord.DirectionIn,
		TransactionID: "01899c62-eba5-7b58-b68d-000000000002",
	}

	return []*txlog.Record{
		recordDelegatedOut,
		recordDelegatedIn,
		recordOut,
		recordIn,
	}, nil
}

func (t *fakeTxLogAPI) ListRecordMetadata(ctx context.Context, transactionID string) ([]*txlog.MetadataRecord, error) {
	metadataRecords := []*txlog.MetadataRecord{
		{
			TransactionID: transactionID,
			Direction:     metadata.DirectionIn,
			Metadata: map[string]map[string]interface{}{
				"HEADERS": {
					"Fsc-Grant-Hash": "$1$4$testhash",
				},
			},
		},
	}

	return metadataRecords, nil
}
