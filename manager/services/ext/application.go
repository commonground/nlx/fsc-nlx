// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package externalservice

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/controller"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	app "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner"
)

type NewApplicationArgs struct {
	Context                   context.Context
	Clock                     clock.Clock
	Logger                    *logger.Logger
	Repository                contract.Repository
	ControllerRepository      controller.Controller
	TXLogRepository           txlog.TXLog
	PeersCommunicationService *services.PeersCommunicationService
	GroupID                   contract.GroupID
	SelfPeer                  *contract.Peer
	TrustedRootCAs            *x509.CertPool
	AutoSignCertificate       *tls.Certificate
	TokenSignCertificate      *tls.Certificate
	TokenTTL                  time.Duration
	IsTxlogDisabled           bool
	AutoSigner                autosigner.AutoSigner
}

func NewApplication(args *NewApplicationArgs) (*app.Application, error) {
	if args.SelfPeer == nil {
		return nil, errors.New("self peer is required")
	}

	if args.SelfPeer.ManagerAddress() == "" {
		return nil, errors.New("self manager address is required")
	}

	commands, err := setupCommands(args)
	if err != nil {
		return nil, err
	}

	queries, err := setupQueries(args)
	if err != nil {
		return nil, err
	}

	application := &app.Application{
		Queries:  *queries,
		Commands: *commands,
	}

	// Register ourselves first
	err = application.Commands.Announce.Handle(args.Context, &command.PeerArgs{
		ID:             args.SelfPeer.ID().Value(),
		Name:           args.SelfPeer.Name().Value(),
		ManagerAddress: args.SelfPeer.ManagerAddress().Value(),
	})
	if err != nil {
		return nil, fmt.Errorf("could not announce to ourselves: %s", err)
	}

	return application, nil
}

func setupCommands(args *NewApplicationArgs) (*app.Commands, error) {
	submitContract, err := command.NewSubmitContractHandler(&command.NewSubmitContractHandlerArgs{
		GroupID:                   args.GroupID,
		SelfPeerID:                args.SelfPeer.ID(),
		TrustedRootCAs:            args.TrustedRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Logger:                    args.Logger,
		Clock:                     args.Clock,
		AutoSignCertificate:       args.AutoSignCertificate,
		AutoSigner:                args.AutoSigner,
	})
	if err != nil {
		return nil, err
	}

	acceptContract, err := command.NewAcceptContractHandler(&command.NewAcceptContractHandlerArgs{
		GroupID:                   args.GroupID,
		SelfPeerID:                args.SelfPeer.ID(),
		TrustedRootCAs:            args.TrustedRootCAs,
		Contracts:                 args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Logger:                    args.Logger,
		Clock:                     args.Clock,
	})
	if err != nil {
		return nil, err
	}

	rejectContract, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		GroupID:                   args.GroupID,
		SelfPeerID:                args.SelfPeer.ID(),
		TrustedRootCAs:            args.TrustedRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Logger:                    args.Logger,
		Clock:                     args.Clock,
	})
	if err != nil {
		return nil, err
	}

	revokeContract, err := command.NewRevokeContractHandler(&command.NewRevokeContractHandlerArgs{
		GroupID:                   args.GroupID,
		SelfPeerID:                args.SelfPeer.ID(),
		TrustedRootCAs:            args.TrustedRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Logger:                    args.Logger,
		Clock:                     args.Clock,
	})
	if err != nil {
		return nil, err
	}

	announce, err := command.NewAnnounceHandler(&command.NewAnnounceHandlerArgs{Repository: args.Repository, Logger: args.Logger})
	if err != nil {
		return nil, err
	}

	return &app.Commands{
		SubmitContract: submitContract,
		AcceptContract: acceptContract,
		RejectContract: rejectContract,
		RevokeContract: revokeContract,
		Announce:       announce,
	}, nil
}

func setupQueries(args *NewApplicationArgs) (*app.Queries, error) {
	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	listContractsHandler, err := query.NewListContractsHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	getKeySetHandler, err := query.NewGetKeySetHandler(args.Repository, args.SelfPeer.ID())
	if err != nil {
		return nil, err
	}

	listPeersHandler, err := query.NewListPeersHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	listServicesHandler, err := query.NewListServicesHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	getPeerInfoHandler, err := query.NewGetPeerInfoHandler()
	if err != nil {
		return nil, err
	}

	listTransactionLogRecords, err := query.NewListTransactionLogRecordsHandler(args.TXLogRepository, args.GroupID, args.IsTxlogDisabled)
	if err != nil {
		return nil, err
	}

	getTokenHandler, err := query.NewGetTokenHandler(&query.NewGetTokenHandlerArgs{
		Clock:            args.Clock,
		Repo:             args.Repository,
		SignTokenCert:    args.TokenSignCertificate,
		TrustedRootCAs:   args.TrustedRootCAs,
		TokenTTL:         args.TokenTTL,
		ManagementClient: args.ControllerRepository,
	})
	if err != nil {
		return nil, err
	}

	getHealthHandler, err := query.NewGetHealthHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	return &app.Queries{
		ListContracts:             listContractsHandler,
		GetKeySet:                 getKeySetHandler,
		ListPeers:                 listPeersHandler,
		ListServices:              listServicesHandler,
		GetPeerInfo:               getPeerInfoHandler,
		ListTransactionLogRecords: listTransactionLogRecords,
		GetToken:                  getTokenHandler,
		GetHealth:                 getHealthHandler,
	}, nil
}
