// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

// Manager-PeerInfo-1
func TestGetPeerInfo(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	// Act
	res, err := client.GetPeerInfoWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	delegationVersion := models.FSCCoreDelegationVersionN100
	loggingVersion := models.N100

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Equal(t, peerA.GetPeerID(), res.JSON200.PeerId)
	assert.Equal(t, peerA.GetName(), res.JSON200.PeerName)
	assert.Equal(t, models.FSCCoreEnabledExtensions{
		EXTENSIONDELEGATION:         &delegationVersion,
		EXTENSIONTRANSACTIONLOGGING: &loggingVersion,
	}, res.JSON200.EnabledExtensions)
	assert.Equal(t, models.FSCCoreFscVersionN100, res.JSON200.FscVersion)
}
