//go:build integration

/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package externalservice_test

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestClientOnCRL(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	orgOnCRL, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.OrgOnCRL)
	if err != nil {
		log.Panic(err)
	}

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgOnCRL)
	assert.NoError(t, err)

	// Act
	res, err := client.GetPeerInfoWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	responseBody := res.Body

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.CertificateRevoked(fmt.Errorf("certificate with serialnumber: %s is present on a Certificate Revocation List", orgOnCRL.Certificate().SerialNumber))

	assert.Equal(t, http.StatusBadRequest, res.StatusCode())
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.CertificateRevokedErr.String(), res.HTTPResponse.Header.Get("Fsc-Error-Code"))
}

func TestServerOnCrl(t *testing.T) {
	testServer := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Fatal("this test server should not be called")
	}))

	testServer.TLS = &tls.Config{
		MinVersion: tls.VersionTLS12,
		Certificates: []tls.Certificate{
			{
				Certificate: peerOnCRL.CertBundle.Cert().Certificate,
				PrivateKey:  peerOnCRL.CertBundle.PrivateKey(),
			},
		},
		ClientCAs: peerOnCRL.CertBundle.RootCAs(),
	}

	testServer.StartTLS()
	defer testServer.Close()

	crlFile, err := os.ReadFile(filepath.Join("..", "..", "..", "testing", "pki", "ca.crl"))
	assert.NoError(t, err)
	revocationList, err := x509.ParseRevocationList(crlFile)
	assert.NoError(t, err)

	crl, err := common_tls.NewCRL(nil)
	assert.NoError(t, err)
	err = crl.AddRevocationList("http://localhost", revocationList)
	assert.NoError(t, err)

	managerClient, err := rest.NewClient("https://manager.org-a.nlx.local:443", peerOnCRL.CertBundle.GetPeerInfo().SerialNumber, testServer.URL, peerA.CertBundle, crl, testClock)
	assert.NoError(t, err)

	_, err = managerClient.GetPeerInfo(context.Background())
	expectedCertificateRevokedErr := common_tls.NewCertificateRevokedError(peerOnCRL.CertBundle.Certificate().SerialNumber.String())
	expectedErr := url.Error{Err: expectedCertificateRevokedErr}
	assert.ErrorAs(t, err, &expectedErr.Err)

}
