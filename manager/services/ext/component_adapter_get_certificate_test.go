// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestGetCertificate(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	err := intApp.Commands.CreateCertificate.Handle(context.Background(), peerA.CertBundle.Cert().Certificate)
	require.NoError(t, err)
	orgACertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)
	crlCache, err := tls.NewCRL([]string{})
	require.NoError(t, err)
	client, err := rest.NewClient(externalHTTPServer.URL, orgACertBundle.GetPeerInfo().SerialNumber, externalHTTPServer.URL, orgACertBundle, crlCache, testClock)
	assert.NoError(t, err)

	// Act
	certificates, err := client.GetCertificates(context.Background())
	require.NoError(t, err)

	// Assert
	assert.NotNil(t, certificates)
	cert, err := certificates.GetCertificate(contract.CertificateThumbprint(peerA.CertBundle.CertificateThumbprint()))
	assert.NoError(t, err)
	assert.Equal(t, cert.PublicKey(), peerA.CertBundle.PublicKey())
	assert.Equal(t, cert.RawDERs(), peerA.CertBundle.Cert().Certificate)
}
