// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

func TestAnnounce(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	tests := map[string]struct {
		Params     *models.AnnounceParams
		Want       string
		WantStatus int
		WantBody   string
	}{
		"with_empty_address": {
			Params: &models.AnnounceParams{
				FscManagerAddress: "",
			},
			Want:       "https://my-manager-address:8443",
			WantStatus: http.StatusBadRequest,
			WantBody:   "Invalid format for parameter fsc-manager-address: parameter 'fsc-manager-address' is empty, can't bind its value\n",
		},
		"with_trailing_slash": {
			Params: &models.AnnounceParams{
				FscManagerAddress: "https://my-manager-address:8443/",
			},
			Want:       "https://my-manager-address:8443",
			WantStatus: http.StatusOK,
			WantBody:   "",
		},
		"happy_flow": {
			Params: &models.AnnounceParams{
				FscManagerAddress: "https://my-manager-address:8443",
			},
			Want:       "https://my-manager-address:8443",
			WantStatus: http.StatusOK,
			WantBody:   "",
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			// Act
			res, err := client.AnnounceWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			// Assert
			if !assert.Equal(t, tc.WantStatus, res.StatusCode()) {
				t.Errorf("unexpected status code %d expected %d response body: %s", res.StatusCode(), tc.WantStatus, res.Body)
			}

			require.Equal(t, tc.WantBody, string(res.Body))

			resPeers, err := client.GetPeersWithResponse(context.Background(), &models.GetPeersParams{
				PeerId: &[]models.FSCCorePeerID{
					peerB.GetPeerID(),
				},
			})
			assert.NoError(t, err)

			if !assert.Equal(t, http.StatusOK, resPeers.StatusCode()) {
				t.Errorf("unexpected status code %d response body: %s", res.StatusCode(), res.Body)
			}

			if tc.WantStatus == http.StatusOK {
				expectedPeers := []models.FSCCorePeer{
					{
						Id:             peerB.GetPeerID(),
						ManagerAddress: tc.Want,
						Name:           peerB.GetName(),
					},
				}
				assert.Equal(t, expectedPeers, resPeers.JSON200.Peers)
			}
		})
	}
}
