// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetHealth(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	// Act
	res, err := clientOrgB.GetHealthWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())
}
