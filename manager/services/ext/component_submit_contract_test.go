// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner/granttype"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

//nolint:funlen // this is a test
func TestSubmitContract(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerB.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	acceptSignature, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), time.Now().Truncate(time.Second))
	assert.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	// Act
	submitContractResp, err := client.SubmitContractWithResponse(context.Background(), &models.SubmitContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.SubmitContractJSONRequestBody(
		models.SubmitContractJSONBody{
			ContractContent: *cc,
			Signature:       acceptSignature.JWS(),
		}),
	)

	// Assert
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, submitContractResp.StatusCode())

	contracts, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantHash: &[]string{
			contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		},
	})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(contracts.JSON200.Contracts))

	expectedAcceptSignatures := models.FSCCoreSignatureMap{
		peerB.GetPeerID(): acceptSignature.JWS(),
	}

	assert.Equal(t, expectedAcceptSignatures, contracts.JSON200.Contracts[0].Signatures.Accept)
}

// nolint:funlen,dupl // this is a test
func TestSubmitContractUsingHTTP2Protocol(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP2,
				},
			},
		},
	})
	assert.NoError(t, err)

	acceptSignature, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), time.Now().Truncate(time.Second))
	assert.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	// Act
	submitContractResp, err := client.SubmitContractWithResponse(context.Background(), &models.SubmitContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.SubmitContractJSONRequestBody(
		models.SubmitContractJSONBody{
			ContractContent: *cc,
			Signature:       acceptSignature.JWS(),
		}),
	)

	// Assert
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, submitContractResp.StatusCode())
}

//nolint:funlen,dupl // this is a test
func TestSubmitContractWithAutoSignGrants(t *testing.T) {
	t.Parallel()

	autoSigner, err := granttype.New([]string{"serviceConnection"})
	require.NoError(t, err)

	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{
		AutoSigner: autoSigner,
	})
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerB.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	acceptSignature, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), time.Now().Truncate(time.Second))
	assert.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	// Act
	submitContractResp, err := client.SubmitContractWithResponse(context.Background(), &models.SubmitContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.SubmitContractJSONRequestBody(
		models.SubmitContractJSONBody{
			ContractContent: *cc,
			Signature:       acceptSignature.JWS(),
		}),
	)

	// Assert
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, submitContractResp.StatusCode())

	contracts, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantHash: &[]string{
			contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		},
	})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(contracts.JSON200.Contracts))

	assert.Contains(t, contracts.JSON200.Contracts[0].Signatures.Accept, peerA.GetPeerID())
	assert.Contains(t, contracts.JSON200.Contracts[0].Signatures.Accept, peerB.GetPeerID())
}

//nolint:dupl,funlen // this is a test
func TestSuiteSubmitContact(t *testing.T) {
	contractDate := testClock.Now().UTC()
	contractValidityStart := contractDate
	contractValidityEnd := contractDate.Add(time.Hour)

	testCases := map[string]struct {
		Description           string
		Request               *models.SubmitContractJSONRequestBody
		WantStatusCode        int
		WantResponseDomain    models.FSCCoreErrorDomain
		WantResponseErrorCode string
	}{
		"Manager-SubmitContract-1": {
			Description: "Submit a Contract with a ServiceConnectionGrant",
			Request: func() *models.SubmitContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantServiceConnectionServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								Name: "parkeerrechten",
							},
						},
					},
				})
				assert.NoError(t, err)

				acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
				assert.NoError(t, err)

				cc, err := rest.ContractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.SubmitContractJSONRequestBody{
					ContractContent: *cc,
					Signature:       acceptSignatureOrgB.JWS(),
				}
			}(),
			WantStatusCode:        http.StatusCreated,
			WantResponseDomain:    "",
			WantResponseErrorCode: "",
		},
		"Manager-SubmitContract-2": {
			Description: "Submit a Contract with a ServicePublicationGrant",
			Request: func() *models.SubmitContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServicePublicationArgs{
							Directory: &contract.NewGrantServicePublicationDirectoryArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           peerA.GetName(),
									ManagerAddress: peerA.ManagerAddress,
								},
							},
							Service: &contract.NewGrantServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           peerB.GetName(),
									ManagerAddress: peerB.ManagerAddress,
								},
								Name:     "parkeerrechten",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
					},
				})
				assert.NoError(t, err)

				cc, err := rest.ContractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
				assert.NoError(t, err)

				return &models.SubmitContractJSONRequestBody{
					ContractContent: *cc,
					Signature:       acceptSignatureOrgB.JWS(),
				}
			}(),
			WantStatusCode:        http.StatusCreated,
			WantResponseDomain:    "",
			WantResponseErrorCode: "",
		},
		"Manager-SubmitContract-3": {
			Description: "Submit a Contract with a DelegatedServicePublicationGrant",
			Request: func() *models.SubmitContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantDelegatedServicePublicationArgs{
							Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{Peer: &contract.NewPeerArgs{
								ID:             peerA.GetPeerID(),
								Name:           peerA.GetName(),
								ManagerAddress: peerA.ManagerAddress,
							}},
							Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           peerB.GetName(),
									ManagerAddress: peerB.ManagerAddress,
								},
								Name:     "parkeerrechten",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
							Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerC.GetPeerID(),
									Name:           peerC.GetName(),
									ManagerAddress: peerC.ManagerAddress,
								},
							},
						},
					},
				})
				assert.NoError(t, err)

				acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
				assert.NoError(t, err)

				cc, err := rest.ContractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.SubmitContractJSONRequestBody{
					ContractContent: *cc,
					Signature:       acceptSignatureOrgB.JWS(),
				}
			}(),
			WantStatusCode:        http.StatusCreated,
			WantResponseDomain:    "",
			WantResponseErrorCode: "",
		},
		"Manager-SubmitContract-4": {
			Description: "Submit a Contract with a DelegatedServiceConnectionGrant",
			Request: func() *models.SubmitContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantDelegatedServiceConnectionArgs{
							Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           peerA.GetName(),
									ManagerAddress: peerA.ManagerAddress,
								},
								PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           peerB.GetName(),
									ManagerAddress: peerB.ManagerAddress,
								},
								Name: "parkeerrechten",
							},
							Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerC.GetPeerID(),
									Name:           peerC.GetName(),
									ManagerAddress: peerC.ManagerAddress,
								},
							},
						},
					},
				})
				assert.NoError(t, err)

				acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
				assert.NoError(t, err)

				cc, err := rest.ContractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.SubmitContractJSONRequestBody{
					ContractContent: *cc,
					Signature:       acceptSignatureOrgB.JWS(),
				}
			}(),
			WantStatusCode:        http.StatusCreated,
			WantResponseDomain:    "",
			WantResponseErrorCode: "",
		},
		"Manager-SubmitContract-5": {
			Description: "Submit a Contract with a ServiceConnectionGrant containing a Delegated Service",
			Request: func() *models.SubmitContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: peerC.GetPeerID(),
								},
							},
						},
					},
				})
				assert.NoError(t, err)

				acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
				assert.NoError(t, err)

				cc, err := rest.ContractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.SubmitContractJSONRequestBody{
					ContractContent: *cc,
					Signature:       acceptSignatureOrgB.JWS(),
				}
			}(),
			WantStatusCode:        http.StatusCreated,
			WantResponseDomain:    "",
			WantResponseErrorCode: "",
		},
		"Manager-SubmitContract-6": {
			Description: "Submit a Contract with a DelegatedServiceConnectionGrant containing a Delegated Service",
			Request: func() *models.SubmitContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantDelegatedServiceConnectionArgs{
							Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           peerA.GetName(),
									ManagerAddress: peerA.ManagerAddress,
								},
								PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantDelegatedServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           peerB.GetName(),
									ManagerAddress: peerB.ManagerAddress,
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: peerDirectory.GetPeerID(),
								},
							},
							Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerC.GetPeerID(),
									Name:           peerC.GetName(),
									ManagerAddress: peerC.ManagerAddress,
								},
							},
						},
					},
				})
				assert.NoError(t, err)

				acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
				assert.NoError(t, err)

				cc, err := rest.ContractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.SubmitContractJSONRequestBody{
					ContractContent: *cc,
					Signature:       acceptSignatureOrgB.JWS(),
				}
			}(),
			WantStatusCode:        http.StatusCreated,
			WantResponseDomain:    "",
			WantResponseErrorCode: "",
		},
		"Manager-SubmitContract-7": {
			Description: "Submit a Contract without being a Peer on the Contract",
			Request: func() *models.SubmitContractJSONRequestBody {
				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: "00000000000000000005",
					Type:   models.SERVICETYPESERVICE,
				})

				dataServiceConnectionGrant := models.FSCCoreGrant_Data{}
				_ = dataServiceConnectionGrant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerC.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerC.GetPeerID(),
					},
					Service: service,
				})

				return &models.SubmitContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt: contractDate.Unix(),
						Grants: []models.FSCCoreGrant{
							{Data: dataServiceConnectionGrant},
						},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "1a6f631a-6e78-429d-94b2-541460c45f2b",
						Validity: models.FSCCoreValidity{
							NotBefore: contractValidityStart.Unix(),
							NotAfter:  contractValidityEnd.Unix(),
						},
					},
					Signature: "arbitrary-signature",
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODEPEERNOTPARTOFCONTRACT),
		},
		"Manager-SubmitContract-8": {
			Description: "Submit a Contract with a ServicePublicationGrant combined with any another grant",
			Request: func() *models.SubmitContractJSONRequestBody {
				dataServicePublication := models.FSCCoreGrant_Data{}
				_ = dataServicePublication.FromFSCCoreGrantServicePublication(models.FSCCoreGrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.FSCCoreDirectory{
						PeerId: peerA.GetPeerID(),
					},
					Service: models.FSCCoreServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerB.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})

				service := models.FSCCoreGrantServiceConnection_Service{}

				_ = service.FromFSCCoreService(models.FSCCoreService{
					Name:   "parkeerrechten",
					PeerId: peerA.GetPeerID(),
					Type:   models.SERVICETYPESERVICE,
				})

				dataServiceConnection := models.FSCCoreGrant_Data{}
				_ = dataServiceConnection.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Outway: models.FSCCoreOutway{
						PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
						PeerId:              peerB.GetPeerID(),
					},
					Service: service,
					Type:    models.GRANTTYPESERVICECONNECTION,
				})

				return &models.SubmitContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt: contractDate.Unix(),
						Grants: []models.FSCCoreGrant{
							{Data: dataServicePublication},
							{Data: dataServiceConnection},
						},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "1a6f631a-6e78-429d-94b2-541460c45f2b",
						Validity: models.FSCCoreValidity{
							NotBefore: contractValidityStart.Unix(),
							NotAfter:  contractValidityEnd.Unix(),
						},
					}, Signature: "signature",
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODEGRANTCOMBINATIONNOTALLOWED),
		},
		"Manager-SubmitContract-9": {
			Description: "Submit a Contract with a Group ID that does not match with the Group ID of the receiving Manager",
			Request: func() *models.SubmitContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "invalid-group-id",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantServiceConnectionServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								Name: "parkeerrechten",
							},
						},
					},
				})
				assert.NoError(t, err)

				acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), testClock.Now().Truncate(time.Second))
				assert.NoError(t, err)

				cc, err := rest.ContractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.SubmitContractJSONRequestBody{
					ContractContent: *cc,
					Signature:       acceptSignatureOrgB.JWS(),
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODEINCORRECTGROUPID),
		},
	}
	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})

	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	for name, tc := range testCases {
		testCase := tc

		t.Run(name, func(t *testing.T) {
			resp, errSubmit := client.SubmitContractWithResponse(context.Background(), &models.SubmitContractParams{
				FscManagerAddress: peerB.ManagerAddress,
			}, *testCase.Request)

			assert.NoError(t, errSubmit)
			if !assert.Equal(t, testCase.WantStatusCode, resp.StatusCode()) {
				println(name, string(resp.Body))
			}

			if testCase.WantResponseDomain != "" || testCase.WantResponseErrorCode != "" {
				assert.Equal(t, testCase.WantResponseErrorCode, resp.HTTPResponse.Header.Get("Fsc-Error-Code"))
				switch testCase.WantStatusCode {
				case http.StatusBadRequest:
					assert.Equal(t, testCase.WantResponseErrorCode, resp.JSON400.Code)
					assert.Equal(t, testCase.WantResponseDomain, resp.JSON400.Domain)

				case http.StatusUnprocessableEntity:
					assert.Equal(t, testCase.WantResponseErrorCode, resp.JSON422.Code)
					assert.Equal(t, testCase.WantResponseDomain, resp.JSON422.Domain)

				default:
					t.Fatalf("unexpected status code %d", resp.StatusCode())
				}
			}
		})
	}
}

// Test suite test case "Manager-SubmitContract-11"
// "Submit a Contract with a Contract Content hash where the Hash Algorithm in the Contract Content hash or Grant Hash is not supported"
//
//nolint:funlen // this is a test
func TestSubmitContractInvalidHashAlgorithm(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _ := newService(t.Name(), &newServiceArgs{})
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerB.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	acceptSignature, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), time.Now().Truncate(time.Second))
	assert.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)
	cc.HashAlgorithm = "HS256"

	// Act
	submitContractResp, err := client.SubmitContractWithResponse(context.Background(), &models.SubmitContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.SubmitContractJSONRequestBody(
		models.SubmitContractJSONBody{
			ContractContent: *cc,
			Signature:       acceptSignature.JWS(),
		}),
	)

	// Assert
	if !assert.Equal(t, http.StatusUnprocessableEntity, submitContractResp.StatusCode()) {
		t.Errorf("response body: %s", submitContractResp.Body)
	}
	assert.Equal(t, "ERROR_CODE_UNKNOWN_HASH_ALGORITHM_HASH", submitContractResp.JSON422.Code)
	assert.Equal(t, models.ERRORDOMAINMANAGER, submitContractResp.JSON422.Domain)
}
