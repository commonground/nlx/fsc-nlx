// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package internalservice

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"net/netip"

	"github.com/cenkalti/backoff/v4"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/controller"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	app "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/authentication"
)

type NewApplicationArgs struct {
	Context                     context.Context
	Clock                       clock.Clock
	Logger                      *logger.Logger
	Repository                  contract.Repository
	PeersCommunicationService   *services.PeersCommunicationService
	ContractDistributionService *services.ContractDistributionService
	TXLog                       txlog.TXLog
	SelfPeer                    *contract.Peer
	TrustedExternalRootCAs      *x509.CertPool
	SignatureCertificate        *tls.Certificate
	ControllerRepository        controller.Controller
	ManagerFactory              manager.Factory
	GroupID                     contract.GroupID
	DirectoryPeerID             string
	DirectoryPeerManagerAddress string
	DirectoryPeerName           string
	IsTxlogDisabled             bool
	Auditlogger                 *auditlog.AuditLogger
	InternalCertificate         *common_tls.CertificateBundle
}

func NewApplication(args *NewApplicationArgs) (*app.Application, error) {
	if args.PeersCommunicationService == nil {
		return nil, errors.New("missing peer communication service")
	}

	if args.ContractDistributionService == nil {
		return nil, errors.New("missing contract distribution service")
	}

	if args.Clock == nil {
		return nil, errors.New("missing clock")
	}

	if args.InternalCertificate == nil {
		return nil, errors.New("missing internal certificate")
	}

	registerPeer, err := command.NewAnnouncePeerHandler(args.TrustedExternalRootCAs, args.PeersCommunicationService, args.Logger)
	if err != nil {
		return nil, err
	}

	createContract, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		TrustedExternalRootCAs:    args.TrustedExternalRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Clock:                     args.Clock,
		Logger:                    args.Logger,
		SignatureCert:             args.SignatureCertificate,
		SelfPeerID:                args.SelfPeer.ID(),
		AuditLgr:                  args.Auditlogger,
	})
	if err != nil {
		return nil, err
	}

	approveContract, err := command.NewAcceptContractHandler(&command.NewAcceptContractHandlerArgs{
		TrustedExternalRootCAs:    args.TrustedExternalRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Clock:                     args.Clock,
		Logger:                    args.Logger,
		SignatureCert:             args.SignatureCertificate,
		AuditLgr:                  args.Auditlogger,
	})
	if err != nil {
		return nil, err
	}

	rejectContract, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		TrustedExternalRootCAs:    args.TrustedExternalRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Clock:                     args.Clock,
		Logger:                    args.Logger,
		SignatureCert:             args.SignatureCertificate,
		AuditLgr:                  args.Auditlogger,
	})
	if err != nil {
		return nil, err
	}

	revokeContract, err := command.NewRevokeContractHandler(&command.NewRevokeContractHandlerArgs{
		TrustedExternalRootCAs:    args.TrustedExternalRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Clock:                     args.Clock,
		Logger:                    args.Logger,
		SignatureCert:             args.SignatureCertificate,
		AuditLgr:                  args.Auditlogger,
	})
	if err != nil {
		return nil, err
	}

	retryContractDistribution, err := command.NewRetryContractDistributionHandler(&command.NewRetryContractDistributionHandlerArgs{
		Repository:               args.Repository,
		PeerCommunicationService: args.PeersCommunicationService,
		AuditLgr:                 args.Auditlogger,
	})
	if err != nil {
		return nil, err
	}

	syncPeers, err := command.NewSyncPeersHandler(&command.NewSyncPeersHandlerArgs{
		ManagerFactory: args.ManagerFactory,
		Repository:     args.Repository,
		Logger:         args.Logger,
	})
	if err != nil {
		return nil, err
	}

	getServicesForOutway, err := query.NewGetServicesForOutwayHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	getCertificates, err := query.NewGetCertificatesHandler(&query.NewGetCertificatesHandlerArg{
		Repository: args.Repository,
		SelfPeerID: args.SelfPeer.ID(),
	})
	if err != nil {
		return nil, err
	}

	getContracts, err := query.NewListContractsHandler(args.Clock, args.Repository, args.SelfPeer.ID())
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create get contracts handler"))
	}

	listOutgoingConnections, err := query.NewListOutgoingConnectionsHandler(&query.NewListOutgoingConnectionsHandlerArgs{
		Repository: args.Repository,
		SelfPeerID: args.SelfPeer.ID(),
		Logger:     args.Logger,
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create list outgoing connections handler"))
	}

	listOutgoingConnectionsForService, err := query.NewListOutgoingConnectionsForServiceHandler(&query.NewListOutgoingConnectionsForServiceHandlerArgs{
		Repository: args.Repository,
		SelfPeerID: args.SelfPeer.ID(),
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create list outgoing connections for service handler"))
	}

	listIncomingConnections, err := query.NewListIncomingConnectionsHandler(args.Repository, args.SelfPeer.ID())
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create list incoming connections handler"))
	}

	listDelegatedConnections, err := query.NewListDelegatedConnectionsHandler(args.Repository, args.SelfPeer.ID())
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create list delegated connections handler"))
	}

	getContractsPending, err := query.NewListContractsPendingHandler(args.Clock, args.Repository, args.SelfPeer.ID())
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create get contracts pending handler"))
	}

	getContractDistributions, err := query.NewListFailedContractDistributionsHandler(args.Repository)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create get contract distribution handler"))
	}

	listServices, err := query.NewListServicesHandler(&query.NewListServicesHandlerArgs{
		ManagerFactory: args.ManagerFactory,
		Logger:         args.Logger,
		Repository:     args.Repository,
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create get services handler"))
	}

	listPublishedServices, err := query.NewListServicePublicationsHandler(args.Clock, args.Repository, args.SelfPeer.ID())
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create get published services handler"))
	}

	getDelegatedPublishedServices, err := query.NewListDelegatedServicePublicationsHandler(args.Clock, args.Repository, args.SelfPeer.ID())
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create get delegated published services handler"))
	}

	createCertificate, err := command.NewCreateCertificateHandler(args.TrustedExternalRootCAs, args.Repository, args.Clock)
	if err != nil {
		return nil, err
	}

	createPeer, err := command.NewCreatePeerHandler(&command.NewCreatePeerHandlerArgs{
		Repository: args.Repository,
		Logger:     args.Logger,
		AuditLgr:   args.Auditlogger,
	})
	if err != nil {
		return nil, err
	}

	getTXLogRecords, err := query.NewListTransactionLogRecordsHandler(args.GroupID, args.TXLog, args.PeersCommunicationService, args.IsTxlogDisabled)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create list txlog records handler"))
	}

	getTXLogMetadata, err := query.NewListTxLogMetadataHandler(args.GroupID, args.TXLog, args.IsTxlogDisabled)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create list txlog metadata handler"))
	}

	getPeerInfo, err := query.NewGetPeerInfoHandler(args.SelfPeer.ID().Value(), args.SelfPeer.Name().Value())
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create get peer info handler"))
	}

	listPeers, err := query.NewListPeersHandler(&query.NewListPeersHandlerArgs{
		ManagerFactory: args.ManagerFactory,
		Repository:     args.Repository,
		Logger:         args.Logger,
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create get peers handler"))
	}

	getHealth, err := query.NewGetHealthHandler(args.Repository)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create get health handler"))
	}

	application := &app.Application{
		Queries: app.Queries{
			GetServicesForOutway:              getServicesForOutway,
			GetCertificates:                   getCertificates,
			GetContracts:                      getContracts,
			ListPendingContracts:              getContractsPending,
			GetContractDistributions:          getContractDistributions,
			ListServices:                      listServices,
			GetDelegatedServicePublications:   getDelegatedPublishedServices,
			GetTXLogRecords:                   getTXLogRecords,
			GetTXLogMetadata:                  getTXLogMetadata,
			GetPeerInfo:                       getPeerInfo,
			ListPeers:                         listPeers,
			ListOutgoingConnections:           listOutgoingConnections,
			ListOutgoingConnectionsForService: listOutgoingConnectionsForService,
			ListIncomingConnections:           listIncomingConnections,
			ListDelegatedConnections:          listDelegatedConnections,
			ListServicePublications:           listPublishedServices,
			GetHealth:                         getHealth,
		},
		Commands: app.Commands{
			AnnouncePeer:              registerPeer,
			CreateContract:            createContract,
			AcceptContract:            approveContract,
			RejectContract:            rejectContract,
			RevokeContract:            revokeContract,
			CreateCertificate:         createCertificate,
			RetryContractDistribution: retryContractDistribution,
			CreatePeer:                createPeer,
			SyncPeers:                 syncPeers,
		},
	}

	err = args.Repository.UpsertPeer(context.Background(), args.SelfPeer)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create self peer"))
	}

	err = args.ContractDistributionService.ClearAllNextAttemptTimestamps()
	if err != nil {
		return nil, errors.Join(err, errors.New("could not clear all contract distribution next attempt timestamps"))
	}

	if args.DirectoryPeerID != "" && args.DirectoryPeerManagerAddress != "" {
		err := application.Commands.CreatePeer.Handle(args.Context, &command.CreatePeerArgs{
			PeerID:         args.DirectoryPeerID,
			PeerName:       directoryPeerName(args.DirectoryPeerName),
			ManagerAddress: args.DirectoryPeerManagerAddress,
			Roles:          []command.PeerRole{command.PeerRoleDirectory},
			AuditLogSource: common_auditlog.SourceHTTP{
				IPAddress: directoryIP(),
				UserAgent: "manager",
			},
			AuthData: &authentication.CertificateData{
				SubjectCommonName:   args.InternalCertificate.Certificate().Subject.CommonName,
				PublicKeyThumbprint: common_tls.PublicKeyThumbprint(args.InternalCertificate.Certificate()),
			},
		})
		if err != nil {
			return nil, errors.Join(err, errors.New("could not create directory peer"))
		}
	}

	createAnnounceDirectoryPeer := func() error {
		directoryPeers, err := args.Repository.ListDirectoryPeers(context.Background())
		if err != nil {
			return errors.Join(err, errors.New("could not list directory peers"))
		}

		peerIds := []string{}
		for _, peer := range directoryPeers {
			peerIds = append(peerIds, peer.ID().Value())
		}

		err = application.Commands.AnnouncePeer.Handle(context.Background(), peerIds)
		if err != nil {
			args.Logger.Error("could not announce to directory peer", err)

			return errors.Join(err, errors.New("could not announce to directory peer"))
		}

		return nil
	}

	go func() {
		err = backoff.Retry(createAnnounceDirectoryPeer, backoff.WithContext(backoff.NewExponentialBackOff(), context.Background()))
		if err != nil {
			args.Logger.Error("could not create directory peer in database", err)
		}
	}()

	return application, nil
}

func directoryPeerName(input string) string {
	result := "Directory"

	if input != "" {
		result = input
	}

	return result
}

func directoryIP() netip.Addr {
	return netip.MustParseAddr("127.0.0.1")
}
