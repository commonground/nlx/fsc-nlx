// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// nolint:dupl // this is a test
func TestGetFailedContractDistributions_InvalidHash(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _, _, err := newServiceWithCustomManagerFactory(t.Name(), &fakeFailingManagerFactory{})
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	res, err := apiClient.GetContractDistributionsWithResponse(context.Background(), "invalid-hash")
	require.NoError(t, err)

	wantErr := models.Error{
		Details: "could not decode content hash from string: invalid hash prefix, must start with '$'",
		Status:  http.StatusUnprocessableEntity,
		Title:   "invalid arguments",
	}

	assert.Equal(t, http.StatusUnprocessableEntity, res.StatusCode())
	assert.Equal(t, &wantErr, res.ApplicationproblemJSON422)
}

// nolint:dupl // this is a test
func TestGetFailedContractDistributions(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp, _, err := newServiceWithCustomManagerFactory(t.Name(), &fakeFailingManagerFactory{})
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	contractContentHash, err := intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerB.GetPeerID(),
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	contracts, err := intApp.Queries.GetContracts.Handle(context.Background(), &query.ListContractsHandlerArgs{
		ContentHashes: []string{contractContentHash},
	})
	require.NoError(t, err)
	require.Len(t, contracts, 1)

	contractAcceptSignatures := contracts[0].SignaturesAccepted()
	require.Len(t, contractAcceptSignatures, 1)

	acceptSignature, ok := contractAcceptSignatures[contract.PeerID(peerA.GetPeerID())]
	require.True(t, ok)

	// Assert
	var failedDistributions query.ListFailedContractDistributions

	expiration := 2 * time.Second
	timeStarted := time.Now()

	// since sending the Contract to the other organization happens in a go-routine, the distribution is
	// not executed immediately. So, we poll the failed distributions until we have one.
	for {
		result, err := intApp.Queries.GetContractDistributions.Handle(context.Background(), contractContentHash)
		require.NoError(t, err)

		if len(result) != 0 {
			failedDistributions = result
			break
		}

		shouldExpire := timeStarted.After(timeStarted.Add(expiration))

		if shouldExpire {
			require.Fail(t, fmt.Sprintf("forced failure, since test took longer than expected %q", expiration))
		}
	}

	require.Len(t, failedDistributions, 1)

	res, err := apiClient.GetContractDistributionsWithResponse(context.Background(), contractContentHash)
	require.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.StatusCode())
	require.Len(t, res.JSON200.Distributions, 1)

	distribution := res.JSON200.Distributions[0]

	// we have to test each property individually, since we can't assert the
	// NextAttemptAt because by the time this request is executed,
	// the NextAttemptAt might have been updated already
	assert.Equal(t, models.DISTRIBUTIONACTIONSUBMITCONTRACT, distribution.Action)
	assert.Equal(t, 1, distribution.Attempts)
	assert.Equal(t, contractContentHash, distribution.ContractHash)
	assert.Equal(t, testClock.Now().Unix(), distribution.LastAttemptAt)
	assert.Equal(t, peerB.GetPeerID(), distribution.PeerId)
	assert.Equal(t, "could not get client for peer \"00000000000000000002\": could not get manager address: could not get manager address from directory: could not get peer from directory: arbitrary", distribution.Reason)
	assert.Equal(t, acceptSignature.JWS(), distribution.Signature)
}
