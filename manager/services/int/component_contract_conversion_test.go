// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func contractContentToAPIModel(cc *contract.Content) (*models.ContractContent, error) {
	grants := cc.Grants()

	grantAPIModels := make([]models.Grant, len(grants))

	for i, grant := range grants {
		switch g := grant.(type) {
		case *contract.GrantServicePublication:
			model, err := servicePublicationGrantToAPIModel(g)
			if err != nil {
				return nil, err
			}

			grantAPIModels[i] = model
		case *contract.GrantServiceConnection:
			grantAPIModels[i] = serviceConnectionGrantToAPIModel(g)
		case *contract.GrantDelegatedServiceConnection:
			grantAPIModels[i] = delegatedServiceConnectionGrantToAPIModel(g)
		case *contract.GrantDelegatedServicePublication:
			model, err := delegatedServicePublicationGrantToAPIModel(g)
			if err != nil {
				return nil, err
			}

			grantAPIModels[i] = model
		}
	}

	return &models.ContractContent{
		CreatedAt:     cc.CreatedAt().Unix(),
		Grants:        grantAPIModels,
		GroupId:       cc.GroupID(),
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Iv:            cc.IV().String(),
		Validity: models.Validity{
			NotAfter:  cc.NotAfter().Unix(),
			NotBefore: cc.NotBefore().Unix(),
		},
	}, nil
}

func servicePublicationGrantToAPIModel(g *contract.GrantServicePublication) (models.Grant, error) {
	data := models.Grant{}

	var protocol models.Protocol

	switch g.Service().Protocol() {
	case contract.ServiceProtocolTCPHTTP1_1:
		protocol = models.PROTOCOLTCPHTTP11
	case contract.ServiceProtocolTCPHTTP2:
		protocol = models.PROTOCOLTCPHTTP2
	default:
		return models.Grant{}, fmt.Errorf("unsupported protocol: %s", g.Service().Protocol())
	}

	_ = data.FromGrantServicePublication(models.GrantServicePublication{
		Type: models.GRANTTYPESERVICEPUBLICATION,
		Directory: models.DirectoryPeer{
			PeerId: g.Directory().Peer().ID().Value(),
		},
		Service: models.ServicePublication{
			Name:     g.Service().Name(),
			PeerId:   g.Service().Peer().ID().Value(),
			Protocol: protocol,
		},
	})

	return data, nil
}

func serviceConnectionGrantToAPIModel(g *contract.GrantServiceConnection) models.Grant {
	service := models.GrantServiceConnection_Service{}

	//nolint:dupl // looks the same but is a different grant
	switch s := g.Service().(type) {
	case *contract.GrantServiceConnectionService:
		_ = service.FromService(models.Service{
			Name: s.Name(),
			Peer: models.Peer{
				Id: s.Peer().ID().Value(),
			},
		})
	case *contract.GrantServiceConnectionDelegatedService:
		_ = service.FromDelegatedService(models.DelegatedService{
			Delegator: models.Peer{
				Id: s.PublicationDelegator().ID().Value(),
			},
			Name: s.Name(),
			Peer: models.Peer{
				Id: s.Peer().ID().Value(),
			},
		})
	}

	data := models.Grant{}
	_ = data.FromGrantServiceConnection(models.GrantServiceConnection{
		Outway: models.OutwayPeer{
			PeerId:              g.Outway().Peer().ID().Value(),
			PublicKeyThumbprint: g.Outway().PublicKeyThumbprint().Value(),
		},
		Service: service,
	})

	return data
}

func delegatedServiceConnectionGrantToAPIModel(g *contract.GrantDelegatedServiceConnection) models.Grant {
	service := models.GrantDelegatedServiceConnection_Service{}

	//nolint:dupl // looks the same but is a different grant
	switch s := g.Service().(type) {
	case *contract.GrantDelegatedServiceConnectionService:
		_ = service.FromService(models.Service{
			Name: s.Name(),
			Peer: models.Peer{
				Id: s.Peer().ID().Value(),
			},
		})
	case *contract.GrantDelegatedServiceConnectionDelegatedService:
		_ = service.FromDelegatedService(models.DelegatedService{
			Delegator: models.Peer{
				Id: s.PublicationDelegator().ID().Value(),
			},
			Name: s.Name(),
			Peer: models.Peer{
				Id: s.Peer().ID().Value(),
			},
		})
	}

	data := models.Grant{}
	_ = data.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
		Delegator: models.DelegatorPeer{
			PeerId: g.Delegator().Peer().ID().Value(),
		},
		Outway: models.OutwayPeer{
			PeerId:              g.Outway().Peer().ID().Value(),
			PublicKeyThumbprint: g.Outway().PublicKeyThumbprint().Value(),
		},
		Service: service,
	})

	return data
}

func delegatedServicePublicationGrantToAPIModel(g *contract.GrantDelegatedServicePublication) (models.Grant, error) {
	data := models.Grant{}

	var protocol models.Protocol

	switch g.Service().Protocol() {
	case contract.ServiceProtocolTCPHTTP1_1:
		protocol = models.PROTOCOLTCPHTTP11
	case contract.ServiceProtocolTCPHTTP2:
		protocol = models.PROTOCOLTCPHTTP2
	default:
		return models.Grant{}, fmt.Errorf("unsupported protocol: %s", g.Service().Protocol())
	}

	_ = data.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
		Delegator: models.DelegatorPeer{
			PeerId: g.Delegator().Peer().ID().Value(),
		},
		Directory: models.DirectoryPeer{
			PeerId: g.Directory().Peer().ID().Value(),
		},
		Service: models.ServicePublication{
			Name:     g.Service().Name(),
			PeerId:   g.Service().Peer().ID().Value(),
			Protocol: protocol,
		},
	})

	return data, nil
}
