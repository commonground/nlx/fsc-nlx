// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

//nolint:unused // most methods are unused but added to comply with the Manager interface
package internalservice_test

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type fakeFailingManager struct{}

func (m *fakeFailingManager) GetCertificates(_ context.Context) (*contract.PeerCertificates, error) {
	return nil, fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) GetPeers(_ context.Context, _ contract.PeersIDs) (contract.Peers, error) {
	return nil, fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) Announce(_ context.Context) error {
	return fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) GetContracts(_ context.Context, _ *contract.GrantType, _ *[]string) ([]*contract.Contract, error) {
	return nil, fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) SubmitContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) AcceptContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) RejectContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) RevokeContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) GetPeerInfo(_ context.Context) (*contract.Peer, error) {
	return nil, fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) GetServices(_ context.Context, _ *contract.PeerID, _ string) ([]*contract.Service, error) {
	return nil, fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) GetToken(ctx context.Context, _ contract.PeerID, grantHash string) (string, error) {
	return "", fmt.Errorf("arbitrary")
}

func (m *fakeFailingManager) GetTXLogRecords(_ context.Context) (contract.TXLogRecords, error) {
	return nil, fmt.Errorf("arbitrary")
}
