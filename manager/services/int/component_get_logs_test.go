// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestGetLogs(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	after := models.Timestamp(100)
	before := models.Timestamp(99)

	response, err := apiClient.GetLogsWithResponse(context.Background(), &models.GetLogsParams{
		Before: &before,
		After:  &after,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, response.StatusCode())
	assert.Len(t, response.JSON200.Records, 2)
	assert.Equal(t, "018f6287-8346-70e4-a681-2a6b4a1de600", response.JSON200.Records[0].TransactionId)
	assert.Equal(t, models.DIRECTIONINCOMING, response.JSON200.Records[0].Direction)
}

func TestGetInternalLogs(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	dataSourcePeerID := peerA.GetPeerID()

	response, err := apiClient.GetLogsWithResponse(context.Background(), &models.GetLogsParams{

		DataSourcePeerId: &dataSourcePeerID,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, response.StatusCode())
	assert.Len(t, response.JSON200.Records, 2)
	assert.Equal(t, "21b77914-7147-4f83-8b46-193a3d5a4093", response.JSON200.Records[0].TransactionId)
	assert.Equal(t, models.DIRECTIONINCOMING, response.JSON200.Records[0].Direction)
}

//nolint:dupl // tests look similar but are different
func TestGetInternalLogsInvalidDataSourcePeerID(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	dataSourcePeerID := "invalid peer id"

	response, err := apiClient.GetLogsWithResponse(context.Background(), &models.GetLogsParams{
		DataSourcePeerId: &dataSourcePeerID,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusInternalServerError, response.StatusCode())
	assert.Equal(t, "peerID cannot be smaller than 20 characters\ninvalid peer ID in args", response.ApplicationproblemJSON500.Details)
}

//nolint:dupl // tests look similar but are different
func TestGetLogsWithInvalidCursor(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	cursor := "invalid cursor"
	limit := models.QueryPaginationLimit(2)
	sortOrder := models.SORTORDERDESCENDING

	response, err := apiClient.GetLogsWithResponse(context.Background(), &models.GetLogsParams{
		Limit:     &limit,
		SortOrder: &sortOrder,
		Cursor:    &cursor,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusInternalServerError, response.StatusCode())
	assert.Equal(t, "invalid startID in pagination: uuid: incorrect UUID length 14 in string \"invalid cursor\"\ninvalid request in transcation log records handler", response.ApplicationproblemJSON500.Details)
}

func TestGetLogsWithInvalidTransactionID(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	response, err := apiClient.GetLogsWithResponse(context.Background(), &models.GetLogsParams{
		TransactionId: &[]models.Uuid{"015e7592-66dd-4414-98f1-28f13b19f67d"},
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusInternalServerError, response.StatusCode())
	assert.Equal(t, "invalid uuid version, must be v7, got v4\ninvalid transaction ID in filter\ninvalid request in transcation log records handler", response.ApplicationproblemJSON500.Details)
}
