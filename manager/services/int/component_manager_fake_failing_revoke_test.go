// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

//nolint:unused // most methods are unused but added to comply with the Manager interface
package internalservice_test

import (
	"context"
	"errors"
	"log"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type fakeFailingRevokeManager struct {
	address string
	orgCert *tls.CertificateBundle
	clock   clock.Clock
}

func (m *fakeFailingRevokeManager) GetCertificates(_ context.Context) (*contract.PeerCertificates, error) {
	return nil, nil
}

// nolint:dupl // it is intentional to keep these implementations duplicate
func (m *fakeFailingRevokeManager) GetPeers(_ context.Context, peerIDsFilter contract.PeersIDs) (contract.Peers, error) {
	orgAPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerA.GetPeerID(),
		Name:           peerA.GetName(),
		ManagerAddress: peerA.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	orgBPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerB.GetPeerID(),
		Name:           peerB.GetName(),
		ManagerAddress: peerB.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	orgCPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerC.GetPeerID(),
		Name:           peerC.GetName(),
		ManagerAddress: peerC.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	orgDPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerD.GetPeerID(),
		Name:           peerD.GetName(),
		ManagerAddress: peerD.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	peerIDs := contract.Peers{
		orgAPeer.ID(): orgAPeer,
		orgBPeer.ID(): orgBPeer,
		orgCPeer.ID(): orgCPeer,
		orgDPeer.ID(): orgDPeer,
	}

	if len(peerIDsFilter) == 0 {
		return peerIDs, nil
	}

	result := contract.Peers{}

	for peerID := range peerIDsFilter {
		peer, ok := peerIDs[peerID]
		if !ok {
			continue
		}

		result[peerID] = peer
	}

	return result, nil
}

func (m *fakeFailingRevokeManager) Announce(_ context.Context) error {
	return nil
}

func (m *fakeFailingRevokeManager) GetContracts(_ context.Context, _ *contract.GrantType, _ *[]string) ([]*contract.Contract, error) {
	return nil, nil
}

func (m *fakeFailingRevokeManager) SubmitContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeFailingRevokeManager) AcceptContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeFailingRevokeManager) RejectContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeFailingRevokeManager) RevokeContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return errors.New("arbitrary error for revoke contract")
}

func (m *fakeFailingRevokeManager) GetPeerInfo(_ context.Context) (*contract.Peer, error) {
	return nil, nil
}

func (m *fakeFailingRevokeManager) GetServices(_ context.Context, _ *contract.PeerID, _ string) ([]*contract.Service, error) {
	return nil, nil
}

func (m *fakeFailingRevokeManager) GetToken(context.Context, string) (string, error) {
	return "", nil
}

func (m *fakeFailingRevokeManager) GetTXLogRecords(_ context.Context) (contract.TXLogRecords, error) {
	return nil, nil
}
