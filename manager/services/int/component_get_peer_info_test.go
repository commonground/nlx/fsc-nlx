// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetPeerInfo(t *testing.T) {
	t.Parallel()

	internalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		internalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(internalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	response, err := apiClient.GetPeerInfoWithResponse(context.Background())
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, response.StatusCode())
	assert.Equal(t, "00000000000000000001", response.JSON200.Peer.Id)
	assert.Equal(t, "nlx-test-a", response.JSON200.Peer.Name)
	assert.Equal(t, "", response.JSON200.Peer.ManagerAddress)
}
