// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:funlen,dupl // this is a test
package internalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

func TestListOutgoingConnectionsForDelegatedService_Response(t *testing.T) {
	t.Parallel()

	notBefore := testClock.Now().Add(-1 * time.Hour).Truncate(time.Second)
	notAfter := testClock.Now().Add(time.Hour).Truncate(time.Second)
	createdAt := testClock.Now().Truncate(time.Second)

	iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractHash, createErr := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: notBefore,
		ContractNotAfter:  notAfter,
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewDelegatedServiceArgs{
					Name:                       "parkeerrechten",
					PeerID:                     peerA.GetPeerID(),
					PublicationDelegatorPeerID: peerB.GetPeerID(),
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, createErr)

	_, createErr = app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: notBefore,
		ContractNotAfter:  notAfter,
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewDelegatedServiceArgs{
					Name:                       "brp",
					PeerID:                     peerA.GetPeerID(),
					PublicationDelegatorPeerID: peerB.GetPeerID(),
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, createErr)

	res, err := apiClient.ListOutgoingConnectionsForDelegatedServiceWithResponse(context.Background(), peerA.GetPeerID(), "parkeerrechten", peerB.GetPeerID(), &models.ListOutgoingConnectionsForDelegatedServiceParams{})

	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}

	assert.Len(t, res.JSON200.Connections, 1)

	c := res.JSON200.Connections[0]

	serviceConnectionGrantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
		ContractContentIV:                 iv.String(),
		ContractGroupID:                   "test-group",
		ContractHashAlgorithm:             1,
		OutwayPeerID:                      peerA.GetPeerID(),
		OutwayCertificateThumbprint:       peerA.CertBundle.PublicKeyThumbprint(),
		ServicePeerID:                     peerA.GetPeerID(),
		ServiceName:                       "parkeerrechten",
		ServicePublicationDelegatorPeerID: peerB.GetPeerID(),
	})
	require.NoError(t, err)

	state := models.CONTRACTSTATEPROPOSED

	servicePublicationDelegatorPeerID := peerB.GetPeerID()

	want := models.Connection{
		ContractHash:                      contractHash,
		CreatedAt:                         createdAt.Unix(),
		DelegatorPeerId:                   nil,
		GrantHash:                         serviceConnectionGrantHash,
		NotAfter:                          notAfter.Unix(),
		NotBefore:                         notBefore.Unix(),
		OutwayPeerId:                      peerA.GetPeerID(),
		OutwayPublicKeyThumbprint:         peerA.CertBundle.PublicKeyThumbprint(),
		ServiceName:                       "parkeerrechten",
		ServicePeerId:                     peerA.GetPeerID(),
		ServicePublicationDelegatorPeerId: &servicePublicationDelegatorPeerID,
		State:                             &state,
	}

	assert.Equal(t, want, c)
}
