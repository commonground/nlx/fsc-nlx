// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestListPeers(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	err = intApp.Commands.CreatePeer.Handle(context.Background(), &command.CreatePeerArgs{
		PeerID:                peerDirectory.GetPeerID(),
		PeerName:              peerDirectory.GetName(),
		ManagerAddress:        peerDirectory.ManagerAddress,
		Roles:                 []command.PeerRole{command.PeerRoleDirectory},
		AuditLogSource:        source,
		AuthData:              authData,
		AuditlogCorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
	})
	require.NoError(t, err)

	err = intApp.Commands.CreatePeer.Handle(context.Background(), &command.CreatePeerArgs{
		PeerID:                peerA.GetPeerID(),
		PeerName:              "Local name Organization A",
		Roles:                 []command.PeerRole{},
		ManagerAddress:        "https://local-manager-address-org-a:8443",
		AuditLogSource:        source,
		AuthData:              authData,
		AuditlogCorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
	})
	require.NoError(t, err)

	err = intApp.Commands.CreatePeer.Handle(context.Background(), &command.CreatePeerArgs{
		PeerID:                "00000000000000000042",
		PeerName:              "Test Organization",
		ManagerAddress:        "https://example:8443",
		AuditLogSource:        source,
		AuthData:              authData,
		AuditlogCorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
	})
	require.NoError(t, err)

	tests := map[string]struct {
		Params *models.ListPeersParams
		Want   []models.Peer
	}{
		"filter_by_id": {
			Params: &models.ListPeersParams{
				PeerIds: func() *[]models.PeerID {
					result := []models.PeerID{
						peerA.GetPeerID(),
					}
					return &result
				}(),
			},
			Want: []models.Peer{
				{
					Id:             peerA.GetPeerID(),
					Name:           "Local name Organization A",
					ManagerAddress: "https://local-manager-address-org-a:8443",
					Roles:          []models.PeerRole{},
				},
			},
		},
		"happy_flow": {
			Want: []models.Peer{
				{
					Id:             peerA.GetPeerID(),
					Name:           "Local name Organization A",
					ManagerAddress: "https://local-manager-address-org-a:8443",
					Roles:          []models.PeerRole{},
				},
				{
					Id:             peerDirectory.GetPeerID(),
					Name:           peerDirectory.GetName(),
					ManagerAddress: peerDirectory.ManagerAddress,
					Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
				},
				{
					Id:             "00000000000000000042",
					Name:           "Test Organization",
					ManagerAddress: "https://example:8443",
					Roles:          []models.PeerRole{},
				},
			},
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			// Act
			res, err := apiClient.ListPeersWithResponse(context.Background(), test.Params)

			// Assert
			require.NoError(t, err)
			require.Equal(t, http.StatusOK, res.StatusCode())
			require.Len(t, res.JSON200.Peers, len(test.Want))
			require.Equal(t, test.Want, res.JSON200.Peers)
		})
	}
}
