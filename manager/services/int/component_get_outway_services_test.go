// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestGetOutwayServices(t *testing.T) {
	t.Parallel()

	// Arrange
	internalHTTPServer, internalApp, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		internalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(internalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractContentHash, err := internalApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                "102635ef-7053-4151-8fd1-2537f99bee79",
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					PeerID: peerA.GetPeerID(),
					Name:   "parkeerrechten",
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	contracts, err := internalApp.Queries.GetContracts.Handle(context.Background(), &query.ListContractsHandlerArgs{
		ContentHashes: []string{
			contractContentHash,
		},
	})
	require.NoError(t, err)

	require.Len(t, contracts, 1)
	require.Len(t, contracts[0].Content().Grants().ServiceConnectionGrants(), 1)

	serviceConnectionGrantHash := contracts[0].Content().Grants().ServiceConnectionGrants()[0].Hash().String()

	// Act
	response, err := apiClient.GetOutwayServicesWithResponse(context.Background(), peerA.CertBundle.PublicKeyThumbprint())
	assert.NoError(t, err)

	// Assert
	if !assert.Equal(t, http.StatusOK, response.StatusCode()) {
		t.Errorf("response body: %s", response.Body)
	}

	assert.Len(t, response.JSON200.Services, 1)
	assert.NotNil(t, response.JSON200.Services[serviceConnectionGrantHash])
	assert.Equal(t, "parkeerrechten", response.JSON200.Services[serviceConnectionGrantHash].Name)
	assert.Equal(t, peerA.GetPeerID(), response.JSON200.Services[serviceConnectionGrantHash].PeerId)
}
