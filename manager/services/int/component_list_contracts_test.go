// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

// nolint:funlen // this is a test
package internalservice_test

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// nolint:dupl // this is a test
func TestListContracts_Response(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Want              []models.Contract
	}{
		"service_conn_grant": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79").String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServiceConnectionArgs{
								OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
								OutwayPeerID:              peerA.GetPeerID(),
								Service: &command.NewServiceArgs{
									Name:   "parkeerrechten",
									PeerID: peerB.GetPeerID(),
								},
							},
						},
						CreatedAt:      now,
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Want: func() []models.Contract {
				service := models.GrantServiceConnection_Service{}

				err := service.FromService(models.Service{
					Name: "parkeerrechten",
					Peer: models.Peer{
						Id: peerB.GetPeerID(),
					},
				})
				assert.NoError(t, err)

				grant := models.Grant{}

				err = grant.FromGrantServiceConnection(models.GrantServiceConnection{
					Outway: models.OutwayPeer{
						PeerId:              peerA.GetPeerID(),
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
					},
					Service: service,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								grant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Peers: []string{
							peerA.GetPeerID(),
							peerB.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
		"service_conn_grant_with_delegator": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79").String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServiceConnectionArgs{
								OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
								OutwayPeerID:              peerA.GetPeerID(),
								Service: &command.NewDelegatedServiceArgs{
									Name:                       "parkeerrechten",
									PeerID:                     peerB.GetPeerID(),
									PublicationDelegatorPeerID: peerC.GetPeerID(),
								},
							},
						},
						CreatedAt:      now,
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Want: func() []models.Contract {
				service := models.GrantServiceConnection_Service{}

				err := service.FromDelegatedService(models.DelegatedService{
					Delegator: models.Peer{
						Id: peerC.GetPeerID(),
					},
					Name: "parkeerrechten",
					Peer: models.Peer{
						Id: peerB.GetPeerID(),
					},
				})
				assert.NoError(t, err)

				grant := models.Grant{}

				err = grant.FromGrantServiceConnection(models.GrantServiceConnection{
					Outway: models.OutwayPeer{
						PeerId:              peerA.GetPeerID(),
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
					},
					Service: service,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								grant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Peers: []string{
							peerA.GetPeerID(),
							peerB.GetPeerID(),
							peerC.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
		"del_service_conn_grant_w_delegator": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79").String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantDelegatedServiceConnectionArgs{
								OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
								OutwayPeerID:              peerA.GetPeerID(),
								Service: &command.NewDelegatedServiceArgs{
									Name:                       "parkeerrechten",
									PeerID:                     peerB.GetPeerID(),
									PublicationDelegatorPeerID: peerC.GetPeerID(),
								},
								DelegatorPeerID: peerD.GetPeerID(),
							},
						},
						CreatedAt:      now,
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Want: func() []models.Contract {
				service := models.GrantDelegatedServiceConnection_Service{}

				err := service.FromDelegatedService(models.DelegatedService{
					Delegator: models.Peer{
						Id: peerC.GetPeerID(),
					},
					Name: "parkeerrechten",
					Peer: models.Peer{
						Id: peerB.GetPeerID(),
					},
				})
				assert.NoError(t, err)

				grant := models.Grant{}

				err = grant.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
					Delegator: models.DelegatorPeer{
						PeerId: peerD.GetPeerID(),
					},
					Outway: models.OutwayPeer{
						PeerId:              peerA.GetPeerID(),
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
					},
					Service: service,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								grant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Peers: []string{
							peerA.GetPeerID(),
							peerB.GetPeerID(),
							peerC.GetPeerID(),
							peerD.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
		"del_service_pub_grant": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee80").String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantDelegatedServicePublicationArgs{
								DelegatorPeerID: peerB.GetPeerID(),
								DirectoryPeerID: peerDirectory.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP2,
							},
						},
						CreatedAt:      now,
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Want: func() []models.Contract {
				grant := models.Grant{}

				err := grant.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
					Delegator: models.DelegatorPeer{
						PeerId: peerB.GetPeerID(),
					},
					Directory: models.DirectoryPeer{
						PeerId: peerDirectory.GetPeerID(),
					},
					Service: models.ServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP2,
					},
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								grant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee80",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Peers: []string{
							peerA.GetPeerID(),
							peerB.GetPeerID(),
							peerDirectory.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79").String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt:      now,
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Want: func() []models.Contract {
				grant := models.Grant{}

				err := grant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublication{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								grant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Peers: []string{
							peerA.GetPeerID(),
							peerC.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
	}
	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	for testName, tc := range tests {
		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			contentHashParams := make([]models.ContentHash, 0)

			for _, createContractArgs := range tc.ContractsToCreate {
				createdContractContentHash, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)

				contentHashParams = append(contentHashParams, createdContractContentHash)
			}

			params := &models.GetContractsParams{ContentHash: &contentHashParams}

			res, err := apiClient.GetContractsWithResponse(context.Background(), params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(tc.Want))

			for i, pendingContract := range res.JSON200.Contracts {
				assert.NotEmpty(t, pendingContract.Hash)
				assert.Equal(t, tc.Want[i].Content.Iv, pendingContract.Content.Iv)
				assert.Equal(t, tc.Want[i].Content.CreatedAt, pendingContract.Content.CreatedAt)
				assert.Len(t, pendingContract.Content.Grants, len(tc.Want[i].Content.Grants))
				assert.Equal(t, tc.Want[i].Content.GroupId, pendingContract.Content.GroupId)
				assert.Equal(t, tc.Want[i].Content.HashAlgorithm, pendingContract.Content.HashAlgorithm)
				assert.Equal(t, tc.Want[i].Content.Validity, pendingContract.Content.Validity)
				assert.Equal(t, tc.Want[i].Peers, pendingContract.Peers)
				assert.Equal(t, tc.Want[i].Signatures, pendingContract.Signatures)
			}
		})
	}
}

func TestListContracts_DefaultOrdering(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
		t:           t,
	})

	idA := contractService.NewServiceConnectionWithPeerCreatedAt(peerB, peerA, now.Truncate(time.Second).Add(-5*time.Minute))
	idB := contractService.NewServiceConnectionWithPeerCreatedAt(peerB, peerA, now.Truncate(time.Second))
	idC := contractService.NewServiceConnectionWithPeerCreatedAt(peerB, peerA, now.Truncate(time.Second).Add(-48*time.Hour))

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	res, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{})
	assert.NoError(t, err)

	require.Equal(t, http.StatusOK, res.StatusCode())
	require.Len(t, res.JSON200.Contracts, 3)
	assert.Equal(t, idB.String(), res.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, idA.String(), res.JSON200.Contracts[1].Content.Iv)
	assert.Equal(t, idC.String(), res.JSON200.Contracts[2].Content.Iv)
}

func TestListContracts_FilterByGrantType(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
		t:           t,
	})

	idA := contractService.NewServiceConnectionWithPeerCreatedAt(peerB, peerA, now.Truncate(time.Second).Add(-48*time.Hour))
	contractService.NewDelegatedServiceConnectionWithCreatedAt(now.Truncate(time.Second))
	idC := contractService.NewServiceConnectionWithPeerCreatedAt(peerB, peerA, now.Truncate(time.Second).Add(-5*time.Minute))
	idD := contractService.NewServiceConnectionWithPeerCreatedAt(peerB, peerA, now.Truncate(time.Second))

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	res, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantType: &[]models.GrantType{
			models.GRANTTYPESERVICECONNECTION,
		},
	})
	require.NoError(t, err)

	require.Equal(t, http.StatusOK, res.StatusCode())
	require.Len(t, res.JSON200.Contracts, 3)
	assert.Equal(t, idD.String(), res.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, idC.String(), res.JSON200.Contracts[1].Content.Iv)
	assert.Equal(t, idA.String(), res.JSON200.Contracts[2].Content.Iv)
}

func TestListContracts_FilterByContentHash(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
		t:           t,
	})

	idA := uuid.MustParse("94ed0235-c25c-44aa-a131-45a2fc743adf")
	idB := uuid.MustParse("6b14f7ac-13d1-4ccd-8a57-fc127beef9bf")
	idC := uuid.MustParse("7e8d9ad5-09ed-4fba-8c9d-960bf2817009")

	_ = contractService.NewServiceConnectionWithPeerCreatedAtAndID(peerB, peerA, now.Truncate(time.Second), idA)
	contractContentHashB := contractService.NewServiceConnectionWithPeerCreatedAtAndID(peerB, peerA, now.Truncate(time.Second), idB)
	_ = contractService.NewServiceConnectionWithPeerCreatedAtAndID(peerB, peerA, now.Truncate(time.Second), idC)

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	res, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		ContentHash: &[]string{contractContentHashB},
	})
	assert.NoError(t, err)

	require.Equal(t, http.StatusOK, res.StatusCode())
	require.Len(t, res.JSON200.Contracts, 1)
	assert.Equal(t, idB.String(), res.JSON200.Contracts[0].Content.Iv)
}

func TestListContracts_FilterByGrantHash(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
		t:           t,
	})

	idA := uuid.MustParse("94ed0235-c25c-44aa-a131-45a2fc743adf")

	contractContentHash := contractService.NewServiceConnectionWithPeerCreatedAtAndID(peerB, peerA, now.Truncate(time.Second), idA)

	contracts, err := app.Queries.GetContracts.Handle(context.Background(), &query.ListContractsHandlerArgs{
		ContentHashes: []string{contractContentHash},
	})
	require.NoError(t, err)
	require.Len(t, contracts, 1)

	res, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantHash: &[]string{contracts[0].Content().Grants().ServiceConnectionGrants()[0].Hash().String()},
	})
	assert.NoError(t, err)

	require.Equal(t, http.StatusOK, res.StatusCode())
	require.Len(t, res.JSON200.Contracts, 1)
	assert.Equal(t, idA.String(), res.JSON200.Contracts[0].Content.Iv)
}

func TestListContracts_Pagination(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
		t:           t,
	})

	idA := uuid.MustParse("94ed0235-c25c-44aa-a131-45a2fc743adf")
	idB := uuid.MustParse("6b14f7ac-13d1-4ccd-8a57-fc127beef9bf")
	idC := uuid.MustParse("7e8d9ad5-09ed-4fba-8c9d-960bf2817009")
	idD := uuid.MustParse("4ddef465-764c-413d-b64c-0b5b995f8c19")
	idE := uuid.MustParse("c82cfff0-6f71-438b-83c4-d7dac1b6d340")

	createdAtA := now.Truncate(time.Second).Add(-48 * time.Hour)
	createdAtB := now.Truncate(time.Second).Add(-10 * time.Minute)
	createdAtC := now.Truncate(time.Second).Add(-7 * time.Minute)
	createdAtD := now.Truncate(time.Second).Add(-7 * time.Minute)
	createdAtE := now.Truncate(time.Second).Add(-7 * time.Minute)

	_ = contractService.NewServiceConnectionWithPeerCreatedAtAndID(peerB, peerA, createdAtA, idA)
	hashB := contractService.NewServiceConnectionWithPeerCreatedAtAndID(peerB, peerA, createdAtB, idB)
	hashC := contractService.NewServiceConnectionWithPeerCreatedAtAndID(peerB, peerA, createdAtC, idC)
	hashD := contractService.NewServiceConnectionWithPeerCreatedAtAndID(peerB, peerA, createdAtD, idD)
	hashE := contractService.NewServiceConnectionWithPeerCreatedAtAndID(peerB, peerA, createdAtE, idE)

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	limit := models.QueryPaginationLimit(2)
	sortOrder := models.SORTORDERASCENDING

	resFirstPage, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		Limit:     &limit,
		SortOrder: &sortOrder,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, resFirstPage.StatusCode())
	assert.Len(t, resFirstPage.JSON200.Contracts, 2)
	assert.Equal(t, idA.String(), resFirstPage.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, idB.String(), resFirstPage.JSON200.Contracts[1].Content.Iv)

	assert.NotEmpty(t, resFirstPage.JSON200.Pagination.NextCursor)

	nextCursorStartDate, nextCursorHash, err := decodePaginationCursor(resFirstPage.JSON200.Pagination.NextCursor)
	require.NoError(t, err)

	assert.Equal(t, hashB, nextCursorHash)
	assert.Equal(t, createdAtB, nextCursorStartDate)

	limitSecondPage := models.QueryPaginationLimit(4)
	resSecondPage, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		Cursor:    &resFirstPage.JSON200.Pagination.NextCursor,
		Limit:     &limitSecondPage,
		SortOrder: &sortOrder,
		GrantType: &[]models.GrantType{
			models.GRANTTYPESERVICECONNECTION,
		},
	})
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, resSecondPage.StatusCode()) {
		t.Errorf("response body: %s", resSecondPage.Body)
	}

	contractContentHashMap := map[string]uuid.UUID{
		hashC: idC,
		hashD: idD,
		hashE: idE,
	}

	contractContentHashes := []string{hashC, hashD, hashE}

	sort.SliceStable(contractContentHashes, func(i, j int) bool {
		bsI := []byte(contractContentHashes[i])
		bsJ := []byte(contractContentHashes[j])

		return bytes.Compare(bsI, bsJ) < 0
	})

	expectedContractIDs := []string{}

	for _, contractContentHash := range contractContentHashes {
		contractID, ok := contractContentHashMap[contractContentHash]
		if !ok {
			t.Fatalf("contract content hash not found in map: %s", contractContentHash)
		}

		expectedContractIDs = append(expectedContractIDs, contractID.String())
	}

	actualContractIDs := []string{}

	for _, c := range resSecondPage.JSON200.Contracts {
		actualContractIDs = append(actualContractIDs, c.Content.Iv)
	}

	assert.Len(t, resSecondPage.JSON200.Contracts, 3)
	assert.Equal(t, expectedContractIDs, actualContractIDs, fmt.Sprintf("%s", contractContentHashes))
}

func decodePaginationCursor(input string) (time.Time, string, error) {
	decodedCursor, err := base64.RawURLEncoding.DecodeString(input)
	if err != nil {
		return time.Now(), "", errors.Wrapf(err, "could not decode cursor from base64 string: %s", input)
	}

	parts := strings.Split(string(decodedCursor), ",")

	const partsInCursor = 2

	if len(parts) != partsInCursor {
		return time.Now(), "", fmt.Errorf("cursor should consist of two parts. found %d", len(parts))
	}

	integerDatetime, err := strconv.Atoi(parts[0])
	if err != nil {
		return time.Now(), "", fmt.Errorf("datetime is not a valid integer: %q", parts[0])
	}

	return time.Unix(int64(integerDatetime), 0).UTC(), parts[1], nil
}

func TestListContracts_WhenPeerInfoIsNotPresent(t *testing.T) {
	t.Parallel()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
		t:           t,
	})

	peerIDNotPresentInDB := "00000000000000000042"

	contractService.NewWithCreatedAtAndPeerID(testClock.Now().Truncate(time.Second), peerIDNotPresentInDB)

	limit := models.QueryPaginationLimit(1)

	res, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		Limit: &limit,
	})
	require.NoError(t, err)

	require.Equal(t, http.StatusOK, res.StatusCode())
	require.Len(t, res.JSON200.Contracts, 1)

	assert.Len(t, res.JSON200.Contracts[0].Peers, 2)

	expectedPeers := []string{peerA.GetPeerID(), peerIDNotPresentInDB}
	assert.Equal(t, expectedPeers, res.JSON200.Contracts[0].Peers)
}
