// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"path/filepath"

	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

type peerInfo struct {
	CertBundle     *tls.CertificateBundle
	ManagerAddress string
}

func newPeerInfo(organisationName testingutils.CertificateBundlePeerName, managerAddress string) (*peerInfo, error) {
	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), organisationName)
	if err != nil {
		return nil, err
	}

	return &peerInfo{
		CertBundle:     orgCertBundle,
		ManagerAddress: managerAddress,
	}, nil
}

func (o *peerInfo) GetName() string {
	return o.CertBundle.GetPeerInfo().Name
}

func (o *peerInfo) GetPeerID() string {
	return o.CertBundle.GetPeerInfo().SerialNumber
}
