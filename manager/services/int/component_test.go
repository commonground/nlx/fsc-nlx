// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"net/netip"
	"os"
	"strings"
	"testing"
	"time"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres"
	externalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner/neverallow"
	restport "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	externalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/ext"
	internalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/int"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var nowInUTC = time.Now().UTC()

var (
	testClock     = testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	peerA         *peerInfo
	peerB         *peerInfo
	peerC         *peerInfo
	peerD         *peerInfo
	peerDirectory *peerInfo

	authData = &authentication.CertificateData{
		SubjectCommonName:   "test-subject",
		PublicKeyThumbprint: "test-fingerprint",
	}

	source = common_auditlog.SourceHTTP{
		IPAddress: netip.AddrFrom4([4]byte{127, 0, 0, 1}),
		UserAgent: "test-useragent",
	}
)

func TestMain(m *testing.M) {
	var err error

	peerA, err = newPeerInfo(testingutils.NLXTestPeerA, "https://manager.org-a.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerB, err = newPeerInfo(testingutils.NLXTestPeerB, "https://manager.org-b.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerC, err = newPeerInfo(testingutils.NLXTestPeerC, "https://manager.org-c.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerD, err = newPeerInfo(testingutils.NLXTestPeerD, "https://manager.org-d.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerDirectory, err = newPeerInfo(testingutils.NLXTestOrgDirectory, "https://directory.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

//nolint:funlen,gocyclo,gocritic // this is a test
func newService(testName string) (*httptest.Server, *internalapp.Application, *externalapp.Application, error) {
	return newServiceWithCustomManagerFactory(testName, &fakeManagerFactory{
		Clock: testClock,
	})
}

//nolint:funlen,gocritic // this is a long function because there is a lot to set up
func newServiceWithCustomManagerFactory(testName string, managerFactory manager.Factory) (*httptest.Server, *internalapp.Application, *externalapp.Application, error) {
	logger := discardlogger.New()

	peerCertBundle := peerA.CertBundle

	postgresqlRepository, err := setupPostgresqlRepository(testName)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to setup postgresql repository: %v", err)
	}

	selfPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerCertBundle.GetPeerInfo().SerialNumber,
		Name:           peerCertBundle.GetPeerInfo().Name,
		ManagerAddress: "https://manager.org-a.nlx.local:443",
	})
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to create self peer: %v", err)
	}

	controllerRepository := newFakeController()

	auditLogger, err := auditlog.New(&auditlog.NewArgs{
		AuditLogger: newFakeAuditlog(),
		GroupID:     "fsc-local",
		Clock:       testClock,
		IDGenerator: uuidgenerator.New(),
	})
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to create auditlogger: %v", err)
	}

	contractDistributionService, err := services.NewContractDistributionService(&services.NewContractDistributionServiceArgs{
		Context:                              context.Background(),
		Logger:                               logger,
		Clock:                                testClock,
		FailedContractDistributionRepository: postgresqlRepository,
	})
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to setup contract distribution service: %s", err)
	}

	peersCommunicationService, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
		Ctx:                         context.Background(),
		Logger:                      logger,
		LocalRepo:                   postgresqlRepository,
		ManagerFactory:              managerFactory,
		SelfPeerID:                  selfPeer.ID(),
		ContractDistributionService: contractDistributionService,
		Clock:                       testClock,
	})
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to setup peers adapter: %s", err)
	}

	txlogRepository := newFakeTxLog()

	internalApp, err := internalservice.NewApplication(&internalservice.NewApplicationArgs{
		Context:                     context.Background(),
		Clock:                       testClock,
		Logger:                      logger,
		Repository:                  postgresqlRepository,
		PeersCommunicationService:   peersCommunicationService,
		ContractDistributionService: contractDistributionService,
		TXLog:                       txlogRepository,
		SelfPeer:                    selfPeer,
		TrustedExternalRootCAs:      peerCertBundle.RootCAs(),
		SignatureCertificate:        peerCertBundle.Cert(),
		ControllerRepository:        controllerRepository,
		ManagerFactory:              managerFactory,
		GroupID:                     "fsc-local",
		DirectoryPeerManagerAddress: peerDirectory.ManagerAddress,
		DirectoryPeerID:             peerDirectory.GetPeerID(),
		DirectoryPeerName:           peerDirectory.GetName(),
		Auditlogger:                 auditLogger,
		InternalCertificate:         peerCertBundle,
	})
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to setup application: %s", err)
	}

	externalApp, err := externalservice.NewApplication(&externalservice.NewApplicationArgs{
		Context:                   context.Background(),
		Clock:                     testClock,
		Logger:                    logger,
		Repository:                postgresqlRepository,
		ControllerRepository:      controllerRepository,
		TXLogRepository:           txlogRepository,
		PeersCommunicationService: peersCommunicationService,
		GroupID:                   "fsc-local",
		SelfPeer:                  selfPeer,
		TrustedRootCAs:            peerCertBundle.RootCAs(),
		AutoSignCertificate:       peerCertBundle.Cert(),
		TokenSignCertificate:      peerCertBundle.Cert(),
		TokenTTL:                  time.Hour,
		IsTxlogDisabled:           true,
		AutoSigner:                neverallow.New(),
	})
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to setup external application: %s", err)
	}

	server, err := restport.New(&restport.NewArgs{
		Logger:     logger,
		App:        internalApp,
		Cert:       peerCertBundle,
		SelfPeerID: selfPeer.ID(),
		Clock:      testClock,
	})
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to setup rest port: %s", err)
	}

	srv := httptest.NewUnstartedServer(server.Handler())
	srv.TLS = peerCertBundle.TLSConfig(peerCertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv, internalApp, externalApp, nil
}

func createInternalManagerAPIClient(managerURL string, certBundle *tls.CertificateBundle) (*api.ClientWithResponses, error) {
	return api.NewClientWithResponses(managerURL, func(c *api.Client) error {
		t := &http.Transport{
			TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
		}
		c.Client = &http.Client{
			Transport: t,
		}

		return nil
	})
}

func setupPostgresqlRepository(testName string) (contract.Repository, error) {
	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)
	dbName = strings.Replace(dbName, "test", "", 1) // strip 'test'-prefix
	dbName = fmt.Sprintf("int_%s", dbName)
	dbName = strings.ReplaceAll(dbName, "/", "_")

	// via https://stackoverflow.com/a/27865772/363448
	const maxLengthDatabaseName = 63

	if len(dbName) > maxLengthDatabaseName {
		return nil, fmt.Errorf("database name too long (%d > 63 characters): %q", len(dbName), dbName)
	}

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		return nil, fmt.Errorf("failed to setup test database: %v", err)
	}

	db, err := postgresadapter.NewConnection(context.Background(), testDB)
	if err != nil {
		return nil, fmt.Errorf("failed to create db connection: %v", err)
	}

	err = postgresadapter.PerformMigrations(testDB)
	if err != nil {
		return nil, fmt.Errorf("failed to perform dbmigrations: %v", err)
	}

	postgresqlRepository, err := postgresadapter.New(peerA.CertBundle.RootCAs(), db, testClock)
	if err != nil {
		return nil, fmt.Errorf("failed to setup postgresql database: %s", err)
	}

	return postgresqlRepository, nil
}
