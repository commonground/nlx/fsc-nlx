// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// nolint:dupl // this is a test•
func TestRetryContractDistribution_Validation(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp, _, err := newServiceWithCustomManagerFactory(t.Name(), &fakeFailingManagerFactory{})
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	contractContentHash, err := intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerB.GetPeerID(),
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	// Assert
	var failedDistributions query.ListFailedContractDistributions

	expiration := 2 * time.Second
	timeStarted := time.Now()

	// since sending the Contract to the other organization happens in a go-routine, the distribution is
	// not executed immediately. So, we poll the failed distributions until we have one.
	for {
		result, err := intApp.Queries.GetContractDistributions.Handle(context.Background(), contractContentHash)
		require.NoError(t, err)

		if len(result) != 0 {
			failedDistributions = result
			break
		}

		shouldExpire := timeStarted.After(timeStarted.Add(expiration))

		if shouldExpire {
			require.Fail(t, fmt.Sprintf("forced failure, since test took longer than expected %q", expiration))
		}
	}

	require.Len(t, failedDistributions, 1)
	assert.Equal(t, query.DistributionActionSubmitContract, failedDistributions[0].Action)
	assert.Equal(t, contractContentHash, failedDistributions[0].ContractHash)

	res, err := apiClient.RetryContractDistributionWithResponse(context.Background(), "invalid-hash", peerB.GetPeerID(), models.DISTRIBUTIONACTIONSUBMITCONTRACT, nil)
	require.NoError(t, err)

	assert.Equal(t, http.StatusUnprocessableEntity, res.StatusCode())

	res, err = apiClient.RetryContractDistributionWithResponse(context.Background(), contractContentHash, "invalid-peer-id", models.DISTRIBUTIONACTIONSUBMITCONTRACT, nil)
	require.NoError(t, err)

	assert.Equal(t, http.StatusUnprocessableEntity, res.StatusCode())

	res, err = apiClient.RetryContractDistributionWithResponse(context.Background(), contractContentHash, peerB.GetPeerID(), "invalid-action", nil)
	require.NoError(t, err)

	assert.Equal(t, http.StatusUnprocessableEntity, res.StatusCode())
}

// nolint:dupl // this is a test•
func TestRetryContractDistribution(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp, _, err := newServiceWithCustomManagerFactory(t.Name(), &fakeFailingManagerFactory{})
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	contractContentHash, err := intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerB.GetPeerID(),
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	// Assert
	var failedDistributions query.ListFailedContractDistributions

	expiration := 2 * time.Second
	timeStarted := time.Now()

	// since sending the Contract to the other organization happens in a go-routine, the distribution is
	// not executed immediately. So, we poll the failed distributions until we have one.
	for {
		result, err := intApp.Queries.GetContractDistributions.Handle(context.Background(), contractContentHash)
		require.NoError(t, err)

		if len(result) != 0 {
			failedDistributions = result
			break
		}

		shouldExpire := timeStarted.After(timeStarted.Add(expiration))

		if shouldExpire {
			require.Fail(t, fmt.Sprintf("forced failure, since test took longer than expected %q", expiration))
		}
	}

	require.Len(t, failedDistributions, 1)
	assert.Equal(t, query.DistributionActionSubmitContract, failedDistributions[0].Action)
	assert.Equal(t, contractContentHash, failedDistributions[0].ContractHash)

	res, err := apiClient.RetryContractDistributionWithResponse(context.Background(), contractContentHash, peerB.GetPeerID(), models.DISTRIBUTIONACTIONSUBMITCONTRACT, nil)
	require.NoError(t, err)

	assert.Equal(t, http.StatusCreated, res.StatusCode())
	assert.Equal(t, []string{fmt.Sprintf("</api/v1/contracts/%s/distributions>; rel=\"status\"", contractContentHash)}, res.HTTPResponse.Header["Link"])
}

// nolint:dupl // this is a test
func TestRetryContractDistribution_RevokeSignature(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp, _, err := newServiceWithCustomManagerFactory(t.Name(), &fakeFailingRevokeManagerFactory{})
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	contractContentHash, err := intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerB.GetPeerID(),
				},
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	err = intApp.Commands.RevokeContract.Handle(context.Background(), &command.RevokeContractHandlerArgs{
		ContentHash:    contractContentHash,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	// Assert
	var failedDistributions query.ListFailedContractDistributions

	expiration := 2 * time.Second
	timeStarted := time.Now()

	// since sending the Signature to the other organization happens in a go-routine, the distribution is
	// not executed immediately. So, we poll the failed distributions until we have one.
	for {
		result, err := intApp.Queries.GetContractDistributions.Handle(context.Background(), contractContentHash)
		require.NoError(t, err)

		if len(result) != 0 {
			failedDistributions = result
			break
		}

		shouldExpire := timeStarted.After(timeStarted.Add(expiration))

		if shouldExpire {
			require.Fail(t, fmt.Sprintf("forced failure, since test took longer than expected %q", expiration))
		}
	}

	require.Len(t, failedDistributions, 1)
	assert.Equal(t, query.DistributionActionSubmitRevokeSignature, failedDistributions[0].Action)
	assert.Equal(t, contractContentHash, failedDistributions[0].ContractHash)

	res, err := apiClient.RetryContractDistributionWithResponse(context.Background(), contractContentHash, peerB.GetPeerID(), models.DISTRIBUTIONACTIONSUBMITREVOKESIGNATURE, nil)
	require.NoError(t, err)

	assert.Equal(t, http.StatusCreated, res.StatusCode())
	assert.Equal(t, []string{fmt.Sprintf("</api/v1/contracts/%s/distributions>; rel=\"status\"", contractContentHash)}, res.HTTPResponse.Header["Link"])
}
