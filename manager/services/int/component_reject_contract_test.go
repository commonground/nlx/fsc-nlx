// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:dupl // similar tests
package internalservice_test

import (
	"context"
	"io"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestRejectContract(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	iv := uuid.New()

	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: now.Truncate(time.Second),
		ContractNotAfter:  now.Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerB.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      now.Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	}

	contractContentHash, err := app.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(t, err)

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	res, err := apiClient.RejectContract(context.Background(), contractContentHash, nil)
	assert.NoError(t, err)

	defer res.Body.Close()

	if !assert.Equal(t, http.StatusNoContent, res.StatusCode) {
		responseBody, err := io.ReadAll(res.Body)
		assert.NoError(t, err)

		t.Logf("response body: %s", responseBody)
	}
}
