// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

//nolint:funlen,dupl // this is a test
func TestSubmitContract(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	client, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	require.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	require.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerB.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	require.NoError(t, err)

	cc, err := contractContentToAPIModel(contractContent)
	require.NoError(t, err)

	idGenerator := uuidgenerator.New()

	auditLogCorrelationID, err := idGenerator.New()
	require.NoError(t, err)

	// Act
	createContractResp, err := client.CreateContractWithResponse(context.Background(), &models.CreateContractParams{
		AuditlogCorrelationId: &auditLogCorrelationID,
	}, models.CreateContractJSONRequestBody(
		models.CreateContractJSONBody{
			ContractContent: *cc,
		}),
	)

	// Assert
	require.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, createContractResp.StatusCode()) {
		t.Errorf("response body: %s", createContractResp.Body)
	}

	contentHashFilter := []models.ContentHash{
		contractContent.Hash().String(),
	}

	contracts, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		Cursor:      nil,
		Limit:       nil,
		SortOrder:   nil,
		GrantType:   nil,
		ContentHash: &contentHashFilter,
	})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(contracts.JSON200.Contracts))
}

func TestSubmitContractToAPeerNotInPeerRepository(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	client, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		Clock:         testClock,
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now().Truncate(time.Second),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now().Truncate(time.Second),
			NotAfter:  time.Now().Truncate(time.Second),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "12345678901234567888",
						Name:           "",
						ManagerAddress: "",
					},
					PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	cc, err := contractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	idGenerator := uuidgenerator.New()

	auditLogCorrelationID, err := idGenerator.New()
	assert.NoError(t, err)

	// Act
	createContractResp, err := client.CreateContractWithResponse(context.Background(), &models.CreateContractParams{
		AuditlogCorrelationId: &auditLogCorrelationID,
	}, models.CreateContractJSONRequestBody(
		models.CreateContractJSONBody{
			ContractContent: *cc,
		}),
	)

	// Assert
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, createContractResp.StatusCode()) {
		t.Errorf("response body: %s", createContractResp.Body)
	}

	contentHashFilter := []models.ContentHash{
		contractContent.Hash().String(),
	}

	contracts, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		Cursor:      nil,
		Limit:       nil,
		SortOrder:   nil,
		GrantType:   nil,
		ContentHash: &contentHashFilter,
	})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(contracts.JSON200.Contracts))
}

//nolint:funlen,dupl // this is a test
func TestSuiteSubmitContact(t *testing.T) {
	t.Parallel()

	idGenerator := uuidgenerator.New()

	auditLogCorrelationID, idGeneratorErr := idGenerator.New()
	assert.NoError(t, idGeneratorErr)

	testCases := map[string]struct {
		Request        *models.CreateContractJSONRequestBody
		WantStatusCode int
	}{
		"with_a_service_connection_grant": {
			Request: func() *models.CreateContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantServiceConnectionServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								Name: "parkeerrechten",
							},
						},
					},
				})
				assert.NoError(t, err)

				cc, err := contractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.CreateContractJSONRequestBody{
					ContractContent: *cc,
				}
			}(),
			WantStatusCode: http.StatusCreated,
		},
		"with_a_service_publication_grant": {
			Request: func() *models.CreateContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServicePublicationArgs{
							Directory: &contract.NewGrantServicePublicationDirectoryArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           peerA.GetName(),
									ManagerAddress: peerA.ManagerAddress,
								},
							},
							Service: &contract.NewGrantServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           peerB.GetName(),
									ManagerAddress: peerB.ManagerAddress,
								},
								Name:     "parkeerrechten",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
					},
				})
				assert.NoError(t, err)

				cc, err := contractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.CreateContractJSONRequestBody{
					ContractContent: *cc,
				}
			}(),
			WantStatusCode: http.StatusCreated,
		},
		"with_a_delegated_service_publication_grant": {
			Request: func() *models.CreateContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantDelegatedServicePublicationArgs{
							Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{Peer: &contract.NewPeerArgs{
								ID:             peerA.GetPeerID(),
								Name:           peerA.GetName(),
								ManagerAddress: peerA.ManagerAddress,
							}},
							Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           peerB.GetName(),
									ManagerAddress: peerB.ManagerAddress,
								},
								Name:     "parkeerrechten",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
							Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerC.GetPeerID(),
									Name:           peerC.GetName(),
									ManagerAddress: peerC.ManagerAddress,
								},
							},
						},
					},
				})
				assert.NoError(t, err)

				cc, err := contractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.CreateContractJSONRequestBody{
					ContractContent: *cc,
				}
			}(),
			WantStatusCode: http.StatusCreated,
		},
		"with_a_delegated_service_connection_grant": {
			Request: func() *models.CreateContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantDelegatedServiceConnectionArgs{
							Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           peerA.GetName(),
									ManagerAddress: peerA.ManagerAddress,
								},
								PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           peerB.GetName(),
									ManagerAddress: peerB.ManagerAddress,
								},
								Name: "parkeerrechten",
							},
							Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerC.GetPeerID(),
									Name:           peerC.GetName(),
									ManagerAddress: peerC.ManagerAddress,
								},
							},
						},
					},
				})
				assert.NoError(t, err)

				cc, err := contractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.CreateContractJSONRequestBody{
					ContractContent: *cc,
				}
			}(),
			WantStatusCode: http.StatusCreated,
		},
		"with_a_service_service_connection_grant_containing_a_delegated_service": {
			Request: func() *models.CreateContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								PublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           "",
									ManagerAddress: "",
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: peerC.GetPeerID(),
								},
							},
						},
					},
				})
				assert.NoError(t, err)

				cc, err := contractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.CreateContractJSONRequestBody{
					ContractContent: *cc,
				}
			}(),
			WantStatusCode: http.StatusCreated,
		},
		"with_a_delegated_service_connection_grant_containing_a_delegated_service": {
			Request: func() *models.CreateContractJSONRequestBody {
				contractIV, err := uuid.New().MarshalBinary()
				assert.NoError(t, err)

				contractContent, err := contract.NewContent(&contract.NewContentArgs{
					Clock:         testClock,
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            contractIV,
					CreatedAt:     testClock.Now().Truncate(time.Second),
					GroupID:       "fsc-local",
					Validity: &contract.NewValidityArgs{
						NotBefore: testClock.Now().Truncate(time.Second),
						NotAfter:  testClock.Now().Truncate(time.Second),
					},
					Grants: []interface{}{
						&contract.NewGrantDelegatedServiceConnectionArgs{
							Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerA.GetPeerID(),
									Name:           peerA.GetName(),
									ManagerAddress: peerA.ManagerAddress,
								},
								PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantDelegatedServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerB.GetPeerID(),
									Name:           peerB.GetName(),
									ManagerAddress: peerB.ManagerAddress,
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: peerD.GetPeerID(),
								},
							},
							Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID:             peerC.GetPeerID(),
									Name:           peerC.GetName(),
									ManagerAddress: peerC.ManagerAddress,
								},
							},
						},
					},
				})
				assert.NoError(t, err)

				cc, err := contractContentToAPIModel(contractContent)
				assert.NoError(t, err)

				return &models.CreateContractJSONRequestBody{
					ContractContent: *cc,
				}
			}(),
			WantStatusCode: http.StatusCreated,
		},
	}
	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	client, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	for name, tc := range testCases {
		testCase := tc

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			resp, errSubmit := client.CreateContractWithResponse(context.Background(), &models.CreateContractParams{
				AuditlogCorrelationId: &auditLogCorrelationID,
			}, *testCase.Request)
			assert.NoError(t, errSubmit)

			if !assert.Equal(t, testCase.WantStatusCode, resp.StatusCode()) {
				t.Errorf("response body: %s", resp.Body)
			}
		})
	}
}
