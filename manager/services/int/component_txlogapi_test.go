// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type fakeTxLogAPI struct{}

func newFakeTxLog() txlog.TXLog {
	return &fakeTxLogAPI{}
}

func (t *fakeTxLogAPI) ListRecords(_ context.Context, _ *txlog.ListRecordsRequest) ([]*txlog.Record, error) {
	return []*txlog.Record{
		{
			TransactionID: "018f6287-8346-70e4-a681-2a6b4a1de600",
			GroupID:       "test-group",
			GrantHash:     "",
			ServiceName:   "",
			Direction:     record.DirectionIn,
			Source: &txlog.RecordSource{
				OutwayPeerID: "00000000000000000000",
			},
			Destination: &txlog.RecordDestination{
				ServicePeerID: "00000000000000000001",
			},
			CreatedAt: time.Time{},
		},
		{
			TransactionID: "31c06835-b480-4529-85ce-9420fcee99b8",
			GroupID:       "test-group",
			GrantHash:     "",
			ServiceName:   "",
			Direction:     record.DirectionOut,
			Source: &txlog.RecordDelegatedSource{
				OutwayPeerID:    "00000000000000000000",
				DelegatorPeerID: "00000000000000000001",
			},
			Destination: &txlog.RecordDelegatedDestination{
				ServicePeerID:   "00000000000000000002",
				DelegatorPeerID: "00000000000000000003",
			},
			CreatedAt: time.Time{},
		},
	}, nil
}

func (t *fakeTxLogAPI) ListRecordMetadata(_ context.Context, transactionID string) ([]*txlog.MetadataRecord, error) {
	metadataRecords := []*txlog.MetadataRecord{
		{
			TransactionID: transactionID,
			Direction:     metadata.DirectionIn,
			Metadata: map[string]map[string]interface{}{
				"HEADERS": {
					"Fsc-Grant-Hash": "$1$4$testhash",
				},
			},
		},
	}

	return metadataRecords, nil
}
