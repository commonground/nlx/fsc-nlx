// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestUpdatePeer_Invalid(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		RequestBody    models.UpdatePeerJSONRequestBody
		WantStatusCode int
		WantDetails    string
	}{
		"empty_name": {
			RequestBody: models.UpdatePeerJSONRequestBody{
				Peer: models.Peer{
					Id:             "00000000000000000001",
					ManagerAddress: "https://manager-ext:8443",
					Name:           "",
					Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
				},
			},
			WantStatusCode: http.StatusUnprocessableEntity,
			WantDetails:    "peer name can not be empty",
		},
		"unknown_role": {
			RequestBody: models.UpdatePeerJSONRequestBody{
				Peer: models.Peer{
					Id:             "00000000000000000001",
					ManagerAddress: "https://manager-ext:8443",
					Name:           "peer-name",
					Roles:          []models.PeerRole{"non-existing-role"},
				},
			},
			WantStatusCode: http.StatusUnprocessableEntity,
			WantDetails:    "unknown peer role \"non-existing-role\". valid roles: PEER_ROLE_DIRECTORY",
		},
		"invalid_manager_address": {
			RequestBody: models.UpdatePeerJSONRequestBody{
				Peer: models.Peer{
					Id:             "00000000000000000001",
					ManagerAddress: "invalid-address",
					Name:           "peer-name",
					Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
				},
			},
			WantStatusCode: http.StatusUnprocessableEntity,
			WantDetails:    "peer invalid manager address: missing https prefix in manager address \"invalid-address\"",
		},
		"empty_id": {
			RequestBody: models.UpdatePeerJSONRequestBody{
				Peer: models.Peer{
					Id:             "",
					ManagerAddress: "https://manager-ext:8443",
					Name:           "peer-name",
					Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
				},
			},
			WantStatusCode: http.StatusUnprocessableEntity,
			WantDetails:    "peerID cannot be empty",
		},
	}

	internalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		internalHTTPServer.Close()
	})

	client, err := createInternalManagerAPIClient(internalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			// Act
			res, err := client.UpdatePeerWithResponse(context.Background(), &models.UpdatePeerParams{}, tc.RequestBody)
			assert.NoError(t, err)

			if !assert.Equal(t, tc.WantStatusCode, res.StatusCode()) {
				t.Errorf("response body: %s", res.Body)
				return
			}

			assert.Equal(t, tc.WantDetails, res.ApplicationproblemJSON422.Details)
		})
	}
}

func TestUpdateNonExistingPeer(t *testing.T) {
	t.Parallel()

	internalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		internalHTTPServer.Close()
	})

	client, err := createInternalManagerAPIClient(internalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	idGenerator := uuidgenerator.New()

	auditLogCorrelationID, err := idGenerator.New()
	assert.NoError(t, err)

	peerIDsFilter := []models.PeerID{"12345678901234567890"}

	res, err := client.ListPeersWithResponse(context.Background(), &models.ListPeersParams{
		PeerIds: &peerIDsFilter,
	})
	assert.NoError(t, err)

	assert.Equal(t, 0, len(res.JSON200.Peers))

	// Act
	createContractResp, err := client.UpdatePeerWithResponse(context.Background(), &models.UpdatePeerParams{
		AuditlogCorrelationId: &auditLogCorrelationID,
	}, models.UpdatePeerJSONRequestBody{
		Peer: models.Peer{
			Id:             "12345678901234567890",
			ManagerAddress: "https://manager-ext:8443",
			Name:           "nlx-org-a",
			Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
		},
	})

	// Assert
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusNoContent, createContractResp.StatusCode()) {
		t.Errorf("response body: %s", createContractResp.Body)
	}

	peerIDsFilter = []models.PeerID{"12345678901234567890"}

	res, err = client.ListPeersWithResponse(context.Background(), &models.ListPeersParams{
		PeerIds: &peerIDsFilter,
	})
	assert.NoError(t, err)

	expectedPeers := []models.Peer{
		{
			Id:             "12345678901234567890",
			ManagerAddress: "https://manager-ext:8443",
			Name:           "nlx-org-a",
			Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
		},
	}

	assert.Equal(t, len(expectedPeers), len(res.JSON200.Peers))
	assert.Equal(t, expectedPeers, res.JSON200.Peers)
}

func TestUpdateExistingPeer(t *testing.T) {
	t.Parallel()

	internalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		internalHTTPServer.Close()
	})

	client, err := createInternalManagerAPIClient(internalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	idGenerator := uuidgenerator.New()

	auditLogCorrelationID, err := idGenerator.New()
	assert.NoError(t, err)

	peerIDsFilter := []models.PeerID{"00000000000000000001"}

	res, err := client.ListPeersWithResponse(context.Background(), &models.ListPeersParams{
		PeerIds: &peerIDsFilter,
	})
	assert.NoError(t, err)

	assert.Equal(t, 1, len(res.JSON200.Peers))
	assert.Equal(t, "00000000000000000001", res.JSON200.Peers[0].Id)
	assert.Equal(t, "nlx-test-a", res.JSON200.Peers[0].Name)
	assert.Equal(t, "https://manager.org-a.nlx.local:443", res.JSON200.Peers[0].ManagerAddress)
	assert.Equal(t, []models.PeerRole{}, res.JSON200.Peers[0].Roles)

	// Act
	createContractResp, err := client.UpdatePeerWithResponse(context.Background(), &models.UpdatePeerParams{
		AuditlogCorrelationId: &auditLogCorrelationID,
	}, models.UpdatePeerJSONRequestBody{
		Peer: models.Peer{
			Id:             "00000000000000000001",
			ManagerAddress: "https://manager-ext:8443",
			Name:           "nlx-org-a",
			Roles: []models.PeerRole{
				models.PEERROLEDIRECTORY,
			},
		},
	})

	// Assert
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusNoContent, createContractResp.StatusCode()) {
		t.Errorf("response body: %s", createContractResp.Body)
	}

	peerIDsFilter = []models.PeerID{"00000000000000000001"}

	res, err = client.ListPeersWithResponse(context.Background(), &models.ListPeersParams{
		PeerIds: &peerIDsFilter,
	})
	assert.NoError(t, err)

	assert.Equal(t, 1, len(res.JSON200.Peers))
	assert.Equal(t, "00000000000000000001", res.JSON200.Peers[0].Id)
	assert.Equal(t, "nlx-org-a", res.JSON200.Peers[0].Name)
	assert.Equal(t, "https://manager-ext:8443", res.JSON200.Peers[0].ManagerAddress)
	assert.Equal(t, []models.PeerRole{models.PEERROLEDIRECTORY}, res.JSON200.Peers[0].Roles)
}
