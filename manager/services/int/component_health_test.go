// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetHealth(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	res, err := apiClient.GetHealthWithResponse(context.Background())

	// Assert
	require.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}
}
