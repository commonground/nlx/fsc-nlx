// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:funlen,dupl // this is a test
package internalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

func TestListIncomingConnections_Response(t *testing.T) {
	t.Parallel()

	notBefore := testClock.Now().Add(-1 * time.Hour).Truncate(time.Second)
	notAfter := testClock.Now().Add(time.Hour).Truncate(time.Second)
	createdAt := testClock.Now().Truncate(time.Second)

	iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractHash, createErr := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: notBefore,
		ContractNotAfter:  notAfter,
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, createErr)

	res, err := apiClient.ListIncomingConnectionsWithResponse(context.Background(), &models.ListIncomingConnectionsParams{})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Connections, 1)

	c := res.JSON200.Connections[0]

	serviceConnectionGrantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
		ContractContentIV:                 iv.String(),
		ContractGroupID:                   "test-group",
		ContractHashAlgorithm:             1,
		OutwayPeerID:                      peerA.GetPeerID(),
		OutwayCertificateThumbprint:       peerA.CertBundle.PublicKeyThumbprint(),
		ServicePeerID:                     peerA.GetPeerID(),
		ServiceName:                       "parkeerrechten",
		ServicePublicationDelegatorPeerID: "",
	})
	require.NoError(t, err)

	state := models.CONTRACTSTATEVALID

	want := models.Connection{
		ContractHash:                      contractHash,
		CreatedAt:                         createdAt.Unix(),
		DelegatorPeerId:                   nil,
		GrantHash:                         serviceConnectionGrantHash,
		NotAfter:                          notAfter.Unix(),
		NotBefore:                         notBefore.Unix(),
		OutwayPeerId:                      peerA.GetPeerID(),
		ServiceName:                       "parkeerrechten",
		ServicePeerId:                     peerA.GetPeerID(),
		ServicePublicationDelegatorPeerId: nil,
		State:                             &state,
	}

	assert.Equal(t, want, c)
}

func TestListIncomingConnections_WithCount(t *testing.T) {
	t.Parallel()

	notBefore := testClock.Now().Add(-1 * time.Hour).Truncate(time.Second)
	notAfter := testClock.Now().Add(time.Hour).Truncate(time.Second)
	createdAt := testClock.Now().Truncate(time.Second)

	iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	_, createErr := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: notBefore,
		ContractNotAfter:  notAfter,
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   "parkeerrechten",
					PeerID: peerA.GetPeerID(),
				},
			},
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   "basisregistratie",
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, createErr)

	includeCount := true

	limit := models.QueryPaginationLimit(1)

	res, err := apiClient.ListIncomingConnectionsWithResponse(context.Background(), &models.ListIncomingConnectionsParams{
		IncludeCount: &includeCount,
		Limit:        &limit,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Connections, 1)
	assert.Equal(t, 2, *res.JSON200.TotalCount)
}

func TestListIncomingConnections_WithCountAndPagination(t *testing.T) {
	t.Parallel()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractHashA := createValidContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-5*time.Second).Truncate(time.Second), "parkeerrechten")
	contractHashB := createValidContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-4*time.Second).Truncate(time.Second), "parkeerrechten")
	contractHashC := createValidContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-3*time.Second).Truncate(time.Second), "parkeerrechten")
	contractHashD := createValidContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-2*time.Second).Truncate(time.Second), "parkeerrechten")

	includeCount := true
	limit := models.QueryPaginationLimit(2)

	// retrieve first page containing one row
	res, err := apiClient.ListIncomingConnectionsWithResponse(context.Background(), &models.ListIncomingConnectionsParams{
		IncludeCount: &includeCount,
		Limit:        &limit,
	})
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}

	assert.Len(t, res.JSON200.Connections, 2)
	assert.Equal(t, 4, *res.JSON200.TotalCount)
	assert.Equal(t, contractHashD, res.JSON200.Connections[0].ContractHash)
	assert.Equal(t, contractHashC, res.JSON200.Connections[1].ContractHash)

	// retrieve second page containing one row
	res, err = apiClient.ListIncomingConnectionsWithResponse(context.Background(), &models.ListIncomingConnectionsParams{
		Cursor:       &res.JSON200.Pagination.NextCursor,
		IncludeCount: &includeCount,
		Limit:        &limit,
	})
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}

	assert.Len(t, res.JSON200.Connections, 2)
	assert.Equal(t, 4, *res.JSON200.TotalCount)
	assert.Equal(t, contractHashB, res.JSON200.Connections[0].ContractHash)
	assert.Equal(t, contractHashA, res.JSON200.Connections[1].ContractHash)
}

func TestListIncomingConnections_ContractStateFilter(t *testing.T) {
	t.Parallel()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	validContractHash := createValidContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-5*time.Second).Truncate(time.Second), "service1")
	rejectedContractHash := createRejectedContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-4*time.Second).Truncate(time.Second), "service2")
	revokedContractHash := createRevokedContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-3*time.Second).Truncate(time.Second), "service3")
	proposedContractHash := createProposedContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-2*time.Second).Truncate(time.Second), "service4")
	expiredContractHash := createExpiredContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-1*time.Second).Truncate(time.Second), "service5")

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	require.NoError(t, err)

	tests := map[string]struct {
		Params                     *models.ListIncomingConnectionsParams
		WantResponseContractHashes []string
	}{
		"filter_on_valid_contracts": {
			Params: &models.ListIncomingConnectionsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEVALID,
				},
			},
			WantResponseContractHashes: []string{
				validContractHash,
			},
		},
		"filter_on_rejected_contracts": {
			Params: &models.ListIncomingConnectionsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEREJECTED,
				},
			},
			WantResponseContractHashes: []string{
				rejectedContractHash,
			},
		},
		"filter_on_revoked_contracts": {
			Params: &models.ListIncomingConnectionsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEREVOKED,
				},
			},
			WantResponseContractHashes: []string{
				revokedContractHash,
			},
		},
		"filter_on_proposed_contracts": {
			Params: &models.ListIncomingConnectionsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEPROPOSED,
				},
			},
			WantResponseContractHashes: []string{
				proposedContractHash,
			},
		},
		"filter_on_expired_contracts": {
			Params: &models.ListIncomingConnectionsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEEXPIRED,
				},
			},
			WantResponseContractHashes: []string{
				expiredContractHash,
			},
		},
		"without_filter": {
			Params: &models.ListIncomingConnectionsParams{
				ContractStates: &[]models.ContractState{},
			},
			WantResponseContractHashes: []string{
				expiredContractHash,
				proposedContractHash,
				revokedContractHash,
				rejectedContractHash,
				validContractHash,
			},
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			res, err := apiClient.ListIncomingConnectionsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
				t.Logf("response body: %s", res.Body)
			}

			assert.Len(t, res.JSON200.Connections, len(test.WantResponseContractHashes))

			responseContractHashes := []string{}

			for _, connection := range res.JSON200.Connections {
				responseContractHashes = append(responseContractHashes, connection.ContractHash)
			}

			assert.Equal(t, tc.WantResponseContractHashes, responseContractHashes)
		})
	}
}

func TestListIncomingConnections_FilterByServiceNames(t *testing.T) {
	t.Parallel()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	createValidContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-1*time.Second).Truncate(time.Second), "service1")
	createValidContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-2*time.Second).Truncate(time.Second), "service2")
	createValidContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-3*time.Second).Truncate(time.Second), "service3")
	createValidContractWithIncomingServiceConnection(t, app, testClock.Now().Add(-4*time.Second).Truncate(time.Second), "service4")

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	require.NoError(t, err)

	serviceNamesFilter := []models.ServiceName{"service1", "service2", "service3"}
	res, err := apiClient.ListIncomingConnectionsWithResponse(context.Background(), &models.ListIncomingConnectionsParams{
		ServiceNames: &serviceNamesFilter,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Connections, 3)
	assert.Equal(t, "service1", res.JSON200.Connections[0].ServiceName)
	assert.Equal(t, "service2", res.JSON200.Connections[1].ServiceName)
	assert.Equal(t, "service3", res.JSON200.Connections[2].ServiceName)
}

func createValidContractWithIncomingServiceConnection(t *testing.T, app *internalapp.Application, createdAt time.Time, serviceName string) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   serviceName,
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	return contractHash
}

func createRejectedContractWithIncomingServiceConnection(t *testing.T, app *internalapp.Application, createdAt time.Time, serviceName string) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee80").String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   serviceName,
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	rejectErr := app.Commands.RejectContract.Handle(context.Background(), &command.RejectContractHandlerArgs{
		ContentHash:    contractHash,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, rejectErr)

	return contractHash
}

func createRevokedContractWithIncomingServiceConnection(t *testing.T, app *internalapp.Application, createdAt time.Time, serviceName string) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee81").String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   serviceName,
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	revokeErr := app.Commands.RevokeContract.Handle(context.Background(), &command.RevokeContractHandlerArgs{
		ContentHash:    contractHash,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, revokeErr)

	return contractHash
}

func createProposedContractWithIncomingServiceConnection(t *testing.T, app *internalapp.Application, createdAt time.Time, serviceName string) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee82").String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerB.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   serviceName,
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	return contractHash
}

func createExpiredContractWithIncomingServiceConnection(t *testing.T, app *internalapp.Application, createdAt time.Time, serviceName string) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee83").String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-2 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					Name:   serviceName,
					PeerID: peerA.GetPeerID(),
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	return contractHash
}
