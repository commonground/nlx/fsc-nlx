// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestListServices(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	response, err := apiClient.ListServicesWithResponse(context.Background(), peerDirectory.GetPeerID(), &models.ListServicesParams{})
	assert.NoError(t, err)

	// Assert
	if !assert.Equal(t, http.StatusOK, response.StatusCode()) {
		t.Fatalf("unexpected status code %d, response body: %s", response.StatusCode(), string(response.Body))
	}

	assert.Len(t, response.JSON200.Services, 2)

	firstService, err := response.JSON200.Services[0].AsService()
	require.NoError(t, err)

	assert.Equal(t, "parkeerrechten", firstService.Name)
	assert.Equal(t, peerA.GetPeerID(), firstService.Peer.Id)
	assert.Equal(t, peerA.GetName(), firstService.Peer.Name)
	assert.Equal(t, "", firstService.Peer.ManagerAddress)

	secondService, err := response.JSON200.Services[1].AsDelegatedService()
	require.NoError(t, err)

	assert.Equal(t, "parkeerrechten-delegated", secondService.Name)
	assert.Equal(t, peerA.GetPeerID(), secondService.Peer.Id)
	assert.Equal(t, peerA.GetName(), secondService.Peer.Name)
	assert.Equal(t, peerB.GetPeerID(), secondService.Delegator.Id)
	assert.Equal(t, peerB.GetName(), secondService.Delegator.Name)
	assert.Equal(t, "", secondService.Delegator.ManagerAddress)
}
