// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	externalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext"
	ext_command "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/command"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

type testContractServiceConfig struct {
	t           *testing.T
	clock       clock.Clock
	internalApp *internalapp.Application
	externalApp *externalapp.Application
}

type testContractService struct {
	config *testContractServiceConfig
}

func newTestContractService(cf *testContractServiceConfig) *testContractService {
	return &testContractService{
		config: cf,
	}
}

func (f *testContractService) NewServiceConnectionWithPeerCreatedAt(outwayPeer, servicePeer *peerInfo, createdAt time.Time) uuid.UUID {
	iv := uuid.New()

	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: f.config.clock.Now().Truncate(time.Second),
		ContractNotAfter:  f.config.clock.Now().Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: outwayPeer.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              outwayPeer.GetPeerID(),
				Service: &command.NewServiceArgs{
					PeerID: servicePeer.GetPeerID(),
					Name:   "parkeerrechten",
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	}

	_, err := f.config.internalApp.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(f.config.t, err)

	return iv
}

func (f *testContractService) CreateIncomingServiceConnectionWithNotBefore(notBefore time.Time) {
	iv := uuid.New()

	ivBytes, err := iv.MarshalBinary()
	require.NoError(f.config.t, err)

	createdAt := f.config.clock.Now().Truncate(time.Second)

	//nolint:dupl // this is not a duplicate
	cc, err := contract.NewContent(&contract.NewContentArgs{
		Clock: testClock,
		Validity: &contract.NewValidityArgs{
			NotBefore: notBefore,
			NotAfter:  notBefore.Add(time.Hour).Truncate(time.Second),
		},
		GroupID: "fsc-local",
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerA.GetPeerID(),
					},
					PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionDelegatedServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: peerB.GetPeerID(),
					},
					Name: "parkeerrechten",
					PublicationDelegator: &contract.NewPeerArgs{
						ID: peerC.GetPeerID(),
					},
				},
			},
		},
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            ivBytes,
		CreatedAt:     createdAt,
	})
	require.NoError(f.config.t, err)

	sigServiceConnection, err := cc.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), createdAt)
	require.NoError(f.config.t, err)

	createContractArgs := &ext_command.HandleSubmitContractArgs{
		ContractContent: &ext_command.ContractContentArgs{
			HashAlgorithm: string(models.HASHALGORITHMSHA3512),
			IV:            ivBytes,
			GroupID:       "fsc-local",
			NotBefore:     notBefore,
			NotAfter:      notBefore.Add(time.Hour),
			CreatedAt:     createdAt,
			Grants: []interface{}{
				&ext_command.GrantServiceConnectionArgs{
					OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
					OutwayPeerID:              peerA.GetPeerID(),
					Service: &ext_command.DelegatedServiceArgs{
						Name:                              "parkeerrechten",
						PeerID:                            peerB.GetPeerID(),
						ServicePublicationDelegatorPeerID: peerC.GetPeerID(),
					},
				},
			},
		},
		Signature:       sigServiceConnection.JWS(),
		SubmittedByPeer: &ext_command.PeerArgs{ID: peerB.GetPeerID(), ManagerAddress: peerB.ManagerAddress, Name: peerB.GetName()},
		PeerCert:        peerB.CertBundle,
	}

	createErr := f.config.externalApp.Commands.SubmitContract.Handle(context.Background(), createContractArgs)
	require.NoError(f.config.t, createErr)
}

func (f *testContractService) NewServiceConnectionWithPeerCreatedAtAndID(outwayPeer, servicePeer *peerInfo, createdAt time.Time, iv uuid.UUID) string {
	//nolint:dupl // this is not a duplicate
	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: f.config.clock.Now().Truncate(time.Second),
		ContractNotAfter:  f.config.clock.Now().Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: outwayPeer.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              outwayPeer.GetPeerID(),
				Service: &command.NewServiceArgs{
					PeerID: servicePeer.GetPeerID(),
					Name:   "parkeerrechten",
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	}

	contractContentHash, err := f.config.internalApp.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(f.config.t, err)

	return contractContentHash
}

func (f *testContractService) NewDelegatedServiceConnectionWithCreatedAt(createdAt time.Time) string {
	iv := uuid.New()

	//nolint:dupl // this is not a duplicate
	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: f.config.clock.Now().Truncate(time.Second),
		ContractNotAfter:  f.config.clock.Now().Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantDelegatedServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerB.GetPeerID(),
				Service: &command.NewServiceArgs{
					PeerID: peerA.GetPeerID(),
					Name:   "parkeerrechten",
				},
				DelegatorPeerID: peerC.GetPeerID(),
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	}

	contractContentHash, err := f.config.internalApp.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(f.config.t, err)

	return contractContentHash
}

func (f *testContractService) NewWithCreatedAtAndPeerID(createdAt time.Time, servicePeerID string) (string, uuid.UUID) {
	iv := uuid.New()

	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: f.config.clock.Now().Truncate(time.Second),
		ContractNotAfter:  f.config.clock.Now().Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				OutwayPublicKeyThumbprint: peerB.CertBundle.PublicKeyThumbprint(),
				OutwayPeerID:              peerA.GetPeerID(),
				Service: &command.NewServiceArgs{
					PeerID: servicePeerID,
					Name:   "parkeerrechten",
				},
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	}

	contractHash, err := f.config.internalApp.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(f.config.t, err)

	return contractHash, iv
}
