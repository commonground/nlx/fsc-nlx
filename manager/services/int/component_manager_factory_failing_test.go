// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

//nolint:unused // most methods are unused but added to comply with the Manager interface
package internalservice_test

import (
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
)

type fakeFailingManagerFactory struct {
}

func (f *fakeFailingManagerFactory) New(_, address string) (manager.Manager, error) {
	return &fakeFailingManager{}, nil
}
