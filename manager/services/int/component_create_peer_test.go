// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestCreatePeer(t *testing.T) {
	t.Parallel()

	internalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		internalHTTPServer.Close()
	})

	client, err := createInternalManagerAPIClient(internalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	idGenerator := uuidgenerator.New()

	auditLogCorrelationID, err := idGenerator.New()
	assert.NoError(t, err)

	// Act
	createContractResp, err := client.CreatePeerWithResponse(context.Background(), &models.CreatePeerParams{
		AuditlogCorrelationId: &auditLogCorrelationID,
	}, models.CreatePeerJSONRequestBody{
		Peer: models.Peer{
			Id:             "12345678901234567890",
			ManagerAddress: "https://manager-ext:8443",
			Name:           "nlx-org-a",
			Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
		},
	})

	// Assert
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, createContractResp.StatusCode()) {
		t.Errorf("response body: %s", createContractResp.Body)
	}

	peerIDsFilter := []models.PeerID{"12345678901234567890"}

	res, err := client.ListPeersWithResponse(context.Background(), &models.ListPeersParams{
		PeerIds: &peerIDsFilter,
	})
	assert.NoError(t, err)

	assert.Equal(t, 1, len(res.JSON200.Peers))
	assert.Equal(t, "12345678901234567890", res.JSON200.Peers[0].Id)
	assert.Equal(t, "nlx-org-a", res.JSON200.Peers[0].Name)
	assert.Equal(t, "https://manager-ext:8443", res.JSON200.Peers[0].ManagerAddress)
	assert.Equal(t, []models.PeerRole{models.PEERROLEDIRECTORY}, res.JSON200.Peers[0].Roles)
}
