// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// nolint:dupl // looks the same but different test case
func TestSyncPeers(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp, _, err := newService(t.Name())
	require.NoError(t, err)

	defer externalHTTPServer.Close()

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	err = intApp.Commands.CreatePeer.Handle(context.Background(), &command.CreatePeerArgs{
		PeerID:                peerDirectory.GetPeerID(),
		PeerName:              peerDirectory.GetName(),
		ManagerAddress:        peerDirectory.ManagerAddress,
		Roles:                 []command.PeerRole{command.PeerRoleDirectory},
		AuditLogSource:        source,
		AuthData:              authData,
		AuditlogCorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
	})
	require.NoError(t, err)

	err = intApp.Commands.CreatePeer.Handle(context.Background(), &command.CreatePeerArgs{
		PeerID:                peerA.GetPeerID(),
		PeerName:              "Local name Organization A",
		Roles:                 []command.PeerRole{},
		ManagerAddress:        "https://local-manager-address-org-a:8443",
		AuditLogSource:        source,
		AuthData:              authData,
		AuditlogCorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
	})
	require.NoError(t, err)

	err = intApp.Commands.CreatePeer.Handle(context.Background(), &command.CreatePeerArgs{
		PeerID:                "00000000000000000042",
		PeerName:              "Test Organization",
		ManagerAddress:        "https://example:8443",
		AuditLogSource:        source,
		AuthData:              authData,
		AuditlogCorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
	})
	require.NoError(t, err)

	tests := map[string]struct {
		Want []models.PeerSynchronizationResult
	}{
		"happy_flow": {
			Want: []models.PeerSynchronizationResult{
				{
					PeerId: peerDirectory.GetPeerID(),
					Ok:     true,
				},
			},
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			// Act
			res, err := apiClient.SyncPeersWithResponse(context.Background())

			// Assert
			require.NoError(t, err)
			require.Equal(t, http.StatusOK, res.StatusCode())
			require.Len(t, res.JSON200.Result, len(test.Want))
			require.Equal(t, test.Want, res.JSON200.Result)

			if res.StatusCode() != http.StatusOK {
				t.Logf("Received non-200 status code: %d, body: %s", res.StatusCode(), string(res.Body))
			}
		})
	}
}

// nolint:dupl // looks the same but different test case
func TestSyncPeersDirectoryUnreachable(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp, _, err := newServiceWithCustomManagerFactory(t.Name(), &fakeFailingManagerFactory{})
	require.NoError(t, err)

	defer externalHTTPServer.Close()

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	err = intApp.Commands.CreatePeer.Handle(context.Background(), &command.CreatePeerArgs{
		PeerID:                peerDirectory.GetPeerID(),
		PeerName:              peerDirectory.GetName(),
		ManagerAddress:        peerDirectory.ManagerAddress,
		Roles:                 []command.PeerRole{command.PeerRoleDirectory},
		AuditLogSource:        source,
		AuthData:              authData,
		AuditlogCorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
	})
	require.NoError(t, err)

	err = intApp.Commands.CreatePeer.Handle(context.Background(), &command.CreatePeerArgs{
		PeerID:                peerA.GetPeerID(),
		PeerName:              "Local name Organization A",
		Roles:                 []command.PeerRole{},
		ManagerAddress:        "https://local-manager-address-org-a:8443",
		AuditLogSource:        source,
		AuthData:              authData,
		AuditlogCorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
	})
	require.NoError(t, err)

	expectedError := "arbitrary"

	tests := map[string]struct {
		Want []models.PeerSynchronizationResult
	}{
		"happy_flow": {
			Want: []models.PeerSynchronizationResult{
				{
					PeerId: peerDirectory.GetPeerID(),
					Error:  &expectedError,
				},
			},
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			// Act
			res, err := apiClient.SyncPeersWithResponse(context.Background())

			// Assert
			require.NoError(t, err)
			require.Equal(t, http.StatusOK, res.StatusCode())
			require.Len(t, res.JSON200.Result, len(test.Want))
			require.Equal(t, test.Want, res.JSON200.Result)

			if res.StatusCode() != http.StatusOK {
				t.Logf("Received non-200 status code: %d, body: %s", res.StatusCode(), string(res.Body))
			}
		})
	}
}
