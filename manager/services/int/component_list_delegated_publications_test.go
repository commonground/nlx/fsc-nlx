// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:funlen,dupl // this is a test
package internalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

func TestListDelegatedPublications_Response(t *testing.T) {
	t.Parallel()

	notBefore := testClock.Now().Add(-1 * time.Hour).Truncate(time.Second)
	notAfter := testClock.Now().Add(time.Hour).Truncate(time.Second)
	createdAt := testClock.Now().Truncate(time.Second)

	iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractHash, createErr := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: notBefore,
		ContractNotAfter:  notAfter,
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: peerA.GetPeerID(),
				DirectoryPeerID: peerDirectory.GetPeerID(),
				ServicePeerID:   peerB.GetPeerID(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, createErr)

	res, err := apiClient.ListDelegatedServicePublicationsWithResponse(context.Background(), &models.ListDelegatedServicePublicationsParams{})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Publications, 1)

	c := res.JSON200.Publications[0]

	servicePublicationGrantHash, err := testhash.GenerateDelegatedServicePublicationGrantHash(&testhash.DelegatedServicePublicationGrantArgs{
		ContractContentIV:     iv.String(),
		ContractGroupID:       "test-group",
		ContractHashAlgorithm: 1,
		DelegatorPeerID:       peerA.GetPeerID(),
		DirectoryPeerID:       peerDirectory.GetPeerID(),
		ServicePeerID:         peerB.GetPeerID(),
		ServiceName:           "parkeerrechten",
		ServiceProtocol:       "PROTOCOL_TCP_HTTP_1.1",
	})
	require.NoError(t, err)

	state := models.CONTRACTSTATEPROPOSED

	delegatodPeerID := peerA.GetPeerID()

	want := models.Publication{
		ContractHash:    contractHash,
		CreatedAt:       createdAt.Unix(),
		DelegatorPeerId: &delegatodPeerID,
		DirectoryPeerId: peerDirectory.GetPeerID(),
		GrantHash:       servicePublicationGrantHash,
		NotAfter:        notAfter.Unix(),
		NotBefore:       notBefore.Unix(),
		ServiceName:     "parkeerrechten",
		ServicePeerId:   peerB.GetPeerID(),
		State:           &state,
	}

	assert.Equal(t, want, c)
}

func TestListDelegatedPublications_WithCount(t *testing.T) {
	t.Parallel()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	_, createErr := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79").String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: peerA.GetPeerID(),
				DirectoryPeerID: peerDirectory.GetPeerID(),
				ServicePeerID:   peerB.GetPeerID(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: peerA.GetPeerID(),
				DirectoryPeerID: peerDirectory.GetPeerID(),
				ServicePeerID:   peerB.GetPeerID(),
				ServiceName:     "basisregister-fictieve-kentekens",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt:      testClock.Now().Truncate(time.Second),
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, createErr)

	includeCount := true

	limit := models.QueryPaginationLimit(1)

	res, err := apiClient.ListDelegatedServicePublicationsWithResponse(context.Background(), &models.ListDelegatedServicePublicationsParams{
		IncludeCount: &includeCount,
		Limit:        &limit,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Publications, 1)
	assert.Equal(t, 2, *res.JSON200.TotalCount)
}

func TestListDelegatedPublications_WithCountAndPagination(t *testing.T) {
	t.Parallel()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractHashA := createValidContractWithDelegatedServicePublication(t, app, testClock.Now().Add(-5*time.Second).Truncate(time.Second))
	contractHashB := createValidContractWithDelegatedServicePublication(t, app, testClock.Now().Add(-4*time.Second).Truncate(time.Second))
	contractHashC := createValidContractWithDelegatedServicePublication(t, app, testClock.Now().Add(-3*time.Second).Truncate(time.Second))
	contractHashD := createValidContractWithDelegatedServicePublication(t, app, testClock.Now().Add(-2*time.Second).Truncate(time.Second))

	includeCount := true
	limit := models.QueryPaginationLimit(2)

	// retrieve first page containing one row
	res, err := apiClient.ListDelegatedServicePublicationsWithResponse(context.Background(), &models.ListDelegatedServicePublicationsParams{
		IncludeCount: &includeCount,
		Limit:        &limit,
	})
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}

	assert.Len(t, res.JSON200.Publications, 2)
	assert.Equal(t, 4, *res.JSON200.TotalCount)
	assert.Equal(t, contractHashD, res.JSON200.Publications[0].ContractHash)
	assert.Equal(t, contractHashC, res.JSON200.Publications[1].ContractHash)

	// retrieve second page containing one row
	res, err = apiClient.ListDelegatedServicePublicationsWithResponse(context.Background(), &models.ListDelegatedServicePublicationsParams{
		Cursor:       &res.JSON200.Pagination.NextCursor,
		IncludeCount: &includeCount,
		Limit:        &limit,
	})
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}

	assert.Len(t, res.JSON200.Publications, 2)
	assert.Equal(t, 4, *res.JSON200.TotalCount)
	assert.Equal(t, contractHashB, res.JSON200.Publications[0].ContractHash)
	assert.Equal(t, contractHashA, res.JSON200.Publications[1].ContractHash)
}

func TestListDelegatedPublications_ContractStateFilter(t *testing.T) {
	t.Parallel()

	externalHTTPServer, app, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	validContractHash := createValidContractWithDelegatedServicePublication(t, app, testClock.Now().Add(-5*time.Second).Truncate(time.Second))
	rejectedContractHash := createRejectedContractWithDelegatedServicePublication(t, app, testClock.Now().Add(-4*time.Second).Truncate(time.Second))
	revokedContractHash := createRevokedContractWithDelegatedServicePublication(t, app, testClock.Now().Add(-3*time.Second).Truncate(time.Second))
	proposedContractHash := createProposedContractWithDelegatedServicePublication(t, app, testClock.Now().Add(-2*time.Second).Truncate(time.Second))
	expiredContractHash := createExpiredContractWithDelegatedServicePublication(t, app, testClock.Now().Add(-1*time.Second).Truncate(time.Second))

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	require.NoError(t, err)

	tests := map[string]struct {
		Params                     *models.ListDelegatedServicePublicationsParams
		WantResponseContractHashes []string
	}{
		"filter_on_valid_contracts": {
			Params: &models.ListDelegatedServicePublicationsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEVALID,
				},
			},
			WantResponseContractHashes: []string{
				validContractHash,
			},
		},
		"filter_on_rejected_contracts": {
			Params: &models.ListDelegatedServicePublicationsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEREJECTED,
				},
			},
			WantResponseContractHashes: []string{
				rejectedContractHash,
			},
		},
		"filter_on_revoked_contracts": {
			Params: &models.ListDelegatedServicePublicationsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEREVOKED,
				},
			},
			WantResponseContractHashes: []string{
				revokedContractHash,
			},
		},
		"filter_on_proposed_contracts": {
			Params: &models.ListDelegatedServicePublicationsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEPROPOSED,
				},
			},
			WantResponseContractHashes: []string{
				proposedContractHash,
			},
		},
		"filter_on_expired_contracts": {
			Params: &models.ListDelegatedServicePublicationsParams{
				ContractStates: &[]models.ContractState{
					models.CONTRACTSTATEEXPIRED,
				},
			},
			WantResponseContractHashes: []string{
				expiredContractHash,
			},
		},
		"without_filter": {
			Params: &models.ListDelegatedServicePublicationsParams{
				ContractStates: &[]models.ContractState{},
			},
			WantResponseContractHashes: []string{
				expiredContractHash,
				proposedContractHash,
				revokedContractHash,
				rejectedContractHash,
				validContractHash,
			},
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			res, err := apiClient.ListDelegatedServicePublicationsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
				t.Logf("response body: %s", res.Body)
			}

			assert.Len(t, res.JSON200.Publications, len(test.WantResponseContractHashes))

			responseContractHashes := []string{}

			for _, connection := range res.JSON200.Publications {
				responseContractHashes = append(responseContractHashes, connection.ContractHash)
			}

			assert.Equal(t, tc.WantResponseContractHashes, responseContractHashes)
		})
	}
}

func createValidContractWithDelegatedServicePublication(t *testing.T, app *internalapp.Application, createdAt time.Time) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.New().String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: peerA.GetPeerID(),
				DirectoryPeerID: peerA.GetPeerID(),
				ServicePeerID:   peerA.GetPeerID(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	return contractHash
}

func createRejectedContractWithDelegatedServicePublication(t *testing.T, app *internalapp.Application, createdAt time.Time) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee80").String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: peerA.GetPeerID(),
				DirectoryPeerID: peerDirectory.GetPeerID(),
				ServicePeerID:   peerB.GetPeerID(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	rejectErr := app.Commands.RejectContract.Handle(context.Background(), &command.RejectContractHandlerArgs{
		ContentHash:    contractHash,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, rejectErr)

	return contractHash
}

func createRevokedContractWithDelegatedServicePublication(t *testing.T, app *internalapp.Application, createdAt time.Time) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee81").String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: peerA.GetPeerID(),
				DirectoryPeerID: peerDirectory.GetPeerID(),
				ServicePeerID:   peerB.GetPeerID(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	revokeErr := app.Commands.RevokeContract.Handle(context.Background(), &command.RevokeContractHandlerArgs{
		ContentHash:    contractHash,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, revokeErr)

	return contractHash
}

func createProposedContractWithDelegatedServicePublication(t *testing.T, app *internalapp.Application, createdAt time.Time) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee82").String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: peerA.GetPeerID(),
				DirectoryPeerID: peerDirectory.GetPeerID(),
				ServicePeerID:   peerB.GetPeerID(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	require.NoError(t, err)

	return contractHash
}

func createExpiredContractWithDelegatedServicePublication(t *testing.T, app *internalapp.Application, createdAt time.Time) string {
	contractHash, err := app.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee83").String(),
		GroupID:           "test-group",
		ContractNotBefore: testClock.Now().Add(-2 * time.Hour).Truncate(time.Second),
		ContractNotAfter:  testClock.Now().Add(-1 * time.Hour).Truncate(time.Second),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: peerA.GetPeerID(),
				DirectoryPeerID: peerDirectory.GetPeerID(),
				ServicePeerID:   peerB.GetPeerID(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt:      createdAt,
		AuditLogSource: source,
		AuthData:       authData,
	})
	assert.NoError(t, err)

	return contractHash
}
