// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"

	"net/http"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestGetLogMetadata(t *testing.T) {
	t.Parallel()

	externalHTTPServer, _, _, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	transactionID, err := uuid.NewV7()
	assert.NoError(t, err)

	response, err := apiClient.GetMetadataRecordsWithResponse(context.Background(), transactionID.String())
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, response.StatusCode())
	assert.Len(t, response.JSON200.Records, 1)
	assert.Equal(t, transactionID.String(), response.JSON200.Records[0].TransactionId)
	assert.Equal(t, models.DIRECTIONINCOMING, response.JSON200.Records[0].Direction)

	metadataHeaders, found := response.JSON200.Records[0].Get("HEADERS")
	assert.True(t, found)
	assert.Equal(t, "$1$4$testhash", metadataHeaders["Fsc-Grant-Hash"])
}
