// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:funlen // this is a test
package internalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	ext_command "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/command"
	int_command "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestListPendingContracts(t *testing.T) {
	t.Parallel()

	now := testClock.Now().Truncate(time.Second)

	createdAt := time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC)
	validNotBefore := time.Date(2030, 5, 23, 0, 0, 0, 0, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*ext_command.HandleSubmitContractArgs
		Params            *models.ListPendingContractsParams
		Want              []models.PendingContract
	}{
		"service_connection_grant_w_delegator": {
			ContractsToCreate: func() []*ext_command.HandleSubmitContractArgs {
				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")

				ivBytes, err := iv.MarshalBinary()
				require.NoError(t, err)

				//nolint:dupl // this is not a duplicate
				cc, err := contract.NewContent(&contract.NewContentArgs{
					Clock: testClock,
					Validity: &contract.NewValidityArgs{
						NotBefore: validNotBefore,
						NotAfter:  validNotBefore.Add(time.Hour).Truncate(time.Second),
					},
					GroupID: "fsc-local",
					Grants: []interface{}{
						&contract.NewGrantServiceConnectionArgs{
							Outway: &contract.NewGrantServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerA.GetPeerID(),
								},
								PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerB.GetPeerID(),
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: peerC.GetPeerID(),
								},
							},
						},
					},
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            ivBytes,
					CreatedAt:     createdAt,
				})
				require.NoError(t, err)

				sigServiceConnection, err := cc.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), now)
				require.NoError(t, err)

				result := []*ext_command.HandleSubmitContractArgs{
					{
						ContractContent: &ext_command.ContractContentArgs{
							HashAlgorithm: string(models.HASHALGORITHMSHA3512),
							IV:            ivBytes,
							GroupID:       "fsc-local",
							NotBefore:     validNotBefore,
							NotAfter:      validNotBefore.Add(time.Hour),
							CreatedAt:     createdAt,
							Grants: []interface{}{
								&ext_command.GrantServiceConnectionArgs{
									OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
									OutwayPeerID:              peerA.GetPeerID(),
									Service: &ext_command.DelegatedServiceArgs{
										Name:                              "parkeerrechten",
										PeerID:                            peerB.GetPeerID(),
										ServicePublicationDelegatorPeerID: peerC.GetPeerID(),
									},
								},
							},
						},
						Signature:       sigServiceConnection.JWS(),
						SubmittedByPeer: &ext_command.PeerArgs{ID: peerB.GetPeerID(), ManagerAddress: peerB.ManagerAddress, Name: peerB.GetName()},
						PeerCert:        peerB.CertBundle,
					},
				}

				return result
			}(),
			Params: &models.ListPendingContractsParams{},
			Want: func() []models.PendingContract {
				service := models.GrantServiceConnection_Service{}

				err := service.FromDelegatedService(models.DelegatedService{
					Delegator: models.Peer{
						Id: peerC.GetPeerID(),
					},
					Name: "parkeerrechten",
					Peer: models.Peer{
						Id: peerB.GetPeerID(),
					},
				})
				assert.NoError(t, err)

				serviceConnectionGrant := models.Grant{}

				err = serviceConnectionGrant.FromGrantServiceConnection(models.GrantServiceConnection{
					Outway: models.OutwayPeer{
						PeerId:              peerA.GetPeerID(),
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
					},
					Service: service,
				})
				assert.NoError(t, err)

				result := []models.PendingContract{
					{
						Grants: []models.Grant{
							serviceConnectionGrant,
						},
						ValidNotBefore: validNotBefore.Unix(),
					},
				}

				return result
			}(),
		},
		"delegated_service_connection_grant": {
			ContractsToCreate: func() []*ext_command.HandleSubmitContractArgs {
				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")

				ivBytes, err := iv.MarshalBinary()
				require.NoError(t, err)

				cc, err := contract.NewContent(&contract.NewContentArgs{
					Clock: testClock,
					Validity: &contract.NewValidityArgs{
						NotBefore: validNotBefore,
						NotAfter:  validNotBefore.Add(time.Hour).Truncate(time.Second),
					},
					GroupID: "fsc-local",
					Grants: []interface{}{
						&contract.NewGrantDelegatedServiceConnectionArgs{
							Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerD.GetPeerID(),
								},
							},
							Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerA.GetPeerID(),
								},
								PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
							},
							Service: &contract.NewGrantDelegatedServiceConnectionDelegatedServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerB.GetPeerID(),
								},
								Name: "parkeerrechten",
								PublicationDelegator: &contract.NewPeerArgs{
									ID: peerC.GetPeerID(),
								},
							},
						},
					},
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            ivBytes,
					CreatedAt:     createdAt,
				})
				require.NoError(t, err)

				sigServiceConnection, err := cc.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), now)
				require.NoError(t, err)

				result := []*ext_command.HandleSubmitContractArgs{
					{
						ContractContent: &ext_command.ContractContentArgs{
							HashAlgorithm: string(models.HASHALGORITHMSHA3512),
							IV:            ivBytes,
							GroupID:       "fsc-local",
							NotBefore:     validNotBefore,
							NotAfter:      validNotBefore.Add(time.Hour),
							CreatedAt:     createdAt,
							Grants: []interface{}{
								&ext_command.GrantDelegatedServiceConnectionArgs{
									OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
									OutwayPeerID:              peerA.GetPeerID(),
									DelegatorPeerID:           peerD.GetPeerID(),
									Service: &ext_command.DelegatedServiceArgs{
										Name:                              "parkeerrechten",
										PeerID:                            peerB.GetPeerID(),
										ServicePublicationDelegatorPeerID: peerC.GetPeerID(),
									},
								},
							},
						},
						Signature:       sigServiceConnection.JWS(),
						SubmittedByPeer: &ext_command.PeerArgs{ID: peerB.GetPeerID(), ManagerAddress: peerB.ManagerAddress, Name: peerB.GetName()},
						PeerCert:        peerB.CertBundle,
					},
				}

				return result
			}(),
			Params: &models.ListPendingContractsParams{},
			Want: func() []models.PendingContract {
				service := models.GrantDelegatedServiceConnection_Service{}

				err := service.FromDelegatedService(models.DelegatedService{
					Delegator: models.Peer{
						Id: peerC.GetPeerID(),
					},
					Name: "parkeerrechten",
					Peer: models.Peer{
						Id: peerB.GetPeerID(),
					},
				})
				assert.NoError(t, err)

				delegatedServiceConnectionGrant := models.Grant{}

				err = delegatedServiceConnectionGrant.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
					Outway: models.OutwayPeer{
						PeerId:              peerA.GetPeerID(),
						PublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
					},
					Service: service,
					Delegator: models.DelegatorPeer{
						PeerId: peerD.GetPeerID(),
					},
				})
				assert.NoError(t, err)

				result := []models.PendingContract{
					{
						Grants: []models.Grant{
							delegatedServiceConnectionGrant,
						},
						ValidNotBefore: validNotBefore.Unix(),
					},
				}

				return result
			}(),
		},
		"service_publication_grant": {
			ContractsToCreate: func() []*ext_command.HandleSubmitContractArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Service: models.ServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerB.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				require.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				require.NoError(t, err)

				ivBytes, err := iv.MarshalBinary()
				require.NoError(t, err)

				cc, err := contract.NewContent(&contract.NewContentArgs{
					Clock: testClock,
					Validity: &contract.NewValidityArgs{
						NotBefore: validNotBefore,
						NotAfter:  validNotBefore.Add(time.Hour).Truncate(time.Second),
					},
					GroupID: "fsc-local",
					Grants: []interface{}{
						&contract.NewGrantServicePublicationArgs{
							Directory: &contract.NewGrantServicePublicationDirectoryArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerA.GetPeerID(),
								},
							},
							Service: &contract.NewGrantServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerB.GetPeerID(),
								},
								Name: "parkeerrechten", Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
					},
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            ivBytes,
					CreatedAt:     createdAt,
				})
				require.NoError(t, err)

				sig, err := cc.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), now)
				require.NoError(t, err)

				result := []*ext_command.HandleSubmitContractArgs{
					{
						ContractContent: &ext_command.ContractContentArgs{
							HashAlgorithm: string(models.HASHALGORITHMSHA3512),
							IV:            ivBytes,
							GroupID:       "fsc-local",
							NotBefore:     validNotBefore,
							NotAfter:      validNotBefore.Add(time.Hour),
							CreatedAt:     createdAt,
							Grants: []interface{}{
								&ext_command.GrantServicePublicationArgs{
									DirectoryPeerID: peerA.GetPeerID(),
									ServicePeerID:   peerB.GetPeerID(),
									ServiceName:     "parkeerrechten",
									ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
								},
							},
						},
						Signature:       sig.JWS(),
						SubmittedByPeer: &ext_command.PeerArgs{ID: peerB.GetPeerID(), ManagerAddress: peerB.ManagerAddress, Name: peerB.GetName()},
						PeerCert:        peerB.CertBundle,
					},
				}

				return result
			}(),
			Params: &models.ListPendingContractsParams{},
			Want: func() []models.PendingContract {
				servicePublication := models.Grant{}

				err := servicePublication.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Service: models.ServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerB.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				result := []models.PendingContract{
					{
						Grants: []models.Grant{
							servicePublication,
						},
						ValidNotBefore: validNotBefore.Unix(),
					},
				}

				return result
			}(),
		},
		"delegated_service_publication_grant": {
			ContractsToCreate: func() []*ext_command.HandleSubmitContractArgs {
				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")

				ivBytes, err := iv.MarshalBinary()
				require.NoError(t, err)

				cc, err := contract.NewContent(&contract.NewContentArgs{
					Clock: testClock,
					Validity: &contract.NewValidityArgs{
						NotBefore: validNotBefore,
						NotAfter:  validNotBefore.Add(time.Hour).Truncate(time.Second),
					},
					GroupID: "fsc-local",
					Grants: []interface{}{
						&contract.NewGrantDelegatedServicePublicationArgs{
							Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerC.GetPeerID(),
								},
							},
							Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerA.GetPeerID(),
								},
							},
							Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
								Peer: &contract.NewPeerArgs{
									ID: peerB.GetPeerID(),
								},
								Name:     "parkeerrechten",
								Protocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
					},
					HashAlgorithm: contract.HashAlgSHA3_512,
					IV:            ivBytes,
					CreatedAt:     createdAt,
				})
				require.NoError(t, err)

				sigServiceConnection, err := cc.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), now)
				require.NoError(t, err)

				result := []*ext_command.HandleSubmitContractArgs{
					{
						ContractContent: &ext_command.ContractContentArgs{
							HashAlgorithm: string(models.HASHALGORITHMSHA3512),
							IV:            ivBytes,
							GroupID:       "fsc-local",
							NotBefore:     validNotBefore,
							NotAfter:      validNotBefore.Add(time.Hour),
							CreatedAt:     createdAt,
							Grants: []interface{}{
								&ext_command.GrantDelegatedServicePublicationArgs{
									DirectoryPeerID: peerA.GetPeerID(),
									DelegatorPeerID: peerC.GetPeerID(),
									ServicePeerID:   peerB.GetPeerID(),
									ServiceName:     "parkeerrechten",
									ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
								},
							},
						},
						Signature:       sigServiceConnection.JWS(),
						SubmittedByPeer: &ext_command.PeerArgs{ID: peerB.GetPeerID(), ManagerAddress: peerB.ManagerAddress, Name: peerB.GetName()},
						PeerCert:        peerB.CertBundle,
					},
				}

				return result
			}(),
			Params: &models.ListPendingContractsParams{},
			Want: func() []models.PendingContract {
				servicePublication := models.Grant{}

				err := servicePublication.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
					Directory: models.DirectoryPeer{PeerId: peerA.GetPeerID()},
					Service: models.ServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerB.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
					Delegator: models.DelegatorPeer{PeerId: peerC.GetPeerID()},
				})
				assert.NoError(t, err)

				result := []models.PendingContract{
					{
						Grants: []models.Grant{
							servicePublication,
						},
						ValidNotBefore: validNotBefore.Unix(),
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			externalHTTPServer, internalApp, externalApp, err := newService(t.Name())
			require.NoError(t, err)

			t.Cleanup(func() {
				externalHTTPServer.Close()
			})

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			// create a contract signed by peerA. This contract should not be returned by ListPendingContracts
			_, err = internalApp.Commands.CreateContract.Handle(context.Background(), &int_command.CreateContractHandlerArgs{
				HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
				IV:                "102635ef-7053-4151-8fd1-2537f99bee88",
				GroupID:           "fsc-local",
				ContractNotBefore: now,
				ContractNotAfter:  now.Add(time.Hour),
				Grants: []interface{}{
					&int_command.GrantServiceConnectionArgs{
						OutwayPublicKeyThumbprint: peerA.CertBundle.PublicKeyThumbprint(),
						OutwayPeerID:              peerA.GetPeerID(),
						Service: &int_command.NewDelegatedServiceArgs{
							Name:                       "parkeerrechten",
							PeerID:                     peerB.GetPeerID(),
							PublicationDelegatorPeerID: peerC.GetPeerID(),
						},
					},
				},
				CreatedAt:      now,
				AuditLogSource: source,
				AuthData:       authData,
			})
			require.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				createErr := externalApp.Commands.SubmitContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)
			}

			res, err := apiClient.ListPendingContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.PendingContracts, len(test.Want))

			for i, pendingContract := range res.JSON200.PendingContracts {
				assert.NotEmpty(t, pendingContract.Hash)
				assert.Regexp(t, "^\\$\\d\\$\\d\\$.{86}", pendingContract.Hash)
				assert.Equal(t, test.Want[i].ValidNotBefore, pendingContract.ValidNotBefore)
			}
		})
	}
}

func TestListPendingContracts_DefaultOrdering(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, internalApp, externalApp, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: internalApp,
		externalApp: externalApp,
		t:           t,
	})

	notBeforeA := now.Truncate(time.Second)
	notBeforeB := now.Truncate(time.Second).Add(5 * time.Minute)
	notBeforeC := now.Truncate(time.Second).Add(48 * time.Hour)

	contractService.CreateIncomingServiceConnectionWithNotBefore(notBeforeA)
	contractService.CreateIncomingServiceConnectionWithNotBefore(notBeforeB)
	contractService.CreateIncomingServiceConnectionWithNotBefore(notBeforeC)

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	res, err := apiClient.ListPendingContractsWithResponse(context.Background(), &models.ListPendingContractsParams{})
	assert.NoError(t, err)

	require.Equal(t, http.StatusOK, res.StatusCode())
	require.Len(t, res.JSON200.PendingContracts, 3)
	assert.Equal(t, notBeforeA.Unix(), res.JSON200.PendingContracts[0].ValidNotBefore, "first")
	assert.Equal(t, notBeforeB.Unix(), res.JSON200.PendingContracts[1].ValidNotBefore, "second")
	assert.Equal(t, notBeforeC.Unix(), res.JSON200.PendingContracts[2].ValidNotBefore, "third")
}

func TestListPendingContracts_Pagination(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app, externalApp, err := newService(t.Name())
	require.NoError(t, err)

	t.Cleanup(func() {
		externalHTTPServer.Close()
	})

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
		externalApp: externalApp,
		t:           t,
	})

	notBeforeA := now.Truncate(time.Second)
	notBeforeB := now.Truncate(time.Second).Add(5 * time.Minute)
	notBeforeC := now.Truncate(time.Second).Add(48 * time.Hour)
	notBeforeD := now.Truncate(time.Second).Add(96 * time.Hour)
	notBeforeE := now.Truncate(time.Second).Add(120 * time.Hour)

	contractService.CreateIncomingServiceConnectionWithNotBefore(notBeforeA)
	contractService.CreateIncomingServiceConnectionWithNotBefore(notBeforeB)
	contractService.CreateIncomingServiceConnectionWithNotBefore(notBeforeC)
	contractService.CreateIncomingServiceConnectionWithNotBefore(notBeforeD)
	contractService.CreateIncomingServiceConnectionWithNotBefore(notBeforeE)

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	limit := models.QueryPaginationLimit(2)
	sortOrder := models.SORTORDERASCENDING

	resFirstPage, err := apiClient.ListPendingContractsWithResponse(context.Background(), &models.ListPendingContractsParams{
		Limit:     &limit,
		SortOrder: &sortOrder,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, resFirstPage.StatusCode())
	assert.Len(t, resFirstPage.JSON200.PendingContracts, 2)
	assert.Equal(t, notBeforeA.Unix(), resFirstPage.JSON200.PendingContracts[0].ValidNotBefore, "first")
	assert.Equal(t, notBeforeB.Unix(), resFirstPage.JSON200.PendingContracts[1].ValidNotBefore, "second")

	assert.NotEmpty(t, resFirstPage.JSON200.Pagination.NextCursor)

	limitSecondPage := models.QueryPaginationLimit(4)
	resSecondPage, err := apiClient.ListPendingContractsWithResponse(context.Background(), &models.ListPendingContractsParams{
		Cursor:    &resFirstPage.JSON200.Pagination.NextCursor,
		Limit:     &limitSecondPage,
		SortOrder: &sortOrder,
	})
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, resSecondPage.StatusCode()) {
		t.Errorf("response body: %s", resSecondPage.Body)
	}

	assert.Len(t, resSecondPage.JSON200.PendingContracts, 3)
	assert.Equal(t, notBeforeC.Unix(), resSecondPage.JSON200.PendingContracts[0].ValidNotBefore, "third")
	assert.Equal(t, notBeforeD.Unix(), resSecondPage.JSON200.PendingContracts[1].ValidNotBefore, "fourth")
	assert.Equal(t, notBeforeE.Unix(), resSecondPage.JSON200.PendingContracts[2].ValidNotBefore, "fifth")
}
