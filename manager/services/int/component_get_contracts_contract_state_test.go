// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

//nolint:funlen,dupl // funlen: letting up the contracts requires more lines of code than allowed. dupl: the tests differ in subtle ways but do require a similar setup
package internalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

// the following tests verify that the returned state of the contract is correct

func TestListContracts_ContractState_Proposed(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 5, 23, 1, 2, 3, 4, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Truncate(time.Second).Add(-1 * time.Hour),
						ContractNotAfter:  now.Truncate(time.Second).Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt:      now.Truncate(time.Second),
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Hash: "$1$2$lpqCkwZsNWnKLnDOwWveETZaIvBFy1t3-Sra1jUbn6ZYb-c4mdr1Z_hqPwnCwr-qyxBuLaYMUKDN3Xp5gWwgvg",
					Service: models.ServicePublication{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684807323,
								NotBefore: 1684800123,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$JKjXOnzxHMVEXn8gPenrhVLPuTdtcPitz6eZNcvY9lSZm7gVm013uTKnIfmKSwPPlgIA3K4lD9XHGf6asqcD0g",
						Peers: []string{
							peerA.GetPeerID(),
							peerC.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), 0, nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			externalHTTPServer, app, _, err := newService(t.Name())
			require.NoError(t, err)

			t.Cleanup(func() {
				externalHTTPServer.Close()
			})

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				_, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}

func TestListContracts_ContractState_Rejected(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 5, 23, 1, 2, 3, 4, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Truncate(time.Second).Add(-1 * time.Hour),
						ContractNotAfter:  now.Truncate(time.Second).Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt:      now.Truncate(time.Second),
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Hash: "$1$2$lpqCkwZsNWnKLnDOwWveETZaIvBFy1t3-Sra1jUbn6ZYb-c4mdr1Z_hqPwnCwr-qyxBuLaYMUKDN3Xp5gWwgvg",
					Service: models.ServicePublication{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684807323,
								NotBefore: 1684800123,
							},
						},
						HasAccepted: true,
						HasRejected: true,
						HasRevoked:  false,
						Hash:        "$1$1$JKjXOnzxHMVEXn8gPenrhVLPuTdtcPitz6eZNcvY9lSZm7gVm013uTKnIfmKSwPPlgIA3K4lD9XHGf6asqcD0g",
						Peers: []string{
							peerA.GetPeerID(),
							peerC.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), 0, nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), 0, nowInUTC.Location()).Unix(),
								},
							},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEREJECTED,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			externalHTTPServer, app, _, err := newService(t.Name())
			require.NoError(t, err)

			t.Cleanup(func() {
				externalHTTPServer.Close()
			})

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				contractHash, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)

				rejectErr := app.Commands.RejectContract.Handle(context.Background(), &command.RejectContractHandlerArgs{
					ContentHash:    contractHash,
					AuditLogSource: source,
					AuthData:       authData,
				})
				assert.NoError(t, rejectErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}

func TestListContracts_ContractState_Revoked(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 5, 23, 1, 2, 3, 4, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Truncate(time.Second).Add(-1 * time.Hour),
						ContractNotAfter:  now.Truncate(time.Second).Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt:      now.Truncate(time.Second),
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Hash: "$1$2$lpqCkwZsNWnKLnDOwWveETZaIvBFy1t3-Sra1jUbn6ZYb-c4mdr1Z_hqPwnCwr-qyxBuLaYMUKDN3Xp5gWwgvg",
					Service: models.ServicePublication{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684807323,
								NotBefore: 1684800123,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  true,
						Hash:        "$1$1$JKjXOnzxHMVEXn8gPenrhVLPuTdtcPitz6eZNcvY9lSZm7gVm013uTKnIfmKSwPPlgIA3K4lD9XHGf6asqcD0g",
						Peers: []string{
							peerA.GetPeerID(),
							peerC.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), 0, nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), 0, nowInUTC.Location()).Unix(),
								},
							},
						},
						State: models.CONTRACTSTATEREVOKED,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			externalHTTPServer, app, _, err := newService(t.Name())
			require.NoError(t, err)

			t.Cleanup(func() {
				externalHTTPServer.Close()
			})

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				contractHash, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)

				rejectErr := app.Commands.RevokeContract.Handle(context.Background(), &command.RevokeContractHandlerArgs{
					ContentHash:    contractHash,
					AuditLogSource: source,
					AuthData:       authData,
				})
				assert.NoError(t, rejectErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
				t.Errorf("response body: %s", res.Body)
			}

			assert.Len(t, res.JSON200.Contracts, len(test.Want))
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}

func TestListContracts_ContractState_Expired(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Service: models.ServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Truncate(time.Second).Add(-2 * time.Hour),
						ContractNotAfter:  now.Truncate(time.Second).Add(-1 * time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerA.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt:      now.Truncate(time.Second),
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				notBefore := now.Add(-2 * time.Hour).Unix()
				notAfter := now.Add(-1 * time.Hour).Unix()

				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "102635ef-7053-4151-8fd1-2537f99bee79",
					GroupID:        "test-group",
					HashAlgorithm:  1,
					ValidNotBefore: notBefore,
					ValidNotAfter:  notAfter,
					ServicePublicationGrants: []testhash.ContractContentServicePublicationGrantArgs{{
						DirectoryPeerID: peerA.GetPeerID(),
						ServicePeerID:   peerA.GetPeerID(),
						ServiceName:     "parkeerrechten",
						ServiceProtocol: "PROTOCOL_TCP_HTTP_1.1",
					}},
					CreatedAt: now.Unix(),
				})
				require.NoError(t, err)

				servicePublicationGrant := models.Grant{}

				err = servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Hash: "$1$2$bjerdKrR8kWhk6CLAsd14BYyj48hPdNyExZppOWnOb0uMy-arvTTuRQwwPwcUuPj0KQycQg_FuQL5fUAZzanPA",
					Service: models.ServicePublication{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  notAfter,
								NotBefore: notBefore,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        contentHash,
						Peers: []string{
							peerA.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), 0, nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEEXPIRED,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			externalHTTPServer, app, _, err := newService(t.Name())
			require.NoError(t, err)

			t.Cleanup(func() {
				externalHTTPServer.Close()
			})

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				_, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))

			for i, c := range res.JSON200.Contracts {
				for j, g := range c.Content.Grants {
					wantGrant, grantErr := test.Want[i].Content.Grants[j].AsGrantServicePublication()
					assert.NoError(t, grantErr)

					gotGrant, grantErr := g.AsGrantServicePublication()
					assert.NoError(t, grantErr)

					assert.Equal(t, wantGrant, gotGrant)
				}
			}

			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}

func TestListContracts_ContractState_Valid(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Service: models.ServicePublication{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: testClock.Now().Truncate(time.Second).Add(-1 * time.Hour),
						ContractNotAfter:  testClock.Now().Truncate(time.Second).Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerA.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt:      testClock.Now().Truncate(time.Second),
						AuditLogSource: source,
						AuthData:       authData,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Hash: "$1$2$bjerdKrR8kWhk6CLAsd14BYyj48hPdNyExZppOWnOb0uMy-arvTTuRQwwPwcUuPj0KQycQg_FuQL5fUAZzanPA",
					Service: models.ServicePublication{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
				})
				assert.NoError(t, err)

				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					IV:             "102635ef-7053-4151-8fd1-2537f99bee79",
					GroupID:        "test-group",
					HashAlgorithm:  1,
					ValidNotBefore: testClock.Now().Add(-1 * time.Hour).Unix(),
					ValidNotAfter:  testClock.Now().Add(time.Hour).Unix(),
					ServicePublicationGrants: []testhash.ContractContentServicePublicationGrantArgs{{
						DirectoryPeerID: peerA.GetPeerID(),
						ServicePeerID:   peerA.GetPeerID(),
						ServiceName:     "parkeerrechten",
						ServiceProtocol: "PROTOCOL_TCP_HTTP_1.1",
					}},
					CreatedAt: testClock.Now().Unix(),
				})
				require.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: testClock.Now().Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  testClock.Now().Add(time.Hour).Unix(),
								NotBefore: testClock.Now().Add(-1 * time.Hour).Unix(),
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        contentHash,
						Peers: []string{
							peerA.GetPeerID(),
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), 0, nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEVALID,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			externalHTTPServer, app, _, err := newService(t.Name())
			require.NoError(t, err)

			t.Cleanup(func() {
				externalHTTPServer.Close()
			})

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				_, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))

			for i, c := range res.JSON200.Contracts {
				for j, g := range c.Content.Grants {
					wantGrant, grantErr := test.Want[i].Content.Grants[j].AsGrantServicePublication()
					assert.NoError(t, grantErr)

					gotGrant, grantErr := g.AsGrantServicePublication()
					assert.NoError(t, grantErr)

					assert.Equal(t, wantGrant, gotGrant)
				}
			}

			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}
