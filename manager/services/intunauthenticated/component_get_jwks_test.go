// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestGetJwk(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		Setup            func(*internalapp.Application, clock.Clock)
		WantStatusCode   int
		WantAmountOfKeys int
	}{
		"happy_flow": {
			Setup: func(intApp *internalapp.Application, _ clock.Clock) {
				err := intApp.Commands.CreateCertificate.Handle(context.Background(), peerA.CertBundle.Cert().Certificate)
				require.NoError(t, err)
			},
			WantStatusCode:   http.StatusOK,
			WantAmountOfKeys: 1,
		},
		"do_not_include_expired_certificates": {
			Setup: func(intApp *internalapp.Application, c clock.Clock) {
				err := intApp.Commands.CreateCertificate.Handle(context.Background(), peerA.CertBundle.Cert().Certificate)
				require.NoError(t, err)

				// the test certificates created on January 2024 expire in 200 years
				const yearsUntilExpired = 201

				c.UpdateTime(time.Date(c.Now().Year()+yearsUntilExpired, c.Now().Month(), c.Now().Day(), c.Now().Hour(), c.Now().Minute(), c.Now().Second(), c.Now().Nanosecond(), c.Now().Location()))
			},
			WantStatusCode:   http.StatusOK,
			WantAmountOfKeys: 0,
		},
		"happy_flow_ec_cert": {
			Setup: func(intApp *internalapp.Application, c clock.Clock) {
				orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), "org-nlx-test-a-ec")
				require.NoError(t, err)

				err = intApp.Commands.CreateCertificate.Handle(context.Background(), orgCertBundle.Cert().Certificate)
				require.NoError(t, err)
			},
			WantStatusCode:   http.StatusOK,
			WantAmountOfKeys: 1,
		},
	}

	for testName, tt := range tests {
		t.Run(testName, func(t *testing.T) {
			t.Parallel()

			// Arrange
			internalUnauthenticatedServer, intApp, _, c := newService(t.Name())
			defer internalUnauthenticatedServer.Close()

			apiClient, err := createInternalUnauthenticatedManagerAPIClient(internalUnauthenticatedServer.URL)
			assert.NoError(t, err)

			tt.Setup(intApp, c)

			// Act
			res, err := apiClient.GetJSONWebKeySetWithResponse(context.Background())

			// Assert
			require.NoError(t, err)

			if !assert.Equal(t, tt.WantStatusCode, res.StatusCode()) {
				t.Errorf("response body: %s", res.Body)
			}

			require.Len(t, res.JSON200.Keys, tt.WantAmountOfKeys)
		})
	}
}
