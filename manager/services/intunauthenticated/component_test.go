// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	crypto_tls "crypto/tls"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres"
	externalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/pkg/autosigner/neverallow"
	restport_unauthenticated "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/intunauthenticated/rest"
	api_unauthenticated "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/intunauthenticated/rest/api/server"
	externalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/ext"
	internalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/int"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var (
	peerA         *peerInfo
	peerB         *peerInfo
	peerC         *peerInfo
	peerD         *peerInfo
	peerDirectory *peerInfo
)

func TestMain(m *testing.M) {
	var err error

	peerA, err = newPeerInfo(testingutils.NLXTestPeerA, "https://manager.org-a.nlx.localhost:443")
	if err != nil {
		log.Fatal(err)
	}

	peerB, err = newPeerInfo(testingutils.NLXTestPeerB, "https://manager.org-b.nlx.localhost:443")
	if err != nil {
		log.Fatal(err)
	}

	peerC, err = newPeerInfo(testingutils.NLXTestPeerC, "https://manager.org-c.nlx.localhost:443")
	if err != nil {
		log.Fatal(err)
	}

	peerD, err = newPeerInfo(testingutils.NLXTestPeerD, "https://manager.org-d.nlx.localhost:443")
	if err != nil {
		log.Fatal(err)
	}

	peerDirectory, err = newPeerInfo(testingutils.NLXTestOrgDirectory, "https://directory.nlx.localhost:443")
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

//nolint:funlen,gocyclo,gocritic // this is a test
func newService(testName string) (*httptest.Server, *internalapp.Application, *externalapp.Application, clock.Clock) {
	return newServiceWithCustomManagerFactory(testName, &fakeManagerFactory{})
}

//nolint:funlen,gocritic // this is a long function because there is a lot to set up
func newServiceWithCustomManagerFactory(testName string, managerFactory manager.Factory) (*httptest.Server, *internalapp.Application, *externalapp.Application, clock.Clock) {
	logger := discardlogger.New()

	nowInUTC := time.Now()
	testClock := testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))

	peerCertBundle := peerA.CertBundle

	postgresqlRepository, err := setupPostgresqlRepository(testName, testClock)
	if err != nil {
		log.Fatalf("failed to setup postgresql repository: %v", err)
	}

	selfPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerCertBundle.GetPeerInfo().SerialNumber,
		Name:           peerCertBundle.GetPeerInfo().Name,
		ManagerAddress: "https://manager.org-a.nlx.localhost:443",
	})
	if err != nil {
		log.Fatalf("failed to create self peer: %v", err)
	}

	controllerRepository := newFakeController()

	auditLogger, err := auditlog.New(&auditlog.NewArgs{
		AuditLogger: newFakeAuditlog(),
		GroupID:     "fsc-local",
		Clock:       testClock,
		IDGenerator: uuidgenerator.New(),
	})
	if err != nil {
		log.Fatalf("failed to create auditlogger: %v", err)
	}

	contractDistributionService, err := services.NewContractDistributionService(&services.NewContractDistributionServiceArgs{
		Context:                              context.Background(),
		Logger:                               logger,
		Clock:                                testClock,
		FailedContractDistributionRepository: postgresqlRepository,
	})
	if err != nil {
		log.Fatalf("failed to setup contract distribution service: %s", err)
	}

	peersCommunicationService, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
		Ctx:                         context.Background(),
		Logger:                      logger,
		LocalRepo:                   postgresqlRepository,
		ManagerFactory:              managerFactory,
		SelfPeerID:                  selfPeer.ID(),
		ContractDistributionService: contractDistributionService,
		Clock:                       testClock,
	})
	if err != nil {
		log.Fatalf("failed to setup peers adapter: %s", err)
	}

	txlogRepository := newFakeTxLog()

	internalApp, err := internalservice.NewApplication(&internalservice.NewApplicationArgs{
		Context:                     context.Background(),
		Clock:                       testClock,
		Logger:                      logger,
		Repository:                  postgresqlRepository,
		PeersCommunicationService:   peersCommunicationService,
		ContractDistributionService: contractDistributionService,
		TXLog:                       txlogRepository,
		SelfPeer:                    selfPeer,
		TrustedExternalRootCAs:      peerCertBundle.RootCAs(),
		SignatureCertificate:        peerCertBundle.Cert(),
		ControllerRepository:        controllerRepository,
		ManagerFactory:              managerFactory,
		GroupID:                     "fsc-local",
		DirectoryPeerManagerAddress: "https://directory.nlx.local:443",
		DirectoryPeerID:             selfPeer.ID().Value(),
		Auditlogger:                 auditLogger,
		InternalCertificate:         peerCertBundle,
	})
	if err != nil {
		log.Fatalf("failed to setup application: %s", err)
	}

	externalApp, err := externalservice.NewApplication(&externalservice.NewApplicationArgs{
		Context:                   context.Background(),
		Clock:                     testClock,
		Logger:                    logger,
		Repository:                postgresqlRepository,
		ControllerRepository:      controllerRepository,
		TXLogRepository:           txlogRepository,
		PeersCommunicationService: peersCommunicationService,
		GroupID:                   "fsc-local",
		SelfPeer:                  selfPeer,
		TrustedRootCAs:            peerCertBundle.RootCAs(),
		AutoSignCertificate:       peerD.CertBundle.Cert(),
		TokenSignCertificate:      peerB.CertBundle.Cert(),
		TokenTTL:                  time.Hour,
		IsTxlogDisabled:           true,
		AutoSigner:                neverallow.New(),
	})
	if err != nil {
		log.Fatalf("failed to setup external application: %s", err)
	}

	server, err := restport_unauthenticated.New(&restport_unauthenticated.NewArgs{
		Logger: logger,
		App:    internalApp,
	})
	if err != nil {
		log.Fatalf("failed to setup unauthenticated rest port: %s", err)
	}

	srv := httptest.NewUnstartedServer(server.Handler())
	srv.StartTLS()

	return srv, internalApp, externalApp, testClock
}

func createInternalUnauthenticatedManagerAPIClient(managerURL string) (*api_unauthenticated.ClientWithResponses, error) {
	return api_unauthenticated.NewClientWithResponses(managerURL, func(c *api_unauthenticated.Client) error {
		t := &http.Transport{
			TLSClientConfig: &crypto_tls.Config{InsecureSkipVerify: true}, //nolint:gosec // no authentication required for this client
		}
		c.Client = &http.Client{
			Transport: t,
		}

		return nil
	})
}

func setupPostgresqlRepository(testName string, c clock.Clock) (contract.Repository, error) {
	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)
	dbName = strings.Replace(dbName, "test", "", 1) // strip 'test'-prefix
	dbName = fmt.Sprintf("int_unauthenticated_%s", dbName)
	dbName = strings.ReplaceAll(dbName, "/", "_")

	// via https://stackoverflow.com/a/27865772/363448
	const maxLengthDatabaseName = 63

	if len(dbName) > maxLengthDatabaseName {
		return nil, fmt.Errorf("database name too long (%d > 63 characters): %q", len(dbName), dbName)
	}

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		return nil, fmt.Errorf("failed to setup test database: %v", err)
	}

	db, err := postgresadapter.NewConnection(context.Background(), testDB)
	if err != nil {
		return nil, fmt.Errorf("failed to create db connection: %v", err)
	}

	err = postgresadapter.PerformMigrations(testDB)
	if err != nil {
		return nil, fmt.Errorf("failed to perform dbmigrations: %v", err)
	}

	postgresqlRepository, err := postgresadapter.New(peerA.CertBundle.RootCAs(), db, c)
	if err != nil {
		return nil, fmt.Errorf("failed to setup postgresql database: %s", err)
	}

	return postgresqlRepository, nil
}
