// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

//nolint:unused // most methods are unused but added to comply with the Manager interface
package internalservice_test

import (
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
)

type fakeManagerFactory struct {
	clock clock.Clock
}

func (f *fakeManagerFactory) New(_, address string) (manager.Manager, error) {
	if address == peerA.ManagerAddress {
		return &fakeManager{
			address: address,
			orgCert: peerA.CertBundle,
			clock:   f.clock,
		}, nil
	}

	if address == peerB.ManagerAddress {
		return &fakeManager{
			address: address,
			orgCert: peerB.CertBundle,
			clock:   f.clock,
		}, nil
	}

	if address == peerC.ManagerAddress {
		return &fakeManager{
			address: address,
			orgCert: peerC.CertBundle,
			clock:   f.clock,
		}, nil
	}

	if address == peerD.ManagerAddress {
		return &fakeManager{
			address: address,
			orgCert: peerD.CertBundle,
			clock:   f.clock,
		}, nil
	}

	if address == peerDirectory.ManagerAddress {
		return &fakeManager{
			address: address,
			orgCert: peerDirectory.CertBundle,
			clock:   f.clock,
		}, nil
	}

	return nil, fmt.Errorf("unknown address '%s', unable to setup manager client", address)
}
