// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetHealth(t *testing.T) {
	t.Parallel()

	// Arrange
	internalUnauthenticatedServer, _, _, _ := newService(t.Name()) //nolint:dogsled // we can create a struct in a next refactoring step
	defer internalUnauthenticatedServer.Close()

	apiClient, err := createInternalUnauthenticatedManagerAPIClient(internalUnauthenticatedServer.URL)
	assert.NoError(t, err)

	// Act
	res, err := apiClient.GetHealthWithResponse(context.Background())

	// Assert
	require.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}
}
