// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

//nolint:unused // most methods are unused but added to comply with the Manager interface
package internalservice_test

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type fakeManager struct {
	address string
	orgCert *tls.CertificateBundle
	clock   clock.Clock
}

func (m *fakeManager) GetCertificates(_ context.Context) (*contract.PeerCertificates, error) {
	peerCert, err := contract.NewPeerCertFromCertificate(m.clock, m.orgCert.RootCAs(), m.orgCert.Cert().Certificate)
	if err != nil {
		log.Printf("failed to create peer cert: %v", err)
	}

	peerCerts := &contract.PeerCertificates{
		peerCert.CertificateThumbprint(): peerCert,
	}

	return peerCerts, nil
}

func (m *fakeManager) GetPeers(_ context.Context, peerIDsFilter contract.PeersIDs) (contract.Peers, error) {
	orgAPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerA.GetPeerID(),
		Name:           peerA.GetName(),
		ManagerAddress: peerA.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	orgBPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerB.GetPeerID(),
		Name:           peerB.GetName(),
		ManagerAddress: peerB.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	orgCPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerC.GetPeerID(),
		Name:           peerC.GetName(),
		ManagerAddress: peerC.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	orgDPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerD.GetPeerID(),
		Name:           peerD.GetName(),
		ManagerAddress: peerD.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	peerIDs := contract.Peers{
		orgAPeer.ID(): orgAPeer,
		orgBPeer.ID(): orgBPeer,
		orgCPeer.ID(): orgCPeer,
		orgDPeer.ID(): orgDPeer,
	}

	if len(peerIDsFilter) == 0 {
		return peerIDs, nil
	}

	result := contract.Peers{}

	for peerID := range peerIDsFilter {
		peer, ok := peerIDs[peerID]
		if !ok {
			continue
		}

		result[peerID] = peer
	}

	return result, nil
}

func (m *fakeManager) Announce(_ context.Context) error {
	panic(fmt.Sprintf("unintended call to announce %s", m.address))
}

func (m *fakeManager) GetContracts(_ context.Context, _ *contract.GrantType, _ *[]string) ([]*contract.Contract, error) {
	panic(fmt.Sprintf("unintended call to get contracts %s", m.address))
}

func (m *fakeManager) SubmitContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeManager) AcceptContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeManager) RejectContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeManager) RevokeContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *fakeManager) GetPeerInfo(_ context.Context) (*contract.Peer, error) {
	panic(fmt.Sprintf("unintended call to get peer info %s", m.address))
}

func (m *fakeManager) GetServices(_ context.Context, _ *contract.PeerID, _ string) ([]*contract.Service, error) {
	return []*contract.Service{
		{
			ContractIV:         "",
			PeerID:             peerA.GetPeerID(),
			PeerManagerAddress: peerA.ManagerAddress,
			PeerName:           peerA.GetName(),
			DelegatorPeerID:    "",
			DelegatorPeerName:  "",
			Name:               "parkeerrechten",
			Protocol:           0,
		},
		{
			ContractIV:         "",
			PeerID:             peerA.GetPeerID(),
			PeerManagerAddress: peerA.ManagerAddress,
			PeerName:           peerA.GetName(),
			DelegatorPeerID:    peerB.GetPeerID(),
			DelegatorPeerName:  peerB.GetName(),
			Name:               "parkeerrechten-delegated",
			Protocol:           0,
		},
	}, nil
}

func (m *fakeManager) GetToken(_ context.Context, _ contract.PeerID, _ string) (string, error) {
	panic("unintended call to get token")
}

func (m *fakeManager) GetTXLogRecords(_ context.Context) (contract.TXLogRecords, error) {
	panic(fmt.Sprintf("unintended call to get txlog records %s", m.address))
}
