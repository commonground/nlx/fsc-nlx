// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package app

import (
	"gitlab.com/commonground/nlx/fsc-nlx/directory-ui/app/query"
)

type Application struct {
	Queries Queries
}

type Queries struct {
	ListServices     *query.ListServicesHandler
	ListParticipants *query.ListParticipantsHandler
}
