// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/directory-ui/adapters/directory"
)

type ListParticipantsHandler struct {
	participantRepository directory.Repository
}

func NewListParticipantsHandler(repository directory.Repository) *ListParticipantsHandler {
	return &ListParticipantsHandler{
		participantRepository: repository,
	}
}

func (l *ListParticipantsHandler) Handle(ctx context.Context, peerNameFilter string) ([]*Participant, error) {
	participants, err := l.participantRepository.ListParticipants(ctx)
	if err != nil {
		return nil, err
	}

	result := make([]*Participant, 0)

	for _, s := range participants {
		if peerNameFilter == "" || strings.Contains(strings.ToLower(s.Name), strings.ToLower(peerNameFilter)) {
			result = append(result, &Participant{
				ID:   s.ID,
				Name: s.Name,
			})
		}
	}

	return result, nil
}
