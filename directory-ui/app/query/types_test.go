// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query_test

import (
	"testing"

	mock_repository "gitlab.com/commonground/nlx/fsc-nlx/directory-ui/adapters/directory/mock"
)

type mocks struct {
	repository *mock_repository.MockRepository
}

func newMocks(t *testing.T) *mocks {
	return &mocks{
		repository: mock_repository.NewMockRepository(t),
	}
}
