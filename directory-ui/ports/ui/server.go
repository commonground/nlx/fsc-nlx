// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"embed"
	"fmt"
	"log"
	"net/http"
	"path"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/common/slogerrorwriter"
	"gitlab.com/commonground/nlx/fsc-nlx/directory-ui/app"
)

//go:embed templates/**
var tplFolder embed.FS

type Server struct {
	staticPath  string
	baseURLPath string
	app         *app.Application
	logger      *logger.Logger
	httpServer  *http.Server
	basePage    *BasePage
}

func New(staticPath, baseURLPath string, lgr *logger.Logger, a *app.Application) (*Server, error) {
	if lgr == nil {
		return nil, fmt.Errorf("logger cannot be nil")
	}

	if a == nil {
		return nil, fmt.Errorf("app cannot be nil")
	}

	if baseURLPath == "" {
		return nil, fmt.Errorf("base URL path can not be empty")
	}

	basePage, err := NewBasePage(staticPath, baseURLPath)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create base page")
	}

	server := &Server{
		staticPath:  staticPath,
		baseURLPath: baseURLPath,
		logger:      lgr,
		app:         a,
		basePage:    basePage,
	}

	return server, nil
}

func (s *Server) ListenAndServe(address string) error {
	r := chi.NewRouter()
	r.Get("/", s.overviewHandler)
	r.Post("/search-services", s.searchServicesHandler)
	r.Get("/participants", s.participantsHandler)
	r.Post("/search-participants", s.searchParticipantsHandler)
	r.Get("/{peerID}/{serviceName}/delegator/{delegatorID}", s.serviceDetailHandler)
	r.Get("/{peerID}/{serviceName}", s.serviceDetailHandler)
	r.Get("/health", func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		w.WriteHeader(http.StatusOK)
	})

	filesDir := http.Dir(s.staticPath)
	r.Handle("/*", http.StripPrefix(path.Join("/", s.baseURLPath, "/"), http.FileServer(filesDir)))

	const readHeaderTimeout = 5 * time.Second

	s.httpServer = &http.Server{
		Addr:              address,
		Handler:           r,
		ReadHeaderTimeout: readHeaderTimeout,
		ErrorLog:          log.New(slogerrorwriter.New(s.logger.Logger), "", 0),
	}

	err := s.httpServer.ListenAndServe()
	if err != http.ErrServerClosed {
		return err
	}

	return nil
}

func (s *Server) Shutdown(ctx context.Context) error {
	err := s.httpServer.Shutdown(ctx)
	if !errors.Is(err, http.ErrServerClosed) {
		return err
	}

	return nil
}
