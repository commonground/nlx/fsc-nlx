// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"net/http"
)

func (s *Server) participantsHandler(w http.ResponseWriter, _ *http.Request) {
	participants, err := s.app.Queries.ListParticipants.Handle(context.Background(), "")
	if err != nil {
		s.logger.Error("list participants", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	searchResults := make([]*ParticipantsSearchResult, len(participants))

	for i, participant := range participants {
		searchResults[i] = &ParticipantsSearchResult{
			PeerID:   participant.ID,
			PeerName: participant.Name,
		}
	}

	page := participantsPage{
		BasePage:    s.basePage,
		Location:    "/participants",
		BaseURLPath: s.baseURLPath,
		Introduction: participantsPageIntroduction{
			Title:       "Directory Deelnemers",
			Description: "In dit overzicht vindt u alle deelnemers aan dit ecosysteem.",
		},
		SearchResults: searchResults,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render participants page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}
