// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"net/http"
)

func (s *Server) overviewHandler(w http.ResponseWriter, _ *http.Request) {
	services, err := s.app.Queries.ListServices.Handle(context.Background(), "")
	if err != nil {
		s.logger.Error("overview handler, could not execute list services query", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	searchResults := make([]*ServicesSearchResult, len(services))

	for i, srv := range services {
		ssr := &ServicesSearchResult{
			ServiceName: srv.Name,
			Provider: Peer{
				ID:   srv.Provider.ID,
				Name: srv.Provider.Name,
			},
		}

		if srv.IsDelegatedPublication() {
			ssr.Delegator = &Peer{
				ID:   srv.Delegator.ID,
				Name: srv.Delegator.Name,
			}
		}

		searchResults[i] = ssr
	}

	page := overviewPage{
		BasePage:    s.basePage,
		Location:    "/",
		BaseURLPath: s.baseURLPath,
		Introduction: overviewPageIntroduction{
			Title:       "Directory",
			Description: "In deze directory vindt u een overzicht van alle beschikbare API’s per omgeving.",
		},
		SearchResults: searchResults,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render overview page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}
