// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

type ServicesSearchResults []*ServicesSearchResult

type ServicesSearchResult struct {
	ServiceName string
	Provider    Peer
	Delegator   *Peer
}

func (s *ServicesSearchResult) IsDelegatedPublication() bool {
	return s.Delegator != nil
}

type Peer struct {
	ID   string
	Name string
}

type ParticipantsSearchResults []*ParticipantsSearchResult

type ParticipantsSearchResult struct {
	PeerName string
	PeerID   string
}
