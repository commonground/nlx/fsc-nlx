# The HOST_IP variables in this config are replaced by the real host machine ip via the entrypoint.sh script

user nobody

pidfile /var/run/sniproxy.pid

access_log {
	filename /dev/stdout
	priority notice
}

error_log {
	filename /dev/stderr
}

listener 80 {
    proto http
    table http_hosts
    bad_requests log
}

listener 443 {
    proto tls
    table https_hosts
    bad_requests log
}

listener 444 {
    proto tls
    table https_unauthenticated_hosts
    bad_requests log
}

listener 8443 {
    proto tls
    table manager_proxy_hosts
    bad_requests log
}

table http_hosts {
    # Org A
    controller.organization-a.nlx.localhost HOST_IP:3011
    outway.organization-a.nlx.localhost HOST_IP:7605
    outway-2.organization-a.nlx.localhost HOST_IP:7608
    txlog-api.organization-a.nlx.localhost HOST_IP:7612

    # Org B
    controller.organization-b.nlx.localhost HOST_IP:3021
    txlog-api.organization-b.nlx.localhost HOST_IP:7706

    # Org C
    controller.organization-c.nlx.localhost HOST_IP:3031
    outway.organization-c.nlx.localhost HOST_IP:7802
    txlog-api.organization-c.nlx.localhost HOST_IP:7806

    # Org D
    controller.organization-d.nlx.localhost HOST_IP:3051
    txlog-api.organization-d.nlx.localhost HOST_IP:7906
}

table https_hosts {
    directory.shared.nlx.localhost HOST_IP:7500

    # Org A
    controller-api.organization-a.nlx.localhost HOST_IP:7600
    inway.organization-a.nlx.localhost HOST_IP:7602
    outway.organization-a.nlx.localhost HOST_IP:7606
    outway-2.organization-a.nlx.localhost HOST_IP:7609
    txlog-api.organization-a.nlx.localhost HOST_IP:7611
    auditlog.organization-a.nlx.localhost HOST_IP:7620
    manager.organization-a.nlx.localhost HOST_IP:7613

    # Org B
    controller-api.organization-b.nlx.localhost HOST_IP:7700
    inway.organization-b.nlx.localhost HOST_IP:7702
    txlog-api.organization-b.nlx.localhost HOST_IP:7705
    auditlog.organization-b.nlx.localhost HOST_IP:7720
    manager.organization-b.nlx.localhost HOST_IP:7707

    # Org C
    controller-api.organization-c.nlx.localhost HOST_IP:7800
    outway.organization-c.nlx.localhost HOST_IP:7803
    txlog-api.organization-c.nlx.localhost HOST_IP:7805
    auditlog.organization-c.nlx.localhost HOST_IP:7820
    manager.organization-c.nlx.localhost HOST_IP:7807

    # Org D
    controller-api.organization-d.nlx.localhost HOST_IP:7903
    txlog-api.organization-d.nlx.localhost HOST_IP:7905
    auditlog.organization-d.nlx.localhost HOST_IP:7920
    manager.organization-d.nlx.localhost HOST_IP:7907
}

table https_unauthenticated_hosts {
    directory.shared.nlx.localhost HOST_IP:7503

    # Org A
    manager.organization-a.nlx.localhost HOST_IP:7616

    # Org B
    manager.organization-b.nlx.localhost HOST_IP:7709

    # Org C
    manager.organization-c.nlx.localhost HOST_IP:7810

    # Org D
    manager.organization-d.nlx.localhost HOST_IP:7910
}

table manager_proxy_hosts {
    directory.shared.nlx.localhost HOST_IP:7501

    # Org A
    manager.organization-a.nlx.localhost HOST_IP:7614

    # Org B
    manager.organization-b.nlx.localhost HOST_IP:7708

    # Org C
    manager.organization-c.nlx.localhost HOST_IP:7808

    # Org D
    manager.organization-d.nlx.localhost HOST_IP:7908
}
