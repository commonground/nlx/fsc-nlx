<img alt="FSC-NLX" src="logo.png" width="200">

FSC-NLX is the reference implementation of [the Federated Service Connectivity standard (FSC)](https://commonground.gitlab.io/standards/fsc/), an open source inter-organizational system facilitating federated authentication, secure connecting and protocolling in a large-scale, dynamic API landscape.

This repository contains all the components required to act out the [FSC-NLX Product Vision](https://docs.fsc.nlx.io/introduction).

## Implementation choices

FSC-NLX is an implementation of FSC and the implementation makes certain choices that will determine if the implementation is suitable for your use-case.

The implementation choices:

1. The Peer ID is determined by the `subject.serialNumber` field of the X.509 certificate.
2. The Peer ID must be a valid [OIN](https://www.logius.nl/domeinen/toegang/organisatie-identificatienummer)
3. The Peer name is determined by the `subject.organization` field of the X.509 certificate.
4. The [Logging extension](https://commonground.gitlab.io/standards/fsc/logging/draft-fsc-logging-00.html) is mandatory.
5. An exponential back off is used as retry mechanism for contract and signature propagation
6. Support for CRLs is build-in.
7. The TLS config is in accordance with https://english.ncsc.nl/publications/publications/2021/january/19/it-security-guidelines-for-transport-layer-security-2.1. The following cipher suites are supported; `TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384`, `TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384`, `TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256`, `TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256`, `TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305, TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305`

## Developing on FSC-NLX

Please find the latest documentation for using FSC-NLX on [docs.fsc.nlx.io](https://docs.fsc.nlx.io). This is a good place to start if you would like to develop an application or service that uses or provides API access over FSC-NLX.

## Questions and contributions

Read more on how to ask questions, file bugs and contribute code and documentation in [`CONTRIBUTING.md`](CONTRIBUTING.md).

## Troubleshooting

If you are running into other issues, please [post an Issue on GitLab](https://gitlab.com/commonground/nlx/fsc-nlx/issues).

## Building and running an FSC-NLX network locally

The FSC-NLX project consists of multiple components that together make up the entire FSC-NLX platform. Some components run as centralized FSC-NLX services, others run on-premise at organizations. All components are maintained in a single repository. This means that a developer has all the tools and code to build and test the complete FSC-NLX platform in a single repository. It simplifies version and dependency management and allows changes that affect multiple components to be combined in a single feature branch and merge-request.

If you want to develop locally, or run your own FSC-NLX network, you will likely want to start all the components.

### Cloning

Clone FSC-NLX in your workspace.

```bash
git clone https://gitlab.com/commonground/nlx/fsc-nlx
cd fsc-nlx
```

### Development setup

Make sure you have installed the following tools:

-   [Docker Desktop / Docker engine](https://docs.docker.com/install/)
-   [Docker Compose](https://docs.docker.com/compose/install/)
-   [Golang](https://golang.org/doc/install)
-   [NodeJS LTS](https://nodejs.org/en/download/)
-   [Sass](https://sass-lang.com/install)
-   [modd](https://github.com/cortesi/modd)
-   [hostctl](https://guumaster.github.io/hostctl/docs/installation/)
-   Make

Install the npm dependencies by running:

```bash
(cd docs/website && npm install)
(cd controller/ports/administration/ui/assets && npm install)
(cd directory-ui/ports/ui/assets && npm install)
```

Make sure the TLS key files have the correct permissions to run the FSC-NLX components

```bash
./pki/fix-permissions.sh
```

Update the `/etc/hosts` file on your system:

```shell
make sync-hosts
```

Before you can start the services you are required to install [delve](https://github.com/go-delve/delve).
Run `go install github.com/go-delve/delve/cmd/dlv@latest`.

To start the services in development daemons with up-to-date databases, run: `./scripts/start-development.sh`. Make sure Docker is running.

During the starting routines of the services, you might see a few services erroring that are dependent on a service that has not yet been started.
This is expected behavior and will resolve itself within 5 seconds.

Services will reload automatically when the code changes and is saved.

Visit the Controllers:

-   [Gemeente Stijns (Organization A)](http://controller.organization-a.nlx.local:3011) (HTTP: 3011) (credentials: admin@nlx.local / development)
-   [RvRD (Organization B)](http://controller.organization-b.nlx.local:3021) (HTTP: 3021)
-   [Vergunningsoftware BV (Organization C)](http://controller.organization-c.nlx.local:3031) (HTTP: 3031)
-   [Directory (Shared)](http://controller.directory.nlx.local:3041) (HTTP: 3041)

The Directory:

-   [directory-ui](http://directory.shared.nlx.local:7402/) (HTTP: 7402)

To start the documentation website locally:

-   `(cd docs/website && npm start)` to run the [docs](http://localhost:3002) (HTTP: 3002)

## Translations

Run `earthly +i18n` to generate translation files. Translations are stored in `out.gotext.json` and `messages.gotext.json`.
Copy the content from `out.gotext.json` to `messages.gotext.json` and add the translation in the `messages.gotext.json` file.
Run `earthly +i18n`. The updated translations are added to `out.gotext.json`. You can now remove `message.gotext.json`.

## Integration tests

We have integration tests using Golang and PostgreSQL.

Specify `POSTGRES_DSN`. If not set, `postgres://postgres:postgres@localhost:5432?sslmode=disable` will be used.
The tests will try to connect with Postgres and create a new database used for the tests.

```shell
go test ./... -tags=integration
```

## Deploying and releasing

The [CI system of GitLab](https://gitlab.com/commonground/nlx/fsc-nlx/pipelines) builds every push to the main branch and creates a release to Docker, tagging it with the short git commit hash.
When a release is successful, it also gets deployed to the acceptance environment.

When a git tag is pushed, GitLab builds and allows you to manually deploy it to the demo environment.

For branches prefixed with `review/` (f.e. `review/feature-name`), a review environment will automatically be created for a shared testing environment.

### Adding a new feature to current release

1. `git checkout main`
1. `git checkout -b review/my-awesome-feature`
1. commit feature on `review/my-awesome-feature` branch
1. merge `review/my-awesome-feature` to `main`
1. `git branch -d review/my-awesome-feature`
1. Run the Semantic Release job from the latest pipeline on `main`

### Creating a new major v1 -> v2

1. Ensure the commit(s) containing the breaking change(s) are on `main` using the
'Adding a new feature to current release' flow.
The commit message of breaking changes should contain `BREAKING CHANGE:`.
See https://semantic-release.gitbook.io/semantic-release#commit-message-format
1. Run the Semantic Release job from the latest pipeline on `main`

### Patching the previous major v1

1. `git checkout v1.0.0` (tag)
1. `git checkout -b patch/fix-bug`
1. `git commit -m "my-awesome-fix"`
1. `git push`
1. Run the Semantic Release job from the latest pipeline on `patch/fix-bug`
1. `git branch -d patch/fix-bug`

### Releasing a Release Candidate of the next major release

1. Create a new branch named `beta` from `main`
1. Commit features for the next major release on the `beta` branch
1. Run the Semantic Release job from the latest pipeline on the `beta` branch
1. If the Release Candidate has been verified and no issues are found, merge the `beta` branch into `main`
1. Remove the `beta` branch

### Semantic Release

In order for Semantic Release to be able to create a Git tag and push the updated changelog, a GitLab Access Token should be created.

The token can be created via https://gitlab.com/commonground/nlx/fsc-nlx/-/settings/access_tokens

```text
Token name: semantic-release
Role: Maintainer
Scopes: api, write_repository
```

Create a GitLab CI/CD variable named 'GITLAB_TOKEN' via https://gitlab.com/commonground/nlx/fsc-nlx/-/settings/ci_cd using the generated Access Token as the value.

Please don't forget to store the Access Token in the VNG 1Password account.

## Live environments

There are multiple environments for FSC-NLX

-   `acceptance`: follows the main branch automatically
-   `demo`: updated after manually triggering a release

Overview of available links per environment:

-   [links.acc.fsc-nlx.io](https://links.acc.fsc.nlx.io/)
-   [links.demo.fsc-nlx.io](https://links.demo.fsc.nlx.io/)

## Generated files

Te execute the commands in the following sections, you will need to install [Earthly](https://earthly.dev/get-earthly).

### Mocks

Mocks can be regenerated using:

```shell
earthly +mocks
```

### Sqlc

Sqlc can be regenerated using:

```shell
earthly +sqlc
```

### OpenAPI Specifications

OAS can be regenerated using:

```shell
earthly +oas
```

### All tasks

If you want to regenerate all files, use the command:

```shell
earthly +all
```

## SOPS

Sops will encrypt your local value using the public key of the cluster.
This encrypted value can only be decrypted using the private key which is on the cluster.

brew install sops
brew install gpg

git clone git@gitlab.com:commonground/haven/internal/configuration.git
cd configuration/flux
gpg --import .sops.pub.asc

Let's add a secret that we want to encrypt:

`secret.yml`

```yaml
apiVersion: v1
kind: Secret
metadata:
    name: gitlab-agent-token
    namespace: gitlab-agent-nlx
type: Opaque
data:
    token: <insert-value-here>
```

Example token value:

```yaml
tls.crt: foo
tls.key: bar
```

Encrypt the token

```shell
sops -e -i azure-core-prod/fsc-nlx-pre-production/internal-ca-issuer-secret.yaml
```

Let's generate a new Key Pair.

cd pki
sh init.sh

open pki/internal/directory/ca

copy contents of intermediate-1.pem to a file
add contents of root-1.pem to that same file

encode the content of that file

cat the-file.txt | base64

add the encoded contet to the secret property `tls.crt`:

For the key, only copy the contents of the intermediate key the `tls.key` property

copy contents of intermediate-1.key.pem

## Further documentation

-   [Technical notes](technical-docs/notes.md)
-   [Official documentation website](https://docs.fsc.nlx.io)

## License

Copyright © VNG Realisatie 2017

[Licensed under the EUPL](LICENCE.md)
