VERSION 0.8

FROM registry.gitlab.com/commonground/nlx/images/earthly:latest

WORKDIR /src

all:
    BUILD +mocks
    BUILD +sqlc
    BUILD +oas
    BUILD +helm-generate-docs
    BUILD +i18n

oas:
    BUILD +oas-manager-auto-sign
    BUILD +oas-manager-external
    BUILD +oas-manager-internal
    BUILD +oas-manager-internal-unauthenticated
    BUILD +oas-txlog-api
    BUILD +oas-controller-registration-rest
    BUILD +oas-controller-administration-rest
    BUILD +oas-controller-authz
    BUILD +oas-auditlog

mocks:
    BUILD +mocks-mockery

sqlc:
    BUILD +sqlc-txlog-api
    BUILD +sqlc-manager
    BUILD +sqlc-controller
    BUILD +sqlc-controller-pgsessionstore
    BUILD +sqlc-auditlog

i18n:
    BUILD +i18n-controller-administration-ui

enums:
    BUILD +enums-permissions

oas-manager-auto-sign:
    COPY ./manager/pkg/autosigner/pdp/*.yaml /src/

    RUN mkdir client
    RUN oapi-codegen --config client.cfg.yaml auto_signer.yaml

    SAVE ARTIFACT /src/client/*.go AS LOCAL ./manager/pkg/autosigner/pdp/client/

oas-manager-external:
    COPY ./manager/ports/ext/rest/api/*.yaml /src/

    RUN npx @redocly/cli join core.yaml logging.yaml --prefix-components-with-info-prop title -o fsc.yaml

    RUN mkdir models
    RUN oapi-codegen --config model.cfg.yaml fsc.yaml
    RUN oapi-codegen --config server.cfg.yaml fsc.yaml

    SAVE ARTIFACT /src/*.go AS LOCAL ./manager/ports/ext/rest/api/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./manager/ports/ext/rest/api/models/

oas-manager-internal:
    COPY ./manager/ports/int/rest/api/*.yaml /src/

    RUN mkdir models server
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config server.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/server/*.go AS LOCAL ./manager/ports/int/rest/api/server/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./manager/ports/int/rest/api/models/

oas-manager-internal-unauthenticated:
    COPY ./manager/ports/intunauthenticated/rest/api/*.yaml /src/

    RUN mkdir models server
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config server.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/server/*.go AS LOCAL ./manager/ports/intunauthenticated/rest/api/server/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./manager/ports/intunauthenticated/rest/api/models/

oas-txlog-api:
    COPY ./txlog-api/ports/rest/api/*.yaml /src/

    RUN npx @redocly/cli join openapi.yaml metadata.yaml -o txlog.yaml

    RUN mkdir models server
    RUN oapi-codegen --config model.cfg.yaml txlog.yaml
    RUN oapi-codegen --config server.cfg.yaml txlog.yaml

    SAVE ARTIFACT /src/server/*.go AS LOCAL ./txlog-api/ports/rest/api/server/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./txlog-api/ports/rest/api/models/

oas-controller-registration-rest:
    COPY ./controller/ports/registration/rest/api/*.yaml /src/

    RUN mkdir models
    RUN mkdir server
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config server.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/server/*.go AS LOCAL ./controller/ports/registration/rest/api/server/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./controller/ports/registration/rest/api/models/

oas-controller-administration-rest:
    COPY ./controller/ports/administration/rest/api/*.yaml /src/

    RUN mkdir models
    RUN oapi-codegen --config model.cfg.yaml api.yaml
    RUN oapi-codegen --config server.cfg.yaml api.yaml

    SAVE ARTIFACT /src/*.go AS LOCAL ./controller/ports/administration/rest/api/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./controller/ports/administration/rest/api/models/

oas-controller-authz:
    COPY ./controller/adapters/authz/rest/api/*.yaml /src/

    RUN mkdir models client
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config client.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/client/*.go AS LOCAL ./controller/adapters/authz/rest/api/client/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./controller/adapters/authz/rest/api/models/

oas-auditlog:
    COPY ./auditlog/ports/rest/api/*.yaml /src/

    RUN mkdir models server
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config server.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/server/*.go AS LOCAL ./auditlog/ports/rest/api/server/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./auditlog/ports/rest/api/models/

mocks-mockery:
    COPY . /src/

    RUN mkdir -p /dist || true

    WORKDIR /src

    RUN mockery

    RUN goimports -w -local "gitlab.com/commonground/nlx/fsc-nlx" /src/

    # common
    SAVE ARTIFACT /src/common/transactionlog/mock/mock_transaction_logger.go AS LOCAL ./common/transactionlog/mock/mock_transaction_logger.go
    SAVE ARTIFACT /src/common/auditlog/mock/mock_audit_logger.go AS LOCAL ./common/auditlog/mock/mock_audit_logger.go
    SAVE ARTIFACT /src/common/clock/mock/mock_clock.go AS LOCAL ./common/clock/mock/mock_clock.go
    SAVE ARTIFACT /src/common/idgenerator/mock/mock_id_generator.go AS LOCAL ./common/idgenerator/mock/mock_id_generator.go

    # manager
    SAVE ARTIFACT /src/manager/domain/contract/mock/mock_repository.go AS LOCAL ./manager/domain/contract/mock/mock_repository.go
    SAVE ARTIFACT /src/manager/adapters/manager/mock/mock_manager.go AS LOCAL ./manager/adapters/manager/mock/mock_manager.go
    SAVE ARTIFACT /src/manager/adapters/manager/mock/mock_factory.go AS LOCAL ./manager/adapters/manager/mock/mock_factory.go
    SAVE ARTIFACT /src/manager/ports/ext/rest/api/mock/mock_client_with_responses_interface.go AS LOCAL ./manager/ports/ext/rest/api/mock/client_with_responses/mock_client_with_responses_interface.go
    SAVE ARTIFACT /src/manager/ports/ext/rest/api/mock/mock_client_interface.go AS LOCAL ./manager/ports/ext/rest/api/mock/client_interface/mock_client_interface.go

    # controller
    SAVE ARTIFACT /src/controller/adapters/manager/mock/mock_manager.go AS LOCAL ./controller/adapters/manager/mock/mock_manager.go
    SAVE ARTIFACT /src/controller/pkg/manager/mock/mock_client.go AS LOCAL ./controller/pkg/manager/mock/mock_client.go
    SAVE ARTIFACT /src/controller/adapters/storage/mock/mock_storage.go AS LOCAL ./controller/adapters/storage/mock/mock_storage.go
    SAVE ARTIFACT /src/controller/adapters/authz/mock/mock_authorization.go AS LOCAL ./controller/adapters/authz/mock/mock_authorization.go
    SAVE ARTIFACT /src/controller/adapters/authz/rest/api/client/mock/mock_client_with_responses_interface.go AS LOCAL ./controller/adapters/authz/rest/api/client/mock/mock_client_with_responses_interface.go

    # outway
    SAVE ARTIFACT /src/outway/domain/config/mock/mock_repository.go AS LOCAL ./outway/domain/config/mock/mock_repository.go

    # txlog
    SAVE ARTIFACT /src/txlog-api/domain/mock/mock_repository.go AS LOCAL ./txlog-api/domain/mock/mock_repository.go

    # directory-ui
    SAVE ARTIFACT /src/directory-ui/adapters/directory/mock/mock_repository.go AS LOCAL ./directory-ui/adapters/directory/mock/mock_repository.go


sqlc-txlog-api:
    COPY ./txlog-api/adapters/storage/postgres/queries /src/txlog-api/adapters/storage/postgres/queries
    COPY ./txlog-api/adapters/storage/postgres/migrations/sql /src/txlog-api/adapters/storage/postgres/migrations/sql

    WORKDIR /src/txlog-api/adapters/storage/postgres/queries

    RUN sqlc generate

    SAVE ARTIFACT /src/txlog-api/adapters/storage/postgres/queries/* AS LOCAL ./txlog-api/adapters/storage/postgres/queries/

sqlc-manager:
    COPY ./manager/adapters/storage/postgres/queries /src/manager/adapters/storage/postgres/queries
    COPY ./manager/adapters/storage/postgres/migrations/sql /src/manager/adapters/storage/postgres/migrations/sql

    WORKDIR /src/manager/adapters/storage/postgres/queries

    RUN sqlc generate

    SAVE ARTIFACT /src/manager/adapters/storage/postgres/queries/* AS LOCAL ./manager/adapters/storage/postgres/queries/

sqlc-controller:
    COPY ./controller/adapters/storage/postgres/queries /src/controller/adapters/storage/postgres/queries
    COPY ./controller/adapters/storage/postgres/migrations/sql /src/controller/adapters/storage/postgres/migrations/sql

    WORKDIR /src/controller/adapters/storage/postgres/queries

    RUN sqlc generate

    SAVE ARTIFACT /src/controller/adapters/storage/postgres/queries/* AS LOCAL ./controller/adapters/storage/postgres/queries/

sqlc-controller-pgsessionstore:
    COPY ./controller/ports/administration/ui/authentication/oidc/pgsessionstore/queries /src/controller/ports/administration/ui/authentication/oidc/pgsessionstore/queries
    COPY ./controller/ports/administration/ui/authentication/oidc/pgsessionstore/migrations /src/controller/ports/administration/ui/authentication/oidc/pgsessionstore/migrations

    WORKDIR /src/controller/ports/administration/ui/authentication/oidc/pgsessionstore/queries

    RUN sqlc generate

    SAVE ARTIFACT /src/controller/ports/administration/ui/authentication/oidc/pgsessionstore/queries/* AS LOCAL ./controller/ports/administration/ui/authentication/oidc/pgsessionstore/queries/

sqlc-auditlog:
    COPY ./auditlog/adapters/storage/postgres/queries /src/auditlog/adapters/storage/postgres/queries
    COPY ./auditlog/adapters/storage/postgres/migrations/sql /src/auditlog/adapters/storage/postgres/migrations/sql

    WORKDIR /src/auditlog/adapters/storage/postgres/queries

    RUN sqlc generate

    SAVE ARTIFACT /src/auditlog/adapters/storage/postgres/queries/* AS LOCAL ./auditlog/adapters/storage/postgres/queries/

i18n-controller-administration-ui:
    COPY ./go.* /src/
    COPY ./controller /src/controller

    # we also need to include dependencies of the controller
    COPY ./txlog-api /src/txlog-api
    COPY ./manager /src/manager
    COPY ./common /src/common
    COPY ./auditlog /src/auditlog

    WORKDIR /src/controller/ports/administration/ui/i18n/text/translations

    RUN gotext-update-templates -srclang=en-US -out=../../../i18n/text/translations/catalog.go -lang=en-US,nl-NL -trfunc=i18n -d=../../../templates ../../..

    # ensure sorting of the translations is stable
    RUN cat locales/en-US/out.gotext.json | jq '.messages |= sort_by(.id)' > locales/en-US/out-formatted.gotext.json
    RUN rm locales/en-US/out.gotext.json
    RUN mv locales/en-US/out-formatted.gotext.json locales/en-US/out.gotext.json

    RUN cat locales/en-US/messages.gotext.json | jq '.messages |= sort_by(.id)' > locales/en-US/messages-formatted.gotext.json
    RUN rm locales/en-US/messages.gotext.json
    RUN mv locales/en-US/messages-formatted.gotext.json locales/en-US/messages.gotext.json

    RUN cat locales/nl-NL/out.gotext.json | jq '.messages |= sort_by(.id)' > locales/nl-NL/out-formatted.gotext.json
    RUN rm locales/nl-NL/out.gotext.json
    RUN mv locales/nl-NL/out-formatted.gotext.json locales/nl-NL/out.gotext.json

    RUN cat locales/nl-NL/messages.gotext.json | jq '.messages |= sort_by(.id)' > locales/nl-NL/messages-formatted.gotext.json
    RUN rm locales/nl-NL/messages.gotext.json
    RUN mv locales/nl-NL/messages-formatted.gotext.json locales/nl-NL/messages.gotext.json

    SAVE ARTIFACT /src/controller/ports/administration/ui/i18n/text/translations/locales/* AS LOCAL ./controller/ports/administration/ui/i18n/text/translations/locales/
    SAVE ARTIFACT /src/controller/ports/administration/ui/i18n/text/translations/catalog.go AS LOCAL ./controller/ports/administration/ui/i18n/text/translations/catalog.go

helm-generate-docs:
    COPY ./helm/charts /src/helm/charts

    WORKDIR /src/helm/charts

    RUN readme-generator -v fsc-apps-overview/values.yaml -r fsc-apps-overview/README.md -s fsc-apps-overview/values.schema.json
    RUN readme-generator -v fsc-ca-certportal/values.yaml -r fsc-ca-certportal/README.md -s fsc-ca-certportal/values.schema.json
    RUN readme-generator -v fsc-ca-cfssl-unsafe/values.yaml -r fsc-ca-cfssl-unsafe/README.md -s fsc-ca-cfssl-unsafe/values.schema.json
    RUN readme-generator -v fsc-nlx-controller/values.yaml -r fsc-nlx-controller/README.md -s fsc-nlx-controller/values.schema.json
    RUN readme-generator -v fsc-nlx-directory-ui/values.yaml -r fsc-nlx-directory-ui/README.md -s fsc-nlx-directory-ui/values.schema.json
    RUN readme-generator -v fsc-nlx-docs/values.yaml -r fsc-nlx-docs/README.md -s fsc-nlx-docs/values.schema.json
    RUN readme-generator -v fsc-nlx-inway/values.yaml -r fsc-nlx-inway/README.md -s fsc-nlx-inway/values.schema.json
    RUN readme-generator -v fsc-nlx-manager/values.yaml -r fsc-nlx-manager/README.md -s fsc-nlx-manager/values.schema.json
    RUN readme-generator -v fsc-nlx-outway/values.yaml -r fsc-nlx-outway/README.md -s fsc-nlx-outway/values.schema.json
    RUN readme-generator -v fsc-nlx-txlog-api/values.yaml -r fsc-nlx-txlog-api/README.md -s fsc-nlx-txlog-api/values.schema.json
    RUN readme-generator -v fsc-nlx-auditlog/values.yaml -r fsc-nlx-auditlog/README.md -s fsc-nlx-auditlog/values.schema.json
    RUN readme-generator -v fsc-nlx-keycloak/values.yaml -r fsc-nlx-keycloak/README.md -s fsc-nlx-keycloak/values.schema.json

    SAVE ARTIFACT /src/helm/charts/* AS LOCAL ./helm/charts/
