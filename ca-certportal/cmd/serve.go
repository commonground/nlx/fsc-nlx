// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package cmd

import (
	"context"
	"log"
	"time"

	"github.com/cloudflare/cfssl/cli"
	"github.com/cloudflare/cfssl/cli/sign"
	"github.com/cloudflare/cfssl/signer"
	"github.com/spf13/cobra"
	"go.uber.org/zap"

	certportal "gitlab.com/commonground/nlx/fsc-nlx/ca-certportal"
	"gitlab.com/commonground/nlx/fsc-nlx/ca-certportal/server"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logoptions"
	"gitlab.com/commonground/nlx/fsc-nlx/common/process"
)

var serveOpts struct {
	ListenAddress string
	CAHost        string
	logoptions.LogOptions
}

//nolint:gochecknoinits // this is the recommended way in Cobra to handle this
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddress, "listen-address", "", "127.0.0.1:8080", "Address for the certportal to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.CAHost, "ca-host", "", "localhost", "The host of the certificate authority (CA).")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "", "Set loglevel")

	if err := serveCommand.MarkFlagRequired("ca-host"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the CA Certportal Server",
	Run: func(cmd *cobra.Command, args []string) {
		if serveOpts.CAHost == "" {
			log.Fatal("CA host option is empty")
		}

		p := process.NewProcess()

		// Setup new zap logger
		config := serveOpts.LogOptions.ZapConfig()

		logger, err := config.Build()
		if err != nil {
			//nolint:gocritic // we know that defer will not run after Fatalf
			log.Fatalf("failed to create new zap logger: %v", err)
		}

		certSigner := func() (signer.Signer, error) {
			signer, signErr := sign.SignerFromConfig(cli.Config{
				Remote: serveOpts.CAHost,
			})
			if signErr != nil {
				logger.Error("failed to sign from config", zap.Error(signErr))
				return nil, signErr
			}

			return signer, nil
		}

		serialNumberGenerator := certportal.GenerateDemoSerialNumber
		cp := server.NewCertPortal(logger, certSigner, serialNumberGenerator, serveOpts.ListenAddress)

		go func() {
			err = cp.Run()
			if err != nil {
				logger.Fatal("error running cert portal", zap.Error(err))
			}
		}()

		p.Wait()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		err = cp.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("shutdown cert portal", zap.Error(err))
		}
	},
}
