// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalapp

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/internalapp/commands"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/internalapp/query"
)

type Application struct {
	Commands Commands
	Queries  Queries
}

type Queries struct {
	GetService *query.GetServiceHandler
	GetHealth  *query.GetHealthHandler
}

type Commands struct {
	RegisterInway  *commands.RegisterInwayHandler
	RegisterOutway *commands.RegisterOutwayHandler
}

type NewApplicationArgs struct {
	Context context.Context
	Storage storage.Storage
}

func NewApplication(args *NewApplicationArgs) (*Application, error) {
	registerInway, err := commands.NewRegisterInwayHandler(args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RegisterInway handler")
	}

	registerOutway, err := commands.NewRegisterOutwayHandler(args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RegisterOutway handler")
	}

	getService, err := query.NewGetServiceHandler(args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetServiceConnectionInfo handler")
	}

	getHealth, err := query.NewGetHealthHandler(args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetHealth handler")
	}

	application := &Application{
		Commands: Commands{
			RegisterInway:  registerInway,
			RegisterOutway: registerOutway,
		},
		Queries: Queries{
			GetService: getService,
			GetHealth:  getHealth,
		},
	}

	return application, nil
}
