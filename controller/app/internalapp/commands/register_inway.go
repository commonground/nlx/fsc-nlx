// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package commands

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type RegisterInwayHandler struct {
	storage storage.Storage
}

func NewRegisterInwayHandler(s storage.Storage) (*RegisterInwayHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &RegisterInwayHandler{
		storage: s,
	}, nil
}

type RegisterInwayArgs struct {
	GroupID string
	Name    string
	Address string
}

func (h *RegisterInwayHandler) Handle(ctx context.Context, args *RegisterInwayArgs) error {
	err := args.Validate()
	if err != nil {
		return fmt.Errorf("%w: invalid register inway args: %s", ErrValidationError, err)
	}

	u, err := url.Parse(args.Address)
	if err != nil {
		return fmt.Errorf("invalid inway address: %w", err)
	}

	err = h.storage.RegisterInway(ctx, args.GroupID, args.Name, u)
	if err != nil {
		return fmt.Errorf("%w: could not register inway in storage: %s", ErrInternalError, err)
	}

	return nil
}

func (a *RegisterInwayArgs) Validate() error {
	_, err := contract.NewGroupID(a.GroupID)
	if err != nil {
		return fmt.Errorf("%w: invalid groupID in args: %w", ErrValidationError, err)
	}

	if a.Name == "" {
		return fmt.Errorf("%w: Name is required", ErrValidationError)
	}

	if a.Address == "" {
		return fmt.Errorf("%w: Address is required", ErrValidationError)
	}

	return nil
}
