// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package commands_test

import (
	"testing"

	mock_storage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/mock"
)

type mocks struct {
	storage *mock_storage.MockStorage
}

func newMocks(t *testing.T) *mocks {
	return &mocks{
		storage: mock_storage.NewMockStorage(t),
	}
}
