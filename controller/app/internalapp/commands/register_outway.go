// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package commands

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type RegisterOutwayHandler struct {
	storage storage.Storage
}

func NewRegisterOutwayHandler(s storage.Storage) (*RegisterOutwayHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &RegisterOutwayHandler{
		storage: s,
	}, nil
}

type RegisterOutwayArgs struct {
	GroupID             string
	Name                string
	PublicKeyThumbprint string
}

func (h *RegisterOutwayHandler) Handle(ctx context.Context, args *RegisterOutwayArgs) error {
	err := args.Validate()
	if err != nil {
		return fmt.Errorf("%w: invalid register outway args: %s", ErrValidationError, err)
	}

	err = h.storage.RegisterOutway(ctx, args.GroupID, args.Name, args.PublicKeyThumbprint)
	if err != nil {
		return fmt.Errorf("%w: could not register outway in storage: %s", ErrInternalError, err)
	}

	return nil
}

func (a *RegisterOutwayArgs) Validate() error {
	_, err := contract.NewGroupID(a.GroupID)
	if err != nil {
		return fmt.Errorf("%w: invalid groupID in args: %w", ErrValidationError, err)
	}

	if a.Name == "" {
		return fmt.Errorf("%w: Name is required", ErrValidationError)
	}

	if a.PublicKeyThumbprint == "" {
		return fmt.Errorf("%w: PublicKeyThumbprint is required", ErrValidationError)
	}

	return nil
}
