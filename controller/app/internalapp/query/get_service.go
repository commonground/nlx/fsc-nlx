// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
)

type GetServiceHandler struct {
	storage storage.Storage
}

type Service struct {
	GroupID      string
	Name         string
	InwayAddress string
	EndpointURL  *url.URL
}

func NewGetServiceHandler(s storage.Storage) (*GetServiceHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &GetServiceHandler{
		storage: s,
	}, nil
}

func (h *GetServiceHandler) Handle(ctx context.Context, groupID, serviceName string) (*Service, error) {
	if groupID == "" {
		return nil, fmt.Errorf("groupID is required")
	}

	if serviceName == "" {
		return nil, fmt.Errorf("serviceName is required")
	}

	service, err := h.storage.GetService(ctx, groupID, serviceName)
	if err != nil {
		return nil, fmt.Errorf("%w: could not get service from storage: %s", ErrInternalError, err)
	}

	return &Service{
		GroupID:      service.GroupID,
		Name:         service.Name,
		InwayAddress: service.InwayAddress,
		EndpointURL:  service.EndpointURL,
	}, nil
}
