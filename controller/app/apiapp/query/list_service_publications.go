// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListServicePublicationsHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

func NewListServicePublicationsHandler(m manager.Manager, a *auth.Auth) (*ListServicePublicationsHandler, error) {
	if m == nil {
		return nil, errors.New("manager is required")
	}

	if a == nil {
		return nil, errors.New("auth is required")
	}

	return &ListServicePublicationsHandler{
		manager: m,
		auth:    a,
	}, nil
}

type ListServicePublicationsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Pagination            *Pagination
	ContractStates        []contract.ContractState
	ServiceNames          []string
}

type Publications struct {
	TotalCount   int
	Publications []*Publication
}

type Publication struct {
	ServicePeerID   string
	ServiceName     string
	ContentHash     string
	CreatedAt       time.Time
	ValidNotBefore  time.Time
	ValidNotAfter   time.Time
	State           string
	GrantHash       string
	DirectoryPeerID string
	DelegatorPeerID string
}

func (h *ListServicePublicationsHandler) Handle(ctx context.Context, args *ListServicePublicationsArgs) (*Publications, string, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListServicePublications, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeService, "*"),
	})
	if err != nil {
		return nil, "", mapError(err, "could not authenticate or authorize")
	}

	pagination := manager.Pagination{}

	if args.Pagination != nil && args.Pagination.StartID != "" {
		pagination.StartID = args.Pagination.StartID
	}

	if args.Pagination != nil && args.Pagination.Limit != 0 {
		pagination.Limit = args.Pagination.Limit
	}

	if args.Pagination != nil && args.Pagination.SortOrder == SortOrderAscending {
		pagination.SortOrder = manager.SortOrderAscending
	} else if args.Pagination != nil && args.Pagination.SortOrder == SortOrderDescending {
		pagination.SortOrder = manager.SortOrderDescending
	}

	publications, paginationNextCursor, err := h.manager.ListServicePublications(ctx, &manager.ListServicePublicationsArgs{
		Pagination:     &pagination,
		ContractStates: args.ContractStates,
		ServiceNames:   args.ServiceNames,
	})
	if err != nil {
		return nil, "", fmt.Errorf("%s: %w", newInternalError("could not get service publications from manager"), err)
	}

	resp := make([]*Publication, len(publications.Publications))

	for i, p := range publications.Publications {
		resp[i] = &Publication{
			ServicePeerID:   p.ServicePeerID,
			ServiceName:     p.ServiceName,
			ContentHash:     p.ContentHash,
			CreatedAt:       p.CreatedAt,
			ValidNotBefore:  p.ValidNotBefore,
			ValidNotAfter:   p.ValidNotAfter,
			State:           p.State,
			GrantHash:       p.GrantHash,
			DirectoryPeerID: p.DirectoryPeerID,
			DelegatorPeerID: p.DelegatorPeerID,
		}
	}

	return &Publications{
		TotalCount:   publications.TotalCount,
		Publications: resp,
	}, paginationNextCursor, nil
}
