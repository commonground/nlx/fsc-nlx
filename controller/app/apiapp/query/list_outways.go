// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

// nolint:dupl // structure is similar but the details differ
package query

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListOutwaysHandler struct {
	storage storage.Storage
	auth    *auth.Auth
	groupID *contract.GroupID
}

type NewListOutwaysHandlerArgs struct {
	Storage storage.Storage
	Auth    *auth.Auth
	GroupID *contract.GroupID
}

type Outway struct {
	Name                string
	PublicKeyThumbprint string
}

func NewListOutwaysHandler(args *NewListOutwaysHandlerArgs) (*ListOutwaysHandler, error) {
	if args.Storage == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.GroupID == nil {
		return nil, fmt.Errorf("group id is required")
	}

	return &ListOutwaysHandler{
		storage: args.Storage,
		auth:    args.Auth,
		groupID: args.GroupID,
	}, nil
}

type ListOutwaysArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	SelfPeerID            string
}

func (h *ListOutwaysHandler) Handle(ctx context.Context, args *ListOutwaysArgs) ([]*Outway, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListOutways, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeOutway, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	outways, err := h.storage.ListOutways(ctx, h.groupID.String())
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get outways from storage"), err)
	}

	resp := make([]*Outway, len(outways))

	for i, outway := range outways {
		resp[i] = &Outway{
			Name:                outway.Name,
			PublicKeyThumbprint: outway.PublicKeyThumbprint,
		}
	}

	return resp, nil
}
