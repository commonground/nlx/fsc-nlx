// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type ListPeerServicesHandler struct {
	m    manager.Manager
	auth *auth.Auth
	l    *logger.Logger
}

type PeerServices []interface{}

type PeerService struct {
	Name     string
	PeerID   string
	PeerName string
}

type DelegatedPeerService struct {
	Name          string
	PeerID        string
	PeerName      string
	DelegatorID   string
	DelegatorName string
}

func NewListPeerServicesHandler(m manager.Manager, a *auth.Auth, l *logger.Logger) (*ListPeerServicesHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if l.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &ListPeerServicesHandler{
		m:    m,
		auth: a,
		l:    l,
	}, nil
}

type ListPeerServicesArgs struct {
	DirectoryPeerID             string
	ServiceProviderPeerIDFilter *string
	AuthData                    authentication.Data
	AuthorizationMetadata       authorization.Metadata
}

func (h *ListPeerServicesHandler) Handle(ctx context.Context, args *ListPeerServicesArgs) (PeerServices, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListPeerServices, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeService, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	services, err := h.m.ListServices(ctx, &manager.ListServicesArgs{
		DirectoryPeerID:             args.DirectoryPeerID,
		ServiceProviderPeerIDFilter: args.ServiceProviderPeerIDFilter,
	})
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get services from manager"), err)
	}

	resp := make(PeerServices, len(services))

	for i, s := range services {
		switch service := s.(type) {
		case *manager.Service:
			resp[i] = &PeerService{
				Name:     service.Name,
				PeerID:   service.PeerID,
				PeerName: service.PeerName,
			}

		case *manager.DelegatedService:
			resp[i] = &DelegatedPeerService{
				Name:          service.Name,
				PeerID:        service.ProviderPeerID,
				PeerName:      service.ProviderPeerName,
				DelegatorID:   service.DelegateeID,
				DelegatorName: service.DelegateeName,
			}

		default:
			h.l.Info(fmt.Sprintf("unknown service type %v", service))
		}
	}

	return resp, nil
}
