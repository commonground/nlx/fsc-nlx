// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query_test

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

var ctxKeyAuthorizationError ctxKey = "authorization_error"

type fakeAuthorization struct{}

func (t *fakeAuthorization) Authorize(ctx context.Context, _ authentication.Data, _ authorization.Metadata, _ authz.Action, _ []*authz.ResourceURN) error {
	if ctx.Value(ctxKeyAuthorizationError) != nil {
		return errors.New("arbitrary error")
	}

	return nil
}
