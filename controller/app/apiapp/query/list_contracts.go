// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type ListContractsHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

type ListContractsFilter struct {
	ContentHash string
	GrantType   GrantType
}

type ListContractsPagination struct {
	Cursor string
	Limit  uint32
}

type GrantType string

const (
	GrantTypeServicePublication          GrantType = "service_publication"
	GrantTypeServiceConnection           GrantType = "service_connection"
	GrantTypeDelegatedServicePublication GrantType = "delegated_service_publication"
	GrantTypeDelegatedServiceConnection  GrantType = "delegated_service_connection"
)

type Contracts []*Contract

func NewListContractsHandler(m manager.Manager, a *auth.Auth) (*ListContractsHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &ListContractsHandler{
		manager: m,
		auth:    a,
	}, nil
}

type ListContractsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Pagination            *ListContractsPagination
	Filters               []*ListContractsFilter
}

// nolint:gocyclo // unable to reduce complexity
func (h *ListContractsHandler) Handle(ctx context.Context, args *ListContractsArgs) (Contracts, string, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListContracts, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, "*"),
	})
	if err != nil {
		return nil, "", mapError(err, "could not authenticate or authorize")
	}

	contentHashes := make([]string, 0)
	grantTypes := make([]manager.GrantType, 0)

	for _, filter := range args.Filters {
		if filter.ContentHash != "" {
			contentHashes = append(contentHashes, filter.ContentHash)
		}

		if filter.GrantType != "" {
			grantTypes = append(grantTypes, convertGrantType(filter.GrantType))
		}
	}

	pagination := manager.ContractsPagination{}

	if args.Pagination != nil && args.Pagination.Cursor != "" {
		pagination.Cursor = args.Pagination.Cursor
	}

	if args.Pagination != nil && args.Pagination.Limit != 0 {
		pagination.Limit = args.Pagination.Limit
	}

	contracts, paginationNextCursor, err := h.manager.ListContracts(ctx, &manager.ListContractsArgs{
		Pagination: &pagination,
		Filters: &manager.ContractsFilters{
			ContentHashes: contentHashes,
			GrantTypes:    grantTypes,
		},
	})
	if err != nil {
		return nil, "", fmt.Errorf("%s: %w", newInternalError("could not get contracts from manager"), err)
	}

	resp := make(Contracts, len(contracts))

	for i, c := range contracts {
		resp[i] = convertContract(c)
	}

	return resp, paginationNextCursor, nil
}

func convertGrantType(input GrantType) manager.GrantType {
	switch input {
	case GrantTypeServicePublication:
		return manager.GrantTypeServicePublication
	case GrantTypeServiceConnection:
		return manager.GrantTypeServiceConnection
	case GrantTypeDelegatedServicePublication:
		return manager.GrantTypeDelegatedServicePublication
	case GrantTypeDelegatedServiceConnection:
		return manager.GrantTypeDelegatedServiceConnection
	default:
		return manager.GrantTypePeerUnspecified
	}
}
