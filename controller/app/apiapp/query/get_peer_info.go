// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type GetPeerInfoHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

func NewGetPeerInfoHandler(m manager.Manager, a *auth.Auth) (*GetPeerInfoHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &GetPeerInfoHandler{
		manager: m,
		auth:    a,
	}, nil
}

type GetPeerInfoArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
}

func (h *GetPeerInfoHandler) Handle(ctx context.Context, args *GetPeerInfoArgs) (*Peer, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscGetPeerInfo, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypePeer, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	p, err := h.manager.GetPeerInfo(ctx)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get peer info from manager"), err)
	}

	return &Peer{
		ID:   p.ID,
		Name: p.Name,
	}, nil
}
