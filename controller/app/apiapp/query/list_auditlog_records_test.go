// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"net/netip"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

func TestNewListAuditLogRecordsHandler_AuditLoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := query.NewListAuditlogRecordsHandler(nil, auth)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auditlog is required")
}

func TestNewListAuditLogRecordsHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := query.NewListAuditlogRecordsHandler(&auditlog.AuditLogger{}, nil)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

// nolint:funlen,dupl // this is a test
func TestListAuditlogRecords(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListAuditlogRecordsArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		Pagination:            &query.Pagination{},
		Filters: &query.AuditlogFilters{
			Period: &query.Period{},
		},
	}

	tests := map[string]struct {
		ctx     context.Context
		args    *query.ListAuditlogRecordsArgs
		want    query.AuditlogRecords
		wantErr error
	}{
		"when_audit_logger_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuditLoggerError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a query.ListAuditlogRecordsArgs) *query.ListAuditlogRecordsArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow": {
			ctx:  context.Background(),
			args: validArgs,
			want: query.AuditlogRecords{
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c001",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventCreateContract{
						ContractHash: "test-contract-hash",
					},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status:    &query.AuditlogStatusInitiated{},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c002",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventAcceptContract{
						ContractHash: "test-contract-hash",
					},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status: &query.AuditlogStatusFailed{
						Error: "some error",
					},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c003",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor:         &query.AuditlogActorUnknown{},
					Event: &query.AuditlogEventRejectContract{
						ContractHash: "test-contract-hash",
					},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status: &query.AuditlogStatusFailed{
						Error: "some error",
					},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c004",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventRevokeContract{
						ContractHash: "test-contract-hash",
					},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.2"),
						UserAgent: "firefox mozilla",
					},
					Status: &query.AuditlogStatusFailed{
						Error: "some error",
					},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c005",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorService{
						SubjectCommonName:   "testSystem",
						PublicKeyThumbprint: "test-public-key-fingerprint",
					},
					Event: &query.AuditlogEventCreateService{
						ServiceName: "service",
					},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status:    &query.AuditlogStatusSucceeded{},
					Component: query.AuditlogComponentController,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c006",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventUpdateService{
						ServiceName: "service",
					},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status:    &query.AuditlogStatusSucceeded{},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c007",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventDeleteService{
						ServiceName: "service",
					},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status:    &query.AuditlogStatusSucceeded{},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c008",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventCreatePeer{},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status: &query.AuditlogStatusUnauthorized{
						Error: "unauthorized access",
					},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c009",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventUpdatePeer{
						PeerID: "test-peer-id",
					},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status:    &query.AuditlogStatusSucceeded{},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c010",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventFailedDistributionRetry{
						PeerID:       "test-peer-id",
						ContractHash: "test-content-hash",
					},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status:    &query.AuditlogStatusSucceeded{},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c010",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventSynchronizePeers{},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status:    &query.AuditlogStatusSucceeded{},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
				&query.AuditlogRecord{
					ID:            "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					GroupID:       "fsc-local",
					CorrelationID: "ce754ff9-3d1d-4dac-9f76-2267ff36c011",
					Actor: &query.AuditlogActorUser{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &query.AuditlogEventLogin{},
					Source: &query.AuditlogSourceHTTP{
						IPAddress: netip.MustParseAddr("127.0.0.1"),
						UserAgent: "firefox mozilla",
					},
					Status:    &query.AuditlogStatusSucceeded{},
					Component: query.AuditlogComponentManager,
					CreatedAt: testClock.Now(),
				},
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			auditLogger, errAuditLog := auditlog.New(&auditlog.NewArgs{AuditLogger: &fakeAuditorLogger{}, GroupID: "fsc-test", Clock: testClock, IDGenerator: &fakeIVGenerator{}})
			require.NoError(t, errAuditLog)

			h, err := query.NewListAuditlogRecordsHandler(auditLogger, auth)
			require.NoError(t, err)

			actual, err := h.Handle(tt.ctx, tt.args)

			assert.Equal(t, tt.want, actual)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
