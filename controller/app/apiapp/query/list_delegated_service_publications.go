// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListDelegatedServicePublicationsHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

type ListDelegatedServicePublicationsPagination struct {
	Cursor    string
	Limit     uint32
	SortOrder SortOrder
}

func NewListDelegatedServicePublicationsHandler(m manager.Manager, a *auth.Auth) (*ListDelegatedServicePublicationsHandler, error) {
	if m == nil {
		return nil, errors.New("manager is required")
	}

	if a == nil {
		return nil, errors.New("auth is required")
	}

	return &ListDelegatedServicePublicationsHandler{
		manager: m,
		auth:    a,
	}, nil
}

type ListDelegatedServicePublicationsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Pagination            *ListDelegatedServicePublicationsPagination
	ContractStates        []contract.ContractState
}

type DelegatedPublications struct {
	TotalCount   int
	Publications []*Publication
}

//nolint:dupl // looks the same as ListServicePublicationsHandler but is different.
func (h *ListDelegatedServicePublicationsHandler) Handle(ctx context.Context, args *ListDelegatedServicePublicationsArgs) (*DelegatedPublications, string, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListDelegatedServicePublications, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeService, "*"),
	})
	if err != nil {
		return nil, "", mapError(err, "could not authenticate or authorize")
	}

	pagination := manager.ListDelegatedServicePublicationsPagination{}

	if args.Pagination != nil && args.Pagination.Cursor != "" {
		pagination.Cursor = args.Pagination.Cursor
	}

	if args.Pagination != nil && args.Pagination.Limit != 0 {
		pagination.Limit = args.Pagination.Limit
	}

	if args.Pagination != nil && args.Pagination.SortOrder == SortOrderAscending {
		pagination.SortOrder = manager.SortOrderAscending
	} else if args.Pagination != nil && args.Pagination.SortOrder == SortOrderDescending {
		pagination.SortOrder = manager.SortOrderDescending
	}

	publications, paginationNextCursor, err := h.manager.ListDelegatedServicePublications(ctx, &manager.ListDelegatedServicePublicationsArgs{
		Pagination:     &pagination,
		ContractStates: args.ContractStates,
	})
	if err != nil {
		return nil, "", fmt.Errorf("%s: %w", newInternalError("could not get delegated service publications from manager"), err)
	}

	resp := make([]*Publication, len(publications.Publications))

	for i, p := range publications.Publications {
		resp[i] = &Publication{
			ServicePeerID:   p.ServicePeerID,
			ServiceName:     p.ServiceName,
			ContentHash:     p.ContentHash,
			CreatedAt:       p.CreatedAt,
			ValidNotBefore:  p.ValidNotBefore,
			ValidNotAfter:   p.ValidNotAfter,
			State:           p.State,
			GrantHash:       p.GrantHash,
			DirectoryPeerID: p.DirectoryPeerID,
			DelegatorPeerID: p.DelegatorPeerID,
		}
	}

	return &DelegatedPublications{
		TotalCount:   publications.TotalCount,
		Publications: resp,
	}, paginationNextCursor, nil
}
