// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different
package query

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListDelegatedConnectionsHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

type ListDelegatedConnectionsPagination struct {
	Cursor string
	Limit  uint32
}

type DelegatedConnections struct {
	Connections []*Connection
	TotalCount  int
}

func NewListDelegatedConnectionsHandler(m manager.Manager, a *auth.Auth) (*ListDelegatedConnectionsHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &ListDelegatedConnectionsHandler{
		manager: m,
		auth:    a,
	}, nil
}

type ListDelegatedConnectionsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Pagination            *ListDelegatedConnectionsPagination
	ContractStates        []contract.ContractState
}

// nolint:dupl // handler is similar but not the same
func (h *ListDelegatedConnectionsHandler) Handle(ctx context.Context, args *ListDelegatedConnectionsArgs) (*DelegatedConnections, string, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListDelegatedConnections, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, "*"),
	})
	if err != nil {
		return nil, "", mapError(err, "could not authenticate or authorize")
	}

	pagination := manager.ListDelegatedConnectionsPagination{}

	if args.Pagination != nil && args.Pagination.Cursor != "" {
		pagination.Cursor = args.Pagination.Cursor
	}

	if args.Pagination != nil && args.Pagination.Limit != 0 {
		pagination.Limit = args.Pagination.Limit
	}

	resp, paginationNextCursor, err := h.manager.ListDelegatedConnections(ctx, &manager.ListDelegatedConnectionsArgs{
		Pagination:     &pagination,
		ContractStates: args.ContractStates,
	})
	if err != nil {
		return nil, "", fmt.Errorf("%s: %w", newInternalError("could not get delegated connections from manager"), err)
	}

	connections := make([]*Connection, len(resp.Connections))

	for i, c := range resp.Connections {
		connections[i] = convertConnection(c)
	}

	return &DelegatedConnections{
		Connections: connections,
		TotalCount:  resp.TotalCount,
	}, paginationNextCursor, nil
}
