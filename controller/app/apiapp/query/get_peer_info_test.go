// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

func TestNewGetPeerInfoHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := query.NewGetPeerInfoHandler(newFakeManager(), nil)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewGetPeerInfoHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := query.NewGetPeerInfoHandler(nil, auth)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestGetPeerInfo(t *testing.T) {
	t.Parallel()

	validArgs := &query.GetPeerInfoArgs{
		AuthData: &authentication.OIDCData{
			Email: "mail@example.com",
			Name:  "John Doe",
		},
		AuthorizationMetadata: &authorization.RestMetaData{},
	}

	testCases := map[string]struct {
		ctx        context.Context
		args       *query.GetPeerInfoArgs
		wantErrMsg string
		wantPeer   *query.Peer
	}{
		"happy_flow": {
			args: validArgs,
			ctx:  context.Background(),
			wantPeer: &query.Peer{
				ID:   "12345678901234567890",
				Name: "Org Name",
			},
		},
		"manager_fails": {
			args: func(a query.GetPeerInfoArgs) *query.GetPeerInfoArgs {
				return &a
			}(*validArgs),
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			wantErrMsg: "internal error: could not get peer info from manager: arbitrary error",
		},
		"authorization_fails": {
			args: func(a query.GetPeerInfoArgs) *query.GetPeerInfoArgs {
				return &a
			}(*validArgs),
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			wantErrMsg: "could not authenticate or authorize: arbitrary error",
		},
	}

	for name, tt := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			fManager := newFakeManager()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, errCommand := query.NewGetPeerInfoHandler(fManager, auth)
			require.NoError(t, errCommand)

			peerInfo, errCommand := h.Handle(tt.ctx, tt.args)

			if tt.wantErrMsg == "" {
				assert.NoError(t, errCommand)

				assert.Equal(t, tt.wantPeer, peerInfo)
			} else {
				assert.ErrorContains(t, errCommand, tt.wantErrMsg)
			}
		})
	}
}
