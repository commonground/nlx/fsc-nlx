/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package query_test

import (
	"context"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

// nolint:funlen,dupl // these tests do not fit in 100 lines
func TestListContractDistributions(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListContractDistributionsArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		ContentHash:           "testContentHash",
	}

	var tests = map[string]struct {
		args    *query.ListContractDistributionsArgs
		setup   func(context.Context, *mocks)
		want    query.ContractDistributions
		wantErr error
	}{
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListContractDistributions,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContractDistribution, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContractDistribution(ctx, validArgs.ContentHash).
					Return(manager.ContractDistributions{
						{
							PeerID:      "peer1",
							ContentHash: validArgs.ContentHash,
							Signature:   "testSignature",
							Action:      manager.SubmitContract,
							Attempts:    2,
							LastAttempt: time.Time{},
							Reason:      "Service Unavailable",
						},
						{
							PeerID:      "peer2",
							ContentHash: validArgs.ContentHash,
							Signature:   "testSignature",
							Action:      manager.SubmitContract,
							Attempts:    0,
							LastAttempt: time.Time{},
							Reason:      "Service Unavailable",
						},
					}, nil)
			},
			want: map[string]map[query.Action]*query.ContractDistribution{
				"peer1": {query.SubmitContract: {
					ContentHash: validArgs.ContentHash,
					Signature:   "testSignature",
					Action:      query.SubmitContract,
					Attempts:    2,
					LastAttempt: time.Time{},
					Reason:      "Service Unavailable",
				}},
				"peer2": {query.SubmitContract: {
					ContentHash: validArgs.ContentHash,
					Signature:   "testSignature",
					Action:      query.SubmitContract,
					Attempts:    0,
					LastAttempt: time.Time{},
					Reason:      "Service Unavailable",
				}},
			},
		},
		"happy_flow_multiple_actions": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListContractDistributions,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContractDistribution, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContractDistribution(ctx, validArgs.ContentHash).
					Return(manager.ContractDistributions{
						{
							PeerID:      "peer1",
							ContentHash: validArgs.ContentHash,
							Signature:   "testSignature",
							Action:      manager.SubmitContract,
							Attempts:    2,
							LastAttempt: time.Time{},
							Reason:      "Service Unavailable",
						},
						{
							PeerID:      "peer1",
							ContentHash: validArgs.ContentHash,
							Signature:   "testSignature",
							Action:      manager.SubmitAcceptSignature,
							Attempts:    0,
							LastAttempt: time.Time{},
							Reason:      "Service Unavailable",
						},
					}, nil)
			},
			want: map[string]map[query.Action]*query.ContractDistribution{
				"peer1": {
					query.SubmitContract: {
						ContentHash: validArgs.ContentHash,
						Signature:   "testSignature",
						Action:      query.SubmitContract,
						Attempts:    2,
						LastAttempt: time.Time{},
						Reason:      "Service Unavailable",
					},
					query.SubmitAcceptSignature: {
						ContentHash: validArgs.ContentHash,
						Signature:   "testSignature",
						Action:      query.SubmitAcceptSignature,
						Attempts:    0,
						LastAttempt: time.Time{},
						Reason:      "Service Unavailable",
					},
				},
			},
		},
		"error_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListContractDistributions,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContractDistribution, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().ListContractDistribution(ctx, validArgs.ContentHash).Return(nil, errors.New("could not get Contract Distributions from manager"))
			},
			wantErr: &query.InternalError{},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListContractDistributionHandler(mocks.manager, mocks.auth)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
