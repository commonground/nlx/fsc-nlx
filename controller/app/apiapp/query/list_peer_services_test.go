// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

// nolint:funlen // these tests should not fit 100 lines
func TestListPeerServices(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListPeerServicesArgs{
		AuthData:                    &authentication.OIDCData{},
		AuthorizationMetadata:       &authorization.RestMetaData{},
		ServiceProviderPeerIDFilter: nil,
	}

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *query.ListPeerServicesArgs
		want    query.PeerServices
		wantErr error
	}{
		"when_storage_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListPeerServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListServices(ctx, &manager.ListServicesArgs{}).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a query.ListPeerServicesArgs) *query.ListPeerServicesArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListPeerServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(authz.NewAuthorizationError("arbitrary"))
			},
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow_one": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListPeerServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListServices(ctx, &manager.ListServicesArgs{}).
					Return(manager.Services{
						&manager.Service{
							PeerID:   "1",
							PeerName: "peer-1",
							Name:     "service-1",
						},
					}, nil)
			},
			want: query.PeerServices{
				&query.PeerService{
					PeerID:   "1",
					PeerName: "peer-1",
					Name:     "service-1",
				},
			},
		},
		"happy_flow_multiple": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListPeerServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListServices(ctx, &manager.ListServicesArgs{}).
					Return(manager.Services{
						&manager.Service{
							PeerID:   "1",
							PeerName: "peer-1",
							Name:     "service-1",
						},
						&manager.Service{
							PeerID:   "2",
							PeerName: "peer-2",
							Name:     "service-2",
						},
						&manager.Service{
							PeerID:   "3",
							PeerName: "peer-3",
							Name:     "service-3",
						},
					}, nil)
			},
			want: query.PeerServices{
				&query.PeerService{
					PeerID:   "1",
					PeerName: "peer-1",
					Name:     "service-1",
				},
				&query.PeerService{
					PeerID:   "2",
					PeerName: "peer-2",
					Name:     "service-2",
				},
				&query.PeerService{
					PeerID:   "3",
					PeerName: "peer-3",
					Name:     "service-3",
				},
			},
		},
		"happy_flow_filter": {
			args: func() *query.ListPeerServicesArgs {
				serviceProviderPeerIDFilter := "1"

				return &query.ListPeerServicesArgs{
					AuthData:                    &authentication.OIDCData{},
					AuthorizationMetadata:       &authorization.RestMetaData{},
					ServiceProviderPeerIDFilter: &serviceProviderPeerIDFilter,
				}
			}(),
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListPeerServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				serviceProviderPeerIDFilter := "1"

				m.manager.EXPECT().
					ListServices(ctx, &manager.ListServicesArgs{
						ServiceProviderPeerIDFilter: &serviceProviderPeerIDFilter,
					}).
					Return(manager.Services{
						&manager.Service{
							PeerID:   "1",
							PeerName: "peer-1",
							Name:     "service-1",
						},
						&manager.Service{
							PeerID:   "1",
							PeerName: "peer-1",
							Name:     "service-2",
						},
					}, nil)
			},
			want: query.PeerServices{
				&query.PeerService{
					PeerID:   "1",
					PeerName: "peer-1",
					Name:     "service-1",
				},
				&query.PeerService{
					PeerID:   "1",
					PeerName: "peer-1",
					Name:     "service-2",
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListPeerServicesHandler(mocks.manager, mocks.auth, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
