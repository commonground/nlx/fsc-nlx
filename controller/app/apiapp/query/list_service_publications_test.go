// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

func TestNewListServicePublicationsHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := query.NewListServicePublicationsHandler(newFakeManager(), nil)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewListServicePublicationsHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := query.NewListServicePublicationsHandler(nil, auth)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestListServicePublications(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListServicePublicationsArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
	}

	var tests = map[string]struct {
		ctx     context.Context
		args    *query.ListServicePublicationsArgs
		want    query.Publications
		wantErr error
	}{
		"when_manager_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)

				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: context.Background(),
			args: func(a query.ListServicePublicationsArgs) *query.ListServicePublicationsArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),

			wantErr: &query.AuthorizationError{},
		},

		"happy_flow": {
			ctx:  context.Background(),
			args: validArgs,
			want: query.Publications{
				TotalCount: 2,
				Publications: []*query.Publication{
					{
						ServiceName:     "test-service",
						ContentHash:     "test_content_hash",
						CreatedAt:       testClock.Now(),
						ValidNotBefore:  testClock.Now(),
						ValidNotAfter:   testClock.Now().Add(1 * time.Hour),
						GrantHash:       "test_grant_hash",
						DirectoryPeerID: "12345678901234567890",
						DelegatorPeerID: "12345678901234567891",
					},
					{
						ServiceName:     "test-service-2",
						ContentHash:     "test_content_hash",
						CreatedAt:       testClock.Now(),
						ValidNotBefore:  testClock.Now(),
						ValidNotAfter:   testClock.Now().Add(1 * time.Hour),
						GrantHash:       "test_grant_hash",
						DirectoryPeerID: "12345678901234567890",
						DelegatorPeerID: "12345678901234567891",
					},
				},
			},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			fManager := newFakeManager()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, err := query.NewListServicePublicationsHandler(fManager, auth)
			require.NoError(t, err)

			actual, _, err := h.Handle(tc.ctx, tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)

				if !assert.Equal(t, tc.want, *actual) {
					for i, c := range tc.want.Publications {
						assert.Equal(t, c, *actual.Publications[i])
					}
				}
			} else {
				assert.ErrorAs(t, err, &tc.wantErr)
			}
		})
	}
}
