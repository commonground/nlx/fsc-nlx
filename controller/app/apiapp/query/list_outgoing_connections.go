// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListContractsWithIncomingConnections
package query

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListOutgoingConnectionsHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

type ListOutgoingConnectionsPagination struct {
	Cursor    string
	Limit     uint32
	SortOrder SortOrder
}

func NewListOutgoingConnectionsHandler(m manager.Manager, a *auth.Auth) (*ListOutgoingConnectionsHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &ListOutgoingConnectionsHandler{
		manager: m,
		auth:    a,
	}, nil
}

type ListOutgoingConnectionsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Pagination            *ListOutgoingConnectionsPagination
	ContractStates        []contract.ContractState
}

type Connection struct {
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
	ServicePeerID                     string
	ServiceName                       string
	ContentHash                       string
	CreatedAt                         time.Time
	ValidNotBefore                    time.Time
	ValidNotAfter                     time.Time
	State                             string
	GrantHash                         string
	ServicePublicationDelegatorPeerID *string
	DelegatorPeerID                   *string
}

type OutgoingConnections struct {
	OutgoingConnections []*Connection
	TotalCount          int
}

func (c *Connection) GetPeerIDs() []string {
	peerIDs := make([]string, 0)

	peerIDs = append(peerIDs, c.ServicePeerID, c.OutwayPeerID)

	if c.DelegatorPeerID != nil {
		peerIDs = append(peerIDs, *c.DelegatorPeerID)
	}

	if c.ServicePublicationDelegatorPeerID != nil {
		peerIDs = append(peerIDs, *c.ServicePublicationDelegatorPeerID)
	}

	return peerIDs
}

// nolint:dupl // handler is similar but not the same
func (h *ListOutgoingConnectionsHandler) Handle(ctx context.Context, args *ListOutgoingConnectionsArgs) (*OutgoingConnections, string, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListContracts, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, "*"),
	})
	if err != nil {
		return nil, "", mapError(err, "could not authenticate or authorize")
	}

	pagination := manager.ListOutgoingConnectionsPagination{}

	if args.Pagination != nil && args.Pagination.Cursor != "" {
		pagination.Cursor = args.Pagination.Cursor
	}

	if args.Pagination != nil && args.Pagination.Limit != 0 {
		pagination.Limit = args.Pagination.Limit
	}

	if args.Pagination != nil && args.Pagination.SortOrder == SortOrderAscending {
		pagination.SortOrder = manager.SortOrderAscending
	} else if args.Pagination != nil && args.Pagination.SortOrder == SortOrderDescending {
		pagination.SortOrder = manager.SortOrderDescending
	}

	connections, paginationNextCursor, err := h.manager.ListOutgoingConnections(ctx, &manager.ListOutgoingConnectionsArgs{
		Pagination:     &pagination,
		ContractStates: args.ContractStates,
	})
	if err != nil {
		return nil, "", fmt.Errorf("%s: %w", newInternalError("could not get outgoing connections from manager"), err)
	}

	resp := make([]*Connection, len(connections.OutgoingConnections))

	for i, c := range connections.OutgoingConnections {
		resp[i] = convertConnection(c)
	}

	return &OutgoingConnections{
		OutgoingConnections: resp,
		TotalCount:          connections.TotalCount,
	}, paginationNextCursor, nil
}

func convertConnection(c *manager.Connection) *Connection {
	return &Connection{
		OutwayPeerID:                      c.OutwayPeerID,
		OutwayPublicKeyThumbprint:         c.OutwayPublicKeyThumbprint,
		ServicePeerID:                     c.ServicePeerID,
		ServiceName:                       c.ServiceName,
		ContentHash:                       c.ContentHash,
		CreatedAt:                         c.CreatedAt,
		ValidNotBefore:                    c.ValidNotBefore,
		ValidNotAfter:                     c.ValidNotAfter,
		State:                             c.State,
		GrantHash:                         c.GrantHash,
		ServicePublicationDelegatorPeerID: c.ServicePublicationDelegatorPeerID,
		DelegatorPeerID:                   c.DelegatorPeerID,
	}
}
