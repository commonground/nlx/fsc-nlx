// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"net/netip"
	"time"

	"github.com/gofrs/uuid"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type AuditlogRecords []*AuditlogRecord

type AuditlogRecord struct {
	ID            string
	GroupID       string
	CorrelationID string
	Actor         AuditlogActor
	Event         AuditlogEvent
	Source        AuditlogSource
	Status        AuditlogStatus
	Component     AuditlogComponent
	CreatedAt     time.Time
}

type ListAuditlogRecordsHandler struct {
	audit *auditlog.AuditLogger
	auth  *auth.Auth
}

func NewListAuditlogRecordsHandler(audit *auditlog.AuditLogger, a *auth.Auth) (*ListAuditlogRecordsHandler, error) {
	if audit == nil {
		return nil, fmt.Errorf("auditlog is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &ListAuditlogRecordsHandler{
		audit: audit,
		auth:  a,
	}, nil
}

type ListAuditlogRecordsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Pagination            *Pagination
	Filters               *AuditlogFilters
}

type AuditlogFilters struct {
	Period         *Period
	IDs            []string
	CorrelationIDs []string
	ContentHashes  []string
	ServiceNames   []string
}

func (h *ListAuditlogRecordsHandler) Handle(ctx context.Context, args *ListAuditlogRecordsArgs) (AuditlogRecords, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListAuditlogs, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeAuditlog, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	records, err := h.audit.ListRecords(ctx, reqToAuditRepo(args))
	if err != nil {
		return nil, mapError(err, "could not get auditlog records from auditlog")
	}

	return mapAuditlogRecords(records)
}

func reqToAuditRepo(args *ListAuditlogRecordsArgs) *common_auditlog.ListRecordsArgs {
	return &common_auditlog.ListRecordsArgs{
		Pagination: &common_auditlog.Pagination{
			StartID:   args.Pagination.StartID,
			Limit:     args.Pagination.Limit,
			SortOrder: mapSortOrder(args.Pagination.SortOrder),
		},
		Filters: &common_auditlog.Filters{
			Period: &common_auditlog.Period{
				Start: args.Filters.Period.Start,
				End:   args.Filters.Period.End,
			},
			IDs:            args.Filters.IDs,
			CorrelationIDs: args.Filters.CorrelationIDs,
			ContentHashes:  args.Filters.ContentHashes,
			ServiceNames:   args.Filters.ServiceNames,
		},
	}
}

func mapSortOrder(s SortOrder) common_auditlog.SortOrder {
	switch s {
	case SortOrderAscending:
		return common_auditlog.SortOrderAscending
	case SortOrderDescending:
		return common_auditlog.SortOrderDescending
	default:
		return common_auditlog.SortOrderDescending
	}
}

func mapAuditlogRecords(recs common_auditlog.Records) (AuditlogRecords, error) {
	records := make(AuditlogRecords, 0, len(recs))

	for _, r := range recs {
		actor, err := actorToModel(r.Actor)
		if err != nil {
			return nil, err
		}

		event, err := eventToModel(r.Event)
		if err != nil {
			return nil, err
		}

		source, err := sourceToModel(r.Source)
		if err != nil {
			return nil, err
		}

		status, err := statusToModel(r.Status)
		if err != nil {
			return nil, err
		}

		records = append(records, &AuditlogRecord{
			ID:            uuid.UUID(r.ID).String(),
			GroupID:       string(r.GroupID),
			CorrelationID: uuid.UUID(r.CorrelationID).String(),
			Actor:         actor,
			Event:         event,
			Source:        source,
			Status:        status,
			Component:     componentToModel(r.Component),
			CreatedAt:     r.CreatedAt,
		})
	}

	return records, nil
}

func actorToModel(a common_auditlog.Actor) (AuditlogActor, error) {
	switch ac := a.(type) {
	case *common_auditlog.ActorUnknown:
		return &AuditlogActorUnknown{}, nil
	case *common_auditlog.ActorUser:
		return &AuditlogActorUser{
			Name:  ac.Name,
			Email: ac.Email,
		}, nil
	case *common_auditlog.ActorService:
		return &AuditlogActorService{
			SubjectCommonName:   ac.SubjectCommonName,
			PublicKeyThumbprint: ac.PublicKeyThumbprint,
		}, nil
	default:
		return nil, fmt.Errorf("unknown actor type: %T", ac)
	}
}

func eventToModel(a common_auditlog.Event) (AuditlogEvent, error) {
	switch ac := a.(type) {
	case *common_auditlog.EventLogin:
		return &AuditlogEventLogin{}, nil

	case *common_auditlog.EventCreateContract:
		return &AuditlogEventCreateContract{
			ContractHash: ac.ContractHash,
		}, nil
	case *common_auditlog.EventAcceptContract:
		return &AuditlogEventAcceptContract{
			ContractHash: ac.ContractHash,
		}, nil
	case *common_auditlog.EventRejectContract:
		return &AuditlogEventRejectContract{
			ContractHash: ac.ContractHash,
		}, nil
	case *common_auditlog.EventRevokeContract:
		return &AuditlogEventRevokeContract{
			ContractHash: ac.ContractHash,
		}, nil

	case *common_auditlog.EventCreateService:
		return &AuditlogEventCreateService{
			ServiceName: ac.ServiceName,
		}, nil
	case *common_auditlog.EventUpdateService:
		return &AuditlogEventUpdateService{
			ServiceName: ac.ServiceName,
		}, nil
	case *common_auditlog.EventDeleteService:
		return &AuditlogEventDeleteService{
			ServiceName: ac.ServiceName,
		}, nil
	case *common_auditlog.EventCreatePeer:
		return &AuditlogEventCreatePeer{
			PeerID: ac.PeerID,
		}, nil
	case *common_auditlog.EventUpdatePeer:
		return &AuditlogEventUpdatePeer{
			PeerID: ac.PeerID,
		}, nil
	case *common_auditlog.EventSynchronizePeers:
		return &AuditlogEventSynchronizePeers{}, nil
	case *common_auditlog.EventFailedDistributionRetry:
		return &AuditlogEventFailedDistributionRetry{
			ContractHash: ac.ContentHash,
			Action:       string(ac.Action),
			PeerID:       ac.PeerID,
		}, nil

	default:
		return nil, fmt.Errorf("unknown event type: %T", ac)
	}
}

func sourceToModel(a common_auditlog.Source) (AuditlogSource, error) {
	switch ac := a.(type) {
	case *common_auditlog.SourceHTTP:
		return &AuditlogSourceHTTP{
			IPAddress: ac.IPAddress,
			UserAgent: ac.UserAgent,
		}, nil
	default:
		return nil, fmt.Errorf("unknown source type: %T", ac)
	}
}

func statusToModel(a common_auditlog.Status) (AuditlogStatus, error) {
	switch ac := a.(type) {
	case *common_auditlog.StatusInitiated:
		return &AuditlogStatusInitiated{}, nil
	case *common_auditlog.StatusUnauthorized:
		return &AuditlogStatusUnauthorized{
			Error: ac.Error,
		}, nil
	case *common_auditlog.StatusFailed:
		return &AuditlogStatusFailed{
			Error: ac.Error,
		}, nil
	case *common_auditlog.StatusSucceeded:
		return &AuditlogStatusSucceeded{}, nil

	default:
		return nil, fmt.Errorf("unknown status type: %T", ac)
	}
}

func componentToModel(c common_auditlog.Component) AuditlogComponent {
	switch c {
	case common_auditlog.ComponentController:
		return AuditlogComponentController
	case common_auditlog.ComponentManager:
		return AuditlogComponentManager
	default:
		return AuditlogComponentUnspecified
	}
}

type AuditlogActor any

type AuditlogActorUser struct {
	Name  string
	Email string
}

type AuditlogActorService struct {
	SubjectCommonName   string
	PublicKeyThumbprint string
}

type AuditlogActorUnknown struct{}

type AuditlogEvent any

type AuditlogEventCreateContract struct {
	ContractHash string
}

type AuditlogEventAcceptContract struct {
	ContractHash string
}

type AuditlogEventRejectContract struct {
	ContractHash string
}

type AuditlogEventRevokeContract struct {
	ContractHash string
}

type AuditlogEventCreateService struct {
	ServiceName string
}

type AuditlogEventUpdateService struct {
	ServiceName string
}

type AuditlogEventDeleteService struct {
	ServiceName string
}

type AuditlogEventCreatePeer struct {
	PeerID string
}

type AuditlogEventUpdatePeer struct {
	PeerID string
}

type AuditlogEventSynchronizePeers struct {
}

type AuditlogEventFailedDistributionRetry struct {
	ContractHash string
	Action       string
	PeerID       string
}

type AuditlogEventLogin struct{}

type AuditlogComponent string

const (
	AuditlogComponentUnspecified AuditlogComponent = ""
	AuditlogComponentController  AuditlogComponent = "controller"
	AuditlogComponentManager     AuditlogComponent = "manager"
)

type AuditlogSource any

type AuditlogSourceHTTP struct {
	IPAddress netip.Addr
	UserAgent string
}

type AuditlogStatus any

type AuditlogStatusFailed struct {
	Error string
}

type AuditlogStatusUnauthorized struct {
	Error string
}

type AuditlogStatusSucceeded struct{}

type AuditlogStatusInitiated struct{}
