// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	mock_storage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func TestNewListInwaysHandler_StorageNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	g, err := contract.NewGroupID("group-id")
	require.NoError(t, err)

	handler, err := query.NewListInwaysHandler(&query.NewListInwaysHandlerArgs{
		Storage: nil,
		Auth:    auth,
		GroupID: &g,
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "storage is required")
}

func TestNewListInwaysHandler_AuthNil(t *testing.T) {
	t.Parallel()

	g, err := contract.NewGroupID("group-id")
	require.NoError(t, err)

	handler, err := query.NewListInwaysHandler(&query.NewListInwaysHandlerArgs{
		Storage: mock_storage.NewMockStorage(t),
		Auth:    nil,
		GroupID: &g,
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewListInwaysHandler_GroupIDNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := query.NewListInwaysHandler(&query.NewListInwaysHandlerArgs{
		Storage: mock_storage.NewMockStorage(t),
		Auth:    auth,
		GroupID: nil,
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "group id is required")
}

func TestListInways(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListInwaysArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
	}

	g, err := contract.NewGroupID("group-id")
	require.NoError(t, err)

	tests := map[string]struct {
		ctx     context.Context
		setup   func(context.Context, *mock_storage.MockStorage)
		args    *query.ListInwaysArgs
		want    []*query.Inway
		wantErr error
	}{
		"when_storage_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			setup: func(ctx context.Context, s *mock_storage.MockStorage) {
				s.EXPECT().ListInways(ctx, g.String()).Return(nil, fmt.Errorf("arbitrary"))
			},
			args:    validArgs,
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a query.ListInwaysArgs) *query.ListInwaysArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow": {
			ctx: context.Background(),
			setup: func(ctx context.Context, s *mock_storage.MockStorage) {
				s.EXPECT().ListInways(ctx, g.String()).Return([]*storage.Inway{
					{
						Name:    "Inway-1",
						Address: "https://inway-1.example.com:443",
					},
				}, nil)
			},
			args: validArgs,
			want: []*query.Inway{
				{
					Name:    "Inway-1",
					Address: "https://inway-1.example.com:443",
				},
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			s := mock_storage.NewMockStorage(t)

			h, err := query.NewListInwaysHandler(&query.NewListInwaysHandlerArgs{
				Storage: s,
				Auth:    auth,
				GroupID: &g,
			})
			require.NoError(t, err)

			if tt.setup != nil {
				tt.setup(tt.ctx, s)
			}

			actual, err := h.Handle(tt.ctx, tt.args)

			assert.Equal(t, tt.want, actual)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
