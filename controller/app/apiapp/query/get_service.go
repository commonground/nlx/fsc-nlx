// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type GetServiceHandler struct {
	storage storage.Storage
	auth    *auth.Auth
	groupID *contract.GroupID
}

type NewGetServiceHandlerArgs struct {
	Storage storage.Storage
	Auth    *auth.Auth
	GroupID *contract.GroupID
}

func NewGetServiceHandler(args *NewGetServiceHandlerArgs) (*GetServiceHandler, error) {
	if args.Storage == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.GroupID == nil {
		return nil, fmt.Errorf("group id is required")
	}

	return &GetServiceHandler{
		storage: args.Storage,
		auth:    args.Auth,
		groupID: args.GroupID,
	}, nil
}

type GetServiceArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Name                  string
}

type GetServiceHandlerResponse struct {
	Name         string
	EndpointURL  *url.URL
	InwayAddress string
}

func (h *GetServiceHandler) Handle(ctx context.Context, args *GetServiceArgs) (*GetServiceHandlerResponse, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscGetService, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeService, args.Name),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	service, err := h.storage.GetService(ctx, h.groupID.String(), args.Name)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get service"), err)
	}

	if service == nil {
		return &GetServiceHandlerResponse{}, nil
	}

	return &GetServiceHandlerResponse{
		Name:         service.Name,
		EndpointURL:  service.EndpointURL,
		InwayAddress: service.InwayAddress,
	}, nil
}
