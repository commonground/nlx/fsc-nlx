// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

// nolint:funlen // these tests do not fit in 100 lines
func TestListContracts(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListContractsArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		Filters:               []*query.ListContractsFilter{},
	}

	var tests = map[string]struct {
		args    *query.ListContractsArgs
		setup   func(context.Context, *mocks)
		want    query.Contracts
		wantErr error
	}{
		"when_manager_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListContracts,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, &manager.ListContractsArgs{
						Pagination: &manager.ContractsPagination{
							Cursor: "",
							Limit:  0,
						},
						Filters: &manager.ContractsFilters{
							ContentHashes: []string{},
							GrantTypes:    []manager.GrantType{},
						},
					}).
					Return(nil, "", errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a query.ListContractsArgs) *query.ListContractsArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListContracts,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(authz.NewAuthorizationError("arbitrary"))
			},
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow_one": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListContracts,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, &manager.ListContractsArgs{
						Pagination: &manager.ContractsPagination{
							Cursor: "",
							Limit:  0,
						},
						Filters: &manager.ContractsFilters{
							ContentHashes: []string{},
							GrantTypes:    []manager.GrantType{},
						},
					}).
					Return(manager.Contracts{
						{
							IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
							GroupID:       "mock-group",
							Hash:          "hash-1",
							HashAlgorithm: manager.HashAlgSHA3_512,
							CreatedAt:     time.Unix(1684938019, 0),
							ValidFrom:     time.Unix(1684938019, 0),
							ValidUntil:    time.Unix(1684939000, 0),
							Peers: []string{
								"12345678901234567890",
								"12345678901234567891",
							},
							RevokeSignatures: map[string]manager.Signature{},
							AcceptSignatures: map[string]manager.Signature{
								"12345678901234567890": {
									SignedAt: time.Unix(1684938020, 0),
								},
							},
							RejectSignatures: map[string]manager.Signature{},
						},
					}, "", nil)
			},
			want: query.Contracts{
				&query.Contract{
					IV:               "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
					GroupID:          "mock-group",
					Hash:             "hash-1",
					HashAlgorithm:    query.HashAlgSHA3_512,
					CreatedAt:        time.Unix(1684938019, 0),
					ValidFrom:        time.Unix(1684938019, 0),
					ValidUntil:       time.Unix(1684939000, 0),
					Peers:            []string{"12345678901234567890", "12345678901234567891"},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
					},
					RevokeSignatures:                  map[string]query.Signature{},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
			},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListContracts,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, &manager.ListContractsArgs{
						Pagination: &manager.ContractsPagination{
							Cursor: "",
							Limit:  0,
						},
						Filters: &manager.ContractsFilters{
							ContentHashes: []string{},
							GrantTypes:    []manager.GrantType{},
						},
					}).
					Return(
						manager.Contracts{
							{
								IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
								GroupID:       "mock-group",
								Hash:          "hash-1",
								HashAlgorithm: manager.HashAlgSHA3_512,
								CreatedAt:     time.Unix(1684938019, 0),
								ValidFrom:     time.Unix(1684938019, 0),
								ValidUntil:    time.Unix(1684939000, 0),
								Peers: []string{
									"12345678901234567890",
									"12345678901234567899",
								},
								AcceptSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938020, 0),
									},
								},
								RejectSignatures: map[string]manager.Signature{},
								RevokeSignatures: map[string]manager.Signature{},
								HasRejected:      false,
								HasAccepted:      false,
								HasRevoked:       false,
								ServicePublicationGrants: []*manager.ServicePublicationGrant{
									{
										DirectoryPeerID: "12345678901234567899",
										ServicePeerID:   "12345678901234567890",
										ServiceName:     "mock-service",
									},
								},
								ServiceConnectionGrants:           nil,
								DelegatedServicePublicationGrants: nil,
								DelegatedServiceConnectionGrants:  nil,
							},
							{
								IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb26",
								GroupID:       "mock-group",
								Hash:          "hash-2",
								HashAlgorithm: manager.HashAlgSHA3_512,
								Peers: []string{
									"12345678901234567890",
									"12345678901234567891",
								},
								CreatedAt:        time.Unix(1684938010, 0),
								ValidFrom:        time.Unix(1684938019, 0),
								ValidUntil:       time.Unix(1684949000, 0),
								RevokeSignatures: map[string]manager.Signature{},
								AcceptSignatures: map[string]manager.Signature{},
								RejectSignatures: map[string]manager.Signature{
									"12345678901234567891": {
										SignedAt: time.Unix(1684938020, 0),
									},
								},
								ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
									{
										ServicePeerID:             "12345678901234567891",
										ServiceName:               "mock-service",
										OutwayPeerID:              "12345678901234567890",
										OutwayPublicKeyThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
									},
								},
							},
							{
								IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb27",
								GroupID:       "mock-group",
								Hash:          "hash-3",
								HashAlgorithm: manager.HashAlgSHA3_512,
								Peers: []string{
									"12345678901234567890",
									"12345678901234567891",
									"12345678901234567899",
								},
								CreatedAt:  time.Unix(1684938021, 0),
								ValidFrom:  time.Unix(1684938000, 0),
								ValidUntil: time.Unix(1684949000, 0),
								AcceptSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938020, 0),
									},
									"12345678901234567891": {
										SignedAt: time.Unix(1684938025, 0),
									},
								},
								RejectSignatures: map[string]manager.Signature{},
								RevokeSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938030, 0),
									},
								},
								DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{
									{
										DirectoryPeerID: "12345678901234567899",
										DelegatorPeerID: "12345678901234567891",
										ServicePeerID:   "12345678901234567890",
										ServiceName:     "mock-service",
									},
								},
							},
							{
								IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb28",
								GroupID:       "mock-group",
								Hash:          "hash-4",
								HashAlgorithm: manager.HashAlgSHA3_512,
								Peers: []string{
									"12345678901234567890",
									"12345678901234567891",
									"12345678901234567892",
								},
								CreatedAt:  time.Unix(1684938021, 0),
								ValidFrom:  time.Unix(1684938000, 0),
								ValidUntil: time.Unix(1684949000, 0),
								AcceptSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938020, 0),
									},
									"12345678901234567891": {
										SignedAt: time.Unix(1684938025, 0),
									},
								},
								RejectSignatures: map[string]manager.Signature{},
								RevokeSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938030, 0),
									},
								},
								DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
									{
										DelegatorPeerID:           "12345678901234567892",
										ServicePeerID:             "12345678901234567891",
										ServiceName:               "mock-service",
										OutwayPeerID:              "12345678901234567890",
										OutwayPublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
									},
								},
							},
						},
						"", nil,
					)
			},
			want: query.Contracts{
				&query.Contract{
					IV:               "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
					GroupID:          "mock-group",
					Hash:             "hash-1",
					HashAlgorithm:    query.HashAlgSHA3_512,
					CreatedAt:        time.Unix(1684938019, 0),
					ValidFrom:        time.Unix(1684938019, 0),
					ValidUntil:       time.Unix(1684939000, 0),
					Peers:            []string{"12345678901234567890", "12345678901234567899"},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
					},
					RevokeSignatures: map[string]query.Signature{},
					ServicePublicationGrants: []*query.ServicePublicationGrant{{
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567890",
						ServiceName:     "mock-service",
					}},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
				&query.Contract{
					IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb26",
					GroupID:       "mock-group",
					Hash:          "hash-2",
					HashAlgorithm: query.HashAlgSHA3_512,
					CreatedAt:     time.Unix(1684938010, 0),
					ValidFrom:     time.Unix(1684938019, 0),
					ValidUntil:    time.Unix(1684949000, 0),
					Peers:         []string{"12345678901234567890", "12345678901234567891"},
					RejectSignatures: map[string]query.Signature{"12345678901234567891": {
						SignedAt: time.Unix(1684938020, 0),
					}},
					AcceptSignatures:         map[string]query.Signature{},
					RevokeSignatures:         map[string]query.Signature{},
					ServicePublicationGrants: []*query.ServicePublicationGrant{},
					ServiceConnectionGrants: []*query.ServiceConnectionGrant{
						{
							ServicePeerID:             "12345678901234567891",
							ServiceName:               "mock-service",
							OutwayPeerID:              "12345678901234567890",
							OutwayPublicKeyThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
						},
					},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
				&query.Contract{
					IV:               "ce754ff9-3d1d-4dac-9f76-2267ff36cb27",
					GroupID:          "mock-group",
					Hash:             "hash-3",
					HashAlgorithm:    query.HashAlgSHA3_512,
					CreatedAt:        time.Unix(1684938021, 0),
					ValidFrom:        time.Unix(1684938000, 0),
					ValidUntil:       time.Unix(1684949000, 0),
					Peers:            []string{"12345678901234567890", "12345678901234567891", "12345678901234567899"},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
						"12345678901234567891": {
							SignedAt: time.Unix(1684938025, 0),
						},
					},
					RevokeSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938030, 0),
						},
					},
					ServicePublicationGrants: []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:  []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{
						{
							DirectoryPeerID: "12345678901234567899",
							DelegatorPeerID: "12345678901234567891",
							ServicePeerID:   "12345678901234567890",
							ServiceName:     "mock-service",
						},
					},
					DelegatedServiceConnectionGrants: []*query.DelegatedServiceConnectionGrant{},
				},
				&query.Contract{
					IV:               "ce754ff9-3d1d-4dac-9f76-2267ff36cb28",
					GroupID:          "mock-group",
					Hash:             "hash-4",
					HashAlgorithm:    query.HashAlgSHA3_512,
					CreatedAt:        time.Unix(1684938021, 0),
					ValidFrom:        time.Unix(1684938000, 0),
					ValidUntil:       time.Unix(1684949000, 0),
					Peers:            []string{"12345678901234567890", "12345678901234567891", "12345678901234567892"},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
						"12345678901234567891": {
							SignedAt: time.Unix(1684938025, 0),
						},
					},
					RevokeSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938030, 0),
						},
					},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants: []*query.DelegatedServiceConnectionGrant{
						{
							DelegatorPeerID: "12345678901234567892",

							OutwayPeerID:              "12345678901234567890",
							OutwayPublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
							ServicePeerID:             "12345678901234567891",
							ServiceName:               "mock-service",
						},
					},
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)

			h, err := query.NewListContractsHandler(m.manager, m.auth)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, m)
			}

			actual, _, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)

				if !assert.Equal(t, tt.want, actual) {
					for i, c := range tt.want {
						assert.Equal(t, c, actual[i])
					}
				}
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
