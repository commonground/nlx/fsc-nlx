// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListServicesHandler struct {
	storage storage.Storage
	manager manager.Manager
	auth    *auth.Auth
	groupID *contract.GroupID
}

type NewListServicesHandlerArgs struct {
	Storage storage.Storage
	Manager manager.Manager
	Auth    *auth.Auth
	GroupID *contract.GroupID
}

type Services []*Service

type Service struct {
	Name         string
	EndpointURL  *url.URL
	InwayAddress string
	IsPublished  bool
}

func NewListServicesHandler(args *NewListServicesHandlerArgs) (*ListServicesHandler, error) {
	if args.Storage == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if args.Manager == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.GroupID == nil {
		return nil, fmt.Errorf("group id is required")
	}

	return &ListServicesHandler{
		storage: args.Storage,
		manager: args.Manager,
		auth:    args.Auth,
		groupID: args.GroupID,
	}, nil
}

type ListServicesArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	SelfPeerID            string
}

func (h *ListServicesHandler) Handle(ctx context.Context, args *ListServicesArgs) (Services, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListServices, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeService, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	services, err := h.storage.ListServices(ctx, h.groupID.String())
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get services from storage"), err)
	}

	resp := make(Services, len(services))

	serviceNames := make([]string, len(services))

	for i, s := range services {
		resp[i] = &Service{
			Name:         s.Name,
			EndpointURL:  s.EndpointURL,
			InwayAddress: s.InwayAddress,
		}

		serviceNames[i] = s.Name
	}

	publications, _, err := h.manager.ListServicePublications(ctx, &manager.ListServicePublicationsArgs{
		ContractStates: []contract.ContractState{contract.ContractStateValid},
		ServiceNames:   serviceNames,
	})

	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get service publications from manager"), err)
	}

	for _, service := range resp {
		for _, publication := range publications.Publications {
			if service.Name == publication.ServiceName {
				service.IsPublished = true
			}
		}
	}

	return resp, nil
}
