// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

func TestNewListTXLogRecordsHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := query.NewListTransactionLogRecordsHandler(newFakeManager(), nil)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewListTXLogRecordsHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := query.NewListTransactionLogRecordsHandler(nil, auth)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

//nolint:funlen // this is a test
func TestListTXLogRecords(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListTXLogRecordsArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		Pagination:            &query.Pagination{},
	}

	tests := map[string]struct {
		ctx     context.Context
		args    *query.ListTXLogRecordsArgs
		want    query.TXLogRecords
		wantErr error
	}{
		"when_manager_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)

				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a query.ListTXLogRecordsArgs) *query.ListTXLogRecordsArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)

				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow": {
			ctx:  context.Background(),
			args: validArgs,
			want: query.TXLogRecords{
				&query.TXLogRecord{
					TransactionID: "id-1",
					GrantHash:     "hash-1",
					ServiceName:   "service-1",
					Direction:     record.DirectionIn,
					Source: &query.TXLogRecordSource{
						OutwayPeerID: "1",
					},
					Destination: &query.TXLogRecordDestination{
						ServicePeerID: "2",
					},
					CreatedAt: time.Unix(1684938019, 0),
				},
				&query.TXLogRecord{
					TransactionID: "id-2",
					GrantHash:     "hash-2",
					ServiceName:   "service-2",
					Direction:     record.DirectionOut,
					Source: &query.TXLogRecordDelegatedSource{
						OutwayPeerID:    "1",
						DelegatorPeerID: "2",
					},
					Destination: &query.TXLogRecordDestination{
						ServicePeerID: "3",
					},
					CreatedAt: time.Unix(1684938020, 0),
				},
				&query.TXLogRecord{
					TransactionID: "id-3",
					GrantHash:     "hash-3",
					ServiceName:   "service-3",
					Direction:     record.DirectionIn,
					Source: &query.TXLogRecordSource{
						OutwayPeerID: "1",
					},
					Destination: &query.TXLogRecordDelegatedDestination{
						ServicePeerID:   "2",
						DelegatorPeerID: "3",
					},
					CreatedAt: time.Unix(1684938021, 0),
				},
				&query.TXLogRecord{
					TransactionID: "id-4",
					GrantHash:     "hash-4",
					ServiceName:   "service-4",
					Direction:     record.DirectionOut,
					Source: &query.TXLogRecordDelegatedSource{
						OutwayPeerID:    "1",
						DelegatorPeerID: "2",
					},
					Destination: &query.TXLogRecordDelegatedDestination{
						ServicePeerID:   "3",
						DelegatorPeerID: "4",
					},
					CreatedAt: time.Unix(1684938022, 0),
				},
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, err := query.NewListTransactionLogRecordsHandler(newFakeManager(), auth)
			require.NoError(t, err)

			actual, err := h.Handle(tt.ctx, tt.args)

			assert.Equal(t, tt.want, actual)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
