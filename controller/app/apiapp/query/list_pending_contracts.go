// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type ListPendingContractsHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

type ListPendingContractsPagination struct {
	Cursor string
	Limit  uint32
}

type PendingContracts struct {
	PendingContracts []*PendingContract
	TotalCount       int
}

type PendingContract struct {
	ContentHash string
	Grants      []*PendingContractGrant
	CreatedAt   time.Time
}

type PendingContractGrant struct {
	ServiceName                   string
	ServicePeerID                 string
	OutwayPeerID                  string
	DelegatorPeerID               string
	ServicePublicationDelegatorID string
	GrantType                     GrantType
}

func NewListPendingContractsHandler(m manager.Manager, a *auth.Auth) (*ListPendingContractsHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &ListPendingContractsHandler{
		manager: m,
		auth:    a,
	}, nil
}

type ListPendingContractsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Pagination            *ListPendingContractsPagination
}

// nolint:dupl // handler is similar but not the same
func (h *ListPendingContractsHandler) Handle(ctx context.Context, args *ListPendingContractsArgs) (*PendingContracts, string, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListContracts, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, "*"),
	})
	if err != nil {
		return nil, "", mapError(err, "could not authenticate or authorize")
	}

	pagination := manager.PendingContractsPagination{}

	if args.Pagination != nil && args.Pagination.Cursor != "" {
		pagination.Cursor = args.Pagination.Cursor
	}

	if args.Pagination != nil && args.Pagination.Limit != 0 {
		pagination.Limit = args.Pagination.Limit
	}

	contractsPending, paginationNextCursor, err := h.manager.ListContractsPending(ctx, &manager.ListContractsPendingArgs{
		Pagination: &pagination,
	})
	if err != nil {
		return nil, "", fmt.Errorf("%s: %w", newInternalError("could not get pending contracts from manager"), err)
	}

	resp := make([]*PendingContract, len(contractsPending.PendingContracts))

	for i, c := range contractsPending.PendingContracts {
		resp[i] = convertContractPending(c)
	}

	pendingContracts := &PendingContracts{
		PendingContracts: resp,
		TotalCount:       contractsPending.TotalCount,
	}

	return pendingContracts, paginationNextCursor, nil
}

func convertContractPending(c *manager.PendingContract) *PendingContract {
	grants := make([]*PendingContractGrant, 0, len(c.Grants))

	for _, g := range c.Grants {
		grants = append(grants, convertGrantPending(g))
	}

	return &PendingContract{
		ContentHash: c.ContentHash,
		Grants:      grants,
		CreatedAt:   c.ValidNotBefore,
	}
}

func convertGrantPending(g *manager.PendingContractGrant) *PendingContractGrant {
	var gt GrantType

	switch g.GrantType {
	case manager.GrantTypeServicePublication:
		gt = GrantTypeServicePublication
	case manager.GrantTypeDelegatedServicePublication:
		gt = GrantTypeDelegatedServicePublication
	case manager.GrantTypeServiceConnection:
		gt = GrantTypeServiceConnection
	case manager.GrantTypeDelegatedServiceConnection:
		gt = GrantTypeDelegatedServiceConnection
	}

	return &PendingContractGrant{
		ServiceName:                   g.ServiceName,
		ServicePeerID:                 g.ServicePeerID,
		OutwayPeerID:                  g.OutwayPeerID,
		DelegatorPeerID:               g.DelegatorPeerID,
		ServicePublicationDelegatorID: g.ServicePublicationDelegatorID,
		GrantType:                     gt,
	}
}
