// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

// nolint:funlen,dupl // these tests do not fit in 100 lines and have similar setup as other tests
func TestGetContract(t *testing.T) {
	t.Parallel()

	validArgs := &query.GetContractArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		ContentHash:           "hash-1",
	}

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *query.GetContractArgs
		want    *query.Contract
		wantErr error
	}{
		"when_manager_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscGetContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, validArgs.ContentHash),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, &manager.ListContractsArgs{
						Filters: &manager.ContractsFilters{
							ContentHashes: []string{validArgs.ContentHash},
							GrantTypes:    nil,
						},
					}).
					Return(nil, "", errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a query.GetContractArgs) *query.GetContractArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscGetContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, validArgs.ContentHash),
					},
				).Return(authz.NewAuthorizationError("arbitrary"))
			},
			wantErr: &query.AuthorizationError{},
		},
		"when_contract_does_not_exist": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscGetContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, validArgs.ContentHash),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, &manager.ListContractsArgs{
						Filters: &manager.ContractsFilters{
							ContentHashes: []string{validArgs.ContentHash},
						},
					}).
					Return(manager.Contracts{}, "", nil)
			},
			wantErr: &query.InternalError{},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscGetContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, validArgs.ContentHash),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, &manager.ListContractsArgs{
						Filters: &manager.ContractsFilters{
							ContentHashes: []string{validArgs.ContentHash},
						},
					}).
					Return(manager.Contracts{
						{
							IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
							GroupID:       "mock-group",
							Hash:          validArgs.ContentHash,
							HashAlgorithm: manager.HashAlgSHA3_512,
							CreatedAt:     time.Unix(1684938019, 0),
							ValidFrom:     time.Unix(1684938019, 0),
							ValidUntil:    time.Unix(1684939000, 0),
							Peers: []string{
								"12345678901234567890",
								"12345678901234567891",
							},
							RejectSignatures: map[string]manager.Signature{
								"12345678901234567890": {SignedAt: time.Unix(1684938020, 0)},
							},
							ServiceConnectionGrants: []*manager.ServiceConnectionGrant{{
								ServicePeerID:             "12345678901234567891",
								ServiceName:               "mock-service",
								OutwayPeerID:              "12345678901234567890",
								OutwayPublicKeyThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
							}},
						},
					}, "", nil)
			},
			want: &query.Contract{
				IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
				GroupID:       "mock-group",
				Hash:          validArgs.ContentHash,
				HashAlgorithm: query.HashAlgSHA3_512,
				CreatedAt:     time.Unix(1684938019, 0),
				ValidFrom:     time.Unix(1684938019, 0),
				ValidUntil:    time.Unix(1684939000, 0),
				Peers:         []string{"12345678901234567890", "12345678901234567891"},
				RejectSignatures: map[string]query.Signature{
					"12345678901234567890": {SignedAt: time.Unix(1684938020, 0)},
				},
				AcceptSignatures:         map[string]query.Signature{},
				RevokeSignatures:         map[string]query.Signature{},
				ServicePublicationGrants: []*query.ServicePublicationGrant{},
				ServiceConnectionGrants: []*query.ServiceConnectionGrant{{
					ServicePeerID:             "12345678901234567891",
					ServiceName:               "mock-service",
					OutwayPeerID:              "12345678901234567890",
					OutwayPublicKeyThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
				}},
				DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
				DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewGetContractHandler(mocks.manager, mocks.auth)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
