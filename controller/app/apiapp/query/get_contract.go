// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type GetContractHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

type Contract struct {
	IV                                string
	Hash                              string
	HashAlgorithm                     HashAlg
	GroupID                           string
	CreatedAt                         time.Time
	ValidFrom                         time.Time
	ValidUntil                        time.Time
	Peers                             []string
	AcceptSignatures                  map[string]Signature
	RejectSignatures                  map[string]Signature
	RevokeSignatures                  map[string]Signature
	HasRejected                       bool
	HasAccepted                       bool
	HasRevoked                        bool
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	State                             ContractState
}

type HashAlg int32

const (
	HashAlgUnspecified HashAlg = iota
	HashAlgSHA3_512
)

type ContractState string

const (
	ContractStateUnspecified ContractState = "CONTRACT_STATE_UNSPECIFIED"
	ContractStateExpired     ContractState = "CONTRACT_STATE_EXPIRED"
	ContractStateProposed    ContractState = "CONTRACT_STATE_PROPOSED"
	ContractStateRejected    ContractState = "CONTRACT_STATE_REJECTED"
	ContractStateRevoked     ContractState = "CONTRACT_STATE_REVOKED"
	ContractStateValid       ContractState = "CONTRACT_STATE_VALID"
)

type Peer struct {
	ID   string
	Name string
}

type ServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type ServiceConnectionGrant struct {
	Hash                              string
	ServicePeerID                     string
	ServicePublicationDelegatorPeerID string
	ServiceName                       string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
}

type DelegatedServiceConnectionGrant struct {
	Hash                              string
	DelegatorPeerID                   string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
	ServicePeerID                     string
	ServicePublicationDelegatorPeerID string
	ServiceName                       string
}

type DelegatedServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
}

type Signature struct {
	SignedAt time.Time
}

type ListContractFilter struct {
	ContentHash string
}

func NewGetContractHandler(m manager.Manager, a *auth.Auth) (*GetContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &GetContractHandler{
		manager: m,
		auth:    a,
	}, nil
}

type GetContractArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	ContentHash           string
}

func (h *GetContractHandler) Handle(ctx context.Context, args *GetContractArgs) (*Contract, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscGetContract, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, args.ContentHash),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	contracts, _, err := h.manager.ListContracts(ctx, &manager.ListContractsArgs{
		Pagination: nil,
		Filters: &manager.ContractsFilters{
			ContentHashes: []string{args.ContentHash},
		},
	})
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts from manager"), err)
	}

	if len(contracts) != 1 {
		return nil, newInternalError("contract not returned by manager")
	}

	c := contracts[0]

	peersByID := make(map[string]string)

	for _, p := range c.Peers {
		peersByID[p] = p
	}

	contract := convertContract(c)

	return contract, nil
}

func convertContract(contract *manager.Contract) *Contract {
	servicePublicationGrants := make([]*ServicePublicationGrant, len(contract.ServicePublicationGrants))

	for i, grant := range contract.ServicePublicationGrants {
		servicePublicationGrants[i] = &ServicePublicationGrant{
			Hash:            grant.Hash,
			DirectoryPeerID: grant.DirectoryPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	serviceConnectionGrants := make([]*ServiceConnectionGrant, len(contract.ServiceConnectionGrants))

	for i, grant := range contract.ServiceConnectionGrants {
		serviceConnectionGrants[i] = &ServiceConnectionGrant{
			Hash:                              grant.Hash,
			ServicePeerID:                     grant.ServicePeerID,
			ServicePublicationDelegatorPeerID: grant.ServicePublicationDelegatorPeerID,
			ServiceName:                       grant.ServiceName,
			OutwayPeerID:                      grant.OutwayPeerID,
			OutwayPublicKeyThumbprint:         grant.OutwayPublicKeyThumbprint,
		}
	}

	delegatedServiceConnectionGrants := make([]*DelegatedServiceConnectionGrant, len(contract.DelegatedServiceConnectionGrants))

	for i, grant := range contract.DelegatedServiceConnectionGrants {
		delegatedServiceConnectionGrants[i] = &DelegatedServiceConnectionGrant{
			Hash:                              grant.Hash,
			DelegatorPeerID:                   grant.DelegatorPeerID,
			OutwayPeerID:                      grant.OutwayPeerID,
			OutwayPublicKeyThumbprint:         grant.OutwayPublicKeyThumbprint,
			ServicePeerID:                     grant.ServicePeerID,
			ServicePublicationDelegatorPeerID: grant.ServicePublicationDelegatorPeerID,
			ServiceName:                       grant.ServiceName,
		}
	}

	delegatedServicePublicationGrants := make([]*DelegatedServicePublicationGrant, len(contract.DelegatedServicePublicationGrants))

	for i, grant := range contract.DelegatedServicePublicationGrants {
		delegatedServicePublicationGrants[i] = &DelegatedServicePublicationGrant{
			Hash:            grant.Hash,
			DirectoryPeerID: grant.DirectoryPeerID,
			DelegatorPeerID: grant.DelegatorPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	convertedContract := &Contract{
		IV:                                contract.IV,
		Hash:                              contract.Hash,
		HashAlgorithm:                     HashAlg(contract.HashAlgorithm),
		GroupID:                           contract.GroupID,
		CreatedAt:                         contract.CreatedAt,
		ValidFrom:                         contract.ValidFrom,
		ValidUntil:                        contract.ValidUntil,
		Peers:                             contract.Peers,
		AcceptSignatures:                  convertSignatures(contract.AcceptSignatures),
		RejectSignatures:                  convertSignatures(contract.RejectSignatures),
		RevokeSignatures:                  convertSignatures(contract.RevokeSignatures),
		HasRejected:                       contract.HasRejected,
		HasAccepted:                       contract.HasAccepted,
		HasRevoked:                        contract.HasRevoked,
		ServicePublicationGrants:          servicePublicationGrants,
		ServiceConnectionGrants:           serviceConnectionGrants,
		DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
		DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		State:                             ContractState(contract.State),
	}

	return convertedContract
}

func convertSignatures(signatures map[string]manager.Signature) map[string]Signature {
	convertedSignatures := make(map[string]Signature)
	for peerID, signature := range signatures {
		convertedSignatures[peerID] = Signature{SignedAt: signature.SignedAt}
	}

	return convertedSignatures
}
