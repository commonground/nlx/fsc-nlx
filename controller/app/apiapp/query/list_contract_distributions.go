/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package query

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type ListContractDistributionsHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

type ListContractDistributionsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	ContentHash           string
}

func NewListContractDistributionHandler(m manager.Manager, a *auth.Auth) (*ListContractDistributionsHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &ListContractDistributionsHandler{
		manager: m,
		auth:    a,
	}, nil
}

func (h *ListContractDistributionsHandler) Handle(ctx context.Context, args *ListContractDistributionsArgs) (contractDistributions ContractDistributions, err error) {
	err = h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListContractDistributions, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContractDistribution, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	managerContractDistributions, err := h.manager.ListContractDistribution(ctx, args.ContentHash)
	if err != nil {
		return nil, mapError(err, "could not get contract distributions from manager")
	}

	contractDistributions = mapDistributionsPerPeerAndAction(managerContractDistributions)

	return contractDistributions, nil
}

func mapDistributionsPerPeerAndAction(managerContractDistributions manager.ContractDistributions) (contractDistributions ContractDistributions) {
	contractDistributions = make(map[string]map[Action]*ContractDistribution)
	for _, managerContractDistribution := range managerContractDistributions {
		if _, ok := contractDistributions[managerContractDistribution.PeerID]; !ok {
			contractDistributions[managerContractDistribution.PeerID] = map[Action]*ContractDistribution{}
		}

		if _, ok := contractDistributions[managerContractDistribution.PeerID][convertAction(managerContractDistribution.Action)]; !ok {
			contractDistributions[managerContractDistribution.PeerID][convertAction(managerContractDistribution.Action)] = &ContractDistribution{
				ContentHash: managerContractDistribution.ContentHash,
				Signature:   managerContractDistribution.Signature,
				Action:      Action(managerContractDistribution.Action),
				Attempts:    managerContractDistribution.Attempts,
				LastAttempt: managerContractDistribution.LastAttempt,
				Reason:      managerContractDistribution.Reason,
				NextAttempt: managerContractDistribution.NextAttempt,
			}
		}

		contractDistributions[managerContractDistribution.PeerID][convertAction(managerContractDistribution.Action)] = &ContractDistribution{
			ContentHash: managerContractDistribution.ContentHash,
			Signature:   managerContractDistribution.Signature,
			Action:      Action(managerContractDistribution.Action),
			Attempts:    managerContractDistribution.Attempts,
			LastAttempt: managerContractDistribution.LastAttempt,
			Reason:      managerContractDistribution.Reason,
			NextAttempt: managerContractDistribution.NextAttempt,
		}
	}

	return contractDistributions
}

type ContractDistributions map[string]map[Action]*ContractDistribution

type ContractDistribution struct {
	ContentHash string
	Signature   string
	Action      Action
	Attempts    int
	LastAttempt time.Time
	NextAttempt *time.Time
	Reason      string
}

type Action int

const (
	SubmitContract Action = iota
	SubmitAcceptSignature
	SubmitRejectSignature
	SubmitRevokeSignature
)

func convertAction(action manager.Action) Action {
	switch action {
	case manager.SubmitContract:
		return SubmitContract
	case manager.SubmitAcceptSignature:
		return SubmitAcceptSignature
	case manager.SubmitRejectSignature:
		return SubmitRejectSignature
	case manager.SubmitRevokeSignature:
		return SubmitRevokeSignature
	default:
		return -1
	}
}
