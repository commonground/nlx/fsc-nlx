// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query_test

import (
	"context"
	"errors"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type fakeManager struct {
	Peers map[string]*manager.Peer
}

func newFakeManager() *fakeManager {
	return &fakeManager{
		Peers: make(map[string]*manager.Peer, 0),
	}
}

func (f *fakeManager) ListContracts(ctx context.Context, args *manager.ListContractsArgs) (manager.Contracts, string, error) {
	return manager.Contracts{}, "", errors.New("not implemented")
}

func (f *fakeManager) ListContractsPending(ctx context.Context, args *manager.ListContractsPendingArgs) (*manager.PendingContracts, string, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, "", errors.New("arbitrary error")
	}

	return &manager.PendingContracts{PendingContracts: []*manager.PendingContract{{
		ContentHash: "content-hash",
		Grants: []*manager.PendingContractGrant{
			{
				ServiceName:   "mock-service",
				ServicePeerID: "12345678901234567890",
				OutwayPeerID:  "12345678901234567891",
				GrantType:     manager.GrantTypeServiceConnection,
			},
			{
				ServiceName:                   "mock-service",
				ServicePeerID:                 "12345678901234567890",
				OutwayPeerID:                  "12345678901234567891",
				DelegatorPeerID:               "12345678901234567892",
				ServicePublicationDelegatorID: "12345678901234567893",
				GrantType:                     manager.GrantTypeDelegatedServiceConnection,
			},
			{
				ServiceName:   "mock-service",
				ServicePeerID: "12345678901234567890",
				GrantType:     manager.GrantTypeServicePublication,
			},
			{
				ServiceName:                   "mock-service",
				ServicePeerID:                 "12345678901234567890",
				ServicePublicationDelegatorID: "12345678901234567893",
				GrantType:                     manager.GrantTypeDelegatedServicePublication,
			},
		},

		ValidNotBefore: testClock.Now(),
	}}, TotalCount: 4}, "", nil
}

func (f *fakeManager) CreateContract(ctx context.Context, args *manager.CreateContractArgs) (string, error) {
	return "", errors.New("not implemented")
}

func (f *fakeManager) AcceptContract(ctx context.Context, contentHash, auditlogCorrelationID string) error {
	return errors.New("not implemented")
}

func (f *fakeManager) RevokeContract(ctx context.Context, contentHash, auditlogCorrelationID string) error {
	return errors.New("not implemented")
}

func (f *fakeManager) RejectContract(ctx context.Context, contentHash, auditlogCorrelationID string) error {
	return errors.New("not implemented")
}

func (f *fakeManager) RetryContract(ctx context.Context, contentHash, peerID string, action manager.Action, auditlogCorrelationID string) error {
	return errors.New("not implemented")
}
func (f *fakeManager) ListContractDistribution(ctx context.Context, contentHash string) (manager.ContractDistributions, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) ListOutgoingConnections(ctx context.Context, args *manager.ListOutgoingConnectionsArgs) (*manager.OutgoingConnections, string, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, "", errors.New("arbitrary error")
	}

	return &manager.OutgoingConnections{
		TotalCount: 1,
		OutgoingConnections: []*manager.Connection{{
			ServicePeerID:             "12345678901234567890",
			ServiceName:               "mock-service",
			OutwayPeerID:              "12345678901234567891",
			OutwayPublicKeyThumbprint: "outway-public-key-thumbprint",
			ContentHash:               "content-hash",
			CreatedAt:                 testClock.Now(),
			ValidNotBefore:            testClock.Now(),
			ValidNotAfter:             testClock.Now(),
			GrantHash:                 "grant-hash",
		}},
	}, "", nil
}

func (f *fakeManager) ListOutgoingConnectionsForService(ctx context.Context, args *manager.ListOutgoingConnectionsForServiceArgs) ([]*manager.Connection, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, errors.New("arbitrary error")
	}

	return []*manager.Connection{{
		ServicePeerID:             "12345678901234567890",
		ServiceName:               "mock-service",
		OutwayPeerID:              "12345678901234567891",
		OutwayPublicKeyThumbprint: "outway-public-key-thumbprint",
		ContentHash:               "content-hash",
		CreatedAt:                 testClock.Now(),
		ValidNotBefore:            testClock.Now(),
		ValidNotAfter:             testClock.Now(),
		GrantHash:                 "grant-hash",
	}}, nil
}

func (f *fakeManager) ListOutgoingConnectionsForDelegatedService(ctx context.Context, args *manager.ListOutgoingConnectionsForDelegatedServiceArgs) ([]*manager.Connection, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, errors.New("arbitrary error")
	}

	servicePublicationPeerID := "12345678901234567892"

	return []*manager.Connection{{
		ServicePeerID:                     "12345678901234567890",
		ServiceName:                       "mock-service",
		ServicePublicationDelegatorPeerID: &servicePublicationPeerID,
		OutwayPeerID:                      "12345678901234567891",
		OutwayPublicKeyThumbprint:         "outway-public-key-thumbprint",
		ContentHash:                       "content-hash",
		CreatedAt:                         testClock.Now(),
		ValidNotBefore:                    testClock.Now(),
		ValidNotAfter:                     testClock.Now(),
		GrantHash:                         "grant-hash",
	}}, nil
}

func (f *fakeManager) ListIncomingConnections(ctx context.Context, args *manager.ListIncomingConnectionsArgs) (*manager.IncomingConnections, string, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, "", errors.New("arbitrary error")
	}

	return &manager.IncomingConnections{
		TotalCount: 1,
		Connections: []*manager.Connection{{
			ServicePeerID:             "12345678901234567890",
			ServiceName:               "mock-service",
			OutwayPeerID:              "12345678901234567891",
			OutwayPublicKeyThumbprint: "outway-public-key-thumbprint",
			ContentHash:               "content-hash",
			CreatedAt:                 testClock.Now(),
			ValidNotBefore:            testClock.Now(),
			ValidNotAfter:             testClock.Now(),
			GrantHash:                 "grant-hash",
		}},
	}, "", nil
}

func (f *fakeManager) ListDelegatedConnections(ctx context.Context, _ *manager.ListDelegatedConnectionsArgs) (*manager.DelegatedConnections, string, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, "", errors.New("arbitrary error")
	}

	delegatorPeerID := "12345678901234567892"

	return &manager.DelegatedConnections{Connections: []*manager.Connection{{
		ServicePeerID:             "12345678901234567890",
		ServiceName:               "mock-service",
		OutwayPeerID:              "12345678901234567891",
		OutwayPublicKeyThumbprint: "thumbprint",
		ContentHash:               "content-hash",
		CreatedAt:                 testClock.Now(),
		ValidNotBefore:            testClock.Now(),
		ValidNotAfter:             testClock.Now(),
		State:                     "",
		GrantHash:                 "grant-hash",
		DelegatorPeerID:           &delegatorPeerID,
	},
	}, TotalCount: 1}, "", nil
}

func (f *fakeManager) SyncPeers(ctx context.Context) (map[string]manager.SynchronizationResult, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) UpdatePeer(ctx context.Context, args *manager.UpdatePeerArgs) error {
	return errors.New("not implemented")
}

func (f *fakeManager) ListServices(ctx context.Context, args *manager.ListServicesArgs) (manager.Services, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) ListServicePublications(ctx context.Context, args *manager.ListServicePublicationsArgs) (*manager.Publications, string, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, "", errors.New("arbitrary error")
	}

	return &manager.Publications{Publications: []*manager.Publication{
		{
			ServiceName:     "test-service",
			ContentHash:     "test_content_hash",
			CreatedAt:       testClock.Now(),
			ValidNotBefore:  testClock.Now(),
			ValidNotAfter:   testClock.Now().Add(1 * time.Hour),
			GrantHash:       "test_grant_hash",
			DirectoryPeerID: "12345678901234567890",
			DelegatorPeerID: "12345678901234567891",
		},
		{
			ServiceName:     "test-service-2",
			ContentHash:     "test_content_hash",
			CreatedAt:       testClock.Now(),
			ValidNotBefore:  testClock.Now(),
			ValidNotAfter:   testClock.Now().Add(1 * time.Hour),
			GrantHash:       "test_grant_hash",
			DirectoryPeerID: "12345678901234567890",
			DelegatorPeerID: "12345678901234567891",
		}},
		TotalCount: 2}, "", nil
}

func (f *fakeManager) ListDelegatedServicePublications(ctx context.Context, args *manager.ListDelegatedServicePublicationsArgs) (*manager.Publications, string, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, "", errors.New("arbitrary error")
	}

	return &manager.Publications{Publications: []*manager.Publication{{
		ServicePeerID:   "12345678901234567890",
		ServiceName:     "mock-service",
		ContentHash:     "content-hash",
		CreatedAt:       testClock.Now(),
		ValidNotBefore:  testClock.Now(),
		ValidNotAfter:   testClock.Now(),
		State:           "",
		GrantHash:       "grant-hash",
		DelegatorPeerID: "12345678901234567892",
	},
	}, TotalCount: 1}, "", nil
}

func (f *fakeManager) ListTXLogRecords(ctx context.Context, args *manager.ListTXLogRecordsRequest) ([]*manager.TXLogRecord, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, errors.New("arbitrary error")
	}

	return []*manager.TXLogRecord{
		{
			TransactionID: "id-1",
			GrantHash:     "hash-1",
			ServiceName:   "service-1",
			Direction:     record.DirectionIn,
			Source: &manager.TXLogRecordSource{
				OutwayPeerID: "1",
			},
			Destination: &manager.TXLogRecordDestination{
				ServicePeerID: "2",
			},
			CreatedAt: time.Unix(1684938019, 0),
		},
		{
			TransactionID: "id-2",
			GrantHash:     "hash-2",
			ServiceName:   "service-2",
			Direction:     record.DirectionOut,
			Source: &manager.TXLogRecordDelegatedSource{
				OutwayPeerID:    "1",
				DelegatorPeerID: "2",
			},
			Destination: &manager.TXLogRecordDestination{
				ServicePeerID: "3",
			},
			CreatedAt: time.Unix(1684938020, 0),
		},
		{
			TransactionID: "id-3",
			GrantHash:     "hash-3",
			ServiceName:   "service-3",
			Direction:     record.DirectionIn,
			Source: &manager.TXLogRecordSource{
				OutwayPeerID: "1",
			},
			Destination: &manager.TXLogRecordDelegatedDestination{
				ServicePeerID:   "2",
				DelegatorPeerID: "3",
			},
			CreatedAt: time.Unix(1684938021, 0),
		},
		{
			TransactionID: "id-4",
			GrantHash:     "hash-4",
			ServiceName:   "service-4",
			Direction:     record.DirectionOut,
			Source: &manager.TXLogRecordDelegatedSource{
				OutwayPeerID:    "1",
				DelegatorPeerID: "2",
			},
			Destination: &manager.TXLogRecordDelegatedDestination{
				ServicePeerID:   "3",
				DelegatorPeerID: "4",
			},
			CreatedAt: time.Unix(1684938022, 0),
		},
	}, nil
}

func (f *fakeManager) GetPeerInfo(ctx context.Context) (*manager.Peer, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, errors.New("arbitrary error")
	}

	return &manager.Peer{
		ID:             "12345678901234567890",
		Name:           "Org Name",
		ManagerAddress: "https://manager.example.com:8443",
	}, nil
}

func (f *fakeManager) ListPeers(ctx context.Context, peerIDs *[]string) (manager.Peers, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) CreatePeer(ctx context.Context, args *manager.CreatePeerArgs) error {
	return errors.New("not implemented")
}

type ctxKey string

var ctxKeyManagerError ctxKey = "manager_error"
