// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type ListTXLogRecordsHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

func NewListTransactionLogRecordsHandler(m manager.Manager, a *auth.Auth) (*ListTXLogRecordsHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &ListTXLogRecordsHandler{
		manager: m,
		auth:    a,
	}, nil
}

type TXLogFilter struct {
	Period        *Period
	TransactionID string
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type ListTXLogRecordsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	DataSourcePeerID      string
	Pagination            *Pagination
	Filters               []*TXLogFilter
}

type TXLogRecords []*TXLogRecord

type TXLogRecord struct {
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type TXLogRecordSource struct {
	OutwayPeerID string
}

type TXLogRecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type TXLogRecordDestination struct {
	ServicePeerID string
}

type TXLogRecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

func (h *ListTXLogRecordsHandler) Handle(ctx context.Context, args *ListTXLogRecordsArgs) (TXLogRecords, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListTransactionLogs, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeTransactionLog, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	req, err := reqToRepo(args)
	if err != nil {
		return nil, errors.Join(err, errors.New("invalid request in transaction log records handler"))
	}

	records, err := h.manager.ListTXLogRecords(ctx, req)
	if err != nil {
		return nil, mapError(err, "could not get tx log records from manager")
	}

	return respToRecords(records)
}

func respToRecords(recs []*manager.TXLogRecord) (TXLogRecords, error) {
	records := make(TXLogRecords, len(recs))

	for i, r := range recs {
		var src interface{}
		switch s := r.Source.(type) {
		case *manager.TXLogRecordSource:
			src = &TXLogRecordSource{
				OutwayPeerID: s.OutwayPeerID,
			}
		case *manager.TXLogRecordDelegatedSource:
			src = &TXLogRecordDelegatedSource{
				OutwayPeerID:    s.OutwayPeerID,
				DelegatorPeerID: s.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		var dest interface{}
		switch d := r.Destination.(type) {
		case *manager.TXLogRecordDestination:
			dest = &TXLogRecordDestination{
				ServicePeerID: d.ServicePeerID,
			}
		case *manager.TXLogRecordDelegatedDestination:
			dest = &TXLogRecordDelegatedDestination{
				ServicePeerID:   d.ServicePeerID,
				DelegatorPeerID: d.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		records[i] = &TXLogRecord{
			TransactionID: r.TransactionID,
			GrantHash:     r.GrantHash,
			Direction:     r.Direction,
			ServiceName:   r.ServiceName,
			Source:        src,
			Destination:   dest,
			CreatedAt:     r.CreatedAt,
		}
	}

	return records, nil
}

func reqToRepo(req *ListTXLogRecordsArgs) (*manager.ListTXLogRecordsRequest, error) {
	filters := make([]*manager.TXLogFilter, len(req.Filters))

	for i, f := range req.Filters {
		id, err := record.NewTransactionIDFromString(f.TransactionID)
		if err != nil {
			return nil, errors.Join(err, errors.New("invalid transaction id in filter"))
		}

		filter := &manager.TXLogFilter{
			TransactionID: id,
			ServiceName:   f.ServiceName,
			GrantHash:     f.GrantHash,
			PeerID:        f.PeerID,
		}

		if f.Period != nil {
			filter.Period = &manager.Period{
				Start: f.Period.Start,
				End:   f.Period.End,
			}
		}

		filters[i] = filter
	}

	return &manager.ListTXLogRecordsRequest{
		DataSourcePeerID: req.DataSourcePeerID,
		Pagination: &manager.Pagination{
			StartID:   req.Pagination.StartID,
			Limit:     req.Pagination.Limit,
			SortOrder: sortOrderToProto(req.Pagination.SortOrder),
		},
		Filters: filters,
	}, nil
}

func sortOrderToProto(o SortOrder) manager.SortOrder {
	switch o {
	case SortOrderAscending:
		return manager.SortOrderAscending
	case SortOrderDescending:
		return manager.SortOrderDescending
	default:
		return manager.SortOrderUnspecified
	}
}
