// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type ListPeersHandler struct {
	manager manager.Manager
	auth    *auth.Auth
	logger  *logger.Logger
}

type ListPeersPeers map[string]*ListPeersPeer

type ListPeersPeer struct {
	ID             string
	Name           string
	ManagerAddress string
	Roles          []PeerRole
}

func NewListPeersHandler(mngr manager.Manager, a *auth.Auth, lgr *logger.Logger) (*ListPeersHandler, error) {
	if mngr == nil {
		return nil, errors.New("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if lgr == nil {
		return nil, errors.New("logger is required")
	}

	return &ListPeersHandler{
		manager: mngr,
		auth:    a,
		logger:  lgr,
	}, nil
}

type ListPeersArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	PeerIDs               *[]string
}

func (h *ListPeersHandler) Handle(ctx context.Context, args *ListPeersArgs) (ListPeersPeers, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListPeers, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypePeer, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	res, err := h.manager.ListPeers(ctx, args.PeerIDs)
	if err != nil {
		return nil, errors.Wrap(err, "could not list peers from manager")
	}

	peers := map[string]*ListPeersPeer{}

	for _, peer := range res {
		peers[peer.ID] = &ListPeersPeer{
			ID:             peer.ID,
			Name:           peer.Name,
			ManagerAddress: peer.ManagerAddress,
			Roles:          fromManagerPeerRole(peer.Roles),
		}
	}

	return peers, nil
}
