// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	mock_storage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func TestNewGetHealthHandler_Storage_Nil(t *testing.T) {
	h, err := query.NewGetHealthHandler(nil)
	assert.Nil(t, h)
	assert.EqualError(t, err, "storage is required")
}

func TestGetHealthHandler_Handle(t *testing.T) {
	t.Parallel()

	s := mock_storage.NewMockStorage(t)

	ctx := context.Background()

	s.EXPECT().Ping(ctx).Return(nil)

	h, err := query.NewGetHealthHandler(s)
	require.NoError(t, err)

	err = h.Handle(ctx)
	assert.NoError(t, err)
}
