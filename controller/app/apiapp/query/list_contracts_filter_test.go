// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

// nolint:funlen,dupl // these tests do not fit in 100 lines
func TestListContractsFilters(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListContractsArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		Filters: []*query.ListContractsFilter{
			{
				ContentHash: "content-hash",
			},
			{
				GrantType: query.GrantTypeServicePublication,
			},
			{
				GrantType: query.GrantTypeServiceConnection,
			},
			{
				GrantType: query.GrantTypeDelegatedServicePublication,
			},
			{
				GrantType: query.GrantTypeDelegatedServiceConnection,
			},
		},
	}

	var tests = map[string]struct {
		args    *query.ListContractsArgs
		setup   func(context.Context, *mocks)
		want    query.Contracts
		wantErr error
	}{
		"happy_flow_single_filter": {
			args: func(a query.ListContractsArgs) *query.ListContractsArgs {
				a.Filters = []*query.ListContractsFilter{
					{
						GrantType: query.GrantTypeServiceConnection,
					},
				}
				return &a
			}(*validArgs),
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListContracts,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, &manager.ListContractsArgs{
						Pagination: &manager.ContractsPagination{
							Cursor: "",
							Limit:  0,
						},
						Filters: &manager.ContractsFilters{
							ContentHashes: []string{},
							GrantTypes: []manager.GrantType{
								manager.GrantTypeServiceConnection,
							},
						},
					}).
					Return(manager.Contracts{
						{
							IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
							GroupID:       "mock-group",
							Hash:          "hash-1",
							HashAlgorithm: manager.HashAlgSHA3_512,
							CreatedAt:     time.Unix(1684938019, 0),
							ValidFrom:     time.Unix(1684938019, 0),
							ValidUntil:    time.Unix(1684939000, 0),
							Peers: []string{
								"12345678901234567890",
								"12345678901234567899",
							},
							RevokeSignatures: map[string]manager.Signature{},
							AcceptSignatures: map[string]manager.Signature{
								"12345678901234567890": {
									SignedAt: time.Unix(1684938020, 0),
								},
							},
							RejectSignatures: map[string]manager.Signature{},
						},
					}, "", nil)
			},
			want: query.Contracts{
				&query.Contract{
					IV:               "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
					GroupID:          "mock-group",
					Hash:             "hash-1",
					HashAlgorithm:    query.HashAlgSHA3_512,
					CreatedAt:        time.Unix(1684938019, 0),
					ValidFrom:        time.Unix(1684938019, 0),
					ValidUntil:       time.Unix(1684939000, 0),
					Peers:            []string{"12345678901234567890", "12345678901234567899"},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
					},
					RevokeSignatures:                  map[string]query.Signature{},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
			},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListContracts,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, &manager.ListContractsArgs{
						Pagination: &manager.ContractsPagination{
							Cursor: "",
							Limit:  0,
						},
						Filters: &manager.ContractsFilters{
							ContentHashes: []string{"content-hash"},
							GrantTypes: []manager.GrantType{
								manager.GrantTypeServicePublication,
								manager.GrantTypeServiceConnection,
								manager.GrantTypeDelegatedServicePublication,
								manager.GrantTypeDelegatedServiceConnection,
							},
						},
					}).
					Return(manager.Contracts{
						{
							IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
							GroupID:       "mock-group",
							Hash:          "hash-1",
							HashAlgorithm: manager.HashAlgSHA3_512,
							CreatedAt:     time.Unix(1684938019, 0),
							ValidFrom:     time.Unix(1684938019, 0),
							ValidUntil:    time.Unix(1684939000, 0),
							Peers: []string{
								"12345678901234567890",
								"12345678901234567899",
							},
							RevokeSignatures: map[string]manager.Signature{},
							AcceptSignatures: map[string]manager.Signature{
								"12345678901234567890": {
									SignedAt: time.Unix(1684938020, 0),
								},
							},
							RejectSignatures: map[string]manager.Signature{},
						},
					}, "", nil)
			},
			want: query.Contracts{
				&query.Contract{
					IV:               "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
					GroupID:          "mock-group",
					Hash:             "hash-1",
					HashAlgorithm:    query.HashAlgSHA3_512,
					CreatedAt:        time.Unix(1684938019, 0),
					ValidFrom:        time.Unix(1684938019, 0),
					ValidUntil:       time.Unix(1684939000, 0),
					Peers:            []string{"12345678901234567890", "12345678901234567899"},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
					},
					RevokeSignatures:                  map[string]query.Signature{},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)

			h, err := query.NewListContractsHandler(m.manager, m.auth)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, m)
			}

			actual, _, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
