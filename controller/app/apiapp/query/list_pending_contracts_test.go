// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

// nolint:dupl // this is a test
package query_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

func TestNewListPendingContractsHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := query.NewListPendingContractsHandler(nil, auth)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestNewListPendingContractsHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := query.NewListPendingContractsHandler(newFakeManager(), nil)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestListPendingContracts(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListPendingContractsArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
	}

	var tests = map[string]struct {
		ctx     context.Context
		args    *query.ListPendingContractsArgs
		want    *query.PendingContracts
		wantErr error
	}{
		"when_manager_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a query.ListPendingContractsArgs) *query.ListPendingContractsArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow": {
			ctx:  context.Background(),
			args: validArgs,
			want: &query.PendingContracts{
				PendingContracts: []*query.PendingContract{{
					ContentHash: "content-hash",
					Grants: []*query.PendingContractGrant{
						{
							ServiceName:   "mock-service",
							ServicePeerID: "12345678901234567890",
							OutwayPeerID:  "12345678901234567891",
							GrantType:     query.GrantTypeServiceConnection,
						},
						{
							ServiceName:                   "mock-service",
							ServicePeerID:                 "12345678901234567890",
							OutwayPeerID:                  "12345678901234567891",
							DelegatorPeerID:               "12345678901234567892",
							ServicePublicationDelegatorID: "12345678901234567893",
							GrantType:                     query.GrantTypeDelegatedServiceConnection,
						},
						{
							ServiceName:   "mock-service",
							ServicePeerID: "12345678901234567890",
							GrantType:     query.GrantTypeServicePublication,
						},
						{
							ServiceName:                   "mock-service",
							ServicePeerID:                 "12345678901234567890",
							ServicePublicationDelegatorID: "12345678901234567893",
							GrantType:                     query.GrantTypeDelegatedServicePublication,
						},
					},
					CreatedAt: testClock.Now(),
				}},
				TotalCount: 4,
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, err := query.NewListPendingContractsHandler(newFakeManager(), auth)
			require.NoError(t, err)

			actual, _, err := h.Handle(tt.ctx, tt.args)

			assert.Equal(t, tt.want, actual)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
