// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"fmt"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
)

type InternalError struct {
	m string
}

func newInternalError(message string) *InternalError {
	return &InternalError{
		m: message,
	}
}

func (v *InternalError) Error() string {
	return "internal error: " + v.m
}

type AuthenticationError struct {
	m string
}

func newAuthenticationError(message string) *AuthenticationError {
	return &AuthenticationError{
		m: message,
	}
}

func (v *AuthenticationError) Error() string {
	return "authentication error: " + v.m
}

type AuthorizationError struct {
	m string
}

func newAuthorizationError(message string) *AuthorizationError {
	return &AuthorizationError{
		m: message,
	}
}

func (v *AuthorizationError) Error() string {
	return "authorization error: " + v.m
}

func mapError(err error, message string) error {
	var a *authentication.AuthenticationError
	if errors.As(err, &a) {
		return fmt.Errorf("%s: %s: %w", message, newAuthenticationError(a.Error()), err)
	}

	var az *authz.AuthorizationError
	if errors.As(err, &az) {
		return fmt.Errorf("%s: %s: %w", message, newAuthorizationError(az.Error()), err)
	}

	return fmt.Errorf("%s: %w", newInternalError(message), err)
}

type SortOrder string

const (
	SortOrderAscending  SortOrder = "asc"
	SortOrderDescending SortOrder = "desc"
)

type Pagination struct {
	StartID   string
	Limit     uint32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}

type PeerRole string

func (r PeerRole) String() string {
	return string(r)
}

func fromManagerPeerRole(peerRoles []manager.PeerRole) []PeerRole {
	if len(peerRoles) == 0 {
		return []PeerRole{}
	}

	roles := make([]PeerRole, len(peerRoles))

	for i, peerRole := range peerRoles {
		switch peerRole {
		case manager.PeerRoleDirectory:
			roles[i] = PeerRoleDirectory
		default:
			roles[i] = ""
		}
	}

	return roles
}

const (
	PeerRoleDirectory PeerRole = "PEER_ROLE_DIRECTORY"
)
