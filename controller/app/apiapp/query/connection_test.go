// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConnection_GetPeerIDs(t *testing.T) {
	t.Parallel()

	c := &Connection{
		ServicePeerID: "12345678901234567890",
		OutwayPeerID:  "12345678901234567891",
	}

	actual := c.GetPeerIDs()
	expected := []string{"12345678901234567890", "12345678901234567891"}
	assert.Equal(t, expected, actual)
}

func TestConnection_GetPeerIDsDelegated(t *testing.T) {
	t.Parallel()

	servicePublicationDelegatorPeerID := "12345678901234567892"
	delegatorPeerID := "12345678901234567893"

	c := &Connection{
		ServicePeerID:                     "12345678901234567890",
		OutwayPeerID:                      "12345678901234567891",
		ServicePublicationDelegatorPeerID: &servicePublicationDelegatorPeerID,
		DelegatorPeerID:                   &delegatorPeerID,
	}

	actual := c.GetPeerIDs()
	expected := []string{"12345678901234567890", "12345678901234567891", "12345678901234567893", "12345678901234567892"}
	assert.Equal(t, expected, actual)
}
