// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

// nolint:dupl // structure is similar but the details differ
package query

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListInwaysHandler struct {
	storage storage.Storage
	auth    *auth.Auth
	groupID *contract.GroupID
}

type NewListInwaysHandlerArgs struct {
	Storage storage.Storage
	Auth    *auth.Auth
	GroupID *contract.GroupID
}

type Inway struct {
	Name    string
	Address string
}

func NewListInwaysHandler(args *NewListInwaysHandlerArgs) (*ListInwaysHandler, error) {
	if args.Storage == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.GroupID == nil {
		return nil, fmt.Errorf("group id is required")
	}

	return &ListInwaysHandler{
		storage: args.Storage,
		auth:    args.Auth,
		groupID: args.GroupID,
	}, nil
}

type ListInwaysArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	SelfPeerID            string
}

func (h *ListInwaysHandler) Handle(ctx context.Context, args *ListInwaysArgs) ([]*Inway, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListInways, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeInway, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	inways, err := h.storage.ListInways(ctx, h.groupID.String())
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get inways from storage"), err)
	}

	resp := make([]*Inway, len(inways))

	for i, inway := range inways {
		resp[i] = &Inway{
			Name:    inway.Name,
			Address: inway.Address,
		}
	}

	return resp, nil
}
