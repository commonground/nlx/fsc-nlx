// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:funlen // these tests should not fit 100 lines
func TestListServices(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListServicesArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		SelfPeerID:            "12345678910000000000",
	}

	groupID := contract.GroupID("mock-group")

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *query.ListServicesArgs
		want    query.Services
		wantErr error
	}{
		"when_storage_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				m.storage.EXPECT().
					ListServices(ctx, groupID.String()).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a query.ListServicesArgs) *query.ListServicesArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(authz.NewAuthorizationError("arbitrary"))
			},
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow_one": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListServicePublications(ctx, &manager.ListServicePublicationsArgs{
						ContractStates: []contract.ContractState{contract.ContractStateValid},
						ServiceNames:   []string{"service-1"},
					}).
					Return(&manager.Publications{
						TotalCount:   1,
						Publications: []*manager.Publication{},
					}, "", nil)

				m.storage.EXPECT().
					ListServices(ctx, groupID.String()).
					Return(storage.Services{
						{
							Name:         "service-1",
							EndpointURL:  newURL(t, "api1.com"),
							InwayAddress: "inway1.com",
							GroupID:      groupID.String(),
						},
					}, nil)
			},
			want: query.Services{
				&query.Service{
					Name:         "service-1",
					EndpointURL:  newURL(t, "api1.com"),
					InwayAddress: "inway1.com",
					IsPublished:  false,
				},
			},
		},
		"happy_flow_multiple": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListServicePublications(ctx, &manager.ListServicePublicationsArgs{
						ContractStates: []contract.ContractState{contract.ContractStateValid},
						ServiceNames:   []string{"service-1", "service-2", "service-3", "service-4"},
					}).
					Return(&manager.Publications{
						Publications: []*manager.Publication{
							{
								ServiceName: "service-4",
								State:       string(manager.ContractStateValid),
							},
						},
					}, "", nil)

				m.storage.EXPECT().
					ListServices(ctx, groupID.String()).
					Return(storage.Services{
						{
							GroupID:      groupID.String(),
							Name:         "service-1",
							EndpointURL:  newURL(t, "api1.com"),
							InwayAddress: "inway1.com",
						},
						{
							GroupID:      groupID.String(),
							Name:         "service-2",
							EndpointURL:  newURL(t, "api2.com"),
							InwayAddress: "inway2.com",
						},
						{
							GroupID:      groupID.String(),
							Name:         "service-3",
							EndpointURL:  newURL(t, "api3.com"),
							InwayAddress: "inway3.com",
						},
						{
							GroupID:      groupID.String(),
							Name:         "service-4",
							EndpointURL:  newURL(t, "api4.com"),
							InwayAddress: "inway4.com",
						},
					}, nil)
			},
			want: query.Services{
				&query.Service{
					Name:         "service-1",
					EndpointURL:  newURL(t, "api1.com"),
					InwayAddress: "inway1.com",
					IsPublished:  false,
				},
				&query.Service{
					Name:         "service-2",
					EndpointURL:  newURL(t, "api2.com"),
					InwayAddress: "inway2.com",
					IsPublished:  false,
				},
				&query.Service{
					Name:         "service-3",
					EndpointURL:  newURL(t, "api3.com"),
					InwayAddress: "inway3.com",
					IsPublished:  false,
				},
				&query.Service{
					Name:         "service-4",
					EndpointURL:  newURL(t, "api4.com"),
					InwayAddress: "inway4.com",
					IsPublished:  true,
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)

			h, err := query.NewListServicesHandler(&query.NewListServicesHandlerArgs{
				Storage: m.storage,
				Manager: m.manager,
				Auth:    m.auth,
				GroupID: &groupID,
			})
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, m)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
