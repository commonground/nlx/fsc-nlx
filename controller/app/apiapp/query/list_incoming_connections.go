// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListContractsWithIncomingConnections
package query

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListIncomingConnectionsHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

type ListIncomingConnectionsPagination struct {
	Cursor    string
	Limit     uint32
	SortOrder SortOrder
}

type IncomingConnections struct {
	Connections []*Connection
	TotalCount  int
}

func NewListIncomingConnectionsHandler(m manager.Manager, a *auth.Auth) (*ListIncomingConnectionsHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &ListIncomingConnectionsHandler{
		manager: m,
		auth:    a,
	}, nil
}

type ListIncomingConnectionsArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Pagination            *ListIncomingConnectionsPagination
	ContractStates        []contract.ContractState
	ServiceNames          []string
}

// nolint:dupl // handler is similar but not the same
func (h *ListIncomingConnectionsHandler) Handle(ctx context.Context, args *ListIncomingConnectionsArgs) (*IncomingConnections, string, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListContracts, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, "*"),
	})
	if err != nil {
		return nil, "", mapError(err, "could not authenticate or authorize")
	}

	pagination := manager.ListIncomingConnectionsPagination{}

	if args.Pagination != nil && args.Pagination.Cursor != "" {
		pagination.Cursor = args.Pagination.Cursor
	}

	if args.Pagination != nil && args.Pagination.Limit != 0 {
		pagination.Limit = args.Pagination.Limit
	}

	if args.Pagination != nil && args.Pagination.SortOrder == SortOrderAscending {
		pagination.SortOrder = manager.SortOrderAscending
	} else if args.Pagination != nil && args.Pagination.SortOrder == SortOrderDescending {
		pagination.SortOrder = manager.SortOrderDescending
	}

	resp, paginationNextCursor, err := h.manager.ListIncomingConnections(ctx, &manager.ListIncomingConnectionsArgs{
		Pagination:     &pagination,
		ContractStates: args.ContractStates,
		ServiceNames:   args.ServiceNames,
	})
	if err != nil {
		return nil, "", fmt.Errorf("%s: %w", newInternalError("could not get incoming connections from manager"), err)
	}

	connections := make([]*Connection, len(resp.Connections))

	for i, c := range resp.Connections {
		connections[i] = convertConnection(c)
	}

	return &IncomingConnections{
		Connections: connections,
		TotalCount:  resp.TotalCount,
	}, paginationNextCursor, nil
}
