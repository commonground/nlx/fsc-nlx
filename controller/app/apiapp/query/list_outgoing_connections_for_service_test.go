// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

// nolint:dupl // this is a test
package query_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

func TestNewListOutgoingConnectionsForServiceHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := query.NewListOutgoingConnectionsForServiceHandler(nil, auth)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestNewListOutgoingConnectionsForServiceHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := query.NewListOutgoingConnectionsForServiceHandler(newFakeManager(), nil)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

// nolint:funlen // this is a test
func TestListOutgoingConnectionsForService(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListOutgoingConnectionsForServiceArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		ServiceName:           "mock-service",
		ServicePeerID:         "12345678901234567890",
	}

	tests := map[string]struct {
		ctx     context.Context
		args    *query.ListOutgoingConnectionsForServiceArgs
		want    []*query.Connection
		wantErr error
	}{
		"when_manager_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.InternalError{},
		},
		"when_manager_delegated_publication_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			args: func(a query.ListOutgoingConnectionsForServiceArgs) *query.ListOutgoingConnectionsForServiceArgs {
				//nolint:goconst // making this a constant would make the code less readable
				a.ServicePublicationDelegatorPeerID = "12345678901234567892"
				return &a
			}(*validArgs),
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a query.ListOutgoingConnectionsForServiceArgs) *query.ListOutgoingConnectionsForServiceArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow": {
			ctx:  context.Background(),
			args: validArgs,
			want: []*query.Connection{
				{
					ServicePeerID:             "12345678901234567890",
					ServiceName:               "mock-service",
					OutwayPeerID:              "12345678901234567891",
					OutwayPublicKeyThumbprint: "outway-public-key-thumbprint",
					ContentHash:               "content-hash",
					CreatedAt:                 testClock.Now(),
					ValidNotBefore:            testClock.Now(),
					ValidNotAfter:             testClock.Now(),
					GrantHash:                 "grant-hash",
				},
			},
		},
		"happy_flow_delegated_publication": {
			ctx: context.Background(),
			args: func(a query.ListOutgoingConnectionsForServiceArgs) *query.ListOutgoingConnectionsForServiceArgs {
				//nolint:goconst // making this a constant would make the code less readable
				a.ServicePublicationDelegatorPeerID = "12345678901234567892"
				return &a
			}(*validArgs),
			want: func() []*query.Connection {
				servicePublicationDelegatorPeerID := "12345678901234567892"

				return []*query.Connection{
					{
						ServicePublicationDelegatorPeerID: &servicePublicationDelegatorPeerID,
						ServicePeerID:                     "12345678901234567890",
						ServiceName:                       "mock-service",
						OutwayPeerID:                      "12345678901234567891",
						OutwayPublicKeyThumbprint:         "outway-public-key-thumbprint",
						ContentHash:                       "content-hash",
						CreatedAt:                         testClock.Now(),
						ValidNotBefore:                    testClock.Now(),
						ValidNotAfter:                     testClock.Now(),
						GrantHash:                         "grant-hash",
					},
				}
			}(),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, err := query.NewListOutgoingConnectionsForServiceHandler(newFakeManager(), auth)
			require.NoError(t, err)

			actual, err := h.Handle(tt.ctx, tt.args)

			assert.Equal(t, tt.want, actual)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
