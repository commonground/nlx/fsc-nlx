// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query_test

import (
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	mock_audit_logger "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog/mock"
	mock_id_generator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	mock_authorization "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/mock"
	mock_manager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/mock"
	mock_storage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

type mocks struct {
	manager     *mock_manager.MockManager
	storage     *mock_storage.MockStorage
	audit       *mock_audit_logger.MockAuditLogger
	auditlogger *auditlog.AuditLogger
	auth        *auth_helper.Auth
	authz       *mock_authorization.MockAuthorization
	clock       *testingutils.MockClock
	logger      *logger.Logger
}

func newMocks(t *testing.T) *mocks {
	nowInUTC := time.Now().UTC()

	testClock := testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))

	authz := mock_authorization.NewMockAuthorization(t)

	auth, err := auth_helper.New(authz)
	assert.NoError(t, err)

	auditMock := mock_audit_logger.NewMockAuditLogger(t)

	auditLogger, err := auditlog.New(&auditlog.NewArgs{AuditLogger: auditMock, GroupID: "fsc-local", Clock: testClock, IDGenerator: mock_id_generator.NewMockIDGenerator(t)})
	assert.NoError(t, err)

	return &mocks{
		manager:     mock_manager.NewMockManager(t),
		storage:     mock_storage.NewMockStorage(t),
		audit:       auditMock,
		auditlogger: auditLogger,
		authz:       authz,
		auth:        auth,
		clock:       testClock,
		logger:      discardlogger.New(),
	}
}

func newURL(t *testing.T, u string) *url.URL {
	ur, err := url.Parse(u)
	require.NoError(t, err)

	return ur
}
