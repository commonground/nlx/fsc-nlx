// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListContractsWithIncomingConnections
package query

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type ListOutgoingConnectionsForServiceHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

func NewListOutgoingConnectionsForServiceHandler(m manager.Manager, a *auth.Auth) (*ListOutgoingConnectionsForServiceHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &ListOutgoingConnectionsForServiceHandler{
		manager: m,
		auth:    a,
	}, nil
}

type ListOutgoingConnectionsForServiceArgs struct {
	AuthData                          authentication.Data
	AuthorizationMetadata             authorization.Metadata
	ServiceName                       string
	ServicePublicationDelegatorPeerID string
	ServicePeerID                     string
	SortOrder                         *SortOrder
}

// nolint:dupl // handler is similar but not the same
func (h *ListOutgoingConnectionsForServiceHandler) Handle(ctx context.Context, args *ListOutgoingConnectionsForServiceArgs) ([]*Connection, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListContracts, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	var sortOrder manager.SortOrder

	if args.SortOrder != nil && *args.SortOrder == SortOrderAscending {
		sortOrder = manager.SortOrderAscending
	} else if args.SortOrder != nil && *args.SortOrder == SortOrderDescending {
		sortOrder = manager.SortOrderDescending
	}

	var connections []*manager.Connection

	if args.ServicePublicationDelegatorPeerID != "" {
		connections, err = h.manager.ListOutgoingConnectionsForDelegatedService(ctx, &manager.ListOutgoingConnectionsForDelegatedServiceArgs{
			SortOrder:       sortOrder,
			DelegatorPeerID: args.ServicePublicationDelegatorPeerID,
			PeerID:          args.ServicePeerID,
			ServiceName:     args.ServiceName,
		})
		if err != nil {
			return nil, fmt.Errorf("%s: %w", newInternalError("could not get outgoing connections for delegated service from manager"), err)
		}
	} else {
		connections, err = h.manager.ListOutgoingConnectionsForService(ctx, &manager.ListOutgoingConnectionsForServiceArgs{
			SortOrder:   sortOrder,
			PeerID:      args.ServicePeerID,
			ServiceName: args.ServiceName,
		})
		if err != nil {
			return nil, fmt.Errorf("%s: %w", newInternalError("could not get outgoing connections for service from manager"), err)
		}
	}

	resp := make([]*Connection, len(connections))

	for i, c := range connections {
		resp[i] = convertConnection(c)
	}

	return resp, nil
}
