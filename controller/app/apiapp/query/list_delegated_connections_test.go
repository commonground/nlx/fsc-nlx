// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

func TestNewListDelegatedConnectionsHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := query.NewListDelegatedConnectionsHandler(nil, auth)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestNewListDelegatedConnectionsHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := query.NewListDelegatedConnectionsHandler(newFakeManager(), nil)

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestListDelegatedConnections(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListDelegatedConnectionsArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
	}

	//nolint:goconst // making this a constant will reduce readability and maintainability
	delegatorPeerID := "12345678901234567892"

	tests := map[string]struct {
		ctx     context.Context
		args    *query.ListDelegatedConnectionsArgs
		want    *query.DelegatedConnections
		wantErr error
	}{
		"when_manager_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a query.ListDelegatedConnectionsArgs) *query.ListDelegatedConnectionsArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow": {
			ctx:  context.Background(),
			args: validArgs,
			want: &query.DelegatedConnections{
				Connections: []*query.Connection{
					{
						OutwayPeerID:              "12345678901234567891",
						OutwayPublicKeyThumbprint: "thumbprint",
						ServicePeerID:             "12345678901234567890",
						ServiceName:               "mock-service",
						ContentHash:               "content-hash",
						CreatedAt:                 testClock.Now(),
						ValidNotBefore:            testClock.Now(),
						ValidNotAfter:             testClock.Now(),
						GrantHash:                 "grant-hash",
						DelegatorPeerID:           &delegatorPeerID,
					},
				},
				TotalCount: 1,
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, err := query.NewListDelegatedConnectionsHandler(newFakeManager(), auth)
			require.NoError(t, err)

			actual, _, err := h.Handle(tt.ctx, tt.args)

			assert.Equal(t, tt.want, actual)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
