// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query_test

import (
	"context"
	"errors"
	"net/netip"
	"time"

	"github.com/gofrs/uuid"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	common_testing "gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var ctxKeyAuditLoggerError ctxKey = "audit_logger_error"

type fakeAuditorLogger struct {
}

func (f *fakeAuditorLogger) CreateRecords(ctx context.Context, records common_auditlog.Records) error {
	return errors.New("not implemented")
}

var testClock = common_testing.NewMockClock(time.Now())

//nolint:funlen // this is a long function because of the amount of records needed for the tests
func (f *fakeAuditorLogger) ListRecords(ctx context.Context, args *common_auditlog.ListRecordsArgs) (common_auditlog.Records, error) {
	if ctx.Value(ctxKeyAuditLoggerError) != nil {
		return nil, errors.New("arbitrary error")
	}

	return []*common_auditlog.Record{
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c001"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventCreateContract{
				ContractHash: "test-contract-hash",
			},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status:    &common_auditlog.StatusInitiated{},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c002"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventAcceptContract{
				ContractHash: "test-contract-hash",
			},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status: &common_auditlog.StatusFailed{
				Error: "some error",
			},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c003"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor:         &common_auditlog.ActorUnknown{},
			Event: &common_auditlog.EventRejectContract{
				ContractHash: "test-contract-hash",
			},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status: &common_auditlog.StatusFailed{
				Error: "some error",
			},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c004"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventRevokeContract{
				ContractHash: "test-contract-hash",
			},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.2"),
				UserAgent: "firefox mozilla",
			},
			Status: &common_auditlog.StatusFailed{
				Error: "some error",
			},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c005"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorService{
				SubjectCommonName:   "testSystem",
				PublicKeyThumbprint: "test-public-key-fingerprint",
			},
			Event: &common_auditlog.EventCreateService{
				ServiceName: "service",
			},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status:    &common_auditlog.StatusSucceeded{},
			Component: common_auditlog.ComponentController,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c006"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventUpdateService{
				ServiceName: "service",
			},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status:    &common_auditlog.StatusSucceeded{},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c007"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventDeleteService{
				ServiceName: "service",
			},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status:    &common_auditlog.StatusSucceeded{},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c008"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventCreatePeer{},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status: &common_auditlog.StatusUnauthorized{
				Error: "unauthorized access",
			},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c009"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventUpdatePeer{
				PeerID: "test-peer-id",
			},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status:    &common_auditlog.StatusSucceeded{},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c010"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventFailedDistributionRetry{
				PeerID:      "test-peer-id",
				ContentHash: "test-content-hash",
			},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status:    &common_auditlog.StatusSucceeded{},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c010"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventSynchronizePeers{},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status:    &common_auditlog.StatusSucceeded{},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
		{
			ID:            common_auditlog.ID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			GroupID:       "fsc-local",
			CorrelationID: common_auditlog.CorrelationID(uuid.Must(uuid.FromString("ce754ff9-3d1d-4dac-9f76-2267ff36c011"))),
			Actor: &common_auditlog.ActorUser{
				Name:  "test-user",
				Email: "test@example.com",
			},
			Event: &common_auditlog.EventLogin{},
			Source: &common_auditlog.SourceHTTP{
				IPAddress: netip.MustParseAddr("127.0.0.1"),
				UserAgent: "firefox mozilla",
			},
			Status:    &common_auditlog.StatusSucceeded{},
			Component: common_auditlog.ComponentManager,
			CreatedAt: testClock.Now(),
		},
	}, nil
}
