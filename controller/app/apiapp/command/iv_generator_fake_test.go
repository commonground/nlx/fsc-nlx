// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command_test

import "github.com/gofrs/uuid"

type fakeIVGenerator struct{}

func (t *fakeIVGenerator) New() (string, error) {
	testUUID, _ := uuid.FromString("222dea14-cdb0-7701-99ba-d55ece9e3894")

	return testUUID.String(), nil
}
