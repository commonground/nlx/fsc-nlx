/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package command

import (
	"context"
	"fmt"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type RetryContractHandler struct {
	auditLgr *auditlog.AuditLogger
	manager  manager.Manager
	auth     *auth.Auth
	lgr      *logger.Logger
}

type NewRetryContractHandlerParams struct {
	AuditLogger *auditlog.AuditLogger
	Manager     manager.Manager
	Logger      *logger.Logger
	Auth        *auth.Auth
}

func NewRetryContractHandler(args *NewRetryContractHandlerParams) (*RetryContractHandler, error) {
	if args.Manager == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if args.AuditLogger == nil {
		return nil, fmt.Errorf("audit logger is required")
	}

	return &RetryContractHandler{
		manager:  args.Manager,
		auth:     args.Auth,
		lgr:      args.Logger,
		auditLgr: args.AuditLogger,
	}, nil
}

type RetryContractHandlerArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	AuditLogSource        common_auditlog.Source
	ContentHash           string
	PeerID                string
	Action                Action
}

type Action int

const (
	SubmitContract Action = iota
	SubmitAcceptSignature
	SubmitRejectSignature
	SubmitRevokeSignature
)

func (h *RetryContractHandler) Handle(ctx context.Context, args *RetryContractHandlerArgs) error {
	handlerFunc := func(auditlogCorrelationID string) error {
		err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscRetryContract, []*authz.ResourceURN{
			authz.NewResourceURN(authz.ResourceTypeContract, args.ContentHash),
		})
		if err != nil {
			return mapError(err, "could not authenticate or authorize")
		}

		if args.ContentHash == "" {
			return newValidationError([]string{"contentHash cannot be empty"})
		}

		if args.PeerID == "" {
			return newValidationError([]string{"peer ID cannot be empty"})
		}

		action := convertAction(args.Action)
		if action == -1 {
			return newValidationError([]string{"invalid action provided"})
		}

		err = h.manager.RetryContract(ctx, args.ContentHash, args.PeerID, action, auditlogCorrelationID)
		if err != nil {
			h.lgr.Error("failed to retry contract distribution", err)

			return mapError(err, "could not retry contract in manager")
		}

		return nil
	}

	var auditLogAction common_auditlog.FailedDistributionRetryAction

	switch args.Action {
	case SubmitContract:
		auditLogAction = common_auditlog.FailedDistributionRetryActionSubmitContract
	case SubmitRejectSignature:
		auditLogAction = common_auditlog.FailedDistributionRetryActionSubmitRejectSignature
	case SubmitAcceptSignature:
		auditLogAction = common_auditlog.FailedDistributionRetryActionSubmitAcceptSignature
	case SubmitRevokeSignature:
		auditLogAction = common_auditlog.FailedDistributionRetryActionSubmitRevokeSignature
	}

	err := h.auditLgr.AddRecord(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData: args.AuthData,
		Event: common_auditlog.EventFailedDistributionRetry{
			ContentHash: args.ContentHash,
			PeerID:      args.PeerID,
			Action:      auditLogAction,
		},
		Source: args.AuditLogSource,
	})
	if err != nil {
		return mapError(err, "could not create AuditLog record")
	}

	return nil
}

func convertAction(action Action) manager.Action {
	switch action {
	case SubmitContract:
		return manager.SubmitContract
	case SubmitAcceptSignature:
		return manager.SubmitAcceptSignature
	case SubmitRejectSignature:
		return manager.SubmitRejectSignature
	case SubmitRevokeSignature:
		return manager.SubmitRevokeSignature
	default:
		return -1
	}
}
