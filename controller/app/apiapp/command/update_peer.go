// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type UpdatePeerHandler struct {
	lgr      *logger.Logger
	s        storage.Storage
	auth     *auth.Auth
	auditLgr *auditlog.AuditLogger
	manager  manager.Manager
}

type NewUpdatePeerHandlerArgs struct {
	Manager     manager.Manager
	Auth        *auth.Auth
	AuditLogger *auditlog.AuditLogger
	Logger      *logger.Logger
}

func NewUpdatePeerHandler(args *NewUpdatePeerHandlerArgs) (*UpdatePeerHandler, error) {
	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.Manager == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if args.AuditLogger == nil {
		return nil, fmt.Errorf("audit logger is required")
	}

	return &UpdatePeerHandler{
		lgr:      args.Logger,
		auth:     args.Auth,
		auditLgr: args.AuditLogger,
		manager:  args.Manager,
	}, nil
}

type UpdatePeerArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	AuditLogSource        common_auditlog.Source
	ID                    string
	Name                  string
	ManagerAddress        string
	Roles                 []PeerRole
}

// nolint:dupl // similar to create peer
func (h *UpdatePeerHandler) Handle(ctx context.Context, args *UpdatePeerArgs) error {
	handlerFunc := func(auditlogCorrelationID string) error {
		err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscUpdatePeer, []*authz.ResourceURN{
			authz.NewResourceURN(authz.ResourceTypePeer, args.ID),
		})
		if err != nil {
			return mapError(err, "could not authenticate or authorize")
		}

		if err := args.Valid(); err != nil {
			return err
		}

		err = h.manager.UpdatePeer(ctx, &manager.UpdatePeerArgs{
			PeerID:                args.ID,
			PeerName:              args.Name,
			ManagerAddress:        args.ManagerAddress,
			AuditlogCorrelationID: auditlogCorrelationID,
			Roles:                 toManagerPeerRole(args.Roles),
		})
		if err != nil {
			h.lgr.Error("failed to update peer", err)

			return mapError(err, "could not update peer in manager")
		}

		return nil
	}

	err := h.auditLgr.AddRecord(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData: args.AuthData,
		Event:    common_auditlog.EventUpdatePeer{PeerID: args.ID},
		Source:   args.AuditLogSource,
	})
	if err != nil {
		return mapError(err, "could not create AuditLog record")
	}

	return nil
}

func (c *UpdatePeerArgs) Valid() *ValidationError {
	validationErrors := make([]string, 0)

	if c.ID == "" {
		validationErrors = append(validationErrors, "peer ID cannot be blank")
	}

	if c.Name == "" {
		validationErrors = append(validationErrors, "peer name cannot be blank")
	}

	if !managerAddressValidator.MatchString(c.ManagerAddress) {
		validationErrors = append(validationErrors, "manager address cannot be blank and must start with https:// and end with :8443")
	}

	return mapValidationError(validationErrors)
}
