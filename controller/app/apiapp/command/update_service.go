// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type UpdateServiceHandler struct {
	groupID  contract.GroupID
	lgr      *logger.Logger
	s        storage.Storage
	auth     *auth.Auth
	auditLgr *auditlog.AuditLogger
}

type NewUpdateServiceHandlerArgs struct {
	GroupID     contract.GroupID
	Auth        *auth.Auth
	AuditLogger *auditlog.AuditLogger
	Logger      *logger.Logger
	Storage     storage.Storage
}

//nolint:dupl // looks the same as CreateServiceHandler but is not
func NewUpdateServiceHandler(args *NewUpdateServiceHandlerArgs) (*UpdateServiceHandler, error) {
	if args.GroupID == "" {
		return nil, fmt.Errorf("group id is required")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if args.Storage == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.AuditLogger == nil {
		return nil, fmt.Errorf("audit logger is required")
	}

	return &UpdateServiceHandler{
		groupID:  args.GroupID,
		lgr:      args.Logger,
		s:        args.Storage,
		auth:     args.Auth,
		auditLgr: args.AuditLogger,
	}, nil
}

type UpdateServiceArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	AuditLogSource        common_auditlog.Source
	Name                  string
	EndpointURL           string
	InwayAddress          string
}

func (h *UpdateServiceHandler) Handle(ctx context.Context, args *UpdateServiceArgs) error {
	handlerFunc := func(_ string) error {
		err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscUpdateService, []*authz.ResourceURN{
			authz.NewResourceURN(authz.ResourceTypeService, args.Name),
		})
		if err != nil {
			return mapError(err, "could not authenticate or authorize")
		}

		if err := args.Valid(); err != nil {
			return err
		}

		err = h.s.UpdateService(ctx, &storage.UpdateServiceArgs{
			GroupID:      h.groupID.String(),
			Name:         args.Name,
			EndpointURL:  args.EndpointURL,
			InwayAddress: args.InwayAddress,
		})
		if err != nil {
			if errors.Is(err, storage.ErrServiceNotFound) {
				return ErrServiceNotFound
			}

			h.lgr.Error("failed to update service", err)

			return mapError(err, "could not update service in storage")
		}

		return nil
	}

	err := h.auditLgr.AddRecord(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData: args.AuthData,
		Event:    common_auditlog.EventUpdateService{ServiceName: args.Name},
		Source:   args.AuditLogSource,
	})
	if err != nil {
		return mapError(err, "could not create AuditLog record")
	}

	return nil
}

func (c *UpdateServiceArgs) Valid() *ValidationError {
	validationErrors := make([]string, 0)

	if c.Name == "" {
		validationErrors = append(validationErrors, "service name cannot be blank")
	}

	if !serviceNameValidator.MatchString(c.Name) {
		validationErrors = append(validationErrors, "service name must contain only letters, numbers, -, _ and .")
	}

	if !serviceEndpointURLValidator.MatchString(c.EndpointURL) {
		validationErrors = append(validationErrors, "endpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash")
	}

	return mapValidationError(validationErrors)
}
