// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type CreatePeerHandler struct {
	lgr      *logger.Logger
	manager  manager.Manager
	auth     *auth.Auth
	auditLgr *auditlog.AuditLogger
}

type NewCreatePeerHandlerArgs struct {
	Logger      *logger.Logger
	Manager     manager.Manager
	Auth        *auth.Auth
	AuditLogger *auditlog.AuditLogger
}

func NewCreatePeerHandler(args *NewCreatePeerHandlerArgs) (*CreatePeerHandler, error) {
	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if args.Manager == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.AuditLogger == nil {
		return nil, fmt.Errorf("audit logger is required")
	}

	return &CreatePeerHandler{
		lgr:      args.Logger,
		manager:  args.Manager,
		auth:     args.Auth,
		auditLgr: args.AuditLogger,
	}, nil
}

type CreatePeerArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	AuditLogSource        common_auditlog.Source
	Name                  string
	PeerID                string
	ManagerAddress        string
	Roles                 []PeerRole
}

//nolint:dupl // looks the same as update peer but is different
func (h *CreatePeerHandler) Handle(ctx context.Context, args *CreatePeerArgs) error {
	handlerFunc := func(auditlogCorrelationID string) error {
		err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscCreatePeer, []*authz.ResourceURN{
			authz.NewResourceURN(authz.ResourceTypePeer, args.PeerID),
		})
		if err != nil {
			return mapError(err, "could not authenticate or authorize")
		}

		if errArgs := args.Valid(); errArgs != nil {
			return errArgs
		}

		err = h.manager.CreatePeer(ctx, &manager.CreatePeerArgs{
			PeerID:                args.PeerID,
			PeerName:              args.Name,
			ManagerAddress:        args.ManagerAddress,
			AuditlogCorrelationID: auditlogCorrelationID,
			Roles:                 toManagerPeerRole(args.Roles),
		})
		if err != nil {
			h.lgr.Error("failed to create peer", err)
			return mapError(err, "could not create peer in manager")
		}

		return nil
	}

	err := h.auditLgr.AddRecord(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData: args.AuthData,
		Event: common_auditlog.EventCreatePeer{
			PeerID: args.PeerID,
		},
		Source: args.AuditLogSource,
	})
	if err != nil {
		return mapError(err, "could not create AuditLog record")
	}

	return nil
}

func (c *CreatePeerArgs) Valid() *ValidationError {
	validationErrors := make([]string, 0)

	if c.Name == "" {
		validationErrors = append(validationErrors, "peer name cannot be blank")
	}

	if c.PeerID == "" {
		validationErrors = append(validationErrors, "peer ID cannot be blank")
	}

	if c.ManagerAddress == "" {
		validationErrors = append(validationErrors, "manager address cannot be blank")
	}

	if !managerAddressValidator.MatchString(c.ManagerAddress) {
		validationErrors = append(validationErrors, "manager address must start with https:// and end with :8443")
	}

	return mapValidationError(validationErrors)
}
