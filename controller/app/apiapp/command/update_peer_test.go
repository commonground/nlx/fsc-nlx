// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // these are the tests for the update peer command, they look similar to the tests for the create peer command
package command_test

import (
	"context"
	"net/netip"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	discard_logger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestNewUpdatePeerHandler_LoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewUpdatePeerHandler(&command.NewUpdatePeerHandlerArgs{
		Logger:      nil,
		Manager:     &fakeManager{},
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "logger is required")
}

func TestNewUpdatePeerHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := command.NewUpdatePeerHandler(&command.NewUpdatePeerHandlerArgs{
		Logger:      discard_logger.New(),
		Manager:     &fakeManager{},
		Auth:        nil,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewUpdatePeerHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewUpdatePeerHandler(&command.NewUpdatePeerHandlerArgs{
		Logger:      discard_logger.New(),
		Manager:     nil,
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestNewUpdatePeerHandler_AuditLoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewUpdatePeerHandler(&command.NewUpdatePeerHandlerArgs{
		Logger:      discard_logger.New(),
		Manager:     &fakeManager{},
		Auth:        auth,
		AuditLogger: nil,
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "audit logger is required")
}

// nolint:funlen // these tests should not fit 100 lines and use the same pattern as other tests
func TestUpdatePeer(t *testing.T) {
	t.Parallel()

	testUUID, err := uuid.FromString("222dea14-cdb0-7701-99ba-d55ece9e3894")
	require.NoError(t, err)

	nowInUTC := time.Now()
	testClock := testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	testIP, err := netip.ParseAddr("0.0.0.0")
	require.NoError(t, err)

	validArgs := &command.UpdatePeerArgs{
		AuthData: &authentication.OIDCData{
			Email: "mail@example.com",
			Name:  "John Doe",
		},
		AuthorizationMetadata: &authorization.RestMetaData{},
		AuditLogSource: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
		Name:           "Organization",
		ID:             "12345678901234567890",
		ManagerAddress: "https://manager.example.com:8443",
	}

	testCases := map[string]struct {
		ctx                 context.Context
		args                *command.UpdatePeerArgs
		wantPeers           manager.Peers
		wantErrMsg          string
		wantAuditLogRecords common_auditlog.Records
	}{
		"happy_flow": {
			args: validArgs,
			ctx:  context.Background(),
			wantPeers: manager.Peers{
				{
					Name:           validArgs.Name,
					ID:             validArgs.ID,
					ManagerAddress: validArgs.ManagerAddress,
				},
			},
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusSucceeded{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"invalid_name": {
			args: func(a command.UpdatePeerArgs) *command.UpdatePeerArgs {
				a.Name = ""
				return &a
			}(*validArgs),
			ctx:        context.Background(),
			wantErrMsg: "validation error: peer name cannot be blank",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: peer name cannot be blank",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"invalid_peer_id": {
			args: func(a command.UpdatePeerArgs) *command.UpdatePeerArgs {
				a.ID = ""
				return &a
			}(*validArgs),
			ctx:        context.Background(),
			wantErrMsg: "validation error: peer ID cannot be blank",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: peer ID cannot be blank",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"invalid_manager_address": {
			args: func(a command.UpdatePeerArgs) *command.UpdatePeerArgs {
				a.ManagerAddress = ""
				return &a
			}(*validArgs),
			ctx:        context.Background(),
			wantErrMsg: "validation error: manager address cannot be blank and must start with https:// and end with :8443",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: manager address cannot be blank and must start with https:// and end with :8443",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"manager_fails": {
			args: func(a command.UpdatePeerArgs) *command.UpdatePeerArgs {
				return &a
			}(*validArgs),
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			wantErrMsg: "could not update peer in manager: arbitrary error",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not update peer in manager: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"authorization_fails": {
			args: func(a command.UpdatePeerArgs) *command.UpdatePeerArgs {
				return &a
			}(*validArgs),
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			wantErrMsg: "could not authenticate or authorize: arbitrary error",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdatePeer{
						PeerID: validArgs.ID,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not authenticate or authorize: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
	}

	for name, tt := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			fakeAuditLogger := &fakeAuditorLogger{}

			auditLogger, errAuditLog := auditlog.New(&auditlog.NewArgs{AuditLogger: fakeAuditLogger, GroupID: "fsc-test", Clock: testClock, IDGenerator: &fakeIVGenerator{}})
			require.NoError(t, errAuditLog)

			fManager := newFakeManager()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			err = fManager.CreatePeer(context.Background(), &manager.CreatePeerArgs{
				PeerID:         validArgs.ID,
				PeerName:       "Before Update",
				ManagerAddress: "https://manager.example.before.update.com:8443",
			})
			require.NoError(t, err)

			h, errCommand := command.NewUpdatePeerHandler(&command.NewUpdatePeerHandlerArgs{
				Logger:      discard_logger.New(),
				Manager:     fManager,
				Auth:        auth,
				AuditLogger: auditLogger,
			})
			require.NoError(t, errCommand)

			errCommand = h.Handle(tt.ctx, tt.args)

			logs, _ := fakeAuditLogger.ListRecords(tt.ctx, &common_auditlog.ListRecordsArgs{})
			assert.Equal(t, 2, len(logs))

			assert.Equal(t, tt.wantAuditLogRecords, logs)

			if tt.wantErrMsg == "" {
				assert.NoError(t, errCommand)

				peers, _ := fManager.ListPeers(tt.ctx, nil)
				assert.Equal(t, tt.wantPeers, peers)
			} else {
				assert.ErrorContains(t, errCommand, tt.wantErrMsg)
			}
		})
	}
}
