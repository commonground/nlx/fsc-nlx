// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command_test

import (
	"context"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
)

type fakeAuditorLogger struct {
	records []*common_auditlog.Record
}

func (f *fakeAuditorLogger) CreateRecords(ctx context.Context, records common_auditlog.Records) error {
	for _, record := range records {
		f.records = append(f.records, &common_auditlog.Record{
			ID:            record.ID,
			GroupID:       record.GroupID,
			CorrelationID: record.CorrelationID,
			Actor:         record.Actor,
			Event:         record.Event,
			Source:        record.Source,
			Status:        record.Status,
			Component:     record.Component,
			CreatedAt:     record.CreatedAt,
		})
	}

	return nil
}

func (f *fakeAuditorLogger) ListRecords(ctx context.Context, args *common_auditlog.ListRecordsArgs) (common_auditlog.Records, error) {
	return f.records, nil
}
