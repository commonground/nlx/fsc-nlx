// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"
	"time"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

type CreateContractHandler struct {
	manager     manager.Manager
	auth        *auth.Auth
	clock       clock.Clock
	idGenerator idgenerator.IDGenerator
	lgr         *logger.Logger
	auditLgr    *auditlog.AuditLogger
}

type NewCreateContractHandlerArgs struct {
	Manager    manager.Manager
	Auth       *auth.Auth
	Clock      clock.Clock
	IVenerator idgenerator.IDGenerator
	Logger     *logger.Logger
	AuditLgr   *auditlog.AuditLogger
}

// nolint:dupl // looks the same but is not
func NewCreateContractHandler(args *NewCreateContractHandlerArgs) (*CreateContractHandler, error) {
	if args.Manager == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.Clock == nil {
		return nil, fmt.Errorf("clock is required")
	}

	if args.IVenerator == nil {
		return nil, fmt.Errorf("ivGenerator is required")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if args.AuditLgr == nil {
		return nil, fmt.Errorf("audit logger is required")
	}

	return &CreateContractHandler{
		manager:     args.Manager,
		auth:        args.Auth,
		clock:       args.Clock,
		idGenerator: args.IVenerator,
		lgr:         args.Logger,
		auditLgr:    args.AuditLgr,
	}, nil
}

type CreateContractArgs struct {
	AuthData                          authentication.Data
	AuthorizationMetadata             authorization.Metadata
	AuditLogSource                    common_auditlog.Source
	HashAlgorithm                     string
	GroupID                           string
	ContractNotBefore                 string
	ContractNotAfter                  string
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
}

type grantType interface {
	ServicePublicationGrant | ServiceConnectionGrant | DelegatedServiceConnectionGrant | DelegatedServicePublicationGrant
}

type grantPointer[T grantType] interface {
	*T
	Validator
}

type ServicePublicationGrant struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

func (g *ServicePublicationGrant) getServiceName() string {
	return g.ServiceName
}

func (g *ServicePublicationGrant) Valid() *ValidationError {
	var errors []string

	if g.DirectoryPeerID == "" {
		errors = append(errors, "directory peer id is missing")
	}

	errors = append(errors, validateGrants(g)...)

	return mapValidationError(errors)
}

type ServiceConnectionGrant struct {
	ServicePeerID                     string
	ServicePublicationDelegatorPeerID string
	ServiceName                       string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
}

func (g *ServiceConnectionGrant) getServiceName() string {
	return g.ServiceName
}

func (g *ServiceConnectionGrant) Valid() *ValidationError {
	var errors []string

	if g.ServicePeerID == "" {
		errors = append(errors, "service peer id is missing")
	}

	if g.OutwayPublicKeyThumbprint == "" {
		errors = append(errors, "outway public key thumbprint is missing")
	}

	errors = append(errors, validateGrants(g)...)

	return mapValidationError(errors)
}

type DelegatedServiceConnectionGrant struct {
	DelegatorPeerID                   string
	ServicePeerID                     string
	ServiceName                       string
	ServicePublicationDelegatorPeerID string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
}

func (g *DelegatedServiceConnectionGrant) getServiceName() string {
	return g.ServiceName
}

func (g *DelegatedServiceConnectionGrant) Valid() *ValidationError {
	return mapValidationError(validateGrants(g))
}

type DelegatedServicePublicationGrant struct {
	DelegatorPeerID string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

func (g *DelegatedServicePublicationGrant) getServiceName() string {
	return g.ServiceName
}

func (g *DelegatedServicePublicationGrant) Valid() *ValidationError {
	return mapValidationError(validateGrants(g))
}

type commonGrantValidation interface {
	getServiceName() string
}

func validateGrants(validation commonGrantValidation) []string {
	validationErrors := make([]string, 0)
	serviceName := validation.getServiceName()

	if serviceName == "" {
		validationErrors = append(validationErrors, "service name cannot be blank")
	}

	if !serviceNameValidator.MatchString(serviceName) {
		validationErrors = append(validationErrors, "service name must contain only letters, numbers, -, _ and .")
	}

	return validationErrors
}

const HashAlgSHA3_512 = "SHA3-512"

var mapHashAlgorithm = map[string]manager.HashAlg{
	HashAlgSHA3_512: manager.HashAlgSHA3_512,
}

func (h *CreateContractHandler) Handle(ctx context.Context, args *CreateContractArgs) error {
	handlerFunc := func(auditlogCorrelationID string) (res any, event common_auditlog.Event, err error) {
		err = h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscCreateContract, []*authz.ResourceURN{
			authz.NewResourceURN(authz.ResourceTypeContract, "*"),
		})
		if err != nil {
			return "", nil, mapError(err, "could not authenticate or authorize")
		}

		notBefore, err := time.Parse("2006-01-02", args.ContractNotBefore)
		if err != nil {
			return "", nil, newValidationError([]string{"the not before date should be formatted as 2006-01-02"})
		}

		notAfter, err := time.Parse("2006-01-02", args.ContractNotAfter)
		if err != nil {
			return "", nil, newValidationError([]string{"the not after date should be formatted as 2006-01-02"})
		}

		servicePublicationGrants, err := mapGrant(args.ServicePublicationGrants, func(grant *ServicePublicationGrant) *manager.ServicePublicationGrant {
			return &manager.ServicePublicationGrant{
				DirectoryPeerID: grant.DirectoryPeerID,
				ServicePeerID:   grant.ServicePeerID,
				ServiceName:     grant.ServiceName,
				ServiceProtocol: string(models.PROTOCOLTCPHTTP11),
			}
		})
		if err != nil {
			return "", nil, err
		}

		serviceConnectionGrants, err := mapGrant(args.ServiceConnectionGrants, func(grant *ServiceConnectionGrant) *manager.ServiceConnectionGrant {
			return &manager.ServiceConnectionGrant{
				ServicePeerID:                     grant.ServicePeerID,
				ServicePublicationDelegatorPeerID: grant.ServicePublicationDelegatorPeerID,
				ServiceName:                       grant.ServiceName,
				OutwayPeerID:                      grant.OutwayPeerID,
				OutwayPublicKeyThumbprint:         grant.OutwayPublicKeyThumbprint,
			}
		})
		if err != nil {
			return "", nil, err
		}

		delegatedServiceConnectionGrants, err := mapGrant(args.DelegatedServiceConnectionGrants, func(grant *DelegatedServiceConnectionGrant) *manager.DelegatedServiceConnectionGrant {
			return &manager.DelegatedServiceConnectionGrant{
				DelegatorPeerID:                   grant.DelegatorPeerID,
				ServicePeerID:                     grant.ServicePeerID,
				ServiceName:                       grant.ServiceName,
				ServicePublicationDelegatorPeerID: grant.ServicePublicationDelegatorPeerID,
				OutwayPeerID:                      grant.OutwayPeerID,
				OutwayPublicKeyThumbprint:         grant.OutwayPublicKeyThumbprint,
			}
		})
		if err != nil {
			return "", nil, err
		}

		delegatedServicePublicationGrants, err := mapGrant(args.DelegatedServicePublicationGrants, func(grant *DelegatedServicePublicationGrant) *manager.DelegatedServicePublicationGrant {
			return &manager.DelegatedServicePublicationGrant{
				DirectoryPeerID: grant.DirectoryPeerID,
				DelegatorPeerID: grant.DelegatorPeerID,
				ServicePeerID:   grant.ServicePeerID,
				ServiceName:     grant.ServiceName,
				ServiceProtocol: string(models.PROTOCOLTCPHTTP11),
			}
		})
		if err != nil {
			return "", nil, err
		}

		iv, err := h.idGenerator.New()
		if err != nil {
			return "", nil, mapError(err, "failed to generate idGenerator")
		}

		hashAlgorithm, ok := mapHashAlgorithm[args.HashAlgorithm]
		if !ok {
			return "", nil, newValidationError([]string{"invalid hash algorithm"})
		}

		createContractArgs := &manager.CreateContractArgs{
			AuditlogCorrelationID:             auditlogCorrelationID,
			IV:                                iv,
			HashAlgorithm:                     hashAlgorithm,
			GroupID:                           args.GroupID,
			ContractNotBefore:                 notBefore,
			ContractNotAfter:                  notAfter,
			ServicePublicationGrants:          servicePublicationGrants,
			ServiceConnectionGrants:           serviceConnectionGrants,
			DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
			DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
			CreatedAt:                         h.clock.Now(),
		}

		var contentHash string

		contentHash, err = h.manager.CreateContract(ctx, createContractArgs)
		if err != nil {
			h.lgr.Error("failed to create contract", err)
			return "", nil, mapError(err, "could not create contract in manager")
		}

		return contentHash, common_auditlog.EventCreateContract{ContractHash: contentHash}, nil
	}

	_, err := h.auditLgr.AddRecordWithResultAndEvent(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData: args.AuthData,
		Event:    common_auditlog.EventCreateContract{},
		Source:   args.AuditLogSource,
	})
	if err != nil {
		return mapError(err, "could not create AuditLog record")
	}

	return nil
}

// mapGrant validates the grants and transforms the grant to the corresponding manager grant
// grants is a slice of grantPointer
// grantPointer is introduced as a composing interface containing a pointer to the grantType and Validator interface.
// This enables the various grant structs constraint by the generic grantType to have pointer receiver methods implementing the Validator interface
func mapGrant[T grantType, G grantPointer[T], R manager.Grant](grants []G, mapGrant func(grants G) *R) ([]*R, error) {
	result := make([]*R, len(grants))

	validationErrors := make([]*ValidationError, 0)

	for i, g := range grants {
		if err := g.Valid(); err != nil {
			validationErrors = append(validationErrors, err)
		}

		result[i] = mapGrant(g)
	}

	validationError := squashValidationErrors(validationErrors)
	if validationError != nil {
		return nil, validationError
	}

	return result, nil
}
