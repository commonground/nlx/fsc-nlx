// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like revoke contract but is different
package command

import (
	"context"
	"fmt"
	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type AcceptContractHandler struct {
	manager     manager.Manager
	auth        *auth.Auth
	logger      *logger.Logger
	auditLogger *auditlog.AuditLogger
}

type NewAcceptContractHandlerArgs struct {
	Manager     manager.Manager
	Auth        *auth.Auth
	AuditLogger *auditlog.AuditLogger
	Logger      *logger.Logger
}

func NewAcceptContractHandler(args *NewAcceptContractHandlerArgs) (*AcceptContractHandler, error) {
	if args.Manager == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if args.AuditLogger == nil {
		return nil, fmt.Errorf("audit logger is required")
	}

	return &AcceptContractHandler{
		manager:     args.Manager,
		auth:        args.Auth,
		logger:      args.Logger,
		auditLogger: args.AuditLogger,
	}, nil
}

type AcceptContractArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	AuditLogSource        common_auditlog.Source
	ContentHash           string
}

func (h *AcceptContractHandler) Handle(ctx context.Context, args *AcceptContractArgs) error {
	handlerFunc := func(auditlogCorrelationID string) error {
		err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscAcceptContract, []*authz.ResourceURN{
			authz.NewResourceURN(authz.ResourceTypeContract, args.ContentHash),
		})
		if err != nil {
			return mapError(err, "could not authenticate or authorize")
		}

		if args.ContentHash == "" {
			return newValidationError([]string{"contentHash cannot be empty"})
		}

		err = h.manager.AcceptContract(ctx, args.ContentHash, auditlogCorrelationID)
		if err != nil {
			h.logger.Error("failed to accept contract", err)

			return mapError(err, "could not accept contract in manager")
		}

		return nil
	}

	err := h.auditLogger.AddRecord(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData: args.AuthData,
		Event:    common_auditlog.EventAcceptContract{ContractHash: args.ContentHash},
		Source:   args.AuditLogSource,
	})
	if err != nil {
		return mapError(err, "could not create AuditLog record")
	}

	return nil
}
