// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command_test

import (
	"context"
	"fmt"
	"net/netip"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	discard_logger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	mock_storage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestNewCreateServiceHandler_LoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewCreateServiceHandler(&command.NewCreateServiceHandlerArgs{
		Logger:      nil,
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
		GroupID:     "group-id",
		Storage:     mock_storage.NewMockStorage(t),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "logger is required")
}

func TestNewCreateServiceHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := command.NewCreateServiceHandler(&command.NewCreateServiceHandlerArgs{
		Logger:      discard_logger.New(),
		Auth:        nil,
		AuditLogger: &auditlog.AuditLogger{},
		GroupID:     "group-id",
		Storage:     mock_storage.NewMockStorage(t),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewCreateServiceHandler_StorageNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewCreateServiceHandler(&command.NewCreateServiceHandlerArgs{
		Logger:      discard_logger.New(),
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
		GroupID:     "group-id",
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "storage is required")
}

func TestNewCreateServiceHandler_AuditLoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewCreateServiceHandler(&command.NewCreateServiceHandlerArgs{
		Logger:      discard_logger.New(),
		Auth:        auth,
		AuditLogger: nil,
		GroupID:     "group-id",
		Storage:     mock_storage.NewMockStorage(t),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "audit logger is required")
}

func TestNewCreateServiceHandler_GroupIDNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewCreateServiceHandler(&command.NewCreateServiceHandlerArgs{
		Logger:      discard_logger.New(),
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
		Storage:     mock_storage.NewMockStorage(t),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "group id is required")
}

// nolint:funlen,dupl // these tests should not fit 100 lines and look similar to other test because the same setup is used
func TestCreateService(t *testing.T) {
	t.Parallel()

	groupID, err := contract.NewGroupID("groupID")
	assert.NoError(t, err)

	nowInUTC := time.Now()
	testClock := testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	testIP, _ := netip.ParseAddr("0.0.0.0")
	testUUID, _ := uuid.FromString("222dea14-cdb0-7701-99ba-d55ece9e3894")

	validArgs := &command.CreateServiceArgs{
		AuthData: &authentication.OIDCData{
			Email: "mail@example.com",
			Name:  "John Doe",
		},
		AuthorizationMetadata: &authorization.RestMetaData{},
		AuditLogSource: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
		Name:         "TestService",
		EndpointURL:  "https://petstore.io",
		InwayAddress: "https://my-inway.local:443",
	}

	testCases := map[string]struct {
		ctx                 context.Context
		setup               func(context.Context, *mock_storage.MockStorage)
		args                *command.CreateServiceArgs
		wantErr             error
		wantAuditLogRecords common_auditlog.Records
	}{
		"when_storage_errors": {
			ctx: context.Background(),
			setup: func(ctx context.Context, s *mock_storage.MockStorage) {
				s.EXPECT().
					CreateService(ctx, &storage.CreateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(fmt.Errorf("arbitrary"))
			},
			args:    validArgs,
			wantErr: &command.InternalError{},
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not create service in storage: arbitrary",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a command.CreateServiceArgs) *command.CreateServiceArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor:         &common_auditlog.ActorUnknown{},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor:         &common_auditlog.ActorUnknown{},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "could not authenticate or authorize: authentication error: authentication error: unauthenticated: authentication error: unauthenticated",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			args: validArgs,
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not authenticate or authorize: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.AuthorizationError{},
		},
		"when_name_in_use": {
			ctx: context.Background(),
			setup: func(ctx context.Context, s *mock_storage.MockStorage) {
				s.EXPECT().
					CreateService(ctx, &storage.CreateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(storage.ErrServiceNameInUse)
			},
			args:    validArgs,
			wantErr: &command.InternalError{},
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "service name is already in use",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"invalid_name": {
			ctx: context.Background(),
			args: func(a command.CreateServiceArgs) *command.CreateServiceArgs {
				a.Name = ""
				return &a
			}(*validArgs),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.ValidationError{},
		},
		"invalid_endpoint_url": {
			ctx: context.Background(),
			args: func(a command.CreateServiceArgs) *command.CreateServiceArgs {
				a.EndpointURL = ""
				return &a
			}(*validArgs),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: endpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.ValidationError{},
		},
		"invalid_inway_address": {
			ctx: context.Background(),
			args: func(a command.CreateServiceArgs) *command.CreateServiceArgs {
				a.InwayAddress = ""
				return &a
			}(*validArgs),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: inway address cannot be blank\ninway address must start with https:// and end with :443",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.ValidationError{},
		},
		"happy_flow": {
			ctx:  context.Background(),
			args: validArgs,
			setup: func(ctx context.Context, s *mock_storage.MockStorage) {
				s.EXPECT().
					CreateService(ctx, &storage.CreateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(nil)
			},
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventCreateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusSucceeded{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: nil,
		},
	}

	//nolint:dupl // looks the same but is not
	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			// The storage is still mocked. The actual storage is tested in the component test.
			s := mock_storage.NewMockStorage(t)

			fakeAuditlogger := &fakeAuditorLogger{}

			auditLogger, errAuditLog := auditlog.New(&auditlog.NewArgs{AuditLogger: fakeAuditlogger, GroupID: "fsc-test", Clock: testClock, IDGenerator: &fakeIVGenerator{}})
			require.NoError(t, errAuditLog)

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, errCommand := command.NewCreateServiceHandler(&command.NewCreateServiceHandlerArgs{
				GroupID:     groupID,
				Logger:      discard_logger.New(),
				Storage:     s,
				Auth:        auth,
				AuditLogger: auditLogger,
			})
			require.NoError(t, errCommand)

			if tc.setup != nil {
				tc.setup(tc.ctx, s)
			}

			errCommand = h.Handle(tc.ctx, tc.args)

			logs, _ := fakeAuditlogger.ListRecords(tc.ctx, &common_auditlog.ListRecordsArgs{})
			assert.Equal(t, 2, len(logs))

			assert.Equal(t, tc.wantAuditLogRecords, logs)

			if tc.wantErr == nil {
				assert.NoError(t, errCommand)
			} else {
				assert.ErrorAs(t, errCommand, &tc.wantErr)
			}
		})
	}
}
