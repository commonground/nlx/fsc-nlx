// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type SyncPeersHandler struct {
	lgr      *logger.Logger
	manager  manager.Manager
	auth     *auth.Auth
	auditLgr *auditlog.AuditLogger
}

type NewSyncPeersHandlerArgs struct {
	Logger      *logger.Logger
	Manager     manager.Manager
	Auth        *auth.Auth
	AuditLogger *auditlog.AuditLogger
}

type SyncPeersArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	AuditLogSource        common_auditlog.Source
}

type SyncPeerResult struct {
	Ok  bool
	Err error
}

func NewSyncPeersHandler(args *NewSyncPeersHandlerArgs) (*SyncPeersHandler, error) {
	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if args.Manager == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.AuditLogger == nil {
		return nil, fmt.Errorf("audit logger is required")
	}

	return &SyncPeersHandler{
		lgr:      args.Logger,
		manager:  args.Manager,
		auth:     args.Auth,
		auditLgr: args.AuditLogger,
	}, nil
}

func (h *SyncPeersHandler) Handle(ctx context.Context, args *SyncPeersArgs) (map[string]*SyncPeerResult, error) {
	result := make(map[string]*SyncPeerResult, 0)

	handlerFunc := func(auditlogCorrelationID string) error {
		err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscSyncPeers, []*authz.ResourceURN{})
		if err != nil {
			return mapError(err, "could not authenticate or authorize")
		}

		syncRest, err := h.manager.SyncPeers(ctx)
		if err != nil {
			h.lgr.Error("failed to synchronize peers", err)
			return mapError(err, "could synchronize in manager")
		}

		for peerID, res := range syncRest {
			result[peerID] = &SyncPeerResult{
				Ok:  res.Ok,
				Err: res.Err,
			}
		}

		return nil
	}

	err := h.auditLgr.AddRecord(ctx, handlerFunc, &auditlog.AddRecordArgs{
		AuthData: args.AuthData,
		Event:    common_auditlog.EventSynchronizePeers{},
		Source:   args.AuditLogSource,
	})
	if err != nil {
		return nil, mapError(err, "could not create AuditLog record")
	}

	return result, nil
}
