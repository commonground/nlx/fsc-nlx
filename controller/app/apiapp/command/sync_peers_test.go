// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks similar to other test because the same setup is used
package command_test

import (
	"context"
	"net/netip"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	discard_logger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestNewSyncPeersHandler_LoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewSyncPeersHandler(&command.NewSyncPeersHandlerArgs{
		Logger:      nil,
		Manager:     &fakeManager{},
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "logger is required")
}

func TestNewSyncPeersHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := command.NewSyncPeersHandler(&command.NewSyncPeersHandlerArgs{
		Logger:      discard_logger.New(),
		Manager:     &fakeManager{},
		Auth:        nil,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewSyncPeersHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewSyncPeersHandler(&command.NewSyncPeersHandlerArgs{
		Logger:      discard_logger.New(),
		Manager:     nil,
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestNewSyncPeersHandler_AuditLoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewSyncPeersHandler(&command.NewSyncPeersHandlerArgs{
		Logger:      discard_logger.New(),
		Manager:     &fakeManager{},
		Auth:        auth,
		AuditLogger: nil,
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "audit logger is required")
}

// nolint:funlen // these tests should not fit 100 lines and use the same pattern as other tests
func TestSyncPeers(t *testing.T) {
	t.Parallel()

	testUUID, err := uuid.FromString("222dea14-cdb0-7701-99ba-d55ece9e3894")
	require.NoError(t, err)

	nowInUTC := time.Now()
	testClock := testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	testIP, err := netip.ParseAddr("0.0.0.0")
	require.NoError(t, err)

	validArgs := &command.SyncPeersArgs{
		AuthData: &authentication.OIDCData{
			Email: "mail@example.com",
			Name:  "John Doe",
		},
		AuthorizationMetadata: &authorization.RestMetaData{},
		AuditLogSource: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
	}

	testCases := map[string]struct {
		ctx                 context.Context
		args                *command.SyncPeersArgs
		wantErrMsg          string
		wantAuditLogRecords common_auditlog.Records
		wantSyncResults     map[string]*command.SyncPeerResult
	}{
		"happy_flow": {
			args: validArgs,
			ctx:  context.Background(),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventSynchronizePeers{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventSynchronizePeers{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusSucceeded{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantSyncResults: map[string]*command.SyncPeerResult{"12345678901234567890": {
				Ok: true,
			}},
		},
		"manager_fails": {
			args: func(a command.SyncPeersArgs) *command.SyncPeersArgs {
				return &a
			}(*validArgs),
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			wantErrMsg: "internal error: could synchronize in manager: arbitrary error",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventSynchronizePeers{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventSynchronizePeers{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could synchronize in manager: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"authorization_fails": {
			args: func(a command.SyncPeersArgs) *command.SyncPeersArgs {
				return &a
			}(*validArgs),
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			wantErrMsg: "could not authenticate or authorize: arbitrary error",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventSynchronizePeers{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventSynchronizePeers{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not authenticate or authorize: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
	}

	for name, tt := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			fakeAuditLogger := &fakeAuditorLogger{}

			auditLogger, errAuditLog := auditlog.New(&auditlog.NewArgs{AuditLogger: fakeAuditLogger, GroupID: "fsc-test", Clock: testClock, IDGenerator: &fakeIVGenerator{}})
			require.NoError(t, errAuditLog)

			fManager := newFakeManager()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, errCommand := command.NewSyncPeersHandler(&command.NewSyncPeersHandlerArgs{
				Logger:      discard_logger.New(),
				Manager:     fManager,
				Auth:        auth,
				AuditLogger: auditLogger,
			})
			require.NoError(t, errCommand)

			res, errCommand := h.Handle(tt.ctx, tt.args)

			logs, _ := fakeAuditLogger.ListRecords(tt.ctx, &common_auditlog.ListRecordsArgs{})
			assert.Equal(t, 2, len(logs))

			assert.Equal(t, tt.wantAuditLogRecords, logs)

			if tt.wantErrMsg == "" {
				assert.NoError(t, errCommand)
				assert.Equal(t, tt.wantSyncResults, res)
			} else {
				assert.ErrorContains(t, errCommand, tt.wantErrMsg)
			}
		})
	}
}
