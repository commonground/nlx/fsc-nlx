// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks like accept contract but is different
package command_test

import (
	"context"
	"net/netip"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	discard_logger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestNewRetryContractHandler_LoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewRetryContractHandler(&command.NewRetryContractHandlerParams{
		Logger:      nil,
		Manager:     &fakeManager{},
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "logger is required")
}

func TestNewRetryContractHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := command.NewRetryContractHandler(&command.NewRetryContractHandlerParams{
		Logger:      discard_logger.New(),
		Manager:     &fakeManager{},
		Auth:        nil,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewRetryContractHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewRetryContractHandler(&command.NewRetryContractHandlerParams{
		Logger:      discard_logger.New(),
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestNewRetryContractHandler_AuditLoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewRetryContractHandler(&command.NewRetryContractHandlerParams{
		Logger:  discard_logger.New(),
		Manager: &fakeManager{},
		Auth:    auth,
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "audit logger is required")
}

//nolint:funlen // this is a test
func TestRetryContract(t *testing.T) {
	t.Parallel()

	testUUID, err := uuid.FromString("222dea14-cdb0-7701-99ba-d55ece9e3894")
	require.NoError(t, err)

	nowInUTC := time.Now()
	testClock := testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	testIP, err := netip.ParseAddr("0.0.0.0")
	require.NoError(t, err)

	validArgs := &command.RetryContractHandlerArgs{
		AuthData: &authentication.OIDCData{
			Email: "mail@example.com",
			Name:  "John Doe",
		},
		AuthorizationMetadata: &authorization.RestMetaData{},
		AuditLogSource: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
		PeerID:      "12345678901234567890",
		ContentHash: "$1$1$mTfBbWw2A6UQU5oU7RR2JbEtTB9GnT2syfopGwiCT5Mo9qDgFb1L_o0kFi0-TG13LX2czdAL60KaY7NbB-uMAQ==",
		Action:      command.SubmitContract,
	}

	testCases := map[string]struct {
		ctx                 context.Context
		args                *command.RetryContractHandlerArgs
		wantErrMsg          string
		wantAuditLogRecords common_auditlog.Records
	}{
		"happy_flow_submit_contract": {
			args: validArgs,
			ctx:  context.Background(),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusSucceeded{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"happy_flow_submit_accept_signature": {
			args: func(a command.RetryContractHandlerArgs) *command.RetryContractHandlerArgs {
				a.Action = command.SubmitAcceptSignature
				return &a
			}(*validArgs),
			ctx: context.Background(),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitAcceptSignature,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitAcceptSignature,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusSucceeded{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"happy_flow_submit_reject_signature": {
			args: func(a command.RetryContractHandlerArgs) *command.RetryContractHandlerArgs {
				a.Action = command.SubmitRejectSignature
				return &a
			}(*validArgs),
			ctx: context.Background(),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitRejectSignature,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitRejectSignature,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusSucceeded{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"happy_flow_submit_revoke_signature": {
			args: func(a command.RetryContractHandlerArgs) *command.RetryContractHandlerArgs {
				a.Action = command.SubmitRevokeSignature
				return &a
			}(*validArgs),
			ctx: context.Background(),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitRevokeSignature,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitRevokeSignature,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusSucceeded{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"invalid_content_hash": {
			args: func(a command.RetryContractHandlerArgs) *command.RetryContractHandlerArgs {
				a.ContentHash = ""
				return &a
			}(*validArgs),
			ctx:        context.Background(),
			wantErrMsg: "validation error: contentHash cannot be empty",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID: validArgs.PeerID,
						Action: common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID: validArgs.PeerID,
						Action: common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: contentHash cannot be empty",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"invalid_peer_id": {
			args: func(a command.RetryContractHandlerArgs) *command.RetryContractHandlerArgs {
				a.PeerID = ""
				return &a
			}(*validArgs),
			ctx:        context.Background(),
			wantErrMsg: "validation error: peer ID cannot be empty",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: peer ID cannot be empty",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"invalid_action": {
			args: func(a command.RetryContractHandlerArgs) *command.RetryContractHandlerArgs {
				a.Action = -1
				return &a
			}(*validArgs),
			ctx:        context.Background(),
			wantErrMsg: "validation error: invalid action provided",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: invalid action provided",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"manager_fails": {
			args: func(a command.RetryContractHandlerArgs) *command.RetryContractHandlerArgs {
				return &a
			}(*validArgs),
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			wantErrMsg: "could not retry contract in manager: arbitrary error",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not retry contract in manager: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"authorization_fails": {
			args: func(a command.RetryContractHandlerArgs) *command.RetryContractHandlerArgs {
				return &a
			}(*validArgs),
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			wantErrMsg: "could not authenticate or authorize: arbitrary error",
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventFailedDistributionRetry{
						PeerID:      validArgs.PeerID,
						ContentHash: validArgs.ContentHash,
						Action:      common_auditlog.FailedDistributionRetryActionSubmitContract,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not authenticate or authorize: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
	}

	for name, tt := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			fakeAuditlogger := &fakeAuditorLogger{}

			auditLogger, errAuditLog := auditlog.New(&auditlog.NewArgs{AuditLogger: fakeAuditlogger, GroupID: "fsc-test", Clock: testClock, IDGenerator: &fakeIVGenerator{}})
			require.NoError(t, errAuditLog)

			fManager := newFakeManager()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, errCommand := command.NewRetryContractHandler(&command.NewRetryContractHandlerParams{
				Logger:      discard_logger.New(),
				Manager:     fManager,
				Auth:        auth,
				AuditLogger: auditLogger,
			})
			require.NoError(t, errCommand)

			errCommand = h.Handle(tt.ctx, tt.args)

			logs, _ := fakeAuditlogger.ListRecords(tt.ctx, &common_auditlog.ListRecordsArgs{})
			assert.Equal(t, 2, len(logs))

			assert.Equal(t, tt.wantAuditLogRecords, logs)

			if tt.wantErrMsg == "" {
				assert.NoError(t, errCommand)
			} else {
				assert.ErrorContains(t, errCommand, tt.wantErrMsg)
			}
		})
	}
}
