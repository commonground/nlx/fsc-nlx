// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
//nolint:dupl // this is a test
package command_test

import (
	"context"
	"fmt"
	"net/netip"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	discard_logger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	mock_storage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestNewUpdateServiceHandler_LoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewUpdateServiceHandler(&command.NewUpdateServiceHandlerArgs{
		Logger:      nil,
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
		GroupID:     "group-id",
		Storage:     mock_storage.NewMockStorage(t),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "logger is required")
}

func TestNewUpdateServiceHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := command.NewUpdateServiceHandler(&command.NewUpdateServiceHandlerArgs{
		Logger:      discard_logger.New(),
		Auth:        nil,
		AuditLogger: &auditlog.AuditLogger{},
		GroupID:     "group-id",
		Storage:     mock_storage.NewMockStorage(t),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewUpdateServiceHandler_StorageNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewUpdateServiceHandler(&command.NewUpdateServiceHandlerArgs{
		Logger:      discard_logger.New(),
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
		GroupID:     "group-id",
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "storage is required")
}

func TestNewUpdateServiceHandler_AuditLoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewUpdateServiceHandler(&command.NewUpdateServiceHandlerArgs{
		Logger:      discard_logger.New(),
		Auth:        auth,
		AuditLogger: nil,
		GroupID:     "group-id",
		Storage:     mock_storage.NewMockStorage(t),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "audit logger is required")
}

func TestNewUpdateServiceHandler_GroupIDNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewUpdateServiceHandler(&command.NewUpdateServiceHandlerArgs{
		Logger:      discard_logger.New(),
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
		Storage:     mock_storage.NewMockStorage(t),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "group id is required")
}

// nolint:funlen // these tests should not fit 100 lines
func TestUpdateService(t *testing.T) {
	t.Parallel()

	groupID, err := contract.NewGroupID("groupID")
	assert.NoError(t, err)

	nowInUTC := time.Now()
	testClock := testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	testIP, _ := netip.ParseAddr("0.0.0.0")
	testUUID, _ := uuid.FromString("222dea14-cdb0-7701-99ba-d55ece9e3894")

	validArgs := &command.UpdateServiceArgs{
		AuthData: &authentication.OIDCData{
			Email: "mail@example.com",
			Name:  "John Doe",
		},
		AuthorizationMetadata: &authorization.RestMetaData{},
		AuditLogSource: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
		Name:         "TestService",
		EndpointURL:  "https://petstore.io",
		InwayAddress: "my-inway.local:443",
	}

	testCases := map[string]struct {
		ctx                 context.Context
		setup               func(context.Context, *mock_storage.MockStorage)
		args                *command.UpdateServiceArgs
		wantErr             error
		wantAuditLogRecords common_auditlog.Records
	}{
		"when_storage_errors": {
			ctx: context.Background(),
			setup: func(ctx context.Context, m *mock_storage.MockStorage) {
				m.EXPECT().
					UpdateService(ctx, &storage.UpdateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(fmt.Errorf("arbitrary"))
			},
			args: validArgs,
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: "TestService",
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: "TestService",
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not update service in storage: arbitrary",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.InternalError{},
		},
		"when_service_not_found": {
			ctx: context.Background(),
			setup: func(ctx context.Context, m *mock_storage.MockStorage) {
				m.EXPECT().
					UpdateService(ctx, &storage.UpdateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(storage.ErrServiceNotFound)
			},
			args: validArgs,
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: "TestService",
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: "TestService",
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "service could not be found",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.InternalError{},
		},
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a command.UpdateServiceArgs) *command.UpdateServiceArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor:         &common_auditlog.ActorUnknown{},
					Event: common_auditlog.EventUpdateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor:         &common_auditlog.ActorUnknown{},
					Event: common_auditlog.EventUpdateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "could not authenticate or authorize: authentication error: authentication error: unauthenticated: authentication error: unauthenticated",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			args: validArgs,
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not authenticate or authorize: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.AuthorizationError{},
		},
		"invalid_name": {
			ctx: context.Background(),
			args: func(a command.UpdateServiceArgs) *command.UpdateServiceArgs {
				a.Name = ""
				return &a
			}(*validArgs),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.ValidationError{},
		},
		"invalid_endpoint_url": {
			ctx: context.Background(),
			args: func(a command.UpdateServiceArgs) *command.UpdateServiceArgs {
				a.EndpointURL = ""
				return &a
			}(*validArgs),
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: endpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: &command.ValidationError{},
		},
		"happy_flow": {
			ctx:  context.Background(),
			args: validArgs,
			setup: func(ctx context.Context, m *mock_storage.MockStorage) {
				m.EXPECT().
					UpdateService(ctx, &storage.UpdateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(nil)
			},
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventUpdateService{
						ServiceName: validArgs.Name,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusSucceeded{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: nil,
		},
	}

	// nolint:dupl // not the same
	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			// The storage is still mocked. The actual storage is tested in the component test.
			s := mock_storage.NewMockStorage(t)

			fakeAuditlogger := &fakeAuditorLogger{}

			auditLogger, errAuditLog := auditlog.New(&auditlog.NewArgs{AuditLogger: fakeAuditlogger, GroupID: "fsc-test", Clock: testClock, IDGenerator: &fakeIVGenerator{}})
			require.NoError(t, errAuditLog)

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, errCommand := command.NewUpdateServiceHandler(&command.NewUpdateServiceHandlerArgs{
				GroupID:     groupID,
				Auth:        auth,
				AuditLogger: auditLogger,
				Logger:      discard_logger.New(),
				Storage:     s,
			})
			require.NoError(t, errCommand)

			if tc.setup != nil {
				tc.setup(tc.ctx, s)
			}

			errCommand = h.Handle(tc.ctx, tc.args)

			logs, _ := fakeAuditlogger.ListRecords(tc.ctx, &common_auditlog.ListRecordsArgs{})
			assert.Equal(t, 2, len(logs))

			assert.Equal(t, tc.wantAuditLogRecords, logs)

			if tc.wantErr == nil {
				assert.NoError(t, errCommand)
			} else {
				assert.ErrorAs(t, errCommand, &tc.wantErr)
			}
		})
	}
}

func TestUpdateServiceArgsValidation(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		args       *command.UpdateServiceArgs
		wantErr    error
		wantErrMsg string
	}

	testCases := tests{
		"invalid_name_spaces": {
			args: &command.UpdateServiceArgs{
				Name:        "test service",
				EndpointURL: "https://example.com",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name must contain only letters, numbers, -, _ and .",
		},
		"invalid_name_blank": {
			args: &command.UpdateServiceArgs{
				Name:        "",
				EndpointURL: "https://example.com",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .",
		},
		"invalid_url_blank": {
			args: &command.UpdateServiceArgs{
				Name:        "testservice",
				EndpointURL: "",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: endpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash",
		},
		"invalid_url_trailing_slash": {
			args: &command.UpdateServiceArgs{
				Name:         "testservice",
				EndpointURL:  "https://service/",
				InwayAddress: "https://inway:443",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: endpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash",
		},
		"invalid_name_and_url": {
			args: &command.UpdateServiceArgs{
				Name:        "",
				EndpointURL: "",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .\nendpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash",
		},
	}

	for testCase, test := range testCases {
		tt := test

		t.Run(testCase, func(t *testing.T) {
			t.Parallel()

			err := tt.args.Valid()
			assert.ErrorAs(t, err, &tt.wantErr)
			assert.Equal(t, tt.wantErrMsg, err.Error())
		})
	}
}
