// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like accept contract but is different
package command_test

import (
	"context"
	"net/netip"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	discard_logger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestNewRejectContractHandler_LoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		Logger:      nil,
		Manager:     &fakeManager{},
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "logger is required")
}

func TestNewRejectContractHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		Logger:      discard_logger.New(),
		Manager:     &fakeManager{},
		Auth:        nil,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewRejectContractHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		Logger:      discard_logger.New(),
		Manager:     nil,
		Auth:        auth,
		AuditLogger: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestNewRejectContractHandler_AuditLoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		Logger:      discard_logger.New(),
		Manager:     &fakeManager{},
		Auth:        auth,
		AuditLogger: nil,
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "audit logger is required")
}

func TestRejectContract(t *testing.T) {
	t.Parallel()

	nowInUTC := time.Now()
	testClock := testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	testIP, _ := netip.ParseAddr("0.0.0.0")
	testUUID, _ := uuid.FromString("222dea14-cdb0-7701-99ba-d55ece9e3894")

	validArgs := &command.RejectContractArgs{
		AuthData: &authentication.OIDCData{
			Email: "mail@example.com",
			Name:  "John Doe",
		},
		AuthorizationMetadata: &authorization.RestMetaData{},
		AuditLogSource: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
		ContentHash: "$1$1$mTfBbWw2A6UQU5oU7RR2JbEtTB9GnT2syfopGwICT5Mo9qDgFb1L_o0kFi0-TG13LX2czdAL60KaY7NbB-uMAQ==",
	}

	testCases := map[string]struct {
		ctx                 context.Context
		args                *command.RejectContractArgs
		wantErr             error
		wantAuditLogRecords common_auditlog.Records
	}{
		"when_manager_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &command.InternalError{},
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventRejectContract{
						ContractHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventRejectContract{
						ContractHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not reject contract in manager: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a command.RejectContractArgs) *command.RejectContractArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &command.AuthenticationError{},
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor:         &common_auditlog.ActorUnknown{},
					Event: common_auditlog.EventRejectContract{
						ContractHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor:         &common_auditlog.ActorUnknown{},
					Event: common_auditlog.EventRejectContract{
						ContractHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "could not authenticate or authorize: authentication error: authentication error: unauthenticated: authentication error: unauthenticated",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &command.AuthorizationError{},
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventRejectContract{
						ContractHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventRejectContract{
						ContractHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "internal error: could not authenticate or authorize: arbitrary error",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"empty_content_hash": {
			ctx: context.Background(),
			args: func(a command.RejectContractArgs) *command.RejectContractArgs {
				a.ContentHash = ""
				return &a
			}(*validArgs),
			wantErr: &command.ValidationError{},
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventRejectContract{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventRejectContract{},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status: common_auditlog.StatusFailed{
						Error: "validation error: contentHash cannot be empty",
					},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
		"happy_flow": {
			ctx:     context.Background(),
			args:    validArgs,
			wantErr: nil,
			wantAuditLogRecords: common_auditlog.Records{
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventRejectContract{
						ContractHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusInitiated{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
				{
					ID:            common_auditlog.ID(testUUID),
					GroupID:       "fsc-test",
					CorrelationID: common_auditlog.CorrelationID(testUUID),
					Actor: &common_auditlog.ActorUser{
						Email: "mail@example.com",
						Name:  "John Doe",
					},
					Event: common_auditlog.EventRejectContract{
						ContractHash: validArgs.ContentHash,
					},
					Source: common_auditlog.SourceHTTP{
						IPAddress: testIP,
						UserAgent: "test",
					},
					Status:    common_auditlog.StatusSucceeded{},
					Component: "controller",
					CreatedAt: testClock.Now(),
				},
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			fakeAuditlogger := &fakeAuditorLogger{}

			auditLogger, errAuditLog := auditlog.New(&auditlog.NewArgs{AuditLogger: fakeAuditlogger, GroupID: "fsc-test", Clock: testClock, IDGenerator: &fakeIVGenerator{}})
			require.NoError(t, errAuditLog)

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
				Manager:     newFakeManager(),
				Auth:        auth,
				AuditLogger: auditLogger,
				Logger:      discard_logger.New(),
			})
			require.NoError(t, err)

			err = h.Handle(tc.ctx, tc.args)

			logs, _ := fakeAuditlogger.ListRecords(tc.ctx, &common_auditlog.ListRecordsArgs{})
			assert.Equal(t, 2, len(logs))

			assert.Equal(t, tc.wantAuditLogRecords, logs)

			if tc.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tc.wantErr)
			}
		})
	}
}
