// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command_test

import (
	"net/netip"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	mock_auditlogger "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog/mock"
	mock_clock "gitlab.com/commonground/nlx/fsc-nlx/common/clock/mock"
	mock_idgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	discard_logger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	mock_authorization "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/mock"
	mock_manager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/mock"
	mock_storage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/mock"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

type mocks struct {
	manager     *mock_manager.MockManager
	storage     *mock_storage.MockStorage
	auth        *auth_helper.Auth
	authz       *mock_authorization.MockAuthorization
	clock       *mock_clock.MockClock
	iv          *mock_idgenerator.MockIDGenerator
	logger      *logger.Logger
	auditLogger *mock_auditlogger.MockAuditLogger
}

func newMocks(t *testing.T) *mocks {
	authz := mock_authorization.NewMockAuthorization(t)

	auth, err := auth_helper.New(authz)
	assert.NoError(t, err)

	return &mocks{
		manager:     mock_manager.NewMockManager(t),
		storage:     mock_storage.NewMockStorage(t),
		authz:       authz,
		auth:        auth,
		clock:       mock_clock.NewMockClock(t),
		iv:          mock_idgenerator.NewMockIDGenerator(t),
		logger:      discard_logger.New(),
		auditLogger: mock_auditlogger.NewMockAuditLogger(t),
	}
}

func createAuditLogInitiatedRecord(testUUID uuid.UUID, testUser common_auditlog.Actor, event common_auditlog.Event, testIP netip.Addr, testClock *testingutils.MockClock) common_auditlog.Records {
	return common_auditlog.Records{&common_auditlog.Record{
		GroupID:       "groupID",
		ID:            common_auditlog.ID(testUUID),
		CorrelationID: common_auditlog.CorrelationID(testUUID),
		Event:         event,
		Source: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
		Actor:     testUser,
		Status:    common_auditlog.StatusInitiated{},
		Component: common_auditlog.ComponentController,
		CreatedAt: testClock.Now(),
	},
	}
}

func createAuditLogFailedRecord(testUUID uuid.UUID, testUser common_auditlog.Actor, event common_auditlog.Event, testIP netip.Addr, err string, testClock *testingutils.MockClock) common_auditlog.Records {
	return common_auditlog.Records{&common_auditlog.Record{
		GroupID:       "groupID",
		ID:            common_auditlog.ID(testUUID),
		CorrelationID: common_auditlog.CorrelationID(testUUID),
		Event:         event,
		Source: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
		Actor: testUser,
		Status: common_auditlog.StatusFailed{
			Error: err,
		},
		Component: common_auditlog.ComponentController,
		CreatedAt: testClock.Now(),
	},
	}
}

func createAuditLogSucceededRecord(testUUID uuid.UUID, testUser common_auditlog.Actor, event common_auditlog.Event, testIP netip.Addr, testClock *testingutils.MockClock) common_auditlog.Records {
	return common_auditlog.Records{&common_auditlog.Record{
		GroupID:       "groupID",
		ID:            common_auditlog.ID(testUUID),
		CorrelationID: common_auditlog.CorrelationID(testUUID),
		Event:         event,
		Source: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
		Actor:     testUser,
		Status:    common_auditlog.StatusSucceeded{},
		Component: common_auditlog.ComponentController,
		CreatedAt: testClock.Now(),
	},
	}
}
