// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package command_test

import (
	"context"
	"net/netip"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	discard_logger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestNewCreateContractHandler_LoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		Logger:     nil,
		Manager:    &fakeManager{},
		Auth:       auth,
		AuditLgr:   &auditlog.AuditLogger{},
		IVenerator: &fakeIVGenerator{},
		Clock:      clock.New(),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "logger is required")
}

func TestNewCreateContractHandler_CloclNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		Logger:     discard_logger.New(),
		Manager:    &fakeManager{},
		Auth:       auth,
		AuditLgr:   &auditlog.AuditLogger{},
		IVenerator: &fakeIVGenerator{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "clock is required")
}

func TestNewCreateContractHandler_AuthNil(t *testing.T) {
	t.Parallel()

	handler, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		Logger:     discard_logger.New(),
		Manager:    &fakeManager{},
		Auth:       nil,
		AuditLgr:   &auditlog.AuditLogger{},
		IVenerator: &fakeIVGenerator{},
		Clock:      clock.New(),
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "auth is required")
}

func TestNewCreateContractHandler_ManagerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		Logger:     discard_logger.New(),
		Manager:    nil,
		Auth:       auth,
		AuditLgr:   &auditlog.AuditLogger{},
		Clock:      clock.New(),
		IVenerator: &fakeIVGenerator{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "manager is required")
}

func TestNewCreateContractHandler_AuditLoggerNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		Logger:     discard_logger.New(),
		Manager:    &fakeManager{},
		Auth:       auth,
		Clock:      clock.New(),
		IVenerator: &fakeIVGenerator{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "audit logger is required")
}

func TestNewCreateContractHandler_IVGeneratorNil(t *testing.T) {
	t.Parallel()

	auth, err := auth_helper.New(&fakeAuthorization{})
	require.NoError(t, err)

	handler, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		Logger:   discard_logger.New(),
		Manager:  &fakeManager{},
		Auth:     auth,
		Clock:    clock.New(),
		AuditLgr: &auditlog.AuditLogger{},
	})

	assert.Nil(t, handler)
	assert.EqualError(t, err, "ivGenerator is required")
}

// nolint:funlen // these tests should not fit 100 lines
func TestCreateContract(t *testing.T) {
	t.Parallel()

	nowInUTC := time.Now()
	testClock := testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	testIP, _ := netip.ParseAddr("0.0.0.0")

	validArgs := &command.CreateContractArgs{
		AuthData: &authentication.OIDCData{
			Email: "mail@example.com",
			Name:  "John Doe",
		},
		AuthorizationMetadata: &authorization.RestMetaData{},
		AuditLogSource: common_auditlog.SourceHTTP{
			IPAddress: testIP,
			UserAgent: "test",
		},
		HashAlgorithm:     command.HashAlgSHA3_512,
		GroupID:           "group-id",
		ContractNotBefore: "2023-05-23",
		ContractNotAfter:  "2023-05-24",
		ServicePublicationGrants: []*command.ServicePublicationGrant{
			{
				DirectoryPeerID: "12345678901234567899",
				ServicePeerID:   "12345678901234567890",
				ServiceName:     "petstore",
			},
		},
		ServiceConnectionGrants: []*command.ServiceConnectionGrant{
			{
				ServicePeerID:             "12345678901234567891",
				ServiceName:               "petstore",
				OutwayPeerID:              "12345678901234567890",
				OutwayPublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
			},
		},
		DelegatedServiceConnectionGrants: []*command.DelegatedServiceConnectionGrant{
			{
				DelegatorPeerID:           "12345678901234567892",
				ServicePeerID:             "12345678901234567891",
				ServiceName:               "petstore",
				OutwayPeerID:              "12345678901234567890",
				OutwayPublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
			},
		},
		DelegatedServicePublicationGrants: []*command.DelegatedServicePublicationGrant{
			{
				DirectoryPeerID: "12345678901234567899",
				DelegatorPeerID: "12345678901234567891",
				ServicePeerID:   "12345678901234567890",
				ServiceName:     "petstore",
			},
		},
	}

	testCases := map[string]struct {
		ctx     context.Context
		args    *command.CreateContractArgs
		wantErr error
	}{
		"when_unauthenticated": {
			ctx: context.Background(),
			args: func(a command.CreateContractArgs) *command.CreateContractArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &command.AuthenticationError{},
		},
		"when_unauthorized": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyAuthorizationError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &command.AuthorizationError{},
		},
		"when_manager_errors": {
			ctx: func() context.Context {
				ctx := context.Background()
				ctx = context.WithValue(ctx, ctxKeyManagerError, true)
				return ctx
			}(),
			args:    validArgs,
			wantErr: &command.InternalError{},
		},
		"happy_flow": {
			ctx:  context.Background(),
			args: validArgs,
		},
		"invalid_not_before_format": {
			ctx: context.Background(),
			args: &command.CreateContractArgs{
				AuthData:                 validArgs.AuthData,
				AuthorizationMetadata:    validArgs.AuthorizationMetadata,
				AuditLogSource:           validArgs.AuditLogSource,
				HashAlgorithm:            validArgs.HashAlgorithm,
				GroupID:                  validArgs.GroupID,
				ContractNotBefore:        "2023-05-23-invalid-format",
				ContractNotAfter:         "2023-05-24",
				ServicePublicationGrants: nil,
				ServiceConnectionGrants:  nil,
			},
			wantErr: &command.ValidationError{},
		},
		"invalid_not_after_format": {
			ctx: context.Background(),
			args: &command.CreateContractArgs{
				AuthData:                 validArgs.AuthData,
				AuthorizationMetadata:    validArgs.AuthorizationMetadata,
				AuditLogSource:           validArgs.AuditLogSource,
				HashAlgorithm:            validArgs.HashAlgorithm,
				GroupID:                  validArgs.GroupID,
				ContractNotBefore:        "2023-05-23",
				ContractNotAfter:         "2023-05-24-invalid-format",
				ServicePublicationGrants: nil,
				ServiceConnectionGrants:  nil,
			},
			wantErr: &command.ValidationError{},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			fakeAuditlogger := &fakeAuditorLogger{}

			auditLogger, errAuditLog := auditlog.New(&auditlog.NewArgs{AuditLogger: fakeAuditlogger, GroupID: "fsc-test", Clock: testClock, IDGenerator: &fakeIVGenerator{}})
			require.NoError(t, errAuditLog)

			fManager := newFakeManager()

			auth, err := auth_helper.New(&fakeAuthorization{})
			require.NoError(t, err)

			h, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
				Manager:    fManager,
				Auth:       auth,
				Clock:      testClock,
				IVenerator: &fakeIVGenerator{},
				Logger:     discard_logger.New(),
				AuditLgr:   auditLogger,
			})
			require.NoError(t, err)

			err = h.Handle(tc.ctx, tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tc.wantErr)
			}
		})
	}
}

func TestCreateContract_ServiceConnectionGrant_Validation(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		args       *command.ServiceConnectionGrant
		wantErr    error
		wantErrMsg string
	}

	testCases := tests{
		"invalid_name_spaces": {
			args: &command.ServiceConnectionGrant{
				ServiceName:               "test service",
				OutwayPublicKeyThumbprint: "thumbprint",
				ServicePeerID:             "12345678900987654321",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name must contain only letters, numbers, -, _ and .",
		},
		"invalid_name_blank": {
			args: &command.ServiceConnectionGrant{
				ServiceName:               "",
				OutwayPublicKeyThumbprint: "thumbprint",
				ServicePeerID:             "12345678900987654321",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .",
		},
		"service_peer_id_blank": {
			args: &command.ServiceConnectionGrant{
				ServiceName:               "test",
				OutwayPublicKeyThumbprint: "thumbprint",
				ServicePeerID:             "",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service peer id is missing",
		},
		"outway_public_key_thumbprint_blank": {
			args: &command.ServiceConnectionGrant{
				ServiceName:               "test",
				OutwayPublicKeyThumbprint: "",
				ServicePeerID:             "12345678900987654321",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: outway public key thumbprint is missing",
		},
	}

	for testCase, tt := range testCases {
		t.Run(testCase, func(t *testing.T) {
			t.Parallel()

			err := tt.args.Valid()
			assert.ErrorAs(t, err, &tt.wantErr)
			assert.Equal(t, tt.wantErrMsg, err.Error())
		})
	}
}
