// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command_test

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
)

type fakeManager struct {
	Peers map[string]*manager.Peer
}

func newFakeManager() *fakeManager {
	return &fakeManager{
		Peers: make(map[string]*manager.Peer, 0),
	}
}

func (f *fakeManager) ListContracts(ctx context.Context, args *manager.ListContractsArgs) (manager.Contracts, string, error) {
	return manager.Contracts{}, "", errors.New("not implemented")
}

func (f *fakeManager) ListContractsPending(ctx context.Context, args *manager.ListContractsPendingArgs) (*manager.PendingContracts, string, error) {
	return nil, "", errors.New("not implemented")
}

func (f *fakeManager) CreateContract(ctx context.Context, args *manager.CreateContractArgs) (string, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return "", errors.New("arbitrary error")
	}

	return "content-hash", nil
}

func (f *fakeManager) AcceptContract(ctx context.Context, contentHash, auditlogCorrelationID string) error {
	if ctx.Value(ctxKeyManagerError) != nil {
		return errors.New("arbitrary error")
	}

	return nil
}

func (f *fakeManager) RevokeContract(ctx context.Context, contentHash, auditlogCorrelationID string) error {
	if ctx.Value(ctxKeyManagerError) != nil {
		return errors.New("arbitrary error")
	}

	return nil
}

func (f *fakeManager) RejectContract(ctx context.Context, contentHash, auditlogCorrelationID string) error {
	if ctx.Value(ctxKeyManagerError) != nil {
		return errors.New("arbitrary error")
	}

	return nil
}

func (f *fakeManager) RetryContract(ctx context.Context, contentHash, peerID string, action manager.Action, auditlogCorrelationID string) error {
	if ctx.Value(ctxKeyManagerError) != nil {
		return errors.New("arbitrary error")
	}

	return nil
}
func (f *fakeManager) ListContractDistribution(ctx context.Context, contentHash string) (manager.ContractDistributions, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) ListOutgoingConnections(ctx context.Context, args *manager.ListOutgoingConnectionsArgs) (*manager.OutgoingConnections, string, error) {
	return nil, "", errors.New("not implemented")
}

func (f *fakeManager) ListOutgoingConnectionsForService(ctx context.Context, args *manager.ListOutgoingConnectionsForServiceArgs) ([]*manager.Connection, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) ListOutgoingConnectionsForDelegatedService(ctx context.Context, args *manager.ListOutgoingConnectionsForDelegatedServiceArgs) ([]*manager.Connection, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) ListIncomingConnections(ctx context.Context, args *manager.ListIncomingConnectionsArgs) (*manager.IncomingConnections, string, error) {
	return nil, "", errors.New("not implemented")
}

func (f *fakeManager) ListDelegatedConnections(ctx context.Context, args *manager.ListDelegatedConnectionsArgs) (*manager.DelegatedConnections, string, error) {
	return nil, "", errors.New("not implemented")
}

func (f *fakeManager) SyncPeers(ctx context.Context) (map[string]manager.SynchronizationResult, error) {
	if ctx.Value(ctxKeyManagerError) != nil {
		return nil, errors.New("arbitrary error")
	}

	return map[string]manager.SynchronizationResult{"12345678901234567890": {
		Ok: true,
	}}, nil
}

func (f *fakeManager) UpdatePeer(ctx context.Context, args *manager.UpdatePeerArgs) error {
	if ctx.Value(ctxKeyManagerError) != nil {
		return errors.New("arbitrary error")
	}

	f.Peers[args.PeerID] = &manager.Peer{
		ID:             args.PeerID,
		Name:           args.PeerName,
		ManagerAddress: args.ManagerAddress,
	}

	return nil
}

func (f *fakeManager) ListServices(ctx context.Context, args *manager.ListServicesArgs) (manager.Services, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) ListServicePublications(ctx context.Context, args *manager.ListServicePublicationsArgs) (*manager.Publications, string, error) {
	return nil, "", errors.New("not implemented")
}

func (f *fakeManager) ListDelegatedServicePublications(ctx context.Context, args *manager.ListDelegatedServicePublicationsArgs) (*manager.Publications, string, error) {
	return nil, "", errors.New("not implemented")
}

func (f *fakeManager) ListTXLogRecords(ctx context.Context, args *manager.ListTXLogRecordsRequest) ([]*manager.TXLogRecord, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) GetPeerInfo(ctx context.Context) (*manager.Peer, error) {
	return nil, errors.New("not implemented")
}

func (f *fakeManager) ListPeers(ctx context.Context, peerIDs *[]string) (manager.Peers, error) {
	peers := make(manager.Peers, 0)

	for _, peer := range f.Peers {
		peers = append(peers, peer)
	}

	return peers, nil
}

func (f *fakeManager) CreatePeer(ctx context.Context, args *manager.CreatePeerArgs) error {
	if ctx.Value(ctxKeyManagerError) != nil {
		return errors.New("arbitrary error")
	}

	f.Peers[args.PeerID] = &manager.Peer{
		ID:             args.PeerID,
		Name:           args.PeerName,
		ManagerAddress: args.ManagerAddress,
	}

	return nil
}

type ctxKey string

var ctxKeyManagerError ctxKey = "manager_error"
