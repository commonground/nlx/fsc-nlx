// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package apiapp

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type Application struct {
	Commands Commands
	Queries  Queries
}

type Commands struct {
	AcceptContract *command.AcceptContractHandler
	RevokeContract *command.RevokeContractHandler
	RejectContract *command.RejectContractHandler
	CreateContract *command.CreateContractHandler
	CreateService  *command.CreateServiceHandler
	DeleteService  *command.DeleteServiceHandler
	UpdateService  *command.UpdateServiceHandler
	RetryContract  *command.RetryContractHandler
	CreatePeer     *command.CreatePeerHandler
	UpdatePeer     *command.UpdatePeerHandler
	SyncPeers      *command.SyncPeersHandler
}

type Queries struct {
	GetContract                       *query.GetContractHandler
	GetService                        *query.GetServiceHandler
	GetHealth                         *query.GetHealthHandler
	ListServices                      *query.ListServicesHandler
	ListServicePublications           *query.ListServicePublicationsHandler
	ListDelegatedServicePublications  *query.ListDelegatedServicePublicationsHandler
	ListContracts                     *query.ListContractsHandler
	ListContractsPending              *query.ListPendingContractsHandler
	ListContractDistributions         *query.ListContractDistributionsHandler
	ListDelegatedConnections          *query.ListDelegatedConnectionsHandler
	ListOutgoingConnections           *query.ListOutgoingConnectionsHandler
	ListOutgoingConnectionsForService *query.ListOutgoingConnectionsForServiceHandler
	ListIncomingConnections           *query.ListIncomingConnectionsHandler
	ListPeerServices                  *query.ListPeerServicesHandler
	ListTXLogRecords                  *query.ListTXLogRecordsHandler
	ListAuditlogRecords               *query.ListAuditlogRecordsHandler
	ListInways                        *query.ListInwaysHandler
	ListOutways                       *query.ListOutwaysHandler
	GetPeerInfo                       *query.GetPeerInfoHandler
	ListPeers                         *query.ListPeersHandler
}

type NewApplicationArgs struct {
	Manager     manager.Manager
	Storage     storage.Storage
	Clock       clock.Clock
	IVGenerator idgenerator.IDGenerator
	Logger      *logger.Logger
	AuditLogger *auditlog.AuditLogger
	GroupID     contract.GroupID
	Auth        *auth.Auth
}

//nolint:gocyclo // new function
func NewApplication(ctx context.Context, args *NewApplicationArgs) (*Application, error) {
	getService, err := query.NewGetServiceHandler(&query.NewGetServiceHandlerArgs{
		Storage: args.Storage,
		Auth:    args.Auth,
		GroupID: &args.GroupID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetServiceConnectionInfo handler")
	}

	listServices, err := query.NewListServicesHandler(&query.NewListServicesHandlerArgs{
		Storage: args.Storage,
		Manager: args.Manager,
		Auth:    args.Auth,
		GroupID: &args.GroupID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListServices handler")
	}

	listServicePublications, err := query.NewListServicePublicationsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListServicePublications handler")
	}

	listDelegatedServicePublications, err := query.NewListDelegatedServicePublicationsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListDelegatedServicePublications handler")
	}

	listContracts, err := query.NewListContractsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListContracts handler")
	}

	listContractsPending, err := query.NewListPendingContractsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListContractsPending handler")
	}

	listDelegatedConnections, err := query.NewListDelegatedConnectionsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListDelegatedConnections handler")
	}

	listOutgoingConnections, err := query.NewListOutgoingConnectionsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListOutgoingConnections handler")
	}

	listOutgoingConnectionsForService, err := query.NewListOutgoingConnectionsForServiceHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new listOutgoingConnectionsForService handler")
	}

	listIncomingConnections, err := query.NewListIncomingConnectionsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new listIncomingConnections handler")
	}

	listContractDistributions, err := query.NewListContractDistributionHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListContractDistributins handler")
	}

	getContract, err := query.NewGetContractHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetContract handler")
	}

	getHealth, err := query.NewGetHealthHandler(args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetHealth handler")
	}

	createContract, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		Manager:    args.Manager,
		Auth:       args.Auth,
		Clock:      args.Clock,
		IVenerator: args.IVGenerator,
		Logger:     args.Logger,
		AuditLgr:   args.AuditLogger,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new CreateContract handler")
	}

	acceptContract, err := command.NewAcceptContractHandler(&command.NewAcceptContractHandlerArgs{
		Manager:     args.Manager,
		Auth:        args.Auth,
		Logger:      args.Logger,
		AuditLogger: args.AuditLogger,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new AcceptContract handler")
	}

	revokeContract, err := command.NewRevokeContractHandler(&command.NewRevokeContractHandlerArgs{
		Manager:     args.Manager,
		Auth:        args.Auth,
		Logger:      args.Logger,
		AuditLogger: args.AuditLogger,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RevokeContract handler")
	}

	rejectContract, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		Manager:     args.Manager,
		Auth:        args.Auth,
		Logger:      args.Logger,
		AuditLogger: args.AuditLogger,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RejectContract handler")
	}

	retryContract, err := command.NewRetryContractHandler(&command.NewRetryContractHandlerParams{
		AuditLogger: args.AuditLogger,
		Manager:     args.Manager,
		Logger:      args.Logger,
		Auth:        args.Auth,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RetryContract handler")
	}

	createService, err := command.NewCreateServiceHandler(&command.NewCreateServiceHandlerArgs{
		GroupID:     args.GroupID,
		Logger:      args.Logger,
		Storage:     args.Storage,
		Auth:        args.Auth,
		AuditLogger: args.AuditLogger,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new CreateService handler")
	}

	deleteService, err := command.NewDeleteServiceHandler(&command.NewDeleteServiceHandlerArgs{
		GroupID:     args.GroupID,
		Logger:      args.Logger,
		Storage:     args.Storage,
		Auth:        args.Auth,
		AuditLogger: args.AuditLogger,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new DeleteService handler")
	}

	updateService, err := command.NewUpdateServiceHandler(&command.NewUpdateServiceHandlerArgs{
		GroupID:     args.GroupID,
		Auth:        args.Auth,
		AuditLogger: args.AuditLogger,
		Logger:      args.Logger,
		Storage:     args.Storage,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new UpdateService handler")
	}

	createPeer, err := command.NewCreatePeerHandler(&command.NewCreatePeerHandlerArgs{
		Logger:      args.Logger,
		Manager:     args.Manager,
		Auth:        args.Auth,
		AuditLogger: args.AuditLogger,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new CreatePeer handler")
	}

	updatePeer, err := command.NewUpdatePeerHandler(&command.NewUpdatePeerHandlerArgs{
		Manager:     args.Manager,
		Auth:        args.Auth,
		AuditLogger: args.AuditLogger,
		Logger:      args.Logger,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new UpdatePeer handler")
	}

	syncPeers, err := command.NewSyncPeersHandler(&command.NewSyncPeersHandlerArgs{
		Logger:      args.Logger,
		Manager:     args.Manager,
		Auth:        args.Auth,
		AuditLogger: args.AuditLogger,
	})

	if err != nil {
		return nil, errors.Wrap(err, "could not create new SyncPeers handler")
	}

	listPeerServices, err := query.NewListPeerServicesHandler(args.Manager, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListPeerServices handler")
	}

	listTXLogRecords, err := query.NewListTransactionLogRecordsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListTXLogRecords handler")
	}

	listAuditlogRecords, err := query.NewListAuditlogRecordsHandler(args.AuditLogger, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListAuditlogRecords handler")
	}

	getPeerInfo, err := query.NewGetPeerInfoHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetPeerInfo handler")
	}

	listPeers, err := query.NewListPeersHandler(args.Manager, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListPeers handler")
	}

	listInways, err := query.NewListInwaysHandler(&query.NewListInwaysHandlerArgs{
		Storage: args.Storage,
		Auth:    args.Auth,
		GroupID: &args.GroupID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListInways handler")
	}

	listOutways, err := query.NewListOutwaysHandler(&query.NewListOutwaysHandlerArgs{
		Storage: args.Storage,
		Auth:    args.Auth,
		GroupID: &args.GroupID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListOutways handler")
	}

	application := &Application{
		Commands: Commands{
			AcceptContract: acceptContract,
			RevokeContract: revokeContract,
			RejectContract: rejectContract,
			RetryContract:  retryContract,
			CreateContract: createContract,
			CreateService:  createService,
			DeleteService:  deleteService,
			UpdateService:  updateService,
			CreatePeer:     createPeer,
			UpdatePeer:     updatePeer,
			SyncPeers:      syncPeers,
		},
		Queries: Queries{
			GetContract:                       getContract,
			GetService:                        getService,
			GetHealth:                         getHealth,
			ListServices:                      listServices,
			ListServicePublications:           listServicePublications,
			ListDelegatedServicePublications:  listDelegatedServicePublications,
			ListContracts:                     listContracts,
			ListContractsPending:              listContractsPending,
			ListContractDistributions:         listContractDistributions,
			ListDelegatedConnections:          listDelegatedConnections,
			ListOutgoingConnections:           listOutgoingConnections,
			ListOutgoingConnectionsForService: listOutgoingConnectionsForService,
			ListIncomingConnections:           listIncomingConnections,
			ListPeerServices:                  listPeerServices,
			ListTXLogRecords:                  listTXLogRecords,
			ListAuditlogRecords:               listAuditlogRecords,
			ListInways:                        listInways,
			GetPeerInfo:                       getPeerInfo,
			ListPeers:                         listPeers,
			ListOutways:                       listOutways,
		},
	}

	return application, nil
}
