// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/lib/pq"
	"github.com/spf13/cobra"

	postgresstorage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication"
	oidcmigrations "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication/oidc/pgsessionstore/migrations"
)

var migrateOpts struct {
	PostgresDSN        string
	AuthenticationType string
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	migrateUpCommand.Flags().StringVarP(&migrateOpts.PostgresDSN, "postgres-dsn", "", "", "Postgres Connection URL")
	migrateUpCommand.Flags().StringVarP(&migrateOpts.AuthenticationType, "authn-type", "", "none", fmt.Sprintf("Type of the authentication adapter, valid values: %s", strings.Join(authentication.ValidStringAuthTypes(), ", ")))

	if err := migrateUpCommand.MarkFlagRequired("postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	migrateCommand.AddCommand(migrateUpCommand)
	migrateCommand.AddCommand(migrateStatusCommand)
}

var migrateCommand = &cobra.Command{
	Use:   "migrate",
	Short: "Run the migration tool",
	Run:   func(cmd *cobra.Command, args []string) {},
}

var migrateUpCommand = &cobra.Command{
	Use:   "up",
	Short: "Up the migrations",
	Run: func(cmd *cobra.Command, args []string) {
		err := postgresstorage.PerformMigrations(migrateOpts.PostgresDSN)
		if err != nil {
			log.Fatal(err)
		}

		if authentication.AuthType(migrateOpts.AuthenticationType) == authentication.AuthTypeOIDC {
			err = oidcmigrations.PerformMigrations(migrateOpts.PostgresDSN)
			if err != nil {
				log.Fatal(err)
			}
		}

		os.Exit(0)
	},
}

var migrateStatusCommand = &cobra.Command{
	Use:   "status",
	Short: "Show migration status",
	Run: func(cmd *cobra.Command, args []string) {
		version, dirty, err := postgresstorage.MigrationStatus(migrateOpts.PostgresDSN)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("dirty=%v version=%d\n", dirty, version)
	},
}
