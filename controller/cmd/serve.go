// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package cmd

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	oidc_server "github.com/coreos/go-oidc/v3/oidc"
	"github.com/spf13/cobra"

	api_auditlog "gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/server"
	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/cmd"
	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logoptions"
	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	"gitlab.com/commonground/nlx/fsc-nlx/common/process"
	"gitlab.com/commonground/nlx/fsc-nlx/common/slogerrorwriter"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/version"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rbac"
	authz_rest "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest"
	authz_rest_client "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest/api/client"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	postgresstorage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/internalapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/health"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest"
	uiport "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication/none"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication/oidc"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/i18n"
	registration_rest "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

type oidcOptions oidc.Options

var serveOpts struct {
	ListenAddressUI       string
	ListenAddressInternal string
	ListenAddressAPI      string

	MonitoringAddress      string
	GroupID                string
	ManagerAddressInternal string
	Environment            string
	StaticPath             string

	StoragePostgresDSN string

	AuthnType string
	oidcOptions

	AuthzType        string
	AuthzRestAddress string

	AuditlogType        string
	AuditlogRESTAddress string

	CsrfProtectionEnabled bool
	CsrfAuthKey           string
	CsrfUnsafeHTTP        bool

	logoptions.LogOptions
	cmd.TLSOptions

	DirectoryPeerID string
}

//nolint:gochecknoinits,funlen,gocyclo // this is the recommended way to use cobra, also a lot of flags..
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressUI, "listen-address-ui", "", "127.0.0.1:3001", "Address for the UI to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressInternal, "listen-address-internal", "", "127.0.0.1:443", "Address for the internal api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressAPI, "listen-address-api", "", "127.0.0.1:444", "Address for the api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.MonitoringAddress, "monitoring-address", "", "127.0.0.1:8081", "Address for the monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")

	serveCommand.Flags().StringVarP(&serveOpts.GroupID, "group-id", "", "", "Group id of the FSC Group")
	serveCommand.Flags().StringVarP(&serveOpts.ManagerAddressInternal, "manager-address-internal", "", "", "URL to the Manager internal API")

	serveCommand.Flags().StringVarP(&serveOpts.StoragePostgresDSN, "storage-postgres-dsn", "", "", "Postgres Connection URL")

	serveCommand.Flags().StringVarP(&serveOpts.StaticPath, "static-path", "", "public", "Path to the static web files")

	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "info", "Set loglevel")

	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")

	serveCommand.Flags().StringVarP(&serveOpts.AuthnType, "authn-type", "", "none", fmt.Sprintf("Type of the authentication adapter, valid values: %s", strings.Join(authentication.ValidStringAuthTypes(), ", ")))
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.SecretKey, "authn-oidc-secret-key", "", "", "Secret key that is used for signing sessions")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.ClientID, "authn-oidc-client-id", "", "", "The OIDC client id")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.ClientSecret, "authn-oidc-client-secret", "", "", "The OIDC client secret")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.DiscoveryURL, "authn-oidc-discovery-url", "", "", "The OIDC discovery URL")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.RedirectURL, "authn-oidc-redirect-url", "", "", "The OIDC redirect URL")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.LogoutURL, "authn-oidc-logout-url", "", "", "The OIDC logout URL")
	serveCommand.Flags().BoolVarP(&serveOpts.oidcOptions.SessionCookieSecure, "authn-oidc-session-cookie-secure", "", false, "Use 'secure' cookies")

	serveCommand.Flags().StringVarP(&serveOpts.AuthzType, "authz-type", "", "rbac", fmt.Sprintf("Type of the authorization adapter, valid values: %s", strings.Join(authorization.ValidStringTypes(), ", ")))
	serveCommand.Flags().StringVarP(&serveOpts.AuthzRestAddress, "authz-rest-address", "", "", "Address for the authorization endpoint")

	serveCommand.Flags().BoolVarP(&serveOpts.CsrfProtectionEnabled, "csrf-protection-enabled", "", false, "enable CSRF protection, if set to true the flag csrf-auth-key is required.")
	serveCommand.Flags().StringVarP(&serveOpts.CsrfAuthKey, "csrf-auth-key", "", "", "32-bytes long authkey used to provide CSRF protection")
	serveCommand.Flags().BoolVarP(&serveOpts.CsrfUnsafeHTTP, "csrf-unsafe-http", "", false, "The csrf protection uses unsafe cookies(can be set by a webserver that runs on http instead of https) when this flag is set to true. This flag is not recommended for production environments.")

	serveCommand.Flags().StringVarP(&serveOpts.AuditlogType, "auditlog-type", "", "stdout", "Type of the authentication adapter, valid values: rest, stdout")
	serveCommand.Flags().StringVarP(&serveOpts.AuditlogRESTAddress, "auditlog-rest-address", "", "", "Address for the Auditlog API endpoint")
	serveCommand.Flags().StringVarP(&serveOpts.DirectoryPeerID, "directory-peer-id", "", "", "ID of directory peer")

	if err := serveCommand.MarkFlagRequired("group-id"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("manager-address-internal"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("storage-postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-root-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-key"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the UI",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		logger, err := zaplogger.New(serveOpts.LogOptions.LogLevel, serveOpts.LogOptions.LogType)
		if err != nil {
			log.Fatalf("failed to create logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version info: version: %s source-hash: %s", version.BuildVersion, version.BuildSourceHash))

		if serveOpts.CsrfProtectionEnabled && serveOpts.CsrfAuthKey == "" {
			logger.Fatal("csrf-auth-key is required when csrf-protection-enabled is set to true", fmt.Errorf("csrf-auth-key not set"))
		}

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn("invalid key permissions", err)
		}

		cert, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("loading internal TLS files", err)
		}

		livenessProbeAdministrationAPI := health.APIProbe(cert, serveOpts.ListenAddressAPI)
		livenessProbeRegistrationAPI := health.APIProbe(cert, serveOpts.ListenAddressInternal)
		livenessProbeManagerAPI := health.ManagerAPIProbe(cert, serveOpts.ManagerAddressInternal)
		livenessProbeAdministrationUI := health.UIProbe(serveOpts.ListenAddressUI)

		readinessProbes := map[string]monitoring.Check{
			"Readiness Probe Controller Registration API":   livenessProbeRegistrationAPI,
			"Readiness Probe Controller Administration API": livenessProbeAdministrationAPI,
			"Readiness Probe Controller Administration UI":  livenessProbeAdministrationUI,
			"Readiness Probe Controller Manager API":        livenessProbeManagerAPI,
		}

		livenessProbes := map[string]monitoring.Check{
			"Readiness Probe Controller Registration API":   livenessProbeRegistrationAPI,
			"Readiness Probe Controller Administration API": livenessProbeAdministrationAPI,
			"Readiness Probe Controller Administration UI":  livenessProbeAdministrationUI,
			"Readiness Probe Controller Manager API":        livenessProbeManagerAPI,
		}

		// Controller monitoring service uses the same probes for liveness and readiness
		monitoringService, err := monitoring.NewMonitoringService(serveOpts.MonitoringAddress, logger, readinessProbes, livenessProbes)
		if err != nil {
			logger.Fatal("unable to create monitoring service", err)
		}

		go func() {
			if err = monitoringService.Start(); err != nil {
				if !errors.Is(err, http.ErrServerClosed) {
					logger.Fatal("error listening on monitoring service", err)
				}

				logger.Fatal("cannot start monitoringService", err)
			}
		}()

		ctx := context.Background()

		managerInternalClient, err := api.NewClientWithResponses(serveOpts.ManagerAddressInternal, func(c *api.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = cert.TLSConfig()

			c.Client = &http.Client{Transport: t}
			return nil
		})
		if err != nil {
			logger.Fatal("could not create manager internal client", err)
		}

		mgr, err := restmanager.New(managerInternalClient, logger)
		if err != nil {
			logger.Fatal("could not create rest manager", err)
		}

		db, err := postgresstorage.NewConnection(ctx, serveOpts.StoragePostgresDSN)
		if err != nil {
			logger.Fatal("could not create db connection", err)
		}

		storage, err := postgresstorage.New(db)
		if err != nil {
			logger.Fatal("could not create postgres storage", err)
		}

		idGenerator := uuidgenerator.New()

		groupID, err := contract.NewGroupID(serveOpts.GroupID)
		if err != nil {
			logger.Fatal(fmt.Sprintf("invalid group id provided '%s'", serveOpts.GroupID), err)
		}

		authzType, err := authorization.TypeFromString(serveOpts.AuthzType)
		if err != nil {
			logger.Fatal("unknown authorization type", err)
		}

		var auditAdapter common_auditlog.AuditLogger

		switch serveOpts.AuditlogType {
		case "stdout":
			auditAdapter, err = common_auditlog.NewStdoutAuditlogger()
			if err != nil {
				logger.Fatal("could not create stdout Audit logger", err)
			}
		case "rest":
			var auditLogClient *api_auditlog.ClientWithResponses

			auditLogClient, err = api_auditlog.NewClientWithResponses(serveOpts.AuditlogRESTAddress, func(c *api_auditlog.Client) error {
				t := http.DefaultTransport.(*http.Transport).Clone()
				t.TLSClientConfig = cert.TLSConfig()

				c.Client = &http.Client{Transport: t}

				return nil
			})
			if err != nil {
				logger.Fatal("could not create AuditLog client", err)
			}

			auditAdapter, err = common_auditlog.NewRestAPIAuditLogger(auditLogClient)
			if err != nil {
				logger.Fatal("could not create REST Audit logger", err)
			}
		default:
			logger.Fatal(fmt.Sprintf("unknown auditlog type: %q", serveOpts.AuditlogType), nil)
		}

		auditLogger, err := auditlog.New(&auditlog.NewArgs{
			AuditLogger: auditAdapter,
			GroupID:     string(groupID),
			Clock:       clock.New(),
			IDGenerator: idGenerator,
		})
		if err != nil {
			logger.Fatal("cannot create Audit log client", err)
		}

		var authz authz.Authorization

		switch authzType {
		case authorization.TypeRBAC:
			authz = rbac.New()
		case authorization.TypeRest:
			var address *url.URL
			address, err = url.Parse(serveOpts.AuthzRestAddress)
			if err != nil {
				logger.Fatal("invalid authz rest address", err)
			}

			if address.Scheme != "https" {
				logger.Fatal("invalid authz rest address", fmt.Errorf("must be an https address"))
			}

			var authzClient *authz_rest_client.ClientWithResponses
			authzClient, err = authz_rest_client.NewClientWithResponses(serveOpts.AuthzRestAddress)
			if err != nil {
				logger.Fatal("could not create rest authorization client", err)
			}

			authz, err = authz_rest.New(authzClient)
			if err != nil {
				logger.Fatal("failed to create rest authorization adapter", err)
			}
		}

		auth, err := auth.New(authz)
		if err != nil {
			logger.Fatal("could not create auth", err)
		}

		app, err := apiapp.NewApplication(ctx, &apiapp.NewApplicationArgs{
			Manager:     mgr,
			Storage:     storage,
			Auth:        auth,
			Clock:       clock.New(),
			IVGenerator: idGenerator,
			Logger:      logger,
			GroupID:     groupID,
			AuditLogger: auditLogger,
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		workDir, err := os.Getwd()
		if err != nil {
			logger.Fatal("failed to get work dir", err)
		}

		staticFilesPath := filepath.Join(workDir, serveOpts.StaticPath)

		authType, err := authentication.AuthTypeFromString(serveOpts.AuthnType)
		if err != nil {
			logger.Fatal("unknown authentication type", err)
		}

		var authn authentication.Authenticator

		switch authType {
		case authentication.AuthTypeNone:
			authn = none.New()
		case authentication.AuthTypeOIDC:
			var oidcProvider *oidc_server.Provider

			oidcProvider, err = oidc_server.NewProvider(context.Background(), serveOpts.oidcOptions.DiscoveryURL)
			if err != nil {
				logger.Fatal("could not initialize OIDC provider", err)
			}

			oidcConfig := &oidc_server.Config{
				ClientID: serveOpts.oidcOptions.ClientID,
			}

			verifier := oidcProvider.Verifier(oidcConfig)

			authn, err = oidc.New(db, logger, oidcProvider, verifier, func(idToken *oidc_server.IDToken) (*oidc.IDTokenClaims, error) {
				claims := &oidc.IDTokenClaims{}
				err = idToken.Claims(claims)
				if err != nil {
					return nil, err
				}

				return claims, nil
			}, (*oidc.Options)(&serveOpts.oidcOptions))
			if err != nil {
				logger.Fatal("failed to create oidc authenticator", err)
			}
		}

		assetsMap, err := getAssetsMap(staticFilesPath, "parcel-manifest.json")
		if err != nil {
			logger.Fatal("failed to load assets map", err)
		}

		uiServer, err := uiport.New(ctx, &uiport.NewServerArgs{
			AssetsMap:             assetsMap,
			Locale:                i18n.LocaleNlNL,
			StaticPath:            staticFilesPath,
			GroupID:               serveOpts.GroupID,
			ServeAddress:          serveOpts.ListenAddressUI,
			Logger:                logger,
			APIApp:                app,
			Authenticator:         authn,
			CsrfAuthKey:           serveOpts.CsrfAuthKey,
			CsrfProtectionEnabled: serveOpts.CsrfProtectionEnabled,
			CsrfUnsafeHTTP:        serveOpts.CsrfUnsafeHTTP,
			DirectoryPeerID:       serveOpts.DirectoryPeerID,
		})
		if err != nil {
			logger.Fatal("failed to create ui server", err)
		}

		go func() {
			err = uiServer.ListenAndServe()
			if err != nil && !errors.Is(err, http.ErrServerClosed) {
				logger.Fatal("could not listen and serve", err)
			}
		}()

		internalApp, err := internalapp.NewApplication(&internalapp.NewApplicationArgs{
			Context: ctx,
			Storage: storage,
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		internalRestServer, err := registration_rest.New(&registration_rest.NewArgs{
			Logger: logger,
			App:    internalApp,
		})
		if err != nil {
			logger.Fatal("could not create internal rest server", err)
		}

		var readHeaderTimeout = 5 * time.Second

		internalSrv := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           internalRestServer.Handler(),
			Addr:              serveOpts.ListenAddressInternal,
			TLSConfig:         cert.TLSConfig(cert.WithTLSClientAuth()),
			ErrorLog:          log.New(slogerrorwriter.New(logger.Logger), "", 0),
		}

		go func() {
			logger.Info(fmt.Sprintf("starting internal rest server. listen address: %q", serveOpts.ListenAddressInternal))

			err = internalSrv.ListenAndServeTLS(serveOpts.CertFile, serveOpts.KeyFile)
			if err != nil && !errors.Is(err, http.ErrServerClosed) {
				logger.Fatal("could not listen and serve internal rest server", err)
			}
		}()

		restAPI, err := rest.New(&rest.NewArgs{
			Logger: logger,
			App:    app,
			Cert:   cert,
		})
		if err != nil {
			logger.Fatal("could not create rest api", err)
		}

		srv := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restAPI.Handler(),
			Addr:              serveOpts.ListenAddressAPI,
			TLSConfig:         cert.TLSConfig(cert.WithTLSClientAuth()),
			ErrorLog:          log.New(slogerrorwriter.New(logger.Logger), "", 0),
		}

		go func() {
			err = srv.ListenAndServeTLS(serveOpts.TLSOptions.CertFile, serveOpts.TLSOptions.KeyFile)
			if err != nil && !errors.Is(err, http.ErrServerClosed) {
				logger.Fatal("could not listen and serve rest api server", err)
			}
		}()

		p.Wait()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		err = internalSrv.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown internal rest server", err)
		}

		err = srv.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown rest api server", err)
		}

		err = uiServer.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown server", err)
		}

		db.Close()

		if err := monitoringService.Stop(); err != nil {
			logger.Error("could not shutdown monitoringService", err)
		}
	},
}

func getAssetsMap(staticPath, manifestFileName string) (uiport.AssetMap, error) {
	result := uiport.AssetMap{}

	content, err := os.ReadFile(path.Join(staticPath, manifestFileName))
	if err != nil {
		return result, errors.Join(err, fmt.Errorf("failed to read manifest file '%s'", manifestFileName))
	}

	var raw map[string]interface{}

	err = json.Unmarshal(content, &raw)
	if err != nil {
		return result, errors.Join(err, fmt.Errorf("error while unmarshalling translation file '%s'", manifestFileName))
	}

	for key, value := range raw {
		result[key] = fmt.Sprintf("%s", value)
	}

	return result, nil
}
