// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package manager

import (
	"context"
	"errors"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type Manager interface {
	// Contracts
	ListContracts(ctx context.Context, args *ListContractsArgs) (Contracts, string, error)
	ListContractsPending(ctx context.Context, args *ListContractsPendingArgs) (*PendingContracts, string, error)
	CreateContract(ctx context.Context, args *CreateContractArgs) (string, error)
	AcceptContract(ctx context.Context, contentHash string, auditlogCorrelationID string) error
	RevokeContract(ctx context.Context, contentHash string, auditlogCorrelationID string) error
	RejectContract(ctx context.Context, contentHash string, auditlogCorrelationID string) error
	RetryContract(ctx context.Context, contentHash string, peerID string, action Action, auditlogCorrelationID string) error
	ListContractDistribution(ctx context.Context, contentHash string) (ContractDistributions, error)
	ListOutgoingConnections(ctx context.Context, args *ListOutgoingConnectionsArgs) (*OutgoingConnections, string, error)
	ListOutgoingConnectionsForService(ctx context.Context, args *ListOutgoingConnectionsForServiceArgs) ([]*Connection, error)
	ListOutgoingConnectionsForDelegatedService(ctx context.Context, args *ListOutgoingConnectionsForDelegatedServiceArgs) ([]*Connection, error)
	ListIncomingConnections(ctx context.Context, args *ListIncomingConnectionsArgs) (*IncomingConnections, string, error)
	ListDelegatedConnections(ctx context.Context, args *ListDelegatedConnectionsArgs) (*DelegatedConnections, string, error)

	// Peers
	CreatePeer(ctx context.Context, args *CreatePeerArgs) error
	UpdatePeer(ctx context.Context, args *UpdatePeerArgs) error

	// Other
	ListServices(ctx context.Context, args *ListServicesArgs) (Services, error)
	ListServicePublications(ctx context.Context, args *ListServicePublicationsArgs) (*Publications, string, error)
	ListDelegatedServicePublications(ctx context.Context, args *ListDelegatedServicePublicationsArgs) (*Publications, string, error)
	ListTXLogRecords(ctx context.Context, args *ListTXLogRecordsRequest) ([]*TXLogRecord, error)
	GetPeerInfo(ctx context.Context) (*Peer, error)
	ListPeers(ctx context.Context, peerIDs *[]string) (Peers, error)
	SyncPeers(ctx context.Context) (map[string]SynchronizationResult, error)
}

var (
	ErrInternalError                      = errors.New("internal error")
	ErrContractDistributionsNotFoundError = errors.New("contract distributions not found")
)

type ValidationError struct {
	message string
}

func NewValidationError(message string) *ValidationError {
	return &ValidationError{
		message: message,
	}
}

func (v *ValidationError) Error() string {
	return v.message
}

type Contracts []*Contract

type Contract struct {
	IV                                string
	Hash                              string
	HashAlgorithm                     HashAlg
	GroupID                           string
	CreatedAt                         time.Time
	Peers                             []string
	AcceptSignatures                  map[string]Signature
	RejectSignatures                  map[string]Signature
	RevokeSignatures                  map[string]Signature
	ValidFrom                         time.Time
	ValidUntil                        time.Time
	HasRejected                       bool
	HasAccepted                       bool
	HasRevoked                        bool
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	State                             ContractState
}

type PendingContracts struct {
	PendingContracts []*PendingContract
	TotalCount       int
}

type PendingContract struct {
	ContentHash    string
	Grants         []*PendingContractGrant
	ValidNotBefore time.Time
}

type PendingContractGrant struct {
	ServiceName                   string
	ServicePeerID                 string
	OutwayPeerID                  string
	DelegatorPeerID               string
	ServicePublicationDelegatorID string
	GrantType                     GrantType
}

type SynchronizationResult struct {
	Ok  bool
	Err error
}

type ContractDistributions []*ContractDistribution

type ContractDistribution struct {
	PeerID      string
	ContentHash string
	Signature   string
	Action      Action
	Attempts    int
	LastAttempt time.Time
	NextAttempt *time.Time
	Reason      string
}

type Action int

const (
	SubmitContract Action = iota
	SubmitAcceptSignature
	SubmitRejectSignature
	SubmitRevokeSignature
)

type Peers []*Peer

type Peer struct {
	ID             string
	Name           string
	ManagerAddress string
	Roles          []PeerRole
}

type Signature struct {
	SignedAt time.Time
}

type Services []interface{}

type Service struct {
	Name     string
	PeerID   string
	PeerName string
}

type DelegatedService struct {
	Name             string
	ProviderPeerID   string
	ProviderPeerName string
	DelegateeID      string
	DelegateeName    string
}

type HashAlg int32

const (
	HashAlgUnspecified HashAlg = iota
	HashAlgSHA3_512
)

type CreateContractArgs struct {
	IV                                string
	HashAlgorithm                     HashAlg
	GroupID                           string
	ContractNotBefore                 time.Time
	ContractNotAfter                  time.Time
	CreatedAt                         time.Time
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	AuditlogCorrelationID             string
}

type Grant interface {
	ServicePublicationGrant | ServiceConnectionGrant | DelegatedServiceConnectionGrant | DelegatedServicePublicationGrant
}

type ServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol string
}

type ServiceConnectionGrant struct {
	Hash                              string
	ServicePeerID                     string
	ServicePublicationDelegatorPeerID string
	ServiceName                       string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
}

type DelegatedServiceConnectionGrant struct {
	Hash                              string
	DelegatorPeerID                   string
	ServicePeerID                     string
	ServiceName                       string
	ServicePublicationDelegatorPeerID string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
}

type DelegatedServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol string
}

type SortOrder uint32

const (
	SortOrderUnspecified SortOrder = iota
	SortOrderAscending
	SortOrderDescending
)

type Pagination struct {
	StartID   string
	Limit     uint32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}

type TXLogFilter struct {
	Period        *Period
	TransactionID *record.TransactionID
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type ListTXLogRecordsRequest struct {
	Pagination       *Pagination
	Filters          []*TXLogFilter
	DataSourcePeerID string
}

type TXLogRecord struct {
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type TXLogRecordSource struct {
	OutwayPeerID string
}

type TXLogRecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type TXLogRecordDestination struct {
	ServicePeerID string
}

type TXLogRecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

type GrantType string

const (
	GrantTypePeerUnspecified             GrantType = ""
	GrantTypeServicePublication          GrantType = "service_publication"
	GrantTypeServiceConnection           GrantType = "service_connection"
	GrantTypeDelegatedServicePublication GrantType = "delegated_service_publication"
	GrantTypeDelegatedServiceConnection  GrantType = "delegated_service_connection"
)

type ContractState string

const (
	ContractStateUnspecified ContractState = "CONTRACT_STATE_UNSPECIFIED"
	ContractStateExpired     ContractState = "CONTRACT_STATE_EXPIRED"
	ContractStateProposed    ContractState = "CONTRACT_STATE_PROPOSED"
	ContractStateRejected    ContractState = "CONTRACT_STATE_REJECTED"
	ContractStateRevoked     ContractState = "CONTRACT_STATE_REVOKED"
	ContractStateValid       ContractState = "CONTRACT_STATE_VALID"
)

type CreatePeerArgs struct {
	PeerID                string
	PeerName              string
	ManagerAddress        string
	AuditlogCorrelationID string
	Roles                 []PeerRole
}

type PeerRole string

const (
	PeerRoleDirectory PeerRole = "PEER_ROLE_DIRECTORY"
)

func (p PeerRole) String() string {
	return string(p)
}

type UpdatePeerArgs struct {
	PeerID                string
	PeerName              string
	ManagerAddress        string
	AuditlogCorrelationID string
	Roles                 []PeerRole
}

type ListContractsArgs struct {
	Pagination *ContractsPagination
	Filters    *ContractsFilters
}

type ListContractsPendingArgs struct {
	Pagination *PendingContractsPagination
}

type ContractsPagination struct {
	Cursor string
	Limit  uint32
}

type PendingContractsPagination struct {
	Cursor string
	Limit  uint32
}

type ContractsFilters struct {
	ContentHashes []string
	GrantTypes    []GrantType
}

type ListServicesArgs struct {
	DirectoryPeerID             string
	ServiceProviderPeerIDFilter *string
}

type ListServicePublicationsArgs struct {
	Pagination     *Pagination
	ServiceNames   []string
	ContractStates []contract.ContractState
}

type ListDelegatedServicePublicationsPagination struct {
	Cursor    string
	Limit     uint32
	SortOrder SortOrder
}

type ListDelegatedServicePublicationsArgs struct {
	ContractStates []contract.ContractState
	Pagination     *ListDelegatedServicePublicationsPagination
}

type Publications struct {
	TotalCount   int
	Publications []*Publication
}

type Publication struct {
	ServicePeerID   string
	ServiceName     string
	ContentHash     string
	CreatedAt       time.Time
	ValidNotBefore  time.Time
	ValidNotAfter   time.Time
	State           string
	GrantHash       string
	DirectoryPeerID string
	DelegatorPeerID string
}

type IncomingConnections struct {
	TotalCount  int
	Connections []*Connection
}

type DelegatedConnections struct {
	TotalCount  int
	Connections []*Connection
}

type OutgoingConnections struct {
	TotalCount          int
	OutgoingConnections []*Connection
}

type Connection struct {
	ServicePeerID                     string
	ServicePublicationDelegatorPeerID *string
	ServiceName                       string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
	ContentHash                       string
	CreatedAt                         time.Time
	ValidNotBefore                    time.Time
	ValidNotAfter                     time.Time
	State                             string
	GrantHash                         string
	DelegatorPeerID                   *string
}

type ListOutgoingConnectionsArgs struct {
	Pagination     *ListOutgoingConnectionsPagination
	ContractStates []contract.ContractState
}

type ListOutgoingConnectionsPagination struct {
	Cursor    string
	Limit     uint32
	SortOrder SortOrder
}

type ListOutgoingConnectionsForServiceArgs struct {
	SortOrder   SortOrder
	PeerID      string
	ServiceName string
}

type ListOutgoingConnectionsForDelegatedServiceArgs struct {
	SortOrder       SortOrder
	DelegatorPeerID string
	PeerID          string
	ServiceName     string
}

type ListIncomingConnectionsArgs struct {
	ContractStates []contract.ContractState
	Pagination     *ListIncomingConnectionsPagination
	ServiceNames   []string
}

type ListIncomingConnectionsPagination struct {
	Cursor    string
	Limit     uint32
	SortOrder SortOrder
}

type ListDelegatedConnectionsArgs struct {
	ContractStates []contract.ContractState
	Pagination     *ListDelegatedConnectionsPagination
}

type ListDelegatedConnectionsPagination struct {
	Cursor    string
	Limit     uint32
	SortOrder SortOrder
}
