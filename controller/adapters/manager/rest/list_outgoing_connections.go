// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListIncomingConnections
package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) ListOutgoingConnections(ctx context.Context, args *manager.ListOutgoingConnectionsArgs) (*manager.OutgoingConnections, string, error) {
	var (
		paginationCursor *models.QueryPaginationCursor
		paginationLimit  *models.QueryPaginationLimit
	)

	if args == nil {
		args = &manager.ListOutgoingConnectionsArgs{}
	}

	if args.Pagination == nil {
		args.Pagination = &manager.ListOutgoingConnectionsPagination{}
	}

	if args.Pagination.Cursor != "" {
		paginationCursor = &args.Pagination.Cursor
	}

	if args.Pagination.Limit != 0 {
		paginationLimit = &args.Pagination.Limit
	}

	sortOrder := convertSortOrder(args.Pagination.SortOrder)

	states := getContractStates(args.ContractStates)

	includeCount := true // Hardcoded to true, if there is a toggle required in the UI this needs to change.

	res, err := m.client.ListOutgoingConnectionsWithResponse(ctx, &models.ListOutgoingConnectionsParams{
		Cursor:         paginationCursor,
		Limit:          paginationLimit,
		SortOrder:      &sortOrder,
		IncludeCount:   &includeCount,
		ContractStates: &states,
	})
	if err != nil {
		return nil, "", errors.Join(err, errors.New("could not list outgoing connections from rest manager"))
	}

	err = mapResponse(res, http.StatusOK, "could not get outgoing connections", res.Body)
	if err != nil {
		if res.StatusCode() == http.StatusInternalServerError {
			m.logger.Error(fmt.Sprintf("failed to get outgoing connections: %s", res.ApplicationproblemJSON500.Details), nil)
		}

		return nil, "", err
	}

	connections := mapConnectionsToResponse(res.JSON200.Connections)

	totalCount := 0
	if res.JSON200.TotalCount != nil {
		totalCount = *res.JSON200.TotalCount
	}

	outgoingConnections := &manager.OutgoingConnections{
		TotalCount:          totalCount,
		OutgoingConnections: connections,
	}

	return outgoingConnections, res.JSON200.Pagination.NextCursor, nil
}

func mapConnectionsToResponse(connections []models.Connection) []*manager.Connection {
	result := make([]*manager.Connection, len(connections))

	for i, connection := range connections {
		c := &manager.Connection{
			ServicePeerID:                     connection.ServicePeerId,
			ServiceName:                       connection.ServiceName,
			OutwayPeerID:                      connection.OutwayPeerId,
			OutwayPublicKeyThumbprint:         connection.OutwayPublicKeyThumbprint,
			ContentHash:                       connection.ContractHash,
			ServicePublicationDelegatorPeerID: connection.ServicePublicationDelegatorPeerId,
			DelegatorPeerID:                   connection.DelegatorPeerId,
			CreatedAt:                         time.Unix(connection.CreatedAt, 0),
			ValidNotBefore:                    time.Unix(connection.NotBefore, 0),
			ValidNotAfter:                     time.Unix(connection.NotAfter, 0),
			State:                             convertState(connection.State),
			GrantHash:                         connection.GrantHash,
		}

		result[i] = c
	}

	return result
}

func convertState(state *models.ContractState) string {
	unknown := "Unknown"

	if state == nil {
		return unknown
	}

	switch *state {
	case models.CONTRACTSTATEVALID:
		return "Valid"
	case models.CONTRACTSTATEPROPOSED:
		return "Proposed"
	case models.CONTRACTSTATEEXPIRED:
		return "Expired"
	case models.CONTRACTSTATEREVOKED:
		return "Revoked"
	case models.CONTRACTSTATEREJECTED:
		return "Rejected"
	default:
		return unknown
	}
}

func getContractStates(states []contract.ContractState) models.QueryContractStates {
	var contractStates models.QueryContractStates

	for _, state := range states {
		switch {
		case state == contract.ContractStateValid:
			contractStates = append(contractStates, models.CONTRACTSTATEVALID)
		case state == contract.ContractStateProposed:
			contractStates = append(contractStates, models.CONTRACTSTATEPROPOSED)
		case state == contract.ContractStateExpired:
			contractStates = append(contractStates, models.CONTRACTSTATEEXPIRED)
		case state == contract.ContractStateRejected:
			contractStates = append(contractStates, models.CONTRACTSTATEREJECTED)
		case state == contract.ContractStateRevoked:
			contractStates = append(contractStates, models.CONTRACTSTATEREVOKED)
		}
	}

	return contractStates
}
