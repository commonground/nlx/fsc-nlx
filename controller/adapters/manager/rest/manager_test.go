// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
)

func TestNewManagerNoClient(t *testing.T) {
	m := newMocks(t)
	_, err := restmanager.New(nil, m.lgr)
	assert.EqualError(t, err, "manager rest client is required")
}

func TestNewManagerNoLogger(t *testing.T) {
	m := newMocks(t)
	_, err := restmanager.New(m.client, nil)
	assert.EqualError(t, err, "logger is required")
}

func TestNewManager(t *testing.T) {
	m := newMocks(t)
	manager, err := restmanager.New(m.client, m.lgr)
	assert.NoError(t, err)
	assert.NotNil(t, manager)
}
