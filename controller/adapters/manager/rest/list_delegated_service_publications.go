// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) ListDelegatedServicePublications(ctx context.Context, args *manager.ListDelegatedServicePublicationsArgs) (*manager.Publications, string, error) {
	var (
		paginationCursor *models.QueryPaginationCursor
		paginationLimit  *models.QueryPaginationLimit
	)

	if args == nil {
		args = &manager.ListDelegatedServicePublicationsArgs{}
	}

	if args.Pagination == nil {
		args.Pagination = &manager.ListDelegatedServicePublicationsPagination{}
	}

	if args.Pagination.Cursor != "" {
		paginationCursor = &args.Pagination.Cursor
	}

	if args.Pagination.Limit != 0 {
		paginationLimit = &args.Pagination.Limit
	}

	sortOrder := convertSortOrder(args.Pagination.SortOrder)

	states := getContractStates(args.ContractStates)

	includeCount := true // Hardcoded to true, if there is a toggle required in the UI this needs to change.

	resp, err := m.client.ListDelegatedServicePublicationsWithResponse(ctx, &models.ListDelegatedServicePublicationsParams{
		Cursor:         paginationCursor,
		Limit:          paginationLimit,
		SortOrder:      &sortOrder,
		IncludeCount:   &includeCount,
		ContractStates: &states,
	})
	if err != nil {
		return nil, "", errors.Join(errors.New("could not list delegated service publications from manager"), err)
	}

	err = mapResponse(resp, http.StatusOK, "could not list delegated service publications", resp.Body)
	if err != nil {
		if resp.StatusCode() == http.StatusInternalServerError {
			m.logger.Error(fmt.Sprintf("failed to list delegated service publications: %s", resp.ApplicationproblemJSON500.Details), nil)
		}

		return nil, "", err
	}

	managerPublications := resp.JSON200.Publications
	publications := convertPublications(managerPublications)

	totalCount := 0
	if resp.JSON200.TotalCount != nil {
		totalCount = *resp.JSON200.TotalCount
	}

	publicationsRes := &manager.Publications{
		TotalCount:   totalCount,
		Publications: publications,
	}

	return publicationsRes, resp.JSON200.Pagination.NextCursor, nil
}
