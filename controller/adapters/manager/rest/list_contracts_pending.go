// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// nolint:gocyclo,funlen,gocognit // unable to make less complex
func (m *restManager) ListContractsPending(ctx context.Context, req *manager.ListContractsPendingArgs) (*manager.PendingContracts, string, error) {
	var (
		paginationCursor *models.QueryPaginationCursor
		paginationLimit  *models.QueryPaginationLimit
	)

	if req == nil {
		req = &manager.ListContractsPendingArgs{}
	}

	if req.Pagination == nil {
		req.Pagination = &manager.PendingContractsPagination{}
	}

	if req.Pagination.Cursor != "" {
		paginationCursor = &req.Pagination.Cursor
	}

	if req.Pagination.Limit != 0 {
		paginationLimit = &req.Pagination.Limit
	}

	includeCount := true // Hardcoded to true, if there is a toggle required in the UI this needs to change.

	res, err := m.client.ListPendingContractsWithResponse(ctx, &models.ListPendingContractsParams{
		Cursor:       paginationCursor,
		Limit:        paginationLimit,
		IncludeCount: &includeCount,
	})
	if err != nil {
		return nil, "", errors.Join(err, errors.New("could not get pending contracts from rest manager"))
	}

	err = mapResponse(res, http.StatusOK, "could not get pending contracts", res.Body)
	if err != nil {
		if res.StatusCode() == http.StatusInternalServerError {
			m.logger.Error(fmt.Sprintf("failed to get pending contracts: %s", res.ApplicationproblemJSON500.Details), nil)
		}

		return nil, "", err
	}

	contracts := make([]*manager.PendingContract, len(res.JSON200.PendingContracts))

	for i, pendingContract := range res.JSON200.PendingContracts {
		pc := &manager.PendingContract{
			ContentHash:    pendingContract.Hash,
			ValidNotBefore: time.Unix(pendingContract.ValidNotBefore, 0),
		}

		grants := make([]*manager.PendingContractGrant, 0, len(pendingContract.Grants))

		for _, grant := range pendingContract.Grants {
			discriminator, err := grant.ValueByDiscriminator()
			if err != nil {
				return nil, "", err
			}

			switch g := discriminator.(type) {
			case models.GrantServicePublication:
				grants = append(grants, &manager.PendingContractGrant{
					ServiceName:   g.Service.Name,
					ServicePeerID: g.Service.PeerId,
					GrantType:     manager.GrantTypeServicePublication,
				})
			case models.GrantDelegatedServicePublication:
				grants = append(grants, &manager.PendingContractGrant{
					ServiceName:     g.Service.Name,
					ServicePeerID:   g.Service.PeerId,
					DelegatorPeerID: g.Delegator.PeerId,
					GrantType:       manager.GrantTypeDelegatedServicePublication,
				})
			case models.GrantServiceConnection:
				v, err := g.Service.ValueByDiscriminator()
				if err != nil {
					return nil, "", err
				}

				gp := &manager.PendingContractGrant{
					OutwayPeerID: g.Outway.PeerId,
					GrantType:    manager.GrantTypeServiceConnection,
				}
				switch s := v.(type) {
				case models.Service:
					gp.ServiceName = s.Name
					gp.ServicePeerID = s.Peer.Id
				case models.DelegatedService:
					gp.ServiceName = s.Name
					gp.ServicePeerID = s.Peer.Id
					gp.ServicePublicationDelegatorID = s.Delegator.Id
				}

				grants = append(grants, gp)
			case models.GrantDelegatedServiceConnection:
				v, err := g.Service.ValueByDiscriminator()
				if err != nil {
					return nil, "", err
				}

				gp := &manager.PendingContractGrant{
					OutwayPeerID:    g.Outway.PeerId,
					DelegatorPeerID: g.Delegator.PeerId,
					GrantType:       manager.GrantTypeDelegatedServiceConnection,
				}
				switch s := v.(type) {
				case models.Service:
					gp.ServiceName = s.Name
					gp.ServicePeerID = s.Peer.Id
				case models.DelegatedService:
					gp.ServiceName = s.Name
					gp.ServicePeerID = s.Peer.Id
					gp.ServicePublicationDelegatorID = s.Delegator.Id
				}

				grants = append(grants, gp)
			}
		}

		pc.Grants = grants
		contracts[i] = pc
	}

	totalCount := 0
	if res.JSON200.TotalCount != nil {
		totalCount = *res.JSON200.TotalCount
	}

	pendingContracts := &manager.PendingContracts{
		PendingContracts: contracts,
		TotalCount:       totalCount,
	}

	return pendingContracts, res.JSON200.Pagination.NextCursor, nil
}
