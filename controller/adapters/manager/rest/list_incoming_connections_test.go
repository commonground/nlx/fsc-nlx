// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:funlen,dupl // table tests are long and share the same structure
func TestListIncomingConnections(t *testing.T) {
	t.Parallel()

	mockClock := testingutils.NewMockClock(time.Now())

	testcases := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *manager.ListIncomingConnectionsArgs
		want    *manager.IncomingConnections
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, m *mocks) {
				totalCount := 1
				state := models.CONTRACTSTATEVALID
				sortOrder := models.SORTORDERASCENDING
				includeCount := true
				var states []models.ContractState
				m.client.EXPECT().ListIncomingConnectionsWithResponse(ctx, &models.ListIncomingConnectionsParams{
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(&api.ListIncomingConnectionsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Connections []models.Connection     `json:"connections"`
						Pagination  models.PaginationResult `json:"pagination"`
						TotalCount  *int                    `json:"totalCount,omitempty"`
					}{Connections: []models.Connection{
						{
							ContractHash:              "test-hash",
							CreatedAt:                 mockClock.Now().Unix(),
							GrantHash:                 "test-grant-hash",
							NotAfter:                  mockClock.Now().Add(1 * time.Hour).Unix(),
							NotBefore:                 mockClock.Now().Unix(),
							OutwayPeerId:              "1",
							OutwayPublicKeyThumbprint: "test-thumbprint",
							ServiceName:               "test-service-name",
							ServicePeerId:             "2",
							State:                     &state,
						},
					}, Pagination: models.PaginationResult{NextCursor: "test-cursor"}, TotalCount: &totalCount},
				}, nil)
			},
			want: &manager.IncomingConnections{
				TotalCount: 1,
				Connections: []*manager.Connection{
					{
						ServicePeerID:             "2",
						ServiceName:               "test-service-name",
						OutwayPeerID:              "1",
						OutwayPublicKeyThumbprint: "test-thumbprint",
						ContentHash:               "test-hash",
						CreatedAt:                 mockClock.Now().Truncate(time.Second),
						ValidNotBefore:            mockClock.Now().Truncate(time.Second),
						ValidNotAfter:             mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
						State:                     "Valid",
						GrantHash:                 "test-grant-hash",
					},
				},
			},
		},
		"happy_flow_with_pagination_and_service_filter": {
			setup: func(ctx context.Context, m *mocks) {
				totalCount := 1
				state := models.CONTRACTSTATEVALID
				sortOrder := models.SORTORDERASCENDING
				includeCount := true
				var states []models.ContractState
				serviceNames := []string{"test-service-name"}
				limit := models.QueryPaginationLimit(1)
				cursor := "test-cursor"
				m.client.EXPECT().ListIncomingConnectionsWithResponse(ctx, &models.ListIncomingConnectionsParams{
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
					ServiceNames:   &serviceNames,
					Limit:          &limit,
					Cursor:         &cursor,
				}).Return(&api.ListIncomingConnectionsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Connections []models.Connection     `json:"connections"`
						Pagination  models.PaginationResult `json:"pagination"`
						TotalCount  *int                    `json:"totalCount,omitempty"`
					}{Connections: []models.Connection{
						{
							ContractHash:              "test-hash",
							CreatedAt:                 mockClock.Now().Unix(),
							GrantHash:                 "test-grant-hash",
							NotAfter:                  mockClock.Now().Add(1 * time.Hour).Unix(),
							NotBefore:                 mockClock.Now().Unix(),
							OutwayPeerId:              "1",
							OutwayPublicKeyThumbprint: "test-thumbprint",
							ServiceName:               "test-service-name",
							ServicePeerId:             "2",
							State:                     &state,
						},
					}, Pagination: models.PaginationResult{NextCursor: "test-cursor"}, TotalCount: &totalCount},
				}, nil)
			},
			args: &manager.ListIncomingConnectionsArgs{
				Pagination: &manager.ListIncomingConnectionsPagination{
					Cursor:    "test-cursor",
					Limit:     1,
					SortOrder: manager.SortOrderAscending,
				},
				ServiceNames: []string{"test-service-name"},
			},
			want: &manager.IncomingConnections{
				TotalCount: 1,
				Connections: []*manager.Connection{
					{
						ServicePeerID:             "2",
						ServiceName:               "test-service-name",
						OutwayPeerID:              "1",
						OutwayPublicKeyThumbprint: "test-thumbprint",
						ContentHash:               "test-hash",
						CreatedAt:                 mockClock.Now().Truncate(time.Second),
						ValidNotBefore:            mockClock.Now().Truncate(time.Second),
						ValidNotAfter:             mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
						State:                     "Valid",
						GrantHash:                 "test-grant-hash",
					},
				},
			},
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks) {
				sortOrder := models.SORTORDERASCENDING
				includeCount := true
				var states []models.ContractState
				m.client.EXPECT().ListIncomingConnectionsWithResponse(ctx, &models.ListIncomingConnectionsParams{
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(nil, errors.New("test error"))
			},
			wantErr: errors.New("test error\ncould not list incoming connections from rest manager"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks) {
				sortOrder := models.SORTORDERASCENDING
				includeCount := true
				var states []models.ContractState
				m.client.EXPECT().ListIncomingConnectionsWithResponse(ctx, &models.ListIncomingConnectionsParams{
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(&api.ListIncomingConnectionsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
					ApplicationproblemJSON500: &models.N500InternalServerError{
						Details:  "test error",
						Instance: "test instance",
						Status:   http.StatusInternalServerError,
						Title:    "test error",
						Type:     "test instance",
					},
				}, nil)
			},
			wantErr: errors.New("could not get incoming connections: internal error"),
		},
	}

	for name, tc := range testcases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			tc.setup(ctx, m)

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			actual, _, err := restManager.ListIncomingConnections(ctx, tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, actual)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
