// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
)

func (m *restManager) GetPeerInfo(ctx context.Context) (*manager.Peer, error) {
	res, err := m.client.GetPeerInfoWithResponse(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "could not get peer info from rest manager")
	}

	err = mapResponse(res, http.StatusOK, "could not get peer info", res.Body)
	if err != nil {
		return nil, err
	}

	return &manager.Peer{
		ID:             res.JSON200.Peer.Id,
		Name:           res.JSON200.Peer.Name,
		ManagerAddress: res.JSON200.Peer.ManagerAddress,
	}, nil
}
