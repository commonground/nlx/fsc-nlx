// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:dupl,funlen // table tests are long and share setup
func TestCreateContract(t *testing.T) {
	t.Parallel()

	mockClock := testingutils.NewMockClock(time.Now())

	const testCorrelateionID = "test-id"

	testCases := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *manager.CreateContractArgs
		wantErr error
		want    string
	}{
		"happy_flow_service_connection_grants": {
			setup: func(ctx context.Context, m *mocks) {
				scg := createServiceConnectionGrant()
				scgDelegatedService := createServiceConnectionGrantDelegatedService()
				dscg := createDelegatedServiceConnectionGrant()
				grants := []models.Grant{scg, scgDelegatedService, dscg}
				m.client.EXPECT().CreateContractWithResponse(ctx, &models.CreateContractParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := testCorrelateionID; return &id }(),
				}, models.CreateContractJSONRequestBody{
					ContractContent: models.ContractContent{
						CreatedAt:     mockClock.Now().Unix(),
						Grants:        grants,
						GroupId:       "test-group-id",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "test-iv",
						Validity: models.Validity{
							NotAfter:  mockClock.Now().Add(1 * time.Hour).Unix(),
							NotBefore: mockClock.Now().Unix(),
						},
					},
				}).Return(&api.CreateContractResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusCreated},
					JSON201: &struct {
						ContentHash models.ContentHash `json:"content_hash"`
					}{ContentHash: "test-hash"},
				}, nil)
			},
			args: &manager.CreateContractArgs{
				IV:                "test-iv",
				HashAlgorithm:     manager.HashAlgSHA3_512,
				GroupID:           "test-group-id",
				ContractNotBefore: mockClock.Now().Truncate(time.Second),
				ContractNotAfter:  mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
				CreatedAt:         mockClock.Now().Truncate(time.Second),
				ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
					{
						ServicePeerID:             "1",
						ServiceName:               "test-service",
						OutwayPeerID:              "1",
						OutwayPublicKeyThumbprint: "test-thumbprint",
					},
					{
						ServicePeerID:                     "1",
						ServiceName:                       "test-service",
						ServicePublicationDelegatorPeerID: "2",
						OutwayPeerID:                      "1",
						OutwayPublicKeyThumbprint:         "test-thumbprint",
					},
				},
				DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
					{
						DelegatorPeerID:           "1",
						ServicePeerID:             "1",
						ServiceName:               "test-service",
						OutwayPeerID:              "1",
						OutwayPublicKeyThumbprint: "test-thumbprint",
					},
				},
				AuditlogCorrelationID: testCorrelateionID,
			},
			want: "test-hash",
		},
		"happy_flow_service_publication_grants": {
			setup: func(ctx context.Context, m *mocks) {
				spg := createServicePublicationGrant()
				dspg := createDelegatedServicePublicationGrant()
				grants := []models.Grant{spg, dspg}
				m.client.EXPECT().CreateContractWithResponse(ctx, &models.CreateContractParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := testCorrelateionID; return &id }(),
				}, models.CreateContractJSONRequestBody{
					ContractContent: models.ContractContent{
						CreatedAt:     mockClock.Now().Unix(),
						Grants:        grants,
						GroupId:       "test-group-id",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "test-iv",
						Validity: models.Validity{
							NotAfter:  mockClock.Now().Add(1 * time.Hour).Unix(),
							NotBefore: mockClock.Now().Unix(),
						},
					},
				}).Return(&api.CreateContractResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusCreated},
					JSON201: &struct {
						ContentHash models.ContentHash `json:"content_hash"`
					}{ContentHash: "test-hash"},
				}, nil)
			},
			args: &manager.CreateContractArgs{
				IV:                "test-iv",
				HashAlgorithm:     manager.HashAlgSHA3_512,
				GroupID:           "test-group-id",
				ContractNotBefore: mockClock.Now().Truncate(time.Second),
				ContractNotAfter:  mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
				CreatedAt:         mockClock.Now().Truncate(time.Second),
				ServicePublicationGrants: []*manager.ServicePublicationGrant{
					{
						DirectoryPeerID: "1",
						ServicePeerID:   "1",
						ServiceName:     "test-service",
						ServiceProtocol: string(models.PROTOCOLTCPHTTP11),
					},
				},
				DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{
					{
						DirectoryPeerID: "1",
						DelegatorPeerID: "2",
						ServicePeerID:   "1",
						ServiceName:     "test-service",
						ServiceProtocol: string(models.PROTOCOLTCPHTTP2),
					},
				},
				AuditlogCorrelationID: testCorrelateionID,
			},
			want: "test-hash",
		},
		"invalid_request_no_grants": {
			wantErr: errors.New("could not convert args to rest api grant args: no valid contract provided"),
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks) {
				scg := createDelegatedServiceConnectionDelegatedServiceGrant()
				grants := []models.Grant{scg}
				m.client.EXPECT().CreateContractWithResponse(ctx, &models.CreateContractParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := testCorrelateionID; return &id }(),
				}, models.CreateContractJSONRequestBody{
					ContractContent: models.ContractContent{
						CreatedAt:     mockClock.Now().Unix(),
						Grants:        grants,
						GroupId:       "test-group-id",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "test-iv",
						Validity: models.Validity{
							NotAfter:  mockClock.Now().Add(1 * time.Hour).Unix(),
							NotBefore: mockClock.Now().Unix(),
						},
					},
				}).Return(nil, errors.New("test error"))
			},
			args: &manager.CreateContractArgs{
				IV:                "test-iv",
				HashAlgorithm:     manager.HashAlgSHA3_512,
				GroupID:           "test-group-id",
				ContractNotBefore: mockClock.Now().Truncate(time.Second),
				ContractNotAfter:  mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
				CreatedAt:         mockClock.Now().Truncate(time.Second),
				DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
					{
						DelegatorPeerID:                   "1",
						ServicePeerID:                     "1",
						ServiceName:                       "test-service",
						ServicePublicationDelegatorPeerID: "2",
						OutwayPeerID:                      "1",
						OutwayPublicKeyThumbprint:         "test-thumbprint",
					},
				},
				AuditlogCorrelationID: testCorrelateionID,
			},
			wantErr: errors.New("could not create contract in rest manager: internal error: test error"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks) {
				scg := createServiceConnectionGrant()
				grants := []models.Grant{scg}
				m.client.EXPECT().CreateContractWithResponse(ctx, &models.CreateContractParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := testCorrelateionID; return &id }(),
				}, models.CreateContractJSONRequestBody{
					ContractContent: models.ContractContent{
						CreatedAt:     mockClock.Now().Unix(),
						Grants:        grants,
						GroupId:       "test-group-id",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Iv:            "test-iv",
						Validity: models.Validity{
							NotAfter:  mockClock.Now().Add(1 * time.Hour).Unix(),
							NotBefore: mockClock.Now().Unix(),
						},
					},
				}).Return(&api.CreateContractResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
					ApplicationproblemJSON500: &models.N500InternalServerError{
						Details:  "test error",
						Instance: "test instance",
						Status:   http.StatusInternalServerError,
						Title:    "test error",
						Type:     "test error",
					},
				}, nil)
			},
			args: &manager.CreateContractArgs{
				IV:                "test-iv",
				HashAlgorithm:     manager.HashAlgSHA3_512,
				GroupID:           "test-group-id",
				ContractNotBefore: mockClock.Now().Truncate(time.Second),
				ContractNotAfter:  mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
				CreatedAt:         mockClock.Now().Truncate(time.Second),
				ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
					{
						ServicePeerID:             "1",
						ServiceName:               "test-service",
						OutwayPeerID:              "1",
						OutwayPublicKeyThumbprint: "test-thumbprint",
					},
				},
				AuditlogCorrelationID: testCorrelateionID,
			},
			wantErr: errors.New("could not create contract: internal error"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			if tc.setup != nil {
				tc.setup(ctx, m)
			}

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			contentHash, err := restManager.CreateContract(ctx, tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, contentHash)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}

//nolint:errcheck // only used for test setup
func createServiceConnectionGrant() models.Grant {
	grant := models.Grant{}
	serviceConnectionService := models.GrantServiceConnection_Service{}
	service := models.Service{
		Name: "test-service",
		Peer: models.Peer{
			Id: "1",
		},
		Type: models.SERVICETYPESERVICE,
	}
	serviceConnectionService.FromService(service)
	serviceConnectionGrant := models.GrantServiceConnection{
		Outway: models.OutwayPeer{
			PeerId:              "1",
			PublicKeyThumbprint: "test-thumbprint",
		},
		Service: serviceConnectionService,
		Type:    models.GRANTTYPESERVICECONNECTION,
	}
	grant.FromGrantServiceConnection(serviceConnectionGrant)

	return grant
}

//nolint:errcheck // only used for test setup
func createServiceConnectionGrantDelegatedService() models.Grant {
	grant := models.Grant{}
	serviceConnectionService := models.GrantServiceConnection_Service{}
	service := models.DelegatedService{
		Name: "test-service",
		Peer: models.Peer{
			Id: "1",
		},
		Delegator: models.Peer{
			Id: "2",
		},
		Type: models.SERVICETYPEDELEGATEDSERVICE,
	}
	serviceConnectionService.FromDelegatedService(service)
	serviceConnectionGrant := models.GrantServiceConnection{
		Outway: models.OutwayPeer{
			PeerId:              "1",
			PublicKeyThumbprint: "test-thumbprint",
		},
		Service: serviceConnectionService,
		Type:    models.GRANTTYPESERVICECONNECTION,
	}
	grant.FromGrantServiceConnection(serviceConnectionGrant)

	return grant
}

//nolint:errcheck // this is only used in tests
func createDelegatedServiceConnectionGrant() models.Grant {
	grant := models.Grant{}
	service := models.Service{
		Name: "test-service",
		Peer: models.Peer{
			Id: "1",
		},
		Type: models.SERVICETYPESERVICE,
	}
	serviceConnectionService := models.GrantDelegatedServiceConnection_Service{}
	serviceConnectionService.FromService(service)

	delegatedServiceConnectionGrant := models.GrantDelegatedServiceConnection{
		Delegator: models.DelegatorPeer{
			PeerId: "1",
		},
		Outway: models.OutwayPeer{
			PeerId:              "1",
			PublicKeyThumbprint: "test-thumbprint",
		},
		Service: serviceConnectionService,
		Type:    models.GRANTTYPEDELEGATEDSERVICECONNECTION,
	}

	grant.FromGrantDelegatedServiceConnection(delegatedServiceConnectionGrant)

	return grant
}

//nolint:errcheck // this is only used in tests
func createDelegatedServiceConnectionDelegatedServiceGrant() models.Grant {
	grant := models.Grant{}
	service := models.DelegatedService{
		Name: "test-service",
		Peer: models.Peer{
			Id: "1",
		},
		Delegator: models.Peer{
			Id: "2",
		},
		Type: models.SERVICETYPEDELEGATEDSERVICE,
	}
	serviceConnectionService := models.GrantDelegatedServiceConnection_Service{}
	serviceConnectionService.FromDelegatedService(service)

	delegatedServiceConnectionGrant := models.GrantDelegatedServiceConnection{
		Delegator: models.DelegatorPeer{
			PeerId: "1",
		},
		Outway: models.OutwayPeer{
			PeerId:              "1",
			PublicKeyThumbprint: "test-thumbprint",
		},
		Service: serviceConnectionService,
		Type:    models.GRANTTYPEDELEGATEDSERVICECONNECTION,
	}

	grant.FromGrantDelegatedServiceConnection(delegatedServiceConnectionGrant)

	return grant
}

//nolint:errcheck // this is only used in tests
func createServicePublicationGrant() models.Grant {
	grant := models.Grant{}

	servicePublicationGrant := models.GrantServicePublication{
		Directory: models.DirectoryPeer{
			PeerId: "1",
		},
		Service: models.ServicePublication{
			Name:     "test-service",
			PeerId:   "1",
			Protocol: models.PROTOCOLTCPHTTP11,
		},
		Type: models.GRANTTYPESERVICEPUBLICATION,
	}

	grant.FromGrantServicePublication(servicePublicationGrant)

	return grant
}

//nolint:errcheck // this is only used in tests
func createDelegatedServicePublicationGrant() models.Grant {
	grant := models.Grant{}

	delegatedServicePublicationGrant := models.GrantDelegatedServicePublication{
		Delegator: models.DelegatorPeer{PeerId: "2"},
		Directory: models.DirectoryPeer{PeerId: "1"},
		Service: models.ServicePublication{
			Name:     "test-service",
			PeerId:   "1",
			Protocol: models.PROTOCOLTCPHTTP2,
		},
		Type: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
	}

	grant.FromGrantDelegatedServicePublication(delegatedServicePublicationGrant)

	return grant
}
