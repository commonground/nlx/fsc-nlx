// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:funlen,errcheck,dupl // table tests are long and share similar setup
func TestListContractsPending(t *testing.T) {
	t.Parallel()

	mockClock := testingutils.NewMockClock(time.Now())

	const testCursor = "test-cursor"

	testCases := map[string]struct {
		setup   func(context.Context, *mocks, *manager.ListContractsPendingArgs)
		args    *manager.ListContractsPendingArgs
		want    manager.PendingContracts
		wantErr error
	}{
		"happy_flow_single_pending_contract": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListContractsPendingArgs) {
				totalCount := 1
				includeCount := true
				grant := models.Grant{}
				spg := models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: "1",
					},
					Hash: "test-grant-hash",
					Service: models.ServicePublication{
						Name:     "test-service",
						PeerId:   "1",
						Protocol: "test-protocol",
					},
					Type: "servicepublication",
				}
				grant.FromGrantServicePublication(spg)

				m.client.EXPECT().ListPendingContractsWithResponse(ctx, &models.ListPendingContractsParams{
					Cursor:       nil,
					Limit:        nil,
					IncludeCount: &includeCount,
				}).Return(&api.ListPendingContractsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination       models.PaginationResult  `json:"pagination"`
						PendingContracts []models.PendingContract `json:"pending_contracts"`
						TotalCount       *int                     `json:"totalCount,omitempty"`
					}{Pagination: models.PaginationResult{}, PendingContracts: []models.PendingContract{
						{
							Grants:         []models.Grant{grant},
							Hash:           "testhash",
							ValidNotBefore: mockClock.Now().Unix(),
						},
					}, TotalCount: &totalCount},
				}, nil)
			},
			want: manager.PendingContracts{
				PendingContracts: []*manager.PendingContract{
					{
						ContentHash: "testhash",
						Grants: []*manager.PendingContractGrant{
							{
								ServiceName:   "test-service",
								ServicePeerID: "1",
								GrantType:     "service_publication",
							},
						},
						ValidNotBefore: mockClock.Now().Truncate(time.Second),
					},
				},
				TotalCount: 1,
			},
		},
		"happy_flow_multiple_pending_contracts": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListContractsPendingArgs) {
				totalCount := 3
				includeCount := true
				grantSpg := models.Grant{}
				spg := models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: "1",
					},
					Hash: "test-grant-hash",
					Service: models.ServicePublication{
						Name:     "test-service",
						PeerId:   "1",
						Protocol: "test-protocol",
					},
					Type: "servicepublication",
				}
				grantSpg.FromGrantServicePublication(spg)

				service := models.GrantServiceConnection_Service{}
				service.FromService(models.Service{
					Name: "test-service",
					Peer: models.Peer{
						Id:             "1",
						ManagerAddress: "https://example.com:8443",
						Name:           "test-peer",
					},
					Type: models.SERVICETYPESERVICE,
				})
				grantScg := models.Grant{}
				scg := models.GrantServiceConnection{
					Hash: "test-grant-hash-2",
					Outway: models.OutwayPeer{
						PeerId:              "1",
						PublicKeyThumbprint: "testpublickeythumbprint",
					},
					Service: service,
					Type:    models.GRANTTYPESERVICECONNECTION,
				}
				grantScg.FromGrantServiceConnection(scg)

				delegatedService := models.GrantDelegatedServiceConnection_Service{}
				delegatedService.FromDelegatedService(models.DelegatedService{
					Delegator: models.Peer{
						Id:             "3",
						ManagerAddress: "https://example.com:8443",
						Name:           "test-peer",
					},
					Name: "test-delegated-service",
					Peer: models.Peer{
						Id: "1",
					},
					Type: models.SERVICETYPEDELEGATEDSERVICE,
				})

				grantDscg := models.Grant{}
				dscg := models.GrantDelegatedServiceConnection{
					Delegator: models.DelegatorPeer{
						PeerId: "3",
					},
					Hash: "test-grant-hash-3",
					Outway: models.OutwayPeer{
						PeerId:              "2",
						PublicKeyThumbprint: "",
					},
					Service: delegatedService,
					Type:    models.GRANTTYPEDELEGATEDSERVICECONNECTION,
				}
				grantDscg.FromGrantDelegatedServiceConnection(dscg)

				m.client.EXPECT().ListPendingContractsWithResponse(ctx, &models.ListPendingContractsParams{
					Cursor:       nil,
					Limit:        nil,
					IncludeCount: &includeCount,
				}).Return(&api.ListPendingContractsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination       models.PaginationResult  `json:"pagination"`
						PendingContracts []models.PendingContract `json:"pending_contracts"`
						TotalCount       *int                     `json:"totalCount,omitempty"`
					}{Pagination: models.PaginationResult{}, PendingContracts: []models.PendingContract{
						{
							Grants:         []models.Grant{grantSpg, grantScg, grantDscg},
							Hash:           "testhash",
							ValidNotBefore: mockClock.Now().Unix(),
						},
					}, TotalCount: &totalCount},
				}, nil)
			},
			want: manager.PendingContracts{
				PendingContracts: []*manager.PendingContract{
					{
						ContentHash: "testhash",
						Grants: []*manager.PendingContractGrant{
							{
								ServiceName:   "test-service",
								ServicePeerID: "1",
								GrantType:     "service_publication",
							},
							{
								OutwayPeerID:  "1",
								ServiceName:   "test-service",
								ServicePeerID: "1",
								GrantType:     "service_connection",
							},
							{
								ServiceName:                   "test-delegated-service",
								ServicePeerID:                 "1",
								OutwayPeerID:                  "2",
								DelegatorPeerID:               "3",
								ServicePublicationDelegatorID: "3",
								GrantType:                     "delegated_service_connection",
							},
						},
						ValidNotBefore: mockClock.Now().Truncate(time.Second),
					},
				},
				TotalCount: 3,
			},
		},
		"happy_flow_single_pending_contract_with_pagination": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListContractsPendingArgs) {
				totalCount := 1
				grant := models.Grant{}
				spg := models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: "1",
					},
					Hash: "test-grant-hash",
					Service: models.ServicePublication{
						Name:     "test-service",
						PeerId:   "1",
						Protocol: "test-protocol",
					},
					Type: "servicepublication",
				}
				grant.FromGrantServicePublication(spg)

				includeCount := true
				limit := models.QueryPaginationLimit(1)
				cursor := testCursor
				m.client.EXPECT().ListPendingContractsWithResponse(ctx, &models.ListPendingContractsParams{
					Cursor:       &cursor,
					Limit:        &limit,
					IncludeCount: &includeCount,
				}).Return(&api.ListPendingContractsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination       models.PaginationResult  `json:"pagination"`
						PendingContracts []models.PendingContract `json:"pending_contracts"`
						TotalCount       *int                     `json:"totalCount,omitempty"`
					}{Pagination: models.PaginationResult{}, PendingContracts: []models.PendingContract{
						{
							Grants:         []models.Grant{grant},
							Hash:           "testhash",
							ValidNotBefore: mockClock.Now().Unix(),
						},
					}, TotalCount: &totalCount},
				}, nil)
			},
			want: manager.PendingContracts{
				PendingContracts: []*manager.PendingContract{
					{
						ContentHash: "testhash",
						Grants: []*manager.PendingContractGrant{
							{
								ServiceName:   "test-service",
								ServicePeerID: "1",
								GrantType:     "service_publication",
							},
						},
						ValidNotBefore: mockClock.Now().Truncate(time.Second),
					},
				},
				TotalCount: 1,
			},
			args: &manager.ListContractsPendingArgs{
				Pagination: &manager.PendingContractsPagination{
					Cursor: testCursor,
					Limit:  1,
				},
			},
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListContractsPendingArgs) {
				includeCount := true

				m.client.EXPECT().ListPendingContractsWithResponse(ctx, &models.ListPendingContractsParams{
					IncludeCount: &includeCount,
				}).Return(nil, errors.New("test error"))
			},
			wantErr: errors.New("test error\ncould not get pending contracts from rest manager"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListContractsPendingArgs) {
				includeCount := true

				m.client.EXPECT().ListPendingContractsWithResponse(ctx, &models.ListPendingContractsParams{
					IncludeCount: &includeCount,
				}).Return(&api.ListPendingContractsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
					ApplicationproblemJSON500: &models.N500InternalServerError{
						Details:  "test error",
						Instance: "test instance",
						Status:   http.StatusInternalServerError,
						Title:    "test error",
						Type:     "test instance",
					},
				}, nil)
			},
			wantErr: errors.New("could not get pending contracts: internal error"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			tc.setup(ctx, m, tc.args)

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			actual, cursor, err := restManager.ListContractsPending(ctx, tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, *actual)
				assert.NotNil(t, cursor)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
