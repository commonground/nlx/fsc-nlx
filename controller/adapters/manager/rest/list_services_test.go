// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

// nolint:funlen,dupl // tests exceed 100 lines and some duplication due to table test setup
func TestListServices(t *testing.T) {
	t.Parallel()

	peerIDGemeenteStijns := "12345678901234567890"

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		want    manager.Services
		args    *manager.ListServicesArgs
		wantErr error
	}{
		"when_manager_errors": {
			args: &manager.ListServicesArgs{
				DirectoryPeerID: "12345678901234567899",
			},
			setup: func(ctx context.Context, m *mocks) {
				var peerID *string
				m.client.EXPECT().
					ListServicesWithResponse(ctx, "12345678901234567899", &models.ListServicesParams{PeerId: peerID}).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: errors.New("could not list services from rest manager: unexpected error"),
		},
		"happy_flow": {
			args: &manager.ListServicesArgs{DirectoryPeerID: "12345678901234567899"},
			setup: func(ctx context.Context, m *mocks) {
				var peerID *string
				m.client.EXPECT().
					ListServicesWithResponse(ctx, "12345678901234567899", &models.ListServicesParams{PeerId: peerID}).
					Return(&api.ListServicesResponse{
						HTTPResponse: &http.Response{StatusCode: http.StatusOK},
						JSON200: &struct {
							Services []models.ServiceListing "json:\"services\""
						}{
							Services: func() []models.ServiceListing {
								s := make([]models.ServiceListing, 0)

								s1 := &models.ServiceListing{}

								_ = s1.FromService(models.Service{
									Name: "basisregister-fictieve-personen",
									Peer: models.Peer{
										Id:   "12345678901234567890",
										Name: "Gemeente Stijns",
									},
								})

								s2 := &models.ServiceListing{}

								_ = s2.FromDelegatedService(models.DelegatedService{
									Name: "basisregister-fictieve-kentekens",
									Peer: models.Peer{
										Id:   "12345678901234567891",
										Name: "RvRD",
									},
									Delegator: models.Peer{
										Id:   "12345678901234567890",
										Name: "Gemeente Stijns",
									},
								})

								s = append(s, *s1, *s2)

								return s
							}(),
						},
					}, nil)
			},
			want: manager.Services{
				&manager.Service{
					Name:     "basisregister-fictieve-personen",
					PeerID:   "12345678901234567890",
					PeerName: "Gemeente Stijns",
				},
				&manager.DelegatedService{
					Name:             "basisregister-fictieve-kentekens",
					ProviderPeerID:   "12345678901234567891",
					ProviderPeerName: "RvRD",
					DelegateeID:      "12345678901234567890",
					DelegateeName:    "Gemeente Stijns",
				},
			},
			wantErr: nil,
		},
		"happy_flow_filter": {
			args: &manager.ListServicesArgs{
				DirectoryPeerID:             "12345678901234567899",
				ServiceProviderPeerIDFilter: &peerIDGemeenteStijns,
			},
			setup: func(ctx context.Context, m *mocks) {
				peerID := peerIDGemeenteStijns

				m.client.EXPECT().
					ListServicesWithResponse(ctx, "12345678901234567899", &models.ListServicesParams{PeerId: &peerID}).
					Return(&api.ListServicesResponse{
						HTTPResponse: &http.Response{StatusCode: http.StatusOK},
						JSON200: &struct {
							Services []models.ServiceListing "json:\"services\""
						}{
							Services: func() []models.ServiceListing {
								s := make([]models.ServiceListing, 0)

								s1 := &models.ServiceListing{}

								_ = s1.FromService(models.Service{
									Name: "basisregister-fictieve-personen",
									Peer: models.Peer{
										Id:   "12345678901234567890",
										Name: "Gemeente Stijns",
									},
								})

								s2 := &models.ServiceListing{}

								_ = s2.FromDelegatedService(models.DelegatedService{
									Name: "basisregister-fictieve-kentekens",
									Peer: models.Peer{
										Id:   "12345678901234567891",
										Name: "RvRD",
									},
									Delegator: models.Peer{
										Id:   "12345678901234567890",
										Name: "Gemeente Stijns",
									},
								})

								s = append(s, *s1, *s2)

								return s
							}(),
						},
					}, nil)
			},
			want: manager.Services{
				&manager.Service{
					Name:     "basisregister-fictieve-personen",
					PeerID:   "12345678901234567890",
					PeerName: "Gemeente Stijns",
				},
				&manager.DelegatedService{
					Name:             "basisregister-fictieve-kentekens",
					ProviderPeerID:   "12345678901234567891",
					ProviderPeerName: "RvRD",
					DelegateeID:      "12345678901234567890",
					DelegateeName:    "Gemeente Stijns",
				},
			},
			wantErr: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			ctx := context.Background()

			tt.setup(ctx, mocks)

			restManager, err := restmanager.New(mocks.client, mocks.lgr)
			assert.NoError(t, err)

			actual, err := restManager.ListServices(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.EqualError(t, err, tt.wantErr.Error())
			}
		})
	}
}
