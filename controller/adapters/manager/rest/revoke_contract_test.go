// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // table tests have similar setup
package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

//nolint:dupl // table tests have similar setup
func TestRevokeContract(t *testing.T) {
	t.Parallel()

	const testCorrelationID = "test-id"

	testCases := map[string]struct {
		setup func(context.Context, *mocks)
		args  struct {
			contentHash           string
			auditlogCorrelationID string
		}
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().RevokeContractWithResponse(ctx, "test-hash", &models.RevokeContractParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := testCorrelationID; return &id }(),
				}).Return(&api.RevokeContractResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusNoContent},
				}, nil)
			},
			args: struct {
				contentHash           string
				auditlogCorrelationID string
			}{contentHash: "test-hash", auditlogCorrelationID: testCorrelationID},
		},
		"invalid_args_content_hash": {
			wantErr: errors.New("no content hash provided"),
		},
		"invalid_args_auditlog_correlation_id": {
			args: struct {
				contentHash           string
				auditlogCorrelationID string
			}{contentHash: "test-content-hash", auditlogCorrelationID: ""},
			wantErr: errors.New("no auditlog correlationID provided"),
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().RevokeContractWithResponse(ctx, "test-hash", &models.RevokeContractParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := testCorrelationID; return &id }(),
				}).Return(nil, errors.New("test error"))
			},
			args: struct {
				contentHash           string
				auditlogCorrelationID string
			}{contentHash: "test-hash", auditlogCorrelationID: testCorrelationID},
			wantErr: errors.New("could not revoke contract with rest manager: test error: internal error"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().RevokeContractWithResponse(ctx, "test-hash", &models.RevokeContractParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := testCorrelationID; return &id }(),
				}).Return(&api.RevokeContractResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
				}, nil)
			},
			args: struct {
				contentHash           string
				auditlogCorrelationID string
			}{contentHash: "test-hash", auditlogCorrelationID: testCorrelationID},
			wantErr: errors.New("could not revoke contract with rest manager: internal error"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			if tc.setup != nil {
				tc.setup(ctx, m)
			}

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			err = restManager.RevokeContract(ctx, tc.args.contentHash, tc.args.auditlogCorrelationID)

			if tc.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
