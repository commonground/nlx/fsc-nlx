/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package restmanager

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m restManager) ListContractDistribution(ctx context.Context, contentHash string) (contractDistributions manager.ContractDistributions, err error) {
	res, err := m.client.GetContractDistributionsWithResponse(ctx, contentHash)
	if err != nil {
		return nil, fmt.Errorf("could not retrieve contract distribution information from Manager: %s: %w", err, manager.ErrContractDistributionsNotFoundError)
	}

	err = mapResponse(res, http.StatusOK, "could not list contract distributions", res.Body)
	if err != nil {
		if res.StatusCode() == http.StatusInternalServerError {
			m.logger.Error(fmt.Sprintf("failed to list contract distributions: %s", res.ApplicationproblemJSON500.Details), nil)
		}

		return nil, err
	}

	contractDistributions = make(manager.ContractDistributions, 0, len(res.JSON200.Distributions))

	for _, contractDistributionResponse := range res.JSON200.Distributions {
		var nextAttempt *time.Time

		if contractDistributionResponse.NextAttemptAt != nil {
			nextAttemptAsTime := time.Unix(*contractDistributionResponse.NextAttemptAt, 0)
			nextAttempt = &nextAttemptAsTime
		}

		contractDistributions = append(contractDistributions, &manager.ContractDistribution{
			PeerID:      contractDistributionResponse.PeerId,
			ContentHash: contractDistributionResponse.ContractHash,
			Signature:   contractDistributionResponse.Signature,
			Action:      convertAction(contractDistributionResponse.Action),
			Attempts:    contractDistributionResponse.Attempts,
			LastAttempt: time.Unix(contractDistributionResponse.LastAttemptAt, 0),
			NextAttempt: nextAttempt,
			Reason:      contractDistributionResponse.Reason,
		})
	}

	return contractDistributions, nil
}

func convertAction(managerAction models.DistributionAction) manager.Action {
	switch managerAction {
	case models.DISTRIBUTIONACTIONSUBMITCONTRACT:
		return manager.SubmitContract
	case models.DISTRIBUTIONACTIONSUBMITACCEPTSIGNATURE:
		return manager.SubmitAcceptSignature
	case models.DISTRIBUTIONACTIONSUBMITREJECTSIGNATURE:
		return manager.SubmitRejectSignature
	case models.DISTRIBUTIONACTIONSUBMITREVOKESIGNATURE:
		return manager.SubmitRevokeSignature
	default:
		return -1
	}
}
