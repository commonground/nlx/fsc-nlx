// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

type restManager struct {
	client api.ClientWithResponsesInterface
	logger *logger.Logger
}

func New(client api.ClientWithResponsesInterface, l *logger.Logger) (manager.Manager, error) {
	if client == nil {
		return nil, errors.New("manager rest client is required")
	}

	if l == nil || l.Logger == nil {
		return nil, errors.New("logger is required")
	}

	return &restManager{
		client: client,
		logger: l,
	}, nil
}
