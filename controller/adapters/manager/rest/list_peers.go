/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package restmanager

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) ListPeers(ctx context.Context, peerIDs *[]string) (manager.Peers, error) {
	p := &models.ListPeersParams{}

	if peerIDs != nil && len(*peerIDs) > 0 {
		var pIDs []models.PeerID

		pIDs = append(pIDs, *peerIDs...)

		p.PeerIds = &pIDs
	}

	res, err := m.client.ListPeersWithResponse(ctx, p)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not retrieve peers from rest manager"))
	}

	err = mapResponse(res, http.StatusOK, "could not list peers", res.Body)
	if err != nil {
		return nil, err
	}

	peers := make(manager.Peers, 0, len(res.JSON200.Peers))

	for _, peer := range res.JSON200.Peers {
		peerRoles := convertPeerRoles(peer.Roles)

		peers = append(peers, &manager.Peer{
			ID:             peer.Id,
			Name:           peer.Name,
			ManagerAddress: peer.ManagerAddress,
			Roles:          peerRoles,
		})
	}

	return peers, nil
}

func convertPeerRoles(roles []models.PeerRole) []manager.PeerRole {
	if len(roles) == 0 {
		return []manager.PeerRole{}
	}

	managerPeerRoles := make([]manager.PeerRole, len(roles))

	for i, role := range roles {
		managerPeerRoles[i] = manager.PeerRole(role)
	}

	return managerPeerRoles
}
