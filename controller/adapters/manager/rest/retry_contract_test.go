// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func TestRetryContract(t *testing.T) {
	t.Parallel()

	const testCorrelationID = "test-id"

	testCases := map[string]struct {
		setup func(context.Context, *mocks)
		args  struct {
			contentHash           string
			peerID                string
			action                manager.Action
			auditlogCorrelationID string
		}
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().RetryContractDistributionWithResponse(ctx, "test-hash", "1", models.DISTRIBUTIONACTIONSUBMITCONTRACT, &models.RetryContractDistributionParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := testCorrelationID; return &id }(),
				}).Return(&api.RetryContractDistributionResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusCreated},
				}, nil)
			},
			args: struct {
				contentHash           string
				peerID                string
				action                manager.Action
				auditlogCorrelationID string
			}{contentHash: "test-hash", peerID: "1", action: manager.SubmitContract, auditlogCorrelationID: testCorrelationID},
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().RetryContractDistributionWithResponse(ctx, "", "", models.DISTRIBUTIONACTIONSUBMITREVOKESIGNATURE, &models.RetryContractDistributionParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := ""; return &id }(),
				}).Return(nil, errors.New("test error"))
			},
			args: struct {
				contentHash           string
				peerID                string
				action                manager.Action
				auditlogCorrelationID string
			}{contentHash: "", peerID: "", action: manager.SubmitRevokeSignature, auditlogCorrelationID: ""},
			wantErr: errors.New("could not trigger retry of contract: test error"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().RetryContractDistributionWithResponse(ctx, "test-hash", "1", models.DISTRIBUTIONACTIONSUBMITREJECTSIGNATURE, &models.RetryContractDistributionParams{
					AuditlogCorrelationId: func() *models.CorrelationID { id := testCorrelationID; return &id }(),
				}).Return(&api.RetryContractDistributionResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
					ApplicationproblemJSON500: &models.N500InternalServerError{
						Details:  "test error",
						Instance: "test instance",
						Status:   http.StatusInternalServerError,
						Title:    "test error",
						Type:     "test instance",
					},
				}, nil)
			},
			args: struct {
				contentHash           string
				peerID                string
				action                manager.Action
				auditlogCorrelationID string
			}{contentHash: "test-hash", peerID: "1", action: manager.SubmitRejectSignature, auditlogCorrelationID: testCorrelationID},
			wantErr: errors.New("manager for trigger retry of contract returned a unexpected status code: 500, body ''"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			if tc.setup != nil {
				tc.setup(ctx, m)
			}

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			err = restManager.RetryContract(ctx, tc.args.contentHash, tc.args.peerID, tc.args.action, tc.args.auditlogCorrelationID)

			if tc.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
