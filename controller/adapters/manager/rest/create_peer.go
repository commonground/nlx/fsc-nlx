// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) CreatePeer(ctx context.Context, args *manager.CreatePeerArgs) error {
	if args == nil {
		return errors.New("no peer provided")
	}

	roles := convertPeerRole(args.Roles)

	res, err := m.client.CreatePeerWithResponse(ctx, &models.CreatePeerParams{
		AuditlogCorrelationId: &args.AuditlogCorrelationID,
	},
		models.CreatePeerJSONRequestBody{
			Peer: models.Peer{
				Id:             args.PeerID,
				ManagerAddress: args.ManagerAddress,
				Name:           args.PeerName,
				Roles:          roles,
			},
		})
	if err != nil {
		return mapError(err, "could not create peer in rest manager")
	}

	return mapResponse(res, http.StatusCreated, "could not create peer", res.Body)
}

func convertPeerRole(input []manager.PeerRole) []models.PeerRole {
	roles := make([]models.PeerRole, len(input))

	for i, role := range input {
		switch role {
		case manager.PeerRoleDirectory:
			roles[i] = models.PEERROLEDIRECTORY
		default:
			return nil
		}
	}

	return roles
}
