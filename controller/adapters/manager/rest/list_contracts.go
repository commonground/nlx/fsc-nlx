// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// nolint:gocyclo,funlen,gocognit // unable to make less complex
func (m *restManager) ListContracts(ctx context.Context, req *manager.ListContractsArgs) (manager.Contracts, string, error) {
	var (
		paginationCursor *models.QueryPaginationCursor
		paginationLimit  *models.QueryPaginationLimit
		contentHashes    []models.ContentHash
	)

	if req == nil {
		req = &manager.ListContractsArgs{}
	}

	if req.Pagination == nil {
		req.Pagination = &manager.ContractsPagination{}
	}

	if req.Filters == nil {
		req.Filters = &manager.ContractsFilters{}
	}

	if req.Pagination.Cursor != "" {
		paginationCursor = &req.Pagination.Cursor
	}

	if req.Pagination.Limit != 0 {
		paginationLimit = &req.Pagination.Limit
	}

	if len(req.Filters.ContentHashes) > 0 {
		contentHashes = req.Filters.ContentHashes
	}

	res, err := m.client.GetContractsWithResponse(ctx, &models.GetContractsParams{
		Cursor:      paginationCursor,
		Limit:       paginationLimit,
		GrantType:   convertGrantTypes(req.Filters.GrantTypes),
		ContentHash: &contentHashes,
	})
	if err != nil {
		return nil, "", errors.Join(err, errors.New("could not get contracts from rest manager"))
	}

	err = mapResponse(res, http.StatusOK, "could not get contracts", res.Body)
	if err != nil {
		if res.StatusCode() == http.StatusInternalServerError {
			m.logger.Error(fmt.Sprintf("failed to get contracts: %s", res.ApplicationproblemJSON500.Details), nil)
		}

		return nil, "", err
	}

	contracts, err := mapContractsToResponse(res.JSON200.Contracts)
	if err != nil {
		return nil, "", errors.Join(err, errors.New("failed to convert contracts to response"))
	}

	return contracts, res.JSON200.Pagination.NextCursor, nil
}

type grants struct {
	servicePublicationGrants          []*manager.ServicePublicationGrant
	serviceConnectionGrants           []*manager.ServiceConnectionGrant
	delegatedServicePublicationGrants []*manager.DelegatedServicePublicationGrant
	delegatedServiceConnectionGrants  []*manager.DelegatedServiceConnectionGrant
}

func mapContractsToResponse(contracts []models.Contract) (manager.Contracts, error) {
	result := make(manager.Contracts, len(contracts))

	for i := range contracts {
		g, err := mapContractGrantsToResponse(contracts[i].Content.Grants)
		if err != nil {
			return nil, err
		}

		result[i] = &manager.Contract{
			IV:                                contracts[i].Content.Iv,
			Hash:                              contracts[i].Hash,
			HashAlgorithm:                     convertHashAlg(contracts[i].Content.HashAlgorithm),
			GroupID:                           contracts[i].Content.GroupId,
			CreatedAt:                         time.Unix(contracts[i].Content.CreatedAt, 0),
			Peers:                             contracts[i].Peers,
			AcceptSignatures:                  convertSignatures(contracts[i].Signatures.Accept),
			RejectSignatures:                  convertSignatures(contracts[i].Signatures.Reject),
			RevokeSignatures:                  convertSignatures(contracts[i].Signatures.Revoke),
			ValidFrom:                         time.Unix(contracts[i].Content.Validity.NotBefore, 0),
			ValidUntil:                        time.Unix(contracts[i].Content.Validity.NotAfter, 0),
			ServicePublicationGrants:          g.servicePublicationGrants,
			ServiceConnectionGrants:           g.serviceConnectionGrants,
			DelegatedServicePublicationGrants: g.delegatedServicePublicationGrants,
			DelegatedServiceConnectionGrants:  g.delegatedServiceConnectionGrants,
			HasRejected:                       contracts[i].HasRejected,
			HasRevoked:                        contracts[i].HasRevoked,
			HasAccepted:                       contracts[i].HasAccepted,
			State:                             manager.ContractState(contracts[i].State),
		}
	}

	return result, nil
}

// nolint:funlen,gocognit,dupl,gocyclo // this logic belongs with each other
func mapContractGrantsToResponse(input []models.Grant) (grants, error) {
	servicePublicationGrants := make([]*manager.ServicePublicationGrant, 0)
	serviceConnectionGrants := make([]*manager.ServiceConnectionGrant, 0)
	delegatedServiceConnectionGrants := make([]*manager.DelegatedServiceConnectionGrant, 0)
	delegatedServicePublicationGrants := make([]*manager.DelegatedServicePublicationGrant, 0)

	for _, grant := range input {
		discriminator, err := grant.ValueByDiscriminator()
		if err != nil {
			return grants{}, err
		}

		switch convertedGrant := discriminator.(type) {
		case models.GrantServicePublication:
			servicePublicationGrants = append(servicePublicationGrants, &manager.ServicePublicationGrant{
				Hash:            convertedGrant.Hash,
				DirectoryPeerID: convertedGrant.Directory.PeerId,
				ServicePeerID:   convertedGrant.Service.PeerId,
				ServiceName:     convertedGrant.Service.Name,
			})

		case models.GrantServiceConnection:
			serviceDiscriminator, errServiceDiscriminator := convertedGrant.Service.Discriminator()
			if errServiceDiscriminator != nil {
				return grants{}, errServiceDiscriminator
			}

			var (
				servicePeerID                     string
				serviceName                       string
				servicePublicationDelegatorPeerID string
			)

			switch serviceDiscriminator {
			case string(models.SERVICETYPESERVICE):
				service, errService := convertedGrant.Service.AsService()
				if errService != nil {
					return grants{}, fmt.Errorf("invalid service in request: %w", errService)
				}

				servicePeerID = service.Peer.Id
				serviceName = service.Name
			case string(models.SERVICETYPEDELEGATEDSERVICE):
				delegatedService, errService := convertedGrant.Service.AsDelegatedService()
				if errService != nil {
					return grants{}, fmt.Errorf("invalid delegated service in request: %w", errService)
				}

				servicePeerID = delegatedService.Peer.Id
				serviceName = delegatedService.Name
				servicePublicationDelegatorPeerID = delegatedService.Delegator.Id
			default:
				return grants{}, fmt.Errorf("invalid service type: %s", serviceDiscriminator)
			}

			serviceConnectionGrants = append(serviceConnectionGrants, &manager.ServiceConnectionGrant{
				Hash:                              convertedGrant.Hash,
				ServicePeerID:                     servicePeerID,
				ServiceName:                       serviceName,
				ServicePublicationDelegatorPeerID: servicePublicationDelegatorPeerID,
				OutwayPeerID:                      convertedGrant.Outway.PeerId,
				OutwayPublicKeyThumbprint:         convertedGrant.Outway.PublicKeyThumbprint,
			})

		case models.GrantDelegatedServiceConnection:
			serviceDiscriminator, errServiceDiscriminator := convertedGrant.Service.Discriminator()
			if errServiceDiscriminator != nil {
				return grants{}, errServiceDiscriminator
			}

			var (
				servicePeerID                     string
				serviceName                       string
				servicePublicationDelegatorPeerID string
			)

			switch serviceDiscriminator {
			case string(models.SERVICETYPESERVICE):
				service, errService := convertedGrant.Service.AsService()
				if errService != nil {
					return grants{}, fmt.Errorf("invalid service in request: %w", errService)
				}

				servicePeerID = service.Peer.Id
				serviceName = service.Name
			case string(models.SERVICETYPEDELEGATEDSERVICE):
				delegatedService, errService := convertedGrant.Service.AsDelegatedService()
				if errService != nil {
					return grants{}, fmt.Errorf("invalid delegated service in request: %w", errService)
				}

				servicePeerID = delegatedService.Peer.Id
				serviceName = delegatedService.Name
				servicePublicationDelegatorPeerID = delegatedService.Delegator.Id
			default:
				return grants{}, fmt.Errorf("invalid service type: %s", serviceDiscriminator)
			}

			delegatedServiceConnectionGrants = append(delegatedServiceConnectionGrants, &manager.DelegatedServiceConnectionGrant{
				Hash:                              convertedGrant.Hash,
				DelegatorPeerID:                   convertedGrant.Delegator.PeerId,
				ServicePeerID:                     servicePeerID,
				ServiceName:                       serviceName,
				ServicePublicationDelegatorPeerID: servicePublicationDelegatorPeerID,
				OutwayPeerID:                      convertedGrant.Outway.PeerId,
				OutwayPublicKeyThumbprint:         convertedGrant.Outway.PublicKeyThumbprint,
			})

		case models.GrantDelegatedServicePublication:
			delegatedServicePublicationGrants = append(delegatedServicePublicationGrants, &manager.DelegatedServicePublicationGrant{
				Hash:            convertedGrant.Hash,
				DirectoryPeerID: convertedGrant.Directory.PeerId,
				DelegatorPeerID: convertedGrant.Delegator.PeerId,
				ServicePeerID:   convertedGrant.Service.PeerId,
				ServiceName:     convertedGrant.Service.Name,
			})
		}
	}

	return grants{
		servicePublicationGrants:          servicePublicationGrants,
		serviceConnectionGrants:           serviceConnectionGrants,
		delegatedServicePublicationGrants: delegatedServicePublicationGrants,
		delegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
	}, nil
}

func convertSignatures(signatures models.SignatureMap) map[string]manager.Signature {
	convertedSignatures := make(map[string]manager.Signature)

	for peerID, signature := range signatures {
		convertedSignatures[peerID] = manager.Signature{
			SignedAt: time.Unix(signature.SignedAt, 0),
		}
	}

	return convertedSignatures
}

func convertHashAlg(a models.HashAlgorithm) manager.HashAlg {
	switch a {
	case models.HASHALGORITHMSHA3512:
		return manager.HashAlgSHA3_512
	default:
		return manager.HashAlgUnspecified
	}
}

func convertGrantTypes(grantTypes []manager.GrantType) *[]models.GrantType {
	gTypes := make([]models.GrantType, 0, len(grantTypes))

	for _, t := range grantTypes {
		switch t {
		case manager.GrantTypeServicePublication:
			gTypes = append(gTypes, models.GRANTTYPESERVICEPUBLICATION)
		case manager.GrantTypeServiceConnection:
			gTypes = append(gTypes, models.GRANTTYPESERVICECONNECTION)
		case manager.GrantTypeDelegatedServiceConnection:
			gTypes = append(gTypes, models.GRANTTYPEDELEGATEDSERVICECONNECTION)
		case manager.GrantTypeDelegatedServicePublication:
			gTypes = append(gTypes, models.GRANTTYPEDELEGATEDSERVICEPUBLICATION)
		default:
			break
		}
	}

	return &gTypes
}
