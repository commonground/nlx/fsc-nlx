/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

// nolint:dupl,funlen // these tests should not fit 100 lines
func TestListPeers(t *testing.T) {
	t.Parallel()

	testcases := map[string]struct {
		setup   func(context.Context, *mocks, []string)
		peerIDs []string
		want    manager.Peers
		wantErr error
	}{
		"when_manager_errors": {
			setup: func(ctx context.Context, m *mocks, peerIDs []string) {
				m.client.EXPECT().
					ListPeersWithResponse(ctx,
						&models.ListPeersParams{}).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: errors.Join(errors.New("unexpected error"), errors.New("could not retrieve peers from rest manager")),
		},
		"happy_flow_two_results": {
			setup: func(ctx context.Context, m *mocks, peerIDs []string) {
				m.client.EXPECT().ListPeersWithResponse(ctx, &models.ListPeersParams{}).Return(&api.ListPeersResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Peers []models.Peer "json:\"peers\""
					}{
						Peers: []models.Peer{
							{
								Id:             "1",
								Name:           "PeerOne",
								ManagerAddress: "https://peer1.com:8443",
							},
							{
								Id:             "2",
								Name:           "Peer2",
								ManagerAddress: "https://peer2.com:8443",
								Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
							},
						},
					},
				}, nil)
			},
			want: manager.Peers{
				{
					ID:             "1",
					Name:           "PeerOne",
					ManagerAddress: "https://peer1.com:8443",
					Roles:          []manager.PeerRole{},
				},
				{
					ID:             "2",
					Name:           "Peer2",
					ManagerAddress: "https://peer2.com:8443",
					Roles:          []manager.PeerRole{manager.PeerRoleDirectory},
				},
			},
		},
		"happy_flow_with_peer_id_filter": {
			setup: func(ctx context.Context, m *mocks, peerIDs []string) {
				m.client.EXPECT().ListPeersWithResponse(ctx, &models.ListPeersParams{PeerIds: &peerIDs}).Return(&api.ListPeersResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Peers []models.Peer "json:\"peers\""
					}{
						Peers: []models.Peer{
							{
								Id:             "1",
								Name:           "PeerOne",
								ManagerAddress: "https://peer1.com:8443",
								Roles:          []models.PeerRole{},
							},
							{
								Id:             "2",
								Name:           "Peer2",
								ManagerAddress: "https://peer2.com:8443",
								Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
							},
						},
					},
				}, nil)
			},
			want: manager.Peers{
				{
					ID:             "1",
					Name:           "PeerOne",
					ManagerAddress: "https://peer1.com:8443",
					Roles:          []manager.PeerRole{},
				},
				{
					ID:             "2",
					Name:           "Peer2",
					ManagerAddress: "https://peer2.com:8443",
					Roles:          []manager.PeerRole{manager.PeerRoleDirectory},
				},
			},
			peerIDs: []string{"1", "2"},
		},
	}

	for name, testcase := range testcases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			m := newMocks(t)

			ctx := context.Background()
			testcase.setup(ctx, m, testcase.peerIDs)

			restManager, err := restmanager.New(m.client, m.lgr)
			assert.NoError(t, err)

			actual, err := restManager.ListPeers(ctx, &testcase.peerIDs)

			if testcase.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, testcase.want, actual)
			} else {
				assert.EqualError(t, err, testcase.wantErr.Error())
			}
		})
	}
}
