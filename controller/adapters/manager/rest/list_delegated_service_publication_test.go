// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:funlen,dupl // table tests are long
func TestListDelegatedServicePublications(t *testing.T) {
	t.Parallel()

	mockClock := testingutils.NewMockClock(time.Now())

	testCases := map[string]struct {
		setup   func(context.Context, *mocks, *manager.ListDelegatedServicePublicationsArgs)
		args    *manager.ListDelegatedServicePublicationsArgs
		want    manager.Publications
		wantErr error
	}{
		"happy_flow_single_publication": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListDelegatedServicePublicationsArgs) {
				totalCount := 1
				state := models.CONTRACTSTATEVALID
				var states []models.ContractState
				sortOrder := models.SORTORDERASCENDING
				includeCount := true
				delegatorPeerID := "00000000000000000004"

				m.client.EXPECT().ListDelegatedServicePublicationsWithResponse(ctx, &models.ListDelegatedServicePublicationsParams{
					Cursor:         nil,
					Limit:          nil,
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(&api.ListDelegatedServicePublicationsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination   models.PaginationResult `json:"pagination"`
						Publications models.Publications     `json:"publications"`
						TotalCount   *int                    `json:"totalCount,omitempty"`
					}{
						Pagination: models.PaginationResult{},
						Publications: models.Publications{
							models.Publication{
								ContractHash:    "testcontracthash",
								CreatedAt:       mockClock.Now().Unix(),
								DelegatorPeerId: &delegatorPeerID,
								DirectoryPeerId: "00000000000000000001",
								GrantHash:       "testgranthash",
								NotAfter:        mockClock.Now().Add(1 * time.Hour).Unix(),
								NotBefore:       mockClock.Now().Unix(),
								ServiceName:     "test-service",
								ServicePeerId:   "00000000000000000002",
								State:           &state,
							},
						},
						TotalCount: &totalCount,
					},
				}, nil)
			},
			want: manager.Publications{
				TotalCount: 1,
				Publications: []*manager.Publication{
					{
						ServicePeerID:   "00000000000000000002",
						ServiceName:     "test-service",
						ContentHash:     "testcontracthash",
						CreatedAt:       mockClock.Now().Truncate(time.Second),
						ValidNotBefore:  mockClock.Now().Truncate(time.Second),
						ValidNotAfter:   mockClock.Now().Truncate(time.Second).Add(1 * time.Hour),
						State:           "Valid",
						GrantHash:       "testgranthash",
						DirectoryPeerID: "00000000000000000001",
						DelegatorPeerID: "00000000000000000004",
					},
				},
			},
		},
		"happy_flow_multiple_publications": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListDelegatedServicePublicationsArgs) {
				totalCount := 1
				state := models.CONTRACTSTATEVALID
				var states []models.ContractState
				sortOrder := models.SORTORDERASCENDING
				includeCount := true

				m.client.EXPECT().ListDelegatedServicePublicationsWithResponse(ctx, &models.ListDelegatedServicePublicationsParams{
					Cursor:         nil,
					Limit:          nil,
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(&api.ListDelegatedServicePublicationsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination   models.PaginationResult `json:"pagination"`
						Publications models.Publications     `json:"publications"`
						TotalCount   *int                    `json:"totalCount,omitempty"`
					}{
						Pagination: models.PaginationResult{},
						Publications: models.Publications{
							models.Publication{
								ContractHash:    "testcontracthash",
								CreatedAt:       mockClock.Now().Unix(),
								DelegatorPeerId: nil,
								DirectoryPeerId: "00000000000000000001",
								GrantHash:       "testgranthash",
								NotAfter:        mockClock.Now().Add(1 * time.Hour).Unix(),
								NotBefore:       mockClock.Now().Unix(),
								ServiceName:     "test-service",
								ServicePeerId:   "00000000000000000002",
								State:           &state,
							},
							models.Publication{
								ContractHash:    "testcontracthash2",
								CreatedAt:       mockClock.Now().Unix(),
								DelegatorPeerId: nil,
								DirectoryPeerId: "00000000000000000001",
								GrantHash:       "testgranthash2",
								NotAfter:        mockClock.Now().Add(1 * time.Hour).Unix(),
								NotBefore:       mockClock.Now().Unix(),
								ServiceName:     "test-service2",
								ServicePeerId:   "00000000000000000002",
								State:           &state,
							},
						},
						TotalCount: &totalCount,
					},
				}, nil)
			},
			want: manager.Publications{
				TotalCount: 1,
				Publications: []*manager.Publication{
					{
						ServicePeerID:   "00000000000000000002",
						ServiceName:     "test-service",
						ContentHash:     "testcontracthash",
						CreatedAt:       mockClock.Now().Truncate(time.Second),
						ValidNotBefore:  mockClock.Now().Truncate(time.Second),
						ValidNotAfter:   mockClock.Now().Truncate(time.Second).Add(1 * time.Hour),
						State:           "Valid",
						GrantHash:       "testgranthash",
						DirectoryPeerID: "00000000000000000001",
						DelegatorPeerID: "",
					},
					{
						ServicePeerID:   "00000000000000000002",
						ServiceName:     "test-service2",
						ContentHash:     "testcontracthash2",
						CreatedAt:       mockClock.Now().Truncate(time.Second),
						ValidNotBefore:  mockClock.Now().Truncate(time.Second),
						ValidNotAfter:   mockClock.Now().Truncate(time.Second).Add(1 * time.Hour),
						State:           "Valid",
						GrantHash:       "testgranthash2",
						DirectoryPeerID: "00000000000000000001",
						DelegatorPeerID: "",
					},
				},
			},
		},
		"happy_flow_service_param": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListDelegatedServicePublicationsArgs) {
				totalCount := 1
				state := models.CONTRACTSTATEVALID
				var states []models.ContractState
				sortOrder := models.SORTORDERASCENDING
				includeCount := true

				m.client.EXPECT().ListDelegatedServicePublicationsWithResponse(ctx, &models.ListDelegatedServicePublicationsParams{
					Cursor:         nil,
					Limit:          nil,
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(&api.ListDelegatedServicePublicationsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination   models.PaginationResult `json:"pagination"`
						Publications models.Publications     `json:"publications"`
						TotalCount   *int                    `json:"totalCount,omitempty"`
					}{
						Pagination: models.PaginationResult{},
						Publications: models.Publications{
							models.Publication{
								ContractHash:    "testcontracthash",
								CreatedAt:       mockClock.Now().Unix(),
								DelegatorPeerId: nil,
								DirectoryPeerId: "00000000000000000001",
								GrantHash:       "testgranthash",
								NotAfter:        mockClock.Now().Add(1 * time.Hour).Unix(),
								NotBefore:       mockClock.Now().Unix(),
								ServiceName:     "test-service",
								ServicePeerId:   "00000000000000000002",
								State:           &state,
							},
						},
						TotalCount: &totalCount,
					},
				}, nil)
			},
			want: manager.Publications{
				TotalCount: 1,
				Publications: []*manager.Publication{
					{
						ServicePeerID:   "00000000000000000002",
						ServiceName:     "test-service",
						ContentHash:     "testcontracthash",
						CreatedAt:       mockClock.Now().Truncate(time.Second),
						ValidNotBefore:  mockClock.Now().Truncate(time.Second),
						ValidNotAfter:   mockClock.Now().Truncate(time.Second).Add(1 * time.Hour),
						State:           "Valid",
						GrantHash:       "testgranthash",
						DirectoryPeerID: "00000000000000000001",
						DelegatorPeerID: "",
					},
				},
			},
		},
		"happy_flow_with_pagination": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListDelegatedServicePublicationsArgs) {
				totalCount := 1
				state := models.CONTRACTSTATEVALID
				states := []models.ContractState{models.CONTRACTSTATEPROPOSED}
				sortOrder := models.SORTORDERASCENDING
				cursor := args.Pagination.Cursor
				limit := args.Pagination.Limit
				includeCount := true

				m.client.EXPECT().ListDelegatedServicePublicationsWithResponse(ctx, &models.ListDelegatedServicePublicationsParams{
					Cursor:         &cursor,
					Limit:          &limit,
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(&api.ListDelegatedServicePublicationsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination   models.PaginationResult `json:"pagination"`
						Publications models.Publications     `json:"publications"`
						TotalCount   *int                    `json:"totalCount,omitempty"`
					}{
						Pagination: models.PaginationResult{},
						Publications: models.Publications{
							models.Publication{
								ContractHash:    "testcontracthash",
								CreatedAt:       mockClock.Now().Unix(),
								DelegatorPeerId: nil,
								DirectoryPeerId: "00000000000000000001",
								GrantHash:       "testgranthash",
								NotAfter:        mockClock.Now().Add(1 * time.Hour).Unix(),
								NotBefore:       mockClock.Now().Unix(),
								ServiceName:     "test-service",
								ServicePeerId:   "00000000000000000002",
								State:           &state,
							},
						},
						TotalCount: &totalCount,
					},
				}, nil)
			},
			want: manager.Publications{
				TotalCount: 1,
				Publications: []*manager.Publication{
					{
						ServicePeerID:   "00000000000000000002",
						ServiceName:     "test-service",
						ContentHash:     "testcontracthash",
						CreatedAt:       mockClock.Now().Truncate(time.Second),
						ValidNotBefore:  mockClock.Now().Truncate(time.Second),
						ValidNotAfter:   mockClock.Now().Truncate(time.Second).Add(1 * time.Hour),
						State:           "Valid",
						GrantHash:       "testgranthash",
						DirectoryPeerID: "00000000000000000001",
						DelegatorPeerID: "",
					},
				},
			},
			args: &manager.ListDelegatedServicePublicationsArgs{
				Pagination: &manager.ListDelegatedServicePublicationsPagination{
					Cursor:    "test-start-id",
					Limit:     1,
					SortOrder: manager.SortOrderAscending,
				},
				ContractStates: func() []contract.ContractState {
					return []contract.ContractState{
						contract.ContractStateProposed,
					}
				}(),
			},
		},
		"happy_flow_with_sort_order_descending": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListDelegatedServicePublicationsArgs) {
				totalCount := 1
				state := models.CONTRACTSTATEVALID
				var states []models.ContractState
				sortOrder := models.SORTORDERDESCENDING
				cursor := args.Pagination.Cursor
				limit := args.Pagination.Limit
				includeCount := true

				m.client.EXPECT().ListDelegatedServicePublicationsWithResponse(ctx, &models.ListDelegatedServicePublicationsParams{
					Cursor:         &cursor,
					Limit:          &limit,
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(&api.ListDelegatedServicePublicationsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination   models.PaginationResult `json:"pagination"`
						Publications models.Publications     `json:"publications"`
						TotalCount   *int                    `json:"totalCount,omitempty"`
					}{
						Pagination: models.PaginationResult{},
						Publications: models.Publications{
							models.Publication{
								ContractHash:    "testcontracthash",
								CreatedAt:       mockClock.Now().Unix(),
								DelegatorPeerId: nil,
								DirectoryPeerId: "00000000000000000001",
								GrantHash:       "testgranthash",
								NotAfter:        mockClock.Now().Add(1 * time.Hour).Unix(),
								NotBefore:       mockClock.Now().Unix(),
								ServiceName:     "test-service",
								ServicePeerId:   "00000000000000000002",
								State:           &state,
							},
						},
						TotalCount: &totalCount,
					},
				}, nil)
			},
			want: manager.Publications{
				TotalCount: 1,
				Publications: []*manager.Publication{
					{
						ServicePeerID:   "00000000000000000002",
						ServiceName:     "test-service",
						ContentHash:     "testcontracthash",
						CreatedAt:       mockClock.Now().Truncate(time.Second),
						ValidNotBefore:  mockClock.Now().Truncate(time.Second),
						ValidNotAfter:   mockClock.Now().Truncate(time.Second).Add(1 * time.Hour),
						State:           "Valid",
						GrantHash:       "testgranthash",
						DirectoryPeerID: "00000000000000000001",
						DelegatorPeerID: "",
					},
				},
			},
			args: &manager.ListDelegatedServicePublicationsArgs{
				Pagination: &manager.ListDelegatedServicePublicationsPagination{
					Cursor:    "test-start-id",
					Limit:     1,
					SortOrder: manager.SortOrderDescending,
				},
			},
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListDelegatedServicePublicationsArgs) {
				var states []models.ContractState
				sortOrder := models.SORTORDERASCENDING
				includeCount := true

				m.client.EXPECT().ListDelegatedServicePublicationsWithResponse(ctx, &models.ListDelegatedServicePublicationsParams{
					Cursor:         nil,
					Limit:          nil,
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(&api.ListDelegatedServicePublicationsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
					ApplicationproblemJSON500: &models.N500InternalServerError{
						Details:  "test error",
						Instance: "test instance",
						Status:   http.StatusInternalServerError,
						Title:    "test error",
						Type:     "test instance",
					},
				}, nil)
			},
			wantErr: errors.New("could not list delegated service publications: internal error"),
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks, args *manager.ListDelegatedServicePublicationsArgs) {
				var states []models.ContractState
				sortOrder := models.SORTORDERASCENDING
				includeCount := true

				m.client.EXPECT().ListDelegatedServicePublicationsWithResponse(ctx, &models.ListDelegatedServicePublicationsParams{
					Cursor:         nil,
					Limit:          nil,
					SortOrder:      &sortOrder,
					IncludeCount:   &includeCount,
					ContractStates: &states,
				}).Return(nil, errors.New("test error"))
			},
			wantErr: errors.New("could not list delegated service publications from manager\ntest error"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			tc.setup(ctx, m, tc.args)

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			actual, cursor, err := restManager.ListDelegatedServicePublications(ctx, tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, *actual)
				assert.NotNil(t, cursor)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
