/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package restmanager

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m restManager) RetryContract(ctx context.Context, contentHash, peerID string, action manager.Action, auditlogCorrelationID string) error {
	res, err := m.client.RetryContractDistributionWithResponse(ctx, contentHash, peerID, convertRetryAction(action), &models.RetryContractDistributionParams{
		AuditlogCorrelationId: &auditlogCorrelationID,
	})
	if err != nil {
		return fmt.Errorf("could not trigger retry of contract: %s", err)
	}

	if res.HTTPResponse.StatusCode != http.StatusCreated {
		return fmt.Errorf("manager for trigger retry of contract returned a unexpected status code: %d, body '%s'", res.HTTPResponse.StatusCode, string(res.Body))
	}

	return nil
}

func convertRetryAction(action manager.Action) models.PathDistributionAction {
	switch action {
	case manager.SubmitContract:
		return models.DISTRIBUTIONACTIONSUBMITCONTRACT
	case manager.SubmitAcceptSignature:
		return models.DISTRIBUTIONACTIONSUBMITACCEPTSIGNATURE
	case manager.SubmitRevokeSignature:
		return models.DISTRIBUTIONACTIONSUBMITREVOKESIGNATURE
	case manager.SubmitRejectSignature:
		return models.DISTRIBUTIONACTIONSUBMITREJECTSIGNATURE
	default:
		return ""
	}
}
