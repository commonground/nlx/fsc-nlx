// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

func (m *restManager) ListTXLogRecords(ctx context.Context, req *manager.ListTXLogRecordsRequest) ([]*manager.TXLogRecord, error) {
	res, err := m.client.GetLogsWithResponse(ctx, reqToRest(req))
	if err != nil {
		return nil, mapError(err, "could not get tx log records from rest manager")
	}

	err = mapResponse(res, http.StatusOK, "could not get tx log records", res.Body)
	if err != nil {
		return nil, err
	}

	return restToRecords(res.JSON200.Records)
}

func restToRecords(res []models.LogRecord) ([]*manager.TXLogRecord, error) {
	records := make([]*manager.TXLogRecord, len(res))

	for i, r := range res {
		source, destination, err := mapSourceAndDestinationToModel(&r)
		if err != nil {
			return nil, fmt.Errorf("could not map source and destination: %w", err)
		}

		records[i] = &manager.TXLogRecord{
			TransactionID: r.TransactionId,
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Direction:     mapDirection(r.Direction),
			Source:        source,
			Destination:   destination,
			CreatedAt:     time.Unix(r.CreatedAt, 0),
		}
	}

	return records, nil
}

func mapSourceAndDestinationToModel(r *models.LogRecord) (source, destination interface{}, err error) {
	sourceDiscriminator, err := r.Source.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedSourceModel := sourceDiscriminator.(type) {
	case models.Source:
		source = &manager.TXLogRecordSource{
			OutwayPeerID: convertedSourceModel.OutwayPeerId,
		}
	case models.SourceDelegated:
		source = &manager.TXLogRecordDelegatedSource{
			OutwayPeerID:    convertedSourceModel.OutwayPeerId,
			DelegatorPeerID: convertedSourceModel.DelegatorPeerId,
		}
	}

	destinationDiscriminator, err := r.Destination.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedDestinationModel := destinationDiscriminator.(type) {
	case models.Destination:
		destination = &manager.TXLogRecordDestination{
			ServicePeerID: convertedDestinationModel.ServicePeerId,
		}
	case models.DestinationDelegated:
		destination = &manager.TXLogRecordDelegatedDestination{
			ServicePeerID:   convertedDestinationModel.ServicePeerId,
			DelegatorPeerID: convertedDestinationModel.DelegatorPeerId,
		}
	}

	return source, destination, nil
}

func mapDirection(d models.DirectionType) record.Direction {
	switch d {
	case models.DIRECTIONINCOMING:
		return record.DirectionIn
	case models.DIRECTIONOUTGOING:
		return record.DirectionOut
	default:
		return record.DirectionUnspecified
	}
}

func reqToRest(req *manager.ListTXLogRecordsRequest) *models.GetLogsParams {
	if req == nil {
		req = &manager.ListTXLogRecordsRequest{}
	}

	var sortOrderParam *models.SortOrder

	if req.Pagination != nil && req.Pagination.SortOrder != 0 {
		sortOrder := sortOrderToRest(req.Pagination.SortOrder)
		sortOrderParam = &sortOrder
	}

	var limit *models.QueryPaginationLimit
	if req.Pagination != nil && req.Pagination.Limit != 0 {
		limit = &req.Pagination.Limit
	}

	var startID *string
	if req.Pagination != nil && req.Pagination.StartID != "" {
		startID = &req.Pagination.StartID
	}

	var dataSourcePeerIDParam *string
	if req.DataSourcePeerID != "" {
		dataSourcePeerIDParam = &req.DataSourcePeerID
	}

	return &models.GetLogsParams{
		Cursor:           startID,
		Limit:            limit,
		SortOrder:        sortOrderParam,
		DataSourcePeerId: dataSourcePeerIDParam,
	}
}

func sortOrderToRest(o manager.SortOrder) models.SortOrder {
	switch o {
	case manager.SortOrderAscending:
		return models.SORTORDERASCENDING
	case manager.SortOrderDescending:
		return models.SORTORDERDESCENDING
	default:
		return models.SORTORDERASCENDING
	}
}
