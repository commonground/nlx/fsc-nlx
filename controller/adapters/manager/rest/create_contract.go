// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) CreateContract(ctx context.Context, args *manager.CreateContractArgs) (string, error) {
	grants, err := convertGrants(args)
	if err != nil {
		return "", fmt.Errorf("could not convert args to rest api grant args: %w", err)
	}

	res, err := m.client.CreateContractWithResponse(ctx, &models.CreateContractParams{
		AuditlogCorrelationId: &args.AuditlogCorrelationID,
	},
		models.CreateContractJSONRequestBody{
			ContractContent: models.ContractContent{
				Iv:      args.IV,
				GroupId: args.GroupID,
				Validity: models.Validity{
					NotBefore: args.ContractNotBefore.Unix(),
					NotAfter:  args.ContractNotAfter.Unix(),
				},
				Grants:        grants,
				HashAlgorithm: mapHashAlgorithm(args.HashAlgorithm),
				CreatedAt:     args.CreatedAt.Unix(),
			},
		})
	if err != nil {
		return "", mapError(err, "could not create contract in rest manager")
	}

	if res.StatusCode() != http.StatusCreated {
		return "", mapResponse(res, http.StatusCreated, "could not create contract", res.Body)
	}

	return res.JSON201.ContentHash, nil
}

func mapHashAlgorithm(a manager.HashAlg) models.HashAlgorithm {
	switch a {
	case manager.HashAlgSHA3_512:
		return models.HASHALGORITHMSHA3512
	default:
		return ""
	}
}

//nolint:gocyclo // complex function because of the number of grant types
func convertGrants(contract *manager.CreateContractArgs) ([]models.Grant, error) {
	if contract == nil {
		return nil, errors.New("no valid contract provided")
	}

	grants := make([]models.Grant, 0)

	for _, g := range contract.ServiceConnectionGrants {
		grant, err := convertServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.ServicePublicationGrants {
		grant, err := convertServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServiceConnectionGrants {
		grant, err := convertDelegatedServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServicePublicationGrants {
		grant, err := convertDelegatedServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	return grants, nil
}

func convertServicePublicationGrant(grant *manager.ServicePublicationGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantServicePublication(models.GrantServicePublication{
		Directory: models.DirectoryPeer{
			PeerId: grant.DirectoryPeerID,
		},
		Service: models.ServicePublication{
			Name:     grant.ServiceName,
			PeerId:   grant.ServicePeerID,
			Protocol: models.Protocol(grant.ServiceProtocol),
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertServiceConnectionGrant(grant *manager.ServiceConnectionGrant) (*models.Grant, error) {
	service := models.GrantServiceConnection_Service{}

	//nolint:dupl // looks the same but is a different grant
	if grant.ServicePublicationDelegatorPeerID == "" {
		err := service.FromService(models.Service{
			Name: grant.ServiceName,
			Peer: models.Peer{
				Id:   grant.ServicePeerID,
				Name: "",
			},
		})
		if err != nil {
			return nil, err
		}
	} else {
		err := service.FromDelegatedService(models.DelegatedService{
			Name: grant.ServiceName,
			Peer: models.Peer{
				Id:   grant.ServicePeerID,
				Name: "",
			},
			Delegator: models.Peer{
				Id:   grant.ServicePublicationDelegatorPeerID,
				Name: "",
			},
		})
		if err != nil {
			return nil, err
		}
	}

	data := models.Grant{}

	err := data.FromGrantServiceConnection(models.GrantServiceConnection{
		Outway: models.OutwayPeer{
			PeerId:              grant.OutwayPeerID,
			PublicKeyThumbprint: grant.OutwayPublicKeyThumbprint,
		},
		Service: service,
	})
	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertDelegatedServiceConnectionGrant(grant *manager.DelegatedServiceConnectionGrant) (*models.Grant, error) {
	service := models.GrantDelegatedServiceConnection_Service{}

	//nolint:dupl // looks the same but is a different grant
	if grant.ServicePublicationDelegatorPeerID == "" {
		err := service.FromService(models.Service{
			Name: grant.ServiceName,
			Peer: models.Peer{
				Id:   grant.ServicePeerID,
				Name: "",
			},
		})
		if err != nil {
			return nil, err
		}
	} else {
		err := service.FromDelegatedService(models.DelegatedService{
			Name: grant.ServiceName,
			Peer: models.Peer{
				Id:   grant.ServicePeerID,
				Name: "",
			},
			Delegator: models.Peer{
				Id:   grant.ServicePublicationDelegatorPeerID,
				Name: "",
			},
		})
		if err != nil {
			return nil, err
		}
	}

	data := models.Grant{}
	err := data.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
		Delegator: models.DelegatorPeer{
			PeerId: grant.DelegatorPeerID,
		},
		Service: service,
		Outway: models.OutwayPeer{
			PeerId:              grant.OutwayPeerID,
			PublicKeyThumbprint: grant.OutwayPublicKeyThumbprint,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertDelegatedServicePublicationGrant(grant *manager.DelegatedServicePublicationGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
		Delegator: models.DelegatorPeer{
			PeerId: grant.DelegatorPeerID,
		},
		Service: models.ServicePublication{
			PeerId:   grant.ServicePeerID,
			Name:     grant.ServiceName,
			Protocol: models.Protocol(grant.ServiceProtocol),
		},
		Directory: models.DirectoryPeer{
			PeerId: grant.DirectoryPeerID,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}
