// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:funlen // table tests are long
func TestListContractDistributions(t *testing.T) {
	t.Parallel()

	mockClock := testingutils.NewMockClock(time.Now())
	nextAttempt := mockClock.Now().Add(time.Hour).Unix()
	nextAttemptAsTime := time.Unix(nextAttempt, 0)

	testcases := map[string]struct {
		setup       func(context.Context, *mocks, string)
		contentHash string
		want        manager.ContractDistributions
		wantErr     error
	}{
		"happy_flow_single_response": {
			setup: func(ctx context.Context, m *mocks, contentHash string) {
				m.client.EXPECT().GetContractDistributionsWithResponse(ctx, contentHash).Return(&api.GetContractDistributionsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Distributions []models.ContractDistribution `json:"distributions"`
					}{Distributions: []models.ContractDistribution{
						{
							Action:        models.DISTRIBUTIONACTIONSUBMITACCEPTSIGNATURE,
							Attempts:      1,
							ContractHash:  "testcontracthash",
							LastAttemptAt: mockClock.Now().Unix(),
							NextAttemptAt: &nextAttempt,
							PeerId:        "1",
							Reason:        "failed",
							Signature:     "testSignature",
						},
					}},
				}, nil)
			},
			contentHash: "testcontracthash",
			want: manager.ContractDistributions{
				&manager.ContractDistribution{
					PeerID:      "1",
					ContentHash: "testcontracthash",
					Signature:   "testSignature",
					Action:      manager.SubmitAcceptSignature,
					Attempts:    1,
					LastAttempt: mockClock.Now().Truncate(time.Second),
					NextAttempt: &nextAttemptAsTime,
					Reason:      "failed",
				},
			},
		},
		"happy_flow_multiple_responses": {
			setup: func(ctx context.Context, m *mocks, contentHash string) {
				m.client.EXPECT().GetContractDistributionsWithResponse(ctx, contentHash).Return(&api.GetContractDistributionsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Distributions []models.ContractDistribution `json:"distributions"`
					}{Distributions: []models.ContractDistribution{
						{
							Action:        models.DISTRIBUTIONACTIONSUBMITACCEPTSIGNATURE,
							Attempts:      1,
							ContractHash:  "testcontracthash",
							LastAttemptAt: mockClock.Now().Unix(),
							NextAttemptAt: &nextAttempt,
							PeerId:        "1",
							Reason:        "failed",
							Signature:     "testSignature",
						},
						{
							Action:        models.DISTRIBUTIONACTIONSUBMITREJECTSIGNATURE,
							Attempts:      1,
							ContractHash:  "testcontracthash-2",
							LastAttemptAt: mockClock.Now().Unix(),
							PeerId:        "2",
							Reason:        "failed",
							Signature:     "testSignature-2",
						},
						{
							Action:        models.DISTRIBUTIONACTIONSUBMITREVOKESIGNATURE,
							Attempts:      1,
							ContractHash:  "testcontracthash-3",
							LastAttemptAt: mockClock.Now().Unix(),
							PeerId:        "3",
							Reason:        "failed",
							Signature:     "testSignature-3",
						},
						{
							Action:        models.DISTRIBUTIONACTIONSUBMITCONTRACT,
							Attempts:      5,
							ContractHash:  "testcontracthash-4",
							LastAttemptAt: mockClock.Now().Unix(),
							PeerId:        "4",
							Reason:        "failed",
							Signature:     "testSignature-4",
						},
					}},
				}, nil)
			},
			contentHash: "testcontracthash",
			want: manager.ContractDistributions{
				&manager.ContractDistribution{
					PeerID:      "1",
					ContentHash: "testcontracthash",
					Signature:   "testSignature",
					Action:      manager.SubmitAcceptSignature,
					Attempts:    1,
					LastAttempt: mockClock.Now().Truncate(time.Second),
					NextAttempt: &nextAttemptAsTime,
					Reason:      "failed",
				},
				&manager.ContractDistribution{
					PeerID:      "2",
					ContentHash: "testcontracthash-2",
					Signature:   "testSignature-2",
					Action:      manager.SubmitRejectSignature,
					Attempts:    1,
					LastAttempt: mockClock.Now().Truncate(time.Second),
					Reason:      "failed",
				},
				&manager.ContractDistribution{
					PeerID:      "3",
					ContentHash: "testcontracthash-3",
					Signature:   "testSignature-3",
					Action:      manager.SubmitRevokeSignature,
					Attempts:    1,
					LastAttempt: mockClock.Now().Truncate(time.Second),
					Reason:      "failed",
				},
				&manager.ContractDistribution{
					PeerID:      "4",
					ContentHash: "testcontracthash-4",
					Signature:   "testSignature-4",
					Action:      manager.SubmitContract,
					Attempts:    5,
					LastAttempt: mockClock.Now().Truncate(time.Second),
					Reason:      "failed",
				},
			},
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks, contentHash string) {
				m.client.EXPECT().GetContractDistributionsWithResponse(ctx, contentHash).Return(nil, errors.New("test error"))
			},
			wantErr: errors.New("could not retrieve contract distribution information from Manager: test error: contract distributions not found"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks, contentHash string) {
				m.client.EXPECT().GetContractDistributionsWithResponse(ctx, contentHash).Return(&api.GetContractDistributionsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
					ApplicationproblemJSON500: &models.N500InternalServerError{
						Details:  "test error",
						Instance: "test instance",
						Status:   http.StatusInternalServerError,
						Title:    "test error",
						Type:     "test instance",
					},
				}, nil)
			},
			wantErr: errors.New("could not list contract distributions: internal error"),
		},
	}

	for name, tc := range testcases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			tc.setup(ctx, m, tc.contentHash)

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			actual, err := restManager.ListContractDistribution(ctx, tc.contentHash)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, actual)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
