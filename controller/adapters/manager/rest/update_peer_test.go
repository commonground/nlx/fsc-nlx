// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // table tests have similar setup
package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

//nolint:funlen // table tests are long
func TestUpdatePeer(t *testing.T) {
	t.Parallel()

	const testCorrelationID = "test"

	testCases := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *manager.UpdatePeerArgs
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().UpdatePeerWithResponse(ctx, &models.UpdatePeerParams{
					AuditlogCorrelationId: func() *string { id := testCorrelationID; return &id }(),
				}, models.UpdatePeerJSONRequestBody{
					Peer: models.Peer{
						Id:             "1",
						ManagerAddress: "test-address",
						Name:           "test-peer",
						Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
					},
				}).Return(&api.UpdatePeerResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusNoContent},
				}, nil)
			},
			args: &manager.UpdatePeerArgs{
				PeerID:                "1",
				PeerName:              "test-peer",
				ManagerAddress:        "test-address",
				AuditlogCorrelationID: testCorrelationID,
				Roles:                 []manager.PeerRole{manager.PeerRoleDirectory},
			},
		},
		"invalid_args": {
			wantErr: errors.New("no peer provided"),
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().UpdatePeerWithResponse(ctx, &models.UpdatePeerParams{
					AuditlogCorrelationId: func() *string { id := testCorrelationID; return &id }(),
				}, models.UpdatePeerJSONRequestBody{
					Peer: models.Peer{
						Id:             "1",
						ManagerAddress: "test-address",
						Name:           "test-peer",
						Roles:          []models.PeerRole{},
					},
				}).Return(nil, errors.New("test error"))
			},
			args: &manager.UpdatePeerArgs{
				PeerID:                "1",
				PeerName:              "test-peer",
				ManagerAddress:        "test-address",
				AuditlogCorrelationID: testCorrelationID,
				Roles:                 []manager.PeerRole{},
			},
			wantErr: errors.New("could not update peer in rest manager: internal error: test error"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().UpdatePeerWithResponse(ctx, &models.UpdatePeerParams{
					AuditlogCorrelationId: func() *string { id := testCorrelationID; return &id }(),
				}, models.UpdatePeerJSONRequestBody{
					Peer: models.Peer{
						Id:             "1",
						ManagerAddress: "test-address",
						Name:           "test-peer",
						Roles:          []models.PeerRole{},
					},
				}).Return(&api.UpdatePeerResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
				}, nil)
			},
			args: &manager.UpdatePeerArgs{
				PeerID:                "1",
				PeerName:              "test-peer",
				ManagerAddress:        "test-address",
				AuditlogCorrelationID: testCorrelationID,
				Roles:                 []manager.PeerRole{},
			},
			wantErr: errors.New("could not update peer: internal error"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			if tc.setup != nil {
				tc.setup(ctx, m)
			}

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			err = restManager.UpdatePeer(ctx, tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
