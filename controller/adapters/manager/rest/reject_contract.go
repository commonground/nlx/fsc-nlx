// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) RejectContract(ctx context.Context, contentHash, auditlogCorrelationID string) error {
	if contentHash == "" {
		return errors.New("no content hash provided")
	}

	if auditlogCorrelationID == "" {
		return errors.New("no auditlog correlationID provided")
	}

	res, err := m.client.RejectContractWithResponse(ctx, contentHash, &models.RejectContractParams{
		AuditlogCorrelationId: &auditlogCorrelationID,
	})
	if err != nil {
		return fmt.Errorf("could not reject contract with rest manager: %s: %w", err, manager.ErrInternalError)
	}

	return mapResponse(res, http.StatusNoContent, "could not reject contract with rest manager", res.Body)
}
