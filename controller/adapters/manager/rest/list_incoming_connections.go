// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListOutgoingConnections
package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) ListIncomingConnections(ctx context.Context, args *manager.ListIncomingConnectionsArgs) (*manager.IncomingConnections, string, error) {
	var (
		paginationCursor *models.QueryPaginationCursor
		paginationLimit  *models.QueryPaginationLimit
	)

	if args == nil {
		args = &manager.ListIncomingConnectionsArgs{}
	}

	if args.Pagination == nil {
		args.Pagination = &manager.ListIncomingConnectionsPagination{}
	}

	if args.Pagination.Cursor != "" {
		paginationCursor = &args.Pagination.Cursor
	}

	if args.Pagination.Limit != 0 {
		paginationLimit = &args.Pagination.Limit
	}

	var serviceNameFilter *[]models.ServiceName

	if len(args.ServiceNames) > 0 {
		var filter []models.ServiceName

		filter = append(filter, args.ServiceNames...)

		serviceNameFilter = &filter
	}

	includeCount := true // Hardcoded to true, if there is a toggle required in the UI this needs to change.

	sortOrder := convertSortOrder(args.Pagination.SortOrder)

	states := getContractStates(args.ContractStates)

	res, err := m.client.ListIncomingConnectionsWithResponse(ctx, &models.ListIncomingConnectionsParams{
		Cursor:         paginationCursor,
		Limit:          paginationLimit,
		SortOrder:      &sortOrder,
		IncludeCount:   &includeCount,
		ContractStates: &states,
		ServiceNames:   serviceNameFilter,
	})
	if err != nil {
		return nil, "", errors.Join(err, errors.New("could not list incoming connections from rest manager"))
	}

	err = mapResponse(res, http.StatusOK, "could not get incoming connections", res.Body)
	if err != nil {
		if res.StatusCode() == http.StatusInternalServerError {
			m.logger.Error(fmt.Sprintf("failed to get incoming connections: %s", res.ApplicationproblemJSON500.Details), nil)
		}

		return nil, "", err
	}

	connections := mapConnectionsToResponse(res.JSON200.Connections)

	totalCount := 0
	if res.JSON200.TotalCount != nil {
		totalCount = *res.JSON200.TotalCount
	}

	incomingConnections := &manager.IncomingConnections{
		TotalCount:  totalCount,
		Connections: connections,
	}

	return incomingConnections, res.JSON200.Pagination.NextCursor, nil
}
