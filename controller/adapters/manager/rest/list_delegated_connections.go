// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListOutgoingConnections
package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) ListDelegatedConnections(ctx context.Context, args *manager.ListDelegatedConnectionsArgs) (*manager.DelegatedConnections, string, error) {
	var (
		paginationCursor    *models.QueryPaginationCursor
		paginationLimit     *models.QueryPaginationLimit
		paginationSortOrder models.QueryPaginationOrder
	)

	if args == nil {
		args = &manager.ListDelegatedConnectionsArgs{}
	}

	if args.Pagination == nil {
		args.Pagination = &manager.ListDelegatedConnectionsPagination{}
	}

	if args.Pagination.Cursor != "" {
		paginationCursor = &args.Pagination.Cursor
	}

	if args.Pagination.Limit != 0 {
		paginationLimit = &args.Pagination.Limit
	}

	if args.Pagination.SortOrder != 0 {
		paginationSortOrder = convertSortOrder(args.Pagination.SortOrder)
	}

	includeCount := true // Hardcoded to true, if there is a toggle required in the UI this needs to change.

	states := getContractStates(args.ContractStates)

	res, err := m.client.ListDelegatedConnectionsWithResponse(ctx, &models.ListDelegatedConnectionsParams{
		Cursor:         paginationCursor,
		Limit:          paginationLimit,
		SortOrder:      &paginationSortOrder,
		IncludeCount:   &includeCount,
		ContractStates: &states,
	})
	if err != nil {
		return nil, "", errors.Join(err, errors.New("could not list delegated connections from rest manager"))
	}

	err = mapResponse(res, http.StatusOK, "could not get delegated connections", res.Body)
	if err != nil {
		if res.StatusCode() == http.StatusInternalServerError {
			m.logger.Error(fmt.Sprintf("failed to get delegated connections: %s", res.ApplicationproblemJSON500.Details), nil)
		}

		return nil, "", err
	}

	connections := mapConnectionsToResponse(res.JSON200.Connections)

	totalCount := 0
	if res.JSON200.TotalCount != nil {
		totalCount = *res.JSON200.TotalCount
	}

	incomingConnections := &manager.DelegatedConnections{
		TotalCount:  totalCount,
		Connections: connections,
	}

	return incomingConnections, res.JSON200.Pagination.NextCursor, nil
}
