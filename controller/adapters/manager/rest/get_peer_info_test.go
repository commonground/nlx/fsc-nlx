// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func TestGetPeerInfo(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		setup   func(context.Context, *mocks)
		want    *manager.Peer
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().GetPeerInfoWithResponse(ctx).Return(&api.GetPeerInfoResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Peer models.Peer `json:"peer"`
					}{
						Peer: models.Peer{
							Id:             "00000000000000000001",
							ManagerAddress: "https://example.com:8443",
							Name:           "test-peer",
						},
					},
				}, nil)
			},
			want: &manager.Peer{
				ID:             "00000000000000000001",
				Name:           "test-peer",
				ManagerAddress: "https://example.com:8443",
			},
		},
		"invalid_status_code": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().GetPeerInfoWithResponse(ctx).Return(&api.GetPeerInfoResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
					ApplicationproblemJSON500: &models.N500InternalServerError{
						Details:  "test error",
						Instance: "test instance",
						Status:   http.StatusInternalServerError,
						Title:    "test error",
						Type:     "test instance",
					},
				}, nil)
			},
			wantErr: errors.New("could not get peer info: internal error"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			tc.setup(ctx, m)

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			actual, err := restManager.GetPeerInfo(ctx)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, actual)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
