// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same but is different from ListIncomingConnections
package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) ListOutgoingConnectionsForService(ctx context.Context, args *manager.ListOutgoingConnectionsForServiceArgs) ([]*manager.Connection, error) {
	if args == nil {
		args = &manager.ListOutgoingConnectionsForServiceArgs{}
	}

	sortOrder := convertSortOrder(args.SortOrder)

	res, err := m.client.ListOutgoingConnectionsForServiceWithResponse(ctx, args.PeerID, args.ServiceName, &models.ListOutgoingConnectionsForServiceParams{
		SortOrder: &sortOrder,
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not list outgoing connections for service from rest manager"))
	}

	err = mapResponse(res, http.StatusOK, "could not list outgoing connections for service", res.Body)
	if err != nil {
		if res.StatusCode() == http.StatusInternalServerError {
			m.logger.Error(fmt.Sprintf("failed to list outgoing connections for service: %s", res.ApplicationproblemJSON500.Details), nil)
		}

		return nil, err
	}

	return mapConnectionsToResponse(res.JSON200.Connections), nil
}
