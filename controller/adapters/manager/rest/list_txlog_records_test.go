// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

//nolint:funlen,dupl,errcheck,stylecheck // this is a table test
func TestListTXLogRecords(t *testing.T) {
	t.Parallel()

	mockClock := testingutils.NewMockClock(time.Now())

	testCases := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *manager.ListTXLogRecordsRequest
		want    []*manager.TXLogRecord
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, m *mocks) {
				destination := models.LogRecord_Destination{}
				d := models.Destination{
					ServicePeerId: "2",
					Type:          models.TYPEDESTINATION,
				}
				destination.FromDestination(d)

				source := models.LogRecord_Source{}
				s := models.Source{
					OutwayPeerId: "1",
					Type:         models.TYPESOURCE,
				}
				source.FromSource(s)

				m.client.EXPECT().GetLogsWithResponse(ctx, &models.GetLogsParams{}).Return(&api.GetLogsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination models.PaginationResult `json:"pagination"`
						Records    []models.LogRecord      `json:"records"`
					}{Pagination: models.PaginationResult{NextCursor: "test-cursor"}, Records: []models.LogRecord{
						{
							CreatedAt:     mockClock.Now().Unix(),
							Destination:   destination,
							Direction:     models.DIRECTIONINCOMING,
							GrantHash:     "test-granthash",
							ServiceName:   "test-service",
							Source:        source,
							TransactionId: "test-transactionID",
						},
					}},
				}, nil)
			},
			want: []*manager.TXLogRecord{
				{
					TransactionID: "test-transactionID",
					GrantHash:     "test-granthash",
					ServiceName:   "test-service",
					Direction:     record.DirectionIn,
					Source:        &manager.TXLogRecordSource{OutwayPeerID: "1"},
					Destination:   &manager.TXLogRecordDestination{ServicePeerID: "2"},
					CreatedAt:     mockClock.Now().Truncate(time.Second),
				},
			},
		},
		"happy_flow_multiple_records": {
			setup: func(ctx context.Context, m *mocks) {
				destination := models.LogRecord_Destination{}
				d := models.Destination{
					ServicePeerId: "2",
					Type:          models.TYPEDESTINATION,
				}
				destination.FromDestination(d)

				source := models.LogRecord_Source{}
				s := models.Source{
					OutwayPeerId: "1",
					Type:         models.TYPESOURCE,
				}
				source.FromSource(s)

				destinationDelegated := models.LogRecord_Destination{}
				destDelegated := models.DestinationDelegated{
					ServicePeerId:   "2",
					Type:            models.TYPEDELEGATEDDESTINATION,
					DelegatorPeerId: "3",
				}
				destinationDelegated.FromDestinationDelegated(destDelegated)

				sourceDelegated := models.LogRecord_Source{}
				sDelegated := models.SourceDelegated{
					OutwayPeerId:    "1",
					Type:            models.TYPEDELEGATEDSOURCE,
					DelegatorPeerId: "4",
				}
				sourceDelegated.FromSourceDelegated(sDelegated)

				m.client.EXPECT().GetLogsWithResponse(ctx, &models.GetLogsParams{}).Return(&api.GetLogsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination models.PaginationResult `json:"pagination"`
						Records    []models.LogRecord      `json:"records"`
					}{Pagination: models.PaginationResult{NextCursor: "test-cursor"}, Records: []models.LogRecord{
						{
							CreatedAt:     mockClock.Now().Unix(),
							Destination:   destination,
							Direction:     models.DIRECTIONINCOMING,
							GrantHash:     "test-granthash",
							ServiceName:   "test-service",
							Source:        source,
							TransactionId: "test-transactionID",
						},
						{
							CreatedAt:     mockClock.Now().Unix(),
							Destination:   destinationDelegated,
							Direction:     models.DIRECTIONOUTGOING,
							GrantHash:     "test-granthash",
							ServiceName:   "test-service",
							Source:        sourceDelegated,
							TransactionId: "test-transactionID",
						},
					}},
				}, nil)
			},
			want: []*manager.TXLogRecord{
				{
					TransactionID: "test-transactionID",
					GrantHash:     "test-granthash",
					ServiceName:   "test-service",
					Direction:     record.DirectionIn,
					Source:        &manager.TXLogRecordSource{OutwayPeerID: "1"},
					Destination:   &manager.TXLogRecordDestination{ServicePeerID: "2"},
					CreatedAt:     mockClock.Now().Truncate(time.Second),
				},
				{
					TransactionID: "test-transactionID",
					GrantHash:     "test-granthash",
					ServiceName:   "test-service",
					Direction:     record.DirectionOut,
					Source:        &manager.TXLogRecordDelegatedSource{OutwayPeerID: "1", DelegatorPeerID: "4"},
					Destination:   &manager.TXLogRecordDelegatedDestination{ServicePeerID: "2", DelegatorPeerID: "3"},
					CreatedAt:     mockClock.Now().Truncate(time.Second),
				},
			},
		},
		"happy_flow_with_filters": {
			setup: func(ctx context.Context, m *mocks) {
				destination := models.LogRecord_Destination{}
				d := models.Destination{
					ServicePeerId: "2",
					Type:          models.TYPEDESTINATION,
				}
				destination.FromDestination(d)

				source := models.LogRecord_Source{}
				s := models.Source{
					OutwayPeerId: "1",
					Type:         models.TYPESOURCE,
				}
				source.FromSource(s)

				m.client.EXPECT().GetLogsWithResponse(ctx, &models.GetLogsParams{
					Cursor:           func() *string { cursor := "test-start-id"; return &cursor }(),
					Limit:            func() *models.QueryPaginationLimit { limit := models.QueryPaginationLimit(10); return &limit }(),
					SortOrder:        func() *models.SortOrder { order := models.SORTORDERDESCENDING; return &order }(),
					DataSourcePeerId: func() *string { sourcePeerId := "1"; return &sourcePeerId }(),
				}).Return(&api.GetLogsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Pagination models.PaginationResult `json:"pagination"`
						Records    []models.LogRecord      `json:"records"`
					}{Pagination: models.PaginationResult{NextCursor: "test-cursor"}, Records: []models.LogRecord{
						{
							CreatedAt:     mockClock.Now().Unix(),
							Destination:   destination,
							Direction:     models.DIRECTIONINCOMING,
							GrantHash:     "test-granthash",
							ServiceName:   "test-service",
							Source:        source,
							TransactionId: "test-transactionID",
						},
					}},
				}, nil)
			},
			want: []*manager.TXLogRecord{
				{
					TransactionID: "test-transactionID",
					GrantHash:     "test-granthash",
					ServiceName:   "test-service",
					Direction:     record.DirectionIn,
					Source:        &manager.TXLogRecordSource{OutwayPeerID: "1"},
					Destination:   &manager.TXLogRecordDestination{ServicePeerID: "2"},
					CreatedAt:     mockClock.Now().Truncate(time.Second),
				},
			},
			args: &manager.ListTXLogRecordsRequest{
				Pagination: &manager.Pagination{
					StartID:   "test-start-id",
					Limit:     10,
					SortOrder: manager.SortOrderDescending,
				},
				DataSourcePeerID: "1",
			},
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().GetLogsWithResponse(ctx, &models.GetLogsParams{}).Return(nil, errors.New("test error"))
			},
			wantErr: errors.New("could not get tx log records from rest manager: internal error: test error"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().GetLogsWithResponse(ctx, &models.GetLogsParams{}).Return(&api.GetLogsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
					ApplicationproblemJSON500: &models.N500InternalServerError{
						Details:  "test error",
						Instance: "test instance",
						Status:   http.StatusInternalServerError,
						Title:    "test error",
						Type:     "test instance",
					},
				}, nil)
			},
			wantErr: errors.New("could not get tx log records: internal error"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			tc.setup(ctx, m)

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			actual, err := restManager.ListTXLogRecords(ctx, tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, actual)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
