// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) UpdatePeer(ctx context.Context, args *manager.UpdatePeerArgs) error {
	if args == nil {
		return errors.New("no peer provided")
	}

	roles := convertPeerRole(args.Roles)

	res, err := m.client.UpdatePeerWithResponse(ctx, &models.UpdatePeerParams{
		AuditlogCorrelationId: &args.AuditlogCorrelationID,
	},
		models.UpdatePeerJSONRequestBody{
			Peer: models.Peer{
				Id:             args.PeerID,
				ManagerAddress: args.ManagerAddress,
				Name:           args.PeerName,
				Roles:          roles,
			},
		})
	if err != nil {
		return mapError(err, "could not update peer in rest manager")
	}

	return mapResponse(res, http.StatusNoContent, "could not update peer", res.Body)
}
