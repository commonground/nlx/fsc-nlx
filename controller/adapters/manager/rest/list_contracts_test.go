// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager_test

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:funlen,dupl,goconst // table tests are long and share similar setup
func TestListContracts(t *testing.T) {
	t.Parallel()

	mockClock := testingutils.NewMockClock(time.Now())

	testCases := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *manager.ListContractsArgs
		wantErr error
		want    manager.Contracts
	}{
		"happy_flow_service_connection_grants": {
			setup: func(ctx context.Context, m *mocks) {
				scg := createServiceConnectionGrant()
				scgDelegatedService := createServiceConnectionGrantDelegatedService()
				dscg := createDelegatedServiceConnectionGrant()
				grants := []models.Grant{scg, scgDelegatedService, dscg}
				var contentHash []models.ContentHash
				m.client.EXPECT().GetContractsWithResponse(ctx, &models.GetContractsParams{
					GrantType:   &[]models.GrantType{},
					ContentHash: &contentHash,
				}).Return(&api.GetContractsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Contracts  []models.Contract       `json:"contracts"`
						Pagination models.PaginationResult `json:"pagination"`
					}{Contracts: []models.Contract{
						{
							Content: models.ContractContent{
								CreatedAt:     mockClock.Now().Unix(),
								Grants:        grants,
								GroupId:       "test-group",
								HashAlgorithm: models.HASHALGORITHMSHA3512,
								Iv:            "test-iv",
								Validity: models.Validity{
									NotAfter:  mockClock.Now().Add(1 * time.Hour).Unix(),
									NotBefore: mockClock.Now().Unix(),
								},
							},
							HasAccepted: false,
							HasRejected: false,
							HasRevoked:  false,
							Hash:        "test-hash",
							Peers: []models.PeerID{
								strconv.Itoa(1), strconv.Itoa(2),
							},
							Signatures: models.Signatures{
								Accept: models.SignatureMap{
									"1": models.Signature{
										Peer: models.Peer{
											Id: "1",
										},
										SignedAt: mockClock.Now().Unix(),
									},
								},
							},
							State: models.CONTRACTSTATEPROPOSED,
						},
					}, Pagination: models.PaginationResult{NextCursor: "test-cursor"},
					}}, nil)
			},
			want: manager.Contracts{
				{
					IV:            "test-iv",
					Hash:          "test-hash",
					HashAlgorithm: manager.HashAlgSHA3_512,
					GroupID:       "test-group",
					CreatedAt:     mockClock.Now().Truncate(time.Second),
					Peers:         []string{"1", "2"},
					AcceptSignatures: map[string]manager.Signature{
						"1": {
							SignedAt: mockClock.Now().Truncate(time.Second),
						},
					},
					RejectSignatures:         map[string]manager.Signature{},
					RevokeSignatures:         map[string]manager.Signature{},
					ValidFrom:                mockClock.Now().Truncate(time.Second),
					ValidUntil:               mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
					HasRejected:              false,
					HasAccepted:              false,
					HasRevoked:               false,
					ServicePublicationGrants: []*manager.ServicePublicationGrant{},
					ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
						{
							Hash:                              "",
							ServicePeerID:                     "1",
							ServicePublicationDelegatorPeerID: "",
							ServiceName:                       "test-service",
							OutwayPeerID:                      "1",
							OutwayPublicKeyThumbprint:         "test-thumbprint",
						},
						{
							Hash:                              "",
							ServicePeerID:                     "1",
							ServicePublicationDelegatorPeerID: "2",
							ServiceName:                       "test-service",
							OutwayPeerID:                      "1",
							OutwayPublicKeyThumbprint:         "test-thumbprint",
						},
					},
					DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
						{
							Hash:                              "",
							DelegatorPeerID:                   "1",
							ServicePeerID:                     "1",
							ServiceName:                       "test-service",
							ServicePublicationDelegatorPeerID: "",
							OutwayPeerID:                      "1",
							OutwayPublicKeyThumbprint:         "test-thumbprint",
						},
					},
					State: manager.ContractStateProposed,
				},
			},
		},
		"happy_flow_service_publication_grants": {
			setup: func(ctx context.Context, m *mocks) {
				spg := createServicePublicationGrant()
				dspg := createDelegatedServicePublicationGrant()
				grants := []models.Grant{spg, dspg}
				var contentHash []models.ContentHash
				m.client.EXPECT().GetContractsWithResponse(ctx, &models.GetContractsParams{
					GrantType:   &[]models.GrantType{},
					ContentHash: &contentHash,
				}).Return(&api.GetContractsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Contracts  []models.Contract       `json:"contracts"`
						Pagination models.PaginationResult `json:"pagination"`
					}{Contracts: []models.Contract{
						{
							Content: models.ContractContent{
								CreatedAt:     mockClock.Now().Unix(),
								Grants:        grants,
								GroupId:       "test-group",
								HashAlgorithm: models.HASHALGORITHMSHA3512,
								Iv:            "test-iv",
								Validity: models.Validity{
									NotAfter:  mockClock.Now().Add(1 * time.Hour).Unix(),
									NotBefore: mockClock.Now().Unix(),
								},
							},
							HasAccepted: false,
							HasRejected: false,
							HasRevoked:  false,
							Hash:        "test-hash",
							Peers: []models.PeerID{
								strconv.Itoa(1), strconv.Itoa(2),
							},
							Signatures: models.Signatures{
								Accept: models.SignatureMap{
									"1": models.Signature{
										Peer: models.Peer{
											Id: "1",
										},
										SignedAt: mockClock.Now().Unix(),
									},
								},
							},
							State: models.CONTRACTSTATEPROPOSED,
						},
					}, Pagination: models.PaginationResult{NextCursor: "test-cursor"},
					}}, nil)
			},
			want: manager.Contracts{
				{
					IV:            "test-iv",
					Hash:          "test-hash",
					HashAlgorithm: manager.HashAlgSHA3_512,
					GroupID:       "test-group",
					CreatedAt:     mockClock.Now().Truncate(time.Second),
					Peers:         []string{"1", "2"},
					AcceptSignatures: map[string]manager.Signature{
						"1": {
							SignedAt: mockClock.Now().Truncate(time.Second),
						},
					},
					RejectSignatures: map[string]manager.Signature{},
					RevokeSignatures: map[string]manager.Signature{},
					ValidFrom:        mockClock.Now().Truncate(time.Second),
					ValidUntil:       mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
					HasRejected:      false,
					HasAccepted:      false,
					HasRevoked:       false,
					ServicePublicationGrants: []*manager.ServicePublicationGrant{
						{
							Hash:            "",
							DirectoryPeerID: "1",
							ServicePeerID:   "1",
							ServiceName:     "test-service",
							ServiceProtocol: "",
						},
					},
					ServiceConnectionGrants: []*manager.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{
						{
							Hash:            "",
							DirectoryPeerID: "1",
							DelegatorPeerID: "2",
							ServicePeerID:   "1",
							ServiceName:     "test-service",
							ServiceProtocol: "",
						},
					},
					DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{},
					State:                            manager.ContractStateProposed,
				},
			},
		},
		"happy_flow_params_multiple_contracts": {
			setup: func(ctx context.Context, m *mocks) {
				scg := createServiceConnectionGrant()
				scgDelegatedService := createServiceConnectionGrantDelegatedService()
				dscg := createDelegatedServiceConnectionGrant()
				grantsContract1 := []models.Grant{scg, scgDelegatedService, dscg}

				spg := createServicePublicationGrant()
				dspg := createDelegatedServicePublicationGrant()
				grantsContract2 := []models.Grant{spg, dspg}

				limit := models.QueryPaginationLimit(10)
				grantTypesFilter := []models.GrantType{
					models.GRANTTYPEDELEGATEDSERVICECONNECTION,
					models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
					models.GRANTTYPESERVICECONNECTION,
					models.GRANTTYPESERVICEPUBLICATION,
				}
				contentHash := []models.ContentHash{"test-hash", "test-hash2"}
				cursor := "test-cursor"

				m.client.EXPECT().GetContractsWithResponse(ctx, &models.GetContractsParams{
					Limit:       &limit,
					GrantType:   &grantTypesFilter,
					ContentHash: &contentHash,
					GrantHash:   nil,
					Cursor:      &cursor,
				}).Return(&api.GetContractsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Contracts  []models.Contract       `json:"contracts"`
						Pagination models.PaginationResult `json:"pagination"`
					}{Contracts: []models.Contract{
						{
							Content: models.ContractContent{
								CreatedAt:     mockClock.Now().Unix(),
								Grants:        grantsContract1,
								GroupId:       "test-group",
								HashAlgorithm: models.HASHALGORITHMSHA3512,
								Iv:            "test-iv",
								Validity: models.Validity{
									NotAfter:  mockClock.Now().Add(1 * time.Hour).Unix(),
									NotBefore: mockClock.Now().Unix(),
								},
							},
							HasAccepted: false,
							HasRejected: false,
							HasRevoked:  false,
							Hash:        "test-hash",
							Peers: []models.PeerID{
								strconv.Itoa(1), strconv.Itoa(2),
							},
							Signatures: models.Signatures{
								Accept: models.SignatureMap{
									"1": models.Signature{
										Peer: models.Peer{
											Id: "1",
										},
										SignedAt: mockClock.Now().Unix(),
									},
								},
							},
							State: models.CONTRACTSTATEPROPOSED,
						},
						{
							Content: models.ContractContent{
								CreatedAt:     mockClock.Now().Unix(),
								Grants:        grantsContract2,
								GroupId:       "test-group",
								HashAlgorithm: models.HASHALGORITHMSHA3512,
								Iv:            "test-iv2",
								Validity: models.Validity{
									NotAfter:  mockClock.Now().Add(1 * time.Hour).Unix(),
									NotBefore: mockClock.Now().Unix(),
								},
							},
							HasAccepted: false,
							HasRejected: false,
							HasRevoked:  false,
							Hash:        "test-hash2",
							Peers: []models.PeerID{
								strconv.Itoa(1), strconv.Itoa(2),
							},
							Signatures: models.Signatures{
								Accept: models.SignatureMap{
									"1": models.Signature{
										Peer: models.Peer{
											Id: "1",
										},
										SignedAt: mockClock.Now().Unix(),
									},
								},
								Reject: models.SignatureMap{
									"2": models.Signature{
										Peer: models.Peer{
											Id: "2",
										},
										SignedAt: mockClock.Now().Unix(),
									},
								},
							},
							State: models.CONTRACTSTATEREJECTED,
						},
					}, Pagination: models.PaginationResult{NextCursor: "test-cursor"},
					}}, nil)
			},
			args: &manager.ListContractsArgs{
				Pagination: &manager.ContractsPagination{
					Cursor: "test-cursor",
					Limit:  10,
				},
				Filters: &manager.ContractsFilters{
					ContentHashes: []string{"test-hash", "test-hash2"},
					GrantTypes: []manager.GrantType{
						manager.GrantTypeDelegatedServiceConnection,
						manager.GrantTypeDelegatedServicePublication,
						manager.GrantTypeServiceConnection,
						manager.GrantTypeServicePublication,
					},
				},
			},
			want: manager.Contracts{
				{
					IV:            "test-iv",
					Hash:          "test-hash",
					HashAlgorithm: manager.HashAlgSHA3_512,
					GroupID:       "test-group",
					CreatedAt:     mockClock.Now().Truncate(time.Second),
					Peers:         []string{"1", "2"},
					AcceptSignatures: map[string]manager.Signature{
						"1": {
							SignedAt: mockClock.Now().Truncate(time.Second),
						},
					},
					RejectSignatures:         map[string]manager.Signature{},
					RevokeSignatures:         map[string]manager.Signature{},
					ValidFrom:                mockClock.Now().Truncate(time.Second),
					ValidUntil:               mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
					HasRejected:              false,
					HasAccepted:              false,
					HasRevoked:               false,
					ServicePublicationGrants: []*manager.ServicePublicationGrant{},
					ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
						{
							Hash:                              "",
							ServicePeerID:                     "1",
							ServicePublicationDelegatorPeerID: "",
							ServiceName:                       "test-service",
							OutwayPeerID:                      "1",
							OutwayPublicKeyThumbprint:         "test-thumbprint",
						},
						{
							Hash:                              "",
							ServicePeerID:                     "1",
							ServicePublicationDelegatorPeerID: "2",
							ServiceName:                       "test-service",
							OutwayPeerID:                      "1",
							OutwayPublicKeyThumbprint:         "test-thumbprint",
						},
					},
					DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
						{
							Hash:                              "",
							DelegatorPeerID:                   "1",
							ServicePeerID:                     "1",
							ServiceName:                       "test-service",
							ServicePublicationDelegatorPeerID: "",
							OutwayPeerID:                      "1",
							OutwayPublicKeyThumbprint:         "test-thumbprint",
						},
					},
					State: manager.ContractStateProposed,
				},
				{
					IV:            "test-iv2",
					Hash:          "test-hash2",
					HashAlgorithm: manager.HashAlgSHA3_512,
					GroupID:       "test-group",
					CreatedAt:     mockClock.Now().Truncate(time.Second),
					Peers:         []string{"1", "2"},
					AcceptSignatures: map[string]manager.Signature{
						"1": {
							SignedAt: mockClock.Now().Truncate(time.Second),
						},
					},
					RejectSignatures: map[string]manager.Signature{
						"2": {
							SignedAt: mockClock.Now().Truncate(time.Second),
						},
					},
					RevokeSignatures: map[string]manager.Signature{},
					ValidFrom:        mockClock.Now().Truncate(time.Second),
					ValidUntil:       mockClock.Now().Add(1 * time.Hour).Truncate(time.Second),
					HasRejected:      false,
					HasAccepted:      false,
					HasRevoked:       false,
					ServicePublicationGrants: []*manager.ServicePublicationGrant{
						{
							Hash:            "",
							DirectoryPeerID: "1",
							ServicePeerID:   "1",
							ServiceName:     "test-service",
							ServiceProtocol: "",
						},
					},
					ServiceConnectionGrants: []*manager.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{
						{
							Hash:            "",
							DirectoryPeerID: "1",
							DelegatorPeerID: "2",
							ServicePeerID:   "1",
							ServiceName:     "test-service",
							ServiceProtocol: "",
						},
					},
					DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{},
					State:                            manager.ContractStateRejected,
				},
			},
		},
		"error_response": {
			setup: func(ctx context.Context, m *mocks) {
				var contentHash []models.ContentHash
				m.client.EXPECT().GetContractsWithResponse(ctx, &models.GetContractsParams{
					GrantType:   &[]models.GrantType{},
					ContentHash: &contentHash,
				}).Return(nil, errors.New("test error"))
			},
			wantErr: errors.New("test error\ncould not get contracts from rest manager"),
		},
		"invalid_response": {
			setup: func(ctx context.Context, m *mocks) {
				var contentHash []models.ContentHash
				m.client.EXPECT().GetContractsWithResponse(ctx, &models.GetContractsParams{
					GrantType:   &[]models.GrantType{},
					ContentHash: &contentHash,
				}).Return(&api.GetContractsResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusServiceUnavailable},
					ApplicationproblemJSON503: &models.N503ServiceUnavailable{
						Details:  "test-error",
						Instance: "test-error",
						Status:   http.StatusServiceUnavailable,
						Title:    "test-error",
						Type:     "test-error",
					},
				}, nil)
			},
			wantErr: errors.New("could not get contracts: internal error"),
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)
			ctx := context.Background()

			if tc.setup != nil {
				tc.setup(ctx, m)
			}

			restManager, err := restmanager.New(m.client, m.lgr)
			require.NoError(t, err)

			contentHash, _, err := restManager.ListContracts(ctx, tc.args)

			if tc.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tc.want, contentHash)
			} else {
				assert.EqualError(t, err, tc.wantErr.Error())
			}
		})
	}
}
