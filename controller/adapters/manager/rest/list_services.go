// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) ListServices(ctx context.Context, args *manager.ListServicesArgs) (manager.Services, error) {
	if args == nil || args.DirectoryPeerID == "" {
		return nil, errors.New("directoryPeerID must be provided")
	}

	res, err := m.client.ListServicesWithResponse(ctx, args.DirectoryPeerID, &models.ListServicesParams{
		PeerId: args.ServiceProviderPeerIDFilter,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not list services from rest manager")
	}

	err = mapResponse(res, http.StatusOK, "could not get services", res.Body)
	if err != nil {
		return nil, err
	}

	services := make(manager.Services, len(res.JSON200.Services))

	for i, s := range res.JSON200.Services {
		t, err := s.ValueByDiscriminator()
		if err != nil {
			return nil, err
		}

		switch convertedModel := t.(type) {
		case models.DelegatedService:
			services[i] = &manager.DelegatedService{
				Name:             convertedModel.Name,
				ProviderPeerID:   convertedModel.Peer.Id,
				ProviderPeerName: convertedModel.Peer.Name,
				DelegateeID:      convertedModel.Delegator.Id,
				DelegateeName:    convertedModel.Delegator.Name,
			}
		case models.Service:
			services[i] = &manager.Service{
				Name:     convertedModel.Name,
				PeerID:   convertedModel.Peer.Id,
				PeerName: convertedModel.Peer.Name,
			}
		}
	}

	return services, nil
}
