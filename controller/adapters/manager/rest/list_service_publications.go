// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) ListServicePublications(ctx context.Context, args *manager.ListServicePublicationsArgs) (*manager.Publications, string, error) {
	var (
		paginationCursor *models.QueryPaginationCursor
		paginationLimit  *models.QueryPaginationLimit
	)

	if args == nil {
		args = &manager.ListServicePublicationsArgs{}
	}

	if args.Pagination == nil {
		args.Pagination = &manager.Pagination{}
	}

	if args.Pagination.StartID != "" {
		paginationCursor = &args.Pagination.StartID
	}

	if args.Pagination.Limit != 0 {
		paginationLimit = &args.Pagination.Limit
	}

	var serviceNameFilter *[]models.ServiceName

	if len(args.ServiceNames) > 0 {
		var filter []models.ServiceName

		filter = append(filter, args.ServiceNames...)

		serviceNameFilter = &filter
	}

	sortOrder := convertSortOrder(args.Pagination.SortOrder)

	states := getContractStates(args.ContractStates)

	includeCount := true // Hardcoded to true, if there is a toggle required in the UI this needs to change.

	resp, err := m.client.ListServicePublicationsWithResponse(ctx, &models.ListServicePublicationsParams{
		Cursor:         paginationCursor,
		Limit:          paginationLimit,
		SortOrder:      &sortOrder,
		IncludeCount:   &includeCount,
		ServiceNames:   serviceNameFilter,
		ContractStates: &states,
	})
	if err != nil {
		return nil, "", errors.Join(errors.New("could not list service publications from manager"), err)
	}

	err = mapResponse(resp, http.StatusOK, "could not list service publications", resp.Body)
	if err != nil {
		if resp.StatusCode() == http.StatusInternalServerError {
			m.logger.Error(fmt.Sprintf("failed to list service publications: %s", resp.ApplicationproblemJSON500.Details), nil)
		}

		return nil, "", err
	}

	managerPublications := resp.JSON200.Publications
	publications := convertPublications(managerPublications)

	totalCount := 0
	if resp.JSON200.TotalCount != nil {
		totalCount = *resp.JSON200.TotalCount
	}

	publicationsRes := &manager.Publications{
		TotalCount:   totalCount,
		Publications: publications,
	}

	return publicationsRes, resp.JSON200.Pagination.NextCursor, nil
}

func convertPublications(managerPublications models.Publications) []*manager.Publication {
	publications := make([]*manager.Publication, len(managerPublications))

	for i, publication := range managerPublications {
		pub := &manager.Publication{
			DirectoryPeerID: publication.DirectoryPeerId,
			ServicePeerID:   publication.ServicePeerId,
			ServiceName:     publication.ServiceName,
			ContentHash:     publication.ContractHash,
			CreatedAt:       time.Unix(publication.CreatedAt, 0),
			ValidNotBefore:  time.Unix(publication.NotBefore, 0),
			ValidNotAfter:   time.Unix(publication.NotAfter, 0),
			State:           convertState(publication.State),
			GrantHash:       publication.GrantHash,
		}

		if publication.DelegatorPeerId != nil {
			pub.DelegatorPeerID = *publication.DelegatorPeerId
		}

		publications[i] = pub
	}

	return publications
}

func convertSortOrder(order manager.SortOrder) models.QueryPaginationOrder {
	switch order {
	case manager.SortOrderAscending:
		return models.SORTORDERASCENDING
	case manager.SortOrderDescending:
		return models.SORTORDERDESCENDING
	default:
		return models.SORTORDERASCENDING
	}
}
