// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package restmanager

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
)

func (m *restManager) SyncPeers(ctx context.Context) (map[string]manager.SynchronizationResult, error) {
	res, err := m.client.SyncPeersWithResponse(ctx)
	if err != nil {
		return nil, errors.Join(err, errors.New("failed to sync peers"))
	}

	if res.StatusCode() != http.StatusOK {
		m.logger.Error(fmt.Sprintf("received non-200 status code: %d, body: %s", res.StatusCode(), string(res.Body)), errors.New("failed to sync peers"))

		return nil, errors.New("failed to sync peers")
	}

	syncResult := make(map[string]manager.SynchronizationResult)

	for _, result := range res.JSON200.Result {
		var errSync error

		if result.Error != nil {
			errSync = errors.New(*result.Error)
		}

		syncResult[result.PeerId] = manager.SynchronizationResult{
			Ok:  result.Ok,
			Err: errSync,
		}
	}

	return syncResult, nil
}
