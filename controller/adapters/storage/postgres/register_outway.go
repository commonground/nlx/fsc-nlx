// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) RegisterOutway(ctx context.Context, groupID, name, publicKeyThumbprint string) error {
	err := p.queries.RegisterOutway(ctx, &queries.RegisterOutwayParams{
		GroupID:             groupID,
		Name:                name,
		PublicKeyThumbprint: publicKeyThumbprint,
	})
	if err != nil {
		return fmt.Errorf("could not register outway in database: %w", err)
	}

	return nil
}
