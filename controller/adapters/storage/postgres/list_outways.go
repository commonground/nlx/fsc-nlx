// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
)

func (p *PostgresStorage) ListOutways(ctx context.Context, groupID string) ([]*storage.Outway, error) {
	rows, err := p.queries.ListOutways(ctx, groupID)
	if err != nil {
		return nil, fmt.Errorf("could not list outways from database: %w", err)
	}

	outways := make([]*storage.Outway, len(rows))

	for i, outway := range rows {
		outways[i] = &storage.Outway{
			Name:                outway.Name,
			PublicKeyThumbprint: outway.PublicKeyThumbprint,
		}
	}

	return outways, nil
}
