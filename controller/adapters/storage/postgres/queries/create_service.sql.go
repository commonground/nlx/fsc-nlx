// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: create_service.sql

package queries

import (
	"context"
)

const createService = `-- name: CreateService :exec

INSERT INTO controller.services (
  group_id,
  name,
  endpoint_url,
  inway_address
)
VALUES ($1, $2, $3, $4)
`

type CreateServiceParams struct {
	GroupID      string
	Name         string
	EndpointUrl  string
	InwayAddress string
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
func (q *Queries) CreateService(ctx context.Context, arg *CreateServiceParams) error {
	_, err := q.db.Exec(ctx, createService,
		arg.GroupID,
		arg.Name,
		arg.EndpointUrl,
		arg.InwayAddress,
	)
	return err
}
