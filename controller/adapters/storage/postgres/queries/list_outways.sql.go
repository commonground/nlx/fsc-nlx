// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: list_outways.sql

package queries

import (
	"context"
)

const listOutways = `-- name: ListOutways :many

SELECT
    name,
    public_key_thumbprint
FROM
    controller.outways
WHERE
    group_id = $1
GROUP BY
    public_key_thumbprint, name
`

type ListOutwaysRow struct {
	Name                string
	PublicKeyThumbprint string
}

// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
func (q *Queries) ListOutways(ctx context.Context, groupID string) ([]*ListOutwaysRow, error) {
	rows, err := q.db.Query(ctx, listOutways, groupID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*ListOutwaysRow{}
	for rows.Next() {
		var i ListOutwaysRow
		if err := rows.Scan(&i.Name, &i.PublicKeyThumbprint); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
