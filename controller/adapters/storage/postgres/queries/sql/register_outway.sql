-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: RegisterOutway :exec
INSERT INTO controller.outways (
    group_id,
    name,
    public_key_thumbprint
)
VALUES ($1, $2, $3)
ON CONFLICT ON CONSTRAINT outways_pk
DO
   UPDATE SET public_key_thumbprint = EXCLUDED.public_key_thumbprint;
