-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: GetService :one
SELECT
    s.group_id,
    s.name,
    s.inway_address,
    s.endpoint_url
FROM
    controller.services as s
WHERE
    group_id = $1
    AND name = $2
    AND (sqlc.narg('inway_address')::text IS NULL OR inway_address = sqlc.narg('inway_address')::text);
