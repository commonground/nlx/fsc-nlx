-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: ListOutways :many
SELECT
    name,
    public_key_thumbprint
FROM
    controller.outways
WHERE
    group_id = $1
GROUP BY
    public_key_thumbprint, name;
