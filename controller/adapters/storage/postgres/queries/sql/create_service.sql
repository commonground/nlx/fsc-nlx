-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: CreateService :exec
INSERT INTO controller.services (
  group_id,
  name,
  endpoint_url,
  inway_address
)
VALUES ($1, $2, $3, $4);
