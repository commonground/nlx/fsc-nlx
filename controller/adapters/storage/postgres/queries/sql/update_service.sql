-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: UpdateService :one
UPDATE controller.services
SET endpoint_url=$3,
    inway_address=$4
WHERE group_id=$1
AND name=$2
RETURNING name;
