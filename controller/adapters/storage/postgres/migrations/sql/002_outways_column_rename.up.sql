-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN transaction;

ALTER TABLE controller.outways ALTER COLUMN certificate_thumbprint TYPE VARCHAR(64);
ALTER TABLE controller.outways RENAME COLUMN certificate_thumbprint TO public_key_thumbprint;

COMMIT;
