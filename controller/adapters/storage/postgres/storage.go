// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
	_ "github.com/lib/pq" // postgres driver

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres/migrations"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres/queries"
)

const (
	migratorDriverName = "embed"

	maxConnLifetime       = 5 * time.Minute
	maxConnLifetimeJitter = 1 * time.Minute
	maxConns              = 100
	minConns              = 5
	healthCheckPeriod     = 1 * time.Minute
)

var registerDriverOnce sync.Once

type DBConn interface {
	Begin(ctx context.Context) (pgx.Tx, error)
	Exec(ctx context.Context, sql string, arguments ...interface{}) (pgconn.CommandTag, error)
	Query(ctx context.Context, sql string, optionsAndArgs ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, optionsAndArgs ...interface{}) pgx.Row
	CopyFrom(ctx context.Context, tableName pgx.Identifier, columnNames []string, rowSrc pgx.CopyFromSource) (int64, error)
}

type PostgresStorage struct {
	db      DBConn
	queries *queries.Queries
}

func New(db DBConn) (*PostgresStorage, error) {
	if db == nil {
		return nil, fmt.Errorf("db is required")
	}

	querier := queries.New(db)

	return &PostgresStorage{
		db:      db,
		queries: querier,
	}, nil
}

func NewConnection(ctx context.Context, dsn string) (*pgxpool.Pool, error) {
	config, err := pgxpool.ParseConfig(dsn)
	if err != nil {
		return nil, fmt.Errorf("could not open connection to postgres: %s", err)
	}

	config.MaxConnLifetime = maxConnLifetime
	config.MaxConnLifetimeJitter = maxConnLifetimeJitter
	config.MaxConns = maxConns
	config.MinConns = minConns
	config.HealthCheckPeriod = healthCheckPeriod

	db, err := pgxpool.NewWithConfig(ctx, config)
	if err != nil {
		return nil, fmt.Errorf("could not open connection to postgres: %s", err)
	}

	return db, nil
}

func setupMigrator(dsn string) (*migrate.Migrate, error) {
	registerDriverOnce.Do(func() {
		migrations.RegisterDriver(migratorDriverName)
	})

	return migrate.New(fmt.Sprintf("%s://", migratorDriverName), dsn)
}

func PerformMigrations(dsn string) error {
	migrator, err := setupMigrator(dsn)
	if err != nil {
		return err
	}

	err = migrator.Up()
	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return fmt.Errorf("running migrations: %v", err)
	}

	return nil
}

func MigrationStatus(dsn string) (version uint, dirty bool, err error) {
	migrator, err := setupMigrator(dsn)
	if err != nil {
		return 0, false, err
	}

	return migrator.Version()
}
