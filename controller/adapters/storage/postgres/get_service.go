// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"errors"
	"fmt"
	"net/url"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) GetService(ctx context.Context, groupID, serviceName string) (*storage.Service, error) {
	service, err := p.queries.GetService(ctx, &queries.GetServiceParams{
		GroupID:      groupID,
		Name:         serviceName,
		InwayAddress: pgtype.Text{},
	})
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrServiceNotFound
		}

		return nil, fmt.Errorf("could not get service from database: %w", err)
	}

	endpointURL, err := url.Parse(service.EndpointUrl)
	if err != nil {
		return nil, fmt.Errorf("invalid endpoint URL in database: %w", err)
	}

	return &storage.Service{
		GroupID:      service.GroupID,
		Name:         service.Name,
		InwayAddress: service.InwayAddress,
		EndpointURL:  endpointURL,
	}, nil
}
