// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) CreateService(ctx context.Context, args *storage.CreateServiceArgs) error {
	err := p.queries.CreateService(ctx, &queries.CreateServiceParams{
		GroupID:      args.GroupID,
		Name:         args.Name,
		EndpointUrl:  args.EndpointURL,
		InwayAddress: args.InwayAddress,
	})
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint \"services_pk\"") {
			return storage.ErrServiceNameInUse
		}

		return fmt.Errorf("could not create service in database: %w", err)
	}

	return nil
}
