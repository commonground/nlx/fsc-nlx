// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package storage

import (
	"context"
	"errors"
	"net/url"
)

type Storage interface {
	// API
	ListServices(ctx context.Context, groupID string) (Services, error)
	CreateService(ctx context.Context, args *CreateServiceArgs) error
	DeleteService(ctx context.Context, args *DeleteServiceArgs) error
	UpdateService(ctx context.Context, args *UpdateServiceArgs) error
	ListInways(ctx context.Context, groupID string) ([]*Inway, error)
	ListOutways(ctx context.Context, groupID string) ([]*Outway, error)
	Ping(ctx context.Context) error

	// Internal
	RegisterInway(ctx context.Context, groupID, name string, address *url.URL) error
	RegisterOutway(ctx context.Context, groupID, name, publicKeyThumbprint string) error
	GetService(ctx context.Context, groupID, serviceName string) (*Service, error)
}

var (
	ErrServiceNotFound  = errors.New("service could not be found")
	ErrServiceNameInUse = errors.New("service name is already in use")
)

type Services []*Service

type Service struct {
	GroupID      string
	Name         string
	EndpointURL  *url.URL
	InwayAddress string
}

type Inway struct {
	Name    string
	Address string
}

type Outway struct {
	Name                string
	PublicKeyThumbprint string
}

type CreateServiceArgs struct {
	GroupID      string
	Name         string
	EndpointURL  string
	InwayAddress string
}

type DeleteServiceArgs struct {
	GroupID string
	Name    string
}

type UpdateServiceArgs struct {
	GroupID      string
	Name         string
	EndpointURL  string
	InwayAddress string
}
