// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package authz

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest/api/models"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type Action string

const (
	FscAcceptContract                   Action = Action(models.FscAcceptContract)
	FscCreateContract                   Action = Action(models.FscCreateContract)
	FscCreateService                    Action = Action(models.FscCreateService)
	FscDeleteService                    Action = Action(models.FscDeleteService)
	FscListContracts                    Action = Action(models.FscListContracts)
	FscListContractDistributions        Action = Action(models.FscListContractDistributions)
	FscGetContract                      Action = Action(models.FscGetContract)
	FscGetService                       Action = Action(models.FscGetService)
	FscListServices                     Action = Action(models.FscListServices)
	FscListPeerServices                 Action = Action(models.FscListPeerServices)
	FscListTransactionLogs              Action = Action(models.FscListTransactionLogs)
	FscListAuditlogs                    Action = Action(models.FscListAuditlogs)
	FscRejectContract                   Action = Action(models.FscRejectContract)
	FscRevokeContract                   Action = Action(models.FscRevokeContract)
	FscRetryContract                    Action = Action(models.FscRetryContract)
	FscUpdateService                    Action = Action(models.FscUpdateService)
	FscGetPeerInfo                      Action = Action(models.FscGetPeerInfo)
	FscListPeers                        Action = Action(models.FscListPeers)
	FscListInways                       Action = Action(models.FscListInways)
	FscListOutways                      Action = Action(models.FscListOutways)
	FscCreatePeer                       Action = Action(models.FscCreatePeer)
	FscUpdatePeer                       Action = Action(models.FscUpdatePeer)
	FscSyncPeers                        Action = Action(models.FscSyncPeers)
	FscListServicePublications                 = Action(models.FscListServicePublications)
	FscListDelegatedServicePublications        = Action(models.FscListDelegatedServicePublications)
	FscListDelegatedConnections                = Action(models.FscListDelegatedConnections)
)

type AuthorizationError struct {
	r string
}

func NewAuthorizationError(reason string) *AuthorizationError {
	return &AuthorizationError{
		r: reason,
	}
}

func (v *AuthorizationError) Error() string {
	return fmt.Sprintf("authorization error: %s", v.r)
}

type ResourceType string

const (
	ResourceTypeService              ResourceType = "urn:fsc:controller:service"
	ResourceTypeContract             ResourceType = "urn:fsc:contract"
	ResourceTypeContractDistribution ResourceType = "urn:fsc:contractDistributions"
	ResourceTypePeer                 ResourceType = "urn:fsc:peer"
	ResourceTypeTransactionLog       ResourceType = "urn:fsc:txlog"
	ResourceTypeAuditlog             ResourceType = "urn:fsc:auditlog"
	ResourceTypeInway                ResourceType = "urn:fsc:controller:inway"
	ResourceTypeOutway               ResourceType = "urn:fsc:controller:outway"
)

type ResourceURN struct {
	ID   string
	Type ResourceType
}

func (r *ResourceURN) String() string {
	return fmt.Sprintf("%s:%s", r.Type, r.ID)
}

func NewResourceURN(typ ResourceType, id string) *ResourceURN {
	return &ResourceURN{
		ID:   id,
		Type: typ,
	}
}

type Authorization interface {
	Authorize(ctx context.Context, authenticationData authentication.Data, metadata authorization.Metadata, action Action, resourceURNs []*ResourceURN) error
}
