// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest_test

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest"
	api "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest/api/client"
	mock_client "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest/api/client/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest/api/models"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

//nolint:funlen // table test results in long function
func TestAuthorize(t *testing.T) {
	t.Parallel()

	mockArgs := models.AuthorizationRequest{
		Input: models.AuthorizationRequestInput{
			Action: models.Action(authz.FscAcceptContract),
			Metadata: models.Metadata{
				Headers: &map[string][]string{
					"Test-Header":   {"test-value"},
					"Authorization": {"Bearer token"},
				},
			},
			Resources: []string{"urn:fsc:contract:1", "urn:fsc:contract:2"},
		},
	}

	tests := map[string]struct {
		setup   func(context.Context, *mock_client.MockClientWithResponsesInterface)
		wantErr error
	}{
		"when_server_returns_non_200": {
			setup: func(ctx context.Context, c *mock_client.MockClientWithResponsesInterface) {
				c.EXPECT().
					AuthorizeWithResponse(ctx, mockArgs).
					Return(&api.AuthorizeResponse{
						HTTPResponse: &http.Response{StatusCode: http.StatusInternalServerError},
					}, nil)
			},
			wantErr: &authz.AuthorizationError{},
		},
		"when_client_error": {
			setup: func(ctx context.Context, c *mock_client.MockClientWithResponsesInterface) {
				c.EXPECT().
					AuthorizeWithResponse(ctx, mockArgs).
					Return(nil, fmt.Errorf("some error"))
			},
			wantErr: &authz.AuthorizationError{},
		},
		"when_unauthorized": {
			setup: func(ctx context.Context, c *mock_client.MockClientWithResponsesInterface) {
				c.EXPECT().
					AuthorizeWithResponse(ctx, mockArgs).
					Return(&api.AuthorizeResponse{
						HTTPResponse: &http.Response{StatusCode: http.StatusOK},
						JSON200: &models.AuthorizationResponse{
							Allowed: false,
							Reason:  stringPointer("some reason"),
						},
					}, nil)
			},
			wantErr: &authz.AuthorizationError{},
		},
		"happy_flow": {
			setup: func(ctx context.Context, c *mock_client.MockClientWithResponsesInterface) {
				c.EXPECT().
					AuthorizeWithResponse(ctx, mockArgs).
					Return(&api.AuthorizeResponse{
						HTTPResponse: &http.Response{StatusCode: http.StatusOK},
						JSON200: &models.AuthorizationResponse{
							Allowed: true,
						},
					}, nil)
			},
			wantErr: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			client := mock_client.NewMockClientWithResponsesInterface(t)

			r, err := rest.New(client)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, client)
			}

			err = r.Authorize(ctx, &authentication.OIDCData{
				Token: "token",
				Email: "test@example.com",
			}, &authorization.RestMetaData{
				Headers: map[string][]string{
					"Test-Header": {"test-value"},
				},
			}, authz.FscAcceptContract, []*authz.ResourceURN{
				authz.NewResourceURN(authz.ResourceTypeContract, "1"),
				authz.NewResourceURN(authz.ResourceTypeContract, "2"),
			})

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}

func stringPointer(s string) *string {
	return &s
}
