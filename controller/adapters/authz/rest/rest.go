// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	api "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest/api/client"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest/api/models"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type Rest struct {
	client api.ClientWithResponsesInterface
}

func New(client api.ClientWithResponsesInterface) (*Rest, error) {
	if client == nil {
		return nil, fmt.Errorf("client is required")
	}

	return &Rest{client: client}, nil
}

func (r *Rest) Authorize(ctx context.Context, authenticationData authentication.Data, metaData authorization.Metadata, action authz.Action, resourceURNs []*authz.ResourceURN) error {
	var (
		headers map[string][]string
	)

	switch md := metaData.(type) {
	case *authorization.RestMetaData:
		headers = md.Headers
	default:
		return fmt.Errorf("unsupported metadata type %T", md)
	}

	switch ad := authenticationData.(type) {
	case *authentication.OIDCData:
		headers["Authorization"] = append(headers["Authorization"], "Bearer "+ad.Token)
	case *authentication.NoneData:
	default:
		return fmt.Errorf("unsupported authentication data type %T", ad)
	}

	resources := make([]string, 0, len(resourceURNs))
	for _, r := range resourceURNs {
		resources = append(resources, r.String())
	}

	res, err := r.client.AuthorizeWithResponse(ctx, models.AuthorizationRequest{
		Input: models.AuthorizationRequestInput{
			Action: models.Action(action),
			Metadata: models.Metadata{
				Headers: &headers,
			},
			Resources: resources,
		},
	})
	if err != nil {
		return fmt.Errorf("could not get authorization result from authorization server: %w", err)
	}

	if res.StatusCode() != http.StatusOK {
		return fmt.Errorf("authorizatin server returned non HTTP 200 status code: %d: %s", res.StatusCode(), res.Body)
	}

	if !res.JSON200.Allowed {
		reason := "no reason returned"

		if res.JSON200.Reason != nil {
			reason = *res.JSON200.Reason
		}

		return authz.NewAuthorizationError(fmt.Sprintf("authorization server did not authorize the request, reason: %s", reason))
	}

	return nil
}
