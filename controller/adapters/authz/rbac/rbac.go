// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rbac

import (
	"context"
	"fmt"
	"slices"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

var roles = map[string][]authz.Action{
	"fsc-admin": {
		authz.FscAcceptContract,
		authz.FscRejectContract,
		authz.FscRevokeContract,
		authz.FscCreateContract,
		authz.FscRetryContract,
		authz.FscListContracts,
		authz.FscGetContract,
		authz.FscListContractDistributions,

		authz.FscCreateService,
		authz.FscDeleteService,
		authz.FscUpdateService,
		authz.FscGetService,
		authz.FscListServices,
		authz.FscListServicePublications,
		authz.FscListDelegatedServicePublications,

		authz.FscListPeerServices,

		authz.FscListTransactionLogs,

		authz.FscListAuditlogs,

		authz.FscGetPeerInfo,

		authz.FscCreatePeer,
		authz.FscUpdatePeer,
		authz.FscListPeers,
		authz.FscSyncPeers,

		authz.FscListInways,
		authz.FscListOutways,

		authz.FscListDelegatedConnections,
	},
	"fsc-readonly": {
		authz.FscListContracts,
		authz.FscGetContract,

		authz.FscGetService,
		authz.FscListServices,
		authz.FscListServicePublications,
		authz.FscListDelegatedServicePublications,

		authz.FscListPeerServices,

		authz.FscListTransactionLogs,

		authz.FscListAuditlogs,

		authz.FscGetPeerInfo,

		authz.FscListPeers,

		authz.FscListInways,
		authz.FscListOutways,

		authz.FscListDelegatedConnections,
	},
}

type RBAC struct {
}

func New() *RBAC {
	return &RBAC{}
}

func (*RBAC) Authorize(ctx context.Context, authenticationData authentication.Data, _ authorization.Metadata, action authz.Action, resourceURNs []*authz.ResourceURN) error {
	var groups []string

	switch ad := authenticationData.(type) {
	case *authentication.OIDCData:
		groups = ad.Groups
	case *authentication.NoneData:
		groups = ad.Groups
	}

	for _, g := range groups {
		allowedActions, ok := roles[g]
		if !ok {
			continue
		}

		if slices.Contains[[]authz.Action](allowedActions, action) {
			return nil
		}
	}

	return authz.NewAuthorizationError(fmt.Sprintf("authorization denied: user has the groups %q but is not in any of the required groups to perform the action %q", strings.Join(groups, ", "), string(action)))
}
