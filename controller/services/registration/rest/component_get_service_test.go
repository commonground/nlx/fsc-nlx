// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
)

func TestGetService(t *testing.T) {
	t.Parallel()

	// Arrange
	internalRESTAPIServer, apiApp := newService(t.Name())
	defer internalRESTAPIServer.Close()

	err := apiApp.Commands.CreateService.Handle(context.Background(), &command.CreateServiceArgs{
		AuthData:              &authentication.NoneData{},
		AuthorizationMetadata: nil,
		AuditLogSource:        auditlog.SourceHTTP{},
		Name:                  "parkeerrechten",
		EndpointURL:           "https://example.com",
		InwayAddress:          "https://inway.local:443",
	})
	require.NoError(t, err)

	client, err := createInternalRESTAPIClient(internalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	res, err := client.GetServiceWithResponse(context.Background(), "test-group", "parkeerrechten")
	assert.NoError(t, err)

	// Assert
	if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}

	assert.Equal(t, "test-group", res.JSON200.Service.GroupId)
	assert.Equal(t, "parkeerrechten", res.JSON200.Service.Name)
	assert.Equal(t, "https://inway.local:443", res.JSON200.Service.InwayAddress)
	assert.Equal(t, "https://example.com", res.JSON200.Service.EndpointUrl)
}

func TestGetNonExistingService(t *testing.T) {
	t.Parallel()

	// Arrange
	internalRESTAPIServer, _ := newService(t.Name())
	defer internalRESTAPIServer.Close()

	client, err := createInternalRESTAPIClient(internalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	res, err := client.GetServiceWithResponse(context.Background(), "test-group", "parkeerrechten")
	assert.NoError(t, err)

	// Assert
	if !assert.Equal(t, http.StatusInternalServerError, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}
}
