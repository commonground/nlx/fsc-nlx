// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type fakeAuth struct{}

func (a *fakeAuth) Authorize(_ context.Context, _ authentication.Data, _ authorization.Metadata, _ authz.Action, _ []*authz.ResourceURN) error {
	return nil
}
