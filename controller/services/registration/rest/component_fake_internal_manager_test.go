// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
)

type fakeInternalManager struct {
}

func (m fakeInternalManager) ListContracts(_ context.Context, _ *manager.ListContractsArgs) (manager.Contracts, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) ListIncomingConnections(_ context.Context, _ *manager.ListIncomingConnectionsArgs) (*manager.IncomingConnections, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) ListOutgoingConnections(_ context.Context, _ *manager.ListOutgoingConnectionsArgs) (*manager.OutgoingConnections, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) ListOutgoingConnectionsForService(_ context.Context, _ *manager.ListOutgoingConnectionsForServiceArgs) ([]*manager.Connection, error) {
	return nil, nil
}

func (m fakeInternalManager) ListOutgoingConnectionsForDelegatedService(_ context.Context, _ *manager.ListOutgoingConnectionsForDelegatedServiceArgs) ([]*manager.Connection, error) {
	return nil, nil
}

func (m fakeInternalManager) ListDelegatedConnections(_ context.Context, _ *manager.ListDelegatedConnectionsArgs) (*manager.DelegatedConnections, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) ListContractsPending(ctx context.Context, args *manager.ListContractsPendingArgs) (*manager.PendingContracts, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) CreateContract(_ context.Context, _ *manager.CreateContractArgs) (string, error) {
	return "", nil
}

func (m fakeInternalManager) AcceptContract(_ context.Context, _, _ string) error {
	return nil
}

func (m fakeInternalManager) RevokeContract(_ context.Context, _, _ string) error {
	return nil
}

func (m fakeInternalManager) RejectContract(_ context.Context, _, _ string) error {
	return nil
}

func (m fakeInternalManager) ListServices(_ context.Context, _ *manager.ListServicesArgs) (manager.Services, error) {
	return nil, nil
}

func (m fakeInternalManager) ListTXLogRecords(_ context.Context, _ *manager.ListTXLogRecordsRequest) ([]*manager.TXLogRecord, error) {
	return nil, nil
}

func (m fakeInternalManager) GetPeerInfo(_ context.Context) (*manager.Peer, error) {
	return nil, nil
}

func (m fakeInternalManager) ListPeers(_ context.Context, _ *[]string) (manager.Peers, error) {
	return nil, nil
}

func (m fakeInternalManager) ListContractDistribution(_ context.Context, _ string) (manager.ContractDistributions, error) {
	return nil, nil
}

func (m fakeInternalManager) RetryContract(_ context.Context, _, _ string, _ manager.Action, _ string) error {
	return nil
}

func (m fakeInternalManager) CreatePeer(_ context.Context, _ *manager.CreatePeerArgs) error {
	return nil
}

func (m fakeInternalManager) UpdatePeer(_ context.Context, _ *manager.UpdatePeerArgs) error {
	return nil
}

func (m fakeInternalManager) ListServicePublications(_ context.Context, _ *manager.ListServicePublicationsArgs) (*manager.Publications, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) ListDelegatedServicePublications(_ context.Context, _ *manager.ListDelegatedServicePublicationsArgs) (*manager.Publications, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) SyncPeers(_ context.Context) (map[string]manager.SynchronizationResult, error) {
	return nil, nil
}
