// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
)

type fakeAuditLogger struct{}

func (a *fakeAuditLogger) CreateRecords(_ context.Context, _ auditlog.Records) error {
	return nil
}

func (a *fakeAuditLogger) ListRecords(_ context.Context, _ *auditlog.ListRecordsArgs) (auditlog.Records, error) {
	return nil, nil
}
