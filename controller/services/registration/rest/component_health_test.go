// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetHealth(t *testing.T) {
	t.Parallel()

	// Arrange
	registrationServer, _ := newService(t.Name())
	defer registrationServer.Close()

	apiClient, err := createInternalRESTAPIClient(registrationServer.URL, peerA.CertBundle)
	require.NoError(t, err)

	// Act
	res, err := apiClient.GetHealthWithResponse(context.Background())

	// Assert
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, res.StatusCode()) {
		t.Errorf("response body:	%s", res.Body)
	}
}
