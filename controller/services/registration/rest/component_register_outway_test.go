// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

//nolint:dupl // these are tests
package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/models"
)

func TestRegisterOutway(t *testing.T) {
	t.Parallel()

	// Arrange
	internalRESTAPIServer, apiApp := newService(t.Name())
	defer internalRESTAPIServer.Close()

	err := apiApp.Commands.CreateService.Handle(context.Background(), &command.CreateServiceArgs{
		AuthData:              &authentication.NoneData{},
		AuthorizationMetadata: nil,
		AuditLogSource:        auditlog.SourceHTTP{},
		Name:                  "parkeerrechten",
		EndpointURL:           "https://example.com",
		InwayAddress:          "https://inway.local:443",
	})
	require.NoError(t, err)

	client, err := createInternalRESTAPIClient(internalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	res, err := client.RegisterOutwayWithResponse(context.Background(), "test-group", "my-outway", models.RegisterOutwayJSONRequestBody{
		PublicKeyThumbprint: "arbitrary",
	})
	assert.NoError(t, err)

	// Assert
	if !assert.Equal(t, http.StatusNoContent, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}
}
