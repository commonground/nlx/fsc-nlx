// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api/models"
)

func TestGetServices(t *testing.T) {
	t.Parallel()

	// Arrange
	externalRESTAPIServer, _ := newService(t.Name())
	defer externalRESTAPIServer.Close()

	client, err := createExternalAPIClient(externalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	_, err = client.CreateServiceWithResponse(context.Background(), models.CreateServiceJSONRequestBody{
		EndpointUrl:  "https://example.com",
		InwayAddress: "https://inway.local:443",
		Name:         "parkeerrechten",
	})
	require.NoError(t, err)

	_, err = client.CreateServiceWithResponse(context.Background(), models.CreateServiceJSONRequestBody{
		EndpointUrl:  "https://example-2.com",
		InwayAddress: "https://inway-2.local:443",
		Name:         "basisregister-fictieve-kentekens",
	})
	require.NoError(t, err)
	// Act
	res, err := client.GetServicesWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Services, 2)

	assert.Equal(t, "parkeerrechten", res.JSON200.Services[1].Name)
	assert.Equal(t, "https://inway.local:443", res.JSON200.Services[1].InwayAddress)
	assert.Equal(t, "https://example.com", res.JSON200.Services[1].EndpointUrl)

	assert.Equal(t, "basisregister-fictieve-kentekens", res.JSON200.Services[0].Name)
	assert.Equal(t, "https://inway-2.local:443", res.JSON200.Services[0].InwayAddress)
	assert.Equal(t, "https://example-2.com", res.JSON200.Services[0].EndpointUrl)
}
