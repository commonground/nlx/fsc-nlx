// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:dupl // structure is similar but the details differ
package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/internalapp/commands"
)

func TestGetOutways(t *testing.T) {
	t.Parallel()

	// Arrange
	externalRESTAPIServer, internalApp := newService(t.Name())
	defer externalRESTAPIServer.Close()

	client, err := createExternalAPIClient(externalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	err = internalApp.Commands.RegisterOutway.Handle(context.Background(), &commands.RegisterOutwayArgs{
		GroupID:             "test-group",
		Name:                "my-outway",
		PublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
	})
	assert.NoError(t, err)

	// Act
	res, err := client.GetOutwaysWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Outways, 1)
	assert.Equal(t, "my-outway", res.JSON200.Outways[0].Name)
	assert.Equal(t, "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae", res.JSON200.Outways[0].PublicKeyThumbprint)
}
