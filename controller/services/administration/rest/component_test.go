// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	postgresstorage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/internalapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var nowInUTC = time.Now().UTC()

var (
	testClock = testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	peerA     *peerInfo
)

func TestMain(m *testing.M) {
	var err error

	peerA, err = newPeerInfo(testingutils.NLXTestPeerA, "https://manager.org-a.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

type peerInfo struct {
	CertBundle     *tls.CertificateBundle
	ManagerAddress string
}

func newPeerInfo(organisationName testingutils.CertificateBundlePeerName, managerAddress string) (*peerInfo, error) {
	peerCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "..", "testing", "pki"), organisationName)
	if err != nil {
		return nil, err
	}

	return &peerInfo{
		CertBundle:     peerCertBundle,
		ManagerAddress: managerAddress,
	}, nil
}

//nolint:funlen,gocyclo // this is a test
func newService(testName string) (*httptest.Server, *internalapp.Application) {
	logger, err := zaplogger.New("debug", "live")
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)
	dbName = fmt.Sprintf("controller_rest_%s", dbName)

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		log.Fatalf("failed to setup test database: %v", err)
	}

	db, err := postgresstorage.NewConnection(context.Background(), testDB)
	if err != nil {
		log.Fatalf("failed to create db connection: %v", err)
	}

	err = postgresstorage.PerformMigrations(testDB)
	if err != nil {
		log.Fatalf("failed to perform dbmigrations: %v", err)
	}

	storage, err := postgresstorage.New(db)
	if err != nil {
		logger.Fatal("could not create postgres storage", err)
	}

	manager := fakeInternalManager{}

	idGenerator := uuidgenerator.New()

	groupID, err := contract.NewGroupID("test-group")
	if err != nil {
		logger.Fatal("invalid group id provided", err)
	}

	internalApp, err := internalapp.NewApplication(&internalapp.NewApplicationArgs{
		Context: context.Background(),
		Storage: storage,
	})
	if err != nil {
		logger.Fatal("could not create internal application", err)
	}

	authorization, err := auth.New(&fakeAuth{})
	if err != nil {
		logger.Fatal("could not create auth", err)
	}

	auditLogr, err := auditlog.New(&auditlog.NewArgs{
		AuditLogger: &fakeAuditLogger{},
		GroupID:     "test-group",
		Clock:       testClock,
		IDGenerator: idGenerator,
	})
	if err != nil {
		logger.Fatal("could not create auditLogger", err)
	}

	apiApp, err := apiapp.NewApplication(context.Background(), &apiapp.NewApplicationArgs{
		Manager:     manager,
		Storage:     storage,
		Clock:       testClock,
		IVGenerator: idGenerator,
		Logger:      logger,
		GroupID:     groupID,
		Auth:        authorization,
		AuditLogger: auditLogr,
	})
	if err != nil {
		logger.Fatal("could not create api application", err)
	}

	server, err := rest.New(&rest.NewArgs{
		Logger: logger,
		App:    apiApp,
		Cert:   peerA.CertBundle,
	})
	if err != nil {
		logger.Fatal("could not create rest port", err)
	}

	srv := httptest.NewUnstartedServer(server.Handler())
	srv.TLS = peerA.CertBundle.TLSConfig(peerA.CertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv, internalApp
}

func createExternalAPIClient(managerURL string, certBundle *tls.CertificateBundle) (*api.ClientWithResponses, error) {
	return api.NewClientWithResponses(managerURL, func(c *api.Client) error {
		t := &http.Transport{
			TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
		}
		c.Client = &http.Client{
			Transport: t,
		}

		return nil
	})
}
