// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api/models"
)

func TestGetService(t *testing.T) {
	t.Parallel()

	// Arrange
	externalRESTAPIServer, _ := newService(t.Name())
	defer externalRESTAPIServer.Close()

	client, err := createExternalAPIClient(externalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	_, err = client.CreateServiceWithResponse(context.Background(), models.CreateServiceJSONRequestBody{
		EndpointUrl:  "https://example.com",
		InwayAddress: "https://inway.local:443",
		Name:         "parkeerrechten",
	})
	assert.NoError(t, err)

	// Act
	res, err := client.GetServiceWithResponse(context.Background(), "parkeerrechten")
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Equal(t, "parkeerrechten", res.JSON200.Name)
	assert.Equal(t, "https://inway.local:443", res.JSON200.InwayAddress)
	assert.Equal(t, "https://example.com", res.JSON200.EndpointUrl)
}

func TestGetNonExistingService(t *testing.T) {
	t.Parallel()

	// Arrange
	externalRESTAPIServer, _ := newService(t.Name())
	defer externalRESTAPIServer.Close()

	client, err := createExternalAPIClient(externalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	// Act
	res, err := client.GetServiceWithResponse(context.Background(), "parkeerrechten")
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusNotFound, res.StatusCode())
}
