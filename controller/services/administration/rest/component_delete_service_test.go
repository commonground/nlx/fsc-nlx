// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:dupl // structure is similar but the details differ
package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api/models"
)

func TestDeleteService(t *testing.T) {
	t.Parallel()

	// Arrange
	externalRESTAPIServer, _ := newService(t.Name())
	defer externalRESTAPIServer.Close()

	client, err := createExternalAPIClient(externalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	res, err := client.CreateServiceWithResponse(context.Background(), models.CreateServiceJSONRequestBody{
		EndpointUrl:  "http://example.com",
		InwayAddress: "https://inway.local:443",
		Name:         "parkeerrechten",
	})
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusCreated, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}

	// Act
	resDelete, err := client.DeleteServiceWithResponse(context.Background(), "parkeerrechten")
	assert.NoError(t, err)

	// Assert
	if !assert.Equal(t, http.StatusNoContent, resDelete.StatusCode()) {
		t.Errorf("response body: %s", resDelete.Body)
	}
}
