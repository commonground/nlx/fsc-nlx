// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api/models"
)

func TestUpdateService(t *testing.T) {
	t.Parallel()

	// Arrange
	externalRESTAPIServer, internalAPI := newService(t.Name())
	defer externalRESTAPIServer.Close()

	client, err := createExternalAPIClient(externalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	_, err = client.CreateServiceWithResponse(context.Background(), models.CreateServiceJSONRequestBody{
		EndpointUrl:  "https://example.com",
		InwayAddress: "https://inway.local:443",
		Name:         "parkeerrechten",
	})
	assert.NoError(t, err)

	// Act
	res, err := client.UpdateServiceWithResponse(context.Background(), "parkeerrechten", models.UpdateServiceJSONRequestBody{
		EndpointUrl:  "https://example-updated.com",
		InwayAddress: "https://inway-updated.local:443",
		Name:         "parkeerrechten-updated",
	})
	assert.NoError(t, err)

	// Assert
	if !assert.Equal(t, http.StatusNoContent, res.StatusCode()) {
		t.Errorf("response body: %s", res.Body)
	}

	service, err := internalAPI.Queries.GetService.Handle(context.Background(), "test-group", "parkeerrechten")
	assert.NoError(t, err)

	assert.Equal(t, "parkeerrechten", service.Name)
	assert.NotNil(t, service.EndpointURL)
	assert.Equal(t, "https://example-updated.com", service.EndpointURL.String())
	assert.Equal(t, "https://inway-updated.local:443", service.InwayAddress)
}
