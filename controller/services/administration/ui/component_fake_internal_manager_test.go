// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package ui_test

import (
	"context"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
)

type fakeInternalManager struct {
}

func (m fakeInternalManager) ListServicePublications(_ context.Context, _ *manager.ListServicePublicationsArgs) (*manager.Publications, string, error) {
	return &manager.Publications{
		TotalCount: 1,
		Publications: []*manager.Publication{
			{
				ServicePeerID:   peerA.CertBundle.GetPeerInfo().SerialNumber,
				ServiceName:     "parkeerrechten",
				ContentHash:     "",
				CreatedAt:       testClock.Now(),
				ValidNotBefore:  testClock.Now(),
				ValidNotAfter:   testClock.Now(),
				State:           "",
				GrantHash:       "",
				DirectoryPeerID: "",
				DelegatorPeerID: "",
			},
		},
	}, "", nil
}

func (m fakeInternalManager) ListDelegatedServicePublications(_ context.Context, _ *manager.ListDelegatedServicePublicationsArgs) (*manager.Publications, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) ListContracts(_ context.Context, _ *manager.ListContractsArgs) (manager.Contracts, string, error) {
	return manager.Contracts{
		{
			IV:               "",
			Hash:             "",
			HashAlgorithm:    0,
			GroupID:          "",
			CreatedAt:        time.Time{},
			Peers:            nil,
			AcceptSignatures: nil,
			RejectSignatures: nil,
			RevokeSignatures: nil,
			ValidFrom:        time.Time{},
			ValidUntil:       time.Time{},
			HasRejected:      false,
			HasAccepted:      false,
			HasRevoked:       false,
			ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
				{
					Hash:                              "",
					ServicePeerID:                     "",
					ServiceName:                       "",
					ServicePublicationDelegatorPeerID: "",
					OutwayPeerID:                      "",
					OutwayPublicKeyThumbprint:         "",
				},
			},
			State: "",
		},
	}, "", nil
}

func (m fakeInternalManager) ListIncomingConnections(_ context.Context, _ *manager.ListIncomingConnectionsArgs) (*manager.IncomingConnections, string, error) {
	return &manager.IncomingConnections{
		TotalCount: 1,
		Connections: []*manager.Connection{
			{
				ServicePeerID:                     peerA.CertBundle.GetPeerInfo().SerialNumber,
				ServicePublicationDelegatorPeerID: nil,
				ServiceName:                       "parkeerrechten",
				OutwayPeerID:                      peerB.CertBundle.GetPeerInfo().SerialNumber,
				OutwayPublicKeyThumbprint:         peerA.CertBundle.PublicKeyThumbprint(),
				ContentHash:                       "arbitrary",
				CreatedAt:                         testClock.Now(),
				ValidNotBefore:                    testClock.Now(),
				ValidNotAfter:                     testClock.Now(),
				State:                             "",
				GrantHash:                         "",
				DelegatorPeerID:                   nil,
			},
		},
	}, "", nil
}

func (m fakeInternalManager) ListDelegatedConnections(_ context.Context, _ *manager.ListDelegatedConnectionsArgs) (*manager.DelegatedConnections, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) ListContractsPending(ctx context.Context, args *manager.ListContractsPendingArgs) (*manager.PendingContracts, string, error) {
	return &manager.PendingContracts{
		PendingContracts: []*manager.PendingContract{
			{
				ContentHash: "arbitrary",
				Grants: []*manager.PendingContractGrant{
					{
						ServiceName:                   "",
						ServicePeerID:                 "",
						OutwayPeerID:                  "",
						DelegatorPeerID:               "",
						ServicePublicationDelegatorID: "",
						GrantType:                     "",
					},
				},
				ValidNotBefore: testClock.Now(),
			},
		},
		TotalCount: 0,
	}, "", nil
}

func (m fakeInternalManager) ListOutgoingConnections(_ context.Context, _ *manager.ListOutgoingConnectionsArgs) (*manager.OutgoingConnections, string, error) {
	return nil, "", nil
}

func (m fakeInternalManager) ListOutgoingConnectionsForService(_ context.Context, _ *manager.ListOutgoingConnectionsForServiceArgs) ([]*manager.Connection, error) {
	return nil, nil
}

func (m fakeInternalManager) ListOutgoingConnectionsForDelegatedService(_ context.Context, _ *manager.ListOutgoingConnectionsForDelegatedServiceArgs) ([]*manager.Connection, error) {
	return nil, nil
}

func (m fakeInternalManager) CreateContract(_ context.Context, _ *manager.CreateContractArgs) (string, error) {
	return "", nil
}

func (m fakeInternalManager) AcceptContract(_ context.Context, _, _ string) error {
	return nil
}

func (m fakeInternalManager) RevokeContract(_ context.Context, _, _ string) error {
	return nil
}

func (m fakeInternalManager) RejectContract(_ context.Context, _, _ string) error {
	return nil
}

func (m fakeInternalManager) ListServices(_ context.Context, _ *manager.ListServicesArgs) (manager.Services, error) {
	return manager.Services{
		&manager.Service{
			Name:     "parkeerrechten",
			PeerID:   peerB.CertBundle.GetPeerInfo().SerialNumber,
			PeerName: peerB.CertBundle.GetPeerInfo().Name,
		},
	}, nil
}

func (m fakeInternalManager) ListTXLogRecords(_ context.Context, _ *manager.ListTXLogRecordsRequest) ([]*manager.TXLogRecord, error) {
	return nil, nil
}

func (m fakeInternalManager) GetPeerInfo(_ context.Context) (*manager.Peer, error) {
	return &manager.Peer{
		ID:             peerA.CertBundle.GetPeerInfo().SerialNumber,
		Name:           peerA.CertBundle.GetPeerInfo().Name,
		ManagerAddress: peerA.ManagerAddress,
		Roles:          []manager.PeerRole{},
	}, nil
}

func (m fakeInternalManager) ListPeers(_ context.Context, _ *[]string) (manager.Peers, error) {
	return nil, nil
}

func (m fakeInternalManager) ListContractDistribution(_ context.Context, _ string) (manager.ContractDistributions, error) {
	return nil, nil
}

func (m fakeInternalManager) RetryContract(_ context.Context, _, _ string, _ manager.Action, _ string) error {
	return nil
}

func (m fakeInternalManager) CreatePeer(_ context.Context, _ *manager.CreatePeerArgs) error {
	return nil
}

func (m fakeInternalManager) UpdatePeer(_ context.Context, _ *manager.UpdatePeerArgs) error {
	return nil
}

func (m fakeInternalManager) SyncPeers(_ context.Context) (map[string]manager.SynchronizationResult, error) {
	return nil, nil
}
