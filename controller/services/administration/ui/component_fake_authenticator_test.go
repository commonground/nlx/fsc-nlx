// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package ui_test

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	auth "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication"
)

type fakeAuthenticator struct{}

func (a *fakeAuthenticator) OnlyAuthenticated(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), auth.ContextAuthenticationDataKey, &authentication.NoneData{
			ID:     "admin",
			Groups: []string{"fsc-admin"},
		})))
	}
}

func (a *fakeAuthenticator) MountRoutes(r chi.Router) {

}

func (a *fakeAuthenticator) LogoutURL() string {
	return ""
}
