// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package ui_test

import (
	"context"
	"fmt"
	"log"
	"net/http/httptest"
	"net/netip"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator/uuid"
	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	postgresstorage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	uiport "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/i18n"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var nowInUTC = time.Now().UTC()

var (
	testClock = testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	peerA     *peerInfo
	peerB     *peerInfo

	authData = &authentication.NoneData{}

	source = common_auditlog.SourceHTTP{
		IPAddress: netip.AddrFrom4([4]byte{127, 0, 0, 1}),
		UserAgent: "test-useragent",
	}
)

func TestMain(m *testing.M) {
	var err error

	peerA, err = newPeerInfo(testingutils.NLXTestPeerA, "https://manager.org-a.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerB, err = newPeerInfo(testingutils.NLXTestPeerB, "https://manager.org-b.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

type peerInfo struct {
	CertBundle     *tls.CertificateBundle
	ManagerAddress string
}

func newPeerInfo(organisationName testingutils.CertificateBundlePeerName, managerAddress string) (*peerInfo, error) {
	peerCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "..", "testing", "pki"), organisationName)
	if err != nil {
		return nil, err
	}

	return &peerInfo{
		CertBundle:     peerCertBundle,
		ManagerAddress: managerAddress,
	}, nil
}

//nolint:funlen,gocyclo // this is a test
func newService(testName string) (*httptest.Server, *apiapp.Application, error) {
	logger, err := zaplogger.New("debug", "live")
	if err != nil {
		return nil, nil, fmt.Errorf("failed to create logger: %v", err)
	}

	postgresqlRepository, err := setupPostgresqlRepository(testName)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to setup postgresql repository: %v", err)
	}

	manager := fakeInternalManager{}

	idGenerator := uuidgenerator.New()

	groupID, err := contract.NewGroupID("test-group")
	if err != nil {
		logger.Fatal("invalid group id provided", err)
	}

	authorization, err := auth.New(&fakeAuthorization{})
	if err != nil {
		logger.Fatal("could not create auth", err)
	}

	auditLogr, err := auditlog.New(&auditlog.NewArgs{
		AuditLogger: &fakeAuditLogger{},
		GroupID:     "test-group",
		Clock:       testClock,
		IDGenerator: idGenerator,
	})
	if err != nil {
		logger.Fatal("could not create auditLogger", err)
	}

	apiApp, err := apiapp.NewApplication(context.Background(), &apiapp.NewApplicationArgs{
		Manager:     manager,
		Storage:     postgresqlRepository,
		Clock:       testClock,
		IVGenerator: idGenerator,
		Logger:      logger,
		GroupID:     groupID,
		Auth:        authorization,
		AuditLogger: auditLogr,
	})
	if err != nil {
		logger.Fatal("could not create api application", err)
	}

	assetMap := uiport.AssetMap{}

	server, err := uiport.New(context.Background(), &uiport.NewServerArgs{
		AssetsMap:             assetMap,
		Locale:                i18n.LocaleEnUS,
		StaticPath:            "/",
		GroupID:               "test-group",
		CsrfAuthKey:           "",
		CsrfProtectionEnabled: false,
		CsrfUnsafeHTTP:        false,
		ServeAddress:          "",
		Logger:                logger,
		APIApp:                apiApp,
		Authenticator:         &fakeAuthenticator{},
		DirectoryPeerID:       "",
	})
	if err != nil {
		logger.Fatal("could not create ui port", err)
	}

	srv := httptest.NewUnstartedServer(server.Handler())
	srv.Start()

	return srv, apiApp, nil
}

func setupPostgresqlRepository(testName string) (storage.Storage, error) {
	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)
	dbName = strings.Replace(dbName, "test", "", 1) // strip 'test'-prefix
	dbName = fmt.Sprintf("ctrl_ui%s", dbName)
	dbName = strings.ReplaceAll(dbName, "/", "_")

	// via https://stackoverflow.com/a/27865772/363448
	const maxLengthDatabaseName = 63

	if len(dbName) > maxLengthDatabaseName {
		return nil, fmt.Errorf("database name too long (%d > 63 characters): %q", len(dbName), dbName)
	}

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		return nil, fmt.Errorf("failed to setup test database: %v", err)
	}

	db, err := postgresstorage.NewConnection(context.Background(), testDB)
	if err != nil {
		return nil, fmt.Errorf("failed to create db connection: %v", err)
	}

	err = postgresstorage.PerformMigrations(testDB)
	if err != nil {
		return nil, fmt.Errorf("failed to perform dbmigrations: %v", err)
	}

	repository, err := postgresstorage.New(db)
	if err != nil {
		return nil, fmt.Errorf("could not create postgres storage: %v", err)
	}

	return repository, nil
}
