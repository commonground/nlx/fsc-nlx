// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package ui_test

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
)

type fakeAuditLogger struct{}

func (a *fakeAuditLogger) CreateRecords(_ context.Context, _ auditlog.Records) error {
	return nil
}

func (a *fakeAuditLogger) ListRecords(_ context.Context, _ *auditlog.ListRecordsArgs) (auditlog.Records, error) {
	return auditlog.Records{
		{
			ID:            auditlog.ID(uuid.New()),
			GroupID:       "test-group",
			CorrelationID: auditlog.CorrelationID(uuid.New()),
			Actor: &auditlog.ActorUser{
				Name:  "Jane Doe",
				Email: "jane.doe@example.com",
			},
			Event: &auditlog.EventUpdatePeer{
				PeerID: peerB.CertBundle.GetPeerInfo().SerialNumber,
			},
			Source:    &auditlog.SourceHTTP{},
			Status:    &auditlog.StatusInitiated{},
			Component: "manager",
			CreatedAt: testClock.Now(),
		},
	}, nil
}
