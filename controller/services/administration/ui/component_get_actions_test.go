// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:dupl // structure is similar but the details differ
package ui_test

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetActions(t *testing.T) {
	t.Parallel()

	// Arrange
	externalUIServer, _, err := newService(t.Name())
	require.NoError(t, err)

	defer externalUIServer.Close()

	url := fmt.Sprintf("%s/actions", externalUIServer.URL)

	resp, err := http.Get(url) // nolint:gosec // the URL contains the test server address
	require.NoError(t, err)

	t.Cleanup(func() {
		resp.Body.Close()
	})

	assert.Equal(t, http.StatusOK, resp.StatusCode)
}
