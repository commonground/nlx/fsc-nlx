// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:dupl // structure is similar but the details differ
package ui_test

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestServicesPublicationsTabActive(t *testing.T) {
	t.Parallel()

	// Arrange
	externalUIServer, _, err := newService(t.Name())
	require.NoError(t, err)

	url := fmt.Sprintf("%s/services/publications/active", externalUIServer.URL)

	resp, err := http.Get(url) // nolint:gosec // the URL contains the test server address
	require.NoError(t, err)

	t.Cleanup(func() {
		externalUIServer.Close()
		resp.Body.Close()
	})

	assert.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestServicesPublicationsTabProposed(t *testing.T) {
	t.Parallel()

	// Arrange
	externalUIServer, _, err := newService(t.Name())
	require.NoError(t, err)

	url := fmt.Sprintf("%s/services/publications/proposed", externalUIServer.URL)

	resp, err := http.Get(url) // nolint:gosec // the URL contains the test server address
	require.NoError(t, err)

	t.Cleanup(func() {
		externalUIServer.Close()
		resp.Body.Close()
	})

	assert.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestServicesPublicationsTabArchive(t *testing.T) {
	t.Parallel()

	// Arrange
	externalUIServer, _, err := newService(t.Name())
	require.NoError(t, err)

	url := fmt.Sprintf("%s/services/publications/archive", externalUIServer.URL)

	resp, err := http.Get(url) // nolint:gosec // the URL contains the test server address
	require.NoError(t, err)

	t.Cleanup(func() {
		externalUIServer.Close()
		resp.Body.Close()
	})

	assert.Equal(t, http.StatusOK, resp.StatusCode)
}
