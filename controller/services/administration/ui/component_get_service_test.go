// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:dupl // structure is similar but the details differ
package ui_test

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
)

func TestGetService(t *testing.T) {
	t.Parallel()

	// Arrange
	externalUIServer, apiApp, err := newService(t.Name())
	require.NoError(t, err)

	err = apiApp.Commands.CreateService.Handle(context.Background(), &command.CreateServiceArgs{
		AuthData:              authData,
		AuthorizationMetadata: nil,
		AuditLogSource:        source,
		Name:                  "parkeerrechten",
		EndpointURL:           "https://example.com",
		InwayAddress:          "https://my-inway.com:443",
	})
	require.NoError(t, err)

	url := fmt.Sprintf("%s/services/parkeerrechten", externalUIServer.URL)

	resp, err := http.Get(url) // nolint:gosec // the URL contains the test server address
	require.NoError(t, err)

	t.Cleanup(func() {
		externalUIServer.Close()
		resp.Body.Close()
	})

	assert.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestGetNonExistingService(t *testing.T) {
	t.Parallel()

	// Arrange
	externalUIServer, apiApp, err := newService(t.Name())
	require.NoError(t, err)

	err = apiApp.Commands.CreateService.Handle(context.Background(), &command.CreateServiceArgs{
		AuthData:              authData,
		AuthorizationMetadata: nil,
		AuditLogSource:        source,
		Name:                  "parkeerrechten",
		EndpointURL:           "https://example.com",
		InwayAddress:          "https://my-inway.com:443",
	})
	require.NoError(t, err)

	url := fmt.Sprintf("%s/services/non-existing-service", externalUIServer.URL)

	resp, err := http.Get(url) // nolint:gosec // the URL contains the test server address
	require.NoError(t, err)

	t.Cleanup(func() {
		externalUIServer.Close()
		resp.Body.Close()
	})

	assert.Equal(t, http.StatusNotFound, resp.StatusCode)
}
