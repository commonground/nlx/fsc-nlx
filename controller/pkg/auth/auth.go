// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auth

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type Auth struct {
	authz authz.Authorization
}

func New(a authz.Authorization) (*Auth, error) {
	if a == nil {
		return nil, fmt.Errorf("authorization adapter is required")
	}

	return &Auth{
		authz: a,
	}, nil
}

func (a *Auth) Auth(ctx context.Context, authenticationData authentication.Data, metaData authorization.Metadata, action authz.Action, resourceURNs []*authz.ResourceURN) error {
	err := authentication.Authenticate(authenticationData)
	if err != nil {
		return err
	}

	err = a.authz.Authorize(ctx, authenticationData, metaData, action, resourceURNs)
	if err != nil {
		return err
	}

	return nil
}
