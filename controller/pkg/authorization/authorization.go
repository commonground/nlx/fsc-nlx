// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package authorization

import "fmt"

type Metadata any

type RestMetaData struct {
	Headers map[string][]string
	URL     string
}

type Type string

const (
	TypeUnspecified Type = ""
	TypeRBAC        Type = "rbac"
	TypeRest        Type = "rest"
)

var types []Type = []Type{TypeRBAC, TypeRest}

func ValidStringTypes() []string {
	s := make([]string, len(types))

	for i, a := range types {
		s[i] = string(a)
	}

	return s
}

func TypeFromString(t string) (Type, error) {
	switch Type(t) {
	case TypeRBAC:
		return TypeRBAC, nil
	case TypeRest:
		return TypeRest, nil
	default:
		return TypeUnspecified, fmt.Errorf("unknown type: %s", t)
	}
}
