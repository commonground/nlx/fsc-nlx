// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package authentication

type Data interface {
	Username() string
	Mail() string
}

type NoneData struct {
	ID     string `json:"email"`
	Groups []string
}

type OIDCData struct {
	Email  string `json:"email"`
	Name   string `json:"name"`
	Token  string
	Groups []string
}

func (o *OIDCData) Mail() string {
	return o.Email
}

func (o *OIDCData) Username() string {
	return o.Name
}

func (n *NoneData) Mail() string {
	return n.ID
}

func (n *NoneData) Username() string {
	return "admin" // default admin username when unauthenticated
}

type AuthenticationError struct {
	m string
}

func newAuthenticationError(message string) *AuthenticationError {
	return &AuthenticationError{
		m: message,
	}
}

func (v *AuthenticationError) Error() string {
	return "authentication error: " + v.m
}

func Authenticate(a Data) error {
	if a == nil {
		return newAuthenticationError("unauthenticated")
	}

	switch a.(type) {
	case *OIDCData:
		return nil
	case *NoneData:
		return nil
	default:
		return newAuthenticationError("unknown data type")
	}
}
