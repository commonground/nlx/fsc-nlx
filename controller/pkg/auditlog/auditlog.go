// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import (
	"context"
	"errors"
	"fmt"

	"github.com/gofrs/uuid"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator"
	auth "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
)

type AddRecordArgs struct {
	AuthData auth.Data
	Event    common_auditlog.Event
	Source   common_auditlog.Source
}

type AuditLogger struct {
	auditLogger common_auditlog.AuditLogger
	groupID     string
	clock       clock.Clock
	idGenerator idgenerator.IDGenerator
}

type NewArgs struct {
	AuditLogger common_auditlog.AuditLogger
	GroupID     string
	Clock       clock.Clock
	IDGenerator idgenerator.IDGenerator
}

func New(args *NewArgs) (*AuditLogger, error) {
	if args.AuditLogger == nil {
		return nil, errors.New("audit logger is required")
	}

	if args.GroupID == "" {
		return nil, errors.New("group id is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.IDGenerator == nil {
		return nil, errors.New("id generator is required")
	}

	return &AuditLogger{
		auditLogger: args.AuditLogger,
		groupID:     args.GroupID,
		clock:       args.Clock,
		idGenerator: args.IDGenerator,
	}, nil
}

func (l *AuditLogger) ListRecords(ctx context.Context, args *common_auditlog.ListRecordsArgs) (common_auditlog.Records, error) {
	args.GroupID = l.groupID

	return l.auditLogger.ListRecords(ctx, args)
}

func (l *AuditLogger) AddRecord(ctx context.Context, action func(auditlogCorrelationID string) error, args *AddRecordArgs) error {
	_, err := l.AddRecordWithResultAndEvent(ctx, func(auditlogCorrelationID string) (any, common_auditlog.Event, error) {
		err := action(auditlogCorrelationID)

		return nil, nil, err
	}, args)

	return err
}

func (l *AuditLogger) AddRecordWithResultAndEvent(ctx context.Context, action func(auditlogCorrelationID string) (res any, event common_auditlog.Event, err error), args *AddRecordArgs) (any, error) {
	initRecord, err := l.createInitRecord(l.groupID, args)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create Initiated Audit log record"))
	}

	err = l.auditLogger.CreateRecords(ctx, common_auditlog.Records{initRecord})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not send Initiated record to Audit log"))
	}

	var (
		res   any
		event common_auditlog.Event
	)

	res, event, err = action(initRecord.CorrelationID.String())
	if err != nil {
		errorRecord, auditErr := initRecord.CloneWithStatus(common_auditlog.StatusFailed{Error: err.Error()}, l.clock.Now(), l.idGenerator)
		if auditErr != nil {
			return nil, errors.Join(err, errors.New("could not create Failed Audit log record"))
		}

		auditErr = l.auditLogger.CreateRecords(ctx, common_auditlog.Records{errorRecord})
		if auditErr != nil {
			return nil, errors.Join(err, errors.New("could not send Failed record to Audit log"))
		}

		return nil, err
	}

	succeededRecord, errWithSucceed := initRecord.CloneWithStatus(common_auditlog.StatusSucceeded{}, l.clock.Now(), l.idGenerator)
	if errWithSucceed != nil {
		return nil, errors.Join(errWithSucceed, errors.New("could not create Succeeded Audit log record"))
	}

	if event != nil {
		succeededRecord.Event = event
	}

	errWithSucceed = l.auditLogger.CreateRecords(ctx, common_auditlog.Records{succeededRecord})
	if errWithSucceed != nil {
		return nil, errors.Join(errWithSucceed, errors.New("could not send Succeeded record to Audit log"))
	}

	return res, nil
}

func (l *AuditLogger) createInitRecord(groupID string, args *AddRecordArgs) (*common_auditlog.Record, error) {
	if args.Event == nil {
		return nil, fmt.Errorf("event is required")
	}

	if args.Source == nil {
		return nil, fmt.Errorf("source is required")
	}

	recordID, err := l.idGenerator.New()
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not generate id"))
	}

	recordUUID, _ := uuid.FromString(recordID)

	correlationID, err := l.idGenerator.New()
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not generate correlation id"))
	}

	correlationUUID, _ := uuid.FromString(correlationID)

	actor := actorFromAuthData(args.AuthData)

	return &common_auditlog.Record{
		ID:            common_auditlog.ID(recordUUID),
		GroupID:       common_auditlog.GroupID(groupID),
		CorrelationID: common_auditlog.CorrelationID(correlationUUID),
		Actor:         actor,
		Event:         args.Event,
		Source:        args.Source,
		Status:        common_auditlog.StatusInitiated{},
		Component:     common_auditlog.ComponentController,
		CreatedAt:     l.clock.Now(),
	}, nil
}

func actorFromAuthData(authData auth.Data) common_auditlog.Actor {
	if authData == nil {
		return &common_auditlog.ActorUnknown{}
	}

	return &common_auditlog.ActorUser{
		Name:  authData.Username(),
		Email: authData.Mail(),
	}
}
