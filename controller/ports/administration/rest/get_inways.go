// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

// nolint:dupl // structure is similar but the details differ
package rest

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api/models"
)

func (s *Server) GetInways(ctx context.Context, req api.GetInwaysRequestObject) (api.GetInwaysResponseObject, error) {
	s.logger.Info("rest request GetInways")

	inways, err := s.app.Queries.ListInways.Handle(ctx, &query.ListInwaysArgs{
		AuthData: authData,
	})
	if err != nil {
		s.logger.Error("failed to list inways", err)

		return nil, errors.Wrap(err, "failed to list inways")
	}

	resp := api.GetInways200JSONResponse{}

	for _, i := range inways {
		resp.Inways = append(resp.Inways, models.Inway{
			Name:    i.Name,
			Address: i.Address,
		})
	}

	return resp, nil
}
