// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

// nolint:dupl // structure is similar but the details differ
package rest

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api/models"
)

func (s *Server) GetOutways(ctx context.Context, req api.GetOutwaysRequestObject) (api.GetOutwaysResponseObject, error) {
	s.logger.Info("rest request GetOutways")

	outways, err := s.app.Queries.ListOutways.Handle(ctx, &query.ListOutwaysArgs{
		AuthData: authData,
	})
	if err != nil {
		s.logger.Error("failed to list outways", err)

		return nil, errors.Wrap(err, "failed to list outways")
	}

	resp := api.GetOutways200JSONResponse{}

	for _, i := range outways {
		resp.Outways = append(resp.Outways, models.Outway{
			Name:                i.Name,
			PublicKeyThumbprint: i.PublicKeyThumbprint,
		})
	}

	return resp, nil
}
