// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
)

func (s *Server) DeleteService(ctx context.Context, req api.DeleteServiceRequestObject) (api.DeleteServiceResponseObject, error) {
	s.logger.Info("rest request DeleteService")

	auditLogSource, err := AuditLogSourceFromHTTPRequestContext(ctx)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)

		return nil, errors.Join(err, fmt.Errorf("could not obtain connection info needed for Audit log"))
	}

	err = s.app.Commands.DeleteService.Handle(ctx, &command.DeleteServiceArgs{
		AuthData:       authData,
		AuditLogSource: auditLogSource,
		Name:           req.Name,
	})
	if err != nil {
		s.logger.Error(fmt.Sprintf("failed to delete service: %q", req.Name), err)

		return nil, errors.Join(err, fmt.Errorf("could not delete service"))
	}

	return api.DeleteService204Response{}, nil
}
