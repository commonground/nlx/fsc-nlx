// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"errors"
	"fmt"
	"net/netip"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
)

type PeerInfo struct {
	ID   string
	Name string
}

func (s *Server) getPeerInfo(ctx context.Context, authData auth.Data) (*PeerInfo, error) {
	if s.peerInfo != nil {
		return s.peerInfo, nil
	}

	p, err := s.app.Queries.GetPeerInfo.Handle(ctx, &query.GetPeerInfoArgs{AuthData: authData})
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not get peer info"))
	}

	peerInfo := &PeerInfo{
		ID:   p.ID,
		Name: p.Name,
	}

	s.peerInfo = peerInfo

	return peerInfo, nil
}

func AuditLogSourceFromHTTPRequestContext(c context.Context) (common_auditlog.Source, error) {
	userAgent := c.Value(UserAgentKey).(string)
	remoteAddress := c.Value(RemoteAddressKey).(string)

	addr, err := netip.ParseAddrPort(remoteAddress)
	if err != nil {
		return nil, errors.Join(err, errors.New("cannot obtain client IP Address from HTTP request"))
	}

	return common_auditlog.SourceHTTP{
		IPAddress: addr.Addr(),
		UserAgent: userAgent,
	}, nil
}
