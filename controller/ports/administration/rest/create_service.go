// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
)

func (s *Server) CreateService(ctx context.Context, req api.CreateServiceRequestObject) (api.CreateServiceResponseObject, error) {
	auditLogSource, err := AuditLogSourceFromHTTPRequestContext(ctx)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)

		return nil, errors.Join(err, fmt.Errorf("could not obtain connection info needed for Audit log"))
	}

	err = s.app.Commands.CreateService.Handle(ctx, &command.CreateServiceArgs{
		AuthData:       authData,
		AuditLogSource: auditLogSource,
		Name:           req.Body.Name,
		EndpointURL:    req.Body.EndpointUrl,
		InwayAddress:   req.Body.InwayAddress,
	})
	if err != nil {
		s.logger.Error("could not create service", err)

		return nil, errors.Join(err, fmt.Errorf("could not create service"))
	}

	return api.CreateService201Response{}, nil
}
