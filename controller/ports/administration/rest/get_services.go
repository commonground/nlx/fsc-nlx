// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api/models"
)

func (s *Server) GetServices(ctx context.Context, _ api.GetServicesRequestObject) (api.GetServicesResponseObject, error) {
	s.logger.Info("rest request GetServices")

	peerInfo, err := s.getPeerInfo(ctx, authData)
	if err != nil {
		s.logger.Error("could not obtain peer info", err)

		return nil, errors.Wrap(err, "could not obtain peer info")
	}

	services, err := s.app.Queries.ListServices.Handle(ctx, &query.ListServicesArgs{
		AuthData:   authData,
		SelfPeerID: peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("could not obtain available services", err)

		return nil, errors.Wrap(err, "could not obtain available services")
	}

	resp := api.GetServices200JSONResponse{
		Services: []models.Service{},
	}

	for _, service := range services {
		endpointURL := service.EndpointURL.String()

		resp.Services = append(resp.Services, models.Service{
			Name:         service.Name,
			EndpointUrl:  endpointURL,
			InwayAddress: service.InwayAddress,
		})
	}

	return resp, nil
}
