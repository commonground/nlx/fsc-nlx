// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
)

type contextKey string

const (
	UserAgentKey     contextKey = "UserAgent"
	RemoteAddressKey contextKey = "SourceAddress"
)

var authData = &authentication.NoneData{
	ID:     "admin",
	Groups: []string{"fsc-admin"},
}

type Server struct {
	app      *apiapp.Application
	logger   *logger.Logger
	cert     *common_tls.CertificateBundle
	peerInfo *PeerInfo
	handler  http.Handler
}

type NewArgs struct {
	Logger *logger.Logger
	App    *apiapp.Application
	Cert   *common_tls.CertificateBundle
}

func New(args *NewArgs) (*Server, error) {
	if args == nil {
		return nil, fmt.Errorf("args required")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger required")
	}

	if args.Cert == nil {
		return nil, fmt.Errorf("cert required")
	}

	if args.App == nil {
		return nil, fmt.Errorf("app required")
	}

	s := &Server{
		app:    args.App,
		logger: args.Logger,
		cert:   args.Cert,
	}

	r := chi.NewRouter()
	r.Use(ExtractUserAgentAndRemoteAddress)
	api.HandlerFromMux(api.NewStrictHandler(s, []api.StrictMiddlewareFunc{}), r)

	s.handler = r

	return s, nil
}

func ExtractUserAgentAndRemoteAddress(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), UserAgentKey, r.UserAgent())
		ctx = context.WithValue(ctx, RemoteAddressKey, r.RemoteAddr)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (s *Server) Handler() http.Handler {
	return s.handler
}
