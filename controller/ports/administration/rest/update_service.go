// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
)

func (s *Server) UpdateService(ctx context.Context, req api.UpdateServiceRequestObject) (api.UpdateServiceResponseObject, error) {
	s.logger.Info("rest request UpdateService")

	auditLogSource, err := AuditLogSourceFromHTTPRequestContext(ctx)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)

		return nil, errors.Join(err, fmt.Errorf("could not obtain connection info needed for Audit log"))
	}

	err = s.app.Commands.UpdateService.Handle(ctx, &command.UpdateServiceArgs{
		AuthData:       authData,
		AuditLogSource: auditLogSource,
		Name:           req.Name,
		EndpointURL:    req.Body.EndpointUrl,
		InwayAddress:   req.Body.InwayAddress,
	})
	if err != nil {
		if errors.Is(err, command.ErrServiceNotFound) {
			return api.UpdateService404Response{}, nil
		}

		s.logger.Error("could not update service", err)

		return nil, errors.Join(err, fmt.Errorf("could not update service"))
	}

	return api.UpdateService204Response{}, nil
}
