// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) updatePeerGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	peerID := chi.URLParam(r, "id")

	page := updatePeerPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathPeersPage,
			Title:                       s.i18n.Translate("Update Peer"),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		Roles: allPeerRoles,
		Form:  &UpdatePeerPageForm{},
	}

	peers, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
	})
	if err != nil {
		s.logger.Error("failed to get peer to update", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	var peer *query.ListPeersPeer

	for _, p := range peers {
		if p.ID != peerID {
			continue
		}

		peer = p
	}

	if peer == nil {
		page.renderNotFound(w)

		return
	}

	page.Form.ID = peer.ID
	page.Form.Name = peer.Name
	page.Form.ManagerAddress = peer.ManagerAddress
	page.Form.Roles = mapPeerRolesToUI(peer.Roles)

	page.render(w)
}

func (s *Server) updatePeerPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	peerID := chi.URLParam(r, "id")

	formValues, err := decodeUpdatePeerPageForm(s.decoder, r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	formValues.ID = peerID

	page := updatePeerPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathPeersPage,
			Title:                       s.i18n.Translate("Update Peer"),
			Description:                 "",
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		Roles: allPeerRoles,
		Form:  formValues,
	}

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	err = s.apiApp.Commands.UpdatePeer.Handle(context.Background(), &command.UpdatePeerArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		AuditLogSource:        auditLogSource,
		ID:                    peerID,
		Name:                  formValues.Name,
		ManagerAddress:        formValues.ManagerAddress,
		Roles:                 peerRolesToCommandRoles(formValues.Roles),
	})
	if err != nil {
		s.logger.Error("update peer", err)

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(s.i18n.Translate("The Peer has been updated"), w)
}
