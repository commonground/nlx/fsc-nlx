// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
)

func (s *Server) publishServiceDelegatedGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	page := publishServiceDelegatedPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathDelegatedServicePublicationsPage,
			Title:                       s.i18n.Translate("Publish Service"),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		DirectoryPeerIDs:    s.getPeerIDs(ctx, getAuthzData(r), authData),
		ServiceNamesForPeer: s.getServiceNamesForPeer(ctx, authzData, authData),
		Form: &PublishServiceDelegatedPageForm{
			DirectoryPeerID: s.DirectoryPeerID,
			ValidFrom:       time.Now().Format("2006-01-02"),
			ValidUntil:      time.Now().Add(Day).Format("2006-01-02"),
		},
	}

	page.render(w)
}

func (s *Server) publishServiceDelegatedPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	formValues, err := decodeDelegatedPublishServicePageForm(s.decoder, r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	page := publishServiceDelegatedPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathDelegatedServicePublicationsPage,
			Title:                       s.i18n.Translate("Publish Service"),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		DirectoryPeerIDs: s.getPeerIDs(ctx, getAuthzData(r), authData),
		Form:             formValues,
	}

	peerInfo := s.getPeerInfo(ctx, getAuthzData(r), authData)

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		AuditLogSource:        auditLogSource,
		HashAlgorithm:         command.HashAlgSHA3_512,
		GroupID:               s.basePage.GroupID,
		ContractNotBefore:     formValues.ValidFrom,
		ContractNotAfter:      formValues.ValidUntil,
		DelegatedServicePublicationGrants: []*command.DelegatedServicePublicationGrant{
			{
				DirectoryPeerID: strings.TrimSpace(formValues.DirectoryPeerID),
				DelegatorPeerID: peerInfo.ID,
				ServicePeerID:   strings.TrimSpace(formValues.ServicePeerID),
				ServiceName:     formValues.Name,
			},
		},
	})
	if err != nil {
		s.logger.Error("create contract for delegated service publication", err)

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(s.i18n.Translate("The Service will be published once the Delegatee accepts the Contract."), w)
}
