// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
)

type peerDetailPage struct {
	*BasePage
	BaseAuthenticatedPage
	Peer              *peerDetailPagePeer
	SuccessMessage    string
	ErrorMessage      string
	ShowConnectButton bool
}

type peerDetailPagePeer struct {
	ID             string
	Name           string
	ManagerAddress string
	Roles          []string
}

func (p *peerDetailPage) render(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/peer-detail.html")
	if err != nil {
		p.logger.Error("failed to parse peer not found page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *peerDetailPage) renderNotFound(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/peer-detail-not-found.html")
	if err != nil {
		p.logger.Error("failed to parse service not found page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *peerDetailPage) renderTemplate(w http.ResponseWriter, templatePath string) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			templatePath,
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
