// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"time"
)

type directoryServiceDetailPage struct {
	*BasePage
	BaseAuthenticatedPage
	Service                    *directoryServiceDetailPageService
	OutwayPublicKeyThumbprints []*directoryServiceDetailPageOutwayPublicKeyThumbprint
	SuccessMessage             string
	ErrorMessage               string
}

type directoryServiceDetailPageService struct {
	Name                         string
	PeerID                       string
	PeerName                     string
	ProviderID                   string
	ProviderName                 string
	IsDelegatedPublication       bool
	ContractDetailsBackButtonURL string
}

type directoryServiceDetailPageOutwayPublicKeyThumbprint struct {
	PublicKeyThumbprint string
	OutwayNames         []string
	OutgoingConnections []*directoryServiceDetailPageOutwayPublicKeyThumbprintOutgoingConnection
}

type directoryServiceDetailPageOutwayPublicKeyThumbprintOutgoingConnection struct {
	RemainingDaysValid   int
	State                string
	Hash                 string
	GrantHashURLFriendly string
	ValidFrom            time.Time
	ValidUntil           time.Time
	CreatedAt            time.Time
}

func (p *directoryServiceDetailPage) render(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/directory-service-detail.html")
	if err != nil {
		p.logger.Error("failed to parse directory service detail page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *directoryServiceDetailPage) renderNotFound(w http.ResponseWriter) {
	p.Title = p.I18n.Translate("Service not found")

	err := p.renderTemplate(w, "templates/directory-service-detail-not-found.html")
	if err != nil {
		p.logger.Error("failed to parse directory service detail not found page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *directoryServiceDetailPage) renderTemplate(w http.ResponseWriter, templatePath string) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/components/contract-status.html",
			templatePath,
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
