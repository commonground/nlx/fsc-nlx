// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"time"
)

type serviceDetailPage struct {
	*BasePage
	BaseAuthenticatedPage
	Service        *serviceDetailPageService
	SuccessMessage string
	ErrorMessage   string
}

type serviceDetailPageService struct {
	Name                string
	EndpointURL         string
	InwayAddress        string
	Publications        []*serviceDetailPageServiceGrant
	IncomingConnections []*serviceDetailPageServiceGrant
}

type serviceDetailPageServiceGrant struct {
	RemainingDaysValid  int
	State               string
	Title               string
	ContractContentHash string
	ValidFrom           time.Time
	ValidUntil          time.Time
	CreatedAt           time.Time
}

func (p *serviceDetailPage) render(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/service-detail.html")
	if err != nil {
		p.logger.Error("failed to parse service not found page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *serviceDetailPage) renderNotFound(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)

	err := p.renderTemplate(w, "templates/service-detail-not-found.html")
	if err != nil {
		p.logger.Error("failed to parse service not found page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *serviceDetailPage) renderTemplate(w http.ResponseWriter, templatePath string) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/components/contract-status.html",
			templatePath,
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
