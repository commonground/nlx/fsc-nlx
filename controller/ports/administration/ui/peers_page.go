// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"
)

type peersPage struct {
	*BasePage
	BaseAuthenticatedPage

	AmountOfPeers uint
	Peers         []*peersPagePeer
	SyncResults   []*syncResultLine
	SyncSuccess   bool
	Error         string
}

type syncResultLine struct {
	Ok       bool
	ErrMsg   string
	PeerID   string
	PeerName string
}

type peersPagePeer struct {
	ID             string
	Name           string
	ManagerAddress string
	Roles          string
}

func (p *peersPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/peers.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
