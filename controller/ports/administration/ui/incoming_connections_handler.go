// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) incomingConnectionsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	const paginationLimit = uint32(10)

	paginationCursor := r.URL.Query().Get("pagination_cursor")

	paginationNextCursor := paginationCursor

	selectedTab := chi.URLParam(r, "selected_tab")

	if selectedTab == "" {
		selectedTab = pageTabActive
	}

	contractStateFilter, err := getContractStateFilterForSelectedTab(selectedTab)
	if err != nil {
		s.logger.Info(err.Error())
		http.Error(w, fmt.Sprintf("%d", http.StatusNotFound), http.StatusNotFound)

		return
	}

	res, nextCursor, err := s.apiApp.Queries.ListIncomingConnections.Handle(ctx, &query.ListIncomingConnectionsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Pagination: &query.ListIncomingConnectionsPagination{
			Cursor:    paginationCursor,
			Limit:     paginationLimit,
			SortOrder: query.SortOrderDescending,
		},
		ContractStates: contractStateFilter,
	})
	if err != nil {
		s.logger.Error("could not render incoming connections page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if nextCursor != "" {
		paginationNextCursor = nextCursor
	}

	peerIDs := []string{}

	for _, c := range res.Connections {
		peerIDs = append(peerIDs, c.GetPeerIDs()...)
	}

	peersInfo, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		PeerIDs:               &peerIDs,
	})
	if err != nil {
		s.logger.Error("could not get details for peers", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	pCollection := newPeersCollection(peersInfo)

	incomingConnections := mapQueryToUIIncomingConnections(res.Connections, pCollection)

	page := incomingConnectionsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathIncomingConnectionsPage,
			Title:                       s.i18n.Translate("Incoming Connections"),
			Description:                 s.i18n.Translate("Overview of connections made to your Services."),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
		Connections:                     incomingConnections,
		AmountOfConnections:             res.TotalCount,
		PaginationNextCursor:            paginationNextCursor,
		PaginationLimit:                 paginationLimit,
		SelectedTab:                     selectedTab,
		ContractDetailBackButtonURLPath: url.QueryEscape(fmt.Sprintf("%s/%s?pagination_cursor=%s", PathIncomingConnectionsPage, selectedTab, paginationCursor)),
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render incoming connections page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func urlFriendlyGrantHash(grantHash string) string {
	// ID must start with alphanumeric character
	prefixedWithAlphaNumeric := fmt.Sprintf("grant-%s", grantHash)

	// ID can not contain $
	withoutDollarSign := strings.ReplaceAll(prefixedWithAlphaNumeric, "$", "")

	return withoutDollarSign
}

func mapQueryToUIIncomingConnections(qConnections []*query.Connection, pCollection *peersCollection) []*incomingConnectionsPageConnection {
	result := []*incomingConnectionsPageConnection{}

	for _, c := range qConnections {
		r := &incomingConnectionsPageConnection{
			ServiceName:          c.ServiceName,
			OutwayPeerID:         c.OutwayPeerID,
			OutwayPeerName:       pCollection.NameForPeerID(c.OutwayPeerID),
			ContentHash:          c.ContentHash,
			GrantHashURLFriendly: urlFriendlyGrantHash(c.GrantHash),
			ValidNotBefore:       c.ValidNotBefore,
			ValidNotAfter:        c.ValidNotAfter,
			RemainingDaysValid:   contractRemainingDaysValid(c.ValidNotBefore, c.ValidNotAfter),
			CreatedAt:            c.CreatedAt,
			State:                c.State,
		}

		if c.DelegatorPeerID != nil {
			r.DelegatorPeerName = pCollection.NameForPeerID(*c.DelegatorPeerID)
			r.DelegatorPeerID = *c.DelegatorPeerID
		}

		if c.ServicePublicationDelegatorPeerID != nil {
			r.ServicePublicationDelegatorPeerID = *c.ServicePublicationDelegatorPeerID
			r.ServicePublicationDelegatorPeerName = pCollection.NameForPeerID(*c.ServicePublicationDelegatorPeerID)
		}

		result = append(result, r)
	}

	return result
}
