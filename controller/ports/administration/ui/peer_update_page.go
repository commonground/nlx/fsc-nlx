// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

type updatePeerPage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage string
	ErrorMessages  []string
	Form           *UpdatePeerPageForm
	Roles          map[string]string
}

type UpdatePeerPageForm struct {
	ID             string
	Name           string
	ManagerAddress string
	Roles          []string
}

func decodeUpdatePeerPageForm(decoder *schema.Decoder, r *http.Request) (*UpdatePeerPageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result UpdatePeerPageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *updatePeerPage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, message := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessages = message

	p.render(w)
}

func (p *updatePeerPage) renderNotFound(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/update-peer-not-found.html")
	if err != nil {
		p.logger.Error("failed to parse service not found page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func (p *updatePeerPage) renderWithSuccess(successMessage string, w http.ResponseWriter) {
	p.SuccessMessage = successMessage
	p.render(w)
}

func (p *updatePeerPage) render(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/update-peer.html")
	if err != nil {
		p.logger.Error("failed to parse service page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func (p *updatePeerPage) renderTemplate(w http.ResponseWriter, templatePath string) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/components/errors.html",
			templatePath,
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
