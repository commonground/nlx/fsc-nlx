// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

const (
	tabInways  = "inways"
	tabOutways = "outways"
)

func (s *Server) inwaysOutwaysHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	tab := chi.URLParam(r, "selected_tab")

	baseAuthenticatedPage := BaseAuthenticatedPage{
		PrimaryNavigationActivePath: PathInwaysOutwaysPage,
		Peer:                        s.getPeerInfo(ctx, authzData, authData),
		Username:                    authData.Username(),
		LogoutURL:                   s.authn.LogoutURL(),
	}

	page := inwaysOutwaysPage{
		BasePage:    s.basePage,
		SelectedTab: tab,
	}

	page.BaseAuthenticatedPage = baseAuthenticatedPage

	switch tab {
	case tabInways:
		err := s.getInways(ctx, &page, authData, authzData)
		if err != nil {
			s.logger.Error("could not render inways page", err)
			http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

			return
		}
	case tabOutways:
		err := s.getOutways(ctx, &page, authData, authzData)
		if err != nil {
			s.logger.Error("could not render outways page", err)
			http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

			return
		}
	default:
		err := s.getInways(ctx, &page, authData, authzData)
		if err != nil {
			s.logger.Error("could not render inways page", err)
			http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

			return
		}
	}

	err := page.render(w)
	if err != nil {
		s.logger.Error("could not render outways page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

//nolint:dupl // looks the same as getOutways but uses inways instead of outways
func (s *Server) getInways(ctx context.Context, page *inwaysOutwaysPage, authData auth.Data, authzData *authorization.RestMetaData) error {
	page.BaseAuthenticatedPage.Title = s.i18n.Translate("Inways")
	page.BaseAuthenticatedPage.Description = s.i18n.Translate("The Inways that have registered with this Controller")

	resp, err := s.apiApp.Queries.ListInways.Handle(ctx, &query.ListInwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		SelfPeerID:            s.peerInfo.ID,
	})
	if err != nil {
		return err
	}

	inways := make([]*inwaysOutwayPageInway, len(resp))
	for i, respInway := range resp {
		inways[i] = &inwaysOutwayPageInway{
			Name:    respInway.Name,
			Address: respInway.Address,
		}
	}

	page.Amount = len(inways)
	page.Inways = inways
	page.SelectedTab = tabInways

	return nil
}

//nolint:dupl // looks the same as getInways but uses outways instead of inways
func (s *Server) getOutways(ctx context.Context, page *inwaysOutwaysPage, authData auth.Data, authzData *authorization.RestMetaData) error {
	page.BaseAuthenticatedPage.Title = s.i18n.Translate("Outways")
	page.BaseAuthenticatedPage.Description = s.i18n.Translate("The Outways that have registered with this Controller")

	resp, err := s.apiApp.Queries.ListOutways.Handle(ctx, &query.ListOutwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		SelfPeerID:            s.peerInfo.ID,
	})
	if err != nil {
		return err
	}

	outways := make([]*inwaysOutwaysPageOutway, len(resp))
	for i, outway := range resp {
		outways[i] = &inwaysOutwaysPageOutway{
			Name:                outway.Name,
			PublicKeyThumbprint: outway.PublicKeyThumbprint,
		}
	}

	page.Amount = len(outways)
	page.Outways = outways
	page.SelectedTab = tabOutways

	return nil
}
