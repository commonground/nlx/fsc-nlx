// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	api_query "gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

const Day = 24 * time.Hour
const htmxReswapHeader = "HX-Reswap"

func (s *Server) addContractGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	peerInfo := s.getPeerInfo(ctx, getAuthzData(r), authData)

	page := getDefaultPageSetup(s, s.basePage, peerInfo, authData, &AddContractsPageForm{
		ValidFrom:  time.Now().Format("2006-01-02"),
		ValidUntil: time.Now().Add(Day).Format("2006-01-02"),
	}, r)

	page.render(w)
}

func (s *Server) addContractPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	peerInfo := s.getPeerInfo(ctx, getAuthzData(r), authData)

	formValues, err := decodeAddContractPageForm(s.decoder, r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	page := getDefaultPageSetup(s, s.basePage, peerInfo, authData, formValues, r)

	outways, err := s.apiApp.Queries.ListOutways.Handle(ctx, &api_query.ListOutwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		SelfPeerID:            peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("failed to retrieve outways", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	owThumbprints := map[string][]string{}

	for _, outway := range outways {
		outwayMap, ok := owThumbprints[outway.PublicKeyThumbprint]
		if !ok {
			owThumbprints[outway.PublicKeyThumbprint] = []string{outway.Name}
		}

		_ = append(outwayMap, outway.Name)
	}

	page.OutwayPublicKeyThumbprints = owThumbprints

	page.PeerIDs = s.getPeerIDs(ctx, getAuthzData(r), authData)
	page.ServiceNames = s.getServiceNames(ctx, getAuthzData(r), authData)

	if formValues.Action == FormActionAddServicePublicationGrant {
		page.Form.ServicePublicationGrants = append(page.Form.ServicePublicationGrants, &AddContractsPageFormServicePublicationGrant{
			ServicePeerID: s.peerInfo.ID,
		})

		w.Header().Add(htmxReswapHeader, "outerHTML show:none")
		page.render(w)

		return
	}

	if formValues.Action == FormActionAddServiceConnectionGrant {
		page.Form.ServiceConnectionGrants = append(page.Form.ServiceConnectionGrants, &AddContractsPageFormServiceConnectionGrant{
			OutwayPeerID: s.peerInfo.ID,
		})

		w.Header().Add(htmxReswapHeader, "outerHTML show:none")
		page.render(w)

		return
	}

	if formValues.Action == FormActionAddDelegatedServiceConnectionGrant {
		page.Form.DelegatedServiceConnectionGrants = append(page.Form.DelegatedServiceConnectionGrants, &AddContractsPageFormDelegatedServiceConnectionGrant{})

		w.Header().Add(htmxReswapHeader, "outerHTML show:none")
		page.render(w)

		return
	}

	if formValues.Action == FormActionAddDelegatedServicePublicationGrant {
		page.Form.DelegatedServicePublicationGrants = append(page.Form.DelegatedServicePublicationGrants, &AddContractsPageFormDelegatedServicePublicationGrant{
			ServicePeerID: s.peerInfo.ID,
		})

		w.Header().Add(htmxReswapHeader, "outerHTML show:none")
		page.render(w)

		return
	}

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
		AuthData:                          authData,
		AuthorizationMetadata:             getAuthzData(r),
		AuditLogSource:                    auditLogSource,
		HashAlgorithm:                     formValues.HashAlgorithm,
		GroupID:                           formValues.GroupID,
		ContractNotBefore:                 formValues.ValidFrom,
		ContractNotAfter:                  formValues.ValidUntil,
		ServicePublicationGrants:          mapServicePublicationGrants(formValues.ServicePublicationGrants),
		ServiceConnectionGrants:           mapServiceConnectionGrants(formValues.ServiceConnectionGrants),
		DelegatedServiceConnectionGrants:  mapDelegatedServiceConnectionGrants(formValues.DelegatedServiceConnectionGrants),
		DelegatedServicePublicationGrants: mapDelegatedServicePublicationGrants(formValues.DelegatedServicePublicationGrants),
	})
	if err != nil {
		s.logger.Error("create contract", err)

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(w, s.i18n.Translate("The contract was added successfully."))
}

func (s *Server) getPeerIDs(ctx context.Context, authzData *authorization.RestMetaData, authData authentication.Data) map[string]string {
	peers, err := s.apiApp.Queries.ListPeers.Handle(ctx, &api_query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
	})
	peerIDList := make(map[string]string)

	if err != nil {
		s.logger.Warn("could not retrieve PeerID list from manager, defaulting to empty list", err)

		return peerIDList
	}

	for _, peer := range peers {
		peerIDList[peer.ID] = peer.Name
	}

	return peerIDList
}

func (s *Server) getServiceNames(ctx context.Context, authzData *authorization.RestMetaData, authData authentication.Data) map[string]string {
	serviceList := make(map[string]string)
	services, err := s.apiApp.Queries.ListServices.Handle(ctx, &api_query.ListServicesArgs{
		AuthData:   authData,
		SelfPeerID: s.getPeerInfo(ctx, authzData, authData).ID,
	})

	if err != nil {
		s.logger.Warn("could not retrieve Service list from manager, defaulting to empty list", err)

		return serviceList
	}

	for _, service := range services {
		serviceList[service.Name] = service.EndpointURL.String()
	}

	return serviceList
}

func getDefaultPageSetup(s *Server, basePage *BasePage, peer *PeerInfo, authData authentication.Data, form *AddContractsPageForm, r *http.Request) addContractsPage {
	return addContractsPage{
		BasePage: basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
			Title:                       s.i18n.Translate("Add new contract"),
			Description:                 s.i18n.Translate("This Contract will be automatically sent to all Peers on the Contract."),
			Peer:                        peer,
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		Groups: []string{basePage.GroupID},
		HashAlgorithms: HashAlgorithms{
			"SHA3-512",
		},
		OutwayPublicKeyThumbprints:                    map[string][]string{},
		Form:                                          form,
		FormActionAddServicePublicationGrant:          FormActionAddServicePublicationGrant,
		FormActionAddServiceConnectionGrant:           FormActionAddServiceConnectionGrant,
		FormActionAddDelegatedServicePublicationGrant: FormActionAddDelegatedServicePublicationGrant,
		FormActionAddDelegatedServiceConnectionGrant:  FormActionAddDelegatedServiceConnectionGrant,
	}
}

func mapServicePublicationGrants(input []*AddContractsPageFormServicePublicationGrant) []*command.ServicePublicationGrant {
	result := make([]*command.ServicePublicationGrant, len(input))

	for i, grant := range input {
		result[i] = &command.ServicePublicationGrant{
			DirectoryPeerID: strings.TrimSpace(grant.DirectoryPeerID),
			ServicePeerID:   strings.TrimSpace(grant.ServicePeerID),
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}

func mapServiceConnectionGrants(input []*AddContractsPageFormServiceConnectionGrant) []*command.ServiceConnectionGrant {
	result := make([]*command.ServiceConnectionGrant, len(input))

	for i, grant := range input {
		result[i] = &command.ServiceConnectionGrant{
			ServicePeerID:                     strings.TrimSpace(grant.ServicePeerID),
			ServicePublicationDelegatorPeerID: strings.TrimSpace(grant.ServicePublicationDelegatorPeerID),
			ServiceName:                       grant.ServiceName,
			OutwayPeerID:                      strings.TrimSpace(grant.OutwayPeerID),
			OutwayPublicKeyThumbprint:         grant.OutwayPublicKeyThumbprint,
		}
	}

	return result
}

func mapDelegatedServiceConnectionGrants(input []*AddContractsPageFormDelegatedServiceConnectionGrant) []*command.DelegatedServiceConnectionGrant {
	result := make([]*command.DelegatedServiceConnectionGrant, len(input))

	for i, grant := range input {
		result[i] = &command.DelegatedServiceConnectionGrant{
			DelegatorPeerID:                   strings.TrimSpace(grant.DelegatorPeerID),
			ServicePeerID:                     strings.TrimSpace(grant.ServicePeerID),
			ServicePublicationDelegatorPeerID: strings.TrimSpace(grant.ServicePublicationDelegatorPeerID),
			ServiceName:                       grant.ServiceName,
			OutwayPeerID:                      strings.TrimSpace(grant.OutwayPeerID),
			OutwayPublicKeyThumbprint:         grant.OutwayPublicKeyThumbprint,
		}
	}

	return result
}

func mapDelegatedServicePublicationGrants(input []*AddContractsPageFormDelegatedServicePublicationGrant) []*command.DelegatedServicePublicationGrant {
	result := make([]*command.DelegatedServicePublicationGrant, len(input))

	for i, grant := range input {
		result[i] = &command.DelegatedServicePublicationGrant{
			DirectoryPeerID: strings.TrimSpace(grant.DirectoryPeerID),
			DelegatorPeerID: strings.TrimSpace(grant.DelegatorPeerID),
			ServicePeerID:   strings.TrimSpace(grant.ServicePeerID),
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}
