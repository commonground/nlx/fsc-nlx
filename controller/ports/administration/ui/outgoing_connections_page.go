// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"
	"time"
)

type outgoingConnectionsPage struct {
	*BasePage
	BaseAuthenticatedPage

	AmountOfConnections             int
	Connections                     []*outgoingConnectionsPageConnection
	PaginationNextCursor            string
	PaginationLimit                 uint32
	ContractDetailBackButtonURLPath string
	SelectedTab                     string
}

type outgoingConnectionsPageConnection struct {
	ServiceName                         string
	ServicePeerID                       string
	ServicePeerName                     string
	DelegatorPeerID                     string
	DelegatorPeerName                   string
	ServicePublicationDelegatorPeerID   string
	ServicePublicationDelegatorPeerName string
	ContentHash                         string
	GrantHashURLFriendly                string
	CreatedAt                           time.Time
	RemainingDaysValid                  int
	ValidNotBefore                      time.Time
	ValidNotAfter                       time.Time
	State                               string
}

func (p *outgoingConnectionsPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/components/contract-status.html",
			"templates/outgoing-connections.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
