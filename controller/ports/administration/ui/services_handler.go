// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

//nolint:dupl // looks the same as directoryHandler but is different.
func (s *Server) servicesHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	peerInfo := s.getPeerInfo(ctx, authzData, authData)

	resp, err := s.apiApp.Queries.ListServices.Handle(ctx, &query.ListServicesArgs{
		AuthData:   authData,
		SelfPeerID: peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("could not render services page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	services := mapQueryToUIServices(resp)

	page := servicesPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
			Title:                       s.i18n.Translate("Services"),
			Description:                 "",
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
		AmountOfServices: len(services),
		Services:         services,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render services page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func mapQueryToUIServices(qServices query.Services) []*servicesPageService {
	contracts := make([]*servicesPageService, len(qServices))

	for i, s := range qServices {
		contracts[i] = &servicesPageService{
			Name:        s.Name,
			IsPublished: s.IsPublished,
		}
	}

	return contracts
}
