// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) contractDetailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	contentHash := chi.URLParam(r, "hash")

	contract, err := s.apiApp.Queries.GetContract.Handle(ctx, &query.GetContractArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		ContentHash:           contentHash,
	})
	if err != nil {
		s.logger.Error("could not get contract", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	contractDistributions, err := s.apiApp.Queries.ListContractDistributions.Handle(ctx, &query.ListContractDistributionsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		ContentHash:           contentHash,
	})
	if err != nil {
		s.logger.Error("could not get contractDistributions", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	showContractDistributions := len(contractDistributions) > 0

	peersInfo, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		PeerIDs:               &contract.Peers,
	})
	if err != nil {
		s.logger.Error("could not get details for peers", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	pCollection := newPeersCollection(peersInfo)

	organizations := getPeers(contract, contractDistributions, pCollection)
	servicePublicationGrants := getServicePublicationGrants(contract.ServicePublicationGrants, pCollection)
	serviceConnectionGrants := getServiceConnectionGrants(contract.ServiceConnectionGrants, pCollection)
	delegatedServiceConnectionGrants := getDelegatedServiceConnectionGrants(contract.DelegatedServiceConnectionGrants, pCollection)
	delegatedServicePublicationGrants := getDelegatedServicePublicationGrants(contract.DelegatedServicePublicationGrants, pCollection)

	contractActionsDisabled := contract.State == query.ContractStateExpired ||
		contract.State == query.ContractStateRejected ||
		contract.State == query.ContractStateRevoked

	backButtonURLPath := PathContractsPage

	if r.URL.Query().Get("back_button_url") != "" {
		backButtonURLPath = r.URL.Query().Get("back_button_url")
	}

	page := contractDetailPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
			Title:                       s.i18n.Translate("Contract"),
			Description:                 s.i18n.Translate("View all details of the Contract."),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		BackButtonURLPath:                 backButtonURLPath,
		IV:                                contract.IV,
		State:                             convertStateToHumanReadableText(contract.State),
		Hash:                              contentHash,
		HashAlgorithm:                     hashAlgorithmString(contract.HashAlgorithm),
		GroupID:                           contract.GroupID,
		Organizations:                     organizations,
		CreatedAt:                         contract.CreatedAt.UTC(),
		ValidFrom:                         contract.ValidFrom.UTC(),
		ValidUntil:                        contract.ValidUntil.UTC(),
		ServicePublicationGrants:          servicePublicationGrants,
		ServiceConnectionGrants:           serviceConnectionGrants,
		DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
		DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		DisableRejectButton:               contract.HasRejected || contract.HasAccepted || contractActionsDisabled,
		DisableAcceptButton:               contract.HasRejected || contract.HasAccepted || contract.HasRevoked || contractActionsDisabled,
		DisableRevokeButton:               contract.HasRevoked || !contract.HasAccepted || contractActionsDisabled,
		IsActive:                          contract.State == query.ContractStateValid,
		ShowContractDistributions:         showContractDistributions,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render contract detail page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

type peersCollection struct {
	peers query.ListPeersPeers
}

func newPeersCollection(peers query.ListPeersPeers) *peersCollection {
	return &peersCollection{
		peers: peers,
	}
}

func (p *peersCollection) NameForPeerID(pID string) string {
	peerInfo, ok := p.peers[pID]
	if !ok {
		return ""
	}

	return peerInfo.Name
}

func convertStateToHumanReadableText(input query.ContractState) string {
	switch input {
	case query.ContractStateRejected:
		return "Rejected"
	case query.ContractStateRevoked:
		return "Revoked"
	case query.ContractStateProposed:
		return "Proposed"
	case query.ContractStateValid:
		return "Valid"
	case query.ContractStateExpired:
		return "Expired"
	default:
		return fmt.Sprintf("unknown state %q", input)
	}
}

func hashAlgorithmString(hashAlg query.HashAlg) string {
	switch hashAlg {
	case query.HashAlgSHA3_512:
		return "SHA3-512"
	default:
		return "Unknown"
	}
}

func getPeers(contract *query.Contract, contractDistributions query.ContractDistributions, peers *peersCollection) []*Peer {
	peerMaps := map[string]*Peer{}

	for _, peerID := range contract.Peers {
		contractPeer := &Peer{
			Name:   peers.NameForPeerID(peerID),
			PeerID: peerID,
		}

		signature, ok := contract.RejectSignatures[peerID]
		if ok {
			contractPeer.RejectSignatureSignedAt = signature.SignedAt.UTC().String()
		}

		signature, ok = contract.AcceptSignatures[peerID]
		if ok {
			contractPeer.AcceptSignatureSignedAt = signature.SignedAt.UTC().String()
		}

		signature, ok = contract.RevokeSignatures[peerID]
		if ok {
			contractPeer.RevokeSignatureSignedAt = signature.SignedAt.UTC().String()
		}

		contractDistributionPerAction, ok := contractDistributions[peerID]
		if ok {
			contractPeer.FailedDistribution = make(map[Action]*FailedDistribution)
			for action, contractDistribution := range contractDistributionPerAction {
				contractPeer.FailedDistribution[convertActionToHumanReadableText(action)] = &FailedDistribution{
					ContentHash: contractDistribution.ContentHash,
					Signature:   contractDistribution.Signature,
					Action:      convertActionToHumanReadableText(contractDistribution.Action),
				}
			}
		}

		peerMaps[peerID] = contractPeer
	}

	result := make([]*Peer, 0)

	for _, peer := range peerMaps {
		result = append(result, peer)
	}

	return result
}

func convertActionToHumanReadableText(input query.Action) Action {
	switch input {
	case query.SubmitContract:
		return SubmitContract
	case query.SubmitAcceptSignature:
		return SubmitAcceptSignature
	case query.SubmitRejectSignature:
		return SubmitRejectSignature
	case query.SubmitRevokeSignature:
		return SubmitRevokeSignatureAction
	}

	return ""
}

func getServicePublicationGrants(grants []*query.ServicePublicationGrant, pCollection *peersCollection) []*ServicePublicationGrant {
	result := make([]*ServicePublicationGrant, len(grants))

	for i, grant := range grants {
		result[i] = &ServicePublicationGrant{
			Hash:              grant.Hash,
			HashURLFriendly:   urlFriendlyGrantHash(grant.Hash),
			DirectoryPeerID:   grant.DirectoryPeerID,
			DirectoryPeerName: pCollection.NameForPeerID(grant.DirectoryPeerID),
			ServicePeerID:     grant.ServicePeerID,
			ServicePeerName:   pCollection.NameForPeerID(grant.ServicePeerID),
			ServiceName:       grant.ServiceName,
		}
	}

	return result
}

func getServiceConnectionGrants(grants []*query.ServiceConnectionGrant, pCollection *peersCollection) []*ServiceConnectionGrant {
	result := make([]*ServiceConnectionGrant, len(grants))

	for i, grant := range grants {
		result[i] = &ServiceConnectionGrant{
			Hash:                                grant.Hash,
			HashURLFriendly:                     urlFriendlyGrantHash(grant.Hash),
			ServicePeerID:                       grant.ServicePeerID,
			ServicePeerName:                     pCollection.NameForPeerID(grant.ServicePeerID),
			ServicePublicationDelegatorPeerID:   grant.ServicePublicationDelegatorPeerID,
			ServicePublicationDelegatorPeerName: pCollection.NameForPeerID(grant.ServicePublicationDelegatorPeerID),
			ServiceName:                         grant.ServiceName,
			OutwayPeerID:                        grant.OutwayPeerID,
			OutwayPeerName:                      pCollection.NameForPeerID(grant.OutwayPeerID),
			OutwayPublicKeyThumbprint:           grant.OutwayPublicKeyThumbprint,
		}
	}

	return result
}

func getDelegatedServiceConnectionGrants(grants []*query.DelegatedServiceConnectionGrant, pCollection *peersCollection) []*DelegatedServiceConnectionGrant {
	result := make([]*DelegatedServiceConnectionGrant, len(grants))

	for i, grant := range grants {
		result[i] = &DelegatedServiceConnectionGrant{
			Hash:                                grant.Hash,
			HashURLFriendly:                     urlFriendlyGrantHash(grant.Hash),
			DelegatorPeerID:                     grant.DelegatorPeerID,
			DelegatorPeerName:                   pCollection.NameForPeerID(grant.DelegatorPeerID),
			OutwayPeerID:                        grant.OutwayPeerID,
			OutwayPeerName:                      pCollection.NameForPeerID(grant.OutwayPeerID),
			OutwayPublicKeyThumbprint:           grant.OutwayPublicKeyThumbprint,
			ServicePeerID:                       grant.ServicePeerID,
			ServicePeerName:                     pCollection.NameForPeerID(grant.ServicePeerID),
			ServicePublicationDelegatorPeerID:   grant.ServicePublicationDelegatorPeerID,
			ServicePublicationDelegatorPeerName: pCollection.NameForPeerID(grant.ServicePublicationDelegatorPeerID),
			ServiceName:                         grant.ServiceName,
		}
	}

	return result
}

func getDelegatedServicePublicationGrants(grants []*query.DelegatedServicePublicationGrant, pCollection *peersCollection) []*DelegatedServicePublicationGrant {
	result := make([]*DelegatedServicePublicationGrant, len(grants))

	for i, grant := range grants {
		result[i] = &DelegatedServicePublicationGrant{
			Hash:              grant.Hash,
			HashURLFriendly:   urlFriendlyGrantHash(grant.Hash),
			DirectoryPeerID:   grant.DirectoryPeerID,
			DirectoryPeerName: pCollection.NameForPeerID(grant.DirectoryPeerID),
			DelegatorPeerID:   grant.DelegatorPeerID,
			DelegatorPeerName: pCollection.NameForPeerID(grant.DelegatorPeerID),
			ServicePeerID:     grant.ServicePeerID,
			ServicePeerName:   pCollection.NameForPeerID(grant.ServicePeerID),
			ServiceName:       grant.ServiceName,
		}
	}

	return result
}
