// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

type addPeerPage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage string
	ErrorMessages  []string
	Form           *AddPeerPageForm
	Roles          map[string]string
}

type AddPeerPageForm struct {
	Name           string
	ManagerAddress string
	ID             string
	Roles          []string
}

func decodeAddPeerPageForm(decoder *schema.Decoder, r *http.Request) (*AddPeerPageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result AddPeerPageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *addPeerPage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, message := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessages = message

	p.render(w)

}

func (p *addPeerPage) renderWithSuccess(successMessage string, w http.ResponseWriter) {
	p.SuccessMessage = successMessage
	p.render(w)
}

func (p *addPeerPage) render(w http.ResponseWriter) {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/add-peer.html",
			"templates/components/errors.html",
		)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)
		return
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)
		return
	}
}
