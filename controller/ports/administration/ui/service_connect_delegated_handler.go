// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
)

func (s *Server) connectToServiceDelegatedGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	outwayPublicKeyThumbprint := r.URL.Query().Get("outwayPublicKeyThumbprint")

	page := connectToServiceDelegatedPage{
		ActionAddOutwayPublicKeyThumbprint: formActionAddOutwayPublicKeyThumbprint,
		BasePage:                           s.basePage,
		OutwayPublicKeyThumbprints:         map[string][]string{},
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathDelegatedConnectionsPage,
			Title:                       s.i18n.Translate("Delegate a Service Connection"),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			CsrfToken:                   csrf.Token(r),
		},
		Form: &ConnectToServiceDelegatedPageForm{
			OutwayPublicKeyThumbprints: []string{outwayPublicKeyThumbprint},
			ValidFrom:                  time.Now().Format("2006-01-02"),
			ValidUntil:                 time.Now().Add(Day).Format("2006-01-02"),
		},
		PeerIDs: s.getPeerIDs(ctx, authzData, authData),
	}

	page.render(w)
}

func (s *Server) connectToServiceDelegatedPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	formValues, err := decodeConnectToServiceDelegatedPageForm(s.decoder, r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	page := connectToServiceDelegatedPage{
		ActionAddOutwayPublicKeyThumbprint: formActionAddOutwayPublicKeyThumbprint,
		BasePage:                           s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathDelegatedConnectionsPage,
			Title:                       s.i18n.Translate("Delegate a Service Connection"),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			CsrfToken:                   csrf.Token(r),
		},
		Form:    formValues,
		PeerIDs: s.getPeerIDs(ctx, authzData, authData),
	}

	if formValues.Action == formActionAddOutwayPublicKeyThumbprint {
		formValues.OutwayPublicKeyThumbprints = append(formValues.OutwayPublicKeyThumbprints, "")
		page.Form = formValues

		page.render(w)

		return
	}

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if len(formValues.OutwayPublicKeyThumbprints) < 1 {
		page.renderWithError(w, errors.New("must provide at least one Outway Public Key thumbprint to connect with Service"))

		return
	}

	err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
		AuthData:                         authData,
		AuthorizationMetadata:            authzData,
		AuditLogSource:                   auditLogSource,
		HashAlgorithm:                    command.HashAlgSHA3_512,
		GroupID:                          s.basePage.GroupID,
		ContractNotBefore:                formValues.ValidFrom,
		ContractNotAfter:                 formValues.ValidUntil,
		DelegatedServiceConnectionGrants: createDelegatedServiceConnectionGrants(formValues.OutwayPeerID, formValues.ServiceDelegatorPeerID, formValues.ServiceProviderPeerID, formValues.ServiceName, page.Peer.ID, formValues.OutwayPublicKeyThumbprints),
	})
	if err != nil {
		s.logger.Error("create contract for delegated service connection", err)

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(s.i18n.Translate("The contract for the Delegated Service connection has been created"), w)
}
