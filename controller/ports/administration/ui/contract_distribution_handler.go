/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package uiport

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) contractDistributionHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	contentHash := chi.URLParam(r, "hash")
	peerID := chi.URLParam(r, "peerID")

	page := contractDistributionPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
			Title:                       s.i18n.Translate("Distribution not found."),
			Description:                 "",
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		PeerID: peerID,
	}

	peers := s.getPeerIDs(ctx, authzData, authData)

	contractDistributions, err := s.apiApp.Queries.ListContractDistributions.Handle(ctx, &query.ListContractDistributionsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		ContentHash:           contentHash,
	})
	if err != nil {
		if errors.Is(err, manager.ErrContractDistributionsNotFoundError) {
			page.renderNotFound(w)
			return
		}

		s.logger.Error("could not get contractDistributions", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	contractDistributionForPeer := contractDistributions[peerID]

	contractDistributionPerAction := make(map[Action]*contractDistributionDetails)
	for action, contractDistribution := range contractDistributionForPeer {
		var na *time.Time

		if contractDistribution.NextAttempt != nil {
			utc := contractDistribution.NextAttempt.UTC()
			na = &utc
		}

		contractDistributionPerAction[convertActionToHumanReadableText(action)] = &contractDistributionDetails{
			ContentHash: contractDistribution.ContentHash,
			Signature:   contractDistribution.Signature,
			Action:      convertActionToHumanReadableText(contractDistribution.Action),
			Attempts:    contractDistribution.Attempts,
			LastAttempt: contractDistribution.LastAttempt.UTC(),
			Reason:      contractDistribution.Reason,
			NextAttempt: na,
		}
	}

	page.PeerName = peers[peerID]
	page.ContentHash = contentHash
	page.ContractDistribution = contractDistributionPerAction

	page.Title = fmt.Sprintf("%s %s", s.i18n.Translate("Error sending to"), peers[peerID])

	page.render(w)
}
