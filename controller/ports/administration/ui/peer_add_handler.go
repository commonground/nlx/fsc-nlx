// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"
	"slices"
	"strings"

	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
)

func (s *Server) addPeerGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	selectedRolesParam := r.URL.Query().Get("roles")
	selectedRoles := strings.Split(selectedRolesParam, ",")

	page := addPeerPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathPeersPage,
			Title:                       s.i18n.Translate("Add new Peer"),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		Roles: allPeerRoles,
		Form: &AddPeerPageForm{
			ID:             "",
			Name:           "",
			ManagerAddress: "",
			Roles:          selectedRoles,
		},
	}

	page.render(w)
}

func (s *Server) addPeerPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	formValues, err := decodeAddPeerPageForm(s.decoder, r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	page := addPeerPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathPeersPage,
			Title:                       s.i18n.Translate("Add new Peer"),
			Description:                 "",
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		Roles: allPeerRoles,
		Form:  formValues,
	}

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	err = s.apiApp.Commands.CreatePeer.Handle(context.Background(), &command.CreatePeerArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		AuditLogSource:        auditLogSource,
		Name:                  formValues.Name,
		PeerID:                strings.TrimSpace(formValues.ID),
		ManagerAddress:        formValues.ManagerAddress,
		Roles:                 peerRolesToCommandRoles(formValues.Roles),
	})
	if err != nil {
		s.logger.Error("create peer", err)

		page.renderWithError(w, err)

		return
	}

	if slices.Contains(formValues.Roles, "directory") {
		page.renderWithSuccess(s.i18n.Translate("The Peer has been added as a Directory"), w)
	} else {
		page.renderWithSuccess(s.i18n.Translate("The Peer has been added"), w)
	}

}
