// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

type contractsPage struct {
	*BasePage
	BaseAuthenticatedPage

	GrantTypeFilters     map[query.GrantType]string
	GrantType            string
	Contracts            []*contractsPageContract
	PaginationNextCursor string
}

type contractsPageContract struct {
	IV                                string
	RemainingDaysValid                int
	Hash                              string
	CreatedAt                         time.Time
	State                             string
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
}

func (p *contractsPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/contracts.html",
			"templates/components/contract-status.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
