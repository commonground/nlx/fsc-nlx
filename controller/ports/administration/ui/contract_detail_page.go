// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"
	"time"
)

type contractDetailPage struct {
	*BasePage
	BaseAuthenticatedPage

	BackButtonURLPath                 string
	IV                                string
	State                             string
	Hash                              string
	HashAlgorithm                     string
	GroupID                           string
	CreatedAt                         time.Time
	ValidFrom                         time.Time
	ValidUntil                        time.Time
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	Organizations                     []*Peer
	DisableRejectButton               bool
	DisableAcceptButton               bool
	DisableRevokeButton               bool
	IsActive                          bool
	ShowContractDistributions         bool
}

type Peer struct {
	Name                    string
	PeerID                  string
	RejectSignatureSignedAt string
	AcceptSignatureSignedAt string
	RevokeSignatureSignedAt string
	FailedDistribution      map[Action]*FailedDistribution
}

type ServicePublicationGrant struct {
	Hash              string
	HashURLFriendly   string
	DirectoryPeerID   string
	DirectoryPeerName string
	ServicePeerID     string
	ServicePeerName   string
	ServiceName       string
}

type ServiceConnectionGrant struct {
	Hash                                string
	HashURLFriendly                     string
	ServicePeerID                       string
	ServicePeerName                     string
	ServicePublicationDelegatorPeerID   string
	ServicePublicationDelegatorPeerName string
	ServiceName                         string
	OutwayPeerID                        string
	OutwayPeerName                      string
	OutwayPublicKeyThumbprint           string
}

type DelegatedServiceConnectionGrant struct {
	Hash                                string
	HashURLFriendly                     string
	DelegatorPeerID                     string
	DelegatorPeerName                   string
	OutwayPeerID                        string
	OutwayPeerName                      string
	OutwayPublicKeyThumbprint           string
	ServicePeerID                       string
	ServicePeerName                     string
	ServicePublicationDelegatorPeerID   string
	ServicePublicationDelegatorPeerName string
	ServiceName                         string
}

type DelegatedServicePublicationGrant struct {
	Hash              string
	HashURLFriendly   string
	DirectoryPeerID   string
	DirectoryPeerName string
	DelegatorPeerID   string
	DelegatorPeerName string
	ServicePeerID     string
	ServicePeerName   string
	ServiceName       string
}

type FailedDistribution struct {
	ContentHash    string
	Signature      string
	Action         Action
	HTTPStatusCode int
}

type Action string

const (
	SubmitContract              Action = "Submit Contract"
	SubmitAcceptSignature       Action = "Submit Accept Signature"
	SubmitRejectSignature       Action = "Submit Reject Signature"
	SubmitRevokeSignatureAction Action = "Submit Revoke Signature"
)

func (p *contractDetailPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/contract-detail.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
