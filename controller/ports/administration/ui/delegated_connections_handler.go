// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) delegatedConnectionsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	const paginationLimit = uint32(10)

	paginationCursor := r.URL.Query().Get("pagination_cursor")

	paginationNextCursor := paginationCursor

	selectedTab := chi.URLParam(r, "selected_tab")

	if selectedTab == "" {
		selectedTab = pageTabActive
	}

	contractStateFilter, err := getContractStateFilterForSelectedTab(selectedTab)
	if err != nil {
		s.logger.Info(err.Error())
		http.Error(w, fmt.Sprintf("%d", http.StatusNotFound), http.StatusNotFound)

		return
	}

	resp, nextCursor, err := s.apiApp.Queries.ListDelegatedConnections.Handle(ctx, &query.ListDelegatedConnectionsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Pagination: &query.ListDelegatedConnectionsPagination{
			Cursor: paginationCursor,
			Limit:  paginationLimit,
		},
		ContractStates: contractStateFilter,
	})
	if err != nil {
		s.logger.Error("could not render delegated connections page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if nextCursor != "" {
		paginationNextCursor = nextCursor
	}

	peerIDs := []string{}

	for _, c := range resp.Connections {
		peerIDs = append(peerIDs, c.GetPeerIDs()...)
	}

	peersInfo, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		PeerIDs:               &peerIDs,
	})
	if err != nil {
		s.logger.Error("could not get details for peers", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	pCollection := newPeersCollection(peersInfo)

	connections := mapQueryToUIDelegatedConnections(resp.Connections, pCollection)

	page := delegatedConnectionsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathDelegatedConnectionsPage,
			Title:                       s.i18n.Translate("Delegated Connections"),
			Description:                 s.i18n.Translate("Overview of the connections that are made by another organization on your behalf."),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
		AmountOfConnections:             resp.TotalCount,
		Connections:                     connections,
		PaginationNextCursor:            paginationNextCursor,
		PaginationLimit:                 paginationLimit,
		SelectedTab:                     selectedTab,
		ContractDetailBackButtonURLPath: url.QueryEscape(fmt.Sprintf("%s/%s?pagination_cursor=%s", PathDelegatedConnectionsPage, selectedTab, paginationCursor)),
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render delegated connections page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

//nolint:dupl // looks the same as mapQueryToUIIncomingConnections but the data structure is different
func mapQueryToUIDelegatedConnections(qConnections []*query.Connection, pCollection *peersCollection) []*delegatedConnectionsPageConnection {
	result := []*delegatedConnectionsPageConnection{}

	for _, c := range qConnections {
		r := &delegatedConnectionsPageConnection{
			OutwayPeerID:         c.OutwayPeerID,
			OutwayPeerName:       pCollection.NameForPeerID(c.OutwayPeerID),
			ServiceName:          c.ServiceName,
			ServicePeerID:        c.ServicePeerID,
			ServicePeerName:      pCollection.NameForPeerID(c.ServicePeerID),
			ContentHash:          c.ContentHash,
			GrantHashURLFriendly: urlFriendlyGrantHash(c.GrantHash),
			ValidNotBefore:       c.ValidNotBefore,
			ValidNotAfter:        c.ValidNotAfter,
			RemainingDaysValid:   contractRemainingDaysValid(c.ValidNotBefore, c.ValidNotAfter),
			CreatedAt:            c.CreatedAt,
			State:                c.State,
		}

		if c.ServicePublicationDelegatorPeerID != nil {
			r.ServicePublicationDelegatorPeerID = *c.ServicePublicationDelegatorPeerID
			r.ServicePublicationDelegatorPeerName = pCollection.NameForPeerID(*c.ServicePublicationDelegatorPeerID)
		}

		result = append(result, r)
	}

	return result
}
