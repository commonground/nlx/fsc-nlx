// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"embed"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/gorilla/csrf"
	"github.com/gorilla/schema"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/common/slogerrorwriter"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/i18n"
	texti18n "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/i18n/text"
)

//go:embed templates/**
var tplFolder embed.FS

type Server struct {
	locale          string
	apiApp          *apiapp.Application
	i18n            i18n.I18n
	logger          *logger.Logger
	httpServer      *http.Server
	basePage        *BasePage
	peerInfo        *PeerInfo
	decoder         *schema.Decoder
	authn           authentication.Authenticator
	handler         http.Handler
	DirectoryPeerID string
}

type NewServerArgs struct {
	Locale                string
	StaticPath            string
	GroupID               string
	CsrfAuthKey           string
	CsrfProtectionEnabled bool
	CsrfUnsafeHTTP        bool
	ServeAddress          string
	Logger                *logger.Logger
	APIApp                *apiapp.Application
	Authenticator         authentication.Authenticator
	DirectoryPeerID       string
	AssetsMap             AssetMap
}

func New(_ context.Context, args *NewServerArgs) (*Server, error) {
	if args.Locale == "" {
		return nil, fmt.Errorf("locale must be set (nl/en)")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger cannot be nil")
	}

	if args.APIApp == nil {
		return nil, fmt.Errorf("API app cannot be nil")
	}

	if args.Authenticator == nil {
		return nil, fmt.Errorf("authenticator cannot be nil")
	}

	if args.AssetsMap == nil {
		return nil, fmt.Errorf("assets map cannot be nil")
	}

	translations, err := texti18n.New(args.Locale)
	if err != nil {
		return nil, errors.Wrap(err, "unable to create new text i18n instance")
	}

	basePage, err := NewBasePage(args.StaticPath, translations, args.GroupID, args.Logger, args.AssetsMap)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create base page")
	}

	decoder := schema.NewDecoder()
	decoder.IgnoreUnknownKeys(true)

	server := &Server{
		locale:          args.Locale,
		logger:          args.Logger,
		i18n:            translations,
		apiApp:          args.APIApp,
		basePage:        basePage,
		authn:           args.Authenticator,
		decoder:         decoder,
		DirectoryPeerID: args.DirectoryPeerID,
	}

	server.setupHTTPServer(args.ServeAddress, args.CsrfAuthKey, args.StaticPath, args.CsrfProtectionEnabled, args.CsrfUnsafeHTTP)

	decoder.IgnoreUnknownKeys(true)

	return server, nil
}

//nolint:funlen // there are a lot of routes
func (s *Server) setupHTTPServer(address, csrfAuthKey, staticPath string, csrfEnabled, csrfUnsafeHTTP bool) {
	r := chi.NewRouter()

	if csrfEnabled {
		opts := []csrf.Option{
			csrf.FieldName("csrfToken"),
		}

		if csrfUnsafeHTTP {
			opts = append(opts, csrf.Secure(false))
		}

		CSRF := csrf.Protect([]byte(csrfAuthKey), opts...)
		r.Use(CSRF)
	}

	r.Use(middleware.Compress(compressionLevel))

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/directory", http.StatusTemporaryRedirect)
	})
	r.Get("/actions", s.authn.OnlyAuthenticated(s.actionsHandler))
	r.Get("/audit-logs", s.authn.OnlyAuthenticated(s.auditLogsHandler))
	r.Get("/services", s.authn.OnlyAuthenticated(s.servicesHandler))
	r.Get("/services/{name}", s.authn.OnlyAuthenticated(s.serviceDetailHandler))
	r.Get("/services/add-service", s.authn.OnlyAuthenticated(s.addServiceGetHandler))
	r.Post("/services/add-service", s.authn.OnlyAuthenticated(s.addServicePostHandler))
	r.Get("/services/{name}/edit", s.authn.OnlyAuthenticated(s.updateServiceGetHandler))
	r.Post("/services/{name}/edit", s.authn.OnlyAuthenticated(s.updateServicePostHandler))
	r.Get("/services/{name}/publish", s.authn.OnlyAuthenticated(s.publishServiceGetHandler))
	r.Post("/services/{name}/publish", s.authn.OnlyAuthenticated(s.publishServicePostHandler))
	r.Post("/services/publish", s.authn.OnlyAuthenticated(s.publishServicePostHandler))
	r.Get("/services/publish", s.authn.OnlyAuthenticated(s.publishServiceGetHandler))
	r.Get("/services/publish/delegate", s.authn.OnlyAuthenticated(s.publishServiceDelegatedGetHandler))
	r.Post("/services/publish/delegate", s.authn.OnlyAuthenticated(s.publishServiceDelegatedPostHandler))
	r.Get("/services/connection/connect", s.authn.OnlyAuthenticated(s.connectToServiceGetHandler))
	r.Get("/services/connection/{peerID}/connect", s.authn.OnlyAuthenticated(s.connectToServiceGetHandler))
	r.Post("/services/connection/{peerID}/connect", s.authn.OnlyAuthenticated(s.connectToServicePostHandler))
	r.Get("/services/connection/delegate", s.authn.OnlyAuthenticated(s.connectToServiceDelegatedGetHandler))
	r.Post("/services/connection/delegate", s.authn.OnlyAuthenticated(s.connectToServiceDelegatedPostHandler))
	r.Get("/services/publications", s.authn.OnlyAuthenticated(s.servicePublicationsHandler))
	r.Get("/services/publications/{selected_tab}", s.authn.OnlyAuthenticated(s.servicePublicationsHandler))
	r.Get("/directory", s.authn.OnlyAuthenticated(s.directoryHandler))
	r.Get("/directory/{directoryPeerID}/{peerID}/{serviceName}/delegator/{delegatorID}", s.authn.OnlyAuthenticated(s.directoryServiceDetailHandler))
	r.Get("/directory/{directoryPeerID}/{peerID}/{serviceName}", s.authn.OnlyAuthenticated(s.directoryServiceDetailHandler))
	r.Get("/contracts", s.authn.OnlyAuthenticated(s.contractsHandler))
	r.Get("/contract/{hash}", s.authn.OnlyAuthenticated(s.contractDetailHandler))
	r.Get("/contract/{hash}/distributions/{peerID}", s.authn.OnlyAuthenticated(s.contractDistributionHandler))
	r.Post("/contract/{hash}/retries/{peerID}/{action}", s.authn.OnlyAuthenticated(s.contractRetryHandler))
	r.Get("/contracts/add", s.authn.OnlyAuthenticated(s.addContractGetHandler))
	r.Post("/contracts/add", s.authn.OnlyAuthenticated(s.addContractPostHandler))
	r.Post("/contract/{hash}/sign", s.authn.OnlyAuthenticated(s.signContractPostHandler))
	r.Get("/peers", s.authn.OnlyAuthenticated(s.peersHandler))
	r.Get("/peers/add", s.authn.OnlyAuthenticated(s.addPeerGetHandler))
	r.Post("/peers/add", s.authn.OnlyAuthenticated(s.addPeerPostHandler))
	r.Get("/peers/{id}", s.authn.OnlyAuthenticated(s.peerDetailHandler))
	r.Get("/peers/{id}/edit", s.authn.OnlyAuthenticated(s.updatePeerGetHandler))
	r.Post("/peers/{id}/edit", s.authn.OnlyAuthenticated(s.updatePeerPostHandler))
	r.Get("/transaction-logs", s.authn.OnlyAuthenticated(s.transactionLogsHandler))
	r.Get("/incoming-connections", s.authn.OnlyAuthenticated(s.incomingConnectionsHandler))
	r.Get("/incoming-connections/{selected_tab}", s.authn.OnlyAuthenticated(s.incomingConnectionsHandler))
	r.Get("/outgoing-connections", s.authn.OnlyAuthenticated(s.outgoingConnectionsHandler))
	r.Get("/outgoing-connections/{selected_tab}", s.authn.OnlyAuthenticated(s.outgoingConnectionsHandler))
	r.Get("/delegated-connections", s.authn.OnlyAuthenticated(s.delegatedConnectionsHandler))
	r.Get("/delegated-connections/{selected_tab}", s.authn.OnlyAuthenticated(s.delegatedConnectionsHandler))
	r.Get("/delegated-publications", s.authn.OnlyAuthenticated(s.delegatedServicePublicationsHandler))
	r.Get("/delegated-publications/{selected_tab}", s.authn.OnlyAuthenticated(s.delegatedServicePublicationsHandler))
	r.Get("/health", s.healthCheckHandler)
	r.Get("/inways-outways/{selected_tab}", s.authn.OnlyAuthenticated(s.inwaysOutwaysHandler))
	r.Get("/inways-outways", s.authn.OnlyAuthenticated(s.inwaysOutwaysHandler))

	s.authn.MountRoutes(r)

	filesDir := http.Dir(staticPath)
	r.Handle("/*", http.FileServer(filesDir))

	const readHeaderTimeout = 5 * time.Second

	s.handler = r

	s.httpServer = &http.Server{
		Addr:              address,
		Handler:           r,
		ReadHeaderTimeout: readHeaderTimeout,
		ErrorLog:          log.New(slogerrorwriter.New(s.logger.Logger), "", 0),
	}
}

func (s *Server) Handler() http.Handler {
	return s.handler
}

const compressionLevel = 5

func (s *Server) ListenAndServe() error {
	err := s.httpServer.ListenAndServe()
	if !errors.Is(err, http.ErrServerClosed) {
		return err
	}

	return nil
}

func (s *Server) Shutdown(ctx context.Context) error {
	err := s.httpServer.Shutdown(ctx)
	if !errors.Is(err, http.ErrServerClosed) {
		return err
	}

	return nil
}
