// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package texti18n

func (s *I18n) Locale() string {
	return s.locale
}
