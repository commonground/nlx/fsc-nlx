// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package texti18n

import (
	"golang.org/x/text/message"
)

func (s *I18n) Translate(key interface{}, args ...interface{}) string {
	keyToReference := key.(message.Reference)
	return s.printer.Sprintf(keyToReference, args...)
}
