// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package texti18n

import (
	"fmt"

	"github.com/pkg/errors"
	"golang.org/x/exp/slices"
	"golang.org/x/text/language"
	"golang.org/x/text/message"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/i18n"
	_ "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/i18n/text/translations"
)

type I18n struct {
	locale  string
	printer *message.Printer
}

func New(locale string) (i18n.I18n, error) {
	if !slices.Contains([]string{i18n.LocaleEnUS, i18n.LocaleNlNL}, locale) {
		return nil, fmt.Errorf("invalid locale '%s' provided", locale)
	}

	var printer *message.Printer

	switch locale {
	case i18n.LocaleNlNL:
		printer = message.NewPrinter(language.MustParse("nl-NL"))
	case i18n.LocaleEnUS:
		printer = message.NewPrinter(language.MustParse("en-US"))
	default:
		return nil, errors.Errorf("invalid locale %q provided", locale)
	}

	return &I18n{
		printer: printer,
		locale:  locale,
	}, nil
}
