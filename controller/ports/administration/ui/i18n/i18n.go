// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package i18n

const LocaleEnUS = "en-US"
const LocaleNlNL = "nl-NL"

type I18n interface {
	Translate(key interface{}, args ...interface{}) string
	Locale() string
}
