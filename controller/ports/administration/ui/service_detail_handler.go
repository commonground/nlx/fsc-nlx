// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) serviceDetailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	name := chi.URLParam(r, "name")

	page := serviceDetailPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
			Title:                       name,
			Description:                 "",
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
		Service: &serviceDetailPageService{
			Name:                name,
			Publications:        []*serviceDetailPageServiceGrant{},
			IncomingConnections: []*serviceDetailPageServiceGrant{},
		},
	}

	service, err := s.apiApp.Queries.GetService.Handle(ctx, &query.GetServiceArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Name:                  name,
	})
	if err != nil {
		if errors.Is(err, storage.ErrServiceNotFound) {
			page.renderNotFound(w)
			return
		}

		s.logger.Error("failed to retrieve service", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	page.Service = &serviceDetailPageService{
		Name:         service.Name,
		EndpointURL:  service.EndpointURL.String(),
		InwayAddress: service.InwayAddress,
	}

	publications, _, err := s.apiApp.Queries.ListServicePublications.Handle(ctx, &query.ListServicePublicationsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		ServiceNames:          []string{service.Name},
	})
	if err != nil {
		s.logger.Error("failed to get publications for the service", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	incomingConnections, _, err := s.apiApp.Queries.ListIncomingConnections.Handle(ctx, &query.ListIncomingConnectionsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		ServiceNames:          []string{service.Name},
	})
	if err != nil {
		s.logger.Error("failed to get incoming connections for the service", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	peerIDs := []string{}

	for _, p := range publications.Publications {
		peerIDs = append(peerIDs, p.ServicePeerID, p.DirectoryPeerID)

		if p.DelegatorPeerID != "" {
			peerIDs = append(peerIDs, p.DelegatorPeerID)
		}
	}

	for _, c := range incomingConnections.Connections {
		peerIDs = append(peerIDs, c.ServicePeerID, c.OutwayPeerID)

		if c.DelegatorPeerID != nil {
			peerIDs = append(peerIDs, *c.DelegatorPeerID)
		}

		if c.ServicePublicationDelegatorPeerID != nil {
			peerIDs = append(peerIDs, *c.ServicePublicationDelegatorPeerID)
		}
	}

	peersInfo, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		PeerIDs:               &peerIDs,
	})
	if err != nil {
		s.logger.Error("could not get details for peers", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	pCollection := newPeersCollection(peersInfo)

	// nolint:dupl // loops are similar but not the same
	for _, p := range publications.Publications {
		var directory string

		if p.DelegatorPeerID != "" {
			directory = fmt.Sprintf("%s (%s) namens %s (%s)", pCollection.NameForPeerID(p.DirectoryPeerID), p.DirectoryPeerID, pCollection.NameForPeerID(p.DelegatorPeerID), p.DelegatorPeerID)
		} else {
			directory = fmt.Sprintf("%s (%s)", pCollection.NameForPeerID(p.DirectoryPeerID), p.DirectoryPeerID)
		}

		page.Service.Publications = append(page.Service.Publications, &serviceDetailPageServiceGrant{
			State:               p.State,
			Title:               directory,
			ContractContentHash: p.ContentHash,
			ValidFrom:           p.ValidNotBefore.UTC(),
			ValidUntil:          p.ValidNotAfter.UTC(),
			CreatedAt:           p.CreatedAt.UTC(),
			RemainingDaysValid:  contractRemainingDaysValid(p.ValidNotBefore, p.ValidNotAfter),
		})
	}

	for _, c := range incomingConnections.Connections {
		var description string

		if c.DelegatorPeerID != nil {
			description = fmt.Sprintf("%s (%s) door %s (%s)", pCollection.NameForPeerID(c.OutwayPeerID), c.OutwayPeerID, pCollection.NameForPeerID(*c.DelegatorPeerID), *c.DelegatorPeerID)
		} else {
			description = fmt.Sprintf("%s (%s)", pCollection.NameForPeerID(c.OutwayPeerID), c.OutwayPeerID)
		}

		page.Service.IncomingConnections = append(page.Service.IncomingConnections, &serviceDetailPageServiceGrant{
			State:               c.State,
			Title:               description,
			ContractContentHash: c.ContentHash,
			ValidFrom:           c.ValidNotBefore.UTC(),
			ValidUntil:          c.ValidNotAfter.UTC(),
			CreatedAt:           c.CreatedAt.UTC(),
			RemainingDaysValid:  contractRemainingDaysValid(c.ValidNotBefore, c.ValidNotAfter),
		})
	}

	page.render(w)
}
