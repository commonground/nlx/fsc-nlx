// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"
	"time"
)

type incomingConnectionsPage struct {
	*BasePage
	BaseAuthenticatedPage

	AmountOfConnections             int
	Connections                     []*incomingConnectionsPageConnection
	PaginationNextCursor            string
	PaginationLimit                 uint32
	SelectedTab                     string
	ContractDetailBackButtonURLPath string
}

type incomingConnectionsPageConnection struct {
	ServiceName                         string
	OutwayPeerID                        string
	OutwayPeerName                      string
	DelegatorPeerID                     string
	DelegatorPeerName                   string
	ServicePublicationDelegatorPeerID   string
	ServicePublicationDelegatorPeerName string
	ContentHash                         string
	GrantHashURLFriendly                string
	CreatedAt                           time.Time
	RemainingDaysValid                  int
	ValidNotBefore                      time.Time
	ValidNotAfter                       time.Time
	State                               string
}

func (p *incomingConnectionsPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/incoming-connections.html",
			"templates/components/contract-status.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
