// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) directoryServiceDetailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	peerID := chi.URLParam(r, "peerID")
	serviceName := chi.URLParam(r, "serviceName")
	servicePublicationDelegatorID := chi.URLParam(r, "delegatorID")
	directoryPeerID := chi.URLParam(r, "directoryPeerID")

	page := directoryServiceDetailPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			Title:                       "",
			PrimaryNavigationActivePath: PathDirectoryPage,
			Description:                 "",
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
		},
	}

	peerServices, err := s.apiApp.Queries.ListPeerServices.Handle(ctx, &query.ListPeerServicesArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		DirectoryPeerID:       directoryPeerID,
	})
	if err != nil {
		s.logger.Error("failed to retrieve peer services", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if len(peerServices) == 0 {
		page.renderNotFound(w)
		return
	}

	peerService := findPeerService(s.logger, peerServices, serviceName, peerID, servicePublicationDelegatorID)

	if peerService == nil {
		page.renderNotFound(w)
		return
	}

	page.Service = convertPeerServiceToUI(peerService)

	page.BaseAuthenticatedPage.Title = page.Service.Name
	page.BaseAuthenticatedPage.Description = s.i18n.Translate("View details of a Directory Service.")

	outways, err := s.apiApp.Queries.ListOutways.Handle(ctx, &query.ListOutwaysArgs{
		AuthData:   authData,
		SelfPeerID: s.getPeerInfo(ctx, getAuthzData(r), authData).ID,
	})
	if err != nil {
		s.logger.Error("failed to retrieve outways", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	outgoingConnections, err := s.apiApp.Queries.ListOutgoingConnectionsForService.Handle(ctx, &query.ListOutgoingConnectionsForServiceArgs{
		AuthData:                          authData,
		AuthorizationMetadata:             authzData,
		ServiceName:                       serviceName,
		ServicePublicationDelegatorPeerID: servicePublicationDelegatorID,
		ServicePeerID:                     peerID,
	})
	if err != nil {
		s.logger.Error("failed to retrieve outgoing connections for service", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	peerInfo := s.getPeerInfo(ctx, getAuthzData(r), authData)

	page.OutwayPublicKeyThumbprints = mapDataToView(outways, outgoingConnections, peerID, servicePublicationDelegatorID, peerInfo.ID, serviceName)

	page.render(w)
}

func findPeerService(lgr *logger.Logger, services query.PeerServices, serviceName, peerID, delegatorID string) interface{} {
	for _, srv := range services {
		switch service := srv.(type) {
		case *query.DelegatedPeerService:
			if service.Name == serviceName && service.PeerID == peerID && service.DelegatorID == delegatorID {
				return service
			}
		case *query.PeerService:
			if service.Name == serviceName && service.PeerID == peerID {
				return service
			}
		default:
			lgr.Info(fmt.Sprintf("unknown peer service type %v", service))
		}
	}

	return nil
}

func convertPeerServiceToUI(peerService interface{}) *directoryServiceDetailPageService {
	result := &directoryServiceDetailPageService{}

	switch service := peerService.(type) {
	case *query.DelegatedPeerService:
		result = &directoryServiceDetailPageService{
			Name:                   service.Name,
			PeerID:                 service.DelegatorID,
			PeerName:               service.DelegatorName,
			ProviderID:             service.PeerID,
			ProviderName:           service.PeerName,
			IsDelegatedPublication: true,
		}
	case *query.PeerService:
		result = &directoryServiceDetailPageService{
			Name:                   service.Name,
			PeerID:                 service.PeerID,
			PeerName:               service.PeerName,
			IsDelegatedPublication: false,
		}
	}

	result.ContractDetailsBackButtonURL = fmt.Sprintf("%s/%s/%s", PathDirectoryPage, result.PeerID, result.Name)

	return result
}

// nolint:gocyclo // this is a complex function because we are filtering in code because the outgoing connections endpoint does not have the filter options we need.
func mapDataToView(outways []*query.Outway, outgoingConnections []*query.Connection, servicePeerID, delegatorPeerID, selfPeerID, serviceName string) []*directoryServiceDetailPageOutwayPublicKeyThumbprint {
	result := []*directoryServiceDetailPageOutwayPublicKeyThumbprint{}

	outwaysByPublicKeyThumbprint := mapOutwaysByPublicKeyThumbprint(outways)

	publicKeyThumbprintAccessMap := map[string]*directoryServiceDetailPageOutwayPublicKeyThumbprint{}

	for _, outway := range outways {
		entry, ok := publicKeyThumbprintAccessMap[outway.PublicKeyThumbprint]
		if !ok {
			entry = &directoryServiceDetailPageOutwayPublicKeyThumbprint{
				OutgoingConnections: []*directoryServiceDetailPageOutwayPublicKeyThumbprintOutgoingConnection{},
			}

			publicKeyThumbprintAccessMap[outway.PublicKeyThumbprint] = entry
		}

		entry.PublicKeyThumbprint = outway.PublicKeyThumbprint
		entry.OutwayNames = append(entry.OutwayNames, outway.Name)
	}

	for _, c := range outgoingConnections {
		if c.ServicePeerID != servicePeerID {
			continue
		}

		if c.ServiceName != serviceName {
			continue
		}

		if c.OutwayPeerID != selfPeerID {
			continue
		}

		if delegatorPeerID != "" && (c.ServicePublicationDelegatorPeerID == nil || *c.ServicePublicationDelegatorPeerID != delegatorPeerID) {
			continue
		}

		access, ok := publicKeyThumbprintAccessMap[c.OutwayPublicKeyThumbprint]
		if !ok {
			publicKeyThumbprintAccessMap[c.OutwayPublicKeyThumbprint] = &directoryServiceDetailPageOutwayPublicKeyThumbprint{
				OutgoingConnections: []*directoryServiceDetailPageOutwayPublicKeyThumbprintOutgoingConnection{},
			}
			access = publicKeyThumbprintAccessMap[c.OutwayPublicKeyThumbprint]
		}

		access.PublicKeyThumbprint = c.OutwayPublicKeyThumbprint
		access.OutwayNames = outwaysByPublicKeyThumbprint[c.OutwayPublicKeyThumbprint]
		access.OutgoingConnections = append(access.OutgoingConnections, &directoryServiceDetailPageOutwayPublicKeyThumbprintOutgoingConnection{
			State:                c.State,
			Hash:                 c.ContentHash,
			GrantHashURLFriendly: urlFriendlyGrantHash(c.GrantHash),
			ValidFrom:            c.ValidNotBefore.UTC(),
			ValidUntil:           c.ValidNotAfter.UTC(),
			CreatedAt:            c.CreatedAt.UTC(),
			RemainingDaysValid:   contractRemainingDaysValid(c.ValidNotBefore, c.ValidNotAfter),
		})
	}

	for _, access := range publicKeyThumbprintAccessMap {
		result = append(result, access)
	}

	return result
}

func mapOutwaysByPublicKeyThumbprint(outways []*query.Outway) map[string][]string {
	result := map[string][]string{}

	for _, outway := range outways {
		outwayNames, owOk := result[outway.PublicKeyThumbprint]
		if !owOk {
			outwayNames = []string{}
		}

		result[outway.PublicKeyThumbprint] = append(outwayNames, outway.Name)
	}

	return result
}
