// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"
)

type directoryPage struct {
	*BasePage
	BaseAuthenticatedPage

	AmountOfServices          int
	Directories               map[string]string
	SelectedDirectoryPeerName string
	SelectedDirectoryPeerID   string
	Services                  []*directoryPageService
}

type directoryPageService struct {
	Name                   string
	PeerID                 string
	PeerName               string
	ProviderID             string
	ProviderName           string
	IsDelegatedPublication bool
}

func (s *directoryPageService) IsDelegated() bool {
	return s.ProviderID != ""
}

func (p *directoryPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/directory.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
