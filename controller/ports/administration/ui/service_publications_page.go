// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

type servicePublicationsPage struct {
	*BasePage
	BaseAuthenticatedPage

	GrantTypeFilters                map[query.GrantType]string
	GrantType                       string
	Publications                    []*publication
	AmountOfPublications            int
	SelectedTab                     string
	PaginationNextCursor            string
	PaginationLimit                 uint32
	WithInvalidContract             bool
	ContractDetailBackButtonURLPath string
}

type publication struct {
	ServicePeerID        string
	ServicePeerName      string
	ServiceName          string
	ContentHash          string
	CreatedAt            time.Time
	ValidNotBefore       time.Time
	ValidNotAfter        time.Time
	State                string
	RemainingDaysValid   int
	GrantHash            string
	GrantHashURLFriendly string
	DirectoryPeerID      string
	DirectoryPeerName    string
	DelegatorPeerID      string
	DelegatorPeerName    string
}

func (p *servicePublicationsPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/service-publications.html",
			"templates/components/contract-status.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
