// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

func (s *Server) publishServiceGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	serviceName := chi.URLParam(r, "name")

	s.getPeerInfo(ctx, authzData, authData)

	serviceNamesForPeer := make([]string, 0)
	if serviceName == "" {
		serviceNamesForPeer = s.getServiceNamesForPeer(ctx, authzData, authData)
	}

	delegatorPeerID := r.URL.Query().Get("delegatorPeerID")

	page := publishServicePage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
			Title:                       s.i18n.Translate("Publish Service"),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		DirectoryPeerIDs:    s.getPeerIDs(ctx, getAuthzData(r), authData),
		ServiceNamesForPeer: serviceNamesForPeer,
		Form: &PublishServicePageForm{
			Name:            serviceName,
			DirectoryPeerID: s.DirectoryPeerID,
			DelegatorPeerID: strings.TrimSpace(delegatorPeerID),
			ValidFrom:       time.Now().Format("2006-01-02"),
			ValidUntil:      time.Now().Add(Day).Format("2006-01-02"),
		},
	}

	page.render(w)
}

func (s *Server) publishServicePostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	formValues, err := decodePublishServicePageForm(s.decoder, r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	serviceName := chi.URLParam(r, "name")

	if serviceName != "" {
		formValues.Name = serviceName
	}

	page := publishServicePage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
			Title:                       s.i18n.Translate("Publish Service"),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
		},
		DirectoryPeerIDs: s.getPeerIDs(ctx, getAuthzData(r), authData),
		Form:             formValues,
	}

	peerInfo := s.getPeerInfo(ctx, getAuthzData(r), authData)

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if formValues.DelegatorPeerID == "" {
		err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
			AuthData:              authData,
			AuthorizationMetadata: authzData,
			AuditLogSource:        auditLogSource,
			HashAlgorithm:         command.HashAlgSHA3_512,
			GroupID:               s.basePage.GroupID,
			ContractNotBefore:     formValues.ValidFrom,
			ContractNotAfter:      formValues.ValidUntil,
			ServicePublicationGrants: []*command.ServicePublicationGrant{
				{
					DirectoryPeerID: strings.TrimSpace(formValues.DirectoryPeerID),
					ServicePeerID:   peerInfo.ID,
					ServiceName:     formValues.Name,
				},
			},
		})
		if err != nil {
			s.logger.Error("create contract for publishing a service", err)

			page.renderWithError(w, err)

			return
		}

		page.renderWithSuccess(s.i18n.Translate("The Service has been published."), w)
	} else {
		err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
			AuthData:              authData,
			AuthorizationMetadata: authzData,
			AuditLogSource:        auditLogSource,
			HashAlgorithm:         command.HashAlgSHA3_512,
			GroupID:               s.basePage.GroupID,
			ContractNotBefore:     formValues.ValidFrom,
			ContractNotAfter:      formValues.ValidUntil,
			DelegatedServicePublicationGrants: []*command.DelegatedServicePublicationGrant{
				{
					DirectoryPeerID: strings.TrimSpace(formValues.DirectoryPeerID),
					DelegatorPeerID: strings.TrimSpace(formValues.DelegatorPeerID),
					ServicePeerID:   peerInfo.ID,
					ServiceName:     formValues.Name,
				},
			},
		})
		if err != nil {
			s.logger.Error("create contract for publishing a service", err)

			page.renderWithError(w, err)

			return
		}

		page.renderWithSuccess(s.i18n.Translate("The Service will be published once the Delegator accepts the Contract."), w)
	}
}

func (s *Server) getServiceNamesForPeer(ctx context.Context, authzData *authorization.RestMetaData, authData authentication.Data) []string {
	serviceNames, err := s.apiApp.Queries.ListServices.Handle(ctx, &query.ListServicesArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		SelfPeerID:            s.peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("failed to retrieve services", err)

		return nil
	}

	serviceNamesForPeer := make([]string, len(serviceNames))
	for i, serviceName := range serviceNames {
		serviceNamesForPeer[i] = serviceName.Name
	}

	return serviceNamesForPeer
}
