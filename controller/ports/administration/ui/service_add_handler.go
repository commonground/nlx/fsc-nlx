// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) addServiceGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	s.getPeerInfo(ctx, authzData, authData)

	inways, err := s.apiApp.Queries.ListInways.Handle(ctx, &query.ListInwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		SelfPeerID:            s.peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("get Inway addresses for form", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	inwayAddresses := []string{}

	for _, inway := range inways {
		inwayAddresses = append(inwayAddresses, inway.Address)
	}

	page := addServicePage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
			Title:                       s.i18n.Translate("Add new service"),
			Description:                 s.i18n.Translate("You can publish this Service using a Contract."),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			CsrfToken:                   csrf.Token(r),
		},
		InwayAddresses: inwayAddresses,
		Form: &AddServicePageForm{
			Name:         "",
			EndpointURL:  "",
			InwayAddress: "",
		},
	}

	page.render(w)
}

func (s *Server) addServicePostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	inways, err := s.apiApp.Queries.ListInways.Handle(ctx, &query.ListInwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		SelfPeerID:            s.peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("get Inway addresses for form", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	inwayAddresses := []string{}

	for _, inway := range inways {
		inwayAddresses = append(inwayAddresses, inway.Address)
	}

	formValues, err := decodeAddServicePageForm(s.decoder, r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	page := addServicePage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
			Title:                       s.i18n.Translate("Add new service"),
			Description:                 s.i18n.Translate("You can publish this Service using a Contract."),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
		},
		InwayAddresses: inwayAddresses,
		Form:           formValues,
	}

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	err = s.apiApp.Commands.CreateService.Handle(context.Background(), &command.CreateServiceArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		AuditLogSource:        auditLogSource,
		Name:                  formValues.Name,
		EndpointURL:           formValues.EndpointURL,
		InwayAddress:          formValues.InwayAddress,
	})
	if err != nil {
		s.logger.Error("create service", err)

		if errors.Is(err, command.ErrServiceNameInUse) {
			page.renderWithErrorMessage(w, http.StatusInternalServerError, []string{s.i18n.Translate("The name of the Service is already in use. Please use a different name.")})

			return
		}

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(s.i18n.Translate("The Service has been added"), w)
}
