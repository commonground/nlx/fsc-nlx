// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/gorilla/csrf"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) updateServiceGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	s.getPeerInfo(ctx, authzData, authData)

	serviceName := chi.URLParam(r, "name")

	inways, err := s.apiApp.Queries.ListInways.Handle(ctx, &query.ListInwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		SelfPeerID:            s.peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("get Inway addresses for form", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	inwayAddresses := []string{}

	for _, inway := range inways {
		inwayAddresses = append(inwayAddresses, inway.Address)
	}

	service, err := s.apiApp.Queries.GetService.Handle(ctx, &query.GetServiceArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Name:                  serviceName,
	})

	if err != nil && errors.Is(errors.Unwrap(err), storage.ErrServiceNotFound) {
		notFoundPage := updateServicePage{
			BasePage: s.basePage,
			BaseAuthenticatedPage: BaseAuthenticatedPage{
				PrimaryNavigationActivePath: PathServicesPage,
				Title:                       serviceName,
				Description:                 "",
				Peer:                        s.peerInfo,
				Username:                    authData.Username(),
				LogoutURL:                   s.authn.LogoutURL(),
			},
			Form: &UpdateServicePageForm{
				Name: serviceName,
			},
		}

		notFoundPage.renderNotFound(w)

		return
	}

	if err != nil {
		s.logger.Error("failed to get service for editing", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	page := updateServicePage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			Title:       service.Name,
			Description: "",
			Peer:        s.peerInfo,
			Username:    authData.Username(),
			LogoutURL:   s.authn.LogoutURL(),
			CsrfToken:   csrf.Token(r),
		},
		InwayAddresses: inwayAddresses,
		Form: &UpdateServicePageForm{
			Name:         serviceName,
			EndpointURL:  service.EndpointURL.String(),
			InwayAddress: service.InwayAddress,
		},
	}

	page.render(w)
}

//nolint:dupl // update and add are similar
func (s *Server) updateServicePostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	serviceName := chi.URLParam(r, "name")

	inways, err := s.apiApp.Queries.ListInways.Handle(ctx, &query.ListInwaysArgs{
		AuthData:   authData,
		SelfPeerID: s.peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("failed to get inway addresses for editing service", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	inwayAddresses := []string{}

	for _, inway := range inways {
		inwayAddresses = append(inwayAddresses, inway.Address)
	}

	formValues, err := decodeUpdateServicePageForm(s.decoder, r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	formValues.Name = serviceName

	page := updateServicePage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			Title:       serviceName,
			Description: "",
			Peer:        s.peerInfo,
			Username:    authData.Username(),
			LogoutURL:   s.authn.LogoutURL(),
		},
		InwayAddresses: inwayAddresses,
		Form:           formValues,
	}

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if formValues.Action == "delete-service" {
		err = s.apiApp.Commands.DeleteService.Handle(ctx, &command.DeleteServiceArgs{
			AuthData:       authData,
			AuditLogSource: auditLogSource,
			Name:           serviceName,
		})
		if err != nil {
			s.logger.Error("failed to delete Service", err)

			page.renderWithError(w, err)

			return
		}

		page.renderWithSuccess(s.i18n.Translate("The Service has been removed"), w)

		return
	}

	err = s.apiApp.Commands.UpdateService.Handle(context.Background(), &command.UpdateServiceArgs{
		AuthData:       authData,
		AuditLogSource: auditLogSource,
		Name:           serviceName,
		EndpointURL:    formValues.EndpointURL,
		InwayAddress:   formValues.InwayAddress,
	})
	if err != nil {
		s.logger.Error("update service", err)

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(s.i18n.Translate("The Service has been updated"), w)
}
