// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//nolint:dupl // looks the same as service_publish_page.go but this page is for a delegated publication
package uiport

import (
	"net/http"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

type publishServiceDelegatedPage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage      string
	ErrorMessages       []string
	DirectoryPeerIDs    map[string]string
	ServiceNamesForPeer []string
	Form                *PublishServiceDelegatedPageForm
}

type PublishServiceDelegatedPageForm struct {
	Name            string
	DirectoryPeerID string
	ServicePeerID   string
	ValidFrom       string
	ValidUntil      string
}

func decodeDelegatedPublishServicePageForm(decoder *schema.Decoder, r *http.Request) (*PublishServiceDelegatedPageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result PublishServiceDelegatedPageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *publishServiceDelegatedPage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, messages := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessages = messages

	p.render(w)
}

func (p *publishServiceDelegatedPage) renderWithSuccess(successMessage string, w http.ResponseWriter) {
	p.SuccessMessage = successMessage
	p.render(w)
}

func (p *publishServiceDelegatedPage) render(w http.ResponseWriter) {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/publish-service-delegated.html",
			"templates/components/errors.html",
		)
	if err != nil {
		p.logger.Error("failed to load templates for the publish service delegated page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		p.logger.Error("failed to execute template for the publish service delegated page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}
