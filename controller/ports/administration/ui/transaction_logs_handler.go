// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/i18n"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

const getParamDataSourceID = "data-source-peer-id"
const txlogRecordsLimit = 100

func (s *Server) transactionLogsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	resp, err := s.apiApp.Queries.ListTXLogRecords.Handle(ctx, &query.ListTXLogRecordsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		DataSourcePeerID:      strings.TrimSpace(r.URL.Query().Get(getParamDataSourceID)),
		Pagination: &query.Pagination{
			SortOrder: query.SortOrderDescending,
			Limit:     txlogRecordsLimit,
		},
	})
	if err != nil {
		s.logger.Error("could not render transaction logs page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	records, err := mapQueryTXLog(s.i18n, resp)
	if err != nil {
		s.logger.Error("could not render transaction logs page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	page := transactionLogsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathTransactionLogsPage,
			Title:                       s.i18n.Translate("Transaction logs"),
			Description:                 s.i18n.Translate("De laatste 100 transactie logs van jouw Inways en Outways"),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
		TransactionLogs:           records,
		GetParamDataSourceID:      getParamDataSourceID,
		GetParamDataSourceIDValue: r.URL.Query().Get(getParamDataSourceID),
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render transaction logs page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func mapQueryTXLog(t i18n.I18n, recs []*query.TXLogRecord) ([]*transactionLogsPageRow, error) {
	records := make([]*transactionLogsPageRow, len(recs))

	for i, r := range recs {
		records[i] = &transactionLogsPageRow{
			TransactionID: r.TransactionID,
			Direction:     mapTXLogDirection(t, r.Direction),
			ServiceName:   r.ServiceName,
			CreatedAt:     r.CreatedAt.UTC(),
		}

		switch s := r.Source.(type) {
		case *query.TXLogRecordSource:
			records[i].SourceOutwayPeerID = s.OutwayPeerID
		case *query.TXLogRecordDelegatedSource:
			records[i].IsSourceDelegated = true
			records[i].SourceOutwayPeerID = s.OutwayPeerID
			records[i].SourceDelegatorPeerID = s.DelegatorPeerID
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		switch d := r.Destination.(type) {
		case *query.TXLogRecordDestination:
			records[i].DestinationServicePeerID = d.ServicePeerID
		case *query.TXLogRecordDelegatedDestination:
			records[i].IsDestinationDelegated = true
			records[i].DestinationServicePeerID = d.ServicePeerID
			records[i].DestinationDelegatorPeerID = d.DelegatorPeerID
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}
	}

	return records, nil
}

func mapTXLogDirection(i i18n.I18n, d record.Direction) string {
	switch d {
	case record.DirectionIn:
		return i.Translate("Incoming")
	case record.DirectionOut:
		return i.Translate("Outgoing")
	default:
		return ""
	}
}
