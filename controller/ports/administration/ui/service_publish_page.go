// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

type publishServicePage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage      string
	ErrorMessages       []string
	DirectoryPeerIDs    map[string]string
	ServiceNamesForPeer []string
	Form                *PublishServicePageForm
}

type PublishServicePageForm struct {
	Name            string
	DirectoryPeerID string
	DelegatorPeerID string
	ValidFrom       string
	ValidUntil      string
}

func decodePublishServicePageForm(decoder *schema.Decoder, r *http.Request) (*PublishServicePageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result PublishServicePageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *publishServicePage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, message := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessages = message

	p.render(w)
}

func (p *publishServicePage) renderWithSuccess(successMessage string, w http.ResponseWriter) {
	p.SuccessMessage = successMessage
	p.render(w)
}

func (p *publishServicePage) render(w http.ResponseWriter) {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/components/errors.html",
			"templates/publish-service.html",
		)
	if err != nil {
		p.logger.Error("failed to load templates for the delegated publish service page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		p.logger.Error("failed to execute template for the delegated publish service page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}
