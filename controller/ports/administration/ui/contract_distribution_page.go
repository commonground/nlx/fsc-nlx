/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package uiport

import (
	"fmt"
	"net/http"
	"time"
)

type contractDistributionPage struct {
	*BasePage
	BaseAuthenticatedPage
	PeerID               string
	PeerName             string
	ContentHash          string
	ContractDistribution map[Action]*contractDistributionDetails
	SuccessMessage       string
	ErrorMessage         string
}

type contractDistributionDetails struct {
	ContentHash string
	Signature   string
	Action      Action
	Attempts    int
	LastAttempt time.Time
	NextAttempt *time.Time
	Reason      string
}

func (p *contractDistributionPage) render(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/contract-distribution-detail.html")
	if err != nil {
		p.logger.Error("failed to parse contract distribution not found page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *contractDistributionPage) renderNotFound(w http.ResponseWriter) {
	p.BaseAuthenticatedPage.Title = p.I18n.Translate("Contract distributions not found")

	err := p.renderTemplate(w, "templates/contract-distribution-not-found.html")
	if err != nil {
		p.logger.Error("failed to parse contract distribution not found page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *contractDistributionPage) renderTemplate(w http.ResponseWriter, templatePath string) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			templatePath,
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
