// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

type delegatedServicePublicationsPage struct {
	*BasePage
	BaseAuthenticatedPage

	GrantTypeFilters                map[query.GrantType]string
	GrantType                       string
	Publications                    []*publication
	AmountOfPublications            int
	PaginationNextCursor            string
	PaginationLimit                 uint32
	SelectedTab                     string
	ContractDetailBackButtonURLPath string
}

func (p *delegatedServicePublicationsPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/delegated-publications.html",
			"templates/components/contract-status.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
