// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

type updateServicePage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage string
	ErrorMessages  []string
	InwayAddresses []string
	Form           *UpdateServicePageForm
}

type UpdateServicePageForm struct {
	Name         string
	EndpointURL  string
	InwayAddress string
	Action       string
}

func decodeUpdateServicePageForm(decoder *schema.Decoder, r *http.Request) (*UpdateServicePageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result UpdateServicePageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *updateServicePage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, message := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessages = message

	p.render(w)
}

func (p *updateServicePage) renderNotFound(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/service-detail-not-found.html")
	if err != nil {
		p.logger.Error("failed to parse service not found page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func (p *updateServicePage) renderWithSuccess(successMessage string, w http.ResponseWriter) {
	p.SuccessMessage = successMessage
	p.render(w)
}

func (p *updateServicePage) render(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/update-service.html")
	if err != nil {
		p.logger.Error("failed to parse service page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func (p *updateServicePage) renderTemplate(w http.ResponseWriter, templatePath string) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/components/errors.html",
			"templates/components/errors.html",
			templatePath,
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
