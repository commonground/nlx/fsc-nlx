// copyright © vng realisatie 2024
// licensed under the eupl

package uiport

import "net/http"

func (s *Server) healthCheckHandler(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
