// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

const formActionAddOutwayPublicKeyThumbprint = "add-outway-public-key-thumbprint"

type connectToServicePage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage                     string
	ErrorMessages                      []string
	OutwayPublicKeyThumbprints         map[string][]string
	PeerIDs                            map[string]string
	ServiceNamesForPeer                []string
	Form                               *ConnectToServicePageForm
	ActionAddOutwayPublicKeyThumbprint string
}

type ConnectToServicePageForm struct {
	Action                     string
	ServiceName                string
	ServiceProviderPeerID      string
	ServiceDelegatorPeerID     string
	DelegatorPeerID            string
	OutwayPublicKeyThumbprints []string
	ValidFrom                  string
	ValidUntil                 string
}

func decodeConnectToServicePageForm(decoder *schema.Decoder, r *http.Request) (*ConnectToServicePageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result ConnectToServicePageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *connectToServicePage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, message := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessages = message

	p.render(w)
}

func (p *connectToServicePage) renderWithSuccess(successMessage string, w http.ResponseWriter) {
	p.SuccessMessage = successMessage
	p.render(w)
}

// nolint:dupl // looks the same but is different from controller/ports/ui/service_publish_page.go
func (p *connectToServicePage) render(w http.ResponseWriter) {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/components/errors.html",
			"templates/connect-to-service.html",
		)
	if err != nil {
		p.logger.Error("failed to load templates for the connect to service page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		p.logger.Error("failed to execute template for the connect to service page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}
