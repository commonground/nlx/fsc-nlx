// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"
	"time"
)

type transactionLogsPage struct {
	*BasePage
	BaseAuthenticatedPage

	TransactionLogs []*transactionLogsPageRow

	GetParamDataSourceID      string
	GetParamDataSourceIDValue string
}

type transactionLogsPageRow struct {
	CreatedAt     time.Time
	TransactionID string
	Direction     string

	IsSourceDelegated     bool
	SourceOutwayPeerID    string
	SourceDelegatorPeerID string

	IsDestinationDelegated     bool
	DestinationServicePeerID   string
	DestinationDelegatorPeerID string

	ServiceName string
}

func (p *transactionLogsPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/transaction-logs.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
