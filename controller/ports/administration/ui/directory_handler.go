// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"slices"
	"sort"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) directoryHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	peers, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
	})
	if err != nil {
		s.logger.Error("could not retrieve Peers from Manager", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	directories := mapQueryToUIDirectories(peers)

	selectedDirectoryPeerName, selectedDirectoryPeerID := getSelectedDirectory(s.DirectoryPeerID, r.URL.Query().Get("directoryPeerID"), directories)

	var services []*directoryPageService

	if selectedDirectoryPeerID != "" {
		resp, err := s.apiApp.Queries.ListPeerServices.Handle(ctx, &query.ListPeerServicesArgs{
			AuthData:              authData,
			AuthorizationMetadata: authzData,
			DirectoryPeerID:       selectedDirectoryPeerID,
		})
		if err != nil {
			s.logger.Error("could not render directory page", err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

			return
		}

		services = mapQueryToUIPeerServices(s.logger, resp)
	}

	page := directoryPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathDirectoryPage,
			Title:                       s.i18n.Translate("Directory"),
			Description:                 "",
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
		AmountOfServices:          len(services),
		Directories:               directories,
		SelectedDirectoryPeerID:   selectedDirectoryPeerID,
		SelectedDirectoryPeerName: selectedDirectoryPeerName,
		Services:                  services,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render directory page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func mapQueryToUIDirectories(peers query.ListPeersPeers) map[string]string {
	directories := make(map[string]string)

	for _, peer := range peers {
		if slices.Contains(peer.Roles, query.PeerRoleDirectory) {
			directories[peer.ID] = peer.Name
		}
	}

	return directories
}

func mapQueryToUIPeerServices(lgr *logger.Logger, qServices query.PeerServices) []*directoryPageService {
	services := make([]*directoryPageService, len(qServices))

	for i, s := range qServices {
		switch service := s.(type) {
		case *query.DelegatedPeerService:
			services[i] = &directoryPageService{
				Name:                   service.Name,
				PeerID:                 service.DelegatorID,
				PeerName:               service.DelegatorName,
				ProviderID:             service.PeerID,
				ProviderName:           service.PeerName,
				IsDelegatedPublication: true,
			}
		case *query.PeerService:
			services[i] = &directoryPageService{
				Name:                   service.Name,
				PeerID:                 service.PeerID,
				PeerName:               service.PeerName,
				ProviderID:             "",
				ProviderName:           "",
				IsDelegatedPublication: false,
			}
		default:
			lgr.Info(fmt.Sprintf("unknown service type %v", service))
		}
	}

	return services
}

func getSelectedDirectory(defaultDirectory, selectedDirectory string, directories map[string]string) (directoryName, directoryPeerID string) {
	if selectedDirectory != "" {
		return directories[selectedDirectory], selectedDirectory
	}

	if defaultDirectory != "" {
		return directories[defaultDirectory], defaultDirectory
	}

	sortedDirectories := make([]string, 0)
	for _, peerName := range directories {
		sortedDirectories = append(sortedDirectories, peerName)
	}

	sort.Strings(sortedDirectories)

	for peerID, peerName := range directories {
		if peerName == sortedDirectories[0] {
			return peerName, peerID
		}
	}

	return directoryName, directoryPeerID
}
