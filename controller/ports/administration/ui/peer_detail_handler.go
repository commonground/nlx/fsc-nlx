// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"slices"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) peerDetailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	id := chi.URLParam(r, "id")

	page := peerDetailPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathPeersPage,
			Title:                       id,
			Description:                 "",
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
		Peer: &peerDetailPagePeer{
			ID:             "",
			Name:           "",
			ManagerAddress: "",
			Roles:          []string{},
		},
	}

	peers, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		PeerIDs:               &[]string{id},
	})
	if err != nil {
		s.logger.Error("failed to retrieve peers", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if len(peers) == 0 {
		page.renderNotFound(w)
		return
	}

	peer := peers[id]

	page.Title = peer.Name

	roleIDs := mapPeerRolesToUI(peer.Roles)

	roles := []string{}

	for _, roleID := range roleIDs {
		roles = append(roles, allPeerRoles[roleID])
	}

	page.Peer = &peerDetailPagePeer{
		ID:             peer.ID,
		Name:           peer.Name,
		ManagerAddress: peer.ManagerAddress,
		Roles:          roles,
	}

	page.ShowConnectButton = !slices.Contains(roleIDs, peerRoleDirectory)

	page.render(w)
}
