// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import "net/http"

type inwaysOutwaysPage struct {
	*BasePage
	BaseAuthenticatedPage

	Amount      int
	Outways     []*inwaysOutwaysPageOutway
	Inways      []*inwaysOutwayPageInway
	SelectedTab string
}

type inwaysOutwaysPageOutway struct {
	Name                string
	PublicKeyThumbprint string
}

type inwaysOutwayPageInway struct {
	Name    string
	Address string
}

func (p *inwaysOutwaysPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(tplFolder,
			"templates/base-authenticated.html",
			"templates/inways-outways.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
