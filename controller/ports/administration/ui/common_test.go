// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"testing"
	"time"
)

func Test_ContractRemainingDaysValid(t *testing.T) {
	tests := map[string]struct {
		notBefore time.Time
		notAfter  time.Time
		want      int
	}{
		"equal_dates": {
			notBefore: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
			notAfter:  time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
			want:      1,
		},
		"less_than_one_day_remaining": {
			notBefore: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
			notAfter:  time.Date(2022, 1, 1, 23, 59, 59, 0, time.UTC),
			want:      1,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if got := contractRemainingDaysValid(tt.notBefore, tt.notAfter); got != tt.want {
				t.Errorf("contractRemainingDaysValid() = %v, want %v", got, tt.want)
			}
		})
	}
}
