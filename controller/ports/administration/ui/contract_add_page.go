// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

type addContractsPage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage                                string
	ErrorMessages                                 []string
	HashAlgorithms                                HashAlgorithms
	Groups                                        []string
	OutwayPublicKeyThumbprints                    map[string][]string
	PeerIDs                                       map[string]string
	ServiceNames                                  map[string]string
	Form                                          *AddContractsPageForm
	FormActionAddServiceConnectionGrant           string
	FormActionAddDelegatedServiceConnectionGrant  string
	FormActionAddServicePublicationGrant          string
	FormActionAddDelegatedServicePublicationGrant string
}

type HashAlgorithms []string

type AddContractsPageForm struct {
	Action        string
	HashAlgorithm string
	GroupID       string
	ValidFrom     string
	ValidUntil    string

	ServicePublicationGrants          []*AddContractsPageFormServicePublicationGrant
	ServiceConnectionGrants           []*AddContractsPageFormServiceConnectionGrant
	DelegatedServiceConnectionGrants  []*AddContractsPageFormDelegatedServiceConnectionGrant
	DelegatedServicePublicationGrants []*AddContractsPageFormDelegatedServicePublicationGrant
}

type AddContractsPageFormServicePublicationGrant struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type AddContractsPageFormServiceConnectionGrant struct {
	ServicePeerID                     string
	ServicePublicationDelegatorPeerID string
	ServiceName                       string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
}

type AddContractsPageFormDelegatedServiceConnectionGrant struct {
	DelegatorPeerID                   string
	ServicePeerID                     string
	ServicePublicationDelegatorPeerID string
	ServiceName                       string
	OutwayPeerID                      string
	OutwayPublicKeyThumbprint         string
}

type AddContractsPageFormDelegatedServicePublicationGrant struct {
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
}

const (
	FormActionAddServicePublicationGrant          = "add-service-publication-grant"
	FormActionAddServiceConnectionGrant           = "add-service-connection-grant"
	FormActionAddDelegatedServiceConnectionGrant  = "add-delegated-service-connection-grant"
	FormActionAddDelegatedServicePublicationGrant = "add-delegated-service-publication-grant"
)

func (p *addContractsPage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, messages := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessages = messages

	p.render(w)
}

func (p *addContractsPage) renderWithSuccess(w http.ResponseWriter, successMessage string) {
	p.SuccessMessage = successMessage
	p.render(w)
}

func decodeAddContractPageForm(decoder *schema.Decoder, r *http.Request) (*AddContractsPageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result AddContractsPageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *addContractsPage) render(w http.ResponseWriter) {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/add-contract.html",
			"templates/components/errors.html",
		)
	if err != nil {
		p.logger.Error("failed to parse add contract page template", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		p.logger.Error("failed to execute add contract page template", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}
