// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package oidc

import (
	"context"
	"encoding/gob"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/golang-jwt/jwt/v5"
	"github.com/gorilla/sessions"
	"github.com/jackc/pgx/v5/pgxpool"
	"golang.org/x/oauth2"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	auth "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication/oidc/pgsessionstore"
)

var ErrUnauthenticated = errors.New("not authenticated")

const (
	bearer          string = "bearer"
	cookieName      string = "nlx_controller_session"
	tokenName       string = "authorization"
	cleanupInterval        = time.Minute * 5
	maxSessionSize         = 1024 * 1024 * 1 // 1 MB
)

type OAuth2Config interface {
	AuthCodeURL(state string, opts ...oauth2.AuthCodeOption) string
	Exchange(ctx context.Context, code string, opts ...oauth2.AuthCodeOption) (*oauth2.Token, error)
}

type Provider interface {
	Endpoint() oauth2.Endpoint
	Verifier(config *oidc.Config) *oidc.IDTokenVerifier
}

type Store interface {
	Get(r *http.Request, name string) (*sessions.Session, error)
	New(r *http.Request, name string) (*sessions.Session, error)
	Save(r *http.Request, w http.ResponseWriter, s *sessions.Session) error
}

type Verifier interface {
	Verify(ctx context.Context, token string) (*oidc.IDToken, error)
}

type GetClaimsFromTokenFunc func(idToken *oidc.IDToken) (*IDTokenClaims, error)

type Authenticator struct {
	store               Store
	oidcProvider        Provider
	logger              *logger.Logger
	oauth2Config        OAuth2Config
	oidcConfig          *oidc.Config
	oidcVerifier        Verifier
	getClaims           GetClaimsFromTokenFunc
	close               func()
	sessionCookieSecure bool
	logoutURL           string
}

type IDTokenClaims struct {
	jwt.RegisteredClaims
	Name   string   `json:"name"`
	Email  string   `json:"email"`
	Groups []string `json:"groups"`
}

type OurUserInfoGetter struct{}

func New(httpsessionsDB *pgxpool.Pool, lgr *logger.Logger, provider *oidc.Provider, verifier Verifier, getClaims GetClaimsFromTokenFunc, options *Options) (*Authenticator, error) {
	gob.Register(&Claims{})

	store, err := pgsessionstore.New(lgr, httpsessionsDB, []byte(options.SecretKey))
	if err != nil {
		return nil, err
	}

	store.MaxLength(maxSessionSize)

	// Run a background goroutine to clean up expired sessions from the database.
	quit, done := store.StartCleanup(cleanupInterval)

	closeFn := func() {
		store.StopCleanup(quit, done)
		httpsessionsDB.Close()
	}

	store.Options = &sessions.Options{
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
		Secure:   options.SessionCookieSecure,
		Path:     "/",
	}

	oauth2Config := &oauth2.Config{
		ClientID:     options.ClientID,
		ClientSecret: options.ClientSecret,
		RedirectURL:  options.RedirectURL,
		Endpoint:     provider.Endpoint(),

		Scopes: []string{oidc.ScopeOpenID, "profile", "email", "groups"},
	}

	oidcConfig := &oidc.Config{
		ClientID: options.ClientID,
	}

	logoutURL := "/"

	if options.LogoutURL != "" {
		logoutURL = options.LogoutURL
	}

	return &Authenticator{
		logger:              lgr,
		store:               store,
		oauth2Config:        oauth2Config,
		oidcConfig:          oidcConfig,
		oidcProvider:        provider,
		oidcVerifier:        verifier,
		getClaims:           getClaims,
		close:               closeFn,
		sessionCookieSecure: options.SessionCookieSecure,
		logoutURL:           logoutURL,
	}, nil
}

func (a *Authenticator) Close() {
	a.close()
}

func (a *Authenticator) LogoutURL() string {
	return "/oidc/logout"
}

func (a *Authenticator) MountRoutes(r chi.Router) {
	routes := chi.NewRouter()
	routes.Get("/authenticate", a.authenticate)
	routes.Get("/callback", a.callback)
	routes.Get("/logout", a.logout)
	routes.Get("/me", a.
		OnlyAuthenticated(http.HandlerFunc(a.me)).
		ServeHTTP,
	)

	r.Mount("/oidc", routes)
}

func (a *Authenticator) OnlyAuthenticated(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		token, claim, err := func() (string, *IDTokenClaims, error) {
			session, err := a.store.Get(r, cookieName)
			if err != nil {
				return "", nil, fmt.Errorf("failed to get session from store: %w", err)
			}

			token, ok := session.Values[tokenName].(string)
			if !ok {
				return "", nil, fmt.Errorf("failed to get token from session")
			}

			verifiedToken, err := a.oidcVerifier.Verify(ctx, token)
			if err != nil {
				return "", nil, fmt.Errorf("failed to verify token: %w", err)
			}

			claim, err := a.getClaims(verifiedToken)
			if err != nil {
				return "", nil, fmt.Errorf("could not get claim from token: %w", err)
			}

			return token, claim, nil
		}()
		if err != nil {
			a.logger.Info(fmt.Sprintf("unauthorized request: %s", err))
			http.Redirect(w, r, "/oidc/authenticate", http.StatusFound)
			return
		}

		ctx = context.WithValue(ctx, auth.ContextAuthenticationDataKey, &authentication.OIDCData{
			Token:  token,
			Email:  claim.Email,
			Name:   claim.Name,
			Groups: claim.Groups,
		})

		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (a *Authenticator) authenticate(w http.ResponseWriter, r *http.Request) {
	session, err := a.store.Get(r, cookieName)
	if err != nil {
		a.logger.Error("unable to get nlx management cookie from the store", err)
		http.Error(w, "unauthorized request", http.StatusUnauthorized)

		return
	}

	bearerToken, ok := session.Values[tokenName].(string)
	if ok {
		_, err := a.oidcVerifier.Verify(r.Context(), bearerToken)
		if err == nil {
			a.logger.Error("cannot verify token", err)

			http.Redirect(w, r, "/", http.StatusFound)

			return
		}
	}

	if _, ok := session.Values[tokenName]; ok {
		delete(session.Values, tokenName)

		err := session.Save(r, w)
		if err != nil {
			a.logger.Error("unable to save session", err)

			http.Error(w, "unable to save session", http.StatusInternalServerError)

			return
		}
	}

	http.Redirect(w, r, a.oauth2Config.AuthCodeURL(""), http.StatusFound)
}

func (a *Authenticator) me(w http.ResponseWriter, r *http.Request) {
	session, err := a.store.Get(r, cookieName)
	if err != nil {
		a.logger.Error("unable to get nlx management cookie from the store", err)

		http.Error(w, "unauthorized request", http.StatusUnauthorized)

		return
	}

	bearerToken, ok := session.Values[tokenName].(string)
	if !ok {
		a.logger.Info("failed to get token from session")

		http.Error(w, "unauthorized request, could not get bearer token", http.StatusUnauthorized)

		return
	}

	idToken, err := a.oidcVerifier.Verify(r.Context(), bearerToken)
	if err != nil {
		a.logger.Error("cannot verify token", err)

		http.Error(w, "unauthorized request, invalid token", http.StatusUnauthorized)

		return
	}

	// first, load the claims
	claims := &Claims{}

	if err = idToken.Claims(claims); err != nil {
		a.logger.Error("unable to parse id token claims", err)

		http.Error(w, "unauthorized request, could not parse token", http.StatusUnauthorized)

		return
	}

	render.JSON(w, r, claims.User())
}

func (a *Authenticator) callback(w http.ResponseWriter, r *http.Request) {
	err := func() error {
		ctx := r.Context()

		session, err := a.store.Get(r, cookieName)
		if err != nil {
			return errors.New("could not get session from cookie")
		}

		accessToken, err := a.oauth2Config.Exchange(ctx, r.URL.Query().Get("code"))
		if err != nil {
			return err
		}

		token, ok := accessToken.Extra("id_token").(string)
		if !ok {
			return errors.New("could not parse id token")
		}

		idToken, err := a.oidcVerifier.Verify(ctx, token)
		if err != nil {
			return errors.New("could not verify id token")
		}

		// first, load the claims
		claims := &Claims{}

		if err = idToken.Claims(claims); err != nil {
			return errors.New("could not extract claims from id-token")
		}

		session.Values[tokenName] = accessToken.AccessToken

		// Convert Unix timestamp in claim to amount of seconds from now.
		// MaxAge needs seconds from now
		session.Options.MaxAge = int(time.Until(time.Unix(claims.ExpiresAt, 0)).Seconds())

		// Only send cookie over https connection
		session.Options.Secure = a.sessionCookieSecure

		if err = session.Save(r, w); err != nil {
			return fmt.Errorf("failed to save session: %w", err)
		}

		return nil
	}()
	if err != nil {
		a.logger.Error("authentication failed", err)

		// if errors.Is(err, database.ErrNotFound) {
		// 	http.Redirect(w, r, "/login#auth-missing-user", http.StatusFound)

		// 	return
		// }

		http.Redirect(w, r, "/login#auth-fail", http.StatusFound)

		return
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

func (a *Authenticator) logout(w http.ResponseWriter, r *http.Request) {
	session, err := a.store.Get(r, cookieName)
	if err != nil {
		http.Redirect(w, r, a.logoutURL, http.StatusFound)
		return
	}

	delete(session.Values, tokenName)

	err = session.Save(r, w)
	if err != nil {
		a.logger.Error("unable to save session", err)

		http.Error(w, "unable to save session", http.StatusInternalServerError)

		return
	}

	http.Redirect(w, r, a.logoutURL, http.StatusFound)
}
