// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package pgsessionstore_test

import (
	"context"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/stretchr/testify/require"

	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication/oidc/pgsessionstore"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication/oidc/pgsessionstore/migrations"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

const (
	maxConnLifetime       = 5 * time.Minute
	maxConnLifetimeJitter = 1 * time.Minute
	maxConns              = 100
	minConns              = 5
	healthCheckPeriod     = 1 * time.Minute
)

func newDB(t *testing.T) *pgxpool.Pool {
	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(t.Name())

	dsn, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		t.Fatal(err)
	}

	dsnForMigrations := testingutils.AddQueryParamToAddress(dsn, "x-migrations-table", dbName)

	err = migrations.PerformMigrations(dsnForMigrations)
	if err != nil {
		t.Fatal(err)
	}

	conn, err := newConnection(context.Background(), dsn)
	require.NoError(t, err)

	return conn
}

func newSessionStore(t *testing.T, secret string) (store *pgsessionstore.PGStore) {
	tx := newDB(t)

	s, err := pgsessionstore.New(discardlogger.New(), tx, []byte(secret))
	require.NoError(t, err)

	return s
}

func newConnection(ctx context.Context, dsn string) (*pgxpool.Pool, error) {
	config, err := pgxpool.ParseConfig(dsn)
	if err != nil {
		return nil, fmt.Errorf("could not open connection to postgres: %s", err)
	}

	config.MaxConnLifetime = maxConnLifetime
	config.MaxConnLifetimeJitter = maxConnLifetimeJitter
	config.MaxConns = maxConns
	config.MinConns = minConns
	config.HealthCheckPeriod = healthCheckPeriod

	db, err := pgxpool.NewWithConfig(ctx, config)
	if err != nil {
		return nil, fmt.Errorf("could not open connection to postgres: %s", err)
	}

	return db, nil
}
