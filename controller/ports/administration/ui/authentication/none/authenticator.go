// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package none

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	auth "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication"
)

type None struct{}

func New() *None {
	return &None{}
}

func (n *None) OnlyAuthenticated(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), auth.ContextAuthenticationDataKey, &authentication.NoneData{
			ID:     "admin",
			Groups: []string{"fsc-admin"},
		})))
	})
}

func (n *None) MountRoutes(r chi.Router) {
	r.Get("/logout", n.logout)
}

func (n *None) LogoutURL() string {
	return "/logout"
}

func (n *None) logout(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/", http.StatusFound)
}
