// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) auditLogsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	resp, err := s.apiApp.Queries.ListAuditlogRecords.Handle(ctx, &query.ListAuditlogRecordsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Pagination:            &query.Pagination{},
		Filters: &query.AuditlogFilters{
			Period: &query.Period{},
		},
	})
	if err != nil {
		s.logger.Error("could not render audit logs page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	page := auditLogsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathAuditLogsPage,
			Title:                       s.i18n.Translate("Audit Logs"),
			Description:                 s.i18n.Translate("The latest 100 audit logs of your organization"),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
		},
		Records: mapQueryAuditlog(resp),
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render audit logs page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func mapQueryAuditlog(recs query.AuditlogRecords) []*auditlog {
	records := make([]*auditlog, 0, len(recs))

	for _, r := range recs {
		icon := mapAuditlogEventToIcon(r)

		record := &auditlog{
			Icon:          icon,
			ID:            r.ID,
			GroupID:       r.GroupID,
			CorrelationID: r.CorrelationID,
			Component:     mapAuditlogComponent(r.Component),
			CreatedAt:     r.CreatedAt.UTC(),
		}

		mapAuditlogActor(r, record)
		mapAuditlogEvent(r, record)
		mapAuditlogSource(r, record)
		mapAuditlogStatus(r, record)

		records = append(records, record)
	}

	return records
}

func mapAuditlogActor(r *query.AuditlogRecord, record *auditlog) {
	switch a := r.Actor.(type) {
	case *query.AuditlogActorUser:
		record.ActorType = "user"
		record.ActorUserName = a.Name
		record.ActorUserEmail = a.Email
	case *query.AuditlogActorService:
		record.ActorType = "service"
		record.ActorServiceSubjectCommonName = a.SubjectCommonName
		record.ActorServicePublicKeyThumbprint = a.PublicKeyThumbprint
	case *query.AuditlogActorUnknown:
		record.ActorType = "unknown"
	default:
		return
	}
}

func mapAuditlogEvent(r *query.AuditlogRecord, record *auditlog) {
	switch e := r.Event.(type) {
	case *query.AuditlogEventLogin:
		record.EventType = "login"

	case *query.AuditlogEventCreateContract:
		record.EventType = "create_contract"
		record.EventContractHash = e.ContractHash
	case *query.AuditlogEventAcceptContract:
		record.EventType = "accept_contract"
		record.EventContractHash = e.ContractHash
	case *query.AuditlogEventRevokeContract:
		record.EventType = "revoke_contract"
		record.EventContractHash = e.ContractHash
	case *query.AuditlogEventRejectContract:
		record.EventType = "reject_contract"
		record.EventContractHash = e.ContractHash

	case *query.AuditlogEventCreateService:
		record.EventType = "create_service"
		record.EventServiceName = e.ServiceName
	case *query.AuditlogEventUpdateService:
		record.EventType = "update_service"
		record.EventServiceName = e.ServiceName
	case *query.AuditlogEventDeleteService:
		record.EventType = "delete_service"
		record.EventServiceName = e.ServiceName

	case *query.AuditlogEventCreatePeer:
		record.EventType = "create_peer"
		record.EventPeerID = e.PeerID

	case *query.AuditlogEventUpdatePeer:
		record.EventType = "update_peer"
		record.EventPeerID = e.PeerID

	case *query.AuditlogEventSynchronizePeers:
		record.EventType = "synchronize_peers"

	case *query.AuditlogEventFailedDistributionRetry:
		record.EventType = "failed_distribution_retry"
		record.EventContractHash = e.ContractHash
		record.EventAction = e.Action
		record.EventPeerID = e.PeerID
	default:
		return
	}
}

//nolint:goconst // making icon-organization a constant would make it harder to understand this code
func mapAuditlogEventToIcon(r *query.AuditlogRecord) string {
	switch r.Event.(type) {
	case *query.AuditlogEventFailedDistributionRetry:
		return "icon-exchange"
	case *query.AuditlogEventLogin:
		return "icon-shut-down-line"
	case *query.AuditlogEventCreateContract:
		return "icon-contracts"
	case *query.AuditlogEventAcceptContract:
		return "icon-check-fill"
	case *query.AuditlogEventRevokeContract:
		return "icon-arrow-go-back-fill"
	case *query.AuditlogEventRejectContract:
		return "icon-close-fill"
	case *query.AuditlogEventCreatePeer:
		return "icon-organization"
	case *query.AuditlogEventUpdatePeer:
		return "icon-organization"
	case *query.AuditlogEventSynchronizePeers:
		return "icon-organization"
	case *query.AuditlogEventCreateService,
		*query.AuditlogEventUpdateService,
		*query.AuditlogEventDeleteService:
		return "icon-services"
	}

	return ""
}

func mapAuditlogSource(r *query.AuditlogRecord, record *auditlog) {
	switch e := r.Source.(type) {
	case *query.AuditlogSourceHTTP:
		record.SourceType = "http"
		record.SourceHTTPIPAddress = e.IPAddress.String()
		record.SourceHTTPUserAgent = e.UserAgent
	default:
		return
	}
}

func mapAuditlogStatus(r *query.AuditlogRecord, record *auditlog) {
	switch e := r.Status.(type) {
	case *query.AuditlogStatusInitiated:
		record.StatusType = "initiated"
	case *query.AuditlogStatusUnauthorized:
		record.StatusType = "unauthorized"
		record.StatusError = e.Error
	case *query.AuditlogStatusFailed:
		record.StatusType = "failed"
		record.StatusError = e.Error
	case *query.AuditlogStatusSucceeded:
		record.StatusType = "succeeded"
	default:
		return
	}
}

func mapAuditlogComponent(c query.AuditlogComponent) string {
	switch c {
	case query.AuditlogComponentController:
		return "controller"
	case query.AuditlogComponentManager:
		return "manager"
	default:
		return ""
	}
}
