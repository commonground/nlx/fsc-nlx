// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

type addServicePage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage string
	ErrorMessages  []string
	InwayAddresses []string
	Form           *AddServicePageForm
}

type AddServicePageForm struct {
	Name         string
	EndpointURL  string
	InwayAddress string
}

func decodeAddServicePageForm(decoder *schema.Decoder, r *http.Request) (*AddServicePageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result AddServicePageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *addServicePage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, message := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessages = message

	p.render(w)
}

func (p *addServicePage) renderWithErrorMessage(w http.ResponseWriter, statusCode int, message []string) {
	w.WriteHeader(statusCode)

	p.ErrorMessages = message

	p.render(w)
}

func (p *addServicePage) renderWithSuccess(successMessage string, w http.ResponseWriter) {
	p.SuccessMessage = successMessage
	p.render(w)
}

func (p *addServicePage) render(w http.ResponseWriter) {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/components/errors.html",
			"templates/add-service.html",
		)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)
		return
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)
		return
	}
}
