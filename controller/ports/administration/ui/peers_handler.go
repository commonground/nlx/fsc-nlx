// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) peersHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	page := peersPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathPeersPage,
			Title:                       s.i18n.Translate("Peers"),
			Description:                 s.i18n.Translate("The Peers known by your organization"),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
	}

	if r.URL.Query().Get("sync") != "" {
		auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
		if err != nil {
			s.logger.Error("cannot obtain connection info needed for Audit log", err)
			http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

			return
		}

		syncPeerResponse, err := s.apiApp.Commands.SyncPeers.Handle(ctx, &command.SyncPeersArgs{
			AuthData:              authData,
			AuthorizationMetadata: authData,
			AuditLogSource:        auditLogSource,
		})
		if err != nil {
			s.logger.Error("could not sync the peers", err)

			page.Error = s.i18n.Translate("Unable to synchronize Peers from the Directories")

			err = page.render(w)
			if err != nil {
				s.logger.Error("could not render peers page", err)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

				return
			}

			return
		}

		syncSuccess := true

		for peerID, syncResult := range syncPeerResponse {
			res := &syncResultLine{
				Ok:     syncResult.Ok,
				PeerID: peerID,
			}

			if !syncResult.Ok {
				if syncResult.Err != nil {
					res.ErrMsg = syncResult.Err.Error()
				}

				syncSuccess = false
			}

			page.SyncResults = append(page.SyncResults, res)
		}

		page.SyncSuccess = syncSuccess
	}

	resp, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData: authData,
	})
	if err != nil {
		s.logger.Error("could not render peers page", err)

		page.Error = s.i18n.Translate("Unable to retrieve the Peers from the Manager")

		err = page.render(w)
		if err != nil {
			s.logger.Error("could not render peers page", err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

			return
		}

		return
	}

	for _, syncRes := range page.SyncResults {
		peerData, exists := resp[syncRes.PeerID]
		if exists {
			syncRes.PeerName = peerData.Name
		}
	}

	peers := mapQueryToUIPeers(resp)

	page.Peers = peers
	page.AmountOfPeers = uint(len(peers))

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render peers page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func mapQueryToUIPeers(qPeers query.ListPeersPeers) []*peersPagePeer {
	peers := []*peersPagePeer{}

	for _, p := range qPeers {
		roles := []string{}

		for _, role := range p.Roles {
			if role == query.PeerRoleDirectory {
				roles = append(roles, allPeerRoles[peerRoleDirectory])
			}
		}

		peers = append(peers, &peersPagePeer{
			ID:             p.ID,
			Name:           p.Name,
			ManagerAddress: p.ManagerAddress,
			Roles:          strings.Join(roles, ", "),
		})
	}

	return peers
}
