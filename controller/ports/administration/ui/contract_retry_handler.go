/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package uiport

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (s *Server) contractRetryHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	contentHash := chi.URLParam(r, "hash")

	peerID := chi.URLParam(r, "peerID")

	action := chi.URLParam(r, "action")
	commandAction, err := convertAction(action)

	if err != nil {
		s.logger.Error("failed to parse action", err)

		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	args := &command.RetryContractHandlerArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		AuditLogSource:        auditLogSource,
		ContentHash:           contentHash,
		PeerID:                peerID,
		Action:                commandAction,
	}

	err = s.apiApp.Commands.RetryContract.Handle(ctx, args)
	if err != nil {
		s.logger.Error("failed to trigger retry", err)

		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Location", fmt.Sprintf("/contract/%s", contentHash))
	w.WriteHeader(http.StatusSeeOther)
}

func convertAction(action string) (command.Action, error) {
	switch action {
	case string(models.DISTRIBUTIONACTIONSUBMITCONTRACT):
		return command.SubmitContract, nil
	case string(models.DISTRIBUTIONACTIONSUBMITACCEPTSIGNATURE):
		return command.SubmitAcceptSignature, nil
	case string(models.DISTRIBUTIONACTIONSUBMITREVOKESIGNATURE):
		return command.SubmitRevokeSignature, nil
	case string(models.DISTRIBUTIONACTIONSUBMITREJECTSIGNATURE):
		return command.SubmitRejectSignature, nil
	default:
		return -1, errors.New("Action is not valid")
	}
}
