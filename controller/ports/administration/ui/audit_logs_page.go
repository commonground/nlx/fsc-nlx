// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"
	"time"
)

type auditLogsPage struct {
	*BasePage
	BaseAuthenticatedPage

	Records []*auditlog
}

type auditlog struct {
	Icon          string
	ID            string
	GroupID       string
	CorrelationID string

	ActorType                       string
	ActorUserName                   string
	ActorUserEmail                  string
	ActorServiceSubjectCommonName   string
	ActorServicePublicKeyThumbprint string

	EventType         string
	EventContractHash string
	EventServiceName  string
	EventAction       string
	EventPeerID       string

	SourceType          string
	SourceHTTPIPAddress string
	SourceHTTPUserAgent string

	StatusType  string
	StatusError string

	Component string
	CreatedAt time.Time
}

func (p *auditLogsPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/audit-logs.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
