// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

// nolint:dupl // this is not a duplicate of the directory
func (s *Server) delegatedServicePublicationsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	const paginationLimit = uint32(10)

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	paginationCursor := r.URL.Query().Get("pagination_cursor")

	paginationNextCursor := paginationCursor

	selectedTab := chi.URLParam(r, "selected_tab")

	if selectedTab == "" {
		selectedTab = pageTabActive
	}

	contractStateFilter, err := getContractStateFilterForSelectedTab(selectedTab)
	if err != nil {
		s.logger.Info(err.Error())
		http.Error(w, fmt.Sprintf("%d", http.StatusNotFound), http.StatusNotFound)

		return
	}

	delegatedServicePublications, nextCursor, err := s.apiApp.Queries.ListDelegatedServicePublications.Handle(ctx, &query.ListDelegatedServicePublicationsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Pagination: &query.ListDelegatedServicePublicationsPagination{
			Cursor:    paginationCursor,
			Limit:     paginationLimit,
			SortOrder: query.SortOrderDescending,
		},
		ContractStates: contractStateFilter,
	})
	if err != nil {
		s.logger.Error("could not get Delegated Service Publications from Manager", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if nextCursor != "" {
		paginationNextCursor = nextCursor
	}

	peerIDs := make([]string, 0)

	for _, p := range delegatedServicePublications.Publications {
		peerIDs = append(peerIDs, p.DirectoryPeerID, p.ServicePeerID)
		if p.DelegatorPeerID != "" {
			peerIDs = append(peerIDs, p.DelegatorPeerID)
		}
	}

	peersInfo, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		PeerIDs:               &peerIDs,
	})
	if err != nil {
		s.logger.Error("could not get details for peers", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	pCollection := newPeersCollection(peersInfo)

	results := convertPublications(delegatedServicePublications.Publications, pCollection)

	page := delegatedServicePublicationsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathDelegatedServicePublicationsPage,
			Title:                       s.i18n.Translate("Delegated Publications"),
			Description:                 s.i18n.Translate("Overview of Services that are published on by another organization on your behalf."),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
		},
		GrantTypeFilters:                getGrantTypeFilters(),
		Publications:                    results,
		AmountOfPublications:            delegatedServicePublications.TotalCount,
		SelectedTab:                     selectedTab,
		PaginationNextCursor:            paginationNextCursor,
		PaginationLimit:                 paginationLimit,
		ContractDetailBackButtonURLPath: url.QueryEscape(fmt.Sprintf("%s/%s?pagination_cursor=%s", PathDelegatedServicePublicationsPage, selectedTab, r.URL.Query().Get("pagination_cursor"))),
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render delegated publications page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}
