// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"
	"time"
)

type actionsPage struct {
	*BasePage
	BaseAuthenticatedPage

	AmountOfPendingContracts        int
	PendingContracts                []*actionsPagePendingContract
	Peers                           map[string]string
	PaginationNextCursor            string
	PaginationLimit                 uint32
	ContractDetailBackButtonURLPath string
}

type actionsPagePendingContract struct {
	Hash      string
	CreatedAt time.Time
	Grants    []*pendingGrant
}

type pendingGrant struct {
	ServiceName                   string
	ServicePeerID                 string
	ConsumerPeerID                string
	DelegatorPeerID               string
	ServicePublicationDelegatorID string
	GrantType                     grantType
}

type grantType string

const (
	serviceConnection           grantType = "serviceConnection"
	servicePublication          grantType = "servicePublication"
	delegatedServiceConnection  grantType = "delegatedServiceConnection"
	delegatedServicePublication grantType = "delegatedServicePublication"
)

func (p *actionsPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/actions.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
