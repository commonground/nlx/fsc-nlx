// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"errors"
	"fmt"
	"math"
	"net/http"
	"net/netip"
	"time"

	common_auditlog "gitlab.com/commonground/nlx/fsc-nlx/common/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func mapError(err error) (statusCode int, message []string) {
	var e *command.ValidationError
	if errors.As(err, &e) {
		return http.StatusBadRequest, e.Messages()
	}

	var ie *command.InternalError
	if errors.As(err, &ie) {
		return http.StatusInternalServerError, []string{ie.Error()}
	}

	var authErr *auth.AuthenticationError
	if errors.As(err, &authErr) {
		return http.StatusForbidden, []string{authErr.Error()}
	}

	return http.StatusInternalServerError, []string{fmt.Sprintf("unexpected error: %s", err)}
}

func (s *Server) getAuthDataFromCtx(ctx context.Context, w http.ResponseWriter) (auth.Data, bool) {
	v := ctx.Value(authentication.ContextAuthenticationDataKey)
	if v == nil {
		s.logger.Error("failed to authenticate", fmt.Errorf("this route needs to be added to the OnlyAuthenticated middleware"))
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return nil, false
	}

	switch a := v.(type) {
	case *auth.OIDCData:
		return a, true
	case *auth.NoneData:
		return a, true
	default:
		s.logger.Error("failed to authenticate", fmt.Errorf("invalid authenticationData type: %T", a))
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return nil, false
	}
}

func getAuthzData(r *http.Request) *authorization.RestMetaData {
	return &authorization.RestMetaData{
		Headers: r.Header,
		URL:     r.URL.String(),
	}
}

func (s *Server) getPeerInfo(ctx context.Context, authzData *authorization.RestMetaData, authData auth.Data) *PeerInfo {
	if s.peerInfo != nil {
		return s.peerInfo
	}

	p, err := s.apiApp.Queries.GetPeerInfo.Handle(ctx, &query.GetPeerInfoArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
	})
	if err != nil {
		// this info is not important enough to error on
		return &PeerInfo{
			ID:   "",
			Name: "",
		}
	}

	peerInfo := &PeerInfo{
		ID:   p.ID,
		Name: p.Name,
	}

	s.peerInfo = peerInfo

	return peerInfo
}

func AuditLogSourceFromHTTPRequest(r *http.Request) (common_auditlog.Source, error) {
	addr, err := netip.ParseAddrPort(r.RemoteAddr)
	if err != nil {
		return nil, errors.Join(err, errors.New("cannot obtain client IP Address from HTTP request"))
	}

	return common_auditlog.SourceHTTP{
		IPAddress: addr.Addr(),
		UserAgent: r.UserAgent(),
	}, nil
}

func contractRemainingDaysValid(notBefore, notAfter time.Time) int {
	const hoursPerDay = 24

	hoursRemaining := notAfter.Sub(notBefore).Hours()
	daysRemaining := math.RoundToEven(hoursRemaining / hoursPerDay)

	return max(1, int(daysRemaining))
}

const (
	pageTabActive   = "active"
	pageTabProposed = "proposed"
	pageTabArchive  = "archive"
)

func getContractStateFilterForSelectedTab(selectedTab string) ([]contract.ContractState, error) {
	var result []contract.ContractState

	switch selectedTab {
	case pageTabActive:
		result = []contract.ContractState{
			contract.ContractStateValid,
		}
	case pageTabProposed:
		result = []contract.ContractState{
			contract.ContractStateProposed,
		}
	case pageTabArchive:
		result = []contract.ContractState{
			contract.ContractStateRejected,
			contract.ContractStateExpired,
			contract.ContractStateRevoked,
		}
	default:
		return nil, fmt.Errorf("invalid tab %q", selectedTab)
	}

	return result, nil
}

const peerRoleDirectory = "directory"

var allPeerRoles = map[string]string{
	peerRoleDirectory: "Directory",
}

func peerRolesToCommandRoles(roles []string) []command.PeerRole {
	var result []command.PeerRole

	for _, r := range roles {
		if r == peerRoleDirectory {
			result = append(result, command.DirectoryRole)
		}
	}

	return result
}

func mapPeerRolesToUI(roles []query.PeerRole) []string {
	result := []string{}

	for _, role := range roles {
		if role == query.PeerRoleDirectory {
			result = append(result, peerRoleDirectory)
		}
	}

	return result
}
