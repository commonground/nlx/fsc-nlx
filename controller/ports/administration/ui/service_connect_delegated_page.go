// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"
)

type connectToServiceDelegatedPage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage                     string
	ErrorMessages                      []string
	OutwayPublicKeyThumbprints         map[string][]string
	PeerIDs                            map[string]string
	Form                               *ConnectToServiceDelegatedPageForm
	ActionAddOutwayPublicKeyThumbprint string
}

type ConnectToServiceDelegatedPageForm struct {
	Action                     string
	ServiceName                string
	OutwayPeerID               string
	ServiceProviderPeerID      string
	ServiceDelegatorPeerID     string
	OutwayPublicKeyThumbprints []string
	ValidFrom                  string
	ValidUntil                 string
}

func decodeConnectToServiceDelegatedPageForm(decoder *schema.Decoder, r *http.Request) (*ConnectToServiceDelegatedPageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result ConnectToServiceDelegatedPageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *connectToServiceDelegatedPage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, messages := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessages = messages

	p.render(w)
}

func (p *connectToServiceDelegatedPage) renderWithSuccess(successMessage string, w http.ResponseWriter) {
	p.SuccessMessage = successMessage
	p.render(w)
}

// nolint:dupl // looks the same but is different from controller/ports/ui/service_publish_page.go
func (p *connectToServiceDelegatedPage) render(w http.ResponseWriter) {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/connect-to-service-delegated.html",
			"templates/components/errors.html",
		)
	if err != nil {
		p.logger.Error("failed to load templates for the connect to service delegated page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		p.logger.Error("failed to execute template for the connect to service delegated page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}
