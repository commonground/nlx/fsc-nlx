// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

// nolint:dupl,funlen // this is not a duplicate of the directory
func (s *Server) servicePublicationsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	const paginationLimit = uint32(10)

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	paginationCursor := r.URL.Query().Get("pagination_cursor")

	paginationNextCursor := paginationCursor

	selectedTab := chi.URLParam(r, "selected_tab")

	if selectedTab == "" {
		selectedTab = pageTabActive
	}

	contractStateFilter, err := getContractStateFilterForSelectedTab(selectedTab)
	if err != nil {
		s.logger.Info(err.Error())
		http.Error(w, fmt.Sprintf("%d", http.StatusNotFound), http.StatusNotFound)

		return
	}

	servicePublications, nextCursor, err := s.apiApp.Queries.ListServicePublications.Handle(ctx, &query.ListServicePublicationsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Pagination: &query.Pagination{
			StartID:   paginationCursor,
			Limit:     paginationLimit,
			SortOrder: query.SortOrderDescending,
		},
		ContractStates: contractStateFilter,
	})
	if err != nil {
		s.logger.Error("could not get Service Publications from Manager", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if nextCursor != "" {
		paginationNextCursor = nextCursor
	}

	peerIDs := make([]string, 0)

	for _, p := range servicePublications.Publications {
		peerIDs = append(peerIDs, p.DirectoryPeerID)
		if p.DelegatorPeerID != "" {
			peerIDs = append(peerIDs, p.DelegatorPeerID)
		}
	}

	peersInfo, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		PeerIDs:               &peerIDs,
	})
	if err != nil {
		s.logger.Error("could not get details for peers", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	pCollection := newPeersCollection(peersInfo)

	results := convertPublications(servicePublications.Publications, pCollection)

	page := servicePublicationsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicePublicationsPage,
			Title:                       s.i18n.Translate("Publications"),
			Description:                 s.i18n.Translate("View all published services of your organization ordered by creation date."),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
		},
		GrantTypeFilters:                getGrantTypeFilters(),
		Publications:                    results,
		AmountOfPublications:            servicePublications.TotalCount,
		SelectedTab:                     selectedTab,
		PaginationNextCursor:            paginationNextCursor,
		PaginationLimit:                 paginationLimit,
		ContractDetailBackButtonURLPath: url.QueryEscape(fmt.Sprintf("%s/%s?pagination_cursor=%s", PathServicePublicationsPage, selectedTab, paginationCursor)),
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render publications page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func convertPublications(pPublications []*query.Publication, pCollection *peersCollection) []*publication {
	publications := make([]*publication, len(pPublications))

	for i, p := range pPublications {
		publications[i] = &publication{
			ServicePeerID:        p.ServicePeerID,
			ServicePeerName:      pCollection.NameForPeerID(p.ServicePeerID),
			ServiceName:          p.ServiceName,
			ContentHash:          p.ContentHash,
			CreatedAt:            p.CreatedAt,
			ValidNotBefore:       p.ValidNotBefore,
			ValidNotAfter:        p.ValidNotAfter,
			State:                p.State,
			RemainingDaysValid:   contractRemainingDaysValid(p.ValidNotBefore, p.ValidNotAfter),
			GrantHash:            p.GrantHash,
			GrantHashURLFriendly: urlFriendlyGrantHash(p.GrantHash),
			DirectoryPeerID:      p.DirectoryPeerID,
			DirectoryPeerName:    pCollection.NameForPeerID(p.DirectoryPeerID),
			DelegatorPeerID:      p.DelegatorPeerID,
			DelegatorPeerName:    pCollection.NameForPeerID(p.DelegatorPeerID),
		}
	}

	return publications
}
