// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) actionsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	const paginationLimit = uint32(10)

	paginationCursor := r.URL.Query().Get("pagination_cursor")

	resp, nextCursor, err := s.apiApp.Queries.ListContractsPending.Handle(ctx, &query.ListPendingContractsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Pagination: &query.ListPendingContractsPagination{
			Cursor: paginationCursor,
			Limit:  paginationLimit,
		},
	})
	if err != nil {
		s.logger.Error("could not render action page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	pendingContracts := mapQueryToUIPendingContracts(resp)

	pMap, err := s.listPeers(ctx, authData)
	if err != nil {
		s.logger.Error("could not obtain peer info for pending contracts", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if nextCursor != "" {
		paginationCursor = nextCursor
	}

	page := actionsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathActionsPage,
			Title:                       s.i18n.Translate("Actions"),
			Description:                 "",
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
		AmountOfPendingContracts:        resp.TotalCount,
		PendingContracts:                pendingContracts,
		Peers:                           pMap,
		PaginationNextCursor:            paginationCursor,
		PaginationLimit:                 paginationLimit,
		ContractDetailBackButtonURLPath: url.QueryEscape(fmt.Sprintf("%s?pagination_cursor=%s", PathActionsPage, r.URL.Query().Get("pagination_cursor"))),
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render actions page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func mapQueryToUIPendingContracts(contractsPending *query.PendingContracts) []*actionsPagePendingContract {
	pendingContracts := []*actionsPagePendingContract{}

	for _, contractPending := range contractsPending.PendingContracts {
		pendingContract := &actionsPagePendingContract{
			Hash:      contractPending.ContentHash,
			CreatedAt: contractPending.CreatedAt,
		}

		for _, grant := range contractPending.Grants {
			pg := &pendingGrant{
				ServiceName:                   grant.ServiceName,
				ServicePeerID:                 strings.TrimSpace(grant.ServicePeerID),
				ConsumerPeerID:                strings.TrimSpace(grant.OutwayPeerID),
				DelegatorPeerID:               strings.TrimSpace(grant.DelegatorPeerID),
				ServicePublicationDelegatorID: grant.ServicePublicationDelegatorID}

			switch grant.GrantType {
			case query.GrantTypeServicePublication:
				pg.GrantType = servicePublication
			case query.GrantTypeDelegatedServicePublication:
				pg.GrantType = delegatedServicePublication
			case query.GrantTypeServiceConnection:
				pg.GrantType = serviceConnection
			case query.GrantTypeDelegatedServiceConnection:
				pg.GrantType = delegatedServiceConnection
			}

			pendingContract.Grants = append(pendingContract.Grants, pg)
		}

		pendingContracts = append(pendingContracts, pendingContract)
	}

	return pendingContracts
}
