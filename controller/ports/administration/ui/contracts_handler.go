// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

// nolint:dupl // this is not a duplicate of the directory
func (s *Server) contractsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	const paginationLimit = uint32(10)

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	grantTypeFilter := r.URL.Query().Get("grant_type")

	paginationCursor := r.URL.Query().Get("pagination_cursor")

	paginationNextCursor := paginationCursor

	resp, nextCursor, err := s.apiApp.Queries.ListContracts.Handle(ctx, &query.ListContractsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Pagination: &query.ListContractsPagination{
			Cursor: paginationCursor,
			Limit:  paginationLimit,
		},
		Filters: []*query.ListContractsFilter{{
			GrantType: query.GrantType(grantTypeFilter),
		}},
	})
	if err != nil {
		s.logger.Error("could not render contracts page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if nextCursor != "" {
		paginationNextCursor = nextCursor
	}

	contractsPeerIDs := []string{}

	for _, c := range resp {
		contractsPeerIDs = append(contractsPeerIDs, c.Peers...)
	}

	peersInfo, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		PeerIDs:               &contractsPeerIDs,
	})
	if err != nil {
		s.logger.Error("could not get details for peers", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	pCollection := newPeersCollection(peersInfo)

	contracts := mapQueryToUIContracts(resp, pCollection)

	page := contractsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
			Title:                       s.i18n.Translate("Contracts"),
			Description:                 s.i18n.Translate("Manage the Contracts of your organization."),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
		},
		GrantTypeFilters:     getGrantTypeFilters(),
		GrantType:            grantTypeFilter,
		Contracts:            contracts,
		PaginationNextCursor: paginationNextCursor,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render contracts page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func mapQueryToUIContracts(qContracts query.Contracts, pCollection *peersCollection) []*contractsPageContract {
	contracts := make([]*contractsPageContract, len(qContracts))

	for i, c := range qContracts {
		contract := &contractsPageContract{
			IV:                                c.IV,
			RemainingDaysValid:                contractRemainingDaysValid(c.ValidFrom, c.ValidUntil),
			Hash:                              c.Hash,
			CreatedAt:                         c.CreatedAt.UTC(),
			State:                             convertStateToHumanReadableText(c.State),
			ServicePublicationGrants:          make([]*ServicePublicationGrant, len(c.ServicePublicationGrants)),
			ServiceConnectionGrants:           make([]*ServiceConnectionGrant, len(c.ServiceConnectionGrants)),
			DelegatedServicePublicationGrants: make([]*DelegatedServicePublicationGrant, len(c.DelegatedServicePublicationGrants)),
			DelegatedServiceConnectionGrants:  make([]*DelegatedServiceConnectionGrant, len(c.DelegatedServiceConnectionGrants)),
		}

		for i, grant := range c.ServicePublicationGrants {
			contract.ServicePublicationGrants[i] = &ServicePublicationGrant{
				DirectoryPeerID:   grant.DirectoryPeerID,
				DirectoryPeerName: pCollection.NameForPeerID(grant.DirectoryPeerID),
				ServicePeerID:     grant.ServicePeerID,
				ServicePeerName:   pCollection.NameForPeerID(grant.ServicePeerID),
				ServiceName:       grant.ServiceName,
			}
		}

		for i, grant := range c.ServiceConnectionGrants {
			contract.ServiceConnectionGrants[i] = &ServiceConnectionGrant{
				ServicePeerID:             grant.ServicePeerID,
				ServicePeerName:           pCollection.NameForPeerID(grant.ServicePeerID),
				ServiceName:               grant.ServiceName,
				OutwayPeerID:              grant.OutwayPeerID,
				OutwayPeerName:            pCollection.NameForPeerID(grant.OutwayPeerID),
				OutwayPublicKeyThumbprint: grant.OutwayPublicKeyThumbprint,
			}
		}

		for i, grant := range c.DelegatedServicePublicationGrants {
			contract.DelegatedServicePublicationGrants[i] = &DelegatedServicePublicationGrant{
				DirectoryPeerID:   grant.DirectoryPeerID,
				DirectoryPeerName: pCollection.NameForPeerID(grant.DirectoryPeerID),
				DelegatorPeerID:   grant.DelegatorPeerID,
				DelegatorPeerName: pCollection.NameForPeerID(grant.DelegatorPeerID),
				ServicePeerID:     grant.ServicePeerID,
				ServicePeerName:   pCollection.NameForPeerID(grant.ServicePeerID),
				ServiceName:       grant.ServiceName,
			}
		}

		for i, grant := range c.DelegatedServiceConnectionGrants {
			contract.DelegatedServiceConnectionGrants[i] = &DelegatedServiceConnectionGrant{
				DelegatorPeerID:           grant.DelegatorPeerID,
				DelegatorPeerName:         pCollection.NameForPeerID(grant.DelegatorPeerID),
				OutwayPeerID:              grant.OutwayPeerID,
				OutwayPeerName:            pCollection.NameForPeerID(grant.OutwayPeerID),
				OutwayPublicKeyThumbprint: grant.OutwayPublicKeyThumbprint,
				ServicePeerID:             grant.ServicePeerID,
				ServicePeerName:           pCollection.NameForPeerID(grant.ServicePeerID),
				ServiceName:               grant.ServiceName,
			}
		}

		contracts[i] = contract
	}

	return contracts
}

func getGrantTypeFilters() map[query.GrantType]string {
	return map[query.GrantType]string{
		query.GrantTypeServicePublication:          "Service Publication",
		query.GrantTypeServiceConnection:           "Service Connection",
		query.GrantTypeDelegatedServicePublication: "Delegated Service Publication",
		query.GrantTypeDelegatedServiceConnection:  "Delegated Service Connection",
	}
}
