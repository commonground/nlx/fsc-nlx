// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

// nolint:dupl // similar but not the same as the archive handler
package uiport

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) outgoingConnectionsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	const paginationLimit = uint32(10)

	paginationCursor := r.URL.Query().Get("pagination_cursor")

	paginationNextCursor := paginationCursor

	selectedTab := chi.URLParam(r, "selected_tab")

	if selectedTab == "" {
		selectedTab = pageTabActive
	}

	contractStateFilter, err := getContractStateFilterForSelectedTab(selectedTab)
	if err != nil {
		s.logger.Info(err.Error())
		http.Error(w, fmt.Sprintf("%d", http.StatusNotFound), http.StatusNotFound)

		return
	}

	resp, nextCursor, err := s.apiApp.Queries.ListOutgoingConnections.Handle(ctx, &query.ListOutgoingConnectionsArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		Pagination: &query.ListOutgoingConnectionsPagination{
			Cursor:    paginationCursor,
			Limit:     paginationLimit,
			SortOrder: query.SortOrderDescending,
		},
		ContractStates: contractStateFilter,
	})
	if err != nil {
		s.logger.Error("could not render outgoing connections page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if nextCursor != "" {
		paginationNextCursor = nextCursor
	}

	contractsPeerIDs := []string{}

	for _, c := range resp.OutgoingConnections {
		contractsPeerIDs = append(contractsPeerIDs, c.GetPeerIDs()...)
	}

	peersInfo, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		PeerIDs:               &contractsPeerIDs,
	})
	if err != nil {
		s.logger.Error("could not get details for peers", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	pCollection := newPeersCollection(peersInfo)
	connections := mapQueryToUIOutgoingConnections(resp.OutgoingConnections, pCollection)

	page := outgoingConnectionsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathOutgoingConnectionsPage,
			Title:                       s.i18n.Translate("Outgoing Connections"),
			Description:                 s.i18n.Translate("Overview of the connections you made ordered by creation date."),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, authzData, authData),
		},
		AmountOfConnections:             resp.TotalCount,
		Connections:                     connections,
		PaginationNextCursor:            paginationNextCursor,
		PaginationLimit:                 paginationLimit,
		SelectedTab:                     selectedTab,
		ContractDetailBackButtonURLPath: url.QueryEscape(fmt.Sprintf("%s/%s?pagination_cursor=%s", PathOutgoingConnectionsPage, selectedTab, paginationCursor)),
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render outgoing connections page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

//nolint:dupl // looks the same as mapQueryToUIIncomingConnections but the data structure is different
func mapQueryToUIOutgoingConnections(qConnections []*query.Connection, pCollection *peersCollection) []*outgoingConnectionsPageConnection {
	result := []*outgoingConnectionsPageConnection{}

	for _, c := range qConnections {
		r := &outgoingConnectionsPageConnection{
			ServiceName:          c.ServiceName,
			ServicePeerID:        c.ServicePeerID,
			ServicePeerName:      pCollection.NameForPeerID(c.ServicePeerID),
			ContentHash:          c.ContentHash,
			GrantHashURLFriendly: urlFriendlyGrantHash(c.GrantHash),
			ValidNotBefore:       c.ValidNotBefore,
			ValidNotAfter:        c.ValidNotAfter,
			RemainingDaysValid:   contractRemainingDaysValid(c.ValidNotBefore, c.ValidNotAfter),
			CreatedAt:            c.CreatedAt,
			State:                c.State,
		}

		if c.DelegatorPeerID != nil {
			r.DelegatorPeerName = pCollection.NameForPeerID(*c.DelegatorPeerID)
			r.DelegatorPeerID = *c.DelegatorPeerID
		}

		if c.ServicePublicationDelegatorPeerID != nil {
			r.ServicePublicationDelegatorPeerID = *c.ServicePublicationDelegatorPeerID
			r.ServicePublicationDelegatorPeerName = pCollection.NameForPeerID(*c.ServicePublicationDelegatorPeerID)
		}

		result = append(result, r)
	}

	return result
}
