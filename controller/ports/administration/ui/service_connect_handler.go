// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/gorilla/csrf"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	api_query "gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

func (s *Server) connectToServiceGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	serviceProviderPeerID := chi.URLParam(r, "peerID")
	serviceName := r.URL.Query().Get("serviceName")
	serviceDelegatorPeerID := r.URL.Query().Get("delegatorID")
	outwayPublicKeyThumbprint := r.URL.Query().Get("outwayPublicKeyThumbprint")

	// If the serviceName is empty get all published services from the serviceProviderPeer from the directory
	directoryPeerServices := make([]string, 0)
	if serviceName == "" && serviceProviderPeerID != "" {
		directoryPeerServices = s.getPublishedServices(ctx, authzData, authData, serviceProviderPeerID)
	}

	page := connectToServicePage{
		ActionAddOutwayPublicKeyThumbprint: formActionAddOutwayPublicKeyThumbprint,
		BasePage:                           s.basePage,
		OutwayPublicKeyThumbprints:         map[string][]string{},
		ServiceNamesForPeer:                directoryPeerServices,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			Title:     s.i18n.Translate("Connect with Service"),
			Username:  authData.Username(),
			LogoutURL: s.authn.LogoutURL(),
			Peer:      s.getPeerInfo(ctx, authzData, authData),
			CsrfToken: csrf.Token(r),
		},
		Form: &ConnectToServicePageForm{
			ServiceName:                serviceName,
			ServiceProviderPeerID:      strings.TrimSpace(serviceProviderPeerID),
			ServiceDelegatorPeerID:     strings.TrimSpace(serviceDelegatorPeerID),
			OutwayPublicKeyThumbprints: []string{outwayPublicKeyThumbprint},
			ValidFrom:                  time.Now().Format("2006-01-02"),
			ValidUntil:                 time.Now().Add(Day).Format("2006-01-02"),
		},
	}

	peerIDList, err := s.listPeers(ctx, authData)
	if err != nil {
		s.logger.Error("could not retrieve PeerID list from manager", err)

		page.renderWithError(w, err)

		return
	}

	page.PeerIDs = peerIDList

	serviceProviderPeer := peerIDList[serviceProviderPeerID]
	if serviceProviderPeerID != "" {
		page.Title = s.i18n.Translate("Connect with Service of %s", serviceProviderPeer)
	}

	outways, err := s.apiApp.Queries.ListOutways.Handle(ctx, &api_query.ListOutwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		SelfPeerID:            s.getPeerInfo(ctx, authzData, authData).ID,
	})
	if err != nil {
		s.logger.Error("failed to retrieve outways", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	owThumbprints := map[string][]string{}

	for _, outway := range outways {
		outwayMap, k := owThumbprints[outway.PublicKeyThumbprint]
		if !k {
			owThumbprints[outway.PublicKeyThumbprint] = []string{outway.Name}
		}

		_ = append(outwayMap, outway.Name)
	}

	page.OutwayPublicKeyThumbprints = owThumbprints

	page.render(w)
}

func (s *Server) connectToServicePostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	authzData := getAuthzData(r)

	servicePeerID := chi.URLParam(r, "peerID")

	formValues, err := decodeConnectToServicePageForm(s.decoder, r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	if servicePeerID != "" {
		formValues.ServiceProviderPeerID = servicePeerID
	}

	page := connectToServicePage{
		ActionAddOutwayPublicKeyThumbprint: formActionAddOutwayPublicKeyThumbprint,
		BasePage:                           s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			Description: s.i18n.Translate("Connect with Service"),
			Username:    authData.Username(),
			LogoutURL:   s.authn.LogoutURL(),
			Peer:        s.getPeerInfo(ctx, authzData, authData),
			CsrfToken:   csrf.Token(r),
		},
		Form: formValues,
	}

	peerIDList, err := s.listPeers(ctx, authData)
	if err != nil {
		s.logger.Error("could not retrieve PeerID list from manager", err)

		page.renderWithError(w, err)

		return
	}

	page.PeerIDs = peerIDList

	page.Title = fmt.Sprintf("%s %s", s.i18n.Translate("Connect with Service from"), peerIDList[formValues.ServiceProviderPeerID])

	selfPeerID := s.getPeerInfo(ctx, authzData, authData).ID

	outways, err := s.apiApp.Queries.ListOutways.Handle(ctx, &api_query.ListOutwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
		SelfPeerID:            selfPeerID,
	})
	if err != nil {
		s.logger.Error("failed to retrieve outways", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	owThumbprints := map[string][]string{}

	for _, outway := range outways {
		outwayMap, k := owThumbprints[outway.PublicKeyThumbprint]
		if !k {
			owThumbprints[outway.PublicKeyThumbprint] = []string{outway.Name}
		}

		_ = append(outwayMap, outway.Name)
	}

	page.OutwayPublicKeyThumbprints = owThumbprints

	if formValues.Action == formActionAddOutwayPublicKeyThumbprint {
		formValues.OutwayPublicKeyThumbprints = append(formValues.OutwayPublicKeyThumbprints, "")
		page.Form = formValues

		page.render(w)

		return
	}

	auditLogSource, err := AuditLogSourceFromHTTPRequest(r)
	if err != nil {
		s.logger.Error("cannot obtain connection info needed for Audit log", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if len(formValues.OutwayPublicKeyThumbprints) < 1 {
		page.renderWithError(w, errors.New("must provide at least one Outway Public Key thumbprint to connect with Service"))

		return
	}

	if formValues.DelegatorPeerID == "" {
		err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
			AuthData:                authData,
			AuthorizationMetadata:   authzData,
			AuditLogSource:          auditLogSource,
			HashAlgorithm:           command.HashAlgSHA3_512,
			GroupID:                 s.basePage.GroupID,
			ContractNotBefore:       formValues.ValidFrom,
			ContractNotAfter:        formValues.ValidUntil,
			ServiceConnectionGrants: createServiceConnectionGrants(selfPeerID, formValues.ServiceDelegatorPeerID, formValues.ServiceProviderPeerID, formValues.ServiceName, formValues.OutwayPublicKeyThumbprints),
		})
		if err != nil {
			s.logger.Error("create contract for connecting to a service", err)

			page.renderWithError(w, err)

			return
		}
	} else {
		err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
			AuthData:                         authData,
			AuthorizationMetadata:            authzData,
			AuditLogSource:                   auditLogSource,
			HashAlgorithm:                    command.HashAlgSHA3_512,
			GroupID:                          s.basePage.GroupID,
			ContractNotBefore:                formValues.ValidFrom,
			ContractNotAfter:                 formValues.ValidUntil,
			DelegatedServiceConnectionGrants: createDelegatedServiceConnectionGrants(selfPeerID, formValues.ServiceDelegatorPeerID, formValues.ServiceProviderPeerID, formValues.ServiceName, formValues.DelegatorPeerID, formValues.OutwayPublicKeyThumbprints),
		})
		if err != nil {
			s.logger.Error("create contract for connecting to a service", err)

			page.renderWithError(w, err)

			return
		}
	}

	page.renderWithSuccess(s.i18n.Translate("The contract for the Service connection has been created"), w)
}

func (s *Server) listPeers(ctx context.Context, authData authentication.Data) (map[string]string, error) {
	peers, err := s.apiApp.Queries.ListPeers.Handle(ctx, &api_query.ListPeersArgs{
		AuthData: authData,
	})
	if err != nil {
		s.logger.Error("could not retrieve PeerID list from manager", err)

		return nil, err
	}

	peerIDList := make(map[string]string)
	for _, peer := range peers {
		peerIDList[peer.ID] = peer.Name
	}

	return peerIDList, nil
}

func createServiceConnectionGrants(outwayPeerID, servicePublicationDelegatorPeerID, serviceProviderPeerID, serviceName string, publicKeyThumbprints []string) []*command.ServiceConnectionGrant {
	grants := make([]*command.ServiceConnectionGrant, len(publicKeyThumbprints))

	for i, thumbprint := range publicKeyThumbprints {
		grants[i] = &command.ServiceConnectionGrant{
			ServicePeerID:                     strings.TrimSpace(serviceProviderPeerID),
			ServicePublicationDelegatorPeerID: strings.TrimSpace(servicePublicationDelegatorPeerID),
			ServiceName:                       serviceName,
			OutwayPeerID:                      strings.TrimSpace(outwayPeerID),
			OutwayPublicKeyThumbprint:         thumbprint,
		}
	}

	return grants
}

func createDelegatedServiceConnectionGrants(outwayPeerID, servicePublicationDelegatorPeerID, serviceProviderPeerID, serviceName, delegatorPeerID string, publicKeyThumbprints []string) []*command.DelegatedServiceConnectionGrant {
	grants := make([]*command.DelegatedServiceConnectionGrant, len(publicKeyThumbprints))

	for i, thumbprint := range publicKeyThumbprints {
		grants[i] = &command.DelegatedServiceConnectionGrant{
			ServicePeerID:                     strings.TrimSpace(serviceProviderPeerID),
			ServicePublicationDelegatorPeerID: strings.TrimSpace(servicePublicationDelegatorPeerID),
			ServiceName:                       serviceName,
			DelegatorPeerID:                   strings.TrimSpace(delegatorPeerID),
			OutwayPeerID:                      strings.TrimSpace(outwayPeerID),
			OutwayPublicKeyThumbprint:         thumbprint,
		}
	}

	return grants
}

func (s *Server) getPublishedServices(ctx context.Context, authzData *authorization.RestMetaData, authData authentication.Data, serviceProviderPeerID string) []string {
	resp, err := s.apiApp.Queries.ListPeerServices.Handle(ctx, &api_query.ListPeerServicesArgs{
		ServiceProviderPeerIDFilter: &serviceProviderPeerID,
		AuthData:                    authData,
		AuthorizationMetadata:       authzData,
		DirectoryPeerID:             "12345678901234567899",
	})
	if err != nil {
		s.logger.Error("could not retrieve published services from Directory", err)

		return nil
	}

	serviceNames := make([]string, len(resp))

	for i, service := range resp {
		switch service := service.(type) {
		case *api_query.DelegatedPeerService:
			serviceNames[i] = service.Name
		case *api_query.PeerService:
			serviceNames[i] = service.Name
		default:
			s.logger.Info(fmt.Sprintf("unknown service type %v", service))
		}
	}

	return serviceNames
}
