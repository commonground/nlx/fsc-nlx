// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"html/template"
	"log"
	"slices"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/ui/i18n"
)

const (
	PathServicesPage                     = "/services"
	PathDirectoryPage                    = "/directory"
	PathContractsPage                    = "/contracts"
	PathActionsPage                      = "/actions"
	PathPeersPage                        = "/peers"
	PathTransactionLogsPage              = "/transaction-logs"
	PathAuditLogsPage                    = "/audit-logs"
	PathIncomingConnectionsPage          = "/incoming-connections"
	PathOutgoingConnectionsPage          = "/outgoing-connections"
	PathDelegatedConnectionsPage         = "/delegated-connections"
	PathServicePublicationsPage          = "/services/publications"
	PathDelegatedServicePublicationsPage = "/delegated-publications"
	PathInwaysOutwaysPage                = "/inways-outways"
)

type BasePage struct {
	staticPath                    string
	svgTemplates                  *template.Template
	assetsMap                     AssetMap
	logger                        *logger.Logger
	I18n                          i18n.I18n
	PathServicesPage              string
	PathDirectoryPage             string
	PathContractsPage             string
	PathActionsPage               string
	PathPeersPage                 string
	PathTransactionLogsPage       string
	PathAuditLogsPage             string
	PathIncomingConnectionsPage   string
	PathOutgoingConnectionsPage   string
	PathDelegatedConnectionsPage  string
	PathPublicationsPage          string
	PathDelegatedPublicationsPage string
	PathInwaysOutwaysPage         string
	GroupID                       string
	Locale                        string
}

func NewBasePage(staticPath string, translations i18n.I18n, groupID string, lgr *logger.Logger, assetMap AssetMap) (*BasePage, error) {
	if staticPath == "" {
		return nil, fmt.Errorf("static path is required")
	}

	svgTemplates := template.Must(template.ParseFS(
		tplFolder,
		"templates/svg/icon-archive-line.svg",
		"templates/svg/icon-arrow-go-back-fill.svg",
		"templates/svg/icon-bar-chart-2-fill.svg",
		"templates/svg/icon-check-fill.svg",
		"templates/svg/icon-eye-line.svg",
		"templates/svg/icon-eye-off-line.svg",
		"templates/svg/icon-search-line.svg",
		"templates/svg/icon-state-active.svg",
		"templates/svg/icon-state-inactive.svg",
		"templates/svg/icon-state-expiring.svg",
		"templates/svg/icon-spinner.svg",
		"templates/svg/icon-information-line.svg",
		"templates/svg/icon-time-line.svg",
		"templates/svg/icon-directory.svg",
		"templates/svg/icon-close-fill.svg",
		"templates/svg/icon-code-s-slash-line.svg",
		"templates/svg/icon-contracts.svg",
		"templates/svg/icon-organization.svg",
		"templates/svg/icon-close.svg",
		"templates/svg/icon-copy.svg",
		"templates/svg/icon-close-circle-line.svg",
		"templates/svg/icon-corner-arrow-right.svg",
		"templates/svg/icon-delete-bin.svg",
		"templates/svg/icon-external-link-line.svg",
		"templates/svg/icon-services.svg",
		"templates/svg/icon-error.svg",
		"templates/svg/icon-hashtag.svg",
		"templates/svg/icon-plus.svg",
		"templates/svg/icon-key-line.svg",
		"templates/svg/icon-chevron-down.svg",
		"templates/svg/icon-chevron-left.svg",
		"templates/svg/icon-chevron-right.svg",
		"templates/svg/icon-settings.svg",
		"templates/svg/icon-shut-down-line.svg",
		"templates/svg/icon-inway.svg",
		"templates/svg/icon-inways-and-outways.svg",
		"templates/svg/icon-link.svg",
		"templates/svg/icon-outway.svg",
		"templates/svg/icon-refresh-line.svg",
		"templates/svg/icon-pen-nib-line.svg",
		"templates/svg/icon-transaction-logs.svg",
		"templates/svg/icon-pencil.svg",
		"templates/svg/icon-exchange.svg",
		"templates/svg/icon-exchange-line.svg",
		"templates/svg/icon-mail-send-line.svg",
		"templates/svg/icon-warning.svg",
		"templates/svg/icon-megaphone.svg",
		"templates/svg/icon-notification-4-fill.svg",
		"templates/svg/icon-download-2-line.svg",
		"templates/svg/icon-shake-hands-line.svg",
		"templates/svg/nlx-logo.svg",
		"templates/svg/fsc-controller-logo.svg",
		"templates/svg/icon-function-line.svg",
	))

	return &BasePage{
		staticPath:                    staticPath,
		assetsMap:                     assetMap,
		svgTemplates:                  svgTemplates,
		logger:                        lgr,
		I18n:                          translations,
		PathServicesPage:              PathServicesPage,
		PathDirectoryPage:             PathDirectoryPage,
		PathContractsPage:             PathContractsPage,
		PathPeersPage:                 PathPeersPage,
		PathTransactionLogsPage:       PathTransactionLogsPage,
		PathAuditLogsPage:             PathAuditLogsPage,
		PathPublicationsPage:          PathServicePublicationsPage,
		PathDelegatedPublicationsPage: PathDelegatedServicePublicationsPage,
		GroupID:                       groupID,
		Locale:                        translations.Locale(),
		PathIncomingConnectionsPage:   PathIncomingConnectionsPage,
		PathOutgoingConnectionsPage:   PathOutgoingConnectionsPage,
		PathDelegatedConnectionsPage:  PathDelegatedConnectionsPage,
		PathActionsPage:               PathActionsPage,
		PathInwaysOutwaysPage:         PathInwaysOutwaysPage,
	}, nil
}

func (b *BasePage) TemplateWithHelpers() *template.Template {
	funcMap := template.FuncMap{
		"svg": func(name, class string) template.HTML {
			svgData := new(strings.Builder)
			err := b.svgTemplates.ExecuteTemplate(svgData, fmt.Sprintf("%s.svg", name), struct {
				Class string
			}{
				Class: class,
			})
			if err != nil {
				log.Printf("unexpected error: %s\n", err)
				return "INVALID SVG NAME PROVIDED"
			}

			// nolint:gosec // we are the owners of the HTML input, so the Cross-site Scripting is not applicable
			return template.HTML(svgData.String())
		},
		"i18n": func(key string, args ...interface{}) template.HTML {
			// nolint:gosec // we are the owners of the HTML input, so the Cross-site Scripting is not applicable
			return template.HTML(b.I18n.Translate(key, args...))
		},
		"asset": func(filePath string) template.HTML {
			result := filePath

			val, ok := b.assetsMap[filePath]
			if ok {
				result = val
			}

			// nolint:gosec // we are the owners of the HTML input, so the Cross-site Scripting is not applicable
			return template.HTML(result)
		},
		"inc": func(input int) template.HTML {
			result := input + 1

			// nolint:gosec // we are the owners of the HTML input, so the Cross-site Scripting is not applicable
			return template.HTML(fmt.Sprintf("%d", result))
		},
		"join": func(input []string) string {
			return strings.Join(input, ", ")
		},
		"contains": func(list []string, element string) bool { // nolint:gocritic // with lambda, we get 'cannot use generic function slices.Contains without instantiation'
			return slices.Contains(list, element)
		},
	}

	return template.
		New("").
		Funcs(funcMap)
}

type AssetMap map[string]string
