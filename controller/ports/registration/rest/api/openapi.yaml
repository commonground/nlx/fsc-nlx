# Copyright © VNG Realisatie 2023
# Licensed under the EUPL

openapi: 3.0.3
info:
  title: Internal Controller API
  version: 1.0.0
  description: |
    Internal Controller REST API

    Can be used to control the Controller and can be used for example to register the Inway and Outway.
servers:
  - url: https://{controllerUrl}:443
    variables:
      controllerUrl:
        default: localhost
        description: URL of the Controller
paths:
  /v1/groups/{group_id}/services/{service_name}:
    get:
      operationId: getService
      summary: Get a service
      description: Returns the service based on group ID and service name
      parameters:
        - $ref: "#/components/parameters/groupID"
        - in: path
          name: service_name
          description: Name of the service
          required: true
          schema:
            $ref: "#/components/schemas/serviceName"
      responses:
        200:
          description: Service
          content:
            application/json:
              schema:
                type: object
                properties:
                  service:
                    $ref: "#/components/schemas/service"
                required:
                  - service
        400:
          $ref: "#/components/responses/400BadRequest"
        401:
          $ref: "#/components/responses/401Unauthorized"
        403:
          $ref: "#/components/responses/403Forbidden"
        404:
          $ref: "#/components/responses/404NotFound"
        408:
          $ref: "#/components/responses/408RequestTimeout"
        413:
          $ref: "#/components/responses/413PayloadTooLarge"
        414:
          $ref: "#/components/responses/414URITooLong"
        422:
          $ref: "#/components/responses/422UnprocessableEntity"
        429:
          $ref: "#/components/responses/429TooManyRequests"
        431:
          $ref: "#/components/responses/431RequestHeaderFieldsTooLarge"
        500:
          $ref: "#/components/responses/500InternalServerError"
        503:
          $ref: "#/components/responses/503ServiceUnavailable"
        505:
          $ref: "#/components/responses/505HTTPVersionNotSupported"
  /v1/groups/{group_id}/inways/{inway_name}:
    put:
      operationId: registerInway
      summary: Register an Inway
      description: Registers a new Inway, if the Inway already exists, it will update the information
      parameters:
        - $ref: "#/components/parameters/groupID"
        - in: path
          name: inway_name
          description: Name of the Inway
          required: true
          schema:
            type: string
            example: Inway-1
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                address:
                  $ref: "#/components/schemas/inwayAddress"
              required:
                - address
      responses:
        204:
          description: Inway registered
        400:
          $ref: "#/components/responses/400BadRequest"
        401:
          $ref: "#/components/responses/401Unauthorized"
        403:
          $ref: "#/components/responses/403Forbidden"
        404:
          $ref: "#/components/responses/404NotFound"
        408:
          $ref: "#/components/responses/408RequestTimeout"
        413:
          $ref: "#/components/responses/413PayloadTooLarge"
        414:
          $ref: "#/components/responses/414URITooLong"
        422:
          $ref: "#/components/responses/422UnprocessableEntity"
        429:
          $ref: "#/components/responses/429TooManyRequests"
        431:
          $ref: "#/components/responses/431RequestHeaderFieldsTooLarge"
        500:
          $ref: "#/components/responses/500InternalServerError"
        503:
          $ref: "#/components/responses/503ServiceUnavailable"
        505:
          $ref: "#/components/responses/505HTTPVersionNotSupported"
  /v1/groups/{group_id}/outways/{outway_name}:
    put:
      operationId: registerOutway
      summary: Register an Outway
      description: Registers a new Outway, if the Outway already exists, it will update the information
      parameters:
        - $ref: "#/components/parameters/groupID"
        - in: path
          name: outway_name
          description: Name of the Outway
          required: true
          schema:
            type: string
            example: Outway-1
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                public_key_thumbprint:
                  description: Thumbprint of the Public key used in the Certificate the Outway is using
                  $ref: "#/components/schemas/publicKeyThumbprint"
              required:
                - public_key_thumbprint
      responses:
        204:
          description: Inway registered
        400:
          $ref: "#/components/responses/400BadRequest"
        401:
          $ref: "#/components/responses/401Unauthorized"
        403:
          $ref: "#/components/responses/403Forbidden"
        404:
          $ref: "#/components/responses/404NotFound"
        408:
          $ref: "#/components/responses/408RequestTimeout"
        413:
          $ref: "#/components/responses/413PayloadTooLarge"
        414:
          $ref: "#/components/responses/414URITooLong"
        422:
          $ref: "#/components/responses/422UnprocessableEntity"
        429:
          $ref: "#/components/responses/429TooManyRequests"
        431:
          $ref: "#/components/responses/431RequestHeaderFieldsTooLarge"
        500:
          $ref: "#/components/responses/500InternalServerError"
        503:
          $ref: "#/components/responses/503ServiceUnavailable"
        505:
          $ref: "#/components/responses/505HTTPVersionNotSupported"
  /v1/health:
    get:
      operationId: getHealth
      summary: health endpoint for the Controller Registration API
      responses:
        200:
          description: Controller Registration API is healthy
        503:
          description: Controller Registration API is unhealthy
components:
  parameters:
    groupID:
      in: path
      name: group_id
      description: Group ID
      required: true
      schema:
        $ref: "#/components/schemas/groupID"
  schemas:
    error:
      description: Error object according to [RFC 9457](https://www.rfc-editor.org/rfc/rfc9457.html)
      properties:
        type:
          type: string
          description: URI reference that identifies the problem type.
          example:
        status:
          type: integer
          description: HTTP status code generated by the origin server for this occurrence of the problem.
          minimum: 100
          maximum: 599
        title:
          type: string
          description: Short, human-readable summary of the problem type.
        details:
          type: string
          description: >
            Human-readable explanation specific to this occurrence of the problem.

            The "detail" string, if present, ought to focus on helping the client correct the problem, rather than giving debugging information.
        instance:
          type: string
          description: URI reference that identifies the specific occurrence of the problem.
      required:
        - type
        - status
        - title
        - details
        - instance
    serviceName:
      description: The name of a service
      type: string
      example: random_service_name
      minLength: 3
      maxLength: 255
    inwayAddress:
      type: string
      description: Address of the inway
      example: "https://inway-1.example.com:443"
    publicKeyThumbprint:
      description: The thumbprint of the public key used in the certificate from the Outway encoded in Hex format
      type: string
      example: 7fbffc722ca236277fd1ddd61a2b62aebe04c2b7d8000a2ae1af99da3702e081
      maxLength: 64
    groupID:
      type: string
      description: The FSC Group ID (Directory URI)
      example: fsc-local
    service:
      type: object
      properties:
        group_id:
          $ref: "#/components/schemas/groupID"
        name:
          $ref: "#/components/schemas/serviceName"
        inway_address:
          $ref: "#/components/schemas/inwayAddress"
        endpoint_url:
          type: string
          description: An URL of the service endpoint
          example: http://internal-api:80
      required:
        - group_id
        - name
        - inway_address
        - endpoint_url
  responses:
    400BadRequest:
      description: Bad Request, (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing).
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    401Unauthorized:
      description: Unauthorized, the user does not have valid authentication credentials for the target resource.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    403Forbidden:
      description: Forbidden, the user does not have the permission to execute this request
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    404NotFound:
      description: Not Found, the requested resource could not be found.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    405MethodNotAllowed:
      description: Method Not Allowed, the request method is not supported for the requested resource
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    408RequestTimeout:
      description: Request Timeout, the client did not produce a request within the time that the server was prepared to wait.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    413PayloadTooLarge:
      description: Payload Too Large, the request is larger than the server is willing or able to process.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    414URITooLong:
      description: URI Too Long, the URI provided was too long for the server to process.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    422UnprocessableEntity:
      description: Unprocessable Entity, the request was well-formed but was unable to be followed due to semantic errors.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    429TooManyRequests:
      description: Too Many Requests, the user has sent too many requests in a given amount of time.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    431RequestHeaderFieldsTooLarge:
      description: Request Header Fields Too Large, the server is unwilling to process the request because either an individual header field, or all the header fields collectively, are too large.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    500InternalServerError:
      description: Internal Server Error, a generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    503ServiceUnavailable:
      description: Service Unavailable, the server cannot handle the request (because it is overloaded or down for maintenance). Generally, this is a temporary state.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    505HTTPVersionNotSupported:
      description: HTTP Version Not Supported, the server does not support the HTTP version used in the request.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
