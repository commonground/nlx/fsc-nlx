// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"

	api "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/server"
)

func (s *Server) GetHealth(ctx context.Context, _ api.GetHealthRequestObject) (api.GetHealthResponseObject, error) {
	err := s.app.Queries.GetHealth.Handle(ctx)
	if err != nil {
		s.logger.Error("health check failed", err)
		return api.GetHealth503Response{}, nil
	}

	return api.GetHealth200Response{}, nil
}
