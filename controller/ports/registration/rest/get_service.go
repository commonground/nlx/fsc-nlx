// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/server"
)

func (s *Server) GetService(ctx context.Context, req api.GetServiceRequestObject) (api.GetServiceResponseObject, error) {
	s.logger.Info("rest request GetServiceConnectionInfo")

	service, err := s.app.Queries.GetService.Handle(ctx, req.GroupId, req.ServiceName)
	if err != nil {
		s.logger.Error("error executing get service query", err)
		return nil, err
	}

	return api.GetService200JSONResponse{
		Service: models.Service{
			GroupId:      service.GroupID,
			Name:         service.Name,
			InwayAddress: service.InwayAddress,
			EndpointUrl:  service.EndpointURL.String(),
		},
	}, nil
}
