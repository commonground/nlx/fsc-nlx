# Release strategy

This document describes the various aspects of releases. From versioning schemes, to release cadence and lifecycle.

## Versioning

The FSC releases follow the [SemVer](https://semver.org/) scheme and have the structure MAJOR.MINOR.PATCH. We increment:

* MAJOR, when we make a breaking change,
* MINOR, when we add a new functionality in a backwards compatible manner, and
* PATCH, when we make backwards compatible bug fixes.

Once a release is created, it will be announced on Slack and Mattermost. All releases contain a [Changelog](CHANGELOG.md) describing the details of changes in this release.

## Backwards compatability and support of older versions

As aligned with SemVer, backwards incompatible changes are released in a new MAJOR version.
There is one thing to note, the FSC NLX is a reference implementation of the FSC standard.

However, the reference implementation implements more than is described in the standard. This is needed to create a working and user-friendly system based on the FSC standard.
As a result, it is possible a backwards incompatible change is introduced in the FSC reference implementation that is not present in the FSC standard.

When this occurs a new MAJOR version is released of the FSC reference implementation whilst the FSC standard remains on the same version.

### Data migrations

FSC performs database migrations out of the box. During development, considerable effort is made to ensure the database can be automatically migrated.

If (exceptionally) an automatic data migration is not possible, the change will be released as a new MAJOR version.
The manual for performing the data migration will be issues alongside with the release notes.

## Release cadence

The FSC team releases a new version roughly every two weeks, after the Sprint Review.
To deliver security updates, fix known issues and introduce new features.

A new major version will be released at max once every six months.

This means that all MAJOR releases of FSC are the most current MAJOR version for ***at least***  six months.

It also means that if an organisation wants to keep up to date with the latest fixes and security updates,
they should update their FSC installation every two weeks.

## Support of older versions

The FSC team will guarantee critical bugs will be fixed for a total of 12 months.
However, these will only be backported to the latest minor release of the previous major version.

For example:
* current latest version 2.3.0
* previous Major version 1.5.2

When a critical bug is solved the following new versions are released:
* current latest version 2.3.1
* previous Major version 1.5.3

`Clients who are on a version < 1.5.2 will NOT receive the bugfix and will have to upgrade to either 1.5.3 or 2.3.1`
`If version 1.5.2 was released more then 12 months ago as of time of the bugfix the bugfix will NOT be backported and clients will need to upgrade to version 2.3.1`

## Future releases

Once a new major version is tagged, future work will be available as a Release Candidate once the features are stable enough to be tested. (e.g. if the current major release is v1.0, the next Release Candidate will be tagged as v2.0.0-rc.1, v2.0.0-rc.2, etc.).
A new version of a Release Candidate may be backwards incompatible with the previous version of the Release Candidate. E.g. v2.0.0-rc2 may not be compatible with v2.0.0-rc1
Release Candidates should be deployed as Review Apps. For the technical implementation details of this flow, see https://semantic-release.gitbook.io/semantic-release/recipes/release-workflow/pre-releases

The Acceptance environment contains the main branch containing the latest work.
The Demo environment contains the latest release which is not an RC

The demo environments can be found here:

* [ACC](https://links.acc.fsc.nlx.io/)
* [Demo](https://links.demo.fsc.nlx.io/)
