#!/usr/bin/env bash
# Copyright © VNG Realisatie 2022
# Licensed under the EUPL

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

PKIS="external internal/directory internal/organization-a internal/organization-b internal/organization-c/ internal/organization-d"

for PKI in ${PKIS}; do
  AMOUNT_OF_CAS=1

  if [[ ${PKI} == "external" ]]; then
    AMOUNT_OF_CAS=2
  fi

  PKI_DIR="${BASE_DIR}/${PKI}"
  CONFIG="${PKI_DIR}/config.json"
  CA_DIR="${PKI_DIR}/ca"

  rm "${CA_DIR}/root.pem"
  touch "${CA_DIR}/root.pem"

  for NUMBER in $(seq 1 $AMOUNT_OF_CAS); do
    cfssl genkey \
      -config "${CONFIG}" \
      -initca \
      "${CA_DIR}/root-${NUMBER}.json" \
    | cfssljson -bare "${CA_DIR}/root-${NUMBER}"

    cfssl genkey \
      -config "${CONFIG}" \
      -initca \
      "${CA_DIR}/intermediate-${NUMBER}.json" \
    | cfssljson -bare "${CA_DIR}/intermediate-${NUMBER}"

    cfssl sign \
      -config "${CONFIG}" \
      -ca "${CA_DIR}/root-${NUMBER}.pem" \
      -ca-key "${CA_DIR}/root-${NUMBER}-key.pem" \
      -profile intermediate \
      "${CA_DIR}/intermediate-${NUMBER}.csr" \
    | cfssljson -bare "${CA_DIR}/intermediate-${NUMBER}"

    rm "${CA_DIR}/root-${NUMBER}.csr"
    rm "${CA_DIR}/intermediate-${NUMBER}.csr"

    cat "${CA_DIR}/root-${NUMBER}.pem" >> "${CA_DIR}/root.pem"
  done

done
