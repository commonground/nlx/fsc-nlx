# Contributing

We – [VNG realisatie](https://www.vngrealisatie.nl/) and the maintainers of this project – know we can only build FSC-NLX together with you. Thus, we appreciate your input, enjoy feedback and welcome improvements to this project and are very open to collaboration.

We love issues and merge requests from everyone.

## 1. Problems, suggestions and questions in Issues

You don't need to change any of our code or documentation to be a contributor. Please help development by reporting problems, suggesting changes and asking questions. To do this, you can [create an issue using GitLab](https://docs.gitlab.com/ee/user/project/issues/create_issues.html) for this project in the [GitLab Issues for FSC-NLX](https://gitlab.com/commonground/nlx/fsc-nlx/-/issues/).

## 2. Documentation and code in Merge Requests

If you want to add to the documentation or code of one of our projects you should push a branch and make a Merge Request. If you have never used GitLab before, get up to speed by reading about the [GitLab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/).

### 2.1. Make your changes

#### 2.1.1. branching
This project uses short-lived feature branches for code changes.
Branches must reference a Gitlab Issue, first submit a Gitlab issue and assign it to your name before working on it.
FSC-NLX used the following branch naming convention:
* [gitlab issue number]-short-description
* review/[gitlab issue number]-short-description

Branches prefixed with `review/` will be deployed on a separate test environment where all automated e2e tests are run.
Also, this review environment will be available for manual testing or demonstration.

As a general rule of thumb, use a `review/` branch when:
* The e2e tests need to executed (for example when these are changes)
* A change in the UI has been made that needs manual verification/demonstration

Examples of branch names:
* `review/123-fix-pagination-bug-contracts`
* `432-add-foo-query-param-internal-manager`

#### 2.1.2. Add docs and tests
If you are adding code, make sure you've added and updated the relevant documentation and tests before you submit your Merge Request. Make sure to write unit tests that show the behaviour of the newly added or changed code.

#### 2.1.3 Signing your commits
It is highly encouraged to sign your commits with your GPG or SSH key that is also added to your GitLab account. To configure Git on your device to sign commits, you can follow the [GitLab documentation](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/) about GPG signed commits.
Or [Gitlab documentation SSH signed commits](https://docs.gitlab.com/ee/user/project/repository/signed_commits/ssh.html) for instructions on how to use SSH keys to sign your commits.

### 2.2. Commit messages

#### 2.2.1. Explain your contributions
Add your changes in commits [with a message that explains them](https://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message). Document choices or decisions you make in the commit message, this will enable everyone to be informed of your choices in the future.

#### 2.2.2. Semantic Release
This project uses [semantic-release](https://semantic-release.gitbook.io/semantic-release/). When merging a MR to main, this will automatically generate our [CHANGELOG](./CHANGELOG.md) based on the commit messages and a version tag will be added.

#### 2.2.3. Conventions for commit messages
We follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification. This is enforced with a linter in the build pipeline. This convention requires you to pas a type and an optional scope as the commit message. The scope is based on the applications in the repository. If you are not sure which scope to use please leave the scope empty.

The type must be one of the following:

- **build**: Changes that affect the build system or external dependencies
- **ci**: Changes to our CI configuration files and scripts
- **docs**: Documentation only changes
- **feat**: A new feature
- **fix**: A bug fix
- **perf**: A code change that improves performance
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **revert**: Changes that revert other changes
- **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semicolons, etc.)
- **test**: Adding missing tests or correcting existing tests

The available scopes are listed in `.commitlintrc.yaml`.

All Commit messages must also reference the Gitlab Issue in the body of the commit message in the following format:
`Issue: fsc-nlx#[issue-nr]` for example: `Issue: fsc-nlx#123` where `123` is a Gitlab issue in the FSC-NLX repository.

### 2.3. Merge Request

#### 2.3.1. Always refer to an issue
Before starting a Merge Request, make sure there is a User Story describing what you want to achieve with the MR. [Create a story by submitting a new issue](https://gitlab.com/commonground/nlx/fsc-nlx/issues) if there is none. New issues come with a User Story template. This template helps you think from the user perspective: 'who wants this new feature and why?'

#### 2.3.2. Describe the MR

When submitting the Merge Request, please accompany it with a short description and eventually some clarifying remarks for the reviewer. Make sure the related issues are linked to the MR.

#### 2.3.3. Combine frontend and backend work in one MR
When working on a feature which requires specific capabilities of multiple developers (eg. both specialist frontend & backend work), work as a team to make sure the MR contains a full feature instead of separate MR's per developer. By doing so, the reviewer can consider the complete solution and give more insightful feedback.

### 2.4. Improve

#### 2.4.1. Reviews
All contributions have to be reviewed. The reviewer will look at the submitted code to assure quality. The reviewer will consider at least the following:
* Does the MR meet the intended acceptance criteria?
* Are there any typos?
* Does the code meet the teams overall quality standards?
* Are there logical errors in the code?
* Is the chosen technical solution reasonably efficient?
* Are all the necessary (unit) tests added?

#### 2.4.2 Releases
The release strategy used for FSC-NLX is documented in a separate [RELEASE_STRATEGY.md](RELEASE_STRATEGY.md).

### 2.5. Celebrate

Your ideas, documentation and code have become an integral part of this project. You are the Open Source hero we need.

## 3. Development process

The part below is meant as documentation for the team developing and maintaining FSC-NLX. It is public to give more insight in the development process.

### 3.1. Agile scrum

The FSC-NLX Team uses the Agile Scrum framework for product development. It encourages us to learn through experiences, self-organise while working on a problem, and reflect on our wins and losses to continuously improve.

#### 3.1.1. Sprints

The development process is structured in sprints:

* A sprint starts on Wednesdays and lasts for two weeks.
* We plan most scrum rituals on that Wednesday:
  * *Sprint review* - review sprint results and adapt backlog
  * *Retrospective* - improving the team with every sprint
  * *Refinement* - finishing touch getting user stories clear, including scrum poker
  * *Sprint planning* - starting the new sprint with a sprint goal
* The other Wednesday, half-way sprint, we spent some time refining user stories as well.

#### 3.1.2. Scrum boards

To keep track of the work, we use the [issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html) of Gitlab:

* [Sprint board](https://gitlab.com/groups/commonground/nlx/-/boards/1860837?milestone_title=Started), which guides us through the sprint:
  * This board contains the sprint backlog, the commited estimation and scope of the Sprint Goal, as planned during the sprint planning.
  * How stories flow through this board is more thoroughly described in [3.2 Development flow](#3-2-development-flow).
  * Column "Open" contains user stories that are waiting to be worked on.
  * Column "Doing" shows work that someone is working on. All those stories have someone assigned.
  * Column "Review" contains user stories that can be reviewed by another team member. This includes a code review, test of functionality and in some cases a Project Owner review. We add just user stories to the board, they are linked to the involved Merge Requests.
  * Column "Closed" means the user story has been completed.

There are some supporting repositories surrounding the main FSC-NLX repository. Those do not contain issues nor project boards.

#### 3.1.3. Definition of Done

With MR's we make User Stories become reality.
This section describes the Definition of Done expressed in the stages a story goes through from cradle to cradle.

**1. Definition of Ready**<BR>
*Responsible: PO/DEV*<BR>
- A story passes the INVEST criteria:<BR>
  “I” ndependent (of all others)<BR>
  “N” egotiable (not a specific contract for features)<BR>
  “V” aluable (or vertical)<BR>
  “E” stimatable (to a good approximation)<BR>
  “S” mall (to fit within an iteration)<BR>
  “T” estable (in principle, even if there isn’t a test for it yet)
  - Test scenarios are written down
  - acceptance criteria are written down in a way no misinterpretations are possible
- We describe the story with the end in mind. So part of the story is the way we are going to demonstrate it to the stakeholder
- We describe the proposed solution which has been discussed during refinement

**2. From Doing to Review**<BR>
*Responsible: DEV*<BR>
- Code builds
  - linting errors are fixed
  - security issues are fixed
- Tests are added
- The developer has checked all acceptance criteria and test scenarios first
- The developer is responsible for arranging the review of the story
  - The developer will notify the team who reviews his story
- Developer moves story to review

**3. From Review to Closed**<BR>
*Responsible: DEV*<BR>
- Test are in place and understood
- Solution has been checked and discussed with responsible developer
- Reviewer and developer agree upon the solution
- Reviewer marks the story as being reviewed
- Developer notifies PO that his story is ready to accept
- PO checks all test scenarios and acceptance criteria
- PO marks the story as being accepted
- Developer merges the code to main
- Developer moves story to closed

### 3.2 Development flow

The development flow describes how we bring user stories from idea to production.

#### 3.2.1. Overview

Development follows this flow:

1. Add to backlog
1. Select, refine, estimate and plan
1. Code
1. Review
1. Acceptance by Product Owner
1. Merge to main branch
1. Automated deploy to acceptance environment
1. Generate version tag (manual trigger)
1. Deploy release to demo (manual trigger)

Issues can be created by anyone and start at the backlog.

#### 3.2.2. Select, refine, estimate and plan

The Product Owner selects user stories from the backlog and refines them, when necessary with the team. Then the team estimates how complex it is to fulfil a story, adding the complexity points. During the sprint planning, the Product Owner and team plan stories to work towards a single sprint goal to be completed during the coming sprint. The scope of the sprint goal is negotiated by selecting a set of issues that together complete the sprint goal. Full focus and commitment goes to completing the sprint goal. During daily standups, progress is discussed.

When the sprint has started, developers select the issue that they will work from the sprint board by assigning it to themselves and by moving it from column "Open" to "Doing".

#### 3.2.3. Code

Every team member develops on a local copy of the repository. New features are added on a feature branch. During this stage, local builds and tests are made. Once ready, the feature branch is pushed to the repository hosted by Gitlab.

With the feature branch now available on Gitlab, a Merge Request is created. This is always a request to merge the new or altered code into the default branch.

Merge Requests to the main branch trigger a CI pipeline to perform unit tests and to build all containers. If this fails, the developer will continue coding and pushing to the feature branch on Gitlab until the pipeline passes.

By starting the title of an MR with `WIP:`, one can indicate "Work in Progress". Gitlab will prevent merging work that is marked `WIP:`. This is useful to trigger a pipeline or to start a discussion.


#### 3.2.4. Review

Once ready, the developer asks for a review of the work by moving the related issue on the Sprint board from column "Doing" to "Review". It is also useful to ask for the Review by announcing it on Slack.

One or more other developers will perform a code review, commenting and discussing until everything is clear. If this results in necessary changes, the issue moves back to the column "Open" (when other work was started meanwhile) or "Doing" (when work continues) until the issue is ready for review again.

The review makes sure that all code is seen by multiple people. This prevents all sort of mistakes, makes sure knowledge is shared throughout the team and makes sure more people feel responsible about the code.

Once a reviewer is satisfied he or she will approve the Merge Request. At lease one approval is required to continue. After approval the
Product Owner accepts the story by also approving the Merge Request. The developer is responsible for merging the Merge Request to main.

Small merge requests that do not change the behaviour of the software itself (e.g. dependency updates) do not have to be accepted by the Product Owner. They can be merged after code review.

The required approval from the Product Owner can be removed by editing the Merge Request and setting the number of approvals required for PO Accept to 0.

While an issue is in 'Review', it remains assigned to the developer who is working on the issue (not the reviewer), and it is the responsibility of this assignee to make sure a timely and complete review of the proposed changes.

As we want to deliver value to the customer as soon as possible, stories shouldn't stay in Review too long. The author of the story should actively reach out to the team members to get the work reviewed.

When a branch begins with `review/` a review app is created for that branch. This app can be used to inspect UI changes of the Merge Request.


#### 3.2.5. Deploy to acceptance environment

A successful merge triggers the pipeline. After testing, it releases the build containers and automatically deploys to the acceptance environment.

After deployment to the acceptance environment, the newly deployed features are checked online by the developer. If everything still works and the new features perform as intended, the issue is moved to the 'Closed' column. If not, bug fixing is in order.


#### 3.2.6. Version tag

A new version is tagged by running the manual Semantic Release job on the main branch. First a 'dry run' job is triggered manually to check the new version number and the generated changelog.

When the output is as expected, the real release job is triggered. The job updates the [CHANGELOG.md](CHANGELOG.md), commits the changes and adds a new tag to the commit with the new version number.


#### 3.2.7. Deploy to production environment and release Docker images

A new production release is done by triggering the manual release job on the main branch. The release is deployed to the following environments:

* Demo
* Pre-production
* Production

After deployment, a manual check is done to check if everything still works as intended.

#### 3.2.8. Version skew policy

For the internal interfaces of the FSC-NLX system we have a version skew policy for MAJOR/MINOR releases of `n-2`. So for example a component of version 1.5 is able to communicate with a component of version 1.3 and a component of version 2.0 is able to communicate with the last two minor versions of the 1.x series.

### 3.3. Communication

#### 3.3.1. Gitlab
  * User story related communication discussion is mostly done in the comments below user stories
  * Reviews are done as comments below Merge Requests
  * All communication on Gitlab is written in English

#### 3.3.2. Slack
  * General communication from chit-chat to important notifications is done via Slack
  * Alerts generated from operations are communicated via Slack
  * The Slack workspace is reserved for the team
  * Slack is high traffic but topics strictly separated in channels
  * Since the team does not work in one location every day, we organise our stand-ups via video calls in Slack.
  * Stand-ups last 15 - 20 minutes. We focus on sharing what every did and what will be done that day, with the occasional exchange about impediments
  * In case the Sprint Backlog needs to be renegotiated (i.e. the scope of the sprint goal is changed), this is done during the standup
  * Slack is used for one-on-one communication between team members as well

#### 3.3.3 MS Teams
  * every two weeks the team performs a Sprint review for all interested stakeholders open for all on [MS Teams](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjViYTM0YjEtMDZhOS00YzI3LTliZmYtYzMzZTNhZTc0YWZm%40thread.v2/0?context=%7b%22Tid%22%3a%226ef029ab-3fd7-4d98-9b0e-d1f5fedea6d1%22%2c%22Oid%22%3a%222792491b-bf61-4388-a055-8272a0083768%22%7d).
  * Every week when there is no Sprint review there is an open consultation hour, where everyone interested can ask questions to the FSC team offered on the same MS Teams channel.

## 4. Miscellaneous

### 4.1. Merge Requests
  * We try not to merge any MRs the day before the sprint review (we agreed upon this during the retrospective of March 3rd 2021).

---

For more information on how to use and contribute to this project, please read the [`README`](README.md).
