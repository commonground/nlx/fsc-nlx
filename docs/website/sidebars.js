/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Introduction',
      collapsible: true,
      collapsed: false,
      items: [
        'introduction/introduction',
        'introduction/functional-description',
      ],
    },
    {
      type: 'category',
      label: 'Try FSC',
      collapsible: true,
      collapsed: true,
      items: [
        {
          type: 'category',
          label: 'Docker Compose',
          collapsible: true,
          collapsed: true,
          items: [
            'try-fsc/docker/introduction',
            'try-fsc/docker/setup-your-environment',
            'try-fsc/docker/retrieve-a-demo-certificate',
            'try-fsc/docker/getting-up-and-running',
            'try-fsc/docker/provide-an-api',
          ],
        },
        {
          type: 'category',
          label: 'Helm',
          collapsible: true,
          collapsed: true,
          items: [
            'try-fsc/helm/introduction',
            'try-fsc/helm/0-preparation',
            'try-fsc/helm/1-create-namespace',
            'try-fsc/helm/2-create-certificate',
            'try-fsc/helm/3-postgresql',
            'try-fsc/helm/4-transaction-log',
            'try-fsc/helm/5-auditlog',
            'try-fsc/helm/6-controller',
            'try-fsc/helm/7-manager',
            'try-fsc/helm/8-inway',
            'try-fsc/helm/9-outway',
            'try-fsc/helm/10-sample-api',
            'try-fsc/helm/11-access-api',
            'try-fsc/helm/finish',
          ],
        },
      ],
    },
    {
      type: 'category',
      label: 'FSC NLX in production',
      collapsible: true,
      collapsed: true,
      items: [
        'fsc-in-production/deployment-strategies',
        'fsc-in-production/system-requirements',
        'fsc-in-production/setup-authorization',
        'fsc-in-production/environments',
        'fsc-in-production/hosting-a-directory',
        'fsc-in-production/setup-oidc',
        'fsc-in-production/renew-certificates',
      ],
    },
    {
      type: 'category',
      label: 'Support',
      collapsible: true,
      collapsed: true,
      items: [
        'support/contact',
        'support/release-strategy',
      ],
    },
  ],
};
