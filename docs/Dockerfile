# Copyright © VNG Realisatie 2022
# Licensed under the EUPL

FROM node:20.18.0-alpine@sha256:c13b26e7e602ef2f1074aef304ce6e9b7dd284c419b35d89fcf3cc8e44a8def9 AS build

ENV CI=true

# First copy only package.json and yarn.lock to make the dependency fetching step optional.
COPY website/package.json \
    website/package-lock.json \
    /go/src/gitlab.com/commonground/nlx/fsc-nlx/docs/website/

WORKDIR /go/src/gitlab.com/commonground/nlx/fsc-nlx/docs/website
RUN npm ci --no-progress --color=false --quiet

# Now copy the whole workdir for the build step.
COPY . /go/src/gitlab.com/commonground/nlx/fsc-nlx/docs

RUN npm run build

RUN mkdir -p /tmpdir/client_temp \
    /tmpdir/fastcgi_temp \
    /tmpdir/proxy_temp \
    /tmpdir/scgi_temp \
    /tmpdir/uwsgi_temp && \
    touch /tmpdir/nginx.pid && \
    chmod 777 /tmpdir/nginx.pid

# Create version.json
FROM alpine:3.20.3@sha256:beefdbd8a1da6d2915566fde36db9db0b524eb737fc57cd1367effd16dc0d06d AS version

RUN apk add --update jq

ARG GIT_TAG_NAME=undefined
ARG GIT_COMMIT_HASH=undefined
RUN jq -ncM --arg tag $GIT_TAG_NAME --arg commit $GIT_COMMIT_HASH  '{tag: $tag, commit: $commit}' | tee /version.json

# Copy static docs to non privileged nginx scratch container.
FROM ricardbejarano/nginx:1.27.2@sha256:e044b6ee2bd6dd17e8bfd958f7b77e7b0dc2338672ff6e785e8667c980bd1a4d

COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/mime.types /etc/nginx/mime.types

COPY --from=build /go/src/gitlab.com/commonground/nlx/fsc-nlx/docs/website/build /usr/share/nginx/html
COPY --from=build --chown=10000:10000 /tmpdir /tmp

COPY --from=version /version.json /usr/share/nginx/html/
