---
id: provide-an-api
title: Provide an API
---

## Introduction

To provide an API to the FSC Group, you need to route traffic through the **Inway**.
To make sure traffic is encrypted between your and other nodes, we will use the certificate which we've setup in [Retrieve a demo certificate ](./retrieve-a-demo-certificate).

Please note that:

* You need a domain name to provide an Inway (an IP address will not work).
* You need a domain name to provide a Manager (an IP address will not work).
* The domain should be the same as the domain you used to generate the certificates (that was in [Retrieve a demo certificate](./retrieve-a-demo-certificate)).

It is not recommended to follow this guide when you're working from your home network.

> In the Docker Compose file we have started, the Inway is running on the port
`443` and the Manager on `8443`. Make sure that both ports are publicly available.

## Verification

Assuming you followed [Getting up and running](./getting-up-and-running), the Inway and Manager should already be up and running.

You can confirm that by inspecting the Controller logs:

```shell
docker logs nlx-try-me-controller
```

The output should look similar to:

```json
{"level":"INFO","time":"2023-08-12T11:07:39.765Z","caller":"zap/logger.go:24","message":"version info: version: v0.1.0 source-hash: undefined"}
```

Next, let's take a look at the logs of our Inway:

```shell
docker logs nlx-try-me-inway
```

The output should look similar to:

```json
{"level":"INFO","time":"2021-01-06T14:12:23.905Z","caller":"nlx-inway/main.go:156","message":"version info","version":"v0.92.0","source-hash":"5d5a8afecb1e504d6ea5c865d839720d47fedb24"}
```

## Create a service

In the following example we will use [Swagger Petstore](https://petstore.swagger.io) as an example API.

To provide our API in the FSC Group we have to create a service in the Controller UI.
You can do that by going to the services page where you click on the 'Add service' button.

For the service name, use `SwaggerPetStore` and for the API endpoint URL use `https://petstore.swagger.io/v2`.
Select `https://<your-inway-domain>:443` as the Inway to be used by this service.

![Add service screen](/img/nlx-controller-add-service-screen.png "Add service screen")

Whenever you're ready, click 'Service toevoegen' to save the details.

> If you're specifying you own API, please note that `localhost` will not work. If your API is
> running on the same machine as your Inway, the endpoint URL should be your network IP and not `localhost`.

## Publishing the service

The next step is to create a Contract with a *Service Publication Grant*. This is needed to get the Service published in the Directory.

Open the Contracts page and click the `Add Contract` button.

Next, add a *Service Publication Grant* by clicking on the `Add` button below the ServicePublicationGrant section.

Use the following information for the form fields:

Directory Peer ID: `12345678901234567899`
Service Peer ID: `<your-peer-id>` (visible in the top right corner of the Contracts overview page)
Service Name: `SwaggerPetStore`

Submit the form by clicking 'Add contract'. This will send the Contract to the Directory. The Directory will sign it automatically.
Navigate to the Directory page, the Service SwaggerPetStore should be visible.

## Querying your own APIs

Now let's try to fetch some data from our Inway using our Outway using `curl`:

```shell
curl http://localhost:8081
```

The response of the `curl` command should look similar to the following output.

```json
{"message":"unknown grant hash: \"\" in Fsc-Grant-Hash header","source":"outway","location":"C1","code":"UNKNOWN_GRANT_HASH_IN_HEADER","metadata":{"services":[]}}%
```

The Outway produced an error because it needs a reference to the Grant in the Contract containing the authorization to use our Service.
This reference is called a Grant hash, and we can obtain one by creating a Contract.

Requesting access via the Controller interface:

1. Navigate to the Contracts page
2. Click the 'Add Contract' button
3. Click the 'Add' button for the ServiceConnectionGrant section
   Directory Peer ID: `12345678901234567899`
   Service Peer ID: `<your-peer-id>` (visible in the top right corner of the Contracts overview page)
   Service Name: `SwaggerPetStore`
   Outway Peer ID: `<your-peer-id>`
4. Confirm that field *Outway Certificate Thumbprint* is prefilled with a thumbprint
5. Submit the form by clicking 'Add contract'

Navigate to the Contracts overview page. A Contract with the Grant type `Service Connection`
for the Service `SwaggerPetStore` should be visible. Normally the Peer hosting the Service must
also sign the Contract but since we are both providing and consuming the Service this is not necessary.

Let's call our Outway again.

```shell
curl localhost:8081
```

The response should look similar to:

```json
{"message":"unknown grant hash: \"\" in Fsc-Grant-Hash header","source":"outway","location":"C1","code":"UNKNOWN_GRANT_HASH_IN_HEADER","metadata":{"services":[{"GrantHash":"$1$4$hdxN9_CV2iFeSA7O_eOn4rgLmiIcZimspK16-VYHGC_ogCTuO6xAC_hv-dA09QS0qKfKEMsFLXjs10LJHosZUQ==","Name":"basisregister-fictieve-kentekens","PeerID":"01691582300541934913"}]}}%
```

The Outway still produces an error telling us it did not receive a Grant hash.
Since we created the Contract already, the available Grant hash is listed in the response.

In the above example that's `$1$4$hdxN9_CV2iFeSA7O_eOn4rgLmiIcZimspK16-VYHGC_ogCTuO6xAC_hv-dA09QS0qKfKEMsFLXjs10LJHosZUQ==`.

Let's call our Outway again with the Grant hash. We can do so by providing the Grant hash
using the HTTP header `Fsc-Grant-Hash`. Make sure to replace `<grant-hash>` with the Grant hash outputted
by the previous command.

```shell
curl -H 'Fsc-Grant-Hash: <grant-hash>' localhost:8081
```

After running the command you should see the following result:

```json
{
  "args": {},
  "headers": {
    "x-forwarded-proto": "https",
    "host": "petstore.swagger.io",
    "accept": "*/*",
    "accept-encoding": "gzip",
    "user-agent": "curl/7.54.0",
    "Fsc-Transaction-Id": "<arbitrary-transaction-id>",
    "x-nlx-request-organization": "00000000000000000001",
    "x-forwarded-port": "443"
  },
  "url": "https://petstore.swagger.io/v2/pet/20002085"
}
```

Congratulations, you have successfully provided an API on the FSC Group! 🎉

## Transaction log

The Inway and the Outway are able to log metadata of the requests they process in the transaction log. You can view the transaction log in the Controller.
Open a browser, navigate to http://localhost:8080/transaction-logs. You should now see two entries for the request you just made (outgoing & incoming log)

## Audit log

The Controller logs the actions modifying the FSC configuration in the Audit Log. You can view the Audit log in the Controller.
Open a browser, navigate to http://localhost:8080/audit-logs. You should see entries for the service you just made.

## In sum

You have provided your API to the FSC Group. In this part, you have:

- Created a service for the demo API
- Published the service in the demo Directory
- Used your Outway to consume your own Inway API.
- Viewed the transaction logs using the Controller
- Viewed the audit log using the Controller
