---
id: introduction
title: Introduction
---

:::caution
Note that this tutorial is not suited for production environments.
Its only purpose is to enable you to setup a local test environment.
:::

In this guide you will learn how to set up a local test environment, provide and consume APIs on the FSC Group.
We will use the Controller, a web interface for managing your FSC components.

The target audience is **system operators**.

## Components

The following components are part of the FSC reference implementation setup :

**Controller**

The Controller is at the heart of the FSC reference implementation. It provides a web interface to manage the FSC reference implementation.
Your Inways and Outways use the Controller to retrieve their configuration.

**Manager**

The Manager is the contact point for your organization. It negotiates Contracts.

**Inway**

The Inway functions as a gateway to the Services you offer to the Group.

**Outway**

The Outway is used by your organization to access services on the Group.

**Transaction Log**

The Transaction Log is a log of transactions made from and to your organization.

**Audit Log**

The Audit Log is a log of changes made to the FSC configuration of your organization.

**PostgreSQL Database**

The configuration of your Inways, the Services you provide and Contracts are stored in a PostgreSQL database.

## In sum

You've learned about all the components used by the FSC reference implementation.
Next up, lets [setup our local environment](./setup-your-environment).
