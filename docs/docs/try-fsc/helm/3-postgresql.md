---
id: 3-postgresql
title: Install PostgreSQL
---

# 3. Install PostgreSQL

The FSC reference implementation uses PostgreSQL to store various data, including the configuration of the various components and transaction logging. In this section, we install PostgreSQL using the [Bitnami Helm chart](https://bitnami.com/stack/postgresql/helm).

Install PostgreSQL by running:

```shell
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update

helm install \
  postgresql bitnami/postgresql \
  --namespace fsc \
  --set auth.database=fsc_manager
```

Then check if PostgreSQL is running properly by running:

```shell
kubectl get pods -n fsc
```

The output should look like this (make sure the status is `Running`).

```
NAME                      READY   STATUS    RESTARTS   AGE
postgresql-0              1/1     Running   0          ?s
```

Make sure you get the password for PostgreSQL and write it down somewhere, we'll need this password later in this guide. Get the password with the following command:

```shell
kubectl get secret --namespace nlx postgresql -o jsonpath="{.data.postgres-password}" | base64 -d
```

*Save this password somewhere without the closing `%` sign*

Create the databases needed for the Controller, Audit log and the Transaction Log.

The commands will ask you for the database password. This is the password you wrote down.

```shell
kubectl -n fsc  exec -ti postgresql-0  -- psql -U postgres -c '\x' -c 'create database fsc_controller;'
```

```shell
kubectl -n fsc  exec -ti postgresql-0  -- psql -U postgres -c '\x' -c 'create database fsc_auditlog;'
```

```shell
kubectl -n fsc  exec -ti postgresql-0  -- psql -U postgres -c '\x' -c 'create database fsc_txlog;'
```

Make sure the databases are created correctly:

```shell
kubectl -n fsc  exec -ti postgresql-0  -- psql -U postgres -c '\x' -c '\l'
```

The output should look similar to the following:

```
List of databases
-[ RECORD 1 ]-----+----------------------
Name              | fsc_controller
Owner             | postgres
Encoding          | UTF8
Collate           | en_US.UTF-8
Ctype             | en_US.UTF-8
ICU Locale        |
Locale Provider   | libc
Access privileges |
-[ RECORD 2 ]-----+----------------------
Name              | fsc_manager
Owner             | postgres
Encoding          | UTF8
Collate           | en_US.UTF-8
Ctype             | en_US.UTF-8
ICU Locale        |
Locale Provider   | libc
Access privileges |
-[ RECORD 3 ]-----+----------------------
Name              | fsc_txlog
Owner             | postgres
Encoding          | UTF8
Collate           | en_US.UTF-8
Ctype             | en_US.UTF-8
ICU Locale        |
Locale Provider   | libc
Access privileges |
-[ RECORD 4 ]-----+----------------------
Name              | fsc_auditlog
Owner             | postgres
Encoding          | UTF8
Collate           | en_US.UTF-8
Ctype             | en_US.UTF-8
ICU Locale        |
Locale Provider   | libc
Access privileges |
```
