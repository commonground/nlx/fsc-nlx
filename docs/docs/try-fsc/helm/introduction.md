---
id: introduction
title: Introduction
---

# Introduction

:::caution
Please note that the result of this guide is an FSC environment that connects with the FSC Demo Group. This environment is not suitable for production purposes.
The configuration described in this guide should not be used in production.
:::

In this guide, we work towards offering an API via an Inway. We request (and grant) access to that API and query that API with a client via an Outway. All the components of the FSC reference implementation are installed on a Kubernetes cluster ([Haven](https://haven.commonground.nl)).
