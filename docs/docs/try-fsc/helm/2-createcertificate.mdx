---
id: 2-create-certificate
title: Create Certificates
---

# 2. Create Certificates

## Install Cert Manager

All FSC components within an organization communicate with each other using internal TLS certificates. These certificates can be managed automatically with the help of [cert-manager](https://cert-manager.io/).

Install cert-manager on the cluster with:

```shell
helm repo add jetstack https://charts.jetstack.io
helm repo update

kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.12.3/cert-manager.crds.yaml

helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.12.3
```

Run the following command to check if the cert-manager is running

```shell
kubectl -n cert-manager get pods
```

The response should look similar to this:

```
NAME                                       READY   STATUS    RESTARTS   AGE
cert-manager-776649d6c6-cqc48              1/1     Running   0          93s
cert-manager-cainjector-7bb8cb69c5-bljdw   1/1     Running   0          93s
cert-manager-webhook-5c8bfb9bdf-64q7d      1/1     Running   0          93s
```

## Create CA Issuer

Now that cert-manager is running, we will create a CA Issuer for the FSC installation. This issuer will be used to issue certificates needed for internal communication between the FSC components.

### Create private key

Create the private key with:

```shell
openssl genrsa -out ca.key 2048
```

Check if `ca.key` is created by running:

```shell
ls
```

### Create Certificate

For Linux:

```shell
openssl req -x509 -new -nodes -key ca.key -subj "/CN=FSC" -days 3650 -reqexts v3_req -extensions v3_ca -out ca.crt
```

For macOS (Intel based):

```shell
openssl req -x509 -new -nodes -key ca.key -subj "/CN=FSC" -days 3650 -reqexts v3_req -extensions v3_ca -out ca.crt -config /usr/local/etc/openssl@1.1/openssl.cnf
```

For macOS (arm based, eg. M1):

```shell
/opt/homebrew/Cellar/openssl@1.1/1.1.1w/bin/openssl req -x509 -new -nodes -key ca.key -subj "/CN=FSC" -days 3650 -reqexts v3_req -extensions v3_ca -out ca.crt
```

Check if `ca.crt` is created by running:

```shell
ls
```

### Create the secret

Let's create the Kubernetes TLS secret now:

```shell
kubectl create secret tls internal-ca \
   --cert=ca.crt \
   --key=ca.key \
   --namespace=fsc
```

We now install the internal-issuer on the cluster:

```shell
kubectl apply -f internal-issuer.yaml
```

Let's check if the internal Issuer is created by running:

```shell
kubectl get issuer --namespace fsc
```

The expected result:

```
NAME       READY   AGE
internal   True    ??
```

## Create the external certificates

Traffic between organizations takes place via an external certificate. For the FSC demo Group, you can easily create the certificates via the [`init-organization-certs.sh`](https://gitlab.com/commonground/nlx/nlx-try-me/-/raw/main/scripts/init-organization-certs.sh?inline=false) script. Download this script and place it in your current work directory

The certificate will be created by executing the script.

For macOS & Linux:

```shell
docker run --rm -it -v $(pwd):/workdir -w /workdir --entrypoint /bin/bash cfssl/cfssl:v1.6.4 ./init-organization-certs.sh
```

For Windows:

```shell
docker run --rm -it -v %cd%:/workdir -w /workdir --entrypoint /bin/bash cfssl/cfssl:v1.6.4 ./init-organization-certs.sh
```

The script will ask you several questions.

### Script questions

Answer the questions accordingly:

- **Manager domain**, this should correspond to the Fully Qualified Domain Name (FQDN) of your Manager,
- **Inway domain**, this should correspond to the Fully Qualified Domain Name (FQDN) of your Inway,
- **Country**, enter any value
- **State**, enter any value
- **Locality Name**, enter any value
- **Organization Name**, please enter a URL-friendly value with a maximum length of 100 characters.
    A good value could be: `my-organization`.
- **Organization Unit Name**, enter any value
- **Email Address**, enter any value
- **Organization Serial Number** (optional), enter a serial number with a maximum length of 20 characters. Also make sure this value is unique for the Group in the [directory overview](https://directory.demo.fsc.nlx.io) as we do not check for uniqueness.

Then update the file permissions of the `ca` and `certs` directory:

```shell
sudo chmod -R 755 ./ca ./certs
```

Then create a Kubernetes TLS secret for the Manager by running:

```shell
kubectl create secret tls manager-group-tls \
   --cert=certs/org.crt \
   --key=certs/org.key \
   --namespace=fsc
```

And a Kubernetes TLS secret for the Inway

```shell
kubectl create secret tls inway-group-tls \
   --cert=certs/org.crt \
   --key=certs/org.key \
   --namespace=fsc
```

Let's check if the secrets are created by running:

```shell
kubectl get secrets --namespace fsc
```

The output should look like:

```
NAME                   TYPE                DATA   AGE
internal-ca            kubernetes.io/tls   2      7m16s
inway-group-tls        kubernetes.io/tls   2      10s
manager-group-tls      kubernetes.io/tls   2      18s
```

Your certificates now exists as secrets in Kubernetes. We will use this secret when we install FSC Manager and the FSC Inway.

## Obtaining your Subject Serial Number

The Subject Serial Number of your certificate, added by the Certificate Portal, is the primary identifier of your organization within the FSC Group.

To obtain your serial number, see the Subject part of the certificate by running:

```shell
openssl x509 -in certs/org.crt -text | grep Subject:
```

Example of the output: `Subject: C=nl, ST=zuid-holland, L=gemeente-stijns, O=my-organization, OU=my-organization-unit, CN=an-awesome-organization.nl/serialNumber=01234567890123456789`.

The value after `serialNumber=` in the Subject's CN field is the Subject Serial Number.
