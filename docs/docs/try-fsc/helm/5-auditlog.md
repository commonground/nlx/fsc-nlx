---
id: 5-auditlog
title: Install the Audit Log
---

# 5. Install the Audit Log

In order to provide a [BIO](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/cybersecurity/bio-en-ensia/baseline-informatiebeveiliging-overheid/) compliant product the FSC reference implementation needs an Audit Log.
When the Controller UI is used to create a Contract, sign a Contract etc., a record will be created in the Audit Log ensuring an audittrail.

In this step you will learn how to setup the Audit Log.

## Certificate

Run the following command to install a certificate for the Audit Log on the Kubernetes cluster:

```shell
kubectl apply -f auditlog-internal-tls.yaml
```

Check if the certificate has been created

```shell
kubectl -n fsc get secrets | grep auditlog-internal-tls
```

The output should look similar to:

```
auditlog-internal-tls             kubernetes.io/tls    3      35s
```

## Install the Audit Log API

Now let's install the Audit Log on the Kubernetes cluster.

First open the `auditlog-values.yaml`, edit the values below and save the file:

-   `<postgres-password>` replace this with the Postgres password you saved earlier.
-   The values `<file: ca.crt>` must be replaced by the contents of the file `ca.crt`. You have this file in your working directory.
    -   Copy the contents of the files **excluding** the '-----BEGIN XXXXXXXXX-----' and '-----END XXXXXXXXX-----' lines.
    -   Paste the content between the start and end lines and make sure the alignment is the same as the start and end lines
    -   Save the modified file

Run the following commands to install the Audit Log on the cluster:

```shell
helm -n fsc upgrade --install auditlog -f auditlog-values.yaml commonground/fsc-nlx-auditlog
```

Check if the Audit Log is running:

```shell
kubectl -n fsc get pods
```

A similar line should now show up:

```
auditlog-fsc-nlx-auditlog-69f9487bf5-fqj2w   1/1     Running     1          1m30s
```

You are now ready to setup your Controller
