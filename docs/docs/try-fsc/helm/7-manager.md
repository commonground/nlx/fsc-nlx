---
id: 7-manager
title: Install the Manager
---

# 7. Install the Manager

We are now going to install the Manager. The Manager negotiates Contracts between Peers. The Manager is exposed externally.

If you have **not** installed Postgres via the Bitnami chart as described in this guide then you need to make sure that a database called `fsc_manager` exists.

## Install internal certificates

Run the following command to install the internal Manager certificates on the Kubernetes cluster

```
kubectl apply -f manager-internal-tls.yaml -f manager-internal-unauthenticated-tls.yaml
```

Check if the certificates have been created

```
kubectl -n fsc get secrets | grep manager-internal
```

The output should look similar to:

```
manager-internal-tls                             kubernetes.io/tls    3      35s
manager-internal-unauthenticated-tls             kubernetes.io/tls    3      35s
```

## Manager chart

We are now going to create a configuration file for the Manager installation.
Open the file `manager-values.yaml` in a text editor and edit the values below:

-   `<managerAddress>` replace this with the external address of the Manager, this address should contain the same domain name as the certificate you created for the Manager in [step 2 of this guide](2-createcertificate.mdx). Make sure to include the scheme and port number. E.g. https://my-manager.my-organisation.com:8443
-   `<postgres-password>` replace this with the Postgres password you saved earlier.
-   The values `<file: ca.crt>` must be replaced by the contents of the file `ca.crt`. You have this file in your working directory.
    -   Copy the contents of the files **excluding** the '-----BEGIN XXXXXXXXX-----' and '-----END XXXXXXXXX-----' lines.
    -   Paste the content between the start and end lines and make sure the alignment is the same as the start and end lines
    -   Save the modified file

Make sure to save the file. Next, let's install the Manager:

```
helm -n fsc upgrade --install manager -f manager-values.yaml commonground/fsc-nlx-manager
```

Check if the Manager is running:

```
kubectl -n fsc get pods
```

The output should look similar to:

```
NAME                                             READY   STATUS    RESTARTS   AGE
controller-fsc-nlx-controller-7fb775cc75-hjg76   1/1     Running   0          4m51s
manager-fsc-nlx-manager-5956594bb7-dkt7j         1/1     Running   0          14s
postgresql-0                                     1/1     Running   0          19m
txlog-api-fsc-nlx-txlog-api-69f9487bf5-fqj2w     1/1     Running   0          11m
```

Now that the Manager is running we can continue open the Contracts page of the Controller.

Open your browser and navigate to the path `/contracts` of the Controller UI. The address if the Controller is determined by the value you used for `<hostname controller>` in [step 4](./6-controller). E.g. https://controller-ui.example.com/contracts

You should see an empty overview of your Contracts. This means both your Manager and Controller are running.
