---
id: 1-create-namespace
title: Create Namespace
---

# 1. Create Namespace

We will place all the components in our namespace.

_Tip: use the suggested namespace 'fsc' as we use this in all example commands in this guide. When you use a different namespace, you will also have to change the namespace in many files and commands._

Create the namespace with:

```
kubectl create namespace fsc
```

The output should be similar to:

```
namespace/fsc created
```
