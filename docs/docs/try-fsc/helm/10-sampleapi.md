---
id: 10-sample-api
title: Install sample API
---

# 10. Install sample API

We are now ready to offer a Service via the Inway. We have an example API available for this that we are going to
download and install called `basisregister-fictieve-kentekens`.

Download and install the Helm chart for the basisregister-fictieve-kentekens by running:

```
git clone https://gitlab.com/commonground/nlx/demo

helm -n nlx upgrade --install brfk demo/helm-charts/basisregister-fictieve-kentekens
```

Check with if the basisregister-fictieve-kentekens pod is healthy.

```
kubectl -n nlx get pods
```

A similar lines should now be displayed:

```
NAME                                                     READY   STATUS    RESTARTS   AGE
brfk-basisregister-fictieve-kentekens-5c6bfb66f8-k78jl   1/1     Running   0          16s
controller-fsc-nlx-controller-7b5589c697-mxrrp           1/1     Running   0          9m25s
inway-fsc-nlx-inway-565d45fd48-r8ckt                     1/1     Running   0          9m12s
manager-fsc-nlx-manager-5857d8f878-h2484                 1/1     Running   0          9m23s
outway-fsc-nlx-outway-675f7fd5bb-4wstg                   1/1     Running   0          104s
postgresql-0                                             1/1     Running   0          59m
txlog-api-fsc-nlx-txlog-api-69f9487bf5-w2xqj             1/1     Running   0          9m22s
```

## Make the example API available through FSC

Now we open the webinterface of the Controller and go to the Services page. Here we add a service by clicking the yellow `Service toevoegen` button. Enter the following values and save the service:

-   Servicenaam: basisregister-fictieve-kentekens
-   API endpoint URL: http://brfk-basisregister-fictieve-kentekens
    -   This URL is not publicly available but can be reached by your Inway.
-   Inways: Select your Inway

The next step is to create a Contract with a Service Publication Grant. This is needed to get the Service published in the Directory.

Open the Contracts page and click the yellow `Add Contract` button.

Next click on the blue `+ Add` button below ServicePublicationGrant

Extra input fields should appear.

Enter the following:

Directory Peer ID: `12345678901234567899`
Service Peer ID: `your peer id` (visible in the top right corner of the Contracts overview page)
Service Name: basisregister-fictieve-kentekens

Press the yellow `Add contract` button. This will send the Contract to the Directory, the Directory will sign it automatically. Navigate to the Directory page, the Service basisregister-fictieve-kentekens should be visible.
