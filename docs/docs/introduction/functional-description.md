---
id: functional-description
title: Functional Description
---

The FSC reference implementation is an implementation of the [FSC Core specification](https://commonground.gitlab.io/standards/fsc/core/draft-fsc-core-00.html) and the [extension logging](https://commonground.gitlab.io/standards/fsc/logging/draft-fsc-logging-00.html)

## Key features

- API discovery
- Route requests to APIs in other contexts (e.g. from Organization A to Organization B)
- Authorize connections to APIs
- Delegate authorizations
- Logging of request made to APIs
- Fine-grained authorization per request
- Automate approval of authorizations

### API discovery

Using the Directory it is possible to discover APIs of participating organizations. Each organization announces itself to the Directory and is able to publish APIs in the Directory.

### Route requests to APIs in other contexts

The FSC reference implementation ensures secure connections to APIs across different contexts allowing users to route requests to APIs hosted by other organizations while maintaining a high level of security and confidentiality.

### Authorize connections

Safely authorize organizations to connect to an API. Authorizations can easily be granted or revoked when necessary.

### Delegate authorizations

Organizations can delegate other organizations to connect or publish APIs on their behalf. This delegation is transparant. I.e. it is always clear that an organization is acting on behalf of another organization.

### Logging of request made to APIs

A log record is created for each API request made. This log record contains the source organization, destination organization, the API, date and in case of delegation the organization who orchestrated the delegation.
Each log record also contains a transaction ID which is unique for each request and can be used to connect log records across multiple organizations.

### Fine-grained authorization per request

An optional Policy Decision Point (PDP) can be configured which will decide if a specific API request should be allowed to continue.
This is an additional authorization step on top of the authorization done by FSC
(the organization has been allowed to connect to the API).
The PDP can be configured for both the API consumer and the API provider.

### Automate approval of authorizations

Authorizing organisations to connect to your APIs can become a time-consuming task when you offer a lot of APIs or have a lot of organisation who want to consume your API.
To make the proces of authorization more manageable, it is possible to configure a Policy Decision Point (PDP) which will be called each time your organisation receives a connection request.
The PDP can decide if the connection result should be automatically approved.
