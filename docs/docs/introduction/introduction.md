---
id: introduction
title: Introduction
---

**The FSC reference implementation** is an open source peer-to-peer system facilitating federated authentication, secure connecting and protocolling in a large-scale, dynamic API ecosystem with many organizations.

The FSC reference implementation is based on the [Federated Service Connectivity (FSC) standard](https://fsc-standaard.nl). This standard describes how (governmental) organizations should interact when exchanging data in a uniform and automated manner. The standard is developed in the context of the [Common Ground vision](https://github.com/VNG-Realisatie/common-ground) and the functionality is located on the integration layer of the Common Ground five-layer model, which supports the separation of  applications and data. This standard fulfills some of the required functions that together lead to technical interoperability. The standard therefore also applies to various national programs such as 'Data bij de Bron', 'Federatief Data Stelsel', 'Regie op gegevens' and the 'Inter-Bestuurlijke Data Strategie'. The standard is about standardizing the exchange of data, not about standardizing the data itself.

### The Component overview

Below an overview of the components in the FSC reference implementation is displayed.

![Component overview](component-overview.drawio.png)

When deploying the FSC reference implementation you will deploy the transaction logging, the Inway, the Outway, the Manager, the Audit log (not described in the FSC standard - it's an audit log to make the reference implementation [BIO](https://www.bio-overheid.nl/) compliant) and the Controller (not described in the FSC standard - it's a UI to manage everything for your convenience).

The component names correspond to the names used in the FSC standard.
