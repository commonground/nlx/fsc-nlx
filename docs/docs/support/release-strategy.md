---
id: release-strategy
title: Release strategy
---

The FSC reference implementation releases new versions roughly every two weeks. These include security updates, fix known issues and introduce new features.
Our releases follow [Semantic Versioning](https://semver.org/) and thus have the structure MAJOR.MINOR.PATCH. In short, we increment:

* MAJOR, when we make a breaking change,
* MINOR, when we add new functionality in a backwards compatible manner, and
* PATCH, when we make backwards compatible bug fixes.

We recommend our users to update their components every two weeks.

### Announcement

New versions are announced on [the releases page](https://gitlab.com/commonground/nlx/fsc-nlx/-/releases).
You can subscribe to new version tags through [RSS](https://gitlab.com/commonground/nlx/fsc-nlx/-/tags).

### Deprecation policy

We try to minimize the effort for administrators to keep up with new versions. When we are not able to make the change in a backward-compatible manner,
there is no other option than to upgrade to the new version.
