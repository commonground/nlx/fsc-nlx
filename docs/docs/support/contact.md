---
id: contact
title: Contact
---

We have multiple communication channels via which you can contact the team behind the FSC reference implementation.

### Sprint review

Every odd week on Wednesday from 10:00 to 11:00 you are more than welcome to join the review of the FSC reference implementation.
This is an [online Teams meeting](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjViYTM0YjEtMDZhOS00YzI3LTliZmYtYzMzZTNhZTc0YWZm%40thread.v2/0?context=%7b%22Tid%22%3a%226ef029ab-3fd7-4d98-9b0e-d1f5fedea6d1%22%2c%22Oid%22%3a%222792491b-bf61-4388-a055-8272a0083768%22%7d) where we interact with our community.

Together we inspect the increment, identify emerging improvements and discuss next steps.

### GitLab

You can find [the Source Code of the FSC reference implementation on GitLab](https://gitlab.com/commonground/nlx/fsc-nlx).
We use GitLab features extensively. Eg. as our CI/CD tool, Security Scanning, etc.

### Mail

You can use [support@nlx.io](mailto:support@nlx.io) for all questions. We are more than happy to help you.

### Slack

The team also uses Slack to communicate with the community. Please [request an invite](https://join.slack.com/t/samenorganiseren/shared_invite/zt-258vvmbgl-~Puj_q8_ugsEsMPe9quiEA) in order to join the Common Ground Slack workspace.
After that, please join the FSC channel and you will find a lively community about FSC.
