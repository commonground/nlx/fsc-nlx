---
id: setup-oidc
title: Setup OpenID Connect
---

In order to restrict access to the Controller web interface it is possible to enable a login. To ensure a secure login it is strongly recommend to use OpenID Connect for user authentication.

This guide will walk you through configuring the Controller to use your chosen OIDC provider, assuming you are deploying on a Kubernetes environment as described in our [try-fsc helm guide](../try-fsc/helm/0-preparation.md).

### Client registration

Before proceeding, ensure that you have registered a client for the Controller web interface with your OpenID Connect (OIDC) provider. You will need to obtain the Client ID and Client Secret provided by your OIDC provider to complete this guide.

### Authorization and claims

The Controller supports role-based authorization, offering two predefined roles: `fsc-admin` and `fsc-readonly`. A user assigned the `fsc-admin` role has full access to all features offered by the Controller, while a user with the `fsc-readonly` role can only view data.

To authenticate users and determine their role, the Controller checks the `groups` claim in the access token provided by the OIDC provider.
To utilize this feature, you must configure your OIDC provider to include the `groups` claim in the access token. The `groups` claim is an array of strings, so to assign a user the role of `fsc-admin`, the value `"["fsc-admin"]"` should be specified.

Please note that configuring the OIDC provider to include the `groups` claim is outside the scope of this guide, as it varies depending on the specific provider being used.

### Secret creation

Following successful registration of a client for the Controller with your OpenID Connect (OIDC) provider, we will create a Kubernetes secret to store the client secret and a secret key that ensures that cookies created by the Controller remain tamper-proof.

Create a file named `fsc-oidc-secret.yaml` with the following content:

```
apiVersion: v1
kind: Secret
metadata:
  name: fsc-oidc-secret
data:
  clientSecret: <clientSecret>
  secretKey: <secretKey>
```

Replace `<clientSecret>` with the client secret provided by your OIDC provider. The client secret should be base64 encoded.
Replace `<secretKey>` with a key that is 32 characters long. The key should be base64 encoded.

Create the secret

```
kubectl apply -n fsc -f fsc-oidc-secret.yaml
```

### Controller deployment

We now need to alter the deployment of the Controller. Edit `controller-values.yaml` and add `authn` to the `config` section.

```
config:
  authn:
    type: oidc
    oidc:
      clientId: <client-id>
      discoveryUrl: <discovery-url>
      redirectUrl: https://<hostname-controller>/oidc/callback
      logoutUrl: <logout-url>
      existingSecret:
        name: fsc-oidc-secret
        secretKeyKey: secretKey
        clientSecretKey: clientSecret
```
Replace the following items:

* `<client-id>`: The Client ID you created for the Controller webinterface.
* `<discovery-url>`: The discovery URL of the OIDC provider
* `<hostname-controller>`: The hostname of your Controller instance
* `<logout-url>`: The URL that should be called when a user logs out

(Re)Deploy the Controller Helm chart using the modified `controller-values.yaml` value file. Upon accessing the Controller webinterface, you should be automatically redirected to your OIDC provider for authentication.
