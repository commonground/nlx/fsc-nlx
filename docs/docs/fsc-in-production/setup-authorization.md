---
id: setup-authorization
title: Setup authorization
---

## Introduction

The FSC reference implementation has authorization features for both the Inway and the Outway. This document describes these features.
One option is to use [Open Policy Agent (OPA)](https://www.openpolicyagent.org/) as the authorization server. Other solutions can be used too,
as long as they conform to the specified Open API Specification (OAS).

## Authorization on an Outway

### Introduction

When you want to restrict which of your applications can communicate with other services using your Outway, you can setup authorization on the Outway.
To enable authorization on an Outway, organizations can plug-in their own authorization service. Once configured,
all requests will be authorized by this service before routing the request to the targeted API.

### How it works

Once an authorization service is configured the Outway will, after receiving a request from an application,
extract all the HTTP-headers from the request. Only the HTTP headers, the path and the Outway certificate will be send out to the authorization service. The authorization service can use this information to determine if authorization should be granted.
The authorization service will send the result back to the Outway. If the authorization is granted, the Outway will continue sending the request (body + HTTP headers) to the destination Inway.

### The authorization interface

In order to keep the components as flexible as possible, each organization will have to implement the "authorization interface"
on their own authorization service. Implementing this interface will enable communication between the outway and the authorization service.
We have described this interface using Open API Specification (OAS). [This specification can be found here](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/outway/authorization-interface.yaml).
A reference implementation has also been made available in the [FSC reference implementation repository](https://gitlab.com/commonground/nlx/fsc-nlx/-/tree/main/auth-opa) using the Open Policy Agent (OPA).

### Configuring the Outway

After you have implemented the authorization interface on your authorization service, you will have to configure the Outway to use it.
This can be done by setting the environment variable `AUTHORIZATION_SERVICE_ADDRESS` in the Docker image of the Outway.
This variable should contain the URL of your authorization service.

You should also set the environment variable `AUTHORIZATION_ROOT_CA`.
This variable contains the path to a root Certificate Authority (CA). To keep everything as secure as possible, your authorization service **must** only accept connections with TLS enabled.
The configured root CA will be used by the Outway to verify the certificates of your authorization service.

## Authorization on an Inway

### Introduction

When you want to restrict organizations from accessing your services, you can setup authorization on the Inway.
To enable authorization on an Inway, organizations can plug-in their own authorization service. Once configured, all requests will be authorized by this service before routing the request to the targeted API.

### How it works

Once an authorization service is configured the Inway will, after receiving a request from an outway, extract all the HTTP-headers from the request. The authorization service can use this information to determine if authorization should be granted. The authorization service will send the result back to the Inway. If the authorization is granted the Inway will send the request (body + HTTP headers) to the destination API.

### The authorization interface

In order to keep the components as flexible as possible, each organization will have to implement the "authorization interface" on their own authorization service. Implementing this interface will enable communication between the Inway and the authorization service.
We have described this interface using Open API Specification (OAS). [This specification can be found here](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/inway/authorization-interface.yaml).
A reference implementation has also been made available in [the Git repository](https://gitlab.com/commonground/nlx/fsc-nlx/-/tree/main/auth-opa) using the Open Policy Agent (OPA).

### Configuring the Inway

After you have implemented the authorization interface on your authorization service, you will have to configure the Inway
to use it. This can be done by setting the environment variable `AUTHORIZATION_SERVICE_ADDRESS` in the Docker image of the Inway.
This variable should contain the URL of your authorization service.

You should also set the environment variable `AUTHORIZATION_ROOT_CA`. This variable contains the path to a root Certificate Authority (CA).
To keep everything as secure as possible, your authorization service **must** only accept connections with TLS enabled.
The configured root CA will be used by the Inway to verify the certificates of your authorization service.
