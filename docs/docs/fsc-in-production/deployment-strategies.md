---
id: deployment-strategies
title: Deployment strategies
---

## Deployment strategies

This guide is made to help you take the next step from trying the FSC reference implementation to deploying in a production like environment.
There is no one size fits all deployment strategy, but we do provide two strategies.

Obviously, we are willing to help you in every way we can when you want to start using the FSC reference implementation in production.
See the [support page](../support/contact) to find out how to get in touch.

### 1. Helm charts

We use these Charts ourselves to deploy to our public environments. The charts can be found at [ArtifactHub](https://artifacthub.io/packages/search?org=commonground).
The [source code](https://gitlab.com/commonground/nlx/fsc-nlx/-/tree/main/helm) is on GitLab.

You can follow out [try FSC Helm guide](../try-fsc/helm/introduction), which is aimed at installing the software using Helm charts.

### 2. Docker containers

If you prefer to use the Docker images directly, you can find them at [Docker Hub](https://hub.docker.com/u/nlxio).
