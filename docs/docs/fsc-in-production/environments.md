---
id: environments
title: Environments
---

## Demo

We provide a Demo environment using self-signed certificates.
The Directory UI is located at https://directory-ui.demo.fsc.nlx.io

**Group ID**

fsc-demo

**Directory Peer ID**

12345678901234567899

**Directory Peer Manager Address**

https://directory.demo.fsc.nlx.io:8443

## Acceptance

RINIS provides the Acceptance environment using the self-signed certificates from our Demo environment.
The Directory UI is located at http://acc-api-directory.datastelsel.eu

**Group ID**

acc-dva-nederland

**Directory Peer ID**

01701105614973123825

**Directory Peer Manager Address**

https://acc.datastelsel.network:8443/

## Production

RINIS will provide a production environment in the near future.
