---
id: system-requirements
title: System Requirements
---

Determining the system requirements for the FSC reference implementation is challenging, as they heavily depend on
the anticipated traffic volume handled by the FSC components. For instance, an API processing 10,000 requests per
minute will require significantly different resources than one handling just 1 request per minute.

The system requirements outlined below refer specifically to the baseline needs for running the FSC components,
but actual requirements may vary based on usage patterns and load.

## Kubernetes

The FSC NLX Helm charts need to be deployed on a Kubernetes cluster.

Kubernetes requires the following setup:

```
2 GB or more of RAM per machine (any less will leave little room for your apps).
2 CPUs or more for control plane machines.
```

These can be found on the [kubeadm installation page](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/).

## Helm charts

The requirements for the FSC NLX components are based on a deployment of four FSC installations on a single cluster.
This contains 4 FSC installations (shared, Gemeente Riemer, Gemeente Stijns, RvRD and  Vergunningsoftware).

Resource consumption of the four FSC installations:

```
Memory: 1598Mi
CPU: 149m
```

So the most basic VM you can pick at Azure or AWS for a Managed Kubernetes installation should be sufficient.

## Try me

For the FSC Try Me you need Docker Compose. The requirements for Docker Compose are:

```
4 GB RAM
64-bit kernel and CPU support for virtualisation
```

These requirements are documented on the [Docker website](https://docs.docker.com/desktop/install/linux/#general-system-requirements).
