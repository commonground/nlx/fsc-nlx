---
id: renew-certificates
title: Renew Certificates
---

Trust within FSC is established by the use of [PKI](https://commonground.gitlab.io/standards/fsc/core/draft-fsc-core-00.html#trustanchor). This means Peers withing FSC must poses valid x.509 certificates in order to participate within the Group. 

However, certificates expire and must be renewed. Whilst there is nothing special about the certificates used within FSC, and they can be renewed via the processes offered by the CA there are some things to take into consideration with regard to FSC Contracts.

This guide will highlight the things to take into consideration while renewing certificates.

### Renewing the Certificate used for signing Contracts

Contracts within FSC are signed, if all Peers have placed their `Accept` signature on the Contract, the Contract becomes `Active`. 
When the certificate used for signing the `Accept` signature expires the Contract will also get the `Expired` state. 

There are no special requirements regarding the process of renewing the certificate used for signing Contracts. As long as the `PeerID` is present in the certificate as agreed upon in the Profile the Certificate can be used. The process for renewal offered by the CA of your choosing can be followed. 

After the new Certificate becomes available and the Manager is started with the new Certificate, Contracts that became expired because of Certificate expiration can be re-activated with the new Certificate. This is done by placing an `Accept` signature on the Contract with the new Certificate.

### Renewing the Certificate used for an Outway

Contracts used for connections, specifically the `ServiceConnectionGrant` and `DelegatedServiceConnectionGrant` contain the `PublicKeythumbprint`. This is used to issue [Certificate bound tokens](https://www.rfc-editor.org/rfc/rfc8705).
When the client Certificate used by the Outway to Connect to Inways and Managers to obtain Access Tokens expires the Outway can no longer obtain Access Tokens and connect to Inways. 

It is important to make sure the same public key is used when renewing the certificate. When a new Public key is used the `publicKeyThumbprint` changes and contracts containing the `publickeyThumbprint` become unusable and must be re-created. 

One way to make sure the same Public key is used, is to use the existing (expired) Certificate as a template for the new CSR (Certificate Signing Request). This can be accomplished with `openssl`:

```
openssl x509 -x509toreq -in existing-certificate.pem -signkey existing-private-key.pem -out new-certificate-siging-request.csr
```

The generated CSR can be submitted to a CA who can generate and sign a certificate.

If you want to verify the public key of the newly created CSR it is possible to extract the public key from the CSR with `openssl`:

```
openssl req -in new-certificate-signing-request.csr -noout -pubkey
```
