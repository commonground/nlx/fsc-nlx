// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage"
	app_errors "gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
)

type CreateRecordsHandler struct {
	storage storage.Storage
	logger  *logger.Logger
}

func NewCreateRecordsHandler(s storage.Storage, lgr *logger.Logger) (*CreateRecordsHandler, error) {
	if s == nil {
		return nil, errors.New("storage is required")
	}

	if lgr == nil {
		return nil, errors.New("logger is required")
	}

	return &CreateRecordsHandler{
		storage: s,
		logger:  lgr,
	}, nil
}

type CreateRecordsArgs struct {
	Records auditlog.NewRecordsArgs
}

func (h *CreateRecordsHandler) Handle(ctx context.Context, args *CreateRecordsArgs) error {
	records, err := auditlog.NewRecords(args.Records)
	if err != nil {
		return app_errors.NewIncorrectInputError(fmt.Sprintf("invalid records in args: %s", err))
	}

	err = h.storage.CreateRecords(ctx, records)
	if err != nil {
		h.logger.Error("could not create records", err)
		return err
	}

	return nil
}
