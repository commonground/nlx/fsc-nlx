// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
)

type SetRecordsUnderReviewHandler struct {
	storage storage.Storage
	logger  *logger.Logger
	clock   clock.Clock
}

func NewSetRecordsUnderReviewHandler(s storage.Storage, lgr *logger.Logger, clk clock.Clock) (*SetRecordsUnderReviewHandler, error) {
	if s == nil {
		return nil, errors.New("storage is required")
	}

	if lgr == nil {
		return nil, errors.New("logger is required")
	}

	if clk == nil {
		return nil, errors.New("clock is required")
	}

	return &SetRecordsUnderReviewHandler{
		storage: s,
		logger:  lgr,
		clock:   clk,
	}, nil
}

type SetRecordsUnderReviewArgs struct {
	IDs []string
}

func (h *SetRecordsUnderReviewHandler) Handle(ctx context.Context, args *SetRecordsUnderReviewArgs) error {
	ids, err := auditlog.NewIDs(args.IDs)
	if err != nil {
		return fmt.Errorf("could not parse ids: %w", err)
	}

	err = h.storage.SetRecordsUnderReview(ctx, &storage.SetRecordsUnderReviewRequest{
		UnderReviewAt: h.clock.Now(),
		IDs:           ids,
	})
	if err != nil {
		h.logger.Error("could not set records under review", err)
		return err
	}

	return nil
}
