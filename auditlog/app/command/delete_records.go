// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
)

const minimumRetentionPeriod = 30 * 24 * time.Hour // 30 days

type DeleteRecordsHandler struct {
	storage storage.Storage
	logger  *logger.Logger
	clock   clock.Clock
}

func NewDeleteRecordsHandler(s storage.Storage, lgr *logger.Logger, clk clock.Clock) (*DeleteRecordsHandler, error) {
	if s == nil {
		return nil, errors.New("storage is required")
	}

	if lgr == nil {
		return nil, errors.New("logger is required")
	}

	if clk == nil {
		return nil, errors.New("clock is required")
	}

	return &DeleteRecordsHandler{
		storage: s,
		logger:  lgr,
		clock:   clk,
	}, nil
}

type RetentionPeriod time.Duration

func NewRetentionPeriod(t time.Duration) (RetentionPeriod, error) {
	if t < minimumRetentionPeriod {
		return 0, fmt.Errorf("retenten period: %s, cannot be less than the minimum retention period: %s", t, minimumRetentionPeriod)
	}

	return RetentionPeriod(t), nil
}

func (r RetentionPeriod) Value() time.Duration {
	return time.Duration(r)
}

type DeleteRecordsArgs struct {
	RetentionPeriod RetentionPeriod
}

func (h *DeleteRecordsHandler) Handle(ctx context.Context, args *DeleteRecordsArgs) error {
	err := h.storage.DeleteRecords(ctx, h.clock.Now().Add(-args.RetentionPeriod.Value()))
	if err != nil {
		h.logger.Error("could not delete records", err)
		return err
	}

	return nil
}
