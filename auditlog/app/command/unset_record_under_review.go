// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
)

type UnsetRecordsUnderReviewHandler struct {
	storage storage.Storage
	logger  *logger.Logger
}

func NewUnsetRecordsUnderReviewHandler(s storage.Storage, lgr *logger.Logger) (*UnsetRecordsUnderReviewHandler, error) {
	if s == nil {
		return nil, errors.New("storage is required")
	}

	if lgr == nil {
		return nil, errors.New("logger is required")
	}

	return &UnsetRecordsUnderReviewHandler{
		storage: s,
		logger:  lgr,
	}, nil
}

type UnsetRecordsUnderReviewArgs struct {
	IDs []string
}

func (h *UnsetRecordsUnderReviewHandler) Handle(ctx context.Context, args *UnsetRecordsUnderReviewArgs) error {
	ids, err := auditlog.NewIDs(args.IDs)
	if err != nil {
		return fmt.Errorf("could not parse ids: %w", err)
	}

	err = h.storage.UnsetRecordsUnderReview(ctx, ids)
	if err != nil {
		h.logger.Error("could not unset records under review", err)
		return err
	}

	return nil
}
