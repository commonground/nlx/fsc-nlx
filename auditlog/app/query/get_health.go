// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage"
)

type GetHealthHandler struct {
	storage storage.Storage
}

func NewGetHealthHandler(strg storage.Storage) (*GetHealthHandler, error) {
	if strg == nil {
		return nil, errors.New("storage is required")
	}

	return &GetHealthHandler{storage: strg}, nil
}

func (h *GetHealthHandler) Handle(ctx context.Context) error {
	return h.storage.Ping(ctx)
}
