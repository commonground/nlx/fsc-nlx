// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
)

const defaultLimit = 100

type ListRecordsHandler struct {
	storage storage.Storage
}

type SortOrder string

const (
	SortOrderAscending  SortOrder = "asc"
	SortOrderDescending SortOrder = "desc"
)

type Period struct {
	Start time.Time
	End   time.Time
}

type Filters struct {
	Period         *Period
	IDs            []string
	CorrelationIDs []string
	ContentHashes  []string
	ServiceNames   []string
	UnderReview    bool
}

type Pagination struct {
	StartID   string
	Limit     uint32
	SortOrder SortOrder
}

type ListRecordsHandlerArgs struct {
	GroupID    string
	Pagination *Pagination
	Filters    *Filters
}

func NewListRecordsHandler(s storage.Storage) (*ListRecordsHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &ListRecordsHandler{
		storage: s,
	}, nil
}

func (l *ListRecordsHandler) Handle(ctx context.Context, args *ListRecordsHandlerArgs) (auditlog.Records, error) {
	req, err := reqToRepo(args)
	if err != nil {
		return nil, errors.Wrap(err, "invalid list records handler args")
	}

	records, err := l.storage.ListRecords(ctx, req)
	if err != nil {
		return nil, err
	}

	return records, nil
}

//nolint:gocyclo // would be not more readable when splitting this function
func reqToRepo(req *ListRecordsHandlerArgs) (*storage.ListRecordsRequest, error) {
	var period *storage.Period

	if req.Filters.Period != nil {
		period = &storage.Period{
			Start: req.Filters.Period.Start,
			End:   req.Filters.Period.End,
		}
	}

	var startID auditlog.ID

	sortOrder := storage.SortOrderAscending

	var limit uint32 = defaultLimit

	if req.Pagination != nil {
		if req.Pagination.StartID != "" {
			id, err := uuid.FromString(req.Pagination.StartID)
			if err != nil {
				return nil, fmt.Errorf("invalid pagination startID: %w", err)
			}

			startID = auditlog.ID(id)
		}

		if req.Pagination.Limit != 0 {
			limit = req.Pagination.Limit
		}

		sortOrder = sortOrderToStorage(req.Pagination.SortOrder)
	}

	if req.GroupID == "" {
		return nil, errors.Errorf("groupID cannot be empty")
	}

	ids, err := auditlog.NewIDs(req.Filters.IDs)
	if err != nil {
		return nil, fmt.Errorf("invalid IDs in filter: %w", err)
	}

	correlationIDs := make([]auditlog.CorrelationID, 0, len(req.Filters.CorrelationIDs))

	for _, i := range req.Filters.CorrelationIDs {
		id, err := auditlog.NewCorrelationID(i)
		if err != nil {
			return nil, fmt.Errorf("invalid CorrelationID in filter: %w", err)
		}

		correlationIDs = append(correlationIDs, id)
	}

	return &storage.ListRecordsRequest{
		GroupID: req.GroupID,
		Pagination: &storage.Pagination{
			StartID:   startID,
			Limit:     limit,
			SortOrder: sortOrder,
		},
		Filters: &storage.Filters{
			Period:         period,
			IDs:            ids,
			CorrelationIDs: correlationIDs,
			ContentHashes:  req.Filters.ContentHashes,
			ServiceNames:   req.Filters.ServiceNames,
			UnderReview:    req.Filters.UnderReview,
		},
	}, nil
}

func sortOrderToStorage(o SortOrder) storage.SortOrder {
	switch o {
	case SortOrderAscending:
		return storage.SortOrderAscending
	case SortOrderDescending:
		return storage.SortOrderDescending
	default:
		return storage.SortOrderUnspecified
	}
}
