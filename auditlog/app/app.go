// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package app

import (
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/query"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
)

type Application struct {
	Queries  Queries
	Commands Commands
}

type Queries struct {
	ListRecords *query.ListRecordsHandler
	GetHealth   *query.GetHealthHandler
}

type Commands struct {
	CreateRecords           *command.CreateRecordsHandler
	DeleteRecords           *command.DeleteRecordsHandler
	SetRecordsUnderReview   *command.SetRecordsUnderReviewHandler
	UnsetRecordsUnderReview *command.UnsetRecordsUnderReviewHandler
}

type NewApplicationArgs struct {
	Storage storage.Storage
	Logger  *logger.Logger
	Clock   clock.Clock
}

func New(a *NewApplicationArgs) (*Application, error) {
	createRecordsHandler, err := command.NewCreateRecordsHandler(a.Storage, a.Logger)
	if err != nil {
		return nil, fmt.Errorf("could not create createRecordsHandler: %w", err)
	}

	deleteRecordsHandler, err := command.NewDeleteRecordsHandler(a.Storage, a.Logger, a.Clock)
	if err != nil {
		return nil, fmt.Errorf("could not create deleteRecordsHandler: %w", err)
	}

	setRecordsUnderReviewHandler, err := command.NewSetRecordsUnderReviewHandler(a.Storage, a.Logger, a.Clock)
	if err != nil {
		return nil, fmt.Errorf("could not create setRecordsUnderReviewHandler: %w", err)
	}

	unsetRecordsUnderReviewHandler, err := command.NewUnsetRecordsUnderReviewHandler(a.Storage, a.Logger)
	if err != nil {
		return nil, fmt.Errorf("could not create unsetRecordsUnderReviewHandler: %w", err)
	}

	listRecordsHandler, err := query.NewListRecordsHandler(a.Storage)
	if err != nil {
		return nil, fmt.Errorf("could not create listRecordsHandler: %w", err)
	}

	getHealthHandler, err := query.NewGetHealthHandler(a.Storage)
	if err != nil {
		return nil, fmt.Errorf("could not create getHealthHandler: %w", err)
	}

	return &Application{
		Queries: Queries{
			ListRecords: listRecordsHandler,
			GetHealth:   getHealthHandler,
		},
		Commands: Commands{
			CreateRecords:           createRecordsHandler,
			DeleteRecords:           deleteRecordsHandler,
			SetRecordsUnderReview:   setRecordsUnderReviewHandler,
			UnsetRecordsUnderReview: unsetRecordsUnderReviewHandler,
		},
	}, nil
}
