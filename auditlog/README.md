# Auditlog

The Auditlog is a component that exposes an Rest API with which other components can record auditlog events and retrieve those auditlog records. The auditlog can be used internally by organizations to get insight and record what happens in their FSC implementation deployment.

# Auditlog Record

An auditlog record consists out of the so called "five W's".

1. What happened?
1. When did it happen?
1. Where did it happen?
1. Who was involved?
1. Where did it come from?

The record contains the following fields:

-   ID
-   Group ID
-   Correlation ID
-   Actor
-   Event
-   Status
-   Source
-   Component
-   Created At

### ID

This is an unique ID, different for every auditlog record, in the format of an UUID v7.

### Group ID

This is the Group ID of a FSC Group. Components can be part of multiple FSC Groups. With this Group ID the auditlog records can be differentiated between multiple groups.

### Correlation ID

When an Event spans across multiple components, the events can be correlated by their Correlation ID. E.g. when the Controller creates a contract in the Manager, the Controller passes the Correlation ID to the Manager so that the Manager can write the auditlog event under the same Correlation ID. This way Events in different components can be linked to eachother.

### Actor

The Actor specifies who was involved. E.g. the name and email of the user who executed the action.

### Event

The Event specifies what happened and on which object. E.g. that a Contract was acceped, including which Contract, identified by its content hash.

### Status

The Status specifies what the status is of the action. E.g. that the action was initiated, that the action succeeded or that the action was unauthorized.

### Source

The Source specifies where the action came from. E.g. the IP Address and User Agent of the request.

### Component

The Component specifies where it happened. E.g. the action happened in the Controller, or in the Manager.

### Created At

The Created At specifies when the action happened. Accurate down to the second.

# OpenAPI Specification

Organizations can use the Rest API defined in `auditlog/ports/rest/api/openapi.yaml` to insert and retrieve auditlog records. This makes it flexible for organizations to use the auditlog component in their own FSC implementation of e.g. the Controller or Manager

# Reliability

The auditlog component itself takes a few security measures. It only allows authorized clients to read and write auditlog records via mTLS and PKI Certificates. Also the auditlog table in the database is protected, records can only be inserted and readed, but not updated or deleted.

The reliability of this component depends also on a few security measures taken by the administrator. First, only authorized components should be given access to the auditlog via PKI Certificates. Second, the administrator must make sure that the database is only accessed by the auditlog component, and no other users or services should be allowed access to the database. Third, the database should be backed up regularly, to prevent accidental and intentional data loss. The components using the auditlog should record all events that change the state of the system or configuration. They should record the initiation of the event and the failure or success. This way the auditlog will also contain records of unauthorized attempts to change the state of the system.

# Retention

The auditlog component can be configured to retain records for a certain duration. The minimum retention duration is 30 days. This means that audilog records are always kept for at least 30 days and up to the specified retention duration. After the retention duration is expired, the record will be automatically deleted. Auditlog records can be put under review. This means that some auditlog records are marked as under review and will not be deleted until that mark is removed. That way the security officer can analyze the auditlog records for certain security breach events independed of the retention period. When the record is no longer under review and the retention duration is expired, the record will be removed.
