// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package cron

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/go-co-op/gocron/v2"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
)

const deleteRecordsInterval = 1 * time.Hour

type Cron struct {
	app       *app.Application
	logger    *logger.Logger
	scheduler gocron.Scheduler
}

type NewArgs struct {
	Ctx                          context.Context
	Logger                       *logger.Logger
	App                          *app.Application
	DeleteRecordsRetentionPeriod time.Duration
}

func New(args *NewArgs) (*Cron, error) {
	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.App == nil {
		return nil, errors.New("app is required")
	}

	retentionPeriod, err := command.NewRetentionPeriod(args.DeleteRecordsRetentionPeriod)
	if err != nil {
		return nil, fmt.Errorf("could not create retention period: %w", err)
	}

	scheduler, err := gocron.NewScheduler()
	if err != nil {
		return nil, fmt.Errorf("could not create scheduler: %w", err)
	}

	_, err = scheduler.NewJob(
		gocron.DurationJob(
			deleteRecordsInterval,
		),
		gocron.NewTask(
			func() {
				args.Logger.Info("deleting records older than the retention period...")

				err = args.App.Commands.DeleteRecords.Handle(args.Ctx, &command.DeleteRecordsArgs{
					RetentionPeriod: retentionPeriod,
				})
				if err != nil {
					args.Logger.Error("could not delete records in command", err)
				}
			},
		),
		gocron.WithSingletonMode(gocron.LimitModeReschedule),
		gocron.WithStartAt(gocron.WithStartImmediately()),
	)
	if err != nil {
		return nil, fmt.Errorf("could not add delete records job to scheduler: %w", err)
	}

	return &Cron{
		app:       args.App,
		logger:    args.Logger,
		scheduler: scheduler,
	}, nil
}

func (c *Cron) Start() {
	c.scheduler.Start()
}

func (c *Cron) Shutdown() error {
	return c.scheduler.Shutdown()
}
