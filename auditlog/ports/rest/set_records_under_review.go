// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/command"
	api "gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/server"
)

func (s *Server) SetRecordsUnderReview(ctx context.Context, req api.SetRecordsUnderReviewRequestObject) (api.SetRecordsUnderReviewResponseObject, error) {
	err := s.app.Commands.SetRecordsUnderReview.Handle(ctx, &command.SetRecordsUnderReviewArgs{IDs: req.Body.Ids})
	if err != nil {
		s.logger.Error("failed to set auditlog records under review", err)

		return nil, fmt.Errorf("could not set records under review: %w", err)
	}

	return api.SetRecordsUnderReview204Response{}, nil
}
