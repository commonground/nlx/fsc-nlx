// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/server"
)

func (s *Server) CreateRecords(ctx context.Context, req api.CreateRecordsRequestObject) (api.CreateRecordsResponseObject, error) {
	recordArgs, err := mapRecordsToCommand(req.Body.Records)
	if err != nil {
		s.logger.Error("could not map input to auditlog records", err)

		return nil, fmt.Errorf("could not map input to auditlog records: %w", err)
	}

	err = s.app.Commands.CreateRecords.Handle(ctx, &command.CreateRecordsArgs{Records: recordArgs})
	if err != nil {
		s.logger.Error("failed to create auditlog records", err)

		return nil, fmt.Errorf("could not create records: %w", err)
	}

	return api.CreateRecords204Response{}, nil
}

func mapRecordsToCommand(recs []models.Record) (auditlog.NewRecordsArgs, error) {
	recordArgs := make(auditlog.NewRecordsArgs, 0, len(recs))

	for i := range recs {
		actor, err := mapActorToCommand(recs[i].Actor)
		if err != nil {
			return nil, fmt.Errorf("could not map actor: %w", err)
		}

		event, err := mapEventToCommand(recs[i].Event)
		if err != nil {
			return nil, fmt.Errorf("could not map event: %w", err)
		}

		source, err := mapSourceToCommand(recs[i].Source)
		if err != nil {
			return nil, fmt.Errorf("could not map source: %w", err)
		}

		status, err := mapStatusToCommand(recs[i].Status)
		if err != nil {
			return nil, fmt.Errorf("could not map status: %w", err)
		}

		component, err := mapComponent(recs[i].Component)
		if err != nil {
			return nil, fmt.Errorf("could not map component: %w", err)
		}

		recordArgs = append(recordArgs, &auditlog.NewRecordArgs{
			ID:            recs[i].Id,
			GroupID:       recs[i].GroupId,
			CorrelationID: recs[i].CorrelationId,
			Actor:         actor,
			Event:         event,
			Source:        source,
			Status:        status,
			Component:     component,
			CreatedAt:     time.Unix(recs[i].CreatedAt, 0),
		})
	}

	return recordArgs, nil
}

func mapActorToCommand(a models.Record_Actor) (auditlog.NewActorArgs, error) {
	model, err := a.ValueByDiscriminator()
	if err != nil {
		return nil, err
	}

	var actor auditlog.NewActorArgs

	switch ac := model.(type) {
	case models.ActorUnknown:
		actor = &auditlog.NewActorUnknownArgs{}
	case models.ActorUser:
		actor = &auditlog.NewActorUserArgs{
			Name:  ac.Name,
			Email: ac.Email,
		}
	case models.ActorService:
		actor = &auditlog.NewActorServiceArgs{
			SubjectCommonName:   ac.SubjectCommonName,
			PublicKeyThumbprint: ac.PublicKeyThumbprint,
		}
	default:
		return nil, fmt.Errorf("unknown actor type: %T", ac)
	}

	return actor, nil
}

func mapEventToCommand(e models.Record_Event) (auditlog.NewEventArgs, error) {
	model, err := e.ValueByDiscriminator()
	if err != nil {
		return nil, err
	}

	var event auditlog.NewEventArgs

	switch ev := model.(type) {
	case models.EventLogin:
		event = &auditlog.NewEventLoginArgs{}

	case models.EventCreateContract:
		event = &auditlog.NewEventCreateContractArgs{
			ContractHash: ev.ContentHash,
		}
	case models.EventAcceptContract:
		event = &auditlog.NewEventAcceptContractArgs{
			ContractHash: ev.ContentHash,
		}
	case models.EventRejectContract:
		event = &auditlog.NewEventRejectContractArgs{
			ContractHash: ev.ContentHash,
		}
	case models.EventRevokeContract:
		event = &auditlog.NewEventRevokeContractArgs{
			ContractHash: ev.ContentHash,
		}

	case models.EventCreateService:
		event = &auditlog.NewEventCreateServiceArgs{
			ServiceName: ev.ServiceName,
		}
	case models.EventUpdateService:
		event = &auditlog.NewEventUpdateServiceArgs{
			ServiceName: ev.ServiceName,
		}
	case models.EventDeleteService:
		event = &auditlog.NewEventDeleteServiceArgs{
			ServiceName: ev.ServiceName,
		}
	case models.EventFailedDistributionRetry:
		action, errMapAction := mapFailedDistributionRetryAction(ev.Action)
		if errMapAction != nil {
			return nil, fmt.Errorf("unknown failed distribution retry action: %s", errMapAction)
		}

		event = &auditlog.NewEventFailedDistributionRetryArgs{
			ContentHash: ev.ContentHash,
			Action:      string(action),
			PeerID:      ev.PeerId,
		}

	case models.EventCreatePeer:
		event = &auditlog.NewEventCreatePeerArgs{
			PeerID: ev.PeerId,
		}
	case models.EventUpdatePeer:
		event = &auditlog.NewEventUpdatePeerArgs{
			PeerID: ev.PeerId,
		}
	case models.EventDeletePeer:
		event = &auditlog.NewEventDeletePeerArgs{
			PeerID: ev.PeerId,
		}

	case models.EventSynchronizePeers:
		event = &auditlog.NewEventSynchronizePeersArgs{}
	default:
		return nil, fmt.Errorf("unknown event type: %T", ev)
	}

	return event, nil
}

func mapFailedDistributionRetryAction(action models.FailedDistributionRetryAction) (auditlog.FailedDistributionAction, error) {
	switch action {
	case models.SUBMITCONTRACT:
		return auditlog.FailedDistributionActionSubmitContract, nil
	case models.SUBMITACCEPTSIGNATURE:
		return auditlog.FailedDistributionActionSubmitAcceptSignature, nil
	case models.SUBMITREJECTSIGNATURE:
		return auditlog.FailedDistributionActionSubmitRejectSignature, nil
	case models.SUBMITREVOKESIGNATURE:
		return auditlog.FailedDistributionActionSubmitRevokeSignature, nil
	default:
		return "", fmt.Errorf("invalid action: %s", action)
	}
}

func mapSourceToCommand(s models.Record_Source) (auditlog.NewSourceArgs, error) {
	model, err := s.ValueByDiscriminator()
	if err != nil {
		return nil, err
	}

	var source auditlog.NewSourceArgs

	switch so := model.(type) {
	case models.SourceHttp:
		source = &auditlog.NewSourceHTTPArgs{
			IPAddress: so.IpAddress,
			UserAgent: so.UserAgent,
		}
	default:
		return nil, fmt.Errorf("unknown source type: %T", so)
	}

	return source, nil
}

func mapStatusToCommand(s models.Record_Status) (auditlog.NewStatusArgs, error) {
	model, err := s.ValueByDiscriminator()
	if err != nil {
		return nil, err
	}

	var status auditlog.NewStatusArgs

	switch sa := model.(type) {
	case models.StatusInitiated:
		status = &auditlog.NewStatusInitiatedArgs{}
	case models.StatusUnauthorized:
		status = &auditlog.NewStatusUnauthorizedArgs{
			Error: sa.Error,
		}
	case models.StatusFailed:
		status = &auditlog.NewStatusFailedArgs{
			Error: sa.Error,
		}
	case models.StatusSucceeded:
		status = &auditlog.NewStatusSucceededArgs{}
	default:
		return nil, fmt.Errorf("unknown status type: %T", sa)
	}

	return status, nil
}

func mapComponent(c models.RecordComponent) (auditlog.Component, error) {
	switch c {
	case models.COMPONENTCONTROLLER:
		return auditlog.ComponentController, nil
	case models.COMPONENTMANAGER:
		return auditlog.ComponentManager, nil
	default:
		return auditlog.ComponentUnspecified, fmt.Errorf("unknown component: %s", c)
	}
}
