// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app"
	api "gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
)

type Server struct {
	app     *app.Application
	logger  *logger.Logger
	handler http.Handler
}

type NewArgs struct {
	Logger *logger.Logger
	App    *app.Application
}

func New(args *NewArgs) (*Server, error) {
	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.App == nil {
		return nil, errors.New("app is required")
	}

	s := &Server{
		app:    args.App,
		logger: args.Logger,
	}

	strict := api.NewStrictHandlerWithOptions(s, []api.StrictMiddlewareFunc{
		func(f api.StrictHandlerFunc, operationID string) api.StrictHandlerFunc {
			return func(ctx context.Context, w http.ResponseWriter, r *http.Request, args interface{}) (interface{}, error) {
				if len(r.TLS.PeerCertificates) == 0 {
					return nil, fmt.Errorf("client certificate missing")
				}

				return f(ctx, w, r, args)
			}
		},
	}, api.StrictHTTPServerOptions{
		RequestErrorHandlerFunc:  RequestErrorHandler,
		ResponseErrorHandlerFunc: ResponseErrorHandler,
	})

	r := chi.NewRouter()
	api.HandlerFromMux(strict, r)

	s.handler = r

	return s, nil
}

func (s *Server) Handler() http.Handler {
	return s.handler
}
