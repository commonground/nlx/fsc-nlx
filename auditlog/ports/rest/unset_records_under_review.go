// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/command"
	api "gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/server"
)

func (s *Server) UnsetRecordsUnderReview(ctx context.Context, req api.UnsetRecordsUnderReviewRequestObject) (api.UnsetRecordsUnderReviewResponseObject, error) {
	err := s.app.Commands.UnsetRecordsUnderReview.Handle(ctx, &command.UnsetRecordsUnderReviewArgs{IDs: req.Body.Ids})
	if err != nil {
		s.logger.Error("failed to unset auditlog records under review", err)

		return nil, fmt.Errorf("could not unset records under review: %w", err)
	}

	return api.UnsetRecordsUnderReview204Response{}, nil
}
