// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/query"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/server"
)

func (s *Server) GetRecords(ctx context.Context, req api.GetRecordsRequestObject) (api.GetRecordsResponseObject, error) {
	records, err := s.app.Queries.ListRecords.Handle(ctx, getRecordsReqToArgs(&req))
	if err != nil {
		return nil, fmt.Errorf("could not list records: %w", err)
	}

	recs, err := recordsToModel(records)
	if err != nil {
		return nil, fmt.Errorf("could not convert records into oas model: %w", err)
	}

	var nextCursor string
	if req.Params.Id == nil && req.Params.CorrelationId == nil {
		nextCursor = getNextLogCursor(records)
	}

	return api.GetRecords200JSONResponse{
		Records: recs,
		Pagination: models.PaginationResult{
			NextCursor: nextCursor,
		},
	}, nil
}

func getRecordsReqToArgs(a *api.GetRecordsRequestObject) *query.ListRecordsHandlerArgs {
	var (
		startID     string
		limit       uint32
		underReview bool
	)

	if a.Params.Cursor != nil {
		startID = *a.Params.Cursor
	}

	if a.Params.Limit != nil {
		limit = *a.Params.Limit
	}

	if a.Params.UnderReview != nil {
		underReview = *a.Params.UnderReview
	}

	return &query.ListRecordsHandlerArgs{
		GroupID: a.Params.GroupId,
		Pagination: &query.Pagination{
			StartID:   startID,
			Limit:     limit,
			SortOrder: sortOrderToQuery(a.Params.SortOrder),
		},
		Filters: &query.Filters{
			Period: &query.Period{
				Start: timestampToQuery(a.Params.After),
				End:   timestampToQuery(a.Params.Before),
			},
			IDs:            arrayToQuery(a.Params.Id),
			CorrelationIDs: arrayToQuery(a.Params.CorrelationId),
			ContentHashes:  arrayToQuery(a.Params.ContentHash),
			ServiceNames:   arrayToQuery(a.Params.ServiceName),
			UnderReview:    underReview,
		},
	}
}

func timestampToQuery(t *models.Timestamp) time.Time {
	if t == nil {
		return time.Time{}
	}

	return time.Unix(*t, 0)
}

func arrayToQuery(a *[]string) []string {
	if a == nil {
		return []string{}
	}

	return *a
}

func sortOrderToQuery(s *models.SortOrder) query.SortOrder {
	if s == nil {
		return query.SortOrder("")
	}

	switch *s {
	case models.SORTORDERASCENDING:
		return query.SortOrderAscending
	case models.SORTORDERDESCENDING:
		return query.SortOrderDescending
	default:
		return query.SortOrder("")
	}
}

func recordsToModel(recs auditlog.Records) ([]models.Record, error) {
	records := make([]models.Record, 0, len(recs))

	for _, r := range recs {
		actor, err := actorToModel(r.Actor())
		if err != nil {
			return nil, err
		}

		event, err := eventToModel(r.Event())
		if err != nil {
			return nil, err
		}

		source, err := sourceToModel(r.Source())
		if err != nil {
			return nil, err
		}

		status, err := statusToModel(r.Status())
		if err != nil {
			return nil, err
		}

		var underReviewAt *int64

		if r.UnderReviewAt() != nil {
			unix := r.UnderReviewAt().Unix()
			underReviewAt = &unix
		}

		records = append(records, models.Record{
			Id:            r.ID().String(),
			GroupId:       r.GroupID(),
			CorrelationId: r.CorrelationID().String(),
			Actor:         *actor,
			Event:         *event,
			Source:        *source,
			Status:        *status,
			Component:     componentToModel(r.Component()),
			UnderReviewAt: underReviewAt,
			CreatedAt:     r.CreatedAt().Unix(),
		})
	}

	return records, nil
}

func actorToModel(a auditlog.Actor) (*models.Record_Actor, error) {
	var err error

	actor := &models.Record_Actor{}

	switch ac := a.(type) {
	case *auditlog.ActorUnknown:
		err = actor.FromActorUnknown(models.ActorUnknown{})
	case *auditlog.ActorUser:
		err = actor.FromActorUser(models.ActorUser{
			Name:  ac.Name(),
			Email: ac.Email(),
		})
	case *auditlog.ActorService:
		err = actor.FromActorService(models.ActorService{
			SubjectCommonName:   ac.SubjectCommonName(),
			PublicKeyThumbprint: ac.PublicKeyThumbprint(),
		})
	default:
		return nil, fmt.Errorf("unknown actor type: %T", ac)
	}

	if err != nil {
		return nil, fmt.Errorf("could not convert actor into oas model: %w", err)
	}

	return actor, nil
}

func eventToModel(a auditlog.Event) (*models.Record_Event, error) {
	var err error

	event := &models.Record_Event{}

	switch ac := a.(type) {
	case *auditlog.EventLogin:
		err = event.FromEventLogin(models.EventLogin{})

	case *auditlog.EventCreateContract:
		err = event.FromEventCreateContract(models.EventCreateContract{
			ContentHash: ac.ContractHash(),
		})
	case *auditlog.EventAcceptContract:
		err = event.FromEventAcceptContract(models.EventAcceptContract{
			ContentHash: ac.ContractHash(),
		})
	case *auditlog.EventRejectContract:
		err = event.FromEventRejectContract(models.EventRejectContract{
			ContentHash: ac.ContractHash(),
		})
	case *auditlog.EventRevokeContract:
		err = event.FromEventRevokeContract(models.EventRevokeContract{
			ContentHash: ac.ContractHash(),
		})

	case *auditlog.EventCreateService:
		err = event.FromEventCreateService(models.EventCreateService{
			ServiceName: ac.ServiceName(),
		})
	case *auditlog.EventUpdateService:
		err = event.FromEventUpdateService(models.EventUpdateService{
			ServiceName: ac.ServiceName(),
		})
	case *auditlog.EventDeleteService:
		err = event.FromEventDeleteService(models.EventDeleteService{
			ServiceName: ac.ServiceName(),
		})

	case *auditlog.EventFailedDistributionRetry:
		action, errMapAction := failedDistributionActionToModel(ac.Action())
		if errMapAction != nil {
			return nil, fmt.Errorf("unknown failed distribution retry action: %q", action)
		}

		err = event.FromEventFailedDistributionRetry(models.EventFailedDistributionRetry{
			ContentHash: ac.ContentHash(),
			Action:      action,
			PeerId:      ac.PeerID(),
		})

	case *auditlog.EventCreatePeer:
		err = event.FromEventCreatePeer(models.EventCreatePeer{
			PeerId: ac.PeerID(),
		})
	case *auditlog.EventUpdatePeer:
		err = event.FromEventUpdatePeer(models.EventUpdatePeer{
			PeerId: ac.PeerID(),
		})
	case *auditlog.EventDeletePeer:
		err = event.FromEventDeletePeer(models.EventDeletePeer{
			PeerId: ac.PeerID(),
		})

	case *auditlog.EventSynchronizePeers:
		err = event.FromEventSynchronizePeers(models.EventSynchronizePeers{})
	default:
		return nil, fmt.Errorf("unknown event type: %T", ac)
	}

	if err != nil {
		return nil, fmt.Errorf("could not convert event into oas model: %w", err)
	}

	return event, nil
}

func failedDistributionActionToModel(a auditlog.FailedDistributionAction) (models.FailedDistributionRetryAction, error) {
	switch a {
	case auditlog.FailedDistributionActionSubmitContract:
		return models.SUBMITCONTRACT, nil
	case auditlog.FailedDistributionActionSubmitAcceptSignature:
		return models.SUBMITACCEPTSIGNATURE, nil
	case auditlog.FailedDistributionActionSubmitRejectSignature:
		return models.SUBMITREJECTSIGNATURE, nil
	case auditlog.FailedDistributionActionSubmitRevokeSignature:
		return models.SUBMITREVOKESIGNATURE, nil
	default:
		return "", fmt.Errorf("unknown failed distribution action: %s", a)
	}
}

func sourceToModel(a auditlog.Source) (*models.Record_Source, error) {
	var err error

	source := &models.Record_Source{}

	switch ac := a.(type) {
	case *auditlog.SourceHTTP:
		err = source.FromSourceHttp(models.SourceHttp{
			IpAddress: ac.IPAddress().String(),
			UserAgent: ac.UserAgent(),
		})
	default:
		return nil, fmt.Errorf("unknown source type: %T", ac)
	}

	if err != nil {
		return nil, fmt.Errorf("could not convert source into oas model: %w", err)
	}

	return source, nil
}

func statusToModel(a auditlog.Status) (*models.Record_Status, error) {
	var err error

	status := &models.Record_Status{}

	switch ac := a.(type) {
	case *auditlog.StatusInitiated:
		err = status.FromStatusInitiated(models.StatusInitiated{})
	case *auditlog.StatusUnauthorized:
		err = status.FromStatusUnauthorized(models.StatusUnauthorized{
			Error: ac.Error(),
		})
	case *auditlog.StatusFailed:
		err = status.FromStatusFailed(models.StatusFailed{
			Error: ac.Error(),
		})
	case *auditlog.StatusSucceeded:
		err = status.FromStatusSucceeded(models.StatusSucceeded{})

	default:
		return nil, fmt.Errorf("unknown status type: %T", ac)
	}

	if err != nil {
		return nil, fmt.Errorf("could not convert status into oas model: %w", err)
	}

	return status, nil
}

func componentToModel(c auditlog.Component) models.RecordComponent {
	switch c {
	case auditlog.ComponentController:
		return models.COMPONENTCONTROLLER
	case auditlog.ComponentManager:
		return models.COMPONENTMANAGER
	default:
		return models.RecordComponent("")
	}
}

func getNextLogCursor(records auditlog.Records) string {
	if len(records) == 0 {
		return ""
	}

	return records[len(records)-1].ID().String()
}
