# Copyright © VNG Realisatie 2023
# Licensed under the EUPL

openapi: 3.0.3
info:
  title: Auditlog API
  version: 1.0.0
  description: |
    Auditlog REST API

    Can be used to create and retrieve auditlog records.
servers:
  - url: https://{auditlogUrl}:443
    variables:
      auditlogUrl:
        default: localhost
        description: URL of the Auditlog
paths:
  /v1/auditlogs:
    post:
      summary: Create auditlog records
      operationId: CreateRecords
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                records:
                  description: List of auditlog records to create
                  type: array
                  items:
                    $ref: "#/components/schemas/record"
              required:
                - records
      responses:
        204:
          description: No content
        400:
          $ref: "#/components/responses/400BadRequest"
        401:
          $ref: "#/components/responses/401Unauthorized"
        403:
          $ref: "#/components/responses/403Forbidden"
        404:
          $ref: "#/components/responses/404NotFound"
        408:
          $ref: "#/components/responses/408RequestTimeout"
        413:
          $ref: "#/components/responses/413PayloadTooLarge"
        414:
          $ref: "#/components/responses/414URITooLong"
        422:
          $ref: "#/components/responses/422UnprocessableEntity"
        429:
          $ref: "#/components/responses/429TooManyRequests"
        431:
          $ref: "#/components/responses/431RequestHeaderFieldsTooLarge"
        500:
          $ref: "#/components/responses/500InternalServerError"
        503:
          $ref: "#/components/responses/503ServiceUnavailable"
        505:
          $ref: "#/components/responses/505HTTPVersionNotSupported"

    get:
      summary: Returns auditlog records
      operationId: getRecords
      parameters:
        - $ref: "#/components/parameters/queryPaginationCursor"
        - $ref: "#/components/parameters/queryPaginationLimit"
        - $ref: "#/components/parameters/queryPaginationOrder"
        - in: query
          description: Auditlog records with a creation date after this value will be returned
          name: after
          schema:
            $ref: "#/components/schemas/timestamp"
          required: false
        - in: query
          description: Auditlog records with a creation date before this value will be returned
          name: before
          schema:
            $ref: "#/components/schemas/timestamp"
          required: false
        - in: query
          description: Get the auditlog records for this group.
          name: group_id
          schema:
            $ref: "#/components/schemas/groupID"
          required: true
        - in: query
          description: A list of IDs on which to filter. When this filter is used, all other filters should be ignored
          name: id
          schema:
            type: array
            items:
              $ref: "#/components/schemas/uuid"
          required: false
          style: form
          explode: false
        - in: query
          description: A list of Correlation IDs on which to filter. When this filter is used, all other filters should be ignored
          name: correlation_id
          schema:
            type: array
            items:
              $ref: "#/components/schemas/uuid"
          required: false
          style: form
          explode: false
        - in: query
          description: A list of content hashes on which to filter, e.g. events which contain these content hashes. When multiple filters are used the records where either condition is true should be returned
          name: content_hash
          schema:
            type: array
            items:
              $ref: "#/components/schemas/contentHash"
          required: false
          style: form
          explode: false
        - in: query
          description: A list of service names on which to filter, e.g. events which contain these service names. When multiple filters are used the records where either condition is true should be returned
          name: service_name
          schema:
            type: array
            items:
              $ref: "#/components/schemas/serviceName"
          required: false
          style: form
          explode: false
        - in: query
          description: When true only list records that are under review
          name: under_review
          schema:
            type: boolean
          required: false
      responses:
        200:
          description: Log records
          content:
            application/json:
              schema:
                type: object
                properties:
                  records:
                    description: List of log records
                    type: array
                    items:
                      $ref: "#/components/schemas/record"
                  pagination:
                    $ref: "#/components/schemas/paginationResult"
                required:
                  - records
                  - pagination
          links:
            pagination:
              operationId: getLogs
              parameters:
                query.cursor: "$response.body#/pagination/next_cursor"
              description: >
                The `next_cursor` value returned in the response can be used as
                the `cursor` query parameter in `GET /v1/logs`.
        400:
          $ref: "#/components/responses/400BadRequest"
        401:
          $ref: "#/components/responses/401Unauthorized"
        403:
          $ref: "#/components/responses/403Forbidden"
        404:
          $ref: "#/components/responses/404NotFound"
        408:
          $ref: "#/components/responses/408RequestTimeout"
        413:
          $ref: "#/components/responses/413PayloadTooLarge"
        414:
          $ref: "#/components/responses/414URITooLong"
        422:
          $ref: "#/components/responses/422UnprocessableEntity"
        429:
          $ref: "#/components/responses/429TooManyRequests"
        431:
          $ref: "#/components/responses/431RequestHeaderFieldsTooLarge"
        500:
          $ref: "#/components/responses/500InternalServerError"
        503:
          $ref: "#/components/responses/503ServiceUnavailable"
        505:
          $ref: "#/components/responses/505HTTPVersionNotSupported"
  /v1/auditlogs/review:
    put:
      summary: Set auditlog records under review
      operationId: SetRecordsUnderReview
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                ids:
                  type: array
                  description: A list of IDs of the records that needs to be set under review
                  items:
                    $ref: "#/components/schemas/uuid"
              required:
                - ids
      responses:
        204:
          description: No content
        400:
          $ref: "#/components/responses/400BadRequest"
        401:
          $ref: "#/components/responses/401Unauthorized"
        403:
          $ref: "#/components/responses/403Forbidden"
        404:
          $ref: "#/components/responses/404NotFound"
        408:
          $ref: "#/components/responses/408RequestTimeout"
        413:
          $ref: "#/components/responses/413PayloadTooLarge"
        414:
          $ref: "#/components/responses/414URITooLong"
        422:
          $ref: "#/components/responses/422UnprocessableEntity"
        429:
          $ref: "#/components/responses/429TooManyRequests"
        431:
          $ref: "#/components/responses/431RequestHeaderFieldsTooLarge"
        500:
          $ref: "#/components/responses/500InternalServerError"
        503:
          $ref: "#/components/responses/503ServiceUnavailable"
        505:
          $ref: "#/components/responses/505HTTPVersionNotSupported"

    delete:
      summary: Unset auditlog records under review
      operationId: UnsetRecordsUnderReview
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                ids:
                  type: array
                  description: A list of IDs of the records that needs to be unset under review
                  items:
                    $ref: "#/components/schemas/uuid"
              required:
                - ids
      responses:
        204:
          description: No content
        400:
          $ref: "#/components/responses/400BadRequest"
        401:
          $ref: "#/components/responses/401Unauthorized"
        403:
          $ref: "#/components/responses/403Forbidden"
        404:
          $ref: "#/components/responses/404NotFound"
        408:
          $ref: "#/components/responses/408RequestTimeout"
        413:
          $ref: "#/components/responses/413PayloadTooLarge"
        414:
          $ref: "#/components/responses/414URITooLong"
        422:
          $ref: "#/components/responses/422UnprocessableEntity"
        429:
          $ref: "#/components/responses/429TooManyRequests"
        431:
          $ref: "#/components/responses/431RequestHeaderFieldsTooLarge"
        500:
          $ref: "#/components/responses/500InternalServerError"
        503:
          $ref: "#/components/responses/503ServiceUnavailable"
        505:
          $ref: "#/components/responses/505HTTPVersionNotSupported"
  /v1/health:
    get:
      operationId: getHealth
      summary: health endpoint for the Audit log API
      responses:
        200:
          description: Audit log API is healthy
        503:
          description: Audit log API is unhealthy
components:
  parameters:
    queryPaginationCursor:
      in: query
      name: cursor
      description: "A cursor from which paginated results should be returned. Leave empty for the first page. Base64 encoded string of two properties: a created_at as unix timestamp and a content hash separated by a comma. Eg. `base64(1672527600,$1$1$+PQI7we01qIfEwq4O5UioLKzjGBgRva6F5+bUfDlKxUjcY5yX1MRsn6NKquDbL8VcklhYO9sk18rHD6La3w/mg)"
      schema:
        type: string
      required: false
    queryPaginationLimit:
      in: query
      name: limit
      description: The maximum number of results
      schema:
        type: integer
        format: uint32
        maximum: 1000
        minimum: 1
        default: 25
      required: false
    queryPaginationOrder:
      in: query
      name: sort_order
      description: The sorting order of the results
      required: false
      schema:
        $ref: "#/components/schemas/sortOrder"
  schemas:
    error:
      description: Error object according to [RFC 9457](https://www.rfc-editor.org/rfc/rfc9457.html)
      properties:
        type:
          type: string
          description: URI reference that identifies the problem type.
          example:
        status:
          type: integer
          description: HTTP status code generated by the origin server for this occurrence of the problem.
          minimum: 100
          maximum: 599
        title:
          type: string
          description: Short, human-readable summary of the problem type.
        details:
          type: string
          description: >
            Human-readable explanation specific to this occurrence of the problem.

            The "detail" string, if present, ought to focus on helping the client correct the problem, rather than giving debugging information.
        instance:
          type: string
          description: URI reference that identifies the specific occurrence of the problem.
      required:
        - type
        - status
        - title
        - details
        - instance
    serviceName:
      description: The name of a service
      type: string
      example: random_service_name
      minLength: 3
      maxLength: 255
    contentHash:
      type: string
      description: The hash of the Content of a Contract
      example: $1$1$+PQI7we01qIfEwq4O5UioLKzjGBgRva6F5+bUfDlKxUjcY5yX1MRsn6NKquDbL8VcklhYO9sk18rHD6La3w/mg
      maxLength: 150
    peerID:
      type: string
      description: The ID of a Peer
      example: 12345678901234567890
      minLength: 20
      maxLength: 20
    groupID:
      description: The FSC Group ID (Directory URI)
      type: string
      example: example.fsc.com
    timestamp:
      type: integer
      description: A Unix timestamp
      format: int64
      example: 1672527600
      minimum: 0
    uuid:
      type: string
      description: UUID v7
      example: 0189f7e5-c883-7106-8272-ccb7fcba0575
      minLength: 36
      maxLength: 36
    failedDistributionRetryAction:
      type: string
      enum:
        - SUBMIT_CONTRACT
        - SUBMIT_ACCEPT_SIGNATURE
        - SUBMIT_REJECT_SIGNATURE
        - SUBMIT_REVOKE_SIGNATURE
    actorType:
      type: string
      enum:
        - ACTOR_TYPE_UNKNOWN
        - ACTOR_TYPE_USER
        - ACTOR_TYPE_SERVICE
    eventType:
      type: string
      enum:
        - EVENT_TYPE_LOGIN
        - EVENT_TYPE_CREATE_CONTRACT
        - EVENT_TYPE_ACCEPT_CONTRACT
        - EVENT_TYPE_REJECT_CONTRACT
        - EVENT_TYPE_REVOKE_CONTRACT
        - EVENT_TYPE_CREATE_SERVICE
        - EVENT_TYPE_UPDATE_SERVICE
        - EVENT_TYPE_DELETE_SERVICE
        - EVENT_TYPE_CREATE_PEER
        - EVENT_TYPE_UPDATE_PEER
        - EVENT_TYPE_DELETE_PEER
        - EVENT_TYPE_SYNCHRONIZE_PEERS
        - EVENT_TYPE_FAILED_DISTRIBUTION_RETRY
    sourceType:
      type: string
      enum:
        - SOURCE_TYPE_HTTP
    statusType:
      type: string
      enum:
        - STATUS_TYPE_INITIATED
        - STATUS_TYPE_FAILED
        - STATUS_TYPE_UNAUTHORIZED
        - STATUS_TYPE_SUCCEEDED
    record:
      description: A auditlog record
      type: object
      properties:
        id:
          $ref: "#/components/schemas/uuid"
        group_id:
          $ref: "#/components/schemas/groupID"
        correlation_id:
          $ref: "#/components/schemas/uuid"
        actor:
          oneOf:
            - $ref: "#/components/schemas/actorUnknown"
            - $ref: "#/components/schemas/actorUser"
            - $ref: "#/components/schemas/actorService"
          discriminator:
            propertyName: type
            mapping:
              ACTOR_TYPE_UNKNOWN: "#/components/schemas/actorUnknown"
              ACTOR_TYPE_USER: "#/components/schemas/actorUser"
              ACTOR_TYPE_SERVICE: "#/components/schemas/actorService"
        event:
          oneOf:
            - $ref: "#/components/schemas/eventLogin"
            - $ref: "#/components/schemas/eventCreateContract"
            - $ref: "#/components/schemas/eventAcceptContract"
            - $ref: "#/components/schemas/eventRejectContract"
            - $ref: "#/components/schemas/eventRevokeContract"
            - $ref: "#/components/schemas/eventCreateService"
            - $ref: "#/components/schemas/eventUpdateService"
            - $ref: "#/components/schemas/eventDeleteService"
            - $ref: "#/components/schemas/eventFailedDistributionRetry"
            - $ref: "#/components/schemas/eventCreatePeer"
            - $ref: "#/components/schemas/eventUpdatePeer"
            - $ref: "#/components/schemas/eventDeletePeer"
            - $ref: "#/components/schemas/eventSynchronizePeers"
          discriminator:
            propertyName: type
            mapping:
              EVENT_TYPE_LOGIN: "#/components/schemas/eventLogin"
              EVENT_TYPE_CREATE_CONTRACT: "#/components/schemas/eventCreateContract"
              EVENT_TYPE_ACCEPT_CONTRACT: "#/components/schemas/eventAcceptContract"
              EVENT_TYPE_REJECT_CONTRACT: "#/components/schemas/eventRejectContract"
              EVENT_TYPE_REVOKE_CONTRACT: "#/components/schemas/eventRevokeContract"
              EVENT_TYPE_CREATE_SERVICE: "#/components/schemas/eventCreateService"
              EVENT_TYPE_UPDATE_SERVICE: "#/components/schemas/eventUpdateService"
              EVENT_TYPE_DELETE_SERVICE: "#/components/schemas/eventDeleteService"
              EVENT_TYPE_CREATE_PEER: "#/components/schemas/eventCreatePeer"
              EVENT_TYPE_UPDATE_PEER: "#/components/schemas/eventUpdatePeer"
              EVENT_TYPE_DELETE_PEER: "#/components/schemas/eventDeletePeer"
              EVENT_TYPE_SYNCHRONIZE_PEERS: "#/components/schemas/eventSynchronizePeers"
              EVENT_TYPE_FAILED_DISTRIBUTION_RETRY: "#/components/schemas/eventFailedDistributionRetry"
        source:
          oneOf:
            - $ref: "#/components/schemas/sourceHttp"
          discriminator:
            propertyName: type
            mapping:
              SOURCE_TYPE_HTTP: "#/components/schemas/sourceHttp"
        status:
          oneOf:
            - $ref: "#/components/schemas/statusInitiated"
            - $ref: "#/components/schemas/statusFailed"
            - $ref: "#/components/schemas/statusUnauthorized"
            - $ref: "#/components/schemas/statusSucceeded"
          discriminator:
            propertyName: type
            mapping:
              STATUS_TYPE_INITIATED: "#/components/schemas/statusInitiated"
              STATUS_TYPE_FAILED: "#/components/schemas/statusFailed"
              STATUS_TYPE_UNAUTHORIZED: "#/components/schemas/statusUnauthorized"
              STATUS_TYPE_SUCCEEDED: "#/components/schemas/statusSucceeded"
        component:
          description: The component creating this record
          type: string
          enum:
            - COMPONENT_CONTROLLER
            - COMPONENT_MANAGER
        under_review_at:
          readOnly: true
          $ref: "#/components/schemas/timestamp"
        created_at:
          $ref: "#/components/schemas/timestamp"
      required:
        - id
        - group_id
        - correlation_id
        - actor
        - event
        - source
        - status
        - component
        - created_at
    actorUnknown:
      description: Used when actor is unknown, e.g. when the event happened before authentication
      type: object
      properties:
        type:
          $ref: "#/components/schemas/actorType"
      required:
        - type
    actorUser:
      description: User that initiated the event
      type: object
      properties:
        type:
          $ref: "#/components/schemas/actorType"
        name:
          description: Name of the user
          type: string
        email:
          description: Email of the user
          type: string
      required:
        - type
        - name
        - email
    actorService:
      description: Service that initiated the event
      type: object
      properties:
        type:
          $ref: "#/components/schemas/actorType"
        subject_common_name:
          description: Subject Common Name of the certificate used by the service
          type: string
        public_key_thumbprint:
          description: Public Key Thumbprint of the certificate used by the service
          type: string
      required:
        - type
        - subject_common_name
        - public_key_thumbprint
    eventLogin:
      description: Event login
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
      required:
        - type
    eventCreateContract:
      description: Event create contract
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        content_hash:
          $ref: "#/components/schemas/contentHash"
      required:
        - type
        - content_hash
    eventAcceptContract:
      description: Event accept contract
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        content_hash:
          $ref: "#/components/schemas/contentHash"
      required:
        - type
        - content_hash
    eventRevokeContract:
      description: Event revoke contract
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        content_hash:
          $ref: "#/components/schemas/contentHash"
      required:
        - type
        - content_hash
    eventRejectContract:
      description: Event reject contract
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        content_hash:
          $ref: "#/components/schemas/contentHash"
      required:
        - type
        - content_hash
    eventCreateService:
      description: Event create service
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        service_name:
          $ref: "#/components/schemas/serviceName"
      required:
        - type
        - service_name
    eventUpdateService:
      description: Event update service
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        service_name:
          $ref: "#/components/schemas/serviceName"
      required:
        - type
        - service_name
    eventDeleteService:
      description: Event delete service
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        service_name:
          $ref: "#/components/schemas/serviceName"
      required:
        - type
        - service_name
    eventFailedDistributionRetry:
      description: Event failed distribution retry
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        content_hash:
          $ref: "#/components/schemas/contentHash"
        peer_id:
          $ref: "#/components/schemas/peerID"
        action:
          $ref: "#/components/schemas/failedDistributionRetryAction"
      required:
        - type
        - content_hash
        - action
        - peer_id
    eventCreatePeer:
      description: Event create peer
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        peer_id:
          $ref: "#/components/schemas/peerID"
      required:
        - type
        - peer_id
    eventUpdatePeer:
      description: Event update peer
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        peer_id:
          $ref: "#/components/schemas/peerID"
      required:
        - type
        - peer_id
    eventDeletePeer:
      description: Event delete peer
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
        peer_id:
          $ref: "#/components/schemas/peerID"
      required:
        - type
        - peer_id
    eventSynchronizePeers:
      description: Event synchronize peers
      type: object
      properties:
        type:
          $ref: "#/components/schemas/eventType"
      required:
        - type
    sourceHttp:
      description: Info about the http source of the event
      type: object
      properties:
        type:
          $ref: "#/components/schemas/sourceType"
        ip_address:
          description: IP address of the request
          type: string
        user_agent:
          description: User agent of the request
          type: string
      required:
        - type
        - ip_address
        - user_agent
    statusInitiated:
      description: Status initiated
      type: object
      properties:
        type:
          $ref: "#/components/schemas/statusType"
      required:
        - type
    statusFailed:
      description: Status failed
      type: object
      properties:
        type:
          $ref: "#/components/schemas/statusType"
        error:
          type: string
          description: Description of the error
      required:
        - type
        - error
    statusUnauthorized:
      description: Status unauthorized
      type: object
      properties:
        type:
          $ref: "#/components/schemas/statusType"
        error:
          type: string
          description: Description of the error
      required:
        - type
        - error
    statusSucceeded:
      description: Status succeeded
      type: object
      properties:
        type:
          $ref: "#/components/schemas/statusType"
      required:
        - type
    sortOrder:
      description: The order in which the results should be returned
      type: string
      default: SORT_ORDER_DESCENDING
      enum:
        - SORT_ORDER_ASCENDING
        - SORT_ORDER_DESCENDING
    paginationResult:
      type: object
      properties:
        next_cursor:
          description: A cursor that can be used to retrieve the next page or an empty string if there are no more results
          type: string
      required:
        - next_cursor
  responses:
    400BadRequest:
      description: Bad Request, (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing).
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    401Unauthorized:
      description: Unauthorized, the user does not have valid authentication credentials for the target resource.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    403Forbidden:
      description: Forbidden, the user does not have the permission to execute this request
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    404NotFound:
      description: Not Found, the requested resource could not be found.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    405MethodNotAllowed:
      description: Method Not Allowed, the request method is not supported for the requested resource
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    408RequestTimeout:
      description: Request Timeout, the client did not produce a request within the time that the server was prepared to wait.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    413PayloadTooLarge:
      description: Payload Too Large, the request is larger than the server is willing or able to process.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    414URITooLong:
      description: URI Too Long, the URI provided was too long for the server to process.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    422UnprocessableEntity:
      description: Unprocessable Entity, the request was well-formed but was unable to be followed due to semantic errors.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    429TooManyRequests:
      description: Too Many Requests, the user has sent too many requests in a given amount of time.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    431RequestHeaderFieldsTooLarge:
      description: Request Header Fields Too Large, the server is unwilling to process the request because either an individual header field, or all the header fields collectively, are too large.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    500InternalServerError:
      description: Internal Server Error, a generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    503ServiceUnavailable:
      description: Service Unavailable, the server cannot handle the request (because it is overloaded or down for maintenance). Generally, this is a temporary state.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
    505HTTPVersionNotSupported:
      description: HTTP Version Not Supported, the server does not support the HTTP version used in the request.
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/error"
