// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package service_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/query"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/models"
)

//nolint:funlen,dupl // this is a test
func TestCreateLogRecords(t *testing.T) {
	testCases := map[string]struct {
		args           models.CreateRecordsJSONRequestBody
		want           *auditlog.Record
		wantStatusCode int
	}{
		"with_create_peer_event": {
			args: func() models.CreateRecordsJSONRequestBody {
				actor := models.Record_Actor{}

				_ = actor.FromActorUser(models.ActorUser{
					Name:  "test-user",
					Email: "test@example.com",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreatePeer(models.EventCreatePeer{
					PeerId: "12345678901234567890",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusFailed(models.StatusFailed{
					Error: "some error",
				})

				return models.CreateRecordsJSONRequestBody{
					Records: []models.Record{
						{
							Id:            "01899c62-eba5-7b58-b68d-000000000003",
							GroupId:       "fsc-local",
							CorrelationId: "01899c62-eba5-7b58-b68d-000000000002",
							Actor:         actor,
							Event:         event,
							Source:        source,
							Status:        status,
							Component:     models.COMPONENTMANAGER,
							CreatedAt:     testClock.Now().Unix(),
						},
					},
				}
			}(),
			want: func() *auditlog.Record {
				r, err := auditlog.NewRecord(&auditlog.NewRecordArgs{
					ID:            "01899c62-eba5-7b58-b68d-000000000003",
					GroupID:       "fsc-local",
					CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
					Actor: &auditlog.NewActorUserArgs{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &auditlog.NewEventCreatePeerArgs{
						PeerID: "12345678901234567890",
					},
					Source: &auditlog.NewSourceHTTPArgs{
						IPAddress: "127.0.0.1",
						UserAgent: "firefox mozilla",
					},
					Status: &auditlog.NewStatusFailedArgs{
						Error: "some error",
					},
					Component: auditlog.ComponentManager,
					CreatedAt: nowInUTC,
				})
				require.NoError(t, err)

				return r
			}(),
			wantStatusCode: http.StatusNoContent,
		},
		"happy_flow": {
			args: func() models.CreateRecordsJSONRequestBody {
				actor := models.Record_Actor{}

				_ = actor.FromActorUser(models.ActorUser{
					Name:  "test-user",
					Email: "test@example.com",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreateContract(models.EventCreateContract{
					ContentHash: "test-content-hash",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusFailed(models.StatusFailed{
					Error: "some error",
				})

				return models.CreateRecordsJSONRequestBody{
					Records: []models.Record{
						{
							Id:            "01899c62-eba5-7b58-b68d-000000000001",
							GroupId:       "fsc-local",
							CorrelationId: "01899c62-eba5-7b58-b68d-000000000002",
							Actor:         actor,
							Event:         event,
							Source:        source,
							Status:        status,
							Component:     models.COMPONENTMANAGER,
							CreatedAt:     testClock.Now().Unix(),
						},
					},
				}
			}(),
			want: func() *auditlog.Record {
				r, err := auditlog.NewRecord(&auditlog.NewRecordArgs{
					ID:            "01899c62-eba5-7b58-b68d-000000000001",
					GroupID:       "fsc-local",
					CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
					Actor: &auditlog.NewActorUserArgs{
						Name:  "test-user",
						Email: "test@example.com",
					},
					Event: &auditlog.NewEventCreateContractArgs{
						ContractHash: "test-content-hash",
					},
					Source: &auditlog.NewSourceHTTPArgs{
						IPAddress: "127.0.0.1",
						UserAgent: "firefox mozilla",
					},
					Status: &auditlog.NewStatusFailedArgs{
						Error: "some error",
					},
					Component: auditlog.ComponentManager,
					CreatedAt: nowInUTC,
				})
				require.NoError(t, err)

				return r
			}(),
			wantStatusCode: http.StatusNoContent,
		},
	}

	for name, tt := range testCases {
		t.Run(name, func(t *testing.T) {
			server, app := newService(t.Name())

			defer server.Close()

			client, err := createAuditlogClient(server.URL, orgB.CertBundle)
			assert.NoError(t, err)

			resp, err := client.CreateRecordsWithResponse(context.Background(), tt.args)
			assert.NoError(t, err)

			if !assert.Equal(t, tt.wantStatusCode, resp.StatusCode()) {
				t.Errorf("response body: %s", resp.Body)
			}

			var id = tt.args.Records[0].Id

			records, err := app.Queries.ListRecords.Handle(context.Background(), &query.ListRecordsHandlerArgs{
				GroupID:    tt.args.Records[0].GroupId,
				Pagination: nil,
				Filters: &query.Filters{
					IDs: []string{id},
				},
			})
			assert.NoError(t, err)

			assert.Len(t, records, 1)
			assert.Equal(t, records[0], tt.want)
		})
	}
}
