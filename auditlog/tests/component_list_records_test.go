// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package service_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/models"
)

//nolint:funlen,gocyclo,dupl // this is a test
func TestListLogRecords(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		records                *command.CreateRecordsArgs
		params                 *models.GetRecordsParams
		wantStatusCode         int
		wantResponseRecords    []models.Record
		wantResponseNextCursor string
	}{
		"happy_flow_limit": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000002",
						GroupID:       "fsc-local-limit",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
						Actor: &auditlog.NewActorUserArgs{
							Name:  "test-user",
							Email: "test@example.com",
						},
						Event: &auditlog.NewEventCreateContractArgs{
							ContractHash: "test-content-hash",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status: &auditlog.NewStatusFailedArgs{
							Error: "some error",
						},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000003",
						GroupID:       "fsc-local-limit",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
						Actor: &auditlog.NewActorUserArgs{
							Name:  "test-user",
							Email: "test@example.com",
						},
						Event: &auditlog.NewEventCreateContractArgs{
							ContractHash: "test-content-hash",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status: &auditlog.NewStatusFailedArgs{
							Error: "some error",
						},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000004",
						GroupID:       "fsc-local-limit",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
						Actor: &auditlog.NewActorUserArgs{
							Name:  "test-user",
							Email: "test@example.com",
						},
						Event: &auditlog.NewEventCreateContractArgs{
							ContractHash: "test-content-hash",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status: &auditlog.NewStatusFailedArgs{
							Error: "some error",
						},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: func() *models.GetRecordsParams {
				limit := models.QueryPaginationLimit(2)
				return &models.GetRecordsParams{
					GroupId: "fsc-local-limit",
					Limit:   &limit,
				}
			}(),
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorUser(models.ActorUser{
					Name:  "test-user",
					Email: "test@example.com",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreateContract(models.EventCreateContract{
					ContentHash: "test-content-hash",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusFailed(models.StatusFailed{
					Error: "some error",
				})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000002",
						GroupId:       "fsc-local-limit",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000002",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					}, {
						Id:            "01899c62-eba5-7b58-b68d-000000000003",
						GroupId:       "fsc-local-limit",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000002",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "01899c62-eba5-7b58-b68d-000000000003",
		},
		"happy_flow_when_filtering_id": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000002",
						GroupID:       "fsc-local-filter-id",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
						Actor: &auditlog.NewActorUserArgs{
							Name:  "test-user",
							Email: "test@example.com",
						},
						Event: &auditlog.NewEventCreateContractArgs{
							ContractHash: "test-content-hash",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status: &auditlog.NewStatusFailedArgs{
							Error: "some error",
						},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-filter-id",
				Id:      &[]string{"01899c62-eba5-7b58-b68d-000000000002"},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorUser(models.ActorUser{
					Name:  "test-user",
					Email: "test@example.com",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreateContract(models.EventCreateContract{
					ContentHash: "test-content-hash",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusFailed(models.StatusFailed{
					Error: "some error",
				})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000002",
						GroupId:       "fsc-local-filter-id",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000002",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"happy_flow_when_filtering_ids": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000003",
						GroupID:       "fsc-local-filter-ids",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
						Actor: &auditlog.NewActorUserArgs{
							Name:  "test-user",
							Email: "test@example.com",
						},
						Event: &auditlog.NewEventCreateContractArgs{
							ContractHash: "test-content-hash",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status: &auditlog.NewStatusFailedArgs{
							Error: "some error",
						},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-filter-ids",
				Id: &[]string{
					"01899c62-eba5-7b58-b68d-000000000003",
					"01899c62-eba5-7b58-b68d-000000000999",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorUser(models.ActorUser{
					Name:  "test-user",
					Email: "test@example.com",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreateContract(models.EventCreateContract{
					ContentHash: "test-content-hash",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusFailed(models.StatusFailed{
					Error: "some error",
				})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000003",
						GroupId:       "fsc-local-filter-ids",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000002",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"happy_flow_when_filtering_correlation_id": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000004",
						GroupID:       "fsc-local-filter-c-id",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000004",
						Actor: &auditlog.NewActorUserArgs{
							Name:  "test-user",
							Email: "test@example.com",
						},
						Event: &auditlog.NewEventCreateContractArgs{
							ContractHash: "test-content-hash",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status: &auditlog.NewStatusFailedArgs{
							Error: "some error",
						},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-filter-c-id",
				CorrelationId: &[]string{
					"01899c62-eba5-7b58-b68d-000000000004",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorUser(models.ActorUser{
					Name:  "test-user",
					Email: "test@example.com",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreateContract(models.EventCreateContract{
					ContentHash: "test-content-hash",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusFailed(models.StatusFailed{
					Error: "some error",
				})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000004",
						GroupId:       "fsc-local-filter-c-id",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000004",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"happy_flow_when_filtering_service_name": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000005",
						GroupID:       "fsc-local-filter-service-name",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
						Actor: &auditlog.NewActorUserArgs{
							Name:  "test-user",
							Email: "test@example.com",
						},
						Event: &auditlog.NewEventCreateServiceArgs{
							ServiceName: "test-service-filter",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status: &auditlog.NewStatusFailedArgs{
							Error: "some error",
						},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId:     "fsc-local-filter-service-name",
				ServiceName: &[]string{"test-service-filter"},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorUser(models.ActorUser{
					Name:  "test-user",
					Email: "test@example.com",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreateService(models.EventCreateService{
					ServiceName: "test-service-filter",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusFailed(models.StatusFailed{
					Error: "some error",
				})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000005",
						GroupId:       "fsc-local-filter-service-name",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000002",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "01899c62-eba5-7b58-b68d-000000000005",
		},
		"happy_flow_when_filtering_content_hash": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000006",
						GroupID:       "fsc-local-filter-hash",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000004",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventAcceptContractArgs{
							ContractHash: "test-content-hash-filter",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-filter-hash",
				ContentHash: &[]string{
					"test-content-hash-filter",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventAcceptContract(models.EventAcceptContract{
					ContentHash: "test-content-hash-filter",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000006",
						GroupId:       "fsc-local-filter-hash",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000004",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "01899c62-eba5-7b58-b68d-000000000006",
		},
		"create_peer_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000007",
						GroupID:       "fsc-local-create-peer-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventCreatePeerArgs{
							PeerID: "12345678901234567890",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-create-peer-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000007",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreatePeer(models.EventCreatePeer{
					PeerId: "12345678901234567890",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000007",
						GroupId:       "fsc-local-create-peer-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"update_peer_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000008",
						GroupID:       "fsc-local-update-peer-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventUpdatePeerArgs{
							PeerID: "12345678901234567890",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-update-peer-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000008",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventUpdatePeer(models.EventUpdatePeer{
					PeerId: "12345678901234567890",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000008",
						GroupId:       "fsc-local-update-peer-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"delete_peer_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000009",
						GroupID:       "fsc-local-delete-peer-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventDeletePeerArgs{
							PeerID: "12345678901234567890",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-delete-peer-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000009",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventDeletePeer(models.EventDeletePeer{
					PeerId: "12345678901234567890",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000009",
						GroupId:       "fsc-local-delete-peer-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"create_contract_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000010",
						GroupID:       "fsc-local-create-contract-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventCreateContractArgs{
							ContractHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-create-contract-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000010",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreateContract(models.EventCreateContract{
					ContentHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000010",
						GroupId:       "fsc-local-create-contract-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"accept_contract_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000011",
						GroupID:       "fsc-local-accept-contract-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventAcceptContractArgs{
							ContractHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-accept-contract-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000011",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventAcceptContract(models.EventAcceptContract{
					ContentHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000011",
						GroupId:       "fsc-local-accept-contract-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"revoke_contract_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000012",
						GroupID:       "fsc-local-revoke-contract-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventRevokeContractArgs{
							ContractHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-revoke-contract-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000012",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventRevokeContract(models.EventRevokeContract{
					ContentHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000012",
						GroupId:       "fsc-local-revoke-contract-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"reject_contract_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000013",
						GroupID:       "fsc-local-reject-contract-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventRejectContractArgs{
							ContractHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-reject-contract-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000013",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventRejectContract(models.EventRejectContract{
					ContentHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000013",
						GroupId:       "fsc-local-reject-contract-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"create_service_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000014",
						GroupID:       "fsc-local-create-service-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventCreateServiceArgs{
							ServiceName: "test-service",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-create-service-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000014",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreateService(models.EventCreateService{
					ServiceName: "test-service",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000014",
						GroupId:       "fsc-local-create-service-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"update_service_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000015",
						GroupID:       "fsc-local-update-service-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventUpdateServiceArgs{
							ServiceName: "test-service",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-update-service-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000015",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventUpdateService(models.EventUpdateService{
					ServiceName: "test-service",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000015",
						GroupId:       "fsc-local-update-service-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"delete_service_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000016",
						GroupID:       "fsc-local-delete-service-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventDeleteServiceArgs{
							ServiceName: "test-service",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentController,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-delete-service-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000016",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventDeleteService(models.EventDeleteService{
					ServiceName: "test-service",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000016",
						GroupId:       "fsc-local-delete-service-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTCONTROLLER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"login_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000017",
						GroupID:       "fsc-local-login-event",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventLoginArgs{},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentController,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-login-event",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000017",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventLogin(models.EventLogin{})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000017",
						GroupId:       "fsc-local-login-event",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTCONTROLLER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
		"failed_distribution_retry_event": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000018",
						GroupID:       "fsc-local-failed-distribution-retry",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000005",
						Actor: &auditlog.NewActorServiceArgs{
							SubjectCommonName:   "test-service",
							PublicKeyThumbprint: "test-public-key-fingerprint",
						},
						Event: &auditlog.NewEventFailedDistributionRetryArgs{
							ContentHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
							Action:      string(auditlog.FailedDistributionActionSubmitContract),
							PeerID:      "12345678901234567890",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status:    &auditlog.NewStatusSucceededArgs{},
						Component: auditlog.ComponentController,
						CreatedAt: nowInUTC,
					},
				},
			},
			params: &models.GetRecordsParams{
				GroupId: "fsc-local-failed-distribution-retry",
				Id: &[]models.Uuid{
					"01899c62-eba5-7b58-b68d-000000000018",
				},
			},
			wantStatusCode: http.StatusOK,
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorService(models.ActorService{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				})

				event := models.Record_Event{}

				_ = event.FromEventFailedDistributionRetry(models.EventFailedDistributionRetry{
					Action:      models.SUBMITCONTRACT,
					ContentHash: "$1$1$vG1uNi_850TFasbkf9xafr-kdrzh1zzvA3Tm2ZsMefEtjzMDTxZc3Im9XSeKuJCwMp1W9DbbIyvbjKOWEkVvoA",
					PeerId:      "12345678901234567890",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusSucceeded(models.StatusSucceeded{})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000018",
						GroupId:       "fsc-local-failed-distribution-retry",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000005",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTCONTROLLER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantResponseNextCursor: "",
		},
	}

	server, app := newService(t.Name())

	t.Cleanup(func() {
		server.Close()
	})

	for name, tt := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			client, err := createAuditlogClient(server.URL, orgB.CertBundle)
			assert.NoError(t, err)

			err = app.Commands.CreateRecords.Handle(context.Background(), tt.records)
			require.NoError(t, err)

			res, err := client.GetRecordsWithResponse(context.Background(), tt.params)
			assert.NoError(t, err)

			if !assert.Equal(t, tt.wantStatusCode, res.StatusCode()) {
				t.Errorf("response body: %s", res.Body)
			}

			if tt.wantStatusCode != http.StatusOK {
				return
			}

			assert.NotNil(t, res.JSON200)
			assert.Len(t, res.JSON200.Records, len(tt.wantResponseRecords))
			assert.Equal(t, tt.wantResponseRecords, res.JSON200.Records)
			assert.Equal(t, tt.wantResponseNextCursor, res.JSON200.Pagination.NextCursor)
		})
	}
}
