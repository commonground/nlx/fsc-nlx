// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package service_test

import (
	"context"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/server"
)

//nolint:funlen,gocyclo,dupl // this is a test
func TestUnsetUnderReview(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		records                 *command.CreateRecordsArgs
		txIDsToUnsetUnderReview []string
		wantBody                string
		wantStatusCode          int
		wantResponseRecords     []models.Record
	}{
		"happy_flow": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000002",
						GroupID:       "fsc-local",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
						Actor: &auditlog.NewActorUserArgs{
							Name:  "test-user",
							Email: "test@example.com",
						},
						Event: &auditlog.NewEventCreateContractArgs{
							ContractHash: "test-content-hash",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status: &auditlog.NewStatusFailedArgs{
							Error: "some error",
						},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			txIDsToUnsetUnderReview: []string{"01899c62-eba5-7b58-b68d-000000000002"},
			wantResponseRecords: func() []models.Record {
				actor := models.Record_Actor{}

				_ = actor.FromActorUser(models.ActorUser{
					Name:  "test-user",
					Email: "test@example.com",
				})

				event := models.Record_Event{}

				_ = event.FromEventCreateContract(models.EventCreateContract{
					ContentHash: "test-content-hash",
				})

				source := models.Record_Source{}

				_ = source.FromSourceHttp(models.SourceHttp{
					IpAddress: "127.0.0.1",
					UserAgent: "firefox mozilla",
				})

				status := models.Record_Status{}

				_ = status.FromStatusFailed(models.StatusFailed{
					Error: "some error",
				})

				return []models.Record{
					{
						Id:            "01899c62-eba5-7b58-b68d-000000000002",
						GroupId:       "fsc-local",
						CorrelationId: "01899c62-eba5-7b58-b68d-000000000002",
						Actor:         actor,
						Event:         event,
						Source:        source,
						Status:        status,
						Component:     models.COMPONENTMANAGER,
						CreatedAt:     testClock.Now().Unix(),
					},
				}
			}(),
			wantStatusCode: http.StatusNoContent,
		},
		"invalid_transaction_id": {
			records: &command.CreateRecordsArgs{
				Records: auditlog.NewRecordsArgs{
					&auditlog.NewRecordArgs{
						ID:            "01899c62-eba5-7b58-b68d-000000000003",
						GroupID:       "fsc-local",
						CorrelationID: "01899c62-eba5-7b58-b68d-000000000002",
						Actor: &auditlog.NewActorUserArgs{
							Name:  "test-user",
							Email: "test@example.com",
						},
						Event: &auditlog.NewEventCreateContractArgs{
							ContractHash: "test-content-hash",
						},
						Source: &auditlog.NewSourceHTTPArgs{
							IPAddress: "127.0.0.1",
							UserAgent: "firefox mozilla",
						},
						Status: &auditlog.NewStatusFailedArgs{
							Error: "some error",
						},
						Component: auditlog.ComponentManager,
						CreatedAt: nowInUTC,
					},
				},
			},
			txIDsToUnsetUnderReview: []string{"invalid"},
			wantResponseRecords:     []models.Record{},
			wantStatusCode:          http.StatusInternalServerError,
			wantBody:                "{\"details\":\"could not unset records under review: could not parse ids: invalid ID in IDs: invalid ID: invalid id: uuid: incorrect UUID length 7 in string \\\"invalid\\\"\",\"instance\":\"\",\"status\":500,\"title\":\"internal server error\",\"type\":\"\"}\n",
		},
	}

	for name, tt := range testCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			server, app := newService(t.Name())
			t.Cleanup(func() { server.Close() })

			client, err := createAuditlogClient(server.URL, orgB.CertBundle)
			assert.NoError(t, err)

			err = app.Commands.CreateRecords.Handle(context.Background(), tt.records)
			require.NoError(t, err)

			for _, record := range tt.records.Records {
				resSet, errSetUnderReview := client.SetRecordsUnderReview(context.Background(), models.SetRecordsUnderReviewJSONRequestBody{
					Ids: []models.Uuid{record.ID}},
				)
				require.NoError(t, errSetUnderReview)
				resSet.Body.Close()
			}

			res, err := client.UnsetRecordsUnderReview(context.Background(), models.UnsetRecordsUnderReviewJSONRequestBody{
				Ids: tt.txIDsToUnsetUnderReview,
			})
			require.NoError(t, err)
			defer res.Body.Close()

			if tt.wantStatusCode == http.StatusNoContent {
				var res *api.GetRecordsResponse

				res, err = client.GetRecordsWithResponse(context.Background(), &models.GetRecordsParams{
					GroupId: "fsc-local",
				})
				assert.NoError(t, err)

				assert.NotNil(t, res.JSON200)
				assert.Len(t, res.JSON200.Records, len(tt.wantResponseRecords))
				assert.Equal(t, tt.wantResponseRecords, res.JSON200.Records)
			} else {
				var bodyBytes []byte
				bodyBytes, err = io.ReadAll(res.Body)

				require.NoError(t, err)
				assert.Equal(t, tt.wantBody, string(bodyBytes))
			}
		})
	}
}
