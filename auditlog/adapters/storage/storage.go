// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package storage

import (
	"context"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
)

type Storage interface {
	CreateRecords(ctx context.Context, records auditlog.Records) error
	ListRecords(ctx context.Context, req *ListRecordsRequest) (auditlog.Records, error)
	DeleteRecords(ctx context.Context, olderThan time.Time) error
	SetRecordsUnderReview(ctx context.Context, req *SetRecordsUnderReviewRequest) error
	UnsetRecordsUnderReview(ctx context.Context, ids auditlog.IDs) error
	Ping(ctx context.Context) error
}

type SortOrder uint32

const (
	SortOrderUnspecified SortOrder = iota
	SortOrderAscending
	SortOrderDescending
)

type Pagination struct {
	StartID   auditlog.ID
	Limit     uint32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}

type Filters struct {
	Period         *Period
	IDs            auditlog.IDs
	CorrelationIDs []auditlog.CorrelationID
	ContentHashes  []string
	ServiceNames   []string
	UnderReview    bool
}

type ListRecordsRequest struct {
	GroupID    string
	Pagination *Pagination
	Filters    *Filters
}

type SetRecordsUnderReviewRequest struct {
	UnderReviewAt time.Time
	IDs           auditlog.IDs
}
