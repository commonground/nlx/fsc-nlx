// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
)

func (r *Postgres) SetRecordsUnderReview(ctx context.Context, args *storage.SetRecordsUnderReviewRequest) error {
	err := r.queries.SetRecordsUnderReview(ctx, &queries.SetRecordsUnderReviewParams{
		UnderReviewAt: pgtype.Timestamptz{
			Valid: true,
			Time:  args.UnderReviewAt,
		},
		Ids: args.IDs.Values(),
	})
	if err != nil {
		return fmt.Errorf("could not set records under review in database: %w", err)
	}

	return nil
}

func (r *Postgres) UnsetRecordsUnderReview(ctx context.Context, ids auditlog.IDs) error {
	err := r.queries.UnsetRecordsUnderReview(ctx, ids.Values())
	if err != nil {
		return fmt.Errorf("could not unset records under review in database: %w", err)
	}

	return nil
}
