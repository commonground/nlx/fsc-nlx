// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
)

type sortOrder string

const (
	sortOrderAsc  sortOrder = "asc"
	sortOrderDesc sortOrder = "desc"
)

func (r *Postgres) ListRecords(ctx context.Context, req *storage.ListRecordsRequest) (auditlog.Records, error) {
	dbRecords, err := r.queries.ListRecords(ctx, listRecordsReqToDB(req))
	if err != nil {
		return nil, err
	}

	records := make(auditlog.Records, 0, len(dbRecords))

	for _, row := range dbRecords {
		var underReviewAt *time.Time

		if row.UnderReviewAt.Valid {
			t := row.UnderReviewAt.Time
			underReviewAt = &t
		}

		rec, errNewRecord := auditlog.NewRecord(&auditlog.NewRecordArgs{
			ID:            row.ID.String(),
			GroupID:       row.GroupID,
			CorrelationID: row.CorrelationID.String(),
			Actor:         actorToModelArgs(row),
			Event:         eventToModelArgs(row),
			Source:        sourceToModelArgs(row),
			Status:        statusToModelArgs(row),
			Component:     componentToModel(row.Component),
			UnderReviewAt: underReviewAt,
			CreatedAt:     row.CreatedAt.Time,
		})
		if errNewRecord != nil {
			return nil, fmt.Errorf("invalid record in DB: %w", errNewRecord)
		}

		records = append(records, rec)
	}

	return records, nil
}

//nolint:gocyclo // would be not more readable when splitting this function
func listRecordsReqToDB(req *storage.ListRecordsRequest) *queries.ListRecordsParams {
	ids := req.Filters.IDs.Values()

	correlationIDs := make([]uuid.UUID, 0, len(req.Filters.CorrelationIDs))

	if len(req.Filters.CorrelationIDs) > 0 {
		for _, id := range req.Filters.CorrelationIDs {
			correlationIDs = append(correlationIDs, uuid.UUID(id))
		}
	}

	var (
		periodStart pgtype.Timestamp
		periodEnd   pgtype.Timestamp
	)

	if req.Filters.Period != nil {
		if !req.Filters.Period.Start.IsZero() {
			periodStart = pgtype.Timestamp{
				Time:  req.Filters.Period.Start,
				Valid: true,
			}
		}

		if !req.Filters.Period.End.IsZero() {
			periodEnd = pgtype.Timestamp{
				Time:  req.Filters.Period.End,
				Valid: true,
			}
		}
	}

	if len(ids) > 0 {
		return &queries.ListRecordsParams{
			Limit:          int32(len(req.Filters.IDs)),
			GroupID:        req.GroupID,
			Ids:            ids,
			CorrelationIds: []uuid.UUID{},
			ContentHashes:  []string{},
			ServiceNames:   []string{},
			OrderDirection: string(sortOrderToDB(req.Pagination.SortOrder)),
			UnderReview:    req.Filters.UnderReview,
		}
	}

	if len(correlationIDs) > 0 {
		return &queries.ListRecordsParams{
			Limit:          int32(len(req.Filters.CorrelationIDs)),
			GroupID:        req.GroupID,
			Ids:            []uuid.UUID{},
			CorrelationIds: correlationIDs,
			ContentHashes:  []string{},
			ServiceNames:   []string{},
			OrderDirection: string(sortOrderToDB(req.Pagination.SortOrder)),
			UnderReview:    req.Filters.UnderReview,
		}
	}

	contentHashes := []string{}
	serviceNames := []string{}

	if len(req.Filters.ContentHashes) > 0 {
		contentHashes = req.Filters.ContentHashes
	}

	if len(req.Filters.ServiceNames) > 0 {
		serviceNames = req.Filters.ServiceNames
	}

	return &queries.ListRecordsParams{
		GroupID:        req.GroupID,
		StartID:        uuid.UUID(req.Pagination.StartID),
		Limit:          int32(req.Pagination.Limit),
		OrderDirection: string(sortOrderToDB(req.Pagination.SortOrder)),
		Ids:            []uuid.UUID{},
		CorrelationIds: []uuid.UUID{},
		ContentHashes:  contentHashes,
		ServiceNames:   serviceNames,
		PeriodStart:    periodStart,
		PeriodEnd:      periodEnd,
		UnderReview:    req.Filters.UnderReview,
	}
}

func sortOrderToDB(d storage.SortOrder) sortOrder {
	switch d {
	case storage.SortOrderAscending:
		return sortOrderAsc
	case storage.SortOrderDescending:
		return sortOrderDesc
	default:
		return sortOrderAsc
	}
}

func componentToModel(c queries.AuditlogComponent) auditlog.Component {
	switch c {
	case queries.AuditlogComponentController:
		return auditlog.ComponentController
	case queries.AuditlogComponentManager:
		return auditlog.ComponentManager
	default:
		return auditlog.ComponentUnspecified
	}
}

func actorToModelArgs(q *queries.ListRecordsRow) auditlog.NewActorArgs {
	switch q.ActorType {
	case queries.AuditlogActorUnknown:
		return &auditlog.NewActorUnknownArgs{}
	case queries.AuditlogActorUser:
		return &auditlog.NewActorUserArgs{
			Name:  *q.ActorUserName,
			Email: *q.ActorUserEmail,
		}
	case queries.AuditlogActorService:
		return &auditlog.NewActorServiceArgs{
			SubjectCommonName:   *q.ActorServiceSubjectCommonName,
			PublicKeyThumbprint: *q.ActorServicePublicKeyThumbprint,
		}
	default:
		return nil
	}
}

func eventToModelArgs(q *queries.ListRecordsRow) auditlog.NewEventArgs {
	switch q.EventType {
	case queries.AuditlogEventLogin:
		return &auditlog.NewEventLoginArgs{}

	case queries.AuditlogEventCreateContract:
		return &auditlog.NewEventCreateContractArgs{
			ContractHash: *q.EventContractContentHash,
		}
	case queries.AuditlogEventAcceptContract:
		return &auditlog.NewEventAcceptContractArgs{
			ContractHash: *q.EventContractContentHash,
		}
	case queries.AuditlogEventRejectContract:
		return &auditlog.NewEventRejectContractArgs{
			ContractHash: *q.EventContractContentHash,
		}
	case queries.AuditlogEventRevokeContract:
		return &auditlog.NewEventRevokeContractArgs{
			ContractHash: *q.EventContractContentHash,
		}

	case queries.AuditlogEventCreateService:
		return &auditlog.NewEventCreateServiceArgs{
			ServiceName: *q.EventServiceName,
		}
	case queries.AuditlogEventUpdateService:
		return &auditlog.NewEventUpdateServiceArgs{
			ServiceName: *q.EventServiceName,
		}
	case queries.AuditlogEventDeleteService:
		return &auditlog.NewEventDeleteServiceArgs{
			ServiceName: *q.EventServiceName,
		}

	case queries.AuditlogEventFailedDistributionRetry:
		return &auditlog.NewEventFailedDistributionRetryArgs{
			ContentHash: *q.EventFailedDistributionRetryContentHash,
			Action:      string(q.EventFailedDistributionRetryAction.AuditlogFailedDistributionRetryAction),
			PeerID:      *q.EventFailedDistributionRetryPeerID,
		}

	case queries.AuditlogEventCreatePeer:
		return &auditlog.NewEventCreatePeerArgs{
			PeerID: *q.EventPeerID,
		}
	case queries.AuditlogEventSynchronizePeers:
		return &auditlog.NewEventSynchronizePeersArgs{}

	case queries.AuditlogEventUpdatePeer:
		return &auditlog.NewEventUpdatePeerArgs{
			PeerID: *q.EventPeerID,
		}

	case queries.AuditlogEventDeletePeer:
		return &auditlog.NewEventDeletePeerArgs{
			PeerID: *q.EventPeerID,
		}
	default:
		return nil
	}
}

func sourceToModelArgs(q *queries.ListRecordsRow) auditlog.NewSourceArgs {
	switch q.SourceType {
	case queries.AuditlogSourceHttp:
		return &auditlog.NewSourceHTTPArgs{
			IPAddress: q.SourceHttpIpAddress.String(),
			UserAgent: *q.SourceHttpUserAgent,
		}
	default:
		return nil
	}
}

func statusToModelArgs(q *queries.ListRecordsRow) auditlog.NewStatusArgs {
	switch q.StatusType {
	case queries.AuditlogStatusInitiated:
		return &auditlog.NewStatusInitiatedArgs{}
	case queries.AuditlogStatusFailed:
		return &auditlog.NewStatusFailedArgs{
			Error: *q.StatusError,
		}
	case queries.AuditlogStatusUnauthorized:
		return &auditlog.NewStatusUnauthorizedArgs{
			Error: *q.StatusError,
		}
	case queries.AuditlogStatusSucceeded:
		return &auditlog.NewStatusSucceededArgs{}
	default:
		return nil
	}
}
