-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListRecords :many
SELECT
    id,
    group_id,
    correlation_id,

    -- Actor
    actor_type,
    actor_user_name,
    actor_user_email,
    actor_service_subject_common_name,
    actor_service_public_key_thumbprint,

    -- Event
    event_type,
    event_contract_content_hash,
    event_service_name,
    event_failed_distribution_retry_content_hash,
    event_failed_distribution_retry_peer_id,
    event_failed_distribution_retry_action,
    event_peer_id,

    -- Source
    source_type,
    source_http_ip_address,
    source_http_user_agent,

    -- Status
    status_type,
    status_error,

    component,
    under_review_at,
    created_at
FROM auditlog.records as r
WHERE
    r.group_id = @group_id::text
    AND (cardinality(@ids::uuid[]) = 0 OR r.id = ANY(@ids::uuid[]))
    AND (cardinality(@correlation_ids::uuid[]) = 0 OR r.correlation_id = ANY(@correlation_ids::uuid[]))
    AND (cardinality(@content_hashes::text[]) = 0 OR r.event_contract_content_hash = ANY(@content_hashes::text[]) OR r.event_failed_distribution_retry_content_hash = ANY(@content_hashes::text[]))
    AND (cardinality(@service_names::text[]) = 0 OR r.event_service_name = ANY(@service_names::text[]))
    AND (sqlc.narg('period_start')::timestamp IS NULL OR r.created_at >= sqlc.narg('period_start')::timestamp)
    AND (sqlc.narg('period_end')::timestamp IS NULL OR r.created_at <= sqlc.narg('period_end')::timestamp)
    AND (@under_review::bool = false OR r.under_review_at IS NOT NULL)
    AND (@start_id::uuid = '00000000-0000-0000-0000-000000000000'
        OR (@order_direction::text = 'asc' AND r.id > @start_id::uuid)
        OR (@order_direction::text = 'desc' AND r.id < @start_id::uuid)
    )
ORDER BY
    CASE
        WHEN @order_direction::text = 'asc' THEN r.id END ASC,
    CASE
        WHEN @order_direction::text = 'desc' THEN r.id END DESC
LIMIT $1;
