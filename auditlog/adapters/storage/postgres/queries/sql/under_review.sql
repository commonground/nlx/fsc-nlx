-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: SetRecordsUnderReview :exec
UPDATE auditlog.records
SET under_review_at = $1
WHERE id = ANY(@ids::uuid[]);

-- name: UnsetRecordsUnderReview :exec
UPDATE auditlog.records
SET under_review_at = NULL
WHERE id = ANY(@ids::uuid[]);
