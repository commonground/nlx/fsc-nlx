-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: DeleteRecords :exec
DELETE FROM auditlog.records
WHERE
    created_at < @older_than::timestamp
    AND under_review_at IS NULL;
