-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: CreateRecords :copyfrom
INSERT INTO auditlog.records (
    id,
    group_id,
    correlation_id,

    -- Actor
    actor_type,
    actor_user_name,
    actor_user_email,
    actor_service_subject_common_name,
    actor_service_public_key_thumbprint,

    -- Event
    event_type,
    event_contract_content_hash,
    event_service_name,
    event_failed_distribution_retry_content_hash,
    event_failed_distribution_retry_peer_id,
    event_failed_distribution_retry_action,
    event_peer_id,

    -- Source
    source_type,
    source_http_ip_address,
    source_http_user_agent,

    -- Status
    status_type,
    status_error,

    component,
    created_at
) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22);
