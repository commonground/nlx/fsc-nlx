// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0

package queries

import (
	"database/sql/driver"
	"fmt"
	"net/netip"

	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v5/pgtype"
)

type AuditlogActor string

const (
	AuditlogActorUnknown AuditlogActor = "unknown"
	AuditlogActorUser    AuditlogActor = "user"
	AuditlogActorService AuditlogActor = "service"
)

func (e *AuditlogActor) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = AuditlogActor(s)
	case string:
		*e = AuditlogActor(s)
	default:
		return fmt.Errorf("unsupported scan type for AuditlogActor: %T", src)
	}
	return nil
}

type NullAuditlogActor struct {
	AuditlogActor AuditlogActor
	Valid         bool // Valid is true if AuditlogActor is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullAuditlogActor) Scan(value interface{}) error {
	if value == nil {
		ns.AuditlogActor, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.AuditlogActor.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullAuditlogActor) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return string(ns.AuditlogActor), nil
}

func (e AuditlogActor) Valid() bool {
	switch e {
	case AuditlogActorUnknown,
		AuditlogActorUser,
		AuditlogActorService:
		return true
	}
	return false
}

type AuditlogComponent string

const (
	AuditlogComponentController AuditlogComponent = "controller"
	AuditlogComponentManager    AuditlogComponent = "manager"
)

func (e *AuditlogComponent) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = AuditlogComponent(s)
	case string:
		*e = AuditlogComponent(s)
	default:
		return fmt.Errorf("unsupported scan type for AuditlogComponent: %T", src)
	}
	return nil
}

type NullAuditlogComponent struct {
	AuditlogComponent AuditlogComponent
	Valid             bool // Valid is true if AuditlogComponent is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullAuditlogComponent) Scan(value interface{}) error {
	if value == nil {
		ns.AuditlogComponent, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.AuditlogComponent.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullAuditlogComponent) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return string(ns.AuditlogComponent), nil
}

func (e AuditlogComponent) Valid() bool {
	switch e {
	case AuditlogComponentController,
		AuditlogComponentManager:
		return true
	}
	return false
}

type AuditlogEvent string

const (
	AuditlogEventLogin                   AuditlogEvent = "login"
	AuditlogEventCreateContract          AuditlogEvent = "create_contract"
	AuditlogEventAcceptContract          AuditlogEvent = "accept_contract"
	AuditlogEventRevokeContract          AuditlogEvent = "revoke_contract"
	AuditlogEventRejectContract          AuditlogEvent = "reject_contract"
	AuditlogEventCreateService           AuditlogEvent = "create_service"
	AuditlogEventUpdateService           AuditlogEvent = "update_service"
	AuditlogEventDeleteService           AuditlogEvent = "delete_service"
	AuditlogEventFailedDistributionRetry AuditlogEvent = "failed_distribution_retry"
	AuditlogEventCreatePeer              AuditlogEvent = "create_peer"
	AuditlogEventUpdatePeer              AuditlogEvent = "update_peer"
	AuditlogEventDeletePeer              AuditlogEvent = "delete_peer"
	AuditlogEventSynchronizePeers        AuditlogEvent = "synchronize_peers"
)

func (e *AuditlogEvent) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = AuditlogEvent(s)
	case string:
		*e = AuditlogEvent(s)
	default:
		return fmt.Errorf("unsupported scan type for AuditlogEvent: %T", src)
	}
	return nil
}

type NullAuditlogEvent struct {
	AuditlogEvent AuditlogEvent
	Valid         bool // Valid is true if AuditlogEvent is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullAuditlogEvent) Scan(value interface{}) error {
	if value == nil {
		ns.AuditlogEvent, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.AuditlogEvent.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullAuditlogEvent) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return string(ns.AuditlogEvent), nil
}

func (e AuditlogEvent) Valid() bool {
	switch e {
	case AuditlogEventLogin,
		AuditlogEventCreateContract,
		AuditlogEventAcceptContract,
		AuditlogEventRevokeContract,
		AuditlogEventRejectContract,
		AuditlogEventCreateService,
		AuditlogEventUpdateService,
		AuditlogEventDeleteService,
		AuditlogEventFailedDistributionRetry,
		AuditlogEventCreatePeer,
		AuditlogEventUpdatePeer,
		AuditlogEventDeletePeer,
		AuditlogEventSynchronizePeers:
		return true
	}
	return false
}

type AuditlogFailedDistributionRetryAction string

const (
	AuditlogFailedDistributionRetryActionSubmitContract        AuditlogFailedDistributionRetryAction = "submit_contract"
	AuditlogFailedDistributionRetryActionSubmitAcceptSignature AuditlogFailedDistributionRetryAction = "submit_accept_signature"
	AuditlogFailedDistributionRetryActionSubmitRejectSignature AuditlogFailedDistributionRetryAction = "submit_reject_signature"
	AuditlogFailedDistributionRetryActionSubmitRevokeSignature AuditlogFailedDistributionRetryAction = "submit_revoke_signature"
)

func (e *AuditlogFailedDistributionRetryAction) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = AuditlogFailedDistributionRetryAction(s)
	case string:
		*e = AuditlogFailedDistributionRetryAction(s)
	default:
		return fmt.Errorf("unsupported scan type for AuditlogFailedDistributionRetryAction: %T", src)
	}
	return nil
}

type NullAuditlogFailedDistributionRetryAction struct {
	AuditlogFailedDistributionRetryAction AuditlogFailedDistributionRetryAction
	Valid                                 bool // Valid is true if AuditlogFailedDistributionRetryAction is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullAuditlogFailedDistributionRetryAction) Scan(value interface{}) error {
	if value == nil {
		ns.AuditlogFailedDistributionRetryAction, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.AuditlogFailedDistributionRetryAction.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullAuditlogFailedDistributionRetryAction) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return string(ns.AuditlogFailedDistributionRetryAction), nil
}

func (e AuditlogFailedDistributionRetryAction) Valid() bool {
	switch e {
	case AuditlogFailedDistributionRetryActionSubmitContract,
		AuditlogFailedDistributionRetryActionSubmitAcceptSignature,
		AuditlogFailedDistributionRetryActionSubmitRejectSignature,
		AuditlogFailedDistributionRetryActionSubmitRevokeSignature:
		return true
	}
	return false
}

type AuditlogSource string

const (
	AuditlogSourceHttp AuditlogSource = "http"
)

func (e *AuditlogSource) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = AuditlogSource(s)
	case string:
		*e = AuditlogSource(s)
	default:
		return fmt.Errorf("unsupported scan type for AuditlogSource: %T", src)
	}
	return nil
}

type NullAuditlogSource struct {
	AuditlogSource AuditlogSource
	Valid          bool // Valid is true if AuditlogSource is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullAuditlogSource) Scan(value interface{}) error {
	if value == nil {
		ns.AuditlogSource, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.AuditlogSource.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullAuditlogSource) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return string(ns.AuditlogSource), nil
}

func (e AuditlogSource) Valid() bool {
	switch e {
	case AuditlogSourceHttp:
		return true
	}
	return false
}

type AuditlogStatus string

const (
	AuditlogStatusInitiated    AuditlogStatus = "initiated"
	AuditlogStatusFailed       AuditlogStatus = "failed"
	AuditlogStatusUnauthorized AuditlogStatus = "unauthorized"
	AuditlogStatusSucceeded    AuditlogStatus = "succeeded"
)

func (e *AuditlogStatus) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = AuditlogStatus(s)
	case string:
		*e = AuditlogStatus(s)
	default:
		return fmt.Errorf("unsupported scan type for AuditlogStatus: %T", src)
	}
	return nil
}

type NullAuditlogStatus struct {
	AuditlogStatus AuditlogStatus
	Valid          bool // Valid is true if AuditlogStatus is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullAuditlogStatus) Scan(value interface{}) error {
	if value == nil {
		ns.AuditlogStatus, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.AuditlogStatus.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullAuditlogStatus) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return string(ns.AuditlogStatus), nil
}

func (e AuditlogStatus) Valid() bool {
	switch e {
	case AuditlogStatusInitiated,
		AuditlogStatusFailed,
		AuditlogStatusUnauthorized,
		AuditlogStatusSucceeded:
		return true
	}
	return false
}

type AuditlogRecord struct {
	ID                                      uuid.UUID
	GroupID                                 string
	CorrelationID                           uuid.UUID
	ActorType                               AuditlogActor
	ActorUserName                           *string
	ActorUserEmail                          *string
	ActorServiceSubjectCommonName           *string
	EventType                               AuditlogEvent
	EventContractContentHash                *string
	EventServiceName                        *string
	SourceType                              AuditlogSource
	SourceHttpIpAddress                     *netip.Addr
	SourceHttpUserAgent                     *string
	StatusType                              AuditlogStatus
	StatusError                             *string
	Component                               AuditlogComponent
	CreatedAt                               pgtype.Timestamptz
	EventFailedDistributionRetryAction      NullAuditlogFailedDistributionRetryAction
	EventFailedDistributionRetryContentHash *string
	EventFailedDistributionRetryPeerID      *string
	UnderReviewAt                           pgtype.Timestamptz
	ActorServicePublicKeyThumbprint         *string
	EventPeerID                             *string
}
