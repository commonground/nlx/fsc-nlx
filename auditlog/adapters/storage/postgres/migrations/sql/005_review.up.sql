-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN TRANSACTION;

ALTER TABLE auditlog.records
ADD COLUMN under_review_at timestamptz NULL;

-- Create function that aborts query when somenone tries to change the auditlog records
CREATE OR REPLACE FUNCTION abort_tf() RETURNS trigger LANGUAGE plpgsql AS
$$
BEGIN
    OLD.under_review_at := NEW.under_review_at; -- allow modifying the under review at column

    IF OLD.* IS DISTINCT FROM NEW.* THEN
        RAISE EXCEPTION 'Updating records in the auditlog is not allowed';
    END IF;

    RETURN OLD;
END;
$$;

COMMIT;
