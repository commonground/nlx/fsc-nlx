-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL


BEGIN transaction;

ALTER TYPE auditlog.event ADD VALUE 'failed_distribution_retry';

COMMIT;
