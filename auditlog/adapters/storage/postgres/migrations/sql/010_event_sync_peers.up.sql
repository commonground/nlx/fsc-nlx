-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN TRANSACTION;

ALTER TYPE auditlog.event ADD VALUE 'synchronize_peers';

COMMIT;
