-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN TRANSACTION;

ALTER TYPE auditlog.event ADD VALUE 'create_peer';
ALTER TYPE auditlog.event ADD VALUE 'update_peer';
ALTER TYPE auditlog.event ADD VALUE 'delete_peer';

COMMIT;
