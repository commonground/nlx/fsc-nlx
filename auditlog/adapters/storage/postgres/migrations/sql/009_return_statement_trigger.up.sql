-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN TRANSACTION;

-- Create function that aborts query when somenone tries to delete auditlog records newer than retention period
CREATE OR REPLACE FUNCTION abort_when_delete_tf() RETURNS trigger LANGUAGE plpgsql AS
$$
BEGIN
    IF OLD.created_at > NOW() - interval '30 days' THEN
        RAISE EXCEPTION 'Deleting records newer than 30 days in the auditlog is not allowed';
    END IF;

    RETURN OLD;
END;
$$;

COMMIT;
