-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL


BEGIN transaction;

CREATE SCHEMA auditlog;

CREATE TYPE auditlog.actor AS ENUM ('unknown', 'user', 'service');

CREATE TYPE auditlog.event AS ENUM (
    'login',

    'create_contract',
    'accept_contract',
    'revoke_contract',
    'reject_contract',

    'create_service',
    'update_service',
    'delete_service'
);

CREATE TYPE auditlog.source AS ENUM ('http');

CREATE TYPE auditlog.status AS ENUM ('initiated', 'failed', 'unauthorized', 'succeeded');

CREATE TYPE auditlog.component AS ENUM ('controller', 'manager');

CREATE TABLE auditlog.records (
    id uuid NOT NULL, -- UUID v7
    group_id text NOT NULL,
    correlation_id uuid NOT NULL, -- UUID v7

    -- Actor
    actor_type auditlog.actor NOT NULL,

    -- Actor user
    actor_user_name text,
    actor_user_email text,


    -- Actor service
    actor_service_name text,

    -- Event
    event_type auditlog.event NOT NULL,

    event_contract_content_hash text,

    event_service_name text,

    -- Source
    source_type auditlog.source NOT NULL,

    -- Source http
    source_http_ip_address inet,
    source_http_user_agent text,

    -- Status
    status_type auditlog.status NOT NULL,

    status_error text,

    component auditlog.component NOT NULL,
    created_at timestamptz NOT NULL,

    CONSTRAINT record_pk PRIMARY KEY (id, group_id),

    CONSTRAINT check_group_id CHECK ((char_length(group_id) <= 255)),
    CONSTRAINT check_actor_user_name CHECK ((char_length(actor_user_name) <= 255)),
    CONSTRAINT check_actor_user_email CHECK ((char_length(actor_user_email) <= 255)),
    CONSTRAINT check_actor_service_name CHECK ((char_length(actor_service_name) <= 255)),
    CONSTRAINT check_event_contract_content_hash CHECK ((char_length(event_contract_content_hash) <= 150)),
    CONSTRAINT check_event_service_name CHECK ((char_length(event_service_name) <= 255)),
    CONSTRAINT check_source_http_user_agent CHECK ((char_length(source_http_user_agent) <= 1024)),
    CONSTRAINT check_status_error CHECK ((char_length(status_error) <= 2048)),

    CONSTRAINT check_actor_type CHECK (
        (
            actor_type = 'unknown'
        ) OR
        (
            actor_type = 'user'
            AND actor_user_name IS NOT NULL
            AND actor_user_email IS NOT NULL
        ) OR
        (
            actor_type = 'service'
            AND actor_service_name IS NOT NULL
        )
    ),

    CONSTRAINT check_event_type CHECK (
        (
            event_type = 'login'
        ) OR
        (
            event_type = 'create_contract'
            AND status_type IN (
                'initiated',
                'unauthorized'
            )
            AND event_contract_content_hash IS NULL
        ) OR
        (
            event_type IN (
                'create_contract',
                'accept_contract',
                'revoke_contract',
                'reject_contract'
            ) AND
            event_contract_content_hash IS NOT NULL
        ) OR
        (
            event_type IN (
                'create_service',
                'update_service',
                'delete_service'
            ) AND
            event_service_name IS NOT NULL
        )
    ),

    CONSTRAINT check_source_type CHECK (
        (
            source_type = 'http'
            AND source_http_ip_address IS NOT NULL
            AND source_http_user_agent IS NOT NULL
        )
    ),

    CONSTRAINT check_status_type CHECK (
        (
            status_type IN (
                'initiated',
                'succeeded'
            )
        ) OR
        (
            status_type IN (
                'failed',
                'unauthorized'
            ) AND
            status_error IS NOT NULL
        )
    )
);

CREATE INDEX records_created_at_idx ON auditlog.records USING btree (created_at);
CREATE INDEX records_correlation_id_idx ON auditlog.records USING btree (correlation_id);

-- Create function that aborts query when somenone tries to change the auditlog records
CREATE FUNCTION abort_tf() RETURNS trigger LANGUAGE plpgsql AS
$$
BEGIN
  RAISE EXCEPTION 'Updating or deleting records in the auditlog is not allowed';
END;
$$;

CREATE TRIGGER no_update_or_delete_trigger
BEFORE UPDATE OR DELETE ON auditlog.records
FOR EACH ROW EXECUTE FUNCTION abort_tf();

COMMIT;
