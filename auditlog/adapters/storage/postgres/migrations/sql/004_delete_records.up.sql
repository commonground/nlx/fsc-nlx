-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN TRANSACTION;

-- Create function that aborts query when somenone tries to change the auditlog records
CREATE OR REPLACE FUNCTION abort_tf() RETURNS trigger LANGUAGE plpgsql AS
$$
BEGIN
  RAISE EXCEPTION 'Updating records in the auditlog is not allowed';
END;
$$;

DROP TRIGGER no_update_or_delete_trigger on auditlog.records;

CREATE TRIGGER no_update_trigger
BEFORE UPDATE ON auditlog.records
FOR EACH ROW EXECUTE FUNCTION abort_tf();

-- Create function that aborts query when somenone tries to delete auditlog records newer than retention period
CREATE FUNCTION abort_when_delete_tf() RETURNS trigger LANGUAGE plpgsql AS
$$
BEGIN
    IF OLD.created_at > NOW() - interval '30 days' THEN
        RAISE EXCEPTION 'Deleting records newer than 30 days in the auditlog is not allowed';
    END IF;
END;
$$;

CREATE TRIGGER no_delete_before_retention_period_trigger
BEFORE DELETE ON auditlog.records
FOR EACH ROW EXECUTE FUNCTION abort_when_delete_tf();

COMMIT;
