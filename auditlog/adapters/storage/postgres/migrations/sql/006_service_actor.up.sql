-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

BEGIN TRANSACTION;

ALTER TABLE auditlog.records
RENAME COLUMN actor_service_name TO actor_service_subject_common_name;

ALTER TABLE auditlog.records
RENAME CONSTRAINT check_actor_service_name TO actor_service_subject_common_name;

ALTER TABLE auditlog.records
ADD COLUMN actor_service_public_key_thumbprint text NULL;

ALTER TABLE auditlog.records DROP CONSTRAINT check_actor_type;

ALTER TABLE auditlog.records ADD CONSTRAINT check_actor_type CHECK (
    (
        actor_type = 'unknown'
    ) OR
    (
        actor_type = 'user'
        AND actor_user_name IS NOT NULL
        AND actor_user_email IS NOT NULL
    ) OR
    (
        actor_type = 'service'
        AND actor_service_subject_common_name IS NOT NULL
        AND actor_service_public_key_thumbprint IS NOT NULL
    )
);

ALTER TABLE auditlog.records ADD CONSTRAINT actor_service_public_key_thumbprint CHECK ((char_length(actor_service_public_key_thumbprint) <= 255));

COMMIT;
