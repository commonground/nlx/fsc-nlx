-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL


BEGIN TRANSACTION;


CREATE TYPE auditlog.failed_distribution_retry_action AS ENUM (
    'submit_contract',
    'submit_accept_signature',
    'submit_reject_signature',
    'submit_revoke_signature'
    );

ALTER TABLE auditlog.records ADD COLUMN event_failed_distribution_retry_action auditlog.failed_distribution_retry_action;

ALTER TABLE auditlog.records ADD COLUMN event_failed_distribution_retry_content_hash text;

ALTER TABLE auditlog.records ADD COLUMN event_failed_distribution_retry_peer_id text;

ALTER TABLE auditlog.records DROP CONSTRAINT check_event_type;

ALTER TABLE auditlog.records ADD CONSTRAINT check_event_type CHECK (
    (
        event_type = 'login'
        ) OR
    (
        event_type IN (
                       'create_contract',
                       'accept_contract',
                       'revoke_contract',
                       'reject_contract'
            ) AND
        event_contract_content_hash IS NOT NULL
        ) OR
    (
        event_type IN (
                       'create_service',
                       'update_service',
                       'delete_service'
            ) AND
        event_service_name IS NOT NULL
        ) OR
    (
        event_type IN (
            'failed_distribution_retry'
            ) AND
        event_failed_distribution_retry_content_hash IS NOT NULL
            AND event_failed_distribution_retry_peer_id IS NOT NULL
            AND event_failed_distribution_retry_action IS NOT NULL
        )
    );

ALTER TABLE auditlog.records ADD CONSTRAINT event_failed_distribution_content_hash CHECK ((char_length(event_failed_distribution_retry_content_hash) <= 150));

ALTER TABLE auditlog.records ADD CONSTRAINT event_failed_distribution_retry_peer_id CHECK ((char_length(event_failed_distribution_retry_peer_id) <= 20));

COMMIT;
