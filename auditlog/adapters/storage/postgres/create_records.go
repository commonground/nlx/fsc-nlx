// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v5/pgtype"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
)

func (r *Postgres) CreateRecords(ctx context.Context, records auditlog.Records) error {
	dbRecords := make([]*queries.CreateRecordsParams, 0, len(records))

	for _, r := range records {
		component, err := mapComponent(r.Component())
		if err != nil {
			return fmt.Errorf("invalid component: %w", err)
		}

		rec := &queries.CreateRecordsParams{
			ID:            uuid.UUID(r.ID().Bytes()),
			GroupID:       r.GroupID(),
			CorrelationID: uuid.UUID(r.CorrelationID().Bytes()),
			Component:     component,
			CreatedAt: pgtype.Timestamptz{
				Time:  r.CreatedAt(),
				Valid: true,
			},
		}

		err = mapActor(rec, r.Actor())
		if err != nil {
			return fmt.Errorf("invalid actor: %w", err)
		}

		err = mapEvent(rec, r.Event())
		if err != nil {
			return fmt.Errorf("invalid event: %w", err)
		}

		err = mapSource(rec, r.Source())
		if err != nil {
			return fmt.Errorf("invalid source: %w", err)
		}

		err = mapStatus(rec, r.Status())
		if err != nil {
			return fmt.Errorf("invalid status: %w", err)
		}

		dbRecords = append(dbRecords, rec)
	}

	_, err := r.queries.CreateRecords(ctx, dbRecords)
	if err != nil {
		return fmt.Errorf("could not create records in database: %w", err)
	}

	return nil
}

func mapActor(q *queries.CreateRecordsParams, a auditlog.Actor) error {
	switch t := a.(type) {
	case *auditlog.ActorUnknown:
		q.ActorType = queries.AuditlogActorUnknown
	case *auditlog.ActorUser:
		q.ActorType = queries.AuditlogActorUser
		q.ActorUserName = stringPointer(t.Name())
		q.ActorUserEmail = stringPointer(t.Email())
	case *auditlog.ActorService:
		q.ActorType = queries.AuditlogActorService
		q.ActorServiceSubjectCommonName = stringPointer(t.SubjectCommonName())
		q.ActorServicePublicKeyThumbprint = stringPointer(t.PublicKeyThumbprint())
	default:
		return fmt.Errorf("invalid actor type: %T", t)
	}

	return nil
}

func mapEvent(q *queries.CreateRecordsParams, e auditlog.Event) error {
	switch t := e.(type) {
	case *auditlog.EventLogin:
		q.EventType = queries.AuditlogEventLogin

	case *auditlog.EventCreateContract:
		q.EventType = queries.AuditlogEventCreateContract
		q.EventContractContentHash = stringPointer(t.ContractHash())
	case *auditlog.EventAcceptContract:
		q.EventType = queries.AuditlogEventAcceptContract
		q.EventContractContentHash = stringPointer(t.ContractHash())
	case *auditlog.EventRejectContract:
		q.EventType = queries.AuditlogEventRejectContract
		q.EventContractContentHash = stringPointer(t.ContractHash())
	case *auditlog.EventRevokeContract:
		q.EventType = queries.AuditlogEventRevokeContract
		q.EventContractContentHash = stringPointer(t.ContractHash())

	case *auditlog.EventCreateService:
		q.EventType = queries.AuditlogEventCreateService
		q.EventServiceName = stringPointer(t.ServiceName())
	case *auditlog.EventUpdateService:
		q.EventType = queries.AuditlogEventUpdateService
		q.EventServiceName = stringPointer(t.ServiceName())
	case *auditlog.EventDeleteService:
		q.EventType = queries.AuditlogEventDeleteService
		q.EventServiceName = stringPointer(t.ServiceName())

	case *auditlog.EventFailedDistributionRetry:
		q.EventType = queries.AuditlogEventFailedDistributionRetry

		action, errMapAction := mapFailedDistributionRetryAction(t.Action())
		if errMapAction != nil {
			return fmt.Errorf("unknown failed distribution retry action: %s", errMapAction)
		}

		q.EventFailedDistributionRetryAction = queries.NullAuditlogFailedDistributionRetryAction{
			AuditlogFailedDistributionRetryAction: action,
			Valid:                                 true,
		}
		q.EventFailedDistributionRetryPeerID = stringPointer(t.PeerID())
		q.EventFailedDistributionRetryContentHash = stringPointer(t.ContentHash())

	case *auditlog.EventCreatePeer:
		q.EventType = queries.AuditlogEventCreatePeer
		q.EventPeerID = stringPointer(t.PeerID())

	case *auditlog.EventUpdatePeer:
		q.EventType = queries.AuditlogEventUpdatePeer
		q.EventPeerID = stringPointer(t.PeerID())

	case *auditlog.EventSynchronizePeers:
		q.EventType = queries.AuditlogEventSynchronizePeers

	case *auditlog.EventDeletePeer:
		q.EventType = queries.AuditlogEventDeletePeer
		q.EventPeerID = stringPointer(t.PeerID())

	default:
		return fmt.Errorf("invalid event type: %T", t)
	}

	return nil
}

func mapFailedDistributionRetryAction(a auditlog.FailedDistributionAction) (queries.AuditlogFailedDistributionRetryAction, error) {
	switch a {
	case auditlog.FailedDistributionActionSubmitContract:
		return queries.AuditlogFailedDistributionRetryActionSubmitContract, nil
	case auditlog.FailedDistributionActionSubmitAcceptSignature:
		return queries.AuditlogFailedDistributionRetryActionSubmitAcceptSignature, nil
	case auditlog.FailedDistributionActionSubmitRevokeSignature:
		return queries.AuditlogFailedDistributionRetryActionSubmitRevokeSignature, nil
	case auditlog.FailedDistributionActionSubmitRejectSignature:
		return queries.AuditlogFailedDistributionRetryActionSubmitRejectSignature, nil
	default:
		return "", fmt.Errorf("invalid failed distribution action: %s", a)
	}
}

func mapSource(q *queries.CreateRecordsParams, s auditlog.Source) error {
	switch t := s.(type) {
	case *auditlog.SourceHTTP:
		q.SourceType = queries.AuditlogSourceHttp

		i := t.IPAddress()
		q.SourceHttpIpAddress = &i
		q.SourceHttpUserAgent = stringPointer(t.UserAgent())
	default:
		return fmt.Errorf("invalid source type: %T", t)
	}

	return nil
}

func mapStatus(q *queries.CreateRecordsParams, s auditlog.Status) error {
	switch t := s.(type) {
	case *auditlog.StatusInitiated:
		q.StatusType = queries.AuditlogStatusInitiated
	case *auditlog.StatusUnauthorized:
		q.StatusType = queries.AuditlogStatusUnauthorized
		q.StatusError = stringPointer(t.Error())
	case *auditlog.StatusFailed:
		q.StatusType = queries.AuditlogStatusFailed
		q.StatusError = stringPointer(t.Error())
	case *auditlog.StatusSucceeded:
		q.StatusType = queries.AuditlogStatusSucceeded
	default:
		return fmt.Errorf("invalid status type: %T", t)
	}

	return nil
}

func mapComponent(c auditlog.Component) (queries.AuditlogComponent, error) {
	switch c {
	case auditlog.ComponentController:
		return queries.AuditlogComponentController, nil
	case auditlog.ComponentManager:
		return queries.AuditlogComponentManager, nil
	default:
		return "", fmt.Errorf("invalid component: %s", c)
	}
}

func stringPointer(s string) *string {
	return &s
}
