// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5/pgtype"
)

func (r *Postgres) DeleteRecords(ctx context.Context, olderThan time.Time) error {
	if olderThan.IsZero() {
		return fmt.Errorf("olderThan is required")
	}

	err := r.queries.DeleteRecords(ctx, pgtype.Timestamp{
		Valid: true,
		Time:  olderThan,
	})
	if err != nil {
		return fmt.Errorf("could not delete records in database: %w", err)
	}

	return nil
}
