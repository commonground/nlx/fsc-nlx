// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package main

import (
	"log"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/cmd"
	common_cmd "gitlab.com/commonground/nlx/fsc-nlx/common/cmd"
)

func main() {
	err := common_cmd.SetupFlagsForEnvironment(cmd.RootCmd)
	if err != nil {
		log.Fatalf("error parsing flags: %v", err)
	}

	if err := cmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
