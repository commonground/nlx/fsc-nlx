// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import "fmt"

const (
	maxActorUserNameLength  = 255
	maxActorUserEmailLength = 255
)

type NewActorUserArgs struct {
	Name  string
	Email string
}

type ActorUser struct {
	name  string
	email string
}

func newActorUser(args *NewActorUserArgs) (*ActorUser, error) {
	if args.Name == "" {
		return nil, fmt.Errorf("name cannot be empty")
	}

	if len(args.Name) > maxActorUserNameLength {
		return nil, fmt.Errorf("name cannot be longer than %d characters", maxActorUserNameLength)
	}

	if args.Email == "" {
		return nil, fmt.Errorf("email cannot be empty")
	}

	if len(args.Email) > maxActorUserEmailLength {
		return nil, fmt.Errorf("email cannot be longer than %d characters", maxActorUserEmailLength)
	}

	return &ActorUser{
		name:  args.Name,
		email: args.Email,
	}, nil
}

func (a ActorUser) Name() string {
	return a.name
}

func (a ActorUser) Email() string {
	return a.email
}
