// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

type NewEventCreatePeerArgs struct {
	PeerID string
}

type EventCreatePeer struct {
	peerID string
}

func newEventCreatePeer(a *NewEventCreatePeerArgs) *EventCreatePeer {
	return &EventCreatePeer{
		peerID: a.PeerID,
	}
}

type NewEventUpdatePeerArgs struct {
	PeerID string
}

type EventUpdatePeer struct {
	peerID string
}

func newEventUpdatePeer(a *NewEventUpdatePeerArgs) *EventUpdatePeer {
	return &EventUpdatePeer{
		peerID: a.PeerID,
	}
}

type NewEventDeletePeerArgs struct {
	PeerID string
}

type EventDeletePeer struct {
	peerID string
}

func newEventDeletePeer(a *NewEventDeletePeerArgs) *EventDeletePeer {
	return &EventDeletePeer{
		peerID: a.PeerID,
	}
}

func (e *EventCreatePeer) PeerID() string {
	return e.peerID
}

func (e *EventUpdatePeer) PeerID() string {
	return e.peerID
}

func (e *EventDeletePeer) PeerID() string {
	return e.peerID
}

type EventSynchronizePeers struct{}

type NewEventSynchronizePeersArgs struct{}

func newEventSynchronizePeers(_ *NewEventSynchronizePeersArgs) *EventSynchronizePeers {
	return &EventSynchronizePeers{}
}
