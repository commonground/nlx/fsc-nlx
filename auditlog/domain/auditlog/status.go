// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import "fmt"

const (
	maxStatusErrorLength = 2048
)

type Status any

type NewStatusArgs any

func newStatus(args NewStatusArgs) (Status, error) {
	switch a := args.(type) {
	case *NewStatusInitiatedArgs:
		return newStatusInitiated(a)
	case *NewStatusFailedArgs:
		return newStatusFailed(a)
	case *NewStatusUnauthorizedArgs:
		return newStatusUnauthorized(a)
	case *NewStatusSucceededArgs:
		return newStatusSucceeded(a)
	default:
		return nil, fmt.Errorf("invalid status type: %T", a)
	}
}

type StatusInitiated struct{}

type NewStatusInitiatedArgs struct{}

func newStatusInitiated(_ *NewStatusInitiatedArgs) (*StatusInitiated, error) {
	return &StatusInitiated{}, nil
}

type StatusFailed struct {
	err string
}

type NewStatusFailedArgs struct {
	Error string
}

func newStatusFailed(args *NewStatusFailedArgs) (*StatusFailed, error) {
	if args.Error == "" {
		return nil, fmt.Errorf("error cannot be empty")
	}

	if len(args.Error) > maxStatusErrorLength {
		return nil, fmt.Errorf("error cannot be longer than %d characters", maxStatusErrorLength)
	}

	return &StatusFailed{
		err: args.Error,
	}, nil
}

func (s *StatusFailed) Error() string {
	return s.err
}

type StatusUnauthorized struct {
	err string
}

type NewStatusUnauthorizedArgs struct {
	Error string
}

func newStatusUnauthorized(args *NewStatusUnauthorizedArgs) (*StatusUnauthorized, error) {
	if args.Error == "" {
		return nil, fmt.Errorf("error cannot be empty")
	}

	if len(args.Error) > maxStatusErrorLength {
		return nil, fmt.Errorf("error cannot be longer than %d characters", maxStatusErrorLength)
	}

	return &StatusUnauthorized{
		err: args.Error,
	}, nil
}

func (s *StatusUnauthorized) Error() string {
	return s.err
}

type StatusSucceeded struct{}

type NewStatusSucceededArgs struct{}

func newStatusSucceeded(_ *NewStatusSucceededArgs) (*StatusSucceeded, error) {
	return &StatusSucceeded{}, nil
}
