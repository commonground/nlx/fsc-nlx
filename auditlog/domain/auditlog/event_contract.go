// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import "fmt"

const (
	maxContractHashLength = 150
)

type NewEventCreateContractArgs struct {
	ContractHash string
}

type EventCreateContract struct {
	contractHash string
}

func newEventCreateContract(a *NewEventCreateContractArgs) (*EventCreateContract, error) {
	err := validateOptionalContractHash(a.ContractHash)
	if err != nil {
		return nil, err
	}

	return &EventCreateContract{
		contractHash: a.ContractHash,
	}, nil
}

type NewEventAcceptContractArgs struct {
	ContractHash string
}

type EventAcceptContract struct {
	contractHash string
}

func newEventAcceptContract(a *NewEventAcceptContractArgs) (*EventAcceptContract, error) {
	err := validateContractHash(a.ContractHash)
	if err != nil {
		return nil, err
	}

	return &EventAcceptContract{
		contractHash: a.ContractHash,
	}, nil
}

type NewEventRejectContractArgs struct {
	ContractHash string
}

type EventRejectContract struct {
	contractHash string
}

func newEventRejectContract(a *NewEventRejectContractArgs) (*EventRejectContract, error) {
	err := validateContractHash(a.ContractHash)
	if err != nil {
		return nil, err
	}

	return &EventRejectContract{
		contractHash: a.ContractHash,
	}, nil
}

type NewEventRevokeContractArgs struct {
	ContractHash string
}

type EventRevokeContract struct {
	contractHash string
}

func newEventRevokeContract(a *NewEventRevokeContractArgs) (*EventRevokeContract, error) {
	err := validateContractHash(a.ContractHash)
	if err != nil {
		return nil, err
	}

	return &EventRevokeContract{
		contractHash: a.ContractHash,
	}, nil
}

func validateOptionalContractHash(h string) error {
	if len(h) > maxContractHashLength {
		return fmt.Errorf("contractHash cannot be longer than %d", maxContractHashLength)
	}

	return nil
}

func validateContractHash(h string) error {
	if h == "" {
		return fmt.Errorf("contractHash cannot be empty")
	}

	return validateOptionalContractHash(h)
}

func (e *EventCreateContract) ContractHash() string {
	return e.contractHash
}

func (e *EventAcceptContract) ContractHash() string {
	return e.contractHash
}

func (e *EventRejectContract) ContractHash() string {
	return e.contractHash
}

func (e *EventRevokeContract) ContractHash() string {
	return e.contractHash
}
