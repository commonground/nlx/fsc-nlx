// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import "fmt"

type Source any

type NewSourceArgs any

func newSource(args NewSourceArgs) (Source, error) {
	switch a := args.(type) {
	case *NewSourceHTTPArgs:
		return newSourceHTTP(a)
	default:
		return nil, fmt.Errorf("invalid source type: %T", a)
	}
}
