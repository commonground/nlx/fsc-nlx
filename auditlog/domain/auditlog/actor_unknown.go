// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

type NewActorUnknownArgs struct{}

type ActorUnknown struct{}

func newActorUnknown(_ *NewActorUnknownArgs) (*ActorUnknown, error) {
	return &ActorUnknown{}, nil
}
