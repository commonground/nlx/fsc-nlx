// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import (
	"slices"
)

type Component string

const (
	ComponentUnspecified Component = ""
	ComponentController  Component = "controller"
	ComponentManager     Component = "manager"
)

var validComponents = []string{string(ComponentController), string(ComponentManager)}

func (c Component) Valid() bool {
	return slices.Contains[[]string](validComponents, string(c))
}
