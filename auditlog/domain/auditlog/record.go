// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import (
	"fmt"
	"time"

	"github.com/gofrs/uuid"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type Records []*Record

type NewRecordsArgs []*NewRecordArgs

type Record struct {
	id            ID
	groupID       GroupID
	correlationID CorrelationID
	actor         Actor
	event         Event
	source        Source
	status        Status
	component     Component
	underReviewAt *time.Time
	createdAt     time.Time
}

type IDs []ID
type ID uuid.UUID

type CorrelationID uuid.UUID
type GroupID string

type NewRecordArgs struct {
	ID            string
	GroupID       string
	CorrelationID string
	Actor         NewActorArgs
	Event         NewEventArgs
	Source        NewSourceArgs
	Status        NewStatusArgs
	Component     Component
	UnderReviewAt *time.Time
	CreatedAt     time.Time
}

func NewRecords(args []*NewRecordArgs) (Records, error) {
	records := make(Records, 0, len(args))

	for _, a := range args {
		r, err := NewRecord(a)
		if err != nil {
			return nil, fmt.Errorf("invalid record: %w", err)
		}

		records = append(records, r)
	}

	return records, nil
}

//nolint:gocyclo // cannot be shorter without degrading readability
func NewRecord(args *NewRecordArgs) (*Record, error) {
	id, err := NewID(args.ID)
	if err != nil {
		return nil, fmt.Errorf("invalid ID: %w", err)
	}

	groupID, err := contract.NewGroupID(args.GroupID)
	if err != nil {
		return nil, fmt.Errorf("invalid groupID: %w", err)
	}

	correlationID, err := NewCorrelationID(args.CorrelationID)
	if err != nil {
		return nil, fmt.Errorf("invalid CorrelationID: %w", err)
	}

	actor, err := newActor(args.Actor)
	if err != nil {
		return nil, fmt.Errorf("invalid actor: %w", err)
	}

	event, err := newEvent(args.Component, args.Event)
	if err != nil {
		return nil, fmt.Errorf("invalid event: %w", err)
	}

	source, err := newSource(args.Source)
	if err != nil {
		return nil, fmt.Errorf("invalid source: %w", err)
	}

	status, err := newStatus(args.Status)
	if err != nil {
		return nil, fmt.Errorf("invalid status: %w", err)
	}

	if !args.Component.Valid() {
		return nil, fmt.Errorf("invalid component %q", args.Component)
	}

	if args.CreatedAt.IsZero() {
		return nil, fmt.Errorf("createdAt cannot be %q", time.Time{})
	}

	var underReviewAt *time.Time

	if args.UnderReviewAt != nil && args.UnderReviewAt.IsZero() {
		return nil, fmt.Errorf("underReviewAt cannot be %q", time.Time{})
	}

	if args.UnderReviewAt != nil {
		t := args.UnderReviewAt.UTC().Truncate(time.Second)
		underReviewAt = &t
	}

	return &Record{
		id:            id,
		groupID:       GroupID(groupID),
		correlationID: correlationID,
		actor:         actor,
		event:         event,
		source:        source,
		status:        status,
		component:     args.Component,
		underReviewAt: underReviewAt,
		createdAt:     args.CreatedAt.UTC().Truncate(time.Second),
	}, nil
}

func newUUID(i string) (uuid.UUID, error) {
	id, err := uuid.FromString(i)
	if err != nil {
		return uuid.UUID{}, fmt.Errorf("invalid id: %w", err)
	}

	if id.Version() != uuid.V7 {
		return uuid.UUID{}, fmt.Errorf("id must be uuid v7 but is v%d", id.Version())
	}

	return id, nil
}

func NewID(i string) (ID, error) {
	id, err := newUUID(i)
	if err != nil {
		return ID{}, fmt.Errorf("invalid ID: %w", err)
	}

	return ID(id), nil
}

func NewIDs(ids []string) (IDs, error) {
	auditlogIDs := make(IDs, 0, len(ids))

	for _, i := range ids {
		id, err := NewID(i)
		if err != nil {
			return nil, fmt.Errorf("invalid ID in IDs: %w", err)
		}

		auditlogIDs = append(auditlogIDs, id)
	}

	return auditlogIDs, nil
}

func (ids IDs) Values() []uuid.UUID {
	uuids := make([]uuid.UUID, 0, len(ids))

	for _, id := range ids {
		uuids = append(uuids, uuid.UUID(id))
	}

	return uuids
}

func NewCorrelationID(i string) (CorrelationID, error) {
	id, err := newUUID(i)
	if err != nil {
		return CorrelationID{}, fmt.Errorf("invalid CorrelationID: %w", err)
	}

	return CorrelationID(id), nil
}

func (r *Record) ID() ID {
	return r.id
}

func (i ID) String() string {
	return uuid.UUID(i).String()
}

func (i ID) Bytes() []byte {
	return uuid.UUID(i).Bytes()
}

func (r *Record) CorrelationID() CorrelationID {
	return r.correlationID
}

func (c CorrelationID) String() string {
	return uuid.UUID(c).String()
}

func (c CorrelationID) Bytes() []byte {
	return uuid.UUID(c).Bytes()
}

func (r *Record) GroupID() string {
	return string(r.groupID)
}

func (r *Record) Actor() Actor {
	return r.actor
}

func (r *Record) Event() Event {
	return r.event
}

func (r *Record) Source() Source {
	return r.source
}

func (r *Record) Status() Status {
	return r.status
}

func (r *Record) Component() Component {
	return r.component
}

func (r *Record) UnderReviewAt() *time.Time {
	return r.underReviewAt
}

func (r *Record) CreatedAt() time.Time {
	return r.createdAt
}
