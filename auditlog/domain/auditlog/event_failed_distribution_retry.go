// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import (
	"fmt"
)

type FailedDistributionAction string

const (
	FailedDistributionActionSubmitContract        FailedDistributionAction = "submit_contract"
	FailedDistributionActionSubmitAcceptSignature FailedDistributionAction = "submit_accept_signature"
	FailedDistributionActionSubmitRevokeSignature FailedDistributionAction = "submit_revoke_signature"
	FailedDistributionActionSubmitRejectSignature FailedDistributionAction = "submit_reject_signature"
)

type EventFailedDistributionRetry struct {
	contentHash string
	peerID      string
	action      FailedDistributionAction
}

type NewEventFailedDistributionRetryArgs struct {
	ContentHash string
	Action      string
	PeerID      string
}

func newEventFailedDistributionRetry(args *NewEventFailedDistributionRetryArgs) (*EventFailedDistributionRetry, error) {
	err := validateContractHash(args.ContentHash)
	if err != nil {
		return nil, err
	}

	var action FailedDistributionAction

	switch args.Action {
	case string(FailedDistributionActionSubmitContract):
		action = FailedDistributionActionSubmitContract
	case string(FailedDistributionActionSubmitAcceptSignature):
		action = FailedDistributionActionSubmitAcceptSignature
	case string(FailedDistributionActionSubmitRevokeSignature):
		action = FailedDistributionActionSubmitRevokeSignature
	case string(FailedDistributionActionSubmitRejectSignature):
		action = FailedDistributionActionSubmitRejectSignature
	default:
		return nil, fmt.Errorf("invalid action: %s", args.Action)
	}

	return &EventFailedDistributionRetry{
		contentHash: args.ContentHash,
		action:      action,
		peerID:      args.PeerID,
	}, nil
}

func (e *EventFailedDistributionRetry) Action() FailedDistributionAction {
	return e.action
}

func (e *EventFailedDistributionRetry) ContentHash() string {
	return e.contentHash
}

func (e *EventFailedDistributionRetry) PeerID() string {
	return e.peerID
}
