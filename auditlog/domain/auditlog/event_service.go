// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import "fmt"

const (
	maxServiceNameLength = 160
)

type NewEventCreateServiceArgs struct {
	ServiceName string
}

type EventCreateService struct {
	serviceName string
}

func newEventCreateService(a *NewEventCreateServiceArgs) (*EventCreateService, error) {
	err := validateServiceName(a.ServiceName)
	if err != nil {
		return nil, err
	}

	return &EventCreateService{
		serviceName: a.ServiceName,
	}, nil
}

type NewEventUpdateServiceArgs struct {
	ServiceName string
}

type EventUpdateService struct {
	serviceName string
}

func newEventUpdateService(a *NewEventUpdateServiceArgs) (*EventUpdateService, error) {
	err := validateServiceName(a.ServiceName)
	if err != nil {
		return nil, err
	}

	return &EventUpdateService{
		serviceName: a.ServiceName,
	}, nil
}

type NewEventDeleteServiceArgs struct {
	ServiceName string
}

type EventDeleteService struct {
	serviceName string
}

func newEventDeleteService(a *NewEventDeleteServiceArgs) (*EventDeleteService, error) {
	err := validateServiceName(a.ServiceName)
	if err != nil {
		return nil, err
	}

	return &EventDeleteService{
		serviceName: a.ServiceName,
	}, nil
}

func validateServiceName(h string) error {
	if h == "" {
		return fmt.Errorf("serviceName cannot be empty")
	}

	if len(h) > maxServiceNameLength {
		return fmt.Errorf("serviceName cannot be longer than %d", maxServiceNameLength)
	}

	return nil
}

func (e *EventCreateService) ServiceName() string {
	return e.serviceName
}

func (e *EventUpdateService) ServiceName() string {
	return e.serviceName
}

func (e *EventDeleteService) ServiceName() string {
	return e.serviceName
}
