// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import "fmt"

type NewActorArgs any

type Actor any

func newActor(args NewActorArgs) (Actor, error) {
	switch a := args.(type) {
	case *NewActorUnknownArgs:
		return newActorUnknown(a)
	case *NewActorUserArgs:
		return newActorUser(a)
	case *NewActorServiceArgs:
		return newActorService(a)
	default:
		return nil, fmt.Errorf("invalid actor type: %T", a)
	}
}
