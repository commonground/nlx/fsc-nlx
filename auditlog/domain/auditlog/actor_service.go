// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import "fmt"

const (
	maxActorServiceSubjectCommonNameLength   = 255
	maxActorServicePublicKeyThumbprintLength = 255
)

type NewActorServiceArgs struct {
	SubjectCommonName   string
	PublicKeyThumbprint string
}

type ActorService struct {
	subjectCommonName   string
	publicKeyThumbprint string
}

func newActorService(args *NewActorServiceArgs) (*ActorService, error) {
	if args.SubjectCommonName == "" {
		return nil, fmt.Errorf("subjectCommonName cannot be empty")
	}

	if len(args.SubjectCommonName) > maxActorServiceSubjectCommonNameLength {
		return nil, fmt.Errorf("subjectCommonName cannot be longer than %d characters", maxActorServiceSubjectCommonNameLength)
	}

	if args.PublicKeyThumbprint == "" {
		return nil, fmt.Errorf("publicKeyThumbprint cannot be empty")
	}

	if len(args.PublicKeyThumbprint) > maxActorServicePublicKeyThumbprintLength {
		return nil, fmt.Errorf("publicKeyThumbprint cannot be longer than %d characters", maxActorServicePublicKeyThumbprintLength)
	}

	return &ActorService{
		subjectCommonName:   args.SubjectCommonName,
		publicKeyThumbprint: args.PublicKeyThumbprint,
	}, nil
}

func (a ActorService) SubjectCommonName() string {
	return a.subjectCommonName
}

func (a ActorService) PublicKeyThumbprint() string {
	return a.publicKeyThumbprint
}
