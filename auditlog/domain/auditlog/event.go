// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import "fmt"

type Event any

type NewEventArgs any

//nolint:gocyclo // cannot be shorter without degrading readability
func newEvent(component Component, event NewEventArgs) (Event, error) {
	var (
		err error
		ev  Event
	)

	switch e := event.(type) {
	case *NewEventLoginArgs:
		ev, err = newEventLogin(e)
	case *NewEventCreateContractArgs:
		ev, err = newEventCreateContract(e)
	case *NewEventAcceptContractArgs:
		ev, err = newEventAcceptContract(e)
	case *NewEventRejectContractArgs:
		ev, err = newEventRejectContract(e)
	case *NewEventRevokeContractArgs:
		ev, err = newEventRevokeContract(e)
	case *NewEventCreateServiceArgs:
		ev, err = newEventCreateService(e)
	case *NewEventUpdateServiceArgs:
		ev, err = newEventUpdateService(e)
	case *NewEventDeleteServiceArgs:
		ev, err = newEventDeleteService(e)
	case *NewEventFailedDistributionRetryArgs:
		ev, err = newEventFailedDistributionRetry(e)
	case *NewEventCreatePeerArgs:
		ev = newEventCreatePeer(e)
	case *NewEventUpdatePeerArgs:
		ev = newEventUpdatePeer(e)
	case *NewEventDeletePeerArgs:
		ev = newEventDeletePeer(e)
	case *NewEventSynchronizePeersArgs:
		ev = newEventSynchronizePeers(e)
	default:
		ev, err = nil, fmt.Errorf("invalid event type: %T", e)
	}

	if err != nil {
		return nil, fmt.Errorf("invalid event: %w", err)
	}

	err = validateEventCombination(ev, component)
	if err != nil {
		return nil, fmt.Errorf("invalid event component combination: %w", err)
	}

	return ev, nil
}

type EventLogin struct{}

type NewEventLoginArgs struct{}

func newEventLogin(_ *NewEventLoginArgs) (*EventLogin, error) {
	return &EventLogin{}, nil
}

func validateEventCombination(e Event, c Component) error {
	switch e.(type) {
	case *EventLogin:
	case *EventCreateService:
	case *EventUpdateService:
	case *EventDeleteService:
		if c != ComponentController {
			return fmt.Errorf("invalid combination of component: %q and event: %T", c, e)
		}
	}

	return nil
}
