// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import (
	"fmt"
	"net/netip"
)

const (
	maxUserAgentLength = 1024
)

type NewSourceHTTPArgs struct {
	IPAddress string
	UserAgent string
}

type SourceHTTP struct {
	ipAddress netip.Addr
	userAgent string
}

func newSourceHTTP(args *NewSourceHTTPArgs) (*SourceHTTP, error) {
	if args.IPAddress == "" {
		return nil, fmt.Errorf("ipAddress cannot be empty")
	}

	ip, err := netip.ParseAddr(args.IPAddress)
	if err != nil {
		return nil, fmt.Errorf("invalid ipAddress: %w", err)
	}

	if args.UserAgent == "" {
		return nil, fmt.Errorf("userAgent cannot be empty")
	}

	if len(args.UserAgent) > maxUserAgentLength {
		return nil, fmt.Errorf("userAgent cannot be longer than %d characters", maxUserAgentLength)
	}

	return &SourceHTTP{
		ipAddress: ip,
		userAgent: args.UserAgent,
	}, nil
}

func (s *SourceHTTP) IPAddress() netip.Addr {
	return s.ipAddress
}

func (s *SourceHTTP) UserAgent() string {
	return s.userAgent
}
