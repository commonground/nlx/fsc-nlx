// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
)

//nolint:funlen,gocyclo // this is a test
func TestNewRecord(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	validArgs := &auditlog.NewRecordArgs{
		ID:            "018dea24-afcf-7415-b380-65a2b7e174fc",
		GroupID:       "fsc-local",
		CorrelationID: "018dea24-afcf-7415-b380-65a2b7e174fd",
		Actor: &auditlog.NewActorUserArgs{
			Name:  "test-user",
			Email: "test@example.com",
		},
		Event: &auditlog.NewEventCreateContractArgs{
			ContractHash: "test-contract-hash",
		},
		Source: &auditlog.NewSourceHTTPArgs{
			IPAddress: "127.0.0.1",
			UserAgent: "firefox mozilla",
		},
		Status:    &auditlog.NewStatusInitiatedArgs{},
		Component: auditlog.ComponentController,
		CreatedAt: now,
	}

	type tests map[string]struct {
		args    *auditlog.NewRecordArgs
		wantErr bool
	}

	allTests := tests{
		"when_id_invalid": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.ID = "018dea24-afcf-4415-b380-65a2b7e174fc"

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_group_id_invalid": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.GroupID = "&"

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_correlation_id_invalid": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.CorrelationID = "018dea24-afcf-4415-b380-65a2b7e174fb"

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_event_invalid": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Event = &auditlog.ActorService{}

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_event_missing_details": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Event = &auditlog.EventAcceptContract{}

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_actor_invalid": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Actor = &auditlog.NewEventAcceptContractArgs{}

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_actor_missing_details": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Actor = &auditlog.NewActorUserArgs{}

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_source_invalid": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Source = &auditlog.NewActorUserArgs{}

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_source_missing_details": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Source = &auditlog.NewSourceHTTPArgs{}

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_status_invalid": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Status = &auditlog.NewSourceHTTPArgs{}

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_status_missing_details": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Status = &auditlog.NewStatusFailedArgs{}

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_component_invalid": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Component = auditlog.Component("invalid")

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"when_created_at_invalid": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.CreatedAt = time.Time{}

				return &a
			}(*validArgs),
			wantErr: true,
		},
		"happy_flow": {
			args: validArgs,
		},
		"happy_flow_actor_service": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Actor = &auditlog.NewActorServiceArgs{
					SubjectCommonName:   "test-service",
					PublicKeyThumbprint: "test-public-key-fingerprint",
				}

				return &a
			}(*validArgs),
		},
		"happy_flow_event_login": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Event = &auditlog.NewEventLoginArgs{}

				return &a
			}(*validArgs),
		},
		"happy_flow_event_accept_contract": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Event = &auditlog.NewEventAcceptContractArgs{
					ContractHash: "test-contract-hash",
				}

				return &a
			}(*validArgs),
		},
		"happy_flow_event_revoke_contract": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Event = &auditlog.NewEventRevokeContractArgs{
					ContractHash: "test-contract-hash",
				}

				return &a
			}(*validArgs),
		},
		"happy_flow_event_reject_contract": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Event = &auditlog.NewEventRejectContractArgs{
					ContractHash: "test-contract-hash",
				}

				return &a
			}(*validArgs),
		},
		"happy_flow_event_create_service": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Event = &auditlog.NewEventCreateServiceArgs{
					ServiceName: "test-service",
				}

				return &a
			}(*validArgs),
		},
		"happy_flow_event_update_service": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Event = &auditlog.NewEventUpdateServiceArgs{
					ServiceName: "test-service",
				}

				return &a
			}(*validArgs),
		},
		"happy_flow_event_delete_service": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Event = &auditlog.NewEventDeleteServiceArgs{
					ServiceName: "test-service",
				}

				return &a
			}(*validArgs),
		},
		"happy_flow_status_unauthorized": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Status = &auditlog.NewStatusUnauthorizedArgs{
					Error: "some error",
				}

				return &a
			}(*validArgs),
		},
		"happy_flow_status_failed": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Status = &auditlog.NewStatusFailedArgs{
					Error: "some error",
				}

				return &a
			}(*validArgs),
		},
		"happy_flow_status_succeeded": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Status = &auditlog.NewStatusSucceededArgs{}

				return &a
			}(*validArgs),
		},
		"happy_flow_component_manager": {
			args: func(a auditlog.NewRecordArgs) *auditlog.NewRecordArgs {
				a.Component = auditlog.ComponentManager

				return &a
			}(*validArgs),
		},
	}

	for name, tt := range allTests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			record, err := auditlog.NewRecord(tt.args)

			if tt.wantErr == true {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)

				assert.Equal(t, tt.args.ID, record.ID().String())
				assert.Equal(t, tt.args.GroupID, record.GroupID())
				assert.Equal(t, tt.args.CorrelationID, record.CorrelationID().String())

				switch a := tt.args.Actor.(type) {
				case *auditlog.NewActorUserArgs:
					assert.Equal(t, a.Name, record.Actor().(*auditlog.ActorUser).Name())
					assert.Equal(t, a.Email, record.Actor().(*auditlog.ActorUser).Email())
				case *auditlog.NewActorServiceArgs:
					assert.Equal(t, a.SubjectCommonName, record.Actor().(*auditlog.ActorService).SubjectCommonName())
					assert.Equal(t, a.PublicKeyThumbprint, record.Actor().(*auditlog.ActorService).PublicKeyThumbprint())
				default:
					t.Errorf("invalid newActorArgs: %T", a)
				}

				switch a := tt.args.Event.(type) {
				case *auditlog.NewEventLoginArgs:
					assert.Equal(t, &auditlog.EventLogin{}, record.Event().(*auditlog.EventLogin))

				case *auditlog.NewEventCreateContractArgs:
					assert.Equal(t, a.ContractHash, record.Event().(*auditlog.EventCreateContract).ContractHash())
				case *auditlog.NewEventAcceptContractArgs:
					assert.Equal(t, a.ContractHash, record.Event().(*auditlog.EventAcceptContract).ContractHash())
				case *auditlog.NewEventRejectContractArgs:
					assert.Equal(t, a.ContractHash, record.Event().(*auditlog.EventRejectContract).ContractHash())
				case *auditlog.NewEventRevokeContractArgs:
					assert.Equal(t, a.ContractHash, record.Event().(*auditlog.EventRevokeContract).ContractHash())

				case *auditlog.NewEventCreateServiceArgs:
					assert.Equal(t, a.ServiceName, record.Event().(*auditlog.EventCreateService).ServiceName())
				case *auditlog.NewEventUpdateServiceArgs:
					assert.Equal(t, a.ServiceName, record.Event().(*auditlog.EventUpdateService).ServiceName())
				case *auditlog.NewEventDeleteServiceArgs:
					assert.Equal(t, a.ServiceName, record.Event().(*auditlog.EventDeleteService).ServiceName())
				default:
					t.Errorf("invalid newEventArgs: %T", a)
				}

				switch a := tt.args.Source.(type) {
				case *auditlog.NewSourceHTTPArgs:
					assert.Equal(t, a.IPAddress, record.Source().(*auditlog.SourceHTTP).IPAddress().String())
					assert.Equal(t, a.UserAgent, record.Source().(*auditlog.SourceHTTP).UserAgent())
				default:
					t.Errorf("invalid newSourceArgs: %T", a)
				}

				switch a := tt.args.Status.(type) {
				case *auditlog.NewStatusInitiatedArgs:
					assert.Equal(t, &auditlog.StatusInitiated{}, record.Status().(*auditlog.StatusInitiated))
				case *auditlog.NewStatusUnauthorizedArgs:
					assert.Equal(t, a.Error, record.Status().(*auditlog.StatusUnauthorized).Error())
				case *auditlog.NewStatusFailedArgs:
					assert.Equal(t, a.Error, record.Status().(*auditlog.StatusFailed).Error())
				case *auditlog.NewStatusSucceededArgs:
					assert.Equal(t, &auditlog.StatusSucceeded{}, record.Status().(*auditlog.StatusSucceeded))
				default:
					t.Errorf("invalid newStatusArgs: %T", a)
				}

				assert.Equal(t, tt.args.Component, record.Component())
				assert.Equal(t, tt.args.CreatedAt, record.CreatedAt())
			}
		})
	}
}
