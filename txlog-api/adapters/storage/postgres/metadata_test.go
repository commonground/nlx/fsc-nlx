// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

//go:build integration

package postgresadapter_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

//nolint:errcheck // this is a test
func TestCreateMetadata(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		loadFixtures bool
		txLogRecords []*record.Record
		args         []*metadata.Record
		wantErr      error
	}{
		"happy_flow": {
			loadFixtures: false,
			txLogRecords: mustNewRecords(t, []*record.NewRecordArgs{{
				TransactionID: "01899c62-eba5-7b58-b68d-000000000003",
				GroupID:       "fsc-local",
				GrantHash:     "$1$4$testhash",
				Direction:     record.DirectionIn,
				ServiceName:   "test-service",
				Source: &record.NewRecordSourceArgs{
					OutwayPeerID: "1",
				},
				Destination: &record.NewRecordDelegatedDestinationArgs{
					ServicePeerID:   "3",
					DelegatorPeerID: "4",
				},
				CreatedAt: time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC),
			},
			}),
			args: mustNewMetadataRecords(t, []*metadata.NewMetadataRecordArgs{
				{
					TransactionID: "01899c62-eba5-7b58-b68d-000000000003",
					GroupID:       "fsc-local",
					Direction:     metadata.DirectionIn,
					Metadata:      []byte(`{"key":"value"}`),
				},
			}),
			wantErr: nil,
		},
	}

	for name, tc := range tests {
		tc := tc

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			repo, closeRepo := newRepo(t, tc.loadFixtures)

			defer func() {
				err := closeRepo(context.Background())
				require.NoError(t, err)
			}()

			err := repo.CreateRecords(context.Background(), tc.txLogRecords)
			assert.NoError(t, err)

			err = repo.CreateMetadataRecords(context.Background(), tc.args)
			require.Equal(t, tc.wantErr, err)

			if tc.wantErr == nil {
				assertMetadataRecordsInRepository(t, repo, tc.args)
			}
		})
	}
}

func assertMetadataRecordsInRepository(t *testing.T, repo domain.Repository, wantRecords []*metadata.Record) {
	require.NotNil(t, wantRecords)

	records, err := repo.ListMetadataRecords(context.Background(), wantRecords[0].TransactionID().UUID())
	require.NoError(t, err)

	found := make(map[string]bool)
	for _, r := range records {
		found[r.TransactionID().String()] = true
	}

	for _, r := range wantRecords {
		require.Equal(t, true, found[r.TransactionID().String()])
	}
}

func mustNewMetadataRecords(t *testing.T, args []*metadata.NewMetadataRecordArgs) []*metadata.Record {
	records := make([]*metadata.Record, len(args))

	for i, arg := range args {
		metadataRecord, err := metadata.NewRecord(arg)
		assert.NoError(t, err)

		records[i] = metadataRecord
	}

	return records
}
