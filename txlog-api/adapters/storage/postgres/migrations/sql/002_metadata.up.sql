-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL


BEGIN transaction;

CREATE TABLE transactionlog.metadata(
                                        id uuid NOT NULL,
                                        transaction_id uuid NOT NULL, -- UUID v7
                                        direction transactionlog.direction NOT NULL,
                                        group_id VARCHAR(100) NOT NULL,
                                        metadata jsonb NOT NULL,
                                        CONSTRAINT metadata_pk PRIMARY KEY (id, transaction_id)
);

ALTER TABLE transactionlog.metadata ADD CONSTRAINT transaction_id FOREIGN KEY (transaction_id, direction, group_id)
    REFERENCES transactionlog.records (transaction_id, direction, group_id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT;
