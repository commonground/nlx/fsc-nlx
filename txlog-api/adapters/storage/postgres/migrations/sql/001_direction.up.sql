-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

BEGIN transaction;

CREATE SCHEMA transactionlog;

CREATE TYPE transactionlog.direction AS ENUM (
    'in',
    'out'
);

CREATE TABLE transactionlog.records (
    transaction_id uuid NOT NULL, -- UUID v7
    group_id VARCHAR(100) NOT NULL,
    grant_hash text NOT NULL,
    direction transactionlog.direction NOT NULL,
    service_name text NOT NULL,
    src_outway_peer_id VARCHAR(20),
    delegated_src_outway_peer_id VARCHAR(20),
    delegated_src_delegator_peer_id VARCHAR(20),
    dest_service_peer_id VARCHAR(20),
    delegated_dest_service_peer_id VARCHAR(20),
    delegated_dest_delegator_peer_id VARCHAR(20),
    created_at timestamp with time zone NOT NULL,
    CONSTRAINT records_pk PRIMARY KEY (transaction_id, group_id, direction)
);

COMMIT;
