// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

//go:build integration

package postgresadapter_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

//nolint:errcheck // this is a test
func TestCreateRecord(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		loadFixtures bool
		args         []*record.Record
		wantErr      error
	}{
		"happy_flow_delegated_dest": {
			loadFixtures: false,
			args: mustNewRecords(t, []*record.NewRecordArgs{
				{
					TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
					GroupID:       "fsc-local",
					GrantHash:     "$1$4$testhash",
					Direction:     record.DirectionIn,
					ServiceName:   "test-service",
					Source: &record.NewRecordSourceArgs{
						OutwayPeerID: "1",
					},
					Destination: &record.NewRecordDelegatedDestinationArgs{
						ServicePeerID:   "3",
						DelegatorPeerID: "4",
					},
					CreatedAt: time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC),
				},
			}),
			wantErr: nil,
		},
		"happy_flow_delegated_src": {
			loadFixtures: false,
			args: mustNewRecords(t, []*record.NewRecordArgs{
				{
					TransactionID: "01899c62-eba5-7b58-b68d-000000000002",
					GroupID:       "fsc-local",
					GrantHash:     "$1$4$testhash",
					Direction:     record.DirectionIn,
					ServiceName:   "test-service",
					Source: &record.NewRecordDelegatedSourceArgs{
						OutwayPeerID:    "1",
						DelegatorPeerID: "2",
					},
					Destination: &record.NewRecordDestinationArgs{
						ServicePeerID: "3",
					},
					CreatedAt: time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC),
				},
			}),
			wantErr: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			repo, closeRepo := newRepo(t, tt.loadFixtures)

			defer func() {
				err := closeRepo(context.Background())
				require.NoError(t, err)
			}()

			err := repo.CreateRecords(context.Background(), tt.args)
			require.Equal(t, tt.wantErr, err)

			if tt.wantErr == nil {
				assertRecordsInRepository(t, repo, tt.args)
			}
		})
	}
}

//nolint:errcheck,dupl // this is a test
func TestListRecords(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		loadFixtures bool
		want         []*record.Record
		wantErr      error
	}{
		"happy_flow": {
			loadFixtures: true,
			want: mustNewRecords(t, []*record.NewRecordArgs{
				{
					TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
					GroupID:       "fsc-local",
					GrantHash:     "$1$4$testhash",
					Direction:     record.DirectionIn,
					ServiceName:   "test-service",
					Source: &record.NewRecordSourceArgs{
						OutwayPeerID: "1",
					},
					Destination: &record.NewRecordDelegatedDestinationArgs{
						ServicePeerID:   "3",
						DelegatorPeerID: "4",
					},
					CreatedAt: time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC),
				},
				{
					TransactionID: "01899c62-eba5-7b58-b68d-000000000002",
					GroupID:       "fsc-local",
					GrantHash:     "$1$4$testhash",
					Direction:     record.DirectionIn,
					ServiceName:   "test-service",
					Source: &record.NewRecordDelegatedSourceArgs{
						OutwayPeerID:    "1",
						DelegatorPeerID: "2",
					},
					Destination: &record.NewRecordDestinationArgs{
						ServicePeerID: "3",
					},
					CreatedAt: time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC),
				},
			}),
			wantErr: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			repo, closeRepo := newRepo(t, tt.loadFixtures)

			defer func() {
				err := closeRepo(context.Background())
				require.NoError(t, err)
			}()

			got, err := repo.ListRecords(context.Background(), &record.ListRecordsRequest{
				Pagination: &record.Pagination{
					Limit: 100,
				},
				GroupID: "fsc-local",
			})
			require.Equal(t, tt.wantErr, err)

			if tt.wantErr == nil {
				for i, g := range got {
					w := tt.want[i]

					assert.Equal(t, w.TransactionID(), g.TransactionID())
					assert.Equal(t, w.GroupID(), g.GroupID())
					assert.Equal(t, w.GrantHash(), g.GrantHash())
					assert.Equal(t, w.Direction(), g.Direction())
					assert.Equal(t, w.ServiceName(), g.ServiceName())
					assert.Equal(t, w.Source(), g.Source())
					assert.Equal(t, w.Destination(), g.Destination())
					assert.Equal(t, w.CreatedAt(), g.CreatedAt())
				}
			}
		})
	}
}

func assertRecordsInRepository(t *testing.T, repo record.Repository, wantRecords []*record.Record) {
	require.NotNil(t, wantRecords)

	records, err := repo.ListRecords(context.Background(), &record.ListRecordsRequest{
		Pagination: &record.Pagination{
			Limit: 100,
		},
		GroupID: "fsc-local",
	})
	require.NoError(t, err)

	found := make(map[string]bool)
	for _, r := range records {
		found[r.TransactionID().String()] = true
	}

	for _, r := range wantRecords {
		require.Equal(t, true, found[r.TransactionID().String()])
	}
}

func mustNewRecords(t *testing.T, args []*record.NewRecordArgs) []*record.Record {
	records := make([]*record.Record, len(args))

	for i, r := range args {
		records[i] = mustNewRecord(t, r)
	}

	return records
}

func mustNewRecord(t *testing.T, args *record.NewRecordArgs) *record.Record {
	r, err := record.NewRecord(args)
	assert.NoError(t, err)

	return r
}
