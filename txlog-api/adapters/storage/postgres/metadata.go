// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"errors"
	"fmt"

	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
)

func (r *PostgreSQLRepository) CreateMetadataRecords(ctx context.Context, metadataRecords []*metadata.Record) error {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return err
	}

	defer func() {
		err = tx.Rollback(ctx)
		if err != nil {
			if errors.Is(err, pgx.ErrTxClosed) {
				return
			}

			fmt.Printf("cannot rollback database transaction while creating record: %e", err)
		}
	}()

	qtx := r.queries.WithTx(tx)

	for _, r := range metadataRecords {
		rec := metadataRecordModelToDB(r)

		err = qtx.CreateMetadataRecord(ctx, rec)
		if err != nil {
			return errors.Join(err, errors.New("could not create log record in database"))
		}
	}

	err = tx.Commit(ctx)
	if err != nil {
		return err
	}

	return nil
}

func metadataRecordModelToDB(r *metadata.Record) *queries.CreateMetadataRecordParams {
	return &queries.CreateMetadataRecordParams{
		ID:            r.ID().UUID(),
		TransactionID: r.TransactionID().UUID(),
		Direction:     metadataDirectionModelToDB(r.Direction()),
		GroupID:       r.GroupID(),
		Metadata:      r.Metadata(),
	}
}

func metadataDirectionModelToDB(d metadata.Direction) queries.TransactionlogDirection {
	switch d {
	case metadata.DirectionIn:
		return queries.TransactionlogDirectionIn
	case metadata.DirectionOut:
		return queries.TransactionlogDirectionOut
	default:
		return queries.TransactionlogDirection("")
	}
}

func (r *PostgreSQLRepository) ListMetadataRecords(ctx context.Context, transactionID uuid.UUID) ([]*metadata.Record, error) {
	dbRecords, err := r.queries.ListMetadataRecords(ctx, transactionID)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not retrieve Metadata for transaction: %s", transactionID.String()))
	}

	records := make([]*metadata.Record, len(dbRecords))

	for i, r := range dbRecords {
		record, _ := metadata.NewRecord(&metadata.NewMetadataRecordArgs{
			TransactionID: r.TransactionID.String(),
			GroupID:       r.GroupID,
			Direction:     metadataDirectionDBToModel(r.Direction),
			Metadata:      r.Metadata,
		})
		records[i] = record
	}

	return records, nil
}

func metadataDirectionDBToModel(d queries.TransactionlogDirection) metadata.Direction {
	switch d {
	case queries.TransactionlogDirectionIn:
		return metadata.DirectionIn
	case queries.TransactionlogDirectionOut:
		return metadata.DirectionOut
	default:
		return metadata.DirectionUnspecified
	}
}
