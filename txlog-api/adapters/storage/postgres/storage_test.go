// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

//go:build integration

package postgresadapter_test

import (
	"context"
	"os"
	"sync"
	"testing"
	"time"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v5"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

const (
	dbName             = "test_txlog"
	defaultPostgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
)

var setupOnce sync.Once

func newRepo(t *testing.T, enableFixtures bool) (repo domain.Repository, closeDB func(ctx context.Context) error) {
	setupOnce.Do(func() {
		setupDatabase(t)
	})

	tx := newDB(t)

	repo, err := postgresadapter.New(tx)
	require.NoError(t, err)

	if enableFixtures {
		err = loadFixtures(t, repo)
		require.NoError(t, err)
	}

	return repo, tx.Rollback
}

func setupDatabase(t *testing.T) {
	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = defaultPostgresDSN
	}

	dsn, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	require.NoError(t, err)

	dsnForMigrations := testingutils.AddQueryParamToAddress(dsn, "x-migrations-table", dbName)
	err = postgresadapter.PerformMigrations(dsnForMigrations)
	require.NoError(t, err)
}

func loadFixtures(t *testing.T, repo record.Repository) error {
	newRecordsArgs := []*record.NewRecordArgs{
		{
			TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
			GroupID:       "fsc-local",
			GrantHash:     "$1$4$testhash",
			Direction:     record.DirectionIn,
			ServiceName:   "test-service",
			Source: &record.NewRecordSourceArgs{
				OutwayPeerID: "1",
			},
			Destination: &record.NewRecordDelegatedDestinationArgs{
				ServicePeerID:   "3",
				DelegatorPeerID: "4",
			},
			CreatedAt: time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC),
		},
		{
			TransactionID: "01899c62-eba5-7b58-b68d-000000000002",
			GroupID:       "fsc-local",
			GrantHash:     "$1$4$testhash",
			Direction:     record.DirectionIn,
			ServiceName:   "test-service",
			Source: &record.NewRecordDelegatedSourceArgs{
				OutwayPeerID:    "1",
				DelegatorPeerID: "2",
			},
			Destination: &record.NewRecordDestinationArgs{
				ServicePeerID: "3",
			},
			CreatedAt: time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC),
		},
	}

	recs := make([]*record.Record, len(newRecordsArgs))
	for i, args := range newRecordsArgs {
		var err error
		recs[i], err = record.NewRecord(args)
		require.NoError(t, err)
	}

	err := repo.CreateRecords(context.Background(), recs)
	require.NoError(t, err)

	return nil
}

func newDB(t *testing.T) pgx.Tx {
	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = defaultPostgresDSN
	}

	dsn, err := testingutils.GetDsn(postgresDSN, dbName)
	require.NoError(t, err)

	db, err := pgx.Connect(context.Background(), dsn)
	require.NoError(t, err)

	tx, err := db.Begin(context.Background())
	require.NoError(t, err)

	return tx
}
