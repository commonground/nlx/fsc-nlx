// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package service

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/query"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/logger"
)

type NewApplicationArgs struct {
	Context    context.Context
	Clock      command.Clock
	Logger     logger.Logger
	Repository domain.Repository
}

func NewApplication(args *NewApplicationArgs) (*app.Application, error) {
	createRecordHandler, err := command.NewCreateRecordsHandler(args.Repository, args.Clock, args.Logger)
	if err != nil {
		return nil, err
	}

	createMetadataRecordHandler, err := command.NewCreateMetadataRecordsHandler(args.Repository, args.Clock, args.Logger)
	if err != nil {
		return nil, err
	}

	getHealthHandler, err := query.NewGetHealthHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	application := &app.Application{
		Queries: app.Queries{
			ListRecords:         query.NewListRecordsHandler(args.Repository),
			ListMetadataRecords: query.NewMetadataRecordsHandler(args.Repository),
			GetHealth:           getHealthHandler,
		},
		Commands: app.Commands{
			CreateRecords:         createRecordHandler,
			CreateMetadataRecords: createMetadataRecordHandler,
		},
	}

	return application, nil
}
