// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package service_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
)

//nolint:funlen,dupl // this is a test
func TestListLogRecords(t *testing.T) {
	testCases := map[string]struct {
		LogRecordsToCreate     []*command.NewRecordArgs
		ListLogRecordsArgs     *models.GetLogRecordsParams
		WantStatusCode         int
		WantResponseLogRecords []models.LogRecord
		WantResponsePagination models.PaginationResult
	}{
		"happy_flow": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return []*command.NewRecordArgs{
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
				}
			}(),
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				return &models.GetLogRecordsParams{
					GroupId:        "test-group",
					TransactionIds: &[]string{"01899c62-eba5-7b58-b68d-000000000001"},
				}
			}(),
			WantStatusCode: http.StatusOK,
			WantResponseLogRecords: func() []models.LogRecord {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				source := models.LogRecord_Source{}

				_ = source.FromSource(models.Source{
					OutwayPeerId: "01234567891234567890",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestination(models.Destination{
					ServicePeerId: "01234567891234567891",
				})

				transactionID := uuid.Must(uuid.FromString("01899c62-eba5-7b58-b68d-000000000001"))

				return []models.LogRecord{
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service",
						Source:        source,
						TransactionId: transactionID.String(),
					}}
			}(),
			WantResponsePagination: models.PaginationResult{
				NextCursor: "",
			},
		},
		"when_filtering_by_transaction_id": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return []*command.NewRecordArgs{
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000002",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
				}
			}(),
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				return &models.GetLogRecordsParams{
					GroupId: "test-group",
					TransactionIds: &[]string{
						"01899c62-eba5-7b58-b68d-000000000002",
					},
				}
			}(),
			WantStatusCode: http.StatusOK,
			WantResponseLogRecords: func() []models.LogRecord {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				source := models.LogRecord_Source{}

				_ = source.FromSource(models.Source{
					OutwayPeerId: "01234567891234567890",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestination(models.Destination{
					ServicePeerId: "01234567891234567891",
				})

				transactionID := uuid.Must(uuid.FromString("01899c62-eba5-7b58-b68d-000000000002"))

				return []models.LogRecord{
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service",
						Source:        source,
						TransactionId: transactionID.String(),
					}}
			}(),
			WantResponsePagination: models.PaginationResult{
				NextCursor: "",
			},
		},
		"when_filtering_by_transaction_id_and_peer_id": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return []*command.NewRecordArgs{
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000003",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000004",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567892",
						},
						CreatedAt: nowInUTC,
					},
				}
			}(),
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				return &models.GetLogRecordsParams{
					GroupId: "test-group",
					TransactionIds: &[]string{
						"01899c62-eba5-7b58-b68d-000000000003",
						"01899c62-eba5-7b58-b68d-000000000004",
					},
					PeerId: &[]string{
						"01234567891234567891",
					},
				}
			}(),
			WantStatusCode: http.StatusOK,
			WantResponseLogRecords: func() []models.LogRecord {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				source := models.LogRecord_Source{}

				_ = source.FromSource(models.Source{
					OutwayPeerId: "01234567891234567890",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestination(models.Destination{
					ServicePeerId: "01234567891234567891",
				})

				transactionID := uuid.Must(uuid.FromString("01899c62-eba5-7b58-b68d-000000000003"))

				return []models.LogRecord{
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service",
						Source:        source,
						TransactionId: transactionID.String(),
					}}
			}(),
			WantResponsePagination: models.PaginationResult{
				NextCursor: "",
			},
		},
		"when_filtering_by_transaction_id_and_servicename": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return []*command.NewRecordArgs{
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000005",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000006",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
				}
			}(),
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				return &models.GetLogRecordsParams{
					GroupId: "test-group",
					TransactionIds: &[]string{
						"01899c62-eba5-7b58-b68d-000000000005",
					},
					ServiceName: &[]string{"test-service"},
				}
			}(),
			WantStatusCode: http.StatusOK,
			WantResponseLogRecords: func() []models.LogRecord {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				source := models.LogRecord_Source{}

				_ = source.FromSource(models.Source{
					OutwayPeerId: "01234567891234567890",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestination(models.Destination{
					ServicePeerId: "01234567891234567891",
				})

				transactionID := uuid.Must(uuid.FromString("01899c62-eba5-7b58-b68d-000000000005"))

				return []models.LogRecord{
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service",
						Source:        source,
						TransactionId: transactionID.String(),
					}}
			}(),
			WantResponsePagination: models.PaginationResult{
				NextCursor: "",
			},
		},
		"when_filtering_by_multiple transaction_ids": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return []*command.NewRecordArgs{
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000007",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000008",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
				}
			}(),
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				return &models.GetLogRecordsParams{
					GroupId: "test-group",
					TransactionIds: &[]string{
						"01899c62-eba5-7b58-b68d-000000000007",
						"01899c62-eba5-7b58-b68d-000000000008",
					},
					ServiceName: &[]string{"test-service"},
				}
			}(),
			WantStatusCode: http.StatusOK,
			WantResponseLogRecords: func() []models.LogRecord {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				source := models.LogRecord_Source{}

				_ = source.FromSource(models.Source{
					OutwayPeerId: "01234567891234567890",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestination(models.Destination{
					ServicePeerId: "01234567891234567891",
				})

				return []models.LogRecord{
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service",
						Source:        source,
						TransactionId: "01899c62-eba5-7b58-b68d-000000000007",
					},
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service",
						Source:        source,
						TransactionId: "01899c62-eba5-7b58-b68d-000000000008",
					},
				}
			}(),
			WantResponsePagination: models.PaginationResult{
				NextCursor: "",
			},
		},
		"happy_flow_filtering_pagination": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return []*command.NewRecordArgs{
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000009",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "dummy-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000010",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "dummy-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000011",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "dummy-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
				}
			}(),
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				return &models.GetLogRecordsParams{
					GroupId:     "test-group",
					ServiceName: &[]string{"dummy-service"},
				}
			}(),
			WantStatusCode: http.StatusOK,
			WantResponseLogRecords: func() []models.LogRecord {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				source := models.LogRecord_Source{}

				_ = source.FromSource(models.Source{
					OutwayPeerId: "01234567891234567890",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestination(models.Destination{
					ServicePeerId: "01234567891234567891",
				})

				return []models.LogRecord{
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "dummy-service",
						Source:        source,
						TransactionId: "01899c62-eba5-7b58-b68d-000000000009",
					},
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "dummy-service",
						Source:        source,
						TransactionId: "01899c62-eba5-7b58-b68d-000000000010",
					},
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "dummy-service",
						Source:        source,
						TransactionId: "01899c62-eba5-7b58-b68d-000000000011",
					},
				}
			}(),
			WantResponsePagination: models.PaginationResult{
				NextCursor: "01899c62-eba5-7b58-b68d-000000000011",
			},
		},
		"when_filtering_by_transaction_id_and_distinct_servicenames": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return []*command.NewRecordArgs{
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000012",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service-name-a",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000013",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service-name-b",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567891",
						},
						CreatedAt: nowInUTC,
					},
				}
			}(),
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				return &models.GetLogRecordsParams{
					GroupId: "test-group",
					TransactionIds: &[]string{
						"01899c62-eba5-7b58-b68d-000000000012",
						"01899c62-eba5-7b58-b68d-000000000013",
					},
					ServiceName: &[]string{"test-service-name-a"},
				}
			}(),
			WantStatusCode: http.StatusOK,
			WantResponseLogRecords: func() []models.LogRecord {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				source := models.LogRecord_Source{}

				_ = source.FromSource(models.Source{
					OutwayPeerId: "01234567891234567890",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestination(models.Destination{
					ServicePeerId: "01234567891234567891",
				})

				return []models.LogRecord{
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service-name-a",
						Source:        source,
						TransactionId: "01899c62-eba5-7b58-b68d-000000000012",
					},
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service-name-b",
						Source:        source,
						TransactionId: "01899c62-eba5-7b58-b68d-000000000013",
					},
				}
			}(),
			WantResponsePagination: models.PaginationResult{
				NextCursor: "",
			},
		},
		"happy_flow_delegated_service": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return []*command.NewRecordArgs{
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000014",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordSourceArgs{
							OutwayPeerID: "01234567891234567890",
						},
						Destination: &command.NewRecordDelegatedDestinationArgs{
							ServicePeerID:   "01234567891234567891",
							DelegatorPeerID: "01234567891234567892",
						},
						CreatedAt: nowInUTC,
					},
				}
			}(),
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				return &models.GetLogRecordsParams{
					GroupId: "test-group",
					TransactionIds: &[]string{
						"01899c62-eba5-7b58-b68d-000000000014",
					},
				}
			}(),
			WantStatusCode: http.StatusOK,
			WantResponseLogRecords: func() []models.LogRecord {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				source := models.LogRecord_Source{}

				_ = source.FromSource(models.Source{
					OutwayPeerId: "01234567891234567890",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestinationDelegated(models.DestinationDelegated{
					ServicePeerId:   "01234567891234567891",
					DelegatorPeerId: "01234567891234567892",
				})

				return []models.LogRecord{
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service",
						Source:        source,
						TransactionId: "01899c62-eba5-7b58-b68d-000000000014",
					}}
			}(),
			WantResponsePagination: models.PaginationResult{
				NextCursor: "",
			},
		},
		"happy_flow_requested_on_behalf_of": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return []*command.NewRecordArgs{
					{
						TransactionID: "01899c62-eba5-7b58-b68d-000000000015",
						GroupID:       "test-group",
						GrantHash:     grantHash,
						ServiceName:   "test-service",
						Direction:     record.DirectionIn,
						Source: &command.NewRecordDelegatedSourceArgs{
							DelegatorPeerID: "01234567891234567890",
							OutwayPeerID:    "01234567891234567891",
						},
						Destination: &command.NewRecordDestinationArgs{
							ServicePeerID: "01234567891234567892",
						},
						CreatedAt: nowInUTC,
					},
				}
			}(),
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				return &models.GetLogRecordsParams{
					GroupId: "test-group",
					TransactionIds: &[]string{
						"01899c62-eba5-7b58-b68d-000000000015",
					},
				}
			}(),
			WantStatusCode: http.StatusOK,
			WantResponseLogRecords: func() []models.LogRecord {
				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				source := models.LogRecord_Source{}

				_ = source.FromSourceDelegated(models.SourceDelegated{
					DelegatorPeerId: "01234567891234567890",
					OutwayPeerId:    "01234567891234567891",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestination(models.Destination{
					ServicePeerId: "01234567891234567892",
				})

				return []models.LogRecord{
					{
						CreatedAt:     nowInUTC.Unix(),
						Destination:   destination,
						Direction:     "DIRECTION_INCOMING",
						GrantHash:     grantHash,
						GroupId:       "test-group",
						ServiceName:   "test-service",
						Source:        source,
						TransactionId: "01899c62-eba5-7b58-b68d-000000000015",
					}}
			}(),
			WantResponsePagination: models.PaginationResult{
				NextCursor: "",
			},
		},
		"with_too_large_limit": {
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				limit := models.QueryPaginationLimit(1100)
				return &models.GetLogRecordsParams{
					GroupId: "test-group",
					Limit:   &limit,
				}
			}(),
			WantStatusCode: http.StatusBadRequest,
		},
		"with_too_small_limit": {
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				limit := models.QueryPaginationLimit(0)
				return &models.GetLogRecordsParams{
					GroupId: "test-group",
					Limit:   &limit,
				}
			}(),
			WantStatusCode: http.StatusBadRequest,
		},
		"without_group_id": {
			ListLogRecordsArgs: func() *models.GetLogRecordsParams {
				limit := models.QueryPaginationLimit(0)
				return &models.GetLogRecordsParams{
					Limit: &limit,
				}
			}(),
			WantStatusCode: http.StatusBadRequest,
		},
	}

	txlogServer, app := newService(t.Name())

	defer txlogServer.Close()

	client, err := createTxLogAPIClient(txlogServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			err := app.Commands.CreateRecords.Handle(context.Background(), tc.LogRecordsToCreate)
			require.NoError(t, err)

			res, errSubmit := client.GetLogRecordsWithResponse(context.Background(), tc.ListLogRecordsArgs)
			assert.NoError(t, errSubmit)

			if !assert.Equal(t, tc.WantStatusCode, res.StatusCode()) {
				t.Errorf("response body: %s", res.Body)
			}

			if tc.WantStatusCode != http.StatusOK {
				return
			}

			assert.NotNil(t, res.JSON200)
			assert.Len(t, res.JSON200.Records, len(tc.WantResponseLogRecords))
			assert.Equal(t, tc.WantResponseLogRecords, res.JSON200.Records)
			assert.Equal(t, tc.WantResponsePagination, res.JSON200.Pagination)
		})
	}
}
