// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package service_test

import (
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
)

type metadataField map[string]map[string]interface{}

func TestListMetadataRecords(t *testing.T) {
	testCases := map[string]struct {
		LogRecordsToCreate      []*command.NewRecordArgs
		MetadataRecordsToCreate []*command.NewMetadataRecordArgs
		TransactionID           models.Uuid
		WantStatusCode          int
		WantResponse            []models.Metadata
	}{
		"happy_flow": {
			LogRecordsToCreate: func() []*command.NewRecordArgs {
				logRecordIncoming := createLogRecord(t, record.DirectionIn)
				logRecordOutgoing := createLogRecord(t, record.DirectionOut)

				return []*command.NewRecordArgs{
					logRecordIncoming,
					logRecordOutgoing,
				}
			}(),
			MetadataRecordsToCreate: func() []*command.NewMetadataRecordArgs {
				metaDataRecordIn := createMetadataRecord(t, metadata.DirectionIn)
				metaDataRecordOut := createMetadataRecord(t, metadata.DirectionOut)

				return []*command.NewMetadataRecordArgs{
					metaDataRecordIn,
					metaDataRecordOut,
				}
			}(),
			TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
			WantResponse: []models.Metadata{
				{
					Direction:     models.MetadataDirectionDIRECTIONINCOMING,
					GroupId:       "test-group",
					TransactionId: "01899c62-eba5-7b58-b68d-000000000001",
					AdditionalProperties: map[string]map[string]interface{}{
						"HEADERS": {
							"Fsc-Grant-Hash": "$1$3$AFCvs4MlJZwsWdqrOKNN-HuZppTG7bBE96vUeaAGiLl-YKu9FwG-Eh4CE5cq26L6nlC0KMNfjC211nrrC6_z-A",
						},
					},
				},
				{
					Direction:     models.MetadataDirectionDIRECTIONOUTGOING,
					GroupId:       "test-group",
					TransactionId: "01899c62-eba5-7b58-b68d-000000000001",
					AdditionalProperties: map[string]map[string]interface{}{
						"HEADERS": {
							"Fsc-Grant-Hash": "$1$3$AFCvs4MlJZwsWdqrOKNN-HuZppTG7bBE96vUeaAGiLl-YKu9FwG-Eh4CE5cq26L6nlC0KMNfjC211nrrC6_z-A",
						},
					},
				},
			},
			WantStatusCode: http.StatusOK,
		},
	}

	txlogServer, app := newService(t.Name())

	t.Cleanup(func() {
		txlogServer.Close()
	})

	client, err := createTxLogAPIClient(txlogServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			err := app.Commands.CreateRecords.Handle(context.Background(), tc.LogRecordsToCreate)
			require.NoError(t, err)

			err = app.Commands.CreateMetadataRecords.Handle(context.Background(), tc.MetadataRecordsToCreate)
			require.NoError(t, err)

			res, errSubmit := client.ListMetadataRecordsWithResponse(context.Background(), tc.TransactionID)
			assert.NoError(t, errSubmit)

			if !assert.Equal(t, tc.WantStatusCode, res.StatusCode()) {
				t.Errorf("response body: %s", res.Body)
			}

			if tc.WantStatusCode != http.StatusOK {
				return
			}

			assert.NotNil(t, res.JSON200)
			assert.Equal(t, tc.WantResponse, res.JSON200.Records)
		})
	}
}

func createLogRecord(t *testing.T, direction record.Direction) *command.NewRecordArgs {
	grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
		ContractContentIV:     "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
		ContractGroupID:       "test-group",
		ContractHashAlgorithm: 1,
	})
	require.NoError(t, err)

	return &command.NewRecordArgs{
		TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
		GroupID:       "test-group",
		GrantHash:     grantHash,
		ServiceName:   "test-service",
		Direction:     direction,
		Source: &command.NewRecordSourceArgs{
			OutwayPeerID: "01234567891234567890",
		},
		Destination: &command.NewRecordDestinationArgs{
			ServicePeerID: "01234567891234567891",
		},
		CreatedAt: nowInUTC,
	}
}

func createMetadataRecord(t *testing.T, direction metadata.Direction) *command.NewMetadataRecordArgs {
	mdf := metadataField{
		"HEADERS": {
			"Fsc-Grant-Hash": "$1$3$AFCvs4MlJZwsWdqrOKNN-HuZppTG7bBE96vUeaAGiLl-YKu9FwG-Eh4CE5cq26L6nlC0KMNfjC211nrrC6_z-A",
		},
	}

	mdfBytes, err := json.Marshal(mdf)
	require.NoError(t, err)

	return &command.NewMetadataRecordArgs{
		TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
		Direction:     direction,
		GroupID:       "test-group",
		Metadata:      mdfBytes,
	}
}
