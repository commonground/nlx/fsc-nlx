// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package service_test

import (
	"context"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest"
	api "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/service"
)

var nowInUTC = time.Now().UTC()

var (
	testClock = testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	orgA      *organizationInfo
	orgB      *organizationInfo
)

func TestMain(m *testing.M) {
	var err error

	orgA, err = newOrganizationInfo(testingutils.NLXTestPeerA)
	if err != nil {
		log.Fatal(err)
	}

	orgB, err = newOrganizationInfo(testingutils.NLXTestPeerB)
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

type organizationInfo struct {
	CertBundle *tls.CertificateBundle
}

func newOrganizationInfo(organisationName testingutils.CertificateBundlePeerName) (*organizationInfo, error) {
	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "../", "testing", "pki"), organisationName)
	if err != nil {
		return nil, err
	}

	return &organizationInfo{
		CertBundle: orgCertBundle,
	}, nil
}

func newService(testName string) (*httptest.Server, *app.Application) {
	logger := discardlogger.New()

	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres:postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		log.Fatalf("failed to setup test database: %v", err)
	}

	db, err := postgresadapter.NewConnection(context.Background(), testDB)
	if err != nil {
		log.Fatal("can not create db connection:", err)
	}

	err = postgresadapter.PerformMigrations(testDB)
	if err != nil {
		log.Fatalf("failed to perform dbmigrations: %v", err)
	}

	storage, err := postgresadapter.New(db)
	if err != nil {
		log.Fatalf("failed to create db connection: %v", err)
	}

	app, err := service.NewApplication(&service.NewApplicationArgs{
		Context:    context.Background(),
		Clock:      testClock,
		Logger:     logger,
		Repository: storage,
	})
	if err != nil {
		log.Fatalf("failed to setup application: %s", err)
	}

	server, err := rest.New(&rest.NewArgs{
		Logger: logger,
		App:    app,
		Cert:   orgA.CertBundle,
	})
	if err != nil {
		log.Fatalf("failed to setup rest port: %s", err)
	}

	srv := httptest.NewUnstartedServer(server.Handler())
	srv.TLS = orgA.CertBundle.TLSConfig(orgA.CertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv, app
}

func createTxLogAPIClient(url string, certBundle *tls.CertificateBundle) (*api.ClientWithResponses, error) {
	return api.NewClientWithResponses(url, func(c *api.Client) error {
		t := &http.Transport{
			TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
		}
		c.Client = &http.Client{
			Transport: t,
		}
		return nil
	})
}
