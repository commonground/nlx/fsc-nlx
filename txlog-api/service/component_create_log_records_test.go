// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package service_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/query"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
)

func TestCreateLogRecords(t *testing.T) {
	testCases := map[string]struct {
		CreateLogRecordsArgs models.CreateLogRecordsJSONRequestBody
		WantStatusCode       int
	}{
		"records_missing": {
			CreateLogRecordsArgs: func() models.CreateLogRecordsJSONRequestBody {
				return models.CreateLogRecordsJSONRequestBody{}
			}(),
			WantStatusCode: http.StatusUnprocessableEntity,
		},
		"happy_flow": {
			CreateLogRecordsArgs: func() models.CreateLogRecordsJSONRequestBody {
				source := models.LogRecord_Source{}

				_ = source.FromSource(models.Source{
					OutwayPeerId: "01234567891234567890",
				})

				destination := models.LogRecord_Destination{}

				_ = destination.FromDestination(models.Destination{
					ServicePeerId: "01234567891234567891",
				})

				transactionID := uuid.Must(uuid.FromString("01899c62-eba5-7b58-b68d-000000000001"))

				grantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
					ContractContentIV:           "ba447ecf-3519-4dfc-8655-6a9262ce8a6b",
					ContractGroupID:             "test-group",
					ContractHashAlgorithm:       1,
					OutwayPeerID:                "",
					OutwayCertificateThumbprint: "",
					ServicePeerID:               "",
					ServiceName:                 "",
				})
				require.NoError(t, err)

				return models.CreateLogRecordsJSONRequestBody{
					Records: []models.LogRecord{
						{
							CreatedAt:     testClock.Now().Unix(),
							Destination:   destination,
							Direction:     models.LogRecordDirectionDIRECTIONOUTGOING,
							GrantHash:     grantHash,
							GroupId:       "test-group",
							ServiceName:   "test-service",
							Source:        source,
							TransactionId: transactionID.String(),
						},
					},
				}
			}(),
			WantStatusCode: http.StatusNoContent,
		},
	}

	txlogServer, app := newService(t.Name())

	defer txlogServer.Close()

	client, err := createTxLogAPIClient(txlogServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	for name, testCase := range testCases {
		tc := testCase

		t.Run(name, func(t *testing.T) {
			resp, errSubmit := client.CreateLogRecordsWithResponse(context.Background(), tc.CreateLogRecordsArgs)
			assert.NoError(t, errSubmit)

			if !assert.Equal(t, testCase.WantStatusCode, resp.StatusCode()) {
				t.Errorf("response body: %s", resp.Body)
			}

			if testCase.WantStatusCode != http.StatusCreated {
				return
			}

			var transactionID = tc.CreateLogRecordsArgs.Records[0].TransactionId

			records, listRecordsErr := app.Queries.ListRecords.Handle(context.Background(), &query.ListTransactionLogRecordsHandlerArgs{
				Pagination: nil,
				Filters: []*query.Filter{
					{
						TransactionID: transactionID,
					},
				},
			})
			assert.NoError(t, listRecordsErr)

			assert.Len(t, records, 1)
			assert.Equal(t, records[0].TransactionID, transactionID)
		})
	}
}
