// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package cmd

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/spf13/cobra"

	"gitlab.com/commonground/nlx/fsc-nlx/common/cmd"
	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logoptions"
	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	"gitlab.com/commonground/nlx/fsc-nlx/common/process"
	"gitlab.com/commonground/nlx/fsc-nlx/common/slogerrorwriter"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/version"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/pkg/health"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/service"
)

var serveOpts struct {
	ListenAddress      string
	MonitoringAddress  string
	StoragePostgresDSN string

	logoptions.LogOptions
	cmd.TLSOptions
}

type clock struct{}

func (c *clock) Now() time.Time {
	return time.Now()
}

//nolint:gochecknoinits,funlen,gocyclo // this is the recommended way to use cobra, also a lot of flags..
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddress, "listen-address", "", "127.0.0.1:8443", "Address for the api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.MonitoringAddress, "monitoring-address", "", "127.0.0.1:8081", "Address for the txlog monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.StoragePostgresDSN, "storage-postgres-dsn", "", "", "Postgres Connection URL")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "info", "Set loglevel")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the API",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		ctx := context.Background()

		logger, err := zaplogger.New(serveOpts.LogOptions.LogLevel, serveOpts.LogOptions.LogType)
		if err != nil {
			log.Fatalf("failed to create logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version info: version: %s source-hash: %s", version.BuildVersion, version.BuildSourceHash))

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn(fmt.Sprintf("invalid internal PKI key permissions: file-path: %s", serveOpts.KeyFile), err)
		}

		certificate, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("loading TLS files", err)
		}

		txLogAPIProbe := health.TxLogAPIProbe(certificate, serveOpts.ListenAddress)
		readinessProbes := map[string]monitoring.Check{
			"TxLog Readiness Probe": txLogAPIProbe,
		}

		livenessProbes := map[string]monitoring.Check{
			"TxLog Readiness Probe": txLogAPIProbe,
		}

		monitorService, err := monitoring.NewMonitoringService(serveOpts.MonitoringAddress, logger, readinessProbes, livenessProbes)
		if err != nil {
			logger.Fatal("unable to create monitoring service", err)
		}

		go func() {
			err = monitorService.Start()
			if err != nil {
				logger.Fatal("error listening on monitoring service", err)
			}
		}()

		db, err := postgresadapter.NewConnection(ctx, serveOpts.StoragePostgresDSN)
		if err != nil {
			logger.Fatal("can not create db connection:", err)
		}

		storage, err := postgresadapter.New(db)
		if err != nil {
			logger.Fatal("failed to setup postgresql txlog database", err)
		}

		app, err := service.NewApplication(&service.NewApplicationArgs{
			Context:    ctx,
			Logger:     logger,
			Repository: storage,
			Clock:      &clock{},
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		restServer, err := rest.New(&rest.NewArgs{
			Logger: logger,
			App:    app,
			Cert:   certificate,
		})
		if err != nil {
			logger.Fatal("could not create rest server", err)
		}

		var readHeaderTimeout = 5 * time.Second

		srv := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restServer.Handler(),
			Addr:              serveOpts.ListenAddress,
			TLSConfig:         certificate.TLSConfig(certificate.WithTLSClientAuth()),
			ErrorLog:          log.New(slogerrorwriter.New(logger.Logger), "", 0),
		}

		go func() {
			logger.Info(fmt.Sprintf("starting rest server. listen address: %q", serveOpts.ListenAddress))

			err = srv.ListenAndServeTLS(serveOpts.CertFile, serveOpts.KeyFile)
			if err != nil && !errors.Is(err, http.ErrServerClosed) {
				logger.Fatal("could not listen and serve rest server", err)
			}
		}()

		p.Wait()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		err = srv.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown rest server", err)
		}

		db.Close()

		err = monitorService.Stop()
		if err != nil {
			logger.Error("could not stop monitoring", err)
		}
	},
}
