// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/query"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

//nolint:gocritic // hugeParam, this is a generated interface by the oas generator so can't change
func (s *Server) GetLogRecords(ctx context.Context, req api.GetLogRecordsRequestObject) (api.GetLogRecordsResponseObject, error) {
	s.logger.Info("rest request GetLogRecords")

	sortOrder := query.SortOrderAscending

	if req.Params.SortOrder != nil && *req.Params.SortOrder == models.SORTORDERDESCENDING {
		sortOrder = query.SortOrderDescending
	}

	var (
		startID string
		limit   int32
	)

	if req.Params.Cursor != nil {
		startID = *req.Params.Cursor
	}

	limit, err := validateAndConvertLimit(req.Params.Limit)
	if err != nil {
		s.logger.Error("invalid limit parameter", err)
		return nil, err
	}

	records, err := s.app.Queries.ListRecords.Handle(ctx, &query.ListTransactionLogRecordsHandlerArgs{
		Pagination: &query.Pagination{
			StartID:   startID,
			Limit:     limit,
			SortOrder: sortOrder,
		},
		Filters: mapRestToQueryFilters(&req.Params),
		GroupID: req.Params.GroupId,
	})
	if err != nil {
		s.logger.Error("error getting tx log records from query", err)
		return nil, err
	}

	response, err := mapQueryToRestLogs(records)
	if err != nil {
		s.logger.Error("unable to convert query model to api model", err)
		return nil, err
	}

	var nextCursor string
	if req.Params.TransactionIds == nil {
		nextCursor = getNextLogCursor(records)
	}

	return api.GetLogRecords200JSONResponse{
		Records: response,
		Pagination: models.PaginationResult{
			NextCursor: nextCursor,
		},
	}, nil
}

func validateAndConvertLimit(reqLimit *models.QueryPaginationLimit) (int32, error) {
	const MinLimit = 1

	const MaxLimit = 1000

	if reqLimit == nil {
		return 0, nil
	}

	if *reqLimit < MinLimit {
		return 0, errors.NewIncorrectInputError(fmt.Sprintf("limit must be greater than %d, got %d", MinLimit, *reqLimit))
	}

	if *reqLimit > MaxLimit {
		return 0, errors.NewIncorrectInputError(fmt.Sprintf("limit must be less than or equal to %d, received limit: %d", MaxLimit, *reqLimit))
	}

	return int32(*reqLimit), nil //nolint:gosec // we know this won't overflow due to the above validations
}

//nolint:gocyclo // further optimization would lead to less readable code
func mapRestToQueryFilters(params *models.GetLogRecordsParams) []*query.Filter {
	filters := make([]*query.Filter, 0)

	if params.GrantHash != nil {
		for _, g := range *params.GrantHash {
			if g != "" {
				filters = append(filters, &query.Filter{
					GrantHash: g,
				})
			}
		}
	}

	if params.ServiceName != nil {
		for _, s := range *params.ServiceName {
			if s != "" {
				filters = append(filters, &query.Filter{
					ServiceName: s,
				})
			}
		}
	}

	if params.TransactionIds != nil {
		for _, t := range *params.TransactionIds {
			if t != "" {
				filters = append(filters, &query.Filter{
					TransactionID: t,
				})
			}
		}
	}

	if params.PeerId != nil {
		for _, p := range *params.PeerId {
			if p != "" {
				filters = append(filters, &query.Filter{
					PeerID: p,
				})
			}
		}
	}

	if params.Before != nil || params.After != nil {
		p := &query.Period{}

		if params.Before != nil && *params.Before > 0 {
			p.End = time.Unix(*params.Before, 0)
		}

		if params.After != nil && *params.After > 0 {
			p.Start = time.Unix(*params.After, 0)
		}

		filters = append(filters, &query.Filter{
			Period: p,
		})
	}

	return filters
}

func mapQueryToRestLogs(records []*query.Record) ([]models.LogRecord, error) {
	recs := make([]models.LogRecord, len(records))

	for i, r := range records {
		data, err := mapSourceAndDirection(r.Source, r.Destination)
		if err != nil {
			return nil, err
		}

		recs[i] = models.LogRecord{
			GroupId:       r.GroupID,
			TransactionId: r.TransactionID,
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Direction:     mapDirection(r.Direction),
			Source:        data.Source,
			Destination:   data.Destination,
			CreatedAt:     r.CreatedAt.Unix(),
		}
	}

	return recs, nil
}

func mapSourceAndDirection(source, destination interface{}) (*models.LogRecord, error) {
	data := &models.LogRecord{}

	switch s := source.(type) {
	case *query.RecordSource:
		err := data.Source.FromSource(models.Source{
			OutwayPeerId: s.OutwayPeerID,
		})
		if err != nil {
			return nil, err
		}

	case *query.RecordDelegatedSource:
		err := data.Source.FromSourceDelegated(models.SourceDelegated{
			OutwayPeerId:    s.OutwayPeerID,
			DelegatorPeerId: s.DelegatorPeerID,
		})
		if err != nil {
			return nil, err
		}

	default:
		return nil, fmt.Errorf("unknown source type: %T", s)
	}

	switch d := destination.(type) {
	case *query.RecordDestination:
		err := data.Destination.FromDestination(models.Destination{
			ServicePeerId: d.ServicePeerID,
		})
		if err != nil {
			return nil, err
		}

	case *query.RecordDelegatedDestination:
		err := data.Destination.FromDestinationDelegated(models.DestinationDelegated{
			ServicePeerId:   d.ServicePeerID,
			DelegatorPeerId: d.DelegatorPeerID,
		})
		if err != nil {
			return nil, err
		}

	default:
		return nil, fmt.Errorf("unknown destination type: %T", d)
	}

	return data, nil
}

func mapDirection(d record.Direction) models.LogRecordDirection {
	switch d {
	case record.DirectionIn:
		return models.LogRecordDirectionDIRECTIONINCOMING
	case record.DirectionOut:
		return models.LogRecordDirectionDIRECTIONOUTGOING
	default:
		return models.LogRecordDirectionDIRECTIONINCOMING
	}
}

func getNextLogCursor(records []*query.Record) string {
	if len(records) == 0 {
		return ""
	}

	return records[len(records)-1].TransactionID
}
