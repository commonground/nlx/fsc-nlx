// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

func (s *Server) CreateLogRecords(ctx context.Context, req api.CreateLogRecordsRequestObject) (api.CreateLogRecordsResponseObject, error) {
	s.logger.Info("rest request CreateLogRecords")

	if len(req.Body.Records) == 0 {
		return api.CreateLogRecords422ApplicationProblemPlusJSONResponse{}, nil
	}

	records, err := restToRecords(req.Body.Records)
	if err != nil {
		s.logger.Error("error converting rest tx log records to command records", err)
		return nil, err
	}

	err = s.app.Commands.CreateRecords.Handle(ctx, records)
	if err != nil {
		s.logger.Error("error creating tx log records in command", err)
		return nil, err
	}

	return api.CreateLogRecords204Response{}, nil
}

func restToRecords(res []models.LogRecord) ([]*command.NewRecordArgs, error) {
	records := make([]*command.NewRecordArgs, len(res))

	for i := range res {
		source, destination, err := mapSourceAndDestinationToModel(&res[i])
		if err != nil {
			return nil, fmt.Errorf("could not map source and destination: %w", err)
		}

		records[i] = &command.NewRecordArgs{
			GroupID:       res[i].GroupId,
			TransactionID: res[i].TransactionId,
			GrantHash:     res[i].GrantHash,
			ServiceName:   res[i].ServiceName,
			Direction:     mapDirectionToRecord(res[i].Direction),
			Source:        source,
			Destination:   destination,
			CreatedAt:     time.Unix(res[i].CreatedAt, 0),
		}
	}

	return records, nil
}

func mapSourceAndDestinationToModel(r *models.LogRecord) (source, destination interface{}, err error) {
	sourceDiscriminator, err := r.Source.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedSourceModel := sourceDiscriminator.(type) {
	case models.Source:
		source = &command.NewRecordSourceArgs{
			OutwayPeerID: convertedSourceModel.OutwayPeerId,
		}
	case models.SourceDelegated:
		source = &command.NewRecordDelegatedSourceArgs{
			OutwayPeerID:    convertedSourceModel.OutwayPeerId,
			DelegatorPeerID: convertedSourceModel.DelegatorPeerId,
		}
	}

	destinationDiscriminator, err := r.Destination.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedDestinationModel := destinationDiscriminator.(type) {
	case models.Destination:
		destination = &command.NewRecordDestinationArgs{
			ServicePeerID: convertedDestinationModel.ServicePeerId,
		}
	case models.DestinationDelegated:
		destination = &command.NewRecordDelegatedDestinationArgs{
			ServicePeerID:   convertedDestinationModel.ServicePeerId,
			DelegatorPeerID: convertedDestinationModel.DelegatorPeerId,
		}
	}

	return source, destination, nil
}

func mapDirectionToRecord(d models.LogRecordDirection) record.Direction {
	switch d {
	case models.LogRecordDirectionDIRECTIONINCOMING:
		return record.DirectionIn
	case models.LogRecordDirectionDIRECTIONOUTGOING:
		return record.DirectionOut
	default:
		return record.DirectionUnspecified
	}
}
