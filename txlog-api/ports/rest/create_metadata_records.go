// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/json"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

func (s *Server) CreateMetadataRecords(ctx context.Context, request api.CreateMetadataRecordsRequestObject) (api.CreateMetadataRecordsResponseObject, error) {
	s.logger.Info("rest request CreateMetadataRecords")

	if len(request.Body.Records) == 0 {
		return api.CreateMetadataRecords422ApplicationProblemPlusJSONResponse{}, nil
	}

	records, err := restToMetadataRecords(request.Body.Records, request.TransactionID)
	if err != nil {
		s.logger.Error("error converting rest metadata log records to command records", err)
		return nil, err
	}

	err = s.app.Commands.CreateMetadataRecords.Handle(ctx, records)
	if err != nil {
		s.logger.Error("error creating metadata log records in command", err)
		return nil, err
	}

	return api.CreateMetadataRecords204Response{}, nil
}

func restToMetadataRecords(metadataRecord []models.Metadata, transactionID string) ([]*command.NewMetadataRecordArgs, error) {
	records := make([]*command.NewMetadataRecordArgs, 0)

	for i := range metadataRecord {
		direction := mapMetadataDirectionToRecord(metadataRecord[i].Direction)

		metadataFields, err := json.Marshal(metadataRecord[i].AdditionalProperties)

		if err != nil {
			return nil, errors.Join(err, errors.New("value is not a JSON payload"))
		}

		records = append(records, &command.NewMetadataRecordArgs{
			TransactionID: transactionID,
			Direction:     direction,
			GroupID:       metadataRecord[i].GroupId,
			Metadata:      metadataFields,
		})
	}

	return records, nil
}

func mapMetadataDirectionToRecord(direction models.MetadataDirection) metadata.Direction {
	switch direction {
	case models.MetadataDirectionDIRECTIONINCOMING:
		return metadata.DirectionIn
	case models.MetadataDirectionDIRECTIONOUTGOING:
		return metadata.DirectionOut
	default:
		return metadata.DirectionUnspecified
	}
}
