// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
)

func TestRestToMetadataRecords(t *testing.T) {
	groupID := "test-group-id"
	direction := models.MetadataDirectionDIRECTIONOUTGOING
	transactionID, err := uuid.NewV7()
	assert.NoError(t, err)

	requestRecords := []models.Metadata{
		{
			Direction: direction,
			GroupId:   groupID,
			AdditionalProperties: map[string]map[string]interface{}{
				"key1": {
					"value": "value1",
				},
				"key2": {
					"value": "value2",
					"foo":   "bar",
				},
			},
		},
		{
			Direction: direction,
			GroupId:   groupID,
			AdditionalProperties: map[string]map[string]interface{}{
				"key3": {
					"value": "value3",
				},
				"key4": {
					"value": "value4",
				},
			},
		},
	}

	commandRecords, err := restToMetadataRecords(requestRecords, transactionID.String())
	assert.NoError(t, err)
	assert.Len(t, commandRecords, 2)
}
