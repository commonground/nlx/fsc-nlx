// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/json"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

func (s *Server) ListMetadataRecords(ctx context.Context, req api.ListMetadataRecordsRequestObject) (api.ListMetadataRecordsResponseObject, error) {
	s.logger.Info("rest request GetMetadataRecords")

	transactionID := req.TransactionID

	records, err := s.app.Queries.ListMetadataRecords.Handle(ctx, transactionID)
	if err != nil {
		s.logger.Error("error getting metadata records from query", err)
		return nil, err
	}

	resRecords := make([]models.Metadata, 0, len(records))

	for _, record := range records {
		additionalProperties, err := metadataToRest(record.Metadata)
		if err != nil {
			s.logger.Error("error marshaling metadata to JSON response", err)
			return nil, err
		}

		resRecords = append(resRecords, models.Metadata{
			Direction:            directionToRest(record.Direction),
			GroupId:              record.GroupID,
			TransactionId:        record.TransactionID,
			AdditionalProperties: additionalProperties,
		})
	}

	return api.ListMetadataRecords200JSONResponse{Records: resRecords}, nil
}

func directionToRest(direction metadata.Direction) models.MetadataDirection {
	switch direction {
	case metadata.DirectionIn:
		return models.MetadataDirectionDIRECTIONINCOMING
	case metadata.DirectionOut:
		return models.MetadataDirectionDIRECTIONOUTGOING
	default:
		return models.MetadataDirectionDIRECTIONINCOMING
	}
}

func metadataToRest(metadataRecord []byte) (map[string]map[string]interface{}, error) {
	additionalProperties := make(map[string]map[string]interface{})
	err := json.Unmarshal(metadataRecord, &additionalProperties)

	if err != nil {
		return nil, errors.Join(err, errors.New("failed to unmarshal value to json"))
	}

	return additionalProperties, nil
}
