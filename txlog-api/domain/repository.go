// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package domain

import (
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type Repository interface {
	record.Repository
	metadata.Repository
}
