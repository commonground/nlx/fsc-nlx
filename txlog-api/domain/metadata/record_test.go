// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package metadata_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
)

func TestNewRecord(t *testing.T) {
	tests := map[string]struct {
		args    *metadata.NewMetadataRecordArgs
		wantErr error
	}{
		"happy_flow": {
			args: &metadata.NewMetadataRecordArgs{
				TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
				GroupID:       "fsc-local",
				Direction:     metadata.DirectionIn,
				Metadata:      []byte("{\"key\":\"value\"}"),
			},
			wantErr: nil,
		},
	}

	for name, tc := range tests {
		tc := tc

		t.Run(name, func(t *testing.T) {
			_, err := metadata.NewRecord(tc.args)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}
