// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package metadata

import (
	"context"

	"github.com/gofrs/uuid"
)

type Repository interface {
	CreateMetadataRecords(context.Context, []*Record) error
	ListMetadataRecords(context.Context, uuid.UUID) ([]*Record, error)
}
