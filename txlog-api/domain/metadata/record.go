// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package metadata

import (
	"errors"

	"github.com/gofrs/uuid"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type MetadataID uuid.UUID

type Direction int32

const (
	DirectionUnspecified Direction = iota
	DirectionIn
	DirectionOut
)

type Record struct {
	id            *MetadataID
	transactionID *record.TransactionID
	groupID       string
	direction     Direction
	metadata      []byte
}

func (r *Record) ID() *MetadataID {
	return r.id
}

func (r *Record) TransactionID() *record.TransactionID {
	return r.transactionID
}

func (r *Record) Metadata() []byte {
	return r.metadata
}

func (r *Record) Direction() Direction {
	return r.direction
}

func (r *Record) GroupID() string {
	return r.groupID
}

func (m *MetadataID) UUID() uuid.UUID {
	return uuid.UUID(*m)
}

func NewRecord(args *NewMetadataRecordArgs) (*Record, error) {
	metadataID, err := NewMetadataID()
	if err != nil {
		return nil, err
	}

	transactionID, err := record.NewTransactionIDFromString(args.TransactionID)
	if err != nil {
		return nil, err
	}

	return &Record{
		id:            metadataID,
		direction:     args.Direction,
		groupID:       args.GroupID,
		transactionID: transactionID,
		metadata:      args.Metadata,
	}, nil
}

func NewMetadataID() (*MetadataID, error) {
	metadataID, err := uuid.NewV7()
	if err != nil {
		return nil, errors.Join(err, errors.New("could not generate new metadata ID"))
	}

	l := MetadataID(metadataID)

	return &l, nil
}
