// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package metadata

type NewMetadataRecordArgs struct {
	TransactionID string
	GroupID       string
	Direction     Direction
	Metadata      []byte
}
