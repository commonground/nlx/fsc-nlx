// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package record

import (
	"context"
	"time"

	"github.com/gofrs/uuid"
)

type Repository interface {
	CreateRecords(context.Context, []*Record) error
	ListRecords(ctx context.Context, req *ListRecordsRequest) ([]*Record, error)
	Ping(ctx context.Context) error
}

type SortOrder uint32

const (
	SortOrderUnspecified SortOrder = iota
	SortOrderAscending
	SortOrderDescending
)

type Pagination struct {
	StartID   uuid.UUID
	Limit     int32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}

type Filter struct {
	Period        *Period
	TransactionID *TransactionID
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type ListRecordsRequest struct {
	Pagination *Pagination
	Filters    []*Filter
	GroupID    string
}
