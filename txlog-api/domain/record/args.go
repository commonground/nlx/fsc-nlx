// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package record

import "time"

type NewRecordArgs struct {
	TransactionID string
	GroupID       string
	GrantHash     string
	ServiceName   string
	Direction     Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type NewRecordSourceArgs struct {
	OutwayPeerID string
}

type NewRecordDelegatedSourceArgs struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type NewRecordDestinationArgs struct {
	ServicePeerID string
}

type NewRecordDelegatedDestinationArgs struct {
	ServicePeerID   string
	DelegatorPeerID string
}
