// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"errors"

	"github.com/gofrs/uuid"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
)

type ListMetadataRecordsHandler struct {
	repository metadata.Repository
}

func NewMetadataRecordsHandler(repository metadata.Repository) *ListMetadataRecordsHandler {
	return &ListMetadataRecordsHandler{
		repository: repository,
	}
}

func (l *ListMetadataRecordsHandler) Handle(ctx context.Context, transactionID string) ([]*MetadataRecord, error) {
	txID, err := uuid.FromString(transactionID)
	if err != nil {
		return nil, errors.Join(err, errors.New("invalid transaction id"))
	}

	records, err := l.repository.ListMetadataRecords(ctx, txID)
	if err != nil {
		return nil, errors.Join(err, errors.New("failed to list metadata records"))
	}

	metadataRecords := make([]*MetadataRecord, 0, len(records))
	for _, record := range records {
		metadataRecords = append(metadataRecords, &MetadataRecord{
			TransactionID: record.TransactionID().String(),
			GroupID:       record.GroupID(),
			Direction:     record.Direction(),
			Metadata:      record.Metadata(),
		})
	}

	return metadataRecords, nil
}
