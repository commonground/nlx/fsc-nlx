// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package query

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type GetHealthHandler struct {
	recordsRepository record.Repository
}

func NewGetHealthHandler(repo record.Repository) (*GetHealthHandler, error) {
	if repo == nil {
		return nil, errors.New("repo is required")
	}

	return &GetHealthHandler{recordsRepository: repo}, nil
}

func (h *GetHealthHandler) Handle(ctx context.Context) error {
	return h.recordsRepository.Ping(ctx)
}
