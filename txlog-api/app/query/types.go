// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type Record struct {
	TransactionID string
	GroupID       string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type RecordSource struct {
	OutwayPeerID string
}

type RecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type RecordDestination struct {
	ServicePeerID string
}

type RecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

type MetadataRecord struct {
	TransactionID string
	GroupID       string
	Direction     metadata.Direction
	Metadata      []byte
}
