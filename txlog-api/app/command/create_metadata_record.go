// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"encoding/json"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/logger"
)

type NewMetadataRecordArgs struct {
	TransactionID string
	Direction     metadata.Direction
	GroupID       string
	Metadata      json.RawMessage
}

type NewCreateMetadataRecordHandler struct{}

type CreateMetadataRecordsHandler struct {
	clock      Clock
	repository metadata.Repository
	logger     logger.Logger
}

func NewCreateMetadataRecordsHandler(repository metadata.Repository, clock Clock, lgr logger.Logger) (*CreateMetadataRecordsHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	if clock == nil {
		return nil, errors.New("clock is required")
	}

	if lgr == nil {
		return nil, errors.New("lgr is required")
	}

	return &CreateMetadataRecordsHandler{
		clock:      clock,
		repository: repository,
		logger:     lgr,
	}, nil
}

func (h *CreateMetadataRecordsHandler) Handle(ctx context.Context, metadataRecords []*NewMetadataRecordArgs) error {
	records := make([]*metadata.Record, 0, len(metadataRecords))

	for _, metadataRecord := range metadataRecords {
		domainRecord := metadata.NewMetadataRecordArgs{
			TransactionID: metadataRecord.TransactionID,
			GroupID:       metadataRecord.GroupID,
			Direction:     metadataRecord.Direction,
			Metadata:      metadataRecord.Metadata,
		}

		rec, err := metadata.NewRecord(&domainRecord)
		if err != nil {
			return err
		}

		records = append(records, rec)
	}

	err := h.repository.CreateMetadataRecords(ctx, records)
	if err != nil {
		h.logger.Error("failed to create metadata records", err)
		return err
	}

	return nil
}
