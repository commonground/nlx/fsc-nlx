// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package health

import (
	"errors"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
)

//nolint:gosec // we only know the IP and not the hostname, since this is an internal call we know who we are talking to, so risk is negligible
func TxLogAPIProbe(cert *common_tls.CertificateBundle, listenAddress string) monitoring.Check {
	tlsConfig := cert.TLSConfig()
	tlsConfig.InsecureSkipVerify = true

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConfig,
		},
	}

	return func() error {
		url := "https://" + listenAddress + "/v1/health"

		response, err := client.Get(url)
		if err != nil {
			return errors.Join(err, fmt.Errorf("health check failed. url %s", url))
		}

		if response.StatusCode == http.StatusOK {
			return nil
		}

		defer response.Body.Close()

		responseBody, err := io.ReadAll(response.Body)
		if err != nil {
			return errors.Join(err, fmt.Errorf("failed to read response body. url: %s", url))
		}

		return errors.Join(err, fmt.Errorf("unexpected response status code %d. url: %s body: %s", response.StatusCode, url, string(responseBody)))
	}
}
