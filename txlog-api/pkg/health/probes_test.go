// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package health_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/pkg/health"
)

func TestTxLogAPIProbeError(t *testing.T) {
	t.Parallel()

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	tlsConfig := orgCertBundle.TLSConfig()

	ts := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("kaboom!")
	}))
	ts.TLS = tlsConfig
	ts.StartTLS()

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.TxLogAPIProbe(orgCertBundle, testProbeURL.Host)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("Get \"https://%s/v1/health\": EOF\nhealth check failed. url https://%s/v1/health", testProbeURL.Host, testProbeURL.Host))
}

func TestTxLogAPIProbeProbeFailing(t *testing.T) {
	t.Parallel()

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	tlsConfig := orgCertBundle.TLSConfig()

	ts := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusServiceUnavailable)
	}))
	ts.TLS = tlsConfig
	ts.StartTLS()

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.TxLogAPIProbe(orgCertBundle, testProbeURL.Host)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("unexpected response status code 503. url: https://%s/v1/health body: ", testProbeURL.Host))
}

func TestTxLogAPIProbeProbeSuccess(t *testing.T) {
	t.Parallel()

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	tlsConfig := orgCertBundle.TLSConfig()

	ts := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))
	ts.TLS = tlsConfig
	ts.StartTLS()

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.TxLogAPIProbe(orgCertBundle, testProbeURL.Host)

	err = probe()
	assert.NoError(t, err)
}
