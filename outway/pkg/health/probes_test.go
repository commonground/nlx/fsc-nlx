// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package health_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/outway/pkg/health"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestOutwayTLSProbeError(t *testing.T) {
	t.Parallel()

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	tlsConfig := orgCertBundle.TLSConfig()

	ts := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("kaboom!")
	}))
	ts.TLS = tlsConfig
	ts.StartTLS()

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.OutwayProbe(orgCertBundle.Cert(), testProbeURL.Host)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("Get \"https://%s/health/live\": EOF\nhealth check failed. url https://%s/health/live", testProbeURL.Host, testProbeURL.Host))
}

func TestOutwayProbeError(t *testing.T) {
	t.Parallel()

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("kaboom!")
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.OutwayProbe(nil, testProbeURL.Host)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("Get \"http://%s/health/live\": EOF\nhealth check failed. url http://%s/health/live", testProbeURL.Host, testProbeURL.Host))
}

func TestOutwayProbeFailing(t *testing.T) {
	t.Parallel()

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusServiceUnavailable)
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.OutwayProbe(nil, testProbeURL.Host)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("unexpected response status code 503. url: http://%s/health/live body: ", testProbeURL.Host))
}

func TestOutwayProbeSuccess(t *testing.T) {
	t.Parallel()

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	probe := health.OutwayProbe(nil, testProbeURL.Host)

	err = probe()
	assert.NoError(t, err)
}

func TestTxLogAPIProbeError(t *testing.T) {
	t.Parallel()

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	tlsConfig := orgCertBundle.TLSConfig()

	ts := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("kaboom!")
	}))
	ts.TLS = tlsConfig
	ts.StartTLS()

	t.Cleanup(func() {
		ts.Close()
	})

	probe := health.TxLogAPIProbe(orgCertBundle, ts.URL)

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("Get \"https://%s/v1/health\": EOF\nhealth check failed. url https://%s/v1/health", testProbeURL.Host, testProbeURL.Host))
}

func TestTxLogAPIProbeFailing(t *testing.T) {
	t.Parallel()

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	tlsConfig := orgCertBundle.TLSConfig()

	ts := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusServiceUnavailable)
	}))
	ts.TLS = tlsConfig
	ts.StartTLS()

	t.Cleanup(func() {
		ts.Close()
	})

	probe := health.TxLogAPIProbe(orgCertBundle, ts.URL)

	testProbeURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	err = probe()
	assert.ErrorContains(t, err, fmt.Sprintf("unexpected response status code 503. url: https://%s/v1/health body: ", testProbeURL.Host))
}

func TestTxLogAPIProbeSuccess(t *testing.T) {
	t.Parallel()

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	require.NoError(t, err)

	tlsConfig := orgCertBundle.TLSConfig()

	ts := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))
	ts.TLS = tlsConfig
	ts.StartTLS()

	t.Cleanup(func() {
		ts.Close()
	})

	probe := health.TxLogAPIProbe(orgCertBundle, ts.URL)

	err = probe()
	assert.NoError(t, err)
}
