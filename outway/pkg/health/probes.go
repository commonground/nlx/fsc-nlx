// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package health

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
)

func TxLogAPIProbe(cert *common_tls.CertificateBundle, listenAddress string) monitoring.Check {
	tlsConfig := cert.TLSConfig()

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConfig,
		},
	}

	return func() error {
		url := listenAddress + "/v1/health"

		response, err := client.Get(url)
		if err != nil {
			return errors.Join(err, fmt.Errorf("health check failed. url %s", url))
		}

		if response.StatusCode == http.StatusOK {
			return nil
		}

		defer response.Body.Close()

		responseBody, err := io.ReadAll(response.Body)
		if err != nil {
			return errors.Join(err, fmt.Errorf("failed to read response body. url: %s", url))
		}

		return errors.Join(err, fmt.Errorf("unexpected response status code %d. url: %s body: %s", response.StatusCode, url, string(responseBody)))
	}
}

func OutwayProbe(cert *tls.Certificate, listenAddress string) monitoring.Check {
	var url string

	client := &http.Client{}

	if cert == nil {
		url = fmt.Sprintf("http://%s/health/live", listenAddress)
	} else {
		url = fmt.Sprintf("https://%s/health/live", listenAddress)
		tlsConfig := common_tls.NewConfig(common_tls.WithTLS12())

		tlsConfig.RootCAs = x509.NewCertPool()
		tlsConfig.RootCAs.AddCert(cert.Leaf)

		client.Transport = &http.Transport{TLSClientConfig: tlsConfig}
	}

	return func() error {
		response, err := client.Get(url)
		if err != nil {
			return errors.Join(err, fmt.Errorf("health check failed. url %s", url))
		}

		if response.StatusCode == http.StatusOK {
			return nil
		}

		defer response.Body.Close()

		responseBody, err := io.ReadAll(response.Body)
		if err != nil {
			return errors.Join(err, fmt.Errorf("failed to read response body. url: %s", url))
		}

		return errors.Join(err, fmt.Errorf("unexpected response status code %d. url: %s body: %s", response.StatusCode, url, string(responseBody)))
	}
}
