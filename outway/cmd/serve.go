// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package cmd

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/cmd"
	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logoptions"
	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	"gitlab.com/commonground/nlx/fsc-nlx/common/process"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/version"
	controllerapi "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/server"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/outway"
	restconfig "gitlab.com/commonground/nlx/fsc-nlx/outway/adapters/config/rest"
	restcontroller "gitlab.com/commonground/nlx/fsc-nlx/outway/adapters/controller/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/pkg/health"
	txlogapi "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

var serveOpts struct {
	Name                        string
	GroupID                     string
	ManagerInternalAddress      string
	ListenAddress               string
	ListenHTTPS                 bool
	MonitoringAddress           string
	ControllerAPIAddress        string
	TxLogAPIAddress             string
	AuthorizationServiceAddress string
	AuthorizationCA             string
	AuthorizationWithBody       bool
	AuthorizationMaxBodySize    int
	AuthorizationChunkSize      int
	ServerCertFile              string
	ServerKeyFile               string
	CRLURLs                     []string
	TxLogMetadataHeaders        []string
	EnableGrantHashSuggestion   bool

	logoptions.LogOptions
	cmd.TLSGroupOptions
	cmd.TLSOptions
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.Name, "name", "", "", "Name of the outway. Every outway should have a unique name within the organization.")
	serveCommand.Flags().StringVarP(&serveOpts.GroupID, "group-id", "", "", "Group ID of the FSC Group")
	serveCommand.Flags().StringVarP(&serveOpts.ManagerInternalAddress, "manager-internal-address", "", "", "Manager address to communicate with for dealing with contracts.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddress, "listen-address", "", "127.0.0.1:8080", "Address for the outway monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().BoolVarP(&serveOpts.ListenHTTPS, "listen-https", "", false, "Enable HTTPS on the ListenAddress")
	serveCommand.Flags().StringVarP(&serveOpts.MonitoringAddress, "monitoring-address", "", "127.0.0.1:8081", "Address for the outway monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ControllerAPIAddress, "controller-api-address", "", "", "Address of the Controller API")
	serveCommand.Flags().StringVarP(&serveOpts.TxLogAPIAddress, "tx-log-api-address", "", "", "Address of the transaction log API")
	serveCommand.Flags().StringVarP(&serveOpts.AuthorizationServiceAddress, "authorization-service-address", "", "", "Address of the authorization service")
	serveCommand.Flags().StringVarP(&serveOpts.AuthorizationCA, "authorization-root-ca", "", "", "absolute path to root CA used to verify auth service certificate")
	serveCommand.Flags().StringVarP(&serveOpts.ServerCertFile, "tls-server-cert", "", "", "Path to a cert .pem, used for the HTTPS server")
	serveCommand.Flags().StringVarP(&serveOpts.ServerKeyFile, "tls-server-key", "", "", "Path to a key.pem, used for the HTTPS server")
	serveCommand.Flags().StringSliceVar(&serveOpts.CRLURLs, "crl-urls", []string{}, "List of URL's of Certificate Revocation Lists used to retrieve Certificate Revocation Lists")
	serveCommand.Flags().StringSliceVar(&serveOpts.TxLogMetadataHeaders, "tx-log-metadata-headers", []string{}, "List of HTTP headers to log as metadata")
	serveCommand.Flags().BoolVarP(&serveOpts.AuthorizationWithBody, "authorization-with-body", "", false, "When set to true, the HTTP request body (if available) will be send to the Authorization Server in base64 encoded format.")
	serveCommand.Flags().IntVarP(&serveOpts.AuthorizationMaxBodySize, "authorization-max-body-size", "", 0, "The maximum HTTP request body size in bytes that is allowed for sending to the authorization server. If a body exceeds this limits, the body is not send to the Authorization Server.")
	serveCommand.Flags().IntVarP(&serveOpts.AuthorizationChunkSize, "authorization-body-chunk-size", "", 0, "The chunk size in bytes that is used to process each HTTP request body chunk.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "info", "Set loglevel")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupRootCert, "tls-group-root-cert", "", "", "Absolute or relative path to the NLX CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupCertFile, "tls-group-cert", "", "", "Absolute or relative path to the FSC Group cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupKeyFile, "tls-group-key", "", "", "Absolute or relative path to the FSC Group key .pem")
	serveCommand.Flags().BoolVarP(&serveOpts.EnableGrantHashSuggestion, "enable-grant-hash-suggestion", "", false, "Enable valid Grant Hashes in output when no Fsc-Grant-Hash header has been provided")

	if err := serveCommand.MarkFlagRequired("name"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("group-id"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("manager-internal-address"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("controller-api-address"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tx-log-api-address"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the Outway Server",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		// Setup new zap logger
		logger, err := zaplogger.New(serveOpts.LogLevel, serveOpts.LogType)
		if err != nil {
			log.Fatalf("failed to create new zap logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version %s, source-hash %s", version.BuildVersion, version.BuildSourceHash))
		logger = logger.With("version", version.BuildVersion)

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.GroupKeyFile); errValidate != nil {
			logger.Warn(fmt.Sprintf("invalid organization key permission. file-path: %s", serveOpts.GroupCertFile), errValidate)
		}

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn(fmt.Sprintf("invalid internal PKI key permissions. file-path: %s", serveOpts.KeyFile), errValidate)
		}

		groupCert, err := common_tls.NewBundleFromFiles(serveOpts.GroupCertFile, serveOpts.GroupKeyFile, serveOpts.GroupRootCert)
		if err != nil {
			logger.Fatal("unable to load organization certificate and key", err)
		}

		internalCert, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("unable to load internal PKI certificate and key", err)
		}

		txlogClient, err := txlogapi.NewClientWithResponses(serveOpts.TxLogAPIAddress, func(c *txlogapi.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = internalCert.TLSConfig()

			c.Client = &http.Client{Transport: t}
			return nil
		})
		if err != nil {
			logger.Fatal("could not create txlog client", err)
		}

		txLogger, err := transactionlog.NewRestAPITransactionLogger(&transactionlog.NewRestAPITransactionLoggerArgs{
			Logger: logger,
			Client: txlogClient,
		})
		if err != nil {
			logger.Fatal("unable to setup the transaction logger", err)
		}

		var serverCertificate *tls.Certificate

		if serveOpts.ListenHTTPS {
			if serveOpts.ServerCertFile == "" || serveOpts.ServerKeyFile == "" {
				logger.Fatal("unable to start", errors.New("server certificate and key are required"))
			}

			cert, certErr := tls.LoadX509KeyPair(serveOpts.ServerCertFile, serveOpts.ServerKeyFile)
			if certErr != nil {
				logger.Fatal("failed to load server certificate", err)
			}

			serverCertificate = &cert
		}

		controllerClient, err := controllerapi.NewClientWithResponses(serveOpts.ControllerAPIAddress, func(c *controllerapi.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = internalCert.TLSConfig()

			c.Client = &http.Client{Transport: t}
			return nil
		})
		if err != nil {
			logger.Fatal("could not create rest controller client", err)
		}

		restController, err := restcontroller.New(controllerClient)
		if err != nil {
			logger.Fatal("could not create rest controller", err)
		}

		managerInternalClient, err := api.NewClientWithResponses(serveOpts.ManagerInternalAddress, func(c *api.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = internalCert.TLSConfig()

			c.Client = &http.Client{Transport: t}

			return nil
		})
		if err != nil {
			logger.Fatal("could not create manager internal client", err)
		}

		crl, err := common_tls.NewCRL(serveOpts.CRLURLs)
		if err != nil {
			logger.Fatal("cannot create crl cache", err)
		}

		restConfigRepository, err := restconfig.New(context.Background(), managerInternalClient, groupCert, crl)
		if err != nil {
			logger.Fatal("failed to setup manager rest config repository", err)
		}

		txLogProbe := health.TxLogAPIProbe(internalCert, serveOpts.TxLogAPIAddress)
		outwayProbe := health.OutwayProbe(serverCertificate, serveOpts.ListenAddress)

		monitoring := &outway.Monitoring{
			Address: serveOpts.MonitoringAddress,
			ReadinessProbes: map[string]monitoring.Check{
				"Readiness Probe TxLog API": txLogProbe,
				"Readiness Probe Outway":    outwayProbe,
			},
			LivenessProbes: map[string]monitoring.Check{
				"Liveness Probe TxLog API": txLogProbe,
				"Liveness Probe Outway":    outwayProbe,
			},
		}

		ow, err := outway.New(&outway.NewOutwayArgs{
			Clock:                     clock.New(),
			GroupID:                   serveOpts.GroupID,
			Name:                      serveOpts.Name,
			Ctx:                       context.Background(),
			Logger:                    logger,
			Txlogger:                  txLogger,
			Controller:                restController,
			Monitoring:                monitoring,
			ExternalCert:              groupCert,
			InternalCert:              internalCert,
			ConfigRepository:          restConfigRepository,
			AuthServiceURL:            serveOpts.AuthorizationServiceAddress,
			AuthCAPath:                serveOpts.AuthorizationCA,
			AuthWithBody:              serveOpts.AuthorizationWithBody,
			AuthMaxBodySize:           serveOpts.AuthorizationMaxBodySize,
			AuthBodyChunkSize:         serveOpts.AuthorizationChunkSize,
			CrlCache:                  crl,
			TxLogMetadataHeaders:      serveOpts.TxLogMetadataHeaders,
			EnableGrantHashSuggestion: serveOpts.EnableGrantHashSuggestion,
		})
		if err != nil {
			logger.Fatal("failed to initialize the outway", err)
		}

		ctxAnnouncementsCancel, cancelAnnouncements := context.WithCancel(context.Background())
		go func() {
			err = ow.Run(ctxAnnouncementsCancel)
			if err != nil {
				logger.Fatal("error running outway", err)
			}

			err = ow.RunServer(serveOpts.ListenAddress, serverCertificate)
			if err != nil {
				logger.Fatal("error running outway server", err)
			}
		}()

		p.Wait()

		logger.Info("starting graceful shutdown")
		cancelAnnouncements()

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		ow.Shutdown(gracefulCtx)

		err = restConfigRepository.Close()
		if err != nil {
			logger.Error("could not close config repository", err)
		}
	},
}
