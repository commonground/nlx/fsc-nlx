// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package main

import (
	"log"

	_ "github.com/lib/pq"

	common_cmd "gitlab.com/commonground/nlx/fsc-nlx/common/cmd"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/cmd"
)

func main() {
	err := common_cmd.SetupFlagsForEnvironment(cmd.RootCmd)
	if err != nil {
		log.Fatalf("error parsing flags: %v", err)
	}

	if err := cmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
