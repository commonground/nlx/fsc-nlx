// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package outway_test

import (
	"fmt"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/outway"
	mock_repository "gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config/mock"
	common_testing "gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var pkiDir = filepath.Join("..", "testing", "pki")

// nolint:funlen // this is a test
func TestSetupNewOutway(t *testing.T) {
	clck := common_testing.NewMockClock(time.Date(2023, 11, 6, 14, 10, 5, 0, time.UTC))

	orgCertWithoutName, err := common_testing.GetCertificateBundle(pkiDir, common_testing.OrgWithoutName)
	require.NoError(t, err)

	orgCert, err := common_testing.GetCertificateBundle(pkiDir, common_testing.NLXTestPeerA)
	require.NoError(t, err)

	orgCertWithoutSerialNumber, err := common_testing.GetCertificateBundle(pkiDir, common_testing.OrgWithoutSerialNumber)
	require.NoError(t, err)

	internalCert, err := common_testing.GetCertificateBundle(pkiDir, common_testing.NLXTestInternal)
	require.NoError(t, err)

	monitoring := &outway.Monitoring{
		Address:         "localhost:8080",
		ReadinessProbes: nil,
		LivenessProbes:  nil,
	}

	tests := map[string]struct {
		args            *outway.NewOutwayArgs
		wantError       error
		wantPluginCount int
	}{
		"certificate_without_organization": {
			args: &outway.NewOutwayArgs{
				Clock:            clck,
				Logger:           discardlogger.New(),
				Name:             "outway-1",
				GroupID:          "fsc-local",
				ExternalCert:     orgCertWithoutName,
				InternalCert:     internalCert,
				Monitoring:       monitoring,
				AuthServiceURL:   "",
				AuthCAPath:       "",
				ConfigRepository: mock_repository.NewMockRepository(t),
			},
			wantError: fmt.Errorf("cannot obtain organization name from self cert"),
		},
		"certificate_without_organization_serial_number": {
			args: &outway.NewOutwayArgs{
				Clock:            clck,
				Logger:           discardlogger.New(),
				Name:             "outway-1",
				GroupID:          "fsc-local",
				ExternalCert:     orgCertWithoutSerialNumber,
				InternalCert:     internalCert,
				Monitoring:       monitoring,
				AuthServiceURL:   "",
				AuthCAPath:       "",
				ConfigRepository: mock_repository.NewMockRepository(t),
			},
			wantError: fmt.Errorf("validation error for subject serial number from cert: cannot be empty"),
		},
		"authorization_service_URL_set_but_no_CA_for_authorization_provided": {
			args: &outway.NewOutwayArgs{
				Logger:           discardlogger.New(),
				Name:             "outway-1",
				GroupID:          "fsc-local",
				Clock:            clck,
				ExternalCert:     orgCert,
				InternalCert:     internalCert,
				Monitoring:       monitoring,
				AuthServiceURL:   "http://auth.nlx.io",
				AuthCAPath:       "",
				ConfigRepository: mock_repository.NewMockRepository(t),
			},
			wantError: fmt.Errorf("authorization service URL set but no CA for authorization provided"),
		},
		"authorization_service_URL_is_not_'https'": {
			args: &outway.NewOutwayArgs{
				Logger:           discardlogger.New(),
				Name:             "outway-1",
				GroupID:          "fsc-local",
				Clock:            clck,
				ExternalCert:     orgCert,
				InternalCert:     internalCert,
				Monitoring:       monitoring,
				AuthServiceURL:   "http://auth.nlx.io",
				AuthCAPath:       "/path/to",
				ConfigRepository: mock_repository.NewMockRepository(t),
			},
			wantError: fmt.Errorf("scheme of authorization service URL is not 'https'"),
		},
		"invalid_monitioring_service_address": {
			args: &outway.NewOutwayArgs{
				Logger:       discardlogger.New(),
				Name:         "test-outway",
				GroupID:      "fsc-local",
				Clock:        clck,
				ExternalCert: orgCert,
				InternalCert: internalCert,
				Monitoring: &outway.Monitoring{
					Address:         "",
					ReadinessProbes: nil,
					LivenessProbes:  nil,
				},
				AuthServiceURL:   "",
				AuthCAPath:       "",
				ConfigRepository: mock_repository.NewMockRepository(t),
			},
			wantError: fmt.Errorf("unable to create monitoring service: address required"),
		},
		"missing_monitioring_service": {
			args: &outway.NewOutwayArgs{
				Logger:           discardlogger.New(),
				Name:             "test-outway",
				GroupID:          "fsc-local",
				Clock:            clck,
				ExternalCert:     orgCert,
				InternalCert:     internalCert,
				Monitoring:       nil,
				AuthServiceURL:   "",
				AuthCAPath:       "",
				ConfigRepository: mock_repository.NewMockRepository(t),
			},
			wantError: fmt.Errorf("missing monitoring configuration"),
		},
		"happy_flow_with_authorization_plugin": {
			args: &outway.NewOutwayArgs{
				Logger:           discardlogger.New(),
				Name:             "test-outway",
				GroupID:          "fsc-local",
				Clock:            clock.New(),
				ExternalCert:     orgCert,
				InternalCert:     internalCert,
				Monitoring:       monitoring,
				AuthServiceURL:   "https://auth.nlx.io",
				AuthCAPath:       "../testing/pki/ca-root.pem",
				ConfigRepository: mock_repository.NewMockRepository(t),
			},
			wantError:       nil,
			wantPluginCount: 3,
		},
		"happy_flow": {
			args: &outway.NewOutwayArgs{
				Logger:           discardlogger.New(),
				Name:             "outway-1",
				GroupID:          "fsc-local",
				ExternalCert:     orgCert,
				InternalCert:     internalCert,
				Monitoring:       monitoring,
				Clock:            clock.New(),
				ConfigRepository: mock_repository.NewMockRepository(t),
			},
			wantError:       nil,
			wantPluginCount: 2,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			ow, err := outway.New(tt.args)

			if tt.wantError != nil {
				assert.Equal(t, tt.wantError.Error(), err.Error())
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, ow)
				assert.Equal(t, tt.wantPluginCount, ow.PluginCount())
			}
		})
	}
}
