// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package http

import (
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
)

func WriteError(w http.ResponseWriter, location httperrors.Location, nlxErr *httperrors.FSCNetworkError) {
	nlxErr.Location = location
	nlxErr.Source = httperrors.Outway
	httperrors.WriteError(w, nlxErr)
}
