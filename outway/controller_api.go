// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package outway

import (
	"context"

	"github.com/cenkalti/backoff/v4"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/adapters/controller"
)

func (o *Outway) announceToControllerAPI(ctx context.Context) {
	register := func() error {
		err := o.controller.RegisterOutway(ctx, &controller.RegisterOutwayArgs{
			GroupID:             o.groupID.String(),
			Name:                o.name,
			PublicKeyThumbprint: o.externalCert.PublicKeyThumbprint(),
		})
		if err != nil {
			o.logger.Error("failed to register to controller api", err)

			return err
		}

		o.logger.Info("controller api registration successful")

		return nil
	}

	err := backoff.Retry(register, backoff.WithContext(backoff.NewExponentialBackOff(), ctx))
	if err != nil {
		o.logger.Error("permanently failed to register to controller api", err)
	}
}
