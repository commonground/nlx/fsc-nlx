// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package outway_test

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/outway"
	mock_repository "gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config/mock"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type testCases map[string]struct {
	url        string
	statusCode int
	grantHash  string
	assertions []func(*testing.T) bool
	wantErr    *httperrors.FSCNetworkError
}

func testRequests(t *testing.T, tests testCases) {
	client := http.Client{}

	for name, tt := range tests {
		tc := tt

		t.Run(name, func(t *testing.T) {
			req, err := http.NewRequest("GET", tc.url, http.NoBody)
			if err != nil {
				t.Fatal("error creating http request", err)
			}

			req.Header.Set("Fsc-Grant-Hash", tc.grantHash)

			resp, err := client.Do(req)
			if err != nil {
				t.Fatal("error making http request", err)
			}
			defer resp.Body.Close()

			assert.Equal(t, tc.statusCode, resp.StatusCode)
			contents, err := io.ReadAll(resp.Body)

			if err != nil {
				t.Fatal("error reading response body", err)
			}

			if tc.wantErr == nil {
				for _, assertion := range tc.assertions {
					assert.True(t, assertion(t))
				}
			}

			if tc.wantErr != nil {
				gotError := &httperrors.FSCNetworkError{}
				err = json.Unmarshal(contents, gotError)
				assert.NoError(t, err)

				assert.Equal(t, tc.wantErr.Code, gotError.Code)
				assert.Equal(t, tc.wantErr.Message, gotError.Message)
				assert.Equal(t, tc.wantErr.Location, gotError.Location)
				assert.Equal(t, tc.wantErr.Source, gotError.Source)
				assert.Equal(t, tc.wantErr.Code.String(), resp.Header.Get("Fsc-Error-Code"))

				if tc.wantErr.Metadata != nil {
					assert.True(t, compareMetadata(t, gotError.Metadata, tc.wantErr.Metadata))
				}
			}
		})
	}
}

func compareMetadata(t *testing.T, gotMetadata, wantMetadata any) bool {
	gotServices, ok := gotMetadata.(map[string]interface{})
	if !ok {
		return false
	}

	wantServices, ok := wantMetadata.(map[string][]interface{})
	if !ok {
		return false
	}

	return assert.ElementsMatch(t, gotServices["services"], wantServices["services"])
}

//nolint:funlen,dupl // this is a test
func TestCases(t *testing.T) {
	grantHashHappyFlow := "$1$4$k4rwlWTsCM_j89Fc3nrbnQa9SkYXK8oAp2j1YA34D0WkqZt7V3Gxr-KB43GGv47YhxadenFdW2xJI8CpBJSsLQ"
	grantHashHappyFlowDelegation := "$1$4$gIZln-SrWEKBBQhiu1orGfctBWvcVXRBDzU97BoV3rw7FiyDJvLsbO101BcoyfdXvWcQvpONmWSFdweS9gHngA"
	grantHashFailingService := "$1$4$FUPy-aW2ho6DkQh-4APucItP9ljUCWwVmEXto3PUxnZyzFb-yXGMi1IY1VptJvZ2biXJLYVcHOOOFR--HBgeJg"
	grantHashUnreachableService := "$1$4$Gkui-H_i073NgI-yLuP6pZXPlfjFOx2l6dAUD6Cy8JUvLrn5-ukLJujFETPRfCZKK9P8QqL3EFJsFF_tQxOR0Q"
	grantHashRevokedCertificate := "$1$4$Gkui-H_i073NgI-yLuP6pZXPlfjFOx2l6dAUD6Cy8JUvLrn5-ukLJujFETPRfCZKK9P8QqL3EFJsFG_tQxOR0R"
	testPath := "/00000000000000000001/"

	var apiServerRequestReceivedAt time.Time

	testService := newMockAPI(t, &apiServerRequestReceivedAt, testPath, grantHashHappyFlow)
	defer testService.Close()

	testServiceRevokedCert := newMockAPIRevokedCertificate()
	defer testServiceRevokedCert.Close()

	mockServices := []*mockService{
		{
			GrantHash: grantHashHappyFlow,
			Name:      "service-happy-flow-testing",
			PeerID:    orgACertBundle.GetPeerInfo().SerialNumber,
			Server:    newMockAPI(t, &apiServerRequestReceivedAt, testPath, grantHashHappyFlow),
		},
		{
			GrantHash:        grantHashHappyFlowDelegation,
			Name:             "service-happy-flow-testing-delegation",
			PeerID:           orgACertBundle.GetPeerInfo().SerialNumber,
			outwayDelegator:  orgBCertBundle.GetPeerInfo(),
			serviceDelegator: orgCCertBundle.GetPeerInfo(),
			Server:           newMockAPI(t, &apiServerRequestReceivedAt, testPath, grantHashHappyFlowDelegation),
		},
		{
			GrantHash: grantHashFailingService,
			Name:      "failing-service",
			PeerID:    orgACertBundle.GetPeerInfo().SerialNumber,
			Server:    newErrorMockAPI(),
		},
		{
			GrantHash: grantHashUnreachableService,
			Name:      "unreachable-service",
			PeerID:    orgACertBundle.GetPeerInfo().SerialNumber,
			Server: &httptest.Server{
				URL: "https://idonotexist",
			},
		},
		{
			GrantHash: grantHashRevokedCertificate,
			Name:      "Inway-certificate-revoked",
			PeerID:    orgACertBundle.GetPeerInfo().SerialNumber,
			Server:    testServiceRevokedCert,
		},
	}

	transactionLogger := newFakeTransactionLogger()

	repository := newFakeRepository(mockServices)

	testOutway := newOutway(repository, transactionLogger, nil, true)
	defer testOutway.Close()

	testOutwayGrantHashSuggestionsDisabled := newOutway(repository, transactionLogger, nil, false)
	defer testOutwayGrantHashSuggestionsDisabled.Close()

	testOutwayBrokenTxLogger := newOutway(repository, &errorTransactionLogger{}, nil, true)
	defer testOutwayBrokenTxLogger.Close()

	testOutwayRevokedCertificate := newOutwayWithCRLCache(repository, transactionLogger)
	defer testOutwayRevokedCertificate.Close()

	tests := testCases{
		"when_request_to_inway_fails": { // testcase Outway-IncomingRequest-3
			url:        fmt.Sprintf("%s/00000000000000000001/", testOutway.URL),
			statusCode: http.StatusBadGateway,
			grantHash:  grantHashUnreachableService,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.O1,
				Code:     httperrors.ServiceUnreachableErr,
				Message:  "failed API request to https://idonotexist/00000000000000000001/ try again later. service api down/unreachable. check error at https://docs.nlx.io/support/common-errors/",
			},
		},
		"when_request_to_outway_contains_unknown_grant_hash": { // testcase: Outway-IncomingRequest-2
			url:        fmt.Sprintf("%s/00000000000000000001/", testOutway.URL),
			statusCode: http.StatusBadRequest,
			grantHash:  "idonotexist",
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.C1,
				Code:     httperrors.UnknownGrantHashInHeaderErr,
				Message:  "unknown grant hash: \"idonotexist\" in Fsc-Grant-Hash header",
			},
		},
		"when_request_to_outway_contains_no_grant_hash": {
			url:        fmt.Sprintf("%s/00000000000000000001/", testOutway.URL),
			statusCode: http.StatusBadRequest,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.C1,
				Code:     httperrors.UnknownGrantHashInHeaderErr,
				Message:  "unknown grant hash: \"\" in Fsc-Grant-Hash header",
				Metadata: map[string][]interface{}{
					"services": {
						map[string]interface{}{
							"GrantHash": "$1$4$Gkui-H_i073NgI-yLuP6pZXPlfjFOx2l6dAUD6Cy8JUvLrn5-ukLJujFETPRfCZKK9P8QqL3EFJsFG_tQxOR0R",
							"Name":      "Inway-certificate-revoked",
							"PeerID":    "00000000000000000001",
						},
						map[string]interface{}{
							"GrantHash": "$1$4$k4rwlWTsCM_j89Fc3nrbnQa9SkYXK8oAp2j1YA34D0WkqZt7V3Gxr-KB43GGv47YhxadenFdW2xJI8CpBJSsLQ",
							"Name":      "service-happy-flow-testing",
							"PeerID":    "00000000000000000001",
						},
						map[string]interface{}{
							"GrantHash": "$1$4$gIZln-SrWEKBBQhiu1orGfctBWvcVXRBDzU97BoV3rw7FiyDJvLsbO101BcoyfdXvWcQvpONmWSFdweS9gHngA",
							"Name":      "service-happy-flow-testing-delegation",
							"PeerID":    "00000000000000000001",
						},
						map[string]interface{}{
							"GrantHash": "$1$4$FUPy-aW2ho6DkQh-4APucItP9ljUCWwVmEXto3PUxnZyzFb-yXGMi1IY1VptJvZ2biXJLYVcHOOOFR--HBgeJg",
							"Name":      "failing-service",
							"PeerID":    "00000000000000000001",
						},
						map[string]interface{}{
							"GrantHash": "$1$4$Gkui-H_i073NgI-yLuP6pZXPlfjFOx2l6dAUD6Cy8JUvLrn5-ukLJujFETPRfCZKK9P8QqL3EFJsFF_tQxOR0Q",
							"Name":      "unreachable-service",
							"PeerID":    "00000000000000000001",
						},
					},
				},
			},
		},
		"when_request_to_outway_contains_no_grant_hash_suggestions_disabled": {
			url:        fmt.Sprintf("%s/00000000000000000001/", testOutwayGrantHashSuggestionsDisabled.URL),
			statusCode: http.StatusBadRequest,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.C1,
				Code:     httperrors.UnknownGrantHashInHeaderErr,
				Message:  "unknown grant hash: \"\" in Fsc-Grant-Hash header",
			},
		},
		"when_txlog_write_fails": { // testcase: Outway-TransactionLog-5
			url:        fmt.Sprintf("%s/00000000000000000001/", testOutwayBrokenTxLogger.URL),
			statusCode: http.StatusInternalServerError,
			grantHash:  grantHashFailingService,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.C1,
				Code:     httperrors.LogRecordWriteErr,
				Message:  "The TransactionLog record could not be created",
			},
		},
		"happy_flow": { // testcase: Outway-TransactionLog-1, Outway-IncomingRequest-1
			url:        fmt.Sprintf("%s%s", testOutway.URL, testPath),
			statusCode: http.StatusOK,
			grantHash:  grantHashHappyFlow,
			assertions: []func(*testing.T) bool{
				func(t *testing.T) bool {
					return assert.True(t, transactionLogger.time.Before(apiServerRequestReceivedAt))
				},
				func(t *testing.T) bool {
					return assert.Equal(t, orgBCertBundle.GetPeerInfo().SerialNumber, transactionLogger.GetRecords(grantHashHappyFlow)[0].Source.(*transactionlog.RecordSource).OutwayPeerID)
				},
				func(t *testing.T) bool {
					return assert.Equal(t, orgACertBundle.GetPeerInfo().SerialNumber, transactionLogger.GetRecords(grantHashHappyFlow)[0].Destination.(*transactionlog.RecordDestination).ServicePeerID)
				},
				func(t *testing.T) bool {
					return assert.Equal(t, "service-happy-flow-testing", transactionLogger.GetRecords(grantHashHappyFlow)[0].ServiceName)
				},
				func(t *testing.T) bool {
					return assert.Equal(t, grantHashHappyFlow, transactionLogger.GetRecords(grantHashHappyFlow)[0].GrantHash)
				},
				func(t *testing.T) bool {
					return assert.Equal(t, record.DirectionOut, transactionLogger.GetRecords(grantHashHappyFlow)[0].Direction)
				},
				func(t *testing.T) bool {
					return assert.Equal(t, "fsc-local", transactionLogger.GetRecords(grantHashHappyFlow)[0].GroupID)
				},
				func(t *testing.T) bool {
					return assert.Equal(t, "/00000000000000000001/", transactionLogger.GetRecords(grantHashHappyFlow)[0].Data["request-path"])
				},
			},
		},
		"happy_flow_delegated_service_connection": { // testcase: Outway-TransactionLog-3
			url:        fmt.Sprintf("%s%s", testOutway.URL, testPath),
			statusCode: http.StatusOK,
			grantHash:  grantHashHappyFlowDelegation,
			assertions: []func(*testing.T) bool{
				func(t *testing.T) bool {
					return assert.True(t, transactionLogger.time.Before(apiServerRequestReceivedAt))
				},
				func(t *testing.T) bool {
					return assert.Equal(t, orgBCertBundle.GetPeerInfo().SerialNumber, transactionLogger.GetRecords(grantHashHappyFlowDelegation)[0].Source.(*transactionlog.RecordDelegatedSource).OutwayPeerID)
				},
			},
		},
		"happy_flow_delegated_service_publication": { // testcase: Outway-TransactionLog-4
			url:        fmt.Sprintf("%s%s", testOutway.URL, testPath),
			statusCode: http.StatusOK,
			grantHash:  grantHashHappyFlowDelegation,
			assertions: []func(*testing.T) bool{
				func(t *testing.T) bool {
					return assert.True(t, transactionLogger.time.Before(apiServerRequestReceivedAt))
				},
				func(t *testing.T) bool {
					return assert.Equal(t, orgACertBundle.GetPeerInfo().SerialNumber, transactionLogger.GetRecords(grantHashHappyFlowDelegation)[0].Destination.(*transactionlog.RecordDelegatedDestination).ServicePeerID)
				},
			},
		},
		"revoked_certificate": { // testcase: Outway-TransactionLog-1, Outway-IncomingRequest-1
			url:        fmt.Sprintf("%s%s", testOutwayRevokedCertificate.URL, testPath),
			statusCode: http.StatusBadRequest,
			grantHash:  grantHashRevokedCertificate,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.O1,
				Code:     httperrors.CertificateRevokedErr,
				Message:  fmt.Sprintf("client used a revoked certificate: certificate with serialnumber: %s is present on a Certificate Revocation List", orgOnCRL.Certificate().SerialNumber),
			},
		},
	}

	testRequests(t, tests)
}

func newMockAPI(t *testing.T, apiServerRequestReceivedAt *time.Time, testPath, grantHash string) *httptest.Server {
	return httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			*apiServerRequestReceivedAt = time.Now()
			_, err := uuid.FromString(r.Header.Get("Fsc-Transaction-Id")) // testcase: Outway-TransactionLog-2
			assert.NoError(t, err)

			w.WriteHeader(http.StatusOK)
			assert.Equal(t, testPath, r.RequestURI)
			assert.NotNil(t, r.Header.Get("Fsc-Authorization"))
			assert.Equal(t, grantHash, r.Header.Get("Fsc-Grant-Hash"))
		}))
}

// Certain test scenarios mandate the test server not to be called. This is forced by throwing an error
func newErrorMockAPI() *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Fatal("this test server should not be called")
	}))
}

func newMockAPIRevokedCertificate() *httptest.Server {
	testServer := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Fatal("this test server should not be called")
	}))

	testServer.TLS = &tls.Config{
		MinVersion: tls.VersionTLS12,
		Certificates: []tls.Certificate{
			{
				Certificate: orgOnCRL.Cert().Certificate,
				PrivateKey:  orgOnCRL.PrivateKey(),
			},
		},
		ClientCAs: orgACertBundle.RootCAs(),
	}

	testServer.StartTLS()

	return testServer
}

func TestRunServer(t *testing.T) {
	t.Parallel()

	logger := discardlogger.New()

	certificate, _ := tls.LoadX509KeyPair(
		filepath.Join("..", "testing", "pki", "org-nlx-test-chain.pem"),
		filepath.Join("..", "testing", "pki", "org-nlx-test-key.pem"),
	)

	tests := map[string]struct {
		listenAddress            string
		monitoringServiceAddress string
		certificate              *tls.Certificate
		errorMessage             string
	}{
		"invalid_listen_address": {
			listenAddress:            "invalid",
			monitoringServiceAddress: "localhost:8081",
			certificate:              nil,
			errorMessage:             "error listening on server\nlisten tcp: address invalid: missing port in address",
		},
		"invalid_listen_address with TLS": {
			listenAddress:            "invalid",
			monitoringServiceAddress: "localhost:8082",
			certificate:              &certificate,
			errorMessage:             "error listening on server\nlisten tcp: address invalid: missing port in address",
		},
	}

	for name, tt := range tests {
		tc := tt

		t.Run(name, func(t *testing.T) {
			monitorService, err := monitoring.NewMonitoringService(tc.monitoringServiceAddress, logger, nil, nil)
			assert.Nil(t, err)

			o, err := outway.New(&outway.NewOutwayArgs{
				Logger:       logger,
				Name:         "outway-1",
				GroupID:      "fsc-local",
				Clock:        testClock,
				ExternalCert: orgBCertBundle,
				Monitoring: &outway.Monitoring{
					Address:         tc.monitoringServiceAddress,
					ReadinessProbes: nil,
					LivenessProbes:  nil,
				},
				ConfigRepository: mock_repository.NewMockRepository(t),
			})
			assert.NoError(t, err)

			err = o.RunServer(tc.listenAddress, tc.certificate)
			assert.EqualError(t, err, tc.errorMessage)

			err = monitorService.Stop()
			assert.NoError(t, err)
		})
	}
}

//func TestEnableGrantHashSuggestion(t *testing.T) {
//	testOutway := newOutway(repository, transactionLogger, nil, true)
//	defer testOutway.Close()
//}
