// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"context"
	"fmt"
	"sync"

	"golang.org/x/sync/singleflight"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
)

type Tokens map[string]*TokenInfo

type TokenInfo struct {
	Token string
	*accesstoken.UnsafeDecodedToken
}

type tokens struct {
	tokens   Tokens
	mut      *sync.RWMutex
	getToken *singleflight.Group
}

type Services map[string]*Service
type ServicesConnectionInfo map[string]*ServiceConnectionInfo

type Service struct {
	Name   string
	PeerID string
}

type ServiceConnectionInfo struct {
	OutwayDelegatorPeerID     string
	OutwayPeerID              string
	OutwayPublicKeyThumbprint string
	ServicePeerID             string
	ServicePeerManagerAddress string
	ServiceName               string
	ServiceDelegatorPeerID    string
}

type servicesConnectionInfo struct {
	servicesConnectionInfo   ServicesConnectionInfo
	mut                      *sync.RWMutex
	getServiceConnectionInfo *singleflight.Group
}

type Config struct {
	clock                       clock.Clock
	repo                        Repository
	OutwayCertificateThumbprint string
	servicesConnectionInfo      *servicesConnectionInfo
	tokens                      *tokens
}

func New(c clock.Clock, outwayCertificateThumbprint string, repo Repository) (*Config, error) {
	if repo == nil {
		return nil, fmt.Errorf("repo is required")
	}

	if c == nil {
		return nil, fmt.Errorf("clock is required")
	}

	if outwayCertificateThumbprint == "" {
		return nil, fmt.Errorf("outwayCertificateThumbprint is required")
	}

	return &Config{
		repo: repo,
		servicesConnectionInfo: &servicesConnectionInfo{
			servicesConnectionInfo:   make(ServicesConnectionInfo),
			mut:                      &sync.RWMutex{},
			getServiceConnectionInfo: &singleflight.Group{},
		},
		tokens: &tokens{
			tokens:   make(Tokens),
			mut:      &sync.RWMutex{},
			getToken: &singleflight.Group{},
		},
		OutwayCertificateThumbprint: outwayCertificateThumbprint,
		clock:                       c,
	}, nil
}

func (c *Config) getServiceConnectionInfo(ctx context.Context, grantHash string) (*ServiceConnectionInfo, error) {
	service, ok := c.servicesConnectionInfo.Get(grantHash)
	if ok {
		return service, nil
	}

	res, err, _ := c.servicesConnectionInfo.getServiceConnectionInfo.Do(grantHash, func() (interface{}, error) {
		s, err := c.repo.GetServiceConnectionInfo(ctx, grantHash)
		if err != nil {
			return nil, err
		}

		c.servicesConnectionInfo.Set(grantHash, s)

		return s, nil
	})
	if err != nil {
		return nil, err
	}

	return res.(*ServiceConnectionInfo), nil
}

func (c *Config) getToken(ctx context.Context, peerID, managerAddress, grantHash string) (*TokenInfo, error) {
	if managerAddress == "" {
		return nil, fmt.Errorf("managerAddress is required")
	}

	if grantHash == "" {
		return nil, fmt.Errorf("grantHash is required")
	}

	token, ok := c.tokens.Get(grantHash)
	if ok && token.ExpiryDate.After(c.clock.Now()) {
		return token, nil
	}

	res, err, _ := c.tokens.getToken.Do(grantHash, func() (interface{}, error) {
		encodedToken, err := c.repo.GetToken(ctx, peerID, managerAddress, grantHash)
		if err != nil {
			return nil, err
		}

		unsafeDecodedToken, err := accesstoken.DecodeUnsafeFromString(encodedToken)
		if err != nil {
			return nil, fmt.Errorf("cannot decode token: %w", err)
		}

		tokenInfo := &TokenInfo{
			Token:              encodedToken,
			UnsafeDecodedToken: unsafeDecodedToken,
		}

		c.tokens.Set(grantHash, tokenInfo)

		return tokenInfo, nil
	})
	if err != nil {
		return nil, err
	}

	return res.(*TokenInfo), nil
}

func (c *Config) GetTokenInfo(ctx context.Context, grantHash string) (*TokenInfo, error) {
	serviceConnectionInfo, err := c.getServiceConnectionInfo(ctx, grantHash)
	if err != nil {
		return nil, err
	}

	token, err := c.getToken(ctx, serviceConnectionInfo.ServicePeerID, serviceConnectionInfo.ServicePeerManagerAddress, grantHash)
	if err != nil {
		return nil, err
	}

	err = validateTokenInfo(grantHash, c.OutwayCertificateThumbprint, serviceConnectionInfo, token)
	if err != nil {
		return nil, fmt.Errorf("invalid token: %w", err)
	}

	return token, nil
}

func validateTokenInfo(grantHash, outwayCertificateThumbprint string, s *ServiceConnectionInfo, t *TokenInfo) error {
	if grantHash != t.GrantHash {
		return fmt.Errorf("invalid grantHash: expected: %q, got: %q", grantHash, t.GrantHash)
	}

	if s.ServiceName != t.ServiceName {
		return fmt.Errorf("invalid serviceName: expected: %q, got: %q", s.ServiceName, t.ServiceName)
	}

	if s.ServicePeerID != t.ServicePeerID {
		return fmt.Errorf("invalid servicePeerID: expected: %q, got: %q", s.ServicePeerID, t.ServicePeerID)
	}

	if s.ServiceDelegatorPeerID != t.ServiceDelegatorPeerID {
		return fmt.Errorf("invalid serviceDelegatorPeerID: expected: %q, got: %q", s.ServiceDelegatorPeerID, t.ServiceDelegatorPeerID)
	}

	if s.OutwayPeerID != t.OutwayPeerID {
		return fmt.Errorf("invalid outwayPeerID: expected: %q, got: %q", s.OutwayPeerID, t.OutwayPeerID)
	}

	if s.OutwayDelegatorPeerID != t.OutwayDelegatorPeerID {
		return fmt.Errorf("invalid outwayDelegatorPeerID: expected: %q, got: %q", s.OutwayDelegatorPeerID, t.OutwayDelegatorPeerID)
	}

	if outwayCertificateThumbprint != t.OutwayCertificateThumbprint {
		return fmt.Errorf("invalid outwayCertificateThumbprint: expected: %q, got: %q", outwayCertificateThumbprint, t.OutwayCertificateThumbprint)
	}

	return nil
}

func (c *Config) GetServices(ctx context.Context) (Services, error) {
	return c.repo.GetServices(ctx)
}

func (s *servicesConnectionInfo) Get(grantHash string) (*ServiceConnectionInfo, bool) {
	s.mut.RLock()
	res, ok := s.servicesConnectionInfo[grantHash]
	s.mut.RUnlock()

	return res, ok
}

func (s *servicesConnectionInfo) Set(grantHash string, service *ServiceConnectionInfo) {
	s.mut.Lock()
	s.servicesConnectionInfo[grantHash] = service
	s.mut.Unlock()
}

func (t *tokens) Get(grantHash string) (*TokenInfo, bool) {
	t.mut.RLock()
	res, ok := t.tokens[grantHash]
	t.mut.RUnlock()

	return res, ok
}

func (t *tokens) Set(grantHash string, tokenInfo *TokenInfo) {
	t.mut.Lock()
	t.tokens[grantHash] = tokenInfo
	t.mut.Unlock()
}
