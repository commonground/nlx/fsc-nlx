// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package config_test

import (
	"context"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
	mock_repository "gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config/mock"
	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestConfig_GetTokenInfo_ShouldNotReuseExpiredToken(t *testing.T) {
	ctx := context.Background()
	testClock := testingutils.NewMockClock(time.Now())
	mockRepo := mock_repository.NewMockRepository(t)

	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	assert.NoError(t, err)

	const sha3_512 = 1

	testGrantHash, err := testhash.GenerateServiceConnectionGrantHash(&testhash.ServiceConnectionGrantArgs{
		ContractContentIV:                 "495d29af-8004-4add-a329-9c0327d10f05",
		ContractGroupID:                   "fsc-test",
		ContractHashAlgorithm:             sha3_512,
		OutwayPeerID:                      "",
		OutwayCertificateThumbprint:       "",
		ServicePeerID:                     "",
		ServiceName:                       "",
		ServicePublicationDelegatorPeerID: "",
	})
	require.NoError(t, err)

	mockRepo.
		EXPECT().
		GetServiceConnectionInfo(ctx, testGrantHash).
		Return(&config.ServiceConnectionInfo{
			OutwayDelegatorPeerID:     "",
			OutwayPeerID:              "",
			OutwayPublicKeyThumbprint: "",
			ServicePeerID:             "",
			ServicePeerManagerAddress: "https://manager-addresss:8443",
			ServiceName:               "",
			ServiceDelegatorPeerID:    "",
		}, nil)

	dateTimeInThePast := testClock.Now().Add(-1 * time.Second)

	tokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-test",
		GrantHash:                   testGrantHash,
		OutwayPeerID:                "",
		OutwayCertificateThumbprint: orgCertBundle.CertificateThumbprint(),
		ServiceName:                 "",
		ServiceInwayAddress:         "",
		ServicePeerID:               "",
		ExpiryDate:                  dateTimeInThePast,
		NotBefore:                   dateTimeInThePast,
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgCertBundle.CertificateThumbprint(),
			PrivateKey:            orgCertBundle.PrivateKey(),
		},
	}

	signedToken, err := accesstoken.New(tokenArgs)
	require.Nil(t, err)

	mockRepo.
		EXPECT().
		GetToken(ctx, "", "https://manager-addresss:8443", testGrantHash).
		Times(2).
		Return(signedToken.Value(), nil)

	c, err := config.New(testClock, orgCertBundle.CertificateThumbprint(), mockRepo)
	require.NoError(t, err)

	// We configured the repository to always return an expired token.
	// Since the Outway requests a token twice from the repository, and the Outway should not reuse expired tokens,
	// we expect the GetToken method of the repository to be called twice.
	_, err = c.GetTokenInfo(ctx, testGrantHash)
	assert.NoError(t, err)

	_, err = c.GetTokenInfo(ctx, testGrantHash)
	assert.NoError(t, err)
}
