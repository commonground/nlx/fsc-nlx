// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"context"
)

type Repository interface {
	GetServices(ctx context.Context) (Services, error)
	GetToken(ctx context.Context, peerID, managerAddress, grantHash string) (string, error)
	GetServiceConnectionInfo(ctx context.Context, grantHash string) (*ServiceConnectionInfo, error)
}

type ServiceNotFoundError struct {
	message string
}

func NewServiceNotFoundError(message string) ServiceNotFoundError {
	return ServiceNotFoundError{
		message: message,
	}
}

func (v ServiceNotFoundError) Error() string {
	return v.message
}
