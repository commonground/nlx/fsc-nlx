// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins_test

import (
	"encoding/base64"
	"net/http"
	"net/http/httptest"

	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/plugins"
)

func fakeContext(token *config.TokenInfo) *plugins.Context {
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/test?foo=bar&bar=foo", http.NoBody)

	return &plugins.Context{
		TokenInfo: token,
		Request:   request,
		Response:  recorder,
		Logger:    discardlogger.New(),
		LogData:   map[string]string{},
	}
}

func nopServeFunc(_ *plugins.Context) error {
	return nil
}

func rawDERstoBase64(rawDERs [][]byte) []string {
	certs := make([]string, len(rawDERs))

	for i, c := range rawDERs {
		certs[i] = base64.URLEncoding.EncodeToString(c)
	}

	return certs
}
