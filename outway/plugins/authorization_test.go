// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins_test

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/plugins"
	common_testing "gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:funlen,dupl // this is a test
func TestAuthorizationPlugin(t *testing.T) {
	tests := map[string]struct {
		args                         *plugins.AuthRequest
		authServerResponse           interface{}
		authServerWithBody           bool
		authServerResponseStatusCode int
		wantErr                      *httperrors.FSCNetworkError
		wantHTTPStatusCode           int
		wantAuthBodyInput            string
	}{
		"when_auth_server_returns_non_OK_status": {
			args: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			authServerResponse: &plugins.AuthResponse{
				Result: plugins.AuthResponseData{
					Allowed: true,
				},
			},
			authServerResponseStatusCode: http.StatusUnauthorized,
			wantHTTPStatusCode:           http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.OAS1,
				Code:     httperrors.ErrorWhileAuthorizingRequestErr,
				Message:  "error authorizing request: unexpected error while calling the authorization service",
			},
		},
		"when_auth_server_returns_invalid_response": {
			args: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			authServerResponse: struct {
				Invalid string `json:"invalid"`
			}{
				Invalid: "this is an invalid response",
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.OAS1,
				Code:     httperrors.UnauthorizedErr,
				Message:  "authorization server denied request: ",
			},
		},
		"when_auth_server_fails": {
			args: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			authServerResponseStatusCode: http.StatusInternalServerError,
			wantHTTPStatusCode:           http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.OAS1,
				Code:     httperrors.ErrorWhileAuthorizingRequestErr,
				Message:  "error authorizing request: unexpected error while calling the authorization service",
			},
		},
		"when_auth_server_returns_no_access": {
			args: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			authServerResponse: &plugins.AuthResponse{
				Result: plugins.AuthResponseData{
					Allowed: false,
					Status: plugins.AuthResponseStatus{
						Reason: "for some reason",
					},
				},
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.OAS1,
				Code:     httperrors.UnauthorizedErr,
				Message:  "authorization server denied request: for some reason",
			},
		},
		"happy_flow": {
			args: &plugins.AuthRequest{
				Input: func() *plugins.AuthRequestInput {
					externalCert, err := common_testing.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), common_testing.NLXTestPeerA)
					assert.NoError(t, err)

					testclock := common_testing.NewMockClock(time.Now())

					c, err := contract.NewPeerCertFromCertificate(testclock, externalCert.RootCAs(), externalCert.Cert().Certificate)
					assert.NoError(t, err)

					return &plugins.AuthRequestInput{
						Headers: http.Header{
							"Proxy-Authorization": []string{"Bearer abc"},
						},
						Path:                   "/test",
						Method:                 http.MethodGet,
						Query:                  "foo=bar&bar=foo",
						OutwayCertificateChain: rawDERstoBase64(c.RawDERs()),
					}
				}(),
			},
			authServerResponse: &plugins.AuthResponse{
				Result: plugins.AuthResponseData{
					Allowed: true,
				},
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusOK,
		},
		"happy_flow_auth_server_enabled_with_body": {
			args: &plugins.AuthRequest{
				Input: func() *plugins.AuthRequestInput {
					externalCert, err := common_testing.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), common_testing.NLXTestPeerA)
					assert.NoError(t, err)

					testclock := common_testing.NewMockClock(time.Now())

					c, err := contract.NewPeerCertFromCertificate(testclock, externalCert.RootCAs(), externalCert.Cert().Certificate)
					assert.NoError(t, err)

					return &plugins.AuthRequestInput{
						Headers: http.Header{
							"Proxy-Authorization": []string{"Bearer abc"},
						},
						Path:                   "/test",
						Query:                  "foo=bar&bar=foo",
						Method:                 http.MethodPost,
						OutwayCertificateChain: rawDERstoBase64(c.RawDERs()),
						Body:                   "Outway test",
					}
				}(),
			},
			authServerWithBody: true,
			authServerResponse: &plugins.AuthResponse{
				Result: plugins.AuthResponseData{
					Allowed: true,
				},
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusOK,
			wantAuthBodyInput:            "T3V0d2F5IHRlc3Q=",
		},
		"happy_flow_auth_server_enabled_with_body_exceeding_max": {
			args: &plugins.AuthRequest{
				Input: func() *plugins.AuthRequestInput {
					externalCert, err := common_testing.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), common_testing.NLXTestPeerA)
					assert.NoError(t, err)

					testclock := common_testing.NewMockClock(time.Now())

					c, err := contract.NewPeerCertFromCertificate(testclock, externalCert.RootCAs(), externalCert.Cert().Certificate)
					assert.NoError(t, err)

					return &plugins.AuthRequestInput{
						Headers: http.Header{
							"Proxy-Authorization": []string{"Bearer abc"},
						},
						Path:                   "/test",
						Query:                  "foo=bar&bar=foo",
						Method:                 http.MethodPost,
						OutwayCertificateChain: rawDERstoBase64(c.RawDERs()),
						Body:                   "This is a test exceeding the maximum of bytes",
					}
				}(),
			},
			authServerWithBody: true,
			authServerResponse: &plugins.AuthResponse{
				Result: plugins.AuthResponseData{
					Allowed: true,
				},
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.OAS1,
				Code:     httperrors.ErrorWhileAuthorizingRequestErr,
				Message:  "error authorizing request: HTTP request body exceeded maximum number of bytes",
			},
			wantAuthBodyInput: "",
		},
	}

	for name, tt := range tests {
		tc := tt

		t.Run(name, func(t *testing.T) {
			context := fakeContext(&config.TokenInfo{
				UnsafeDecodedToken: &accesstoken.UnsafeDecodedToken{
					GrantHash:                   "gh",
					OutwayPeerID:                "12345678901234567890",
					OutwayDelegatorPeerID:       "12345678901234567890",
					OutwayCertificateThumbprint: "",
					ServiceName:                 "serviceName",
					ServiceInwayAddress:         "inway.local",
					ServicePeerID:               "12345678901234567890",
					ServiceDelegatorPeerID:      "12345678901234567890",
					ExpiryDate:                  time.Now(),
				},
			})

			for k, values := range tc.args.Input.Headers {
				for _, v := range values {
					context.Request.Header.Add(k, v)
				}
			}

			if tc.authServerWithBody {
				buf := bytes.Buffer{}
				buf.WriteString(tc.args.Input.Body)
				context.Request.Body = io.NopCloser(&buf)

				context.Request.Method = tc.args.Input.Method
			}

			var gotAuthorizationServiceRequest []byte

			server := httptest.NewServer(
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					body := r.Body

					var err error
					gotAuthorizationServiceRequest, err = io.ReadAll(body)
					assert.NoError(t, err)

					var authInput plugins.AuthRequest
					err = json.Unmarshal(gotAuthorizationServiceRequest, &authInput)
					assert.NoError(t, err)

					if tc.wantAuthBodyInput != "" {
						assert.Equal(t, tc.wantAuthBodyInput, authInput.Input.Body)
					}

					w.WriteHeader(tc.authServerResponseStatusCode)

					b, err := json.Marshal(tc.authServerResponse)
					assert.NoError(t, err)

					_, err = w.Write(b)
					assert.NoError(t, err)
				}),
			)

			externalCert, err := common_testing.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), common_testing.NLXTestPeerA)
			assert.NoError(t, err)

			plugin, err := plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
				ServiceURL:          server.URL,
				AuthorizationClient: *http.DefaultClient,
				ExternalCertificate: externalCert,
				Clock:               clock.New(),
				WithBody:            tc.authServerWithBody,
				ChunkSize:           1024,
				MaxBodyBytes:        15,
			})
			assert.NoError(t, err)

			err = plugin.Serve(nopServeFunc)(context)
			assert.NoError(t, err)

			response := context.Response.(*httptest.ResponseRecorder).Result()

			defer response.Body.Close()

			contents, err := io.ReadAll(response.Body)
			assert.NoError(t, err)

			assert.Equal(t, tc.wantHTTPStatusCode, response.StatusCode)

			if tc.wantErr != nil {
				gotError := &httperrors.FSCNetworkError{}
				errUnmarshal := json.Unmarshal(contents, gotError)
				assert.NoError(t, errUnmarshal)

				assert.Equal(t, tc.wantErr, gotError)
			} else {
				if tc.authServerWithBody && len(tc.args.Input.Body) < 15 {
					tc.args.Input.Body = base64.StdEncoding.EncodeToString([]byte(tc.args.Input.Body))
				}

				wantAuthorizationServiceRequest, errUnmarshall := json.Marshal(tc.args)
				assert.NoError(t, errUnmarshall)

				assert.Equal(t, wantAuthorizationServiceRequest, gotAuthorizationServiceRequest)
			}
		})
	}
}
