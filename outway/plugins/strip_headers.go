// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins

type StripHeadersPlugin struct {
	selfPeerID string
}

func NewStripHeadersPlugin(selfPeerID string) *StripHeadersPlugin {
	return &StripHeadersPlugin{
		selfPeerID: selfPeerID,
	}
}

func (plugin *StripHeadersPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		context.Request.Header.Del("Proxy-Authorization")

		return next(context)
	}
}
