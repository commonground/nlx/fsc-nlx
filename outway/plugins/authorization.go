// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	common_http "gitlab.com/commonground/nlx/fsc-nlx/common/http"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	outway_http "gitlab.com/commonground/nlx/fsc-nlx/outway/http"
)

type AuthRequest struct {
	Input *AuthRequestInput `json:"input"`
}

type AuthRequestInput struct {
	Headers                http.Header `json:"headers"`
	Path                   string      `json:"path"`
	Method                 string      `json:"method"`
	Query                  string      `json:"query"`
	OutwayCertificateChain []string    `json:"outway_certificate_chain"`
	Body                   string      `json:"body"`
}

type AuthResponse struct {
	Result AuthResponseData `json:"result"`
}

type AuthResponseData struct {
	Allowed bool               `json:"allowed"`
	Status  AuthResponseStatus `json:"status"`
}

type AuthResponseStatus struct {
	Reason string `json:"reason"`
}
type AuthorizationPlugin struct {
	serviceURL             string
	authorizationClient    http.Client
	outwayCertificateChain []string
	crlCache               *common_tls.CRLsCache
	externalCertificate    *common_tls.CertificateBundle
	withBody               bool
	maxBodyBytes           int
	chunkSize              int
}

type NewAuthorizationPluginArgs struct {
	ServiceURL          string
	AuthorizationClient http.Client
	ExternalCertificate *common_tls.CertificateBundle
	CrlCache            *common_tls.CRLsCache
	Clock               clock.Clock
	WithBody            bool
	MaxBodyBytes        int
	ChunkSize           int
}

func NewAuthorizationPlugin(args *NewAuthorizationPluginArgs) (*AuthorizationPlugin, error) {
	if args.ServiceURL == "" {
		return nil, fmt.Errorf("ServiceURL cannot be empty")
	}

	if args.ExternalCertificate == nil {
		return nil, fmt.Errorf("ExternalCertificate cannot be nil")
	}

	if args.Clock == nil {
		return nil, fmt.Errorf("clock cannot be nil")
	}

	c, err := contract.NewPeerCertFromCertificate(args.Clock, args.ExternalCertificate.RootCAs(), args.ExternalCertificate.Cert().Certificate)
	if err != nil {
		return nil, fmt.Errorf("could not create peer cert from args: %w", err)
	}

	return &AuthorizationPlugin{
		serviceURL:             args.ServiceURL,
		authorizationClient:    args.AuthorizationClient,
		outwayCertificateChain: rawDERstoBase64(c.RawDERs()),
		crlCache:               args.CrlCache,
		externalCertificate:    args.ExternalCertificate,
		withBody:               args.WithBody,
		maxBodyBytes:           args.MaxBodyBytes,
		chunkSize:              args.ChunkSize,
	}, nil
}

func (plugin *AuthorizationPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		authorizationResponse, authErr := plugin.authorizeRequest(context)
		if authErr != nil && strings.Contains(authErr.Error(), "body exceeds maxBytes and cannot be encoded") {
			context.Logger.Error("error authorizing request", authErr)
			outway_http.WriteError(context.Response, httperrors.OAS1, httperrors.ErrorWhileAuthorizingRequestMaxBodyExceeded())

			return nil
		}
		if authErr != nil {
			context.Logger.Error("error authorizing request", authErr)

			outway_http.WriteError(context.Response, httperrors.OAS1, httperrors.ErrorWhileAuthorizingRequest())

			return nil
		}

		context.Logger.With("authorized", authorizationResponse.Result.Allowed).Info("authorization result")

		if !authorizationResponse.Result.Allowed {
			outway_http.WriteError(context.Response, httperrors.OAS1, httperrors.Unauthorized(authorizationResponse.Result.Status.Reason))

			return nil
		}

		return next(context)
	}
}

func (plugin *AuthorizationPlugin) authorizeRequest(c *Context) (*AuthResponse, error) {
	req, err := http.NewRequest(http.MethodPost, plugin.serviceURL, http.NoBody)
	if err != nil {
		return nil, err
	}

	authRequestInput := &AuthRequestInput{
		Headers:                c.Request.Header,
		Path:                   c.Request.URL.Path,
		Query:                  c.Request.URL.RawQuery,
		Method:                 c.Request.Method,
		OutwayCertificateChain: plugin.outwayCertificateChain,
	}

	if plugin.withBody {
		buf := bytes.Buffer{}
		tee := io.TeeReader(c.Request.Body, &buf)

		c.Request.Body = io.NopCloser(&buf)

		authzBody, err := common_http.EncodeBody(tee, plugin.maxBodyBytes, plugin.chunkSize)
		if err != nil {
			return nil, err
		}

		authRequestInput.Body = authzBody
	}

	body, err := json.Marshal(&AuthRequest{
		Input: authRequestInput,
	})
	if err != nil {
		return nil, err
	}

	req.Body = io.NopCloser(bytes.NewBuffer(body))

	resp, err := plugin.authorizationClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("authorization service return non 200 status code. status code: %d", resp.StatusCode)
	}

	authResponse := &AuthResponse{}

	err = json.NewDecoder(resp.Body).Decode(authResponse)
	if err != nil {
		return nil, err
	}

	return authResponse, nil
}

func rawDERstoBase64(rawDERs [][]byte) []string {
	certs := make([]string, len(rawDERs))

	for i, c := range rawDERs {
		certs[i] = base64.URLEncoding.EncodeToString(c)
	}

	return certs
}
