// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/plugins"
)

func TestStripHeadersPlugin(t *testing.T) {
	headers := []string{
		"Proxy-Authorization",
		"Fsc-Transaction-Id",
	}

	safeHeaders := []string{
		"Fsc-Transaction-Id",
	}

	tests := map[string]struct {
		name                 string
		receiverOrganization string
		expectHeaders        []string
		disallowedHeaders    []string
	}{
		"different_organization": {
			receiverOrganization: "00000000000000000002",
			expectHeaders:        safeHeaders,
			disallowedHeaders:    nil,
		},
		"same_organization": {
			receiverOrganization: "00000000000000000001",
			expectHeaders:        safeHeaders,
			disallowedHeaders:    nil,
		},
		"different_organization_do_not_pass_authorization_headers": {
			receiverOrganization: "00000000000000000002",
			expectHeaders:        nil,
			disallowedHeaders:    []string{"Proxy-Authorization"},
		},
		"same_organization_do_not_pass_authorization_headers": {
			receiverOrganization: "00000000000000000001",
			expectHeaders:        nil,
			disallowedHeaders:    []string{"Proxy-Authorization"},
		},
	}
	for name, tt := range tests {
		tc := tt

		t.Run(name, func(t *testing.T) {
			context := fakeContext(&config.TokenInfo{
				UnsafeDecodedToken: &accesstoken.UnsafeDecodedToken{
					ServicePeerID: "00000000000000000001",
				},
			})

			for _, header := range headers {
				context.Request.Header.Add(header, header)
			}

			plugin := plugins.NewStripHeadersPlugin(tc.receiverOrganization)

			err := plugin.Serve(nopServeFunc)(context)
			assert.NoError(t, err)

			if tc.expectHeaders != nil {
				for _, header := range tc.expectHeaders {
					assert.Equal(t, header, context.Request.Header.Get(header))
				}
			}

			if tc.disallowedHeaders != nil {
				for _, header := range tc.disallowedHeaders {
					assert.Equal(t, "", context.Request.Header.Get(header))
				}
			}
		})
	}
}
