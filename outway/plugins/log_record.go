// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	outway_http "gitlab.com/commonground/nlx/fsc-nlx/outway/http"
	txlog_metadata "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type LogRecordPlugin struct {
	organizationPeerID string
	txLogger           transactionlog.TransactionLogger
	metadata           LogRecordMetadata
}

type LogRecordMetadata struct {
	Headers []string
	// now only headers are supported, can be extended to include query params, body values
}

type NewLogRecordPluginArgs struct {
	OrganizationPeerID string
	TxLogger           transactionlog.TransactionLogger
	MetadataFields     LogRecordMetadata
}

func NewLogRecordPlugin(args NewLogRecordPluginArgs) *LogRecordPlugin {
	return &LogRecordPlugin{
		organizationPeerID: args.OrganizationPeerID,
		txLogger:           args.TxLogger,
		metadata:           args.MetadataFields,
	}
}

func (plugin *LogRecordPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		var transactionID *transactionlog.TransactionID

		var err error

		transactionIDHeaderValue := context.Request.Header.Get("Fsc-Transaction-Id")

		if transactionIDHeaderValue != "" {
			transactionID, err = transactionlog.NewTransactionIDFromString(transactionIDHeaderValue)
			if err != nil {
				context.Logger.Error("invalid transaction ID provided in the HTTP header Fsc-Transaction-Id", err)

				outway_http.WriteError(context.Response, httperrors.C1, httperrors.InvalidLogRecordID())

				return nil
			}
		} else {
			transactionID, err = transactionlog.NewTransactionID()
			if err != nil {
				context.Logger.Error("failed to generate a transaction ID", err)

				outway_http.WriteError(context.Response, httperrors.C1, httperrors.InvalidLogRecordID())

				return nil
			}

			context.Request.Header.Set("Fsc-Transaction-Id", transactionID.String())
		}

		err = plugin.createLogRecord(context, transactionID)
		if err != nil {
			context.Logger.Error("failed to store transactionlog record", err)

			outway_http.WriteError(context.Response, httperrors.C1, httperrors.LogRecordWriteError())

			return nil
		}

		err = plugin.createMetadataRecord(context, transactionID, &plugin.metadata)
		if err != nil {
			context.Logger.Error("failed to store LogRecordMetadata record", err)

			outway_http.WriteError(context.Response, httperrors.O1, httperrors.LogRecordWriteError())

			return nil
		}

		return next(context)
	}
}

func (plugin *LogRecordPlugin) createLogRecord(requestContext *Context, txLogRecordID *transactionlog.TransactionID) error {
	var source interface{}
	if requestContext.TokenInfo.OutwayDelegatorPeerID == "" {
		source = &transactionlog.RecordSource{
			OutwayPeerID: requestContext.TokenInfo.OutwayPeerID,
		}
	} else {
		source = &transactionlog.RecordDelegatedSource{
			OutwayPeerID:    requestContext.TokenInfo.OutwayPeerID,
			DelegatorPeerID: requestContext.TokenInfo.OutwayDelegatorPeerID,
		}
	}

	var destination interface{}
	if requestContext.TokenInfo.ServiceDelegatorPeerID == "" {
		destination = &transactionlog.RecordDestination{
			ServicePeerID: requestContext.TokenInfo.ServicePeerID,
		}
	} else {
		destination = &transactionlog.RecordDelegatedDestination{
			ServicePeerID:   requestContext.TokenInfo.ServicePeerID,
			DelegatorPeerID: requestContext.TokenInfo.ServiceDelegatorPeerID,
		}
	}

	rec := &transactionlog.Record{
		TransactionID: txLogRecordID,
		GroupID:       requestContext.TokenInfo.GroupID,
		GrantHash:     requestContext.TokenInfo.GrantHash,
		ServiceName:   requestContext.TokenInfo.ServiceName,
		Direction:     record.DirectionOut,
		Source:        source,
		Destination:   destination,
		CreatedAt:     requestContext.RequestCreatedAt,
		Data: map[string]interface{}{
			"request-path": requestContext.Request.URL.Path,
		},
	}

	if err := plugin.txLogger.AddRecords(context.TODO(), []*transactionlog.Record{rec}); err != nil {
		return errors.Join(err, errors.New("unable to add records to txlog"))
	}

	return nil
}

func (plugin *LogRecordPlugin) createMetadataRecord(requestContext *Context, id *transactionlog.TransactionID, metadataFields *LogRecordMetadata) error {
	if metadataFields == nil {
		return nil
	}

	metadataContent := make(map[string]map[string]interface{})

	if metadataFields.Headers != nil {
		metadataContent["HEADERS"] = mapMetadataHeaders(requestContext.Request.Header, metadataFields)
	}

	rec := &transactionlog.MetadataRecord{
		TransactionID: id,
		GroupID:       requestContext.TokenInfo.GroupID,
		Direction:     txlog_metadata.DirectionOut,
		Metadata:      metadataContent,
	}

	if err := plugin.txLogger.AddMetadataRecords(context.TODO(), []*transactionlog.MetadataRecord{rec}); err != nil {
		return errors.Join(err, errors.New("unable to add LogRecordMetadata records to txlog"))
	}

	return nil
}

func mapMetadataHeaders(headers http.Header, metadataFields *LogRecordMetadata) map[string]interface{} {
	if len(metadataFields.Headers) == 0 {
		return nil
	}

	metadataHeaders := make(map[string]interface{})
	for _, field := range metadataFields.Headers {
		metadataHeaders[field] = headers.Get(field)
	}

	return metadataHeaders
}
