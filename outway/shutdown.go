// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package outway

import (
	"context"
)

func (o *Outway) Shutdown(ctx context.Context) {
	o.logger.Debug("shutting down")

	err := o.httpServer.Shutdown(ctx)
	if err != nil {
		o.logger.Error("error shutting down server", err)
	}

	err = o.monitorService.Stop()
	if err != nil {
		o.logger.Error("error shutting down monitoring service", err)
	}
}
