// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package outway

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/adapters/controller"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/plugins"
)

const DefaultChunkByteSize = 1024

type Organization struct {
	peerID string
	name   string
}

type Monitoring struct {
	Address         string
	ReadinessProbes map[string]monitoring.Check
	LivenessProbes  map[string]monitoring.Check
}

type Outway struct {
	name                      string
	groupID                   contract.GroupID
	ctx                       context.Context
	wg                        *sync.WaitGroup
	organization              *Organization // the organization running this outway
	externalCert              *common_tls.CertificateBundle
	logger                    *logger.Logger
	txlogger                  transactionlog.TransactionLogger
	cfg                       *config.Config
	controller                controller.Controller
	httpServer                *http.Server
	monitorService            *monitoring.Service
	plugins                   []plugins.Plugin
	proxies                   *HTTPProxies
	clk                       clock.Clock
	crlCache                  *common_tls.CRLsCache
	EnableGrantHashSuggestion bool
}

type NewOutwayArgs struct {
	Clock                     clock.Clock
	Name                      string
	GroupID                   string
	Ctx                       context.Context
	Logger                    *logger.Logger
	Txlogger                  transactionlog.TransactionLogger
	Controller                controller.Controller
	ConfigRepository          config.Repository
	Monitoring                *Monitoring
	ExternalCert              *common_tls.CertificateBundle
	InternalCert              *common_tls.CertificateBundle
	AuthServiceURL            string
	AuthCAPath                string
	AuthWithBody              bool
	AuthMaxBodySize           int
	AuthBodyChunkSize         int
	CrlCache                  *common_tls.CRLsCache
	TxLogMetadataHeaders      []string
	EnableGrantHashSuggestion bool
}

func New(args *NewOutwayArgs) (*Outway, error) {
	if args.Name == "" {
		return nil, fmt.Errorf("name cannot be empty")
	}

	if args.Clock == nil {
		return nil, fmt.Errorf("clock cannot be nil")
	}

	groupID, err := contract.NewGroupID(args.GroupID)
	if err != nil {
		return nil, fmt.Errorf("invalid groupID in params: %w", err)
	}

	if args.ExternalCert == nil {
		return nil, fmt.Errorf("externalCert is required")
	}

	cert := args.ExternalCert.Certificate()

	if len(cert.Subject.Organization) != 1 {
		return nil, errors.New("cannot obtain organization name from self cert")
	}

	err = common_tls.ValidatePeerID(cert.Subject.SerialNumber)
	if err != nil {
		return nil, fmt.Errorf("validation error for subject serial number from cert: %s", err)
	}

	organizationName := cert.Subject.Organization[0]
	organizationPeerID := cert.Subject.SerialNumber

	if args.ConfigRepository == nil {
		return nil, errors.New("missing config repository")
	}

	if args.Monitoring == nil {
		return nil, errors.New("missing monitoring configuration")
	}

	proxies, err := NewHTTPProxies(args.Logger, args.ExternalCert)
	if err != nil {
		return nil, errors.Wrap(err, "could not create http proxies struct")
	}

	cfg, err := config.New(args.Clock, args.ExternalCert.CertificateThumbprint(), args.ConfigRepository)
	if err != nil {
		return nil, errors.Wrap(err, "could not create config")
	}

	o := &Outway{
		ctx:        args.Ctx,
		name:       args.Name,
		groupID:    groupID,
		wg:         &sync.WaitGroup{},
		controller: args.Controller,
		cfg:        cfg,
		logger: args.Logger.With(
			"outway-organization-name", organizationName,
			"outway-organization-peer-id", organizationPeerID),
		txlogger: args.Txlogger,
		organization: &Organization{
			peerID: cert.Subject.SerialNumber,
			name:   organizationName,
		},
		externalCert:              args.ExternalCert,
		proxies:                   proxies,
		clk:                       args.Clock,
		crlCache:                  args.CrlCache,
		EnableGrantHashSuggestion: args.EnableGrantHashSuggestion,
	}

	authorizationPlugin, err := o.configureAuthorizationPlugin(args.AuthCAPath, args.AuthServiceURL, args.AuthWithBody, args.AuthMaxBodySize, args.AuthBodyChunkSize)
	if err != nil {
		return nil, err
	}

	o.monitorService, err = monitoring.NewMonitoringService(
		args.Monitoring.Address,
		args.Logger,
		args.Monitoring.ReadinessProbes,
		args.Monitoring.LivenessProbes,
	)
	if err != nil {
		return nil, errors.Wrap(err, "unable to create monitoring service")
	}

	o.plugins = []plugins.Plugin{
		plugins.NewLogRecordPlugin(plugins.NewLogRecordPluginArgs{
			OrganizationPeerID: o.organization.peerID,
			TxLogger:           o.txlogger,
			MetadataFields:     plugins.LogRecordMetadata{Headers: args.TxLogMetadataHeaders},
		}),
	}

	if authorizationPlugin != nil {
		o.plugins = append(o.plugins, authorizationPlugin)
	}

	// Strip headers as last step in plugin chain so intermediate steps can still access all headers
	o.plugins = append(o.plugins, plugins.NewStripHeadersPlugin(o.organization.peerID))

	return o, nil
}

func (o *Outway) configureAuthorizationPlugin(authCAPath, authServiceURL string, withBody bool, maxBodyBytes, chunkSize int) (*plugins.AuthorizationPlugin, error) {
	if authServiceURL == "" {
		return nil, nil
	}

	if authCAPath == "" {
		return nil, fmt.Errorf("authorization service URL set but no CA for authorization provided")
	}

	authURL, err := url.Parse(authServiceURL)
	if err != nil {
		return nil, err
	}

	if authURL.Scheme != "https" {
		return nil, errors.New("scheme of authorization service URL is not 'https'")
	}

	ca, err := common_tls.NewCertPoolFromFile(authCAPath)
	if err != nil {
		return nil, err
	}

	tlsConfig := common_tls.NewConfig(common_tls.WithTLS12())
	tlsConfig.RootCAs = ca

	if chunkSize == 0 {
		chunkSize = DefaultChunkByteSize
	}

	if maxBodyBytes == 0 {
		maxBodyBytes = DefaultChunkByteSize * DefaultChunkByteSize
	}

	return plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
		ServiceURL: authURL.String(),
		AuthorizationClient: http.Client{
			Transport: createHTTPTransport(tlsConfig),
		},
		ExternalCertificate: o.externalCert,
		Clock:               o.clk,
		WithBody:            withBody,
		MaxBodyBytes:        maxBodyBytes,
		ChunkSize:           chunkSize,
	})
}

func (o *Outway) Run(ctx context.Context) error {
	go o.announceToControllerAPI(ctx)

	return nil
}

func (o *Outway) PluginCount() int {
	return len(o.plugins)
}

func createHTTPTransport(tlsConfig *tls.Config) *http.Transport {
	const (
		timeOut               = 30 * time.Second
		keepAlive             = 30 * time.Second
		maxIdleCons           = 100
		idleConnTimeout       = 20 * time.Second
		tlsHandshakeTimeout   = 10 * time.Second
		expectContinueTimeout = 1 * time.Second
	)

	return &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			a := &net.Dialer{
				Timeout:   timeOut,
				KeepAlive: keepAlive,
			}

			return a.DialContext(ctx, network, addr)
		},
		MaxIdleConns:          maxIdleCons,
		IdleConnTimeout:       idleConnTimeout,
		TLSHandshakeTimeout:   tlsHandshakeTimeout,
		ExpectContinueTimeout: expectContinueTimeout,
		TLSClientConfig:       tlsConfig,
	}
}
