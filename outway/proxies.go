// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package outway

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	outway_http "gitlab.com/commonground/nlx/fsc-nlx/outway/http"
)

type HTTPProxies struct {
	mut     sync.RWMutex
	proxies map[string]*HTTPService
	orgCert *common_tls.CertificateBundle
	logger  *logger.Logger
}

func NewHTTPProxies(lgr *logger.Logger, orgCert *common_tls.CertificateBundle) (*HTTPProxies, error) {
	if orgCert == nil {
		return nil, errors.New("organization certificate is required")
	}

	return &HTTPProxies{
		mut:     sync.RWMutex{},
		proxies: make(map[string]*HTTPService),
		orgCert: orgCert,
		logger:  lgr,
	}, nil
}

func (p *HTTPProxies) Get(inwayAddress string, crlCache *common_tls.CRLsCache) (*HTTPService, error) {
	p.mut.RLock()
	proxy, ok := p.proxies[inwayAddress]
	p.mut.RUnlock()

	if ok {
		return proxy, nil
	}

	proxy, err := NewHTTPService(p.proxyErrorHandler, p.orgCert, inwayAddress, crlCache)
	if err != nil {
		return nil, errors.Wrapf(err, "could not create proxy to inway: %s", inwayAddress)
	}

	p.mut.Lock()
	p.proxies[inwayAddress] = proxy
	p.mut.Unlock()

	return proxy, nil
}

func (p *HTTPProxies) proxyErrorHandler(w http.ResponseWriter, r *http.Request, err error) {
	var certificateRevokedError *common_tls.CertificateRevokedError
	if errors.As(err, &certificateRevokedError) {
		p.logger.Error("could not proxy request. certificated revoked", err)

		outway_http.WriteError(w, httperrors.O1, httperrors.CertificateRevoked(err))

		return
	}

	p.logger.Error(fmt.Sprintf("error proxying the request to the Inway address %q", r.URL.String()), err)
	outway_http.WriteError(w, httperrors.O1, httperrors.ServiceUnreachable(r.URL.String()))
}
