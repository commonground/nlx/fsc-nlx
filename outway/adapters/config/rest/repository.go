// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restconfig

import (
	"context"
	"fmt"
	"net/http"

	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	ext_api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	int_api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

type Repository struct {
	managerClient *int_api.ClientWithResponses
	externalCert  *common_tls.CertificateBundle
	crlCache      *common_tls.CRLsCache
}

func New(_ context.Context, managerClient *int_api.ClientWithResponses, externalCert *common_tls.CertificateBundle, cache *common_tls.CRLsCache) (*Repository, error) {
	if externalCert == nil {
		return nil, fmt.Errorf("missing external certificate")
	}

	if managerClient == nil {
		return nil, fmt.Errorf("manager client is required")
	}

	return &Repository{
		managerClient: managerClient,
		externalCert:  externalCert,
		crlCache:      cache,
	}, nil
}

func (r *Repository) NewManagerClient(peerID, address string) (*ext_api.ClientWithResponses, error) {
	tlsConfig := r.externalCert.TLSConfig(
		common_tls.WithCRLPeerCertificateVerification(r.crlCache),
		common_tls.WithVerifyFunc(common_tls.VerifyConnStatePeerID(peerID)),
	)

	c, err := ext_api.NewClientWithResponses(address, func(c *ext_api.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = tlsConfig

		c.Client = &http.Client{Transport: t}

		return nil
	})

	if err != nil {
		return nil, err
	}

	return c, nil
}

func (r *Repository) Close() error {
	return nil
}
