// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restconfig

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	ext_api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
)

func (r *Repository) GetServices(ctx context.Context) (config.Services, error) {
	res, err := r.managerClient.GetOutwayServicesWithResponse(ctx, tls.PublicKeyThumbprint(r.externalCert.Certificate()))
	if err != nil {
		return nil, errors.Join(err, errors.New("could not get outway services from manager"))
	}

	if res.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not get outway services, received invalid status code %d: %s", res.StatusCode(), string(res.Body))
	}

	services := make(config.Services)
	for grantHash, s := range res.JSON200.Services {
		services[grantHash] = &config.Service{
			PeerID: s.PeerId,
			Name:   s.Name,
		}
	}

	return services, nil
}

func (r *Repository) GetServiceConnectionInfo(ctx context.Context, grantHash string) (*config.ServiceConnectionInfo, error) {
	res, err := r.managerClient.GetContractsWithResponse(ctx, &models.GetContractsParams{
		GrantHash: &[]string{grantHash},
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("could not get contracts from manager"))
	}

	if res.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not get contracts from manager, received invalid status code %d: %s", res.StatusCode(), string(res.Body))
	}

	serviceConnectionInfo, err := mapContracts(grantHash, &res.JSON200.Contracts)
	if err != nil {
		return nil, err
	}

	pIDs := []string{serviceConnectionInfo.ServicePeerID}

	var pResp *api.ListPeersResponse

	pResp, err = r.managerClient.ListPeersWithResponse(ctx, &models.ListPeersParams{
		PeerIds: &pIDs,
	})
	if err != nil {
		return nil, errors.Join(err, errors.New("unable to get peer from manager"))
	}

	if pResp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not get peer from manager, received invalid status code %d: %s", pResp.StatusCode(), string(pResp.Body))
	}

	if len(pResp.JSON200.Peers) != 1 {
		return nil, fmt.Errorf("invalid number of returned. expected 1, got %d ", len(pResp.JSON200.Peers))
	}

	serviceConnectionInfo.ServicePeerManagerAddress = pResp.JSON200.Peers[0].ManagerAddress

	return serviceConnectionInfo, nil
}

//nolint:funlen,gocognit,gocyclo // reducing funlen won't result in more readable code
func mapContracts(grantHash string, contracts *[]models.Contract) (*config.ServiceConnectionInfo, error) {
	s := &config.ServiceConnectionInfo{}

	c := *contracts

	for i := range c {
		contract := c[i]

		if contract.State != models.CONTRACTSTATEVALID {
			continue
		}

		for _, grant := range contract.Content.Grants {
			g := grant

			discriminator, err := g.ValueByDiscriminator()
			if err != nil {
				return nil, err
			}

			switch convertedGrant := discriminator.(type) {
			case models.GrantServiceConnection:
				if convertedGrant.Hash != grantHash {
					continue
				}

				mapServiceConnection(s, &convertedGrant)

				var serviceDiscriminator interface{}

				serviceDiscriminator, err = convertedGrant.Service.ValueByDiscriminator()
				if err != nil {
					return nil, err
				}

				switch service := serviceDiscriminator.(type) {
				case models.Service:
					mapService(s, &service)
				case models.DelegatedService:
					mapDelegatedService(s, &service)
				default:
					return nil, fmt.Errorf("invalid service type: %s", serviceDiscriminator)
				}

				return s, nil
			case models.GrantDelegatedServiceConnection:
				if convertedGrant.Hash != grantHash {
					continue
				}

				mapDelegatedServiceConnection(s, &convertedGrant)

				var serviceDiscriminator interface{}

				serviceDiscriminator, err = convertedGrant.Service.ValueByDiscriminator()
				if err != nil {
					return nil, err
				}

				switch service := serviceDiscriminator.(type) {
				case models.Service:
					mapService(s, &service)
				case models.DelegatedService:
					mapDelegatedService(s, &service)
				default:
					return nil, fmt.Errorf("invalid service type: %s", serviceDiscriminator)
				}

				return s, nil
			default:
				continue
			}
		}
	}

	return nil, config.NewServiceNotFoundError(fmt.Sprintf("could not find valid contract with grant with grant hash: %q", grantHash))
}

func mapServiceConnection(s *config.ServiceConnectionInfo, m *models.GrantServiceConnection) {
	s.OutwayPeerID = m.Outway.PeerId
}

func mapDelegatedServiceConnection(s *config.ServiceConnectionInfo, m *models.GrantDelegatedServiceConnection) {
	s.OutwayPeerID = m.Outway.PeerId

	s.OutwayDelegatorPeerID = m.Delegator.PeerId
}

func mapService(s *config.ServiceConnectionInfo, m *models.Service) {
	s.ServiceName = m.Name
	s.ServicePeerID = m.Peer.Id
}

func mapDelegatedService(s *config.ServiceConnectionInfo, m *models.DelegatedService) {
	s.ServiceName = m.Name
	s.ServicePeerID = m.Peer.Id

	s.ServiceDelegatorPeerID = m.Delegator.Id
}

func (r *Repository) GetToken(ctx context.Context, peerID, managerAddress, grantHash string) (string, error) {
	c, err := r.NewManagerClient(peerID, managerAddress)
	if err != nil {
		return "", fmt.Errorf("could not create manager client: %w", err)
	}

	response, err := c.GetTokenWithFormdataBodyWithResponse(ctx, ext_api.GetTokenFormdataRequestBody{
		GrantType: ext_api.ClientCredentials,
		Scope:     grantHash,
		ClientId:  r.externalCert.GetPeerInfo().SerialNumber,
	})
	if err != nil {
		return "", errors.Join(err, fmt.Errorf("could not get token"))
	}

	if response.StatusCode() != http.StatusOK {
		return "", fmt.Errorf("could not get token: received invalid status code %d: %s", response.StatusCode(), string(response.Body))
	}

	if response.JSON200.TokenType != ext_api.Bearer {
		return "", fmt.Errorf("invalid token type: %s", response.JSON200.TokenType)
	}

	return response.JSON200.AccessToken, nil
}
