// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package outway

import (
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/slogerrorwriter"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
	outway_http "gitlab.com/commonground/nlx/fsc-nlx/outway/http"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/plugins"
)

const (
	FscGrantHashHeader           = "Fsc-Grant-Hash"
	FscAuthorizationHeader       = "Fsc-Authorization"
	FscAuthorizationHeaderScheme = "Bearer "
	readHeaderTimeout            = time.Second * 60
)

func (o *Outway) RunServer(listenAddress string, serverCertificate *tls.Certificate) error {
	r := chi.NewRouter()
	r.HandleFunc("/*", o.ServeHTTP)
	r.Get("/health/live", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	o.httpServer = &http.Server{
		Addr:              listenAddress,
		Handler:           r,
		ReadHeaderTimeout: readHeaderTimeout,
		ErrorLog:          log.New(slogerrorwriter.New(o.logger.Logger), "", 0),
	}

	errorChannel := make(chan error)

	if serverCertificate == nil {
		go func() {
			o.logger.Info(fmt.Sprintf("starting HTTP server on %s", listenAddress))
			errorChannel <- o.httpServer.ListenAndServe()
		}()
	} else {
		tlsConfig := common_tls.NewConfig(common_tls.WithTLS12())
		tlsConfig.Certificates = []tls.Certificate{*serverCertificate}

		o.httpServer.TLSConfig = tlsConfig

		go func() {
			o.logger.Info(fmt.Sprintf("starting HTTPS server on %s", listenAddress))
			errorChannel <- o.httpServer.ListenAndServeTLS("", "")
		}()
	}

	go func() {
		err := o.monitorService.Start()
		if err != nil {
			errorChannel <- errors.Join(fmt.Errorf("error listening on monitoring service"), err)
		}
	}()

	err := <-errorChannel

	if errors.Is(err, http.ErrServerClosed) {
		return nil
	}

	return errors.Join(fmt.Errorf("error listening on server"), err)
}

func (o *Outway) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	now := o.clk.Now()

	logger := o.logger.With(
		"request-path", r.URL.Path,
		"request-remote-address", r.RemoteAddr,
	)

	logger.Info("received request")

	grantHashHeaderValue := r.Header.Get(FscGrantHashHeader)

	tokenInfo, err := o.cfg.GetTokenInfo(ctx, grantHashHeaderValue)
	if err != nil {
		if !errors.As(err, &config.ServiceNotFoundError{}) {
			o.logger.Warn(fmt.Sprintf("could not get token for service. grant-hash: %s", grantHashHeaderValue), err)

			outway_http.WriteError(w, httperrors.C1, httperrors.UnableToGetTokenForGrantHash(grantHashHeaderValue))

			return
		}

		svcs := make([]*httperrors.Service, 0)

		if o.EnableGrantHashSuggestion {
			var services config.Services
			services, err = o.cfg.GetServices(ctx)

			if err != nil {
				o.logger.Warn("could not get service", err)

				outway_http.WriteError(w, httperrors.C1, httperrors.ServerError(fmt.Errorf("could not get services for UnknownGrantHashInHeader error: %w", err)))

				return
			}

			for grantHash, s := range services {
				svcs = append(svcs, &httperrors.Service{
					GrantHash: grantHash,
					Name:      s.Name,
					PeerID:    s.PeerID,
				})
			}
		}

		outway_http.WriteError(w, httperrors.C1, httperrors.UnknownGrantHashInHeader(grantHashHeaderValue, svcs))

		return
	}

	if !o.groupID.IsEqual(tokenInfo.GroupID) {
		o.logger.Warn("invalid group id", fmt.Errorf("expected group id: %s but found %s in token", o.groupID.String(), tokenInfo.GroupID))

		outway_http.WriteError(w, httperrors.C1, httperrors.WrongGroupIDInToken(o.groupID.String(), tokenInfo.GroupID))

		return
	}

	if r.Header.Get(FscAuthorizationHeader) != "" {
		outway_http.WriteError(w, httperrors.C1, httperrors.AuthHeaderMustNotBeSet())

		return
	}

	r.Header.Add(FscAuthorizationHeader, FscAuthorizationHeaderScheme+tokenInfo.Token)

	proxy, err := o.proxies.Get(tokenInfo.ServiceInwayAddress, o.crlCache)
	if err != nil {
		outway_http.WriteError(w, httperrors.O1, httperrors.CouldNotSetupConnToInway(tokenInfo.ServiceInwayAddress, err))
		return
	}

	chain := buildChain(func(context *plugins.Context) error {
		proxy.HandleRequest(context.Response, context.Request)
		return nil
	}, o.plugins...)

	customCtx := &plugins.Context{
		Response:         w,
		Request:          r,
		Logger:           o.logger,
		RequestCreatedAt: now,
		TokenInfo:        tokenInfo,
		LogData:          map[string]string{},
	}

	logger.With(
		"grant-hash", tokenInfo.GrantHash,
		"service-inway-address", tokenInfo.ServiceInwayAddress,
		"service", tokenInfo.ServiceName,
		"path", r.URL.Path,
		"token-outway-certificate-thumbprint",
		tokenInfo.OutwayCertificateThumbprint,
		"source-peer-id", tokenInfo.OutwayPeerID,
		"source-delegator-peer-id", tokenInfo.OutwayDelegatorPeerID,
		"destination-peer-id", tokenInfo.ServicePeerID,
		"destination-delegator-peer-id", tokenInfo.ServiceDelegatorPeerID).Info("forwarding API request")

	if err := chain(customCtx); err != nil {
		logger.Error("error while handling API request", err)
	}
}

func buildChain(serve plugins.ServeFunc, pluginList ...plugins.Plugin) plugins.ServeFunc {
	if len(pluginList) == 0 {
		return serve
	}

	return pluginList[0].Serve(buildChain(serve, pluginList[1:]...))
}
