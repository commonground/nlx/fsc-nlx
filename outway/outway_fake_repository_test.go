// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package outway_test

import (
	"context"
	"errors"
	"net/http/httptest"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
)

type mockService struct {
	GrantHash        string
	Name             string
	PeerID           string
	outwayDelegator  *tls.OrganizationInformation
	serviceDelegator *tls.OrganizationInformation
	Server           *httptest.Server
}

type fakeRepository struct {
	Services []*mockService
}

func newFakeRepository(services []*mockService) *fakeRepository {
	return &fakeRepository{
		Services: services,
	}
}

func (r *fakeRepository) GetServices(_ context.Context) (config.Services, error) {
	services := make(config.Services, len(r.Services))

	for _, service := range r.Services {
		services[service.GrantHash] = &config.Service{
			Name:   service.Name,
			PeerID: service.PeerID,
		}
	}

	return services, nil
}

func (r *fakeRepository) GetServiceConnectionInfo(_ context.Context, grantHash string) (*config.ServiceConnectionInfo, error) {
	s, err := r.getMockService(grantHash)
	if err != nil {
		return nil, err
	}

	return &config.ServiceConnectionInfo{
		ServicePeerID: s.PeerID,
		ServiceName:   s.Name,
		ServiceDelegatorPeerID: func() string {
			if s.serviceDelegator == nil {
				return ""
			}

			return s.serviceDelegator.SerialNumber
		}(),
		ServicePeerManagerAddress: "https://idonotexist",
		OutwayPeerID:              orgBCertBundle.GetPeerInfo().SerialNumber,
		OutwayDelegatorPeerID: func() string {
			if s.outwayDelegator == nil {
				return ""
			}

			return s.outwayDelegator.SerialNumber
		}(),
	}, nil
}

func (r *fakeRepository) getMockService(grantHash string) (*mockService, error) {
	for _, service := range r.Services {
		if service.GrantHash == grantHash {
			return service, nil
		}
	}

	return nil, config.NewServiceNotFoundError("service not found in mock services")
}

func (r *fakeRepository) GetToken(_ context.Context, peerID, managerAddress, grantHash string) (string, error) {
	inwayService, err := r.getMockService(grantHash)
	if err != nil {
		return "", err
	}

	signedToken, err := generateValidToken(inwayService.Server.URL, inwayService.Name, grantHash, inwayService.outwayDelegator, inwayService.serviceDelegator)
	if err != nil {
		return "", errors.New("cannot generate token")
	}

	return signedToken, nil
}

func (r *fakeRepository) Close() error {
	return nil
}

func generateValidToken(serviceInwayAddress, serviceName, grantHash string, outwayDelegator, serviceDelegator *tls.OrganizationInformation) (string, error) {
	tokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-local",
		GrantHash:                   grantHash,
		OutwayPeerID:                orgBCertBundle.GetPeerInfo().SerialNumber,
		OutwayCertificateThumbprint: orgBCertBundle.CertificateThumbprint(),
		ServiceName:                 serviceName,
		ServiceInwayAddress:         serviceInwayAddress,
		ServicePeerID:               orgACertBundle.GetPeerInfo().SerialNumber,
		ExpiryDate:                  testClock.Now().Add(time.Minute),
		NotBefore:                   testClock.Now().Add(-time.Second),
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PrivateKey:            orgACertBundle.PrivateKey(),
		},
	}

	if outwayDelegator != nil {
		tokenArgs.OutwayDelegatorPeerID = outwayDelegator.SerialNumber
	}

	if serviceDelegator != nil {
		tokenArgs.ServiceDelegatorPeerID = serviceDelegator.SerialNumber
	}

	signedToken, err := accesstoken.New(tokenArgs)
	if err != nil {
		return "", err
	}

	return signedToken.Value(), nil
}
