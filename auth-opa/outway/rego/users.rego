package httpapi.outway

import rego.v1

default allow := {
	"allowed": false,
	"status": {"reason": "permission denied"},
}

allow := {"allowed": true, "status": {"reason": reason}} if {
	allowed

	reason := "all clear"
}

# Allow users with token containing a subject that is present in the data
allowed if {
	claims.sub in data.organizations
}

claims := payload if {
	[_, payload, _] := io.jwt.decode(bearer_token)
}

bearer_token := t if {
	v := input.headers["Fsc-Authorization"][0]
	startswith(v, "Bearer ")
	t := substring(v, count("Bearer "), -1)
}
