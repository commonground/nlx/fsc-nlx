package httpapi.manager

import rego.v1

default autosign := {
	"allowed": false,
	"status": {"reason": "peer not part of the trusted peer list"},
}

autosign := {"allowed": true, "status": {"reason": reason}} if {
	allowed

	reason := "peer present on trusted peer list"
}

allowed if {
    every peerID in input.peer_ids {
        peerID in data.peers
    }
}
