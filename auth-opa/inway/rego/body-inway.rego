package httpapi.inway.body

import rego.v1

default allow := {
	"allowed": false,
	"status": {"reason": "permission denied body does not match expectations"},
}

allow := {"allowed": true, "status": {"reason": reason}} if {
    valid_body
	reason := "body matched expected body"
}

valid_body if {
    input.body == ""
    reason := "no body provided so skipping further validation"
}

valid_body if {
	request_body := base64.decode(input.body)
	request_body == "allowed"
}
