// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"net/http"

	"github.com/cucumber/godog"
	"github.com/stretchr/testify/require"
)

func thenServiceIsCreated(ctx context.Context) error {
	t := godog.T(ctx)

	t.Log("then the service is created")

	testConfig := testConfigFromCtx(ctx)

	testConfig.LastPage.MustElement(".alert--success").MustWaitVisible()

	resp, err := testConfig.Organizations.Get(GemeenteStijns).Controller().GetService(ctx, testConfig.getName("basisregister"))
	require.NoError(t, err)

	require.Equal(t, http.StatusOK, resp.StatusCode)

	return nil
}
