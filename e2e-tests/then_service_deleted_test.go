// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"net/http"

	"github.com/cucumber/godog"
	"github.com/stretchr/testify/require"
)

func thenServiceIsDeleted(ctx context.Context) error {
	t := godog.T(ctx)

	t.Log("then the service is deleted")

	testConfig := testConfigFromCtx(ctx)

	testConfig.LastPage.MustElement(".alert--success").MustWaitVisible()

	resp, err := testConfig.Organizations.Get(GemeenteStijns).Controller().GetService(ctx, testConfig.getName("delete-service"))
	require.NoError(t, err)

	require.Equal(t, http.StatusNotFound, resp.StatusCode)

	return nil
}
