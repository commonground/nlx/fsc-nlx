// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"strings"

	"github.com/cucumber/godog"
)

func givenOrganizationIsUpAndRunning(ctx context.Context, organizationName string) error {
	t := godog.T(ctx)

	t.Logf("given organization %q is up and running", organizationName)

	testConfig := testConfigFromCtx(ctx)

	b := testConfig.Browser

	org := testConfig.Organizations.Get(organizationName)

	if org.State.IsLoggedIn {
		t.Logf("organization %q is already logged in", organizationName)
		return nil
	}

	page := b.MustPage(org.Info.ControllerUIURL)

	if org.Info.AuthenticationRequired {
		t.Logf("authentication required for organization %q", organizationName)

		page.MustWaitLoad()

		if strings.Contains(page.MustInfo().URL, "keycloak") {
			page.MustElement("#username").MustInput(org.Info.ControllerUIUsername)
			page.MustElement("#password").MustInput(org.Info.ControllerUIPassword)
			page.MustElement("#kc-login").MustClick()
			page.MustElement(".authenticated-page-template")

			t.Logf("logged successfully into %q", organizationName)
		}
	} else {
		t.Logf("no authentication required for organization %q", organizationName)

		page.MustElement(".authenticated-page-template")
	}

	org.State.IsLoggedIn = true

	testConfig.LastPage = page

	return nil
}
