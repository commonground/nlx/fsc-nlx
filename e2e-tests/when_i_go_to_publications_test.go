// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"

	"github.com/cucumber/godog"
)

func whenIGoToServicePublicationsOnTab(ctx context.Context, tabURLPath string) error {
	t := godog.T(ctx)

	t.Logf("when I go to the service publications page on tab %q", tabURLPath)

	testConfig := testConfigFromCtx(ctx)
	testConfig.TabURLPath = tabURLPath

	b := testConfig.Browser

	urlPath := fmt.Sprintf("services/publications/%s", tabURLPath)
	page := b.MustPage(testConfig.Organizations.Get(GemeenteStijns).Info.ControllerUIURL, urlPath)

	testConfig.LastPage = page

	return nil
}
