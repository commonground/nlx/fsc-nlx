// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/cucumber/godog"
	"github.com/go-rod/rod"
	"github.com/go-rod/rod/lib/launcher"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestFeatures(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:      "pretty,cucumber:./output/report.json",
			Paths:       []string{"features"},
			TestingT:    t,
			Strict:      true,
			Randomize:   getEnvInt64("E2E_RANDOM_SEED", -1), // Randomize order to prevent tests depending on eachother
			Concurrency: getEnvInt("E2E_MAX_CONCURRENT_TESTS", 5),
			Tags:        getEnvString("E2E_RUN_TAGS", "~@only-run-in-ci"),
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run feature tests")
	}
}

type ctxKeyTestConfiguration struct{}

type TestConfiguration struct {
	BackOffTimeout    time.Duration
	Browser           *rod.Browser
	LastPage          *rod.Page
	TabURLPath        string
	SelectedDirectory string
	Organizations     Organizations
	Parkeerrechten    *Parkeerrechten
	RandomID          string
	GroupID           string
	ContentHash       string
	GrantHash         string
	ServiceName       string
	Contract          *models.Contract
}

type Parkeerrechten struct {
	URL                     string
	ParkeerrechtenGrantHash string
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	orgInfos, err := getOrganizationInfos()
	if err != nil {
		log.Fatalf("failed to retrieve organization infos: %v", err)
	}

	ctx.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		godog.Logf(ctx, "initializing scenario: %s", sc.Name)

		randomID := getRandomTestID()

		godog.Logf(ctx, "random id: %s", randomID)

		browserTimeout := getEnvInt("E2E_BROWSER_TIMEOUT", 45)
		backoffTimeout := getEnvInt("E2E_BACKOFF_TIMEOUT", 30)

		ctx = WithTestConfig(ctx, &TestConfiguration{
			BackOffTimeout: time.Duration(backoffTimeout) * time.Second,
			Browser:        getBrowser().Timeout(time.Duration(browserTimeout) * time.Second),
			Organizations:  getOrganizations(orgInfos),
			Parkeerrechten: &Parkeerrechten{
				URL: getEnvString("E2E_PARKEERRECHTEN_URL", ""),
			},
			RandomID: randomID,
			GroupID:  getEnvString("E2E_GROUP_ID", "fsc-local"),
		})

		return ctx, nil
	})

	ctx.StepContext().Before(beforeStepHook)
	ctx.After(afterScenarioHook)

	// Given
	ctx.Given(`^"(.*)" is up and running$`, givenOrganizationIsUpAndRunning)
	ctx.Given(`^"(.*)" created the Service "([a-zA-Z0-9-._]{1,100})" with endpoint URL "(.+)"$`, givenOrganizationHasCreatedTheServiceWithEndpoint)
	ctx.Given(`^"(.*)" created the Service "([a-zA-Z0-9-._]{1,100})"$`, givenOrganizationHasCreatedTheService)
	ctx.Given(`^a Contract with the following grants is created by "(.+)":$`, givenOrganizationHasContractWithGrants)

	ctx.Given(`^"(.*)" accepts the Contract$`, givenOrganizationAcceptsContract)
	ctx.Given(`^"(.*)" rejects the Contract$`, givenOrganizationRejectsContract)
	ctx.Given(`^"(.*)" revokes the Contract$`, givenOrganizationRevokesContract)
	ctx.Given(`^"(.*)" verifies all Peers have Accepted the Contract$`, givenAllPeersAcceptedContract)
	ctx.Given(`^"(.*)" verifies "(.*)" has Rejected the Contract$`, givenPeerRejectedContract)
	ctx.Given(`^a kenteken is added to the parkeerrechten$`, givenKentekenAddedToParkeerrechten)

	// When
	ctx.When(`^Outway "(.*)" of "(.*)" makes a request with the (ServiceConnectionGrant|DelegatedServiceConnectionGrant) from the Contract expecting a status code (\d+)$`, whenOutwayCallsService)
	ctx.When(`^I retrieve the parkeerrechten$`, whenRetrieveParkeerrechten)
	ctx.When(`I create a service$`, whenICreateService)
	ctx.When(`I delete the service$`, whenIDeleteService)
	ctx.When(`^a Contract with the following grants is created by "(.*)":$`, givenOrganizationHasContractWithGrants)
	ctx.When(`^"(.*)" retrieves the Contract$`, whenOrganizationRetrievesContract)
	ctx.When(`I go to the incoming connections page$`, whenIGoToIncomingConnections)
	ctx.When(`I go to the service publications page on tab "(.*)"$`, whenIGoToServicePublicationsOnTab)
	ctx.When(`I go to the directories page with the selected directory (.*)$`, whenIGoToDirectories)
	ctx.When(`I go to the outgoing connections page on tab "(.*)"$`, whenIGoToOutgoingConnectionsOnTab)
	ctx.When(`I go to the delegated connections page on tab "(.*)"$`, whenIGoToDelegatedConnectionsOnTab)
	ctx.When(`^"(.*)" marks "(.*)" as Directory`, whenIMarkOrganizationAsDirectory)

	// Then
	ctx.Then(`^"(.*)" receives a response with status code (\d+)$`, thenOrganizationReceivesResponseWithStatusCode)
	ctx.Then(`^"(.*)" has a transaction record$`, thenOrganizationHasATransactionRecord)
	ctx.Then(`^"(.*)" has the same transaction record as "(.*)"$`, thenOrganizationHasSameTransactionRecordAsOrg)
	ctx.Then(`^I see the parkeerrechten$`, thenISeeParkeerrechten)
	ctx.Then(`the service is created successfully$`, thenServiceIsCreated)
	ctx.Then(`the service is deleted successfully$`, thenServiceIsDeleted)
	ctx.Then(`I see the Service publication grant in the overview$`, thenISeeServicePublication)
	ctx.Then(`I see the Service publication on the directory page$`, thenISeeServicePublicationOnDirectoryPage)
	ctx.Then(`I see the Incoming Service Connection grant in the overview$`, thenISeeTheIncomingServiceConnectionGrant)
	ctx.Then(`I see the Outgoing Service Connection grant in the overview$`, thenISeeTheOutgoingServiceConnectionGrant)
	ctx.Then(`I see the delegated service connection grant in the overview$`, thenISeeDelegatedConnection)
	ctx.Then(`all Peers have accepted the Contract$`, thenAllPeersAcceptedTheContract)
	ctx.Then(`^"(.*)" has the role Directory for "(.*)"`, thenOrganizationHasTheRoleDirectory)
}

func getRandomTestID() string {
	return uuid.Must(uuid.NewV7()).String()
}

func getBrowser() *rod.Browser {
	l := launcher.MustNewManaged(getEnvString("E2E_CHROMEDP_ADDR", "ws://127.0.0.1:7317"))

	return rod.New().Client(l.MustClient()).MustConnect()
}

func testConfigFromCtx(ctx context.Context) *TestConfiguration {
	t := godog.T(ctx)

	c, ok := ctx.Value(ctxKeyTestConfiguration{}).(*TestConfiguration)
	require.True(t, ok)

	return c
}

func WithTestConfig(ctx context.Context, c *TestConfiguration) context.Context {
	return context.WithValue(ctx, ctxKeyTestConfiguration{}, c)
}

func beforeStepHook(ctx context.Context, st *godog.Step) (context.Context, error) {
	c := testConfigFromCtx(ctx)

	c.Browser = c.Browser.WithPanic(func(err interface{}) {
		t := godog.T(ctx)

		e, ok := err.(error)
		require.True(t, ok)

		t.Fatalf("something went wrong in the browser: %s: %w", e, e)
	})

	ctx = WithTestConfig(ctx, c)

	return ctx, nil
}

func afterScenarioHook(ctx context.Context, sc *godog.Scenario, err error) (context.Context, error) {
	c := testConfigFromCtx(ctx)

	if c.Browser != nil {
		if err != nil {

			c.screenshot(sc.Name)
		}

		c.Browser.Close()
	}

	return ctx, nil
}

func (t *TestConfiguration) screenshot(name string) {
	if t.Browser != nil {
		pages := t.Browser.MustPages()

		for _, p := range pages {
			p.MustScreenshotFullPage(fmt.Sprintf("./screenshots/%s-%s.png", time.Now().Format("2006-01-02T15-04-05"), name))
		}
	}
}

func (tc *TestConfiguration) callWithBackoff(ctx context.Context, makeRequest func() error) {
	t := godog.T(ctx)

	err := backoff.RetryNotify(makeRequest, backoff.WithContext(backoff.NewExponentialBackOff(backoff.WithMaxElapsedTime(tc.BackOffTimeout)), ctx), func(err error, duration time.Duration) {
		t.Logf("received error: %v", err)
	})
	require.NoError(t, err)
}

func uint32Pointer(i uint32) *uint32 {
	return &i
}

func (t *TestConfiguration) getName(n string) string {
	return fmt.Sprintf("%s-%s", t.RandomID, n)
}
