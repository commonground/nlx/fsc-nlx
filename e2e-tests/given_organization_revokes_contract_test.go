// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/cucumber/godog"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

//nolint:funlen,gocritic,gocyclo // this is a test
func givenOrganizationRevokesContract(ctx context.Context, organizationName string) error {
	t := godog.T(ctx)

	t.Logf("given organization %s revokes the contract", organizationName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)

	revokeContract := func() error {
		t.Logf("attempt to revoke contract")

		res, err := org.ManagerInternal().RevokeContractWithResponse(ctx, testConfig.ContentHash, &models.RevokeContractParams{})
		if err != nil {
			return fmt.Errorf("could not revoke contract: %v", err)
		}

		if res.StatusCode() != http.StatusNoContent {
			return fmt.Errorf("unexpected status code %d, expected %d. response body %s", res.StatusCode(), http.StatusNoContent, string(res.Body))
		}

		return nil
	}

	testConfig.callWithBackoff(ctx, revokeContract)

	t.Logf("%s revoked the contract", organizationName)

	return nil
}
