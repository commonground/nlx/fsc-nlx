// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"

	"github.com/cucumber/godog"
)

func whenRetrieveParkeerrechten(ctx context.Context) error {
	t := godog.T(ctx)

	t.Logf("when I retrieve the parkeerrechten")

	testConfig := testConfigFromCtx(ctx)

	b := testConfig.Browser

	page := b.MustPage(testConfig.Parkeerrechten.URL, "ophalen")

	page.MustElement("input[name=Stijns]").MustInput(testConfig.Parkeerrechten.ParkeerrechtenGrantHash)
	page.MustElementR("button", "Ophalen").MustClick()

	testConfig.LastPage = page

	return nil
}
