// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"github.com/cucumber/godog"
)

func thenAllPeersAcceptedTheContract(ctx context.Context) error {
	t := godog.T(ctx)

	t.Logf("Verifies all Peers have Accepted the Contract")

	testConfig := testConfigFromCtx(ctx)

	if testConfig.Contract == nil {
		return fmt.Errorf("no contract found")
	}

	checkContract := func() error {
		for _, peer := range testConfig.Contract.Peers {
			_, ok := testConfig.Contract.Signatures.Accept[peer]
			if !ok {
				return fmt.Errorf("peer %q has not accepted the contract yet", peer)
			}
		}

		return nil
	}

	testConfig.callWithBackoff(ctx, checkContract)

	return nil

}
