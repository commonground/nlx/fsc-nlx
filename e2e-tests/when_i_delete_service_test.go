// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"

	"github.com/cucumber/godog"
)

func whenIDeleteService(ctx context.Context) error {
	t := godog.T(ctx)

	t.Log("when I delete the service")

	testConfig := testConfigFromCtx(ctx)

	b := testConfig.Browser

	page := b.MustPage(testConfig.Organizations.Get(GemeenteStijns).Info.ControllerUIURL, "services", testConfig.getName("delete-service"), "edit")

	page.MustWaitLoad()

	page.MustElement("button[value=delete]").MustClick()

	page.MustElement("button[value=delete-service]").MustClick()

	testConfig.LastPage = page

	return nil
}
