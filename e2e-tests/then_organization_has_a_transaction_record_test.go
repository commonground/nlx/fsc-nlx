// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"net/http"

	"github.com/cucumber/godog"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func thenOrganizationHasATransactionRecord(ctx context.Context, organizationName string) error {
	t := godog.T(ctx)

	t.Logf("organization %s has a transaction record", organizationName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)

	rec := retrieveTXLog(ctx, testConfig, org)

	org.State.TxLogID = rec.TransactionId

	return nil
}

func thenOrganizationHasSameTransactionRecordAsOrg(ctx context.Context, organizationName, otherOrganizationName string) error {
	t := godog.T(ctx)

	t.Logf("organization %s has a transaction record", organizationName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)
	otherOrg := testConfig.Organizations.Get(otherOrganizationName)

	rec := retrieveTXLog(ctx, testConfig, org)

	require.Equal(t, rec.TransactionId, otherOrg.State.TxLogID)

	org.State.TxLogID = rec.TransactionId

	return nil
}

func retrieveTXLog(ctx context.Context, testConfig *TestConfiguration, org *Organization) *models.LogRecord {
	t := godog.T(ctx)

	t.Logf("retrieving Tx log records based on grant hash: %s", testConfig.GrantHash)

	require.NotEmpty(t, testConfig.GrantHash)

	logRes, err := org.ManagerInternal().GetLogsWithResponse(ctx, &models.GetLogsParams{
		GrantHash: &[]string{testConfig.GrantHash},
		Limit:     uint32Pointer(1),
	})
	require.NoError(t, err)

	require.Equal(t, http.StatusOK, logRes.StatusCode())

	require.Len(t, logRes.JSON200.Records, 1)

	return &logRes.JSON200.Records[0]
}
