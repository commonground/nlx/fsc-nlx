// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/cucumber/godog"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func whenIMarkOrganizationAsDirectory(ctx context.Context, organizationName, directoryName string) error {
	t := godog.T(ctx)

	t.Logf("%s is marked as directory, by: %s", directoryName, organizationName)

	testConfig := testConfigFromCtx(ctx)
	org := testConfig.Organizations.Get(organizationName)
	orgAsDirectory := testConfig.Organizations.Get(directoryName)

	updatePeer := func() error {
		updatePeerResponse, err := org.ManagerInternal().UpdatePeerWithResponse(ctx, &models.UpdatePeerParams{}, models.UpdatePeerJSONRequestBody{
			Peer: models.Peer{
				Id:             orgAsDirectory.Info.OIN,
				ManagerAddress: orgAsDirectory.Info.ManagerExternalRESTAPIURL,
				Name:           directoryName,
				Roles:          []models.PeerRole{models.PEERROLEDIRECTORY},
			},
		})
		if err != nil {
			return fmt.Errorf("could not update peer: %v", err)
		}

		if updatePeerResponse.StatusCode() != http.StatusNoContent {
			return fmt.Errorf("could not update peer: want status: 204, got status: %d: %s", updatePeerResponse.StatusCode(), string(updatePeerResponse.Body))
		}

		return nil
	}

	testConfig.callWithBackoff(ctx, updatePeer)

	return nil
}
