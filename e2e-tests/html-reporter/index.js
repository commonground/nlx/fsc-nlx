/**
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

var reporter = require('cucumber-html-reporter');

var options = {
    theme: 'foundation',
    jsonFile: './output/report.json',
    output: './output/report.html',
    reportSuiteAsScenarios: true,
    scenarioTimestamp: true,
    failedSummaryReport: true,
    name: 'FSC - Test suite - Integration',
    brandTitle: 'FSC - Test suite - Integration'
};

reporter.generate(options);
