// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/cucumber/godog"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func givenPeerRejectedContract(ctx context.Context, organizationName, organizationNameWithRejectSignature string) error {
	t := godog.T(ctx)

	t.Logf("%q verifies %q has Rejected the Contract", organizationName, organizationNameWithRejectSignature)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)
	orgWithRejectSignature := testConfig.Organizations.Get(organizationNameWithRejectSignature)

	checkContract := func() error {
		contractsResp, err := org.ManagerInternal().GetContractsWithResponse(ctx, &models.GetContractsParams{
			Cursor:      nil,
			Limit:       nil,
			SortOrder:   nil,
			GrantType:   nil,
			ContentHash: &[]string{testConfig.ContentHash},
		})
		if err != nil {
			return fmt.Errorf("could not get contract: %v", err)
		}

		if contractsResp.StatusCode() != http.StatusOK {
			return fmt.Errorf("could nog get contract: want status: 200, got status: %d: %s", contractsResp.StatusCode(), string(contractsResp.Body))
		}

		if len(contractsResp.JSON200.Contracts) != 1 {
			return fmt.Errorf("contract with hash %q not found", testConfig.ContentHash)
		}

		contract := contractsResp.JSON200.Contracts[0]

		if contract.Hash != testConfig.ContentHash {
			return fmt.Errorf("got wrong contract from manager: got: %q, want: %q", contract.Hash, testConfig.ContentHash)
		}

		for _, peer := range contract.Peers {
			if peer != orgWithRejectSignature.Info.OIN {
				continue
			}

			_, ok := contract.Signatures.Reject[orgWithRejectSignature.Info.OIN]
			if !ok {
				return fmt.Errorf("peer %q has not rejected the contract yet", peer)
			}
		}

		return nil
	}

	testConfig.callWithBackoff(ctx, checkContract)

	return nil
}
