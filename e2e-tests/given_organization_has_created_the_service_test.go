// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"net/http"

	"github.com/cucumber/godog"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api/models"
)

func givenOrganizationHasCreatedTheService(ctx context.Context, organizationName, serviceName string) error {
	createService(ctx, organizationName, serviceName, "https://nlx.io")

	return nil
}

func givenOrganizationHasCreatedTheServiceWithEndpoint(ctx context.Context, organizationName, serviceName, endpointURL string) error {
	createService(ctx, organizationName, serviceName, endpointURL)

	return nil
}

func createService(ctx context.Context, organizationName, serviceName, endpointURL string) {
	t := godog.T(ctx)

	t.Logf("given organization %s has created the service %q", organizationName, serviceName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)

	inways, err := org.Controller().GetInwaysWithResponse(ctx)
	require.NoError(t, err)

	require.Equal(t, http.StatusOK, inways.StatusCode())

	require.NotEmpty(t, inways.JSON200.Inways)

	res, err := org.Controller().CreateServiceWithResponse(ctx, models.CreateServiceJSONRequestBody{
		EndpointUrl:  endpointURL,
		InwayAddress: inways.JSON200.Inways[0].Address,
		Name:         testConfig.getName(serviceName),
	})
	require.NoError(t, err)

	require.Equal(t, http.StatusCreated, res.StatusCode(), string(res.Body))
}
