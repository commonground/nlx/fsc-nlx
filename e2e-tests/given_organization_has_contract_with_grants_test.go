// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"net/http"
	"time"

	"github.com/cucumber/godog"
	"github.com/stretchr/testify/require"

	controller_api "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

//nolint:funlen,gocyclo // complicated test
func givenOrganizationHasContractWithGrants(ctx context.Context, organizationName string, table *godog.Table) error {
	t := godog.T(ctx)

	t.Logf("given organization %s has contract with grants", organizationName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)

	contractContent := models.ContractContent{
		CreatedAt:     time.Now().Unix(),
		Grants:        getGrants(ctx, testConfig, org, table),
		GroupId:       testConfig.GroupID,
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Iv:            testConfig.RandomID,
		Validity: models.Validity{
			NotBefore: time.Now().Add(time.Hour * -1).Unix(),
			NotAfter:  time.Now().Add(time.Hour).Unix(),
		},
	}

	res, err := org.ManagerInternal().CreateContractWithResponse(ctx, &models.CreateContractParams{}, models.CreateContractJSONRequestBody{
		ContractContent: contractContent,
	})
	require.NoError(t, err)

	require.Equal(t, http.StatusCreated, res.StatusCode(), string(res.Body))

	testConfig.ContentHash = res.JSON201.ContentHash

	return nil
}

func getGrants(ctx context.Context, testConfig *TestConfiguration, org *Organization, table *godog.Table) []models.Grant {
	t := godog.T(ctx)

	grants := make([]models.Grant, 0)

	head := table.Rows[0].Cells

	for i := 1; i < len(table.Rows); i++ {
		var (
			grantType              string
			serviceType            string
			servicePeerID          string
			serviceName            string
			serviceProtocol        string
			serviceDelegatorPeerID string
			outwayPeerID           string
			outwayThumbprint       string
			outwayDelegatorPeerID  string
			directoryPeerID        string
		)

		for n, cell := range table.Rows[i].Cells {
			switch head[n].Value {
			case "grant_type":
				grantType = cell.Value
			case "service_type":
				serviceType = cell.Value
			case "service_peer_id":
				servicePeerID = cell.Value
			case "service_name":
				serviceName = testConfig.getName(cell.Value)
				testConfig.ServiceName = serviceName
			case "service_protocol":
				serviceProtocol = cell.Value
			case "service_delegator_peer_id":
				serviceDelegatorPeerID = cell.Value
			case "outway_peer_id":
				outwayPeerID = cell.Value
			case "outway_name":
				outwayThumbprint = getOutwayThumbprint(ctx, org.Controller(), cell.Value)
			case "outway_delegator_peer_id":
				outwayDelegatorPeerID = cell.Value
			case "directory_peer_id":
				directoryPeerID = cell.Value
			default:
				t.Fatalf("unexpected column name: %s", head[n].Value)
			}
		}

		if serviceProtocol == "" {
			serviceProtocol = "PROTOCOL_TCP_HTTP_1.1"
		}

		grant := models.Grant{}

		switch grantType {
		case "servicePublication":
			err := grant.FromGrantServicePublication(models.GrantServicePublication{
				Directory: models.DirectoryPeer{
					PeerId: directoryPeerID,
				},
				Service: models.ServicePublication{
					Name:     serviceName,
					PeerId:   servicePeerID,
					Protocol: models.Protocol(serviceProtocol),
				},
			})
			require.NoError(t, err)
		case "delegatedServicePublication":
			err := grant.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
				Directory: models.DirectoryPeer{
					PeerId: directoryPeerID,
				},
				Service: models.ServicePublication{
					Name:     serviceName,
					PeerId:   servicePeerID,
					Protocol: models.Protocol(serviceProtocol),
				},
				Delegator: models.DelegatorPeer{
					PeerId: serviceDelegatorPeerID,
				},
			})
			require.NoError(t, err)
		case "serviceConnection":
			service := models.GrantServiceConnection_Service{}

			switch serviceType {
			case "service":
				err := service.FromService(models.Service{
					Peer: models.Peer{Id: servicePeerID},
					Name: serviceName,
				})
				require.NoError(t, err)
			case "delegatedService":
				err := service.FromDelegatedService(models.DelegatedService{
					Peer:      models.Peer{Id: servicePeerID},
					Delegator: models.Peer{Id: serviceDelegatorPeerID},
					Name:      serviceName,
				})
				require.NoError(t, err)
			default:
				t.Fatalf("unknown service type: %s", serviceType)
			}

			err := grant.FromGrantServiceConnection(models.GrantServiceConnection{
				Outway: models.OutwayPeer{
					PeerId:              outwayPeerID,
					PublicKeyThumbprint: outwayThumbprint,
				},
				Service: service,
			})
			require.NoError(t, err)
		case "delegatedServiceConnection":
			service := models.GrantDelegatedServiceConnection_Service{}

			switch serviceType {
			case "service":
				err := service.FromService(models.Service{
					Peer: models.Peer{Id: servicePeerID},
					Name: serviceName,
				})
				require.NoError(t, err)
			case "delegatedService":
				err := service.FromDelegatedService(models.DelegatedService{
					Peer:      models.Peer{Id: servicePeerID},
					Delegator: models.Peer{Id: serviceDelegatorPeerID},
					Name:      serviceName,
				})
				require.NoError(t, err)
			default:
				t.Fatalf("unknown service type: %s", serviceType)
			}

			err := grant.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
				Outway: models.OutwayPeer{
					PeerId:              outwayPeerID,
					PublicKeyThumbprint: outwayThumbprint,
				},
				Service:   service,
				Delegator: models.DelegatorPeer{PeerId: outwayDelegatorPeerID},
			})
			require.NoError(t, err)
		default:
			t.Fatalf("unknown grant type: %s", grantType)
		}

		grants = append(grants, grant)
	}

	return grants
}

func getOutwayThumbprint(ctx context.Context, client *controller_api.ClientWithResponses, outwayName string) string {
	t := godog.T(ctx)

	response, err := client.GetOutwaysWithResponse(ctx)
	require.NoError(t, err)

	for _, outway := range response.JSON200.Outways {
		if outway.Name != outwayName {
			continue
		}

		t.Logf("found thumbprint: %q", outway.PublicKeyThumbprint)

		return outway.PublicKeyThumbprint
	}

	t.Fatalf("outway %q not found", outwayName)

	return ""
}
