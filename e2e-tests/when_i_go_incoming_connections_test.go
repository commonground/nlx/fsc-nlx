// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"

	"github.com/cucumber/godog"
)

func whenIGoToIncomingConnections(ctx context.Context) error {
	t := godog.T(ctx)

	t.Log("when I go to the incoming connections page")

	testConfig := testConfigFromCtx(ctx)

	b := testConfig.Browser

	page := b.MustPage(testConfig.Organizations.Get(RvRD).Info.ControllerUIURL, "incoming-connections")

	page.MustWaitLoad()

	testConfig.LastPage = page

	return nil
}
