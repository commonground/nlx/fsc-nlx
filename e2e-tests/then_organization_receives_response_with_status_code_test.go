// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"io"

	"github.com/cucumber/godog"
	"github.com/stretchr/testify/require"
)

func thenOrganizationReceivesResponseWithStatusCode(ctx context.Context, organizationName string, wantStatusCode int) error {
	t := godog.T(ctx)

	t.Logf("organization %s receives a response with statuscode", organizationName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)

	res := org.State.LatestRequestResponse

	defer res.Body.Close()

	responseBody, err := io.ReadAll(res.Body)
	require.NoError(t, err)

	require.Equal(t, wantStatusCode, res.StatusCode, string(responseBody))

	return nil
}
