// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"

	"github.com/cucumber/godog"
)

func thenISeeParkeerrechten(ctx context.Context) error {
	t := godog.T(ctx)

	t.Logf("then I see the parkeerrechten")

	testConfig := testConfigFromCtx(ctx)

	testConfig.callWithBackoff(ctx, func() error {
		cells := testConfig.LastPage.MustElements("td")

		for _, c := range cells {
			kenteken := c.MustText()

			t.Logf("found kenteken: %q", kenteken)

			if kenteken == "RT774D" {
				return nil
			}
		}

		return fmt.Errorf("could not find correct kenteken on page")
	})

	return nil
}
