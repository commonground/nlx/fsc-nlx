// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"io"
	"net/http"

	"github.com/cucumber/godog"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

//nolint:funlen,govet,bodyclose,gocritic,gocyclo // this is a test
func whenOutwayCallsService(ctx context.Context, outwayName, organizationName, connectionGrantType string, wantStatusCode int) error {
	t := godog.T(ctx)

	t.Logf("when outway %s of organization %s makes request using the contract", outwayName, organizationName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)

	makeRequest := func() error {
		contractsResp, err := org.ManagerInternal().GetContractsWithResponse(ctx, &models.GetContractsParams{ //nolint:govet
			Cursor:      nil,
			Limit:       nil,
			SortOrder:   nil,
			GrantType:   nil,
			ContentHash: &[]string{testConfig.ContentHash},
		})
		if err != nil {
			return fmt.Errorf("could not get contracts: %v", err)
		}

		if len(contractsResp.JSON200.Contracts) != 1 {
			return fmt.Errorf("contract with hash %q not found", testConfig.ContentHash)
		}

		contract := contractsResp.JSON200.Contracts[0]

		if contract.Hash != testConfig.ContentHash {
			return fmt.Errorf("got wrong contract from manager: got: %q, want: %q", contract.Hash, testConfig.ContentHash)
		}

		var grantHash string

		switch connectionGrantType {
		case "ServiceConnectionGrant":
			g, err := contract.Content.Grants[0].AsGrantServiceConnection()
			if err != nil {
				return fmt.Errorf("could not convert grant into service connection grant: %v", err)
			}

			grantHash = g.Hash
		case "DelegatedServiceConnectionGrant":
			g, err := contract.Content.Grants[0].AsGrantDelegatedServiceConnection()
			if err != nil {
				return fmt.Errorf("could not convert grant into delegated service connection grant: %v", err)
			}

			grantHash = g.Hash
		default:
			return fmt.Errorf("unknown connection grant type: %q", connectionGrantType)
		}

		t.Logf("Grant Hash when making request to the Outway: %q", grantHash)

		testConfig.GrantHash = grantHash

		req, err := http.NewRequest(http.MethodGet, org.Info.DefaultOutwayURL, http.NoBody)
		if err != nil {
			return fmt.Errorf("could not create request: %v", err)
		}

		req.Header.Add("fsc-grant-hash", grantHash)

		client := &http.Client{}

		res, err := client.Do(req)
		if err != nil {
			return fmt.Errorf("could not execute request to outway: %v", err)
		}

		if res.StatusCode != wantStatusCode {
			defer res.Body.Close()

			responseBody, err := io.ReadAll(res.Body)
			if err != nil {
				return fmt.Errorf("could not read error body from outway: %w", err)
			}

			return fmt.Errorf("received unexpected status code from outway, want: %d, got: %d: %s", wantStatusCode, res.StatusCode, string(responseBody))
		}

		org.State.LatestRequestResponse = res

		return nil
	}

	testConfig.callWithBackoff(ctx, makeRequest)

	return nil
}
