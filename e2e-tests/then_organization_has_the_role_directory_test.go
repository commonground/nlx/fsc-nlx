// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"net/http"
	"slices"

	"github.com/cucumber/godog"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func thenOrganizationHasTheRoleDirectory(ctx context.Context, directoryName, organizationName string) error {
	t := godog.T(ctx)

	t.Logf("%s has the role of directory, by: %s", directoryName, organizationName)

	testConfig := testConfigFromCtx(ctx)
	org := testConfig.Organizations.Get(organizationName)
	orgAsDirectory := testConfig.Organizations.Get(directoryName)

	getPeers := func() error {
		peerIDs := []models.PeerID{orgAsDirectory.Info.OIN}

		getPeersResponse, err := org.ManagerInternal().ListPeersWithResponse(ctx, &models.ListPeersParams{
			PeerIds: &peerIDs,
		})
		if err != nil {
			return fmt.Errorf("could not list peers: %v", err)
		}

		if getPeersResponse.StatusCode() != http.StatusOK {
			return fmt.Errorf("could not list peers: want status: 200, got status: %d: %s", getPeersResponse.StatusCode(), string(getPeersResponse.Body))
		}

		if len(getPeersResponse.JSON200.Peers) == 0 {
			return fmt.Errorf("organization %s does not exist", directoryName)
		}

		if !slices.Contains(getPeersResponse.JSON200.Peers[0].Roles, models.PEERROLEDIRECTORY) {
			return fmt.Errorf("organization %s does not have the role PEER_ROLE_DIRECTORY", directoryName)
		}

		return nil
	}

	testConfig.callWithBackoff(ctx, getPeers)

	return nil
}
