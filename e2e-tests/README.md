E2E tests
---

## Local setup

### 1. Start development setup from the root of the project

```shell
sh ../scripts/start-development.sh
```

### 3. Run the E2E tests

```shell
go test --tags=e2e
```

## General

The following list contains the environment variables
being used along with their default value:

```
E2E_BACKOFF_TIMEOUT: 30
E2E_BROWSER_TIMEOUT: 45

E2E_GEMEENTE_STIJNS_PEER_ID: "12345678901234567890"
E2E_GEMEENTE_STIJNS_CONTROLLER_UI_URL: "http://controller.organization-a.nlx.localhost:3011"
E2E_GEMEENTE_STIJNS_CONTROLLER_UI_AUTHENTICATION_REQUIRED: "true"
E2E_GEMEENTE_STIJNS_CONTROLLER_UI_USERNAME: "admin"
E2E_GEMEENTE_STIJNS_CONTROLLER_UI_PASSWORD: "password"
E2E_GEMEENTE_STIJNS_CONTROLLER_REST_API_URL: "https://controller-api.organization-a.nlx.localhost:7615"
E2E_GEMEENTE_STIJNS_DEFAULT_OUTWAY_URL: "http://localhost:7605"
E2E_GEMEENTE_STIJNS_MANAGER_INTERNAL_REST_API_URL: "https://manager.organization-a.nlx.localhost:443"
E2E_GEMEENTE_STIJNS_CONTROLLER_INTERNAL_CERT_ROOT_CA_PATH: "../pki/internal/organization-a/ca/root-1.pem"
E2E_GEMEENTE_STIJNS_CONTROLLER_INTERNAL_CERT_PATH: "../pki/internal/organization-a/certs/controller-api/cert.pem"
E2E_GEMEENTE_STIJNS_CONTROLLER_INTERNAL_CERT_KEY_PATH: "../pki/internal/organization-a/certs/controller-api/key.pem"
E2E_GEMEENTE_STIJNS_MANAGER_INTERNAL_CERT_ROOT_CA_PATH: "../pki/internal/organization-a/ca/root-1.pem"
E2E_GEMEENTE_STIJNS_MANAGER_INTERNAL_CERT_PATH: "../pki/internal/organization-a/certs/controller-api/cert.pem"
E2E_GEMEENTE_STIJNS_MANAGE_INTERNAL_CERT_KEY_PATH: "../pki/internal/organization-a/certs/controller-api/key.pem"

E2E_RVRD_PEER_ID: "12345678901234567891"
E2E_RVRD_CONTROLLER_UI_URL: "http://controller.organization-b.nlx.localhost:3021"
E2E_RVRD_CONTROLLER_UI_AUTHENTICATION_REQUIRED: "false"
E2E_RVRD_CONTROLLER_UI_USERNAME: ""
E2E_RVRD_CONTROLLER_UI_PASSWORD: ""
E2E_RVRD_CONTROLLER_REST_API_URL: "https://controller-api.organization-b.nlx.localhost:7710"
E2E_RVRD_DEFAULT_OUTWAY_URL: ""
E2E_RVRD_MANAGER_INTERNAL_REST_API_URL: "https://manager.organization-b.nlx.localhost:443"
E2E_RVRD_CONTROLLER_INTERNAL_CERT_ROOT_CA_PATH: "../pki/internal/organization-b/ca/root-1.pem"
E2E_RVRD_CONTROLLER_INTERNAL_CERT_PATH: "../pki/internal/organization-b/certs/controller-api/cert.pem"
E2E_RVRD_CONTROLLER_INTERNAL_CERT_KEY_PATH: "../pki/internal/organization-b/certs/controller-api/key.pem"
E2E_RVRD_MANAGER_INTERNAL_CERT_ROOT_CA_PATH: "../pki/internal/organization-b/ca/root-1.pem"
E2E_RVRD_MANAGER_INTERNAL_CERT_PATH: "../pki/internal/organization-b/certs/controller-api/cert.pem"
E2E_RVRD_MANAGER_INTERNAL_CERT_KEY_PATH: "../pki/internal/organization-b/certs/controller-api/key.pem"
```
