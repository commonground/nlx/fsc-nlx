// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/cucumber/godog"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

//nolint:funlen,gocritic,gocyclo // this is a test
func givenOrganizationAcceptsContract(ctx context.Context, organizationName string) error {
	t := godog.T(ctx)

	t.Logf("given organization %s accepts the contract", organizationName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)

	acceptContract := func() error {
		res, err := org.ManagerInternal().AcceptContractWithResponse(ctx, testConfig.ContentHash, &models.AcceptContractParams{})
		if err != nil {
			return fmt.Errorf("could not accept contract: %v", err)
		}

		if res.StatusCode() != http.StatusNoContent {
			return fmt.Errorf("unexpected status code %d. response body %s", res.StatusCode(), string(res.Body))
		}

		return nil
	}

	testConfig.callWithBackoff(ctx, acceptContract)

	t.Logf("%s accepted the contract", organizationName)

	return nil
}
