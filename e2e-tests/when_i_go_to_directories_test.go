// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"

	"github.com/cucumber/godog"
)

func whenIGoToDirectories(ctx context.Context, selectedDirectoryPath string) error {
	t := godog.T(ctx)

	t.Logf("when I go to the directories page with the selected directory %s", selectedDirectoryPath)

	testConfig := testConfigFromCtx(ctx)
	testConfig.SelectedDirectory = selectedDirectoryPath

	b := testConfig.Browser

	urlPath := fmt.Sprintf("/directory?directoryPeerID=%s", selectedDirectoryPath)
	page := b.MustPage(testConfig.Organizations.Get(GemeenteStijns).Info.ControllerUIURL, urlPath)

	testConfig.LastPage = page

	return nil
}
