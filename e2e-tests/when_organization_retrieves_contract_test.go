// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/cucumber/godog"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func whenOrganizationRetrievesContract(ctx context.Context, organizationName string) error {
	t := godog.T(ctx)

	t.Logf("%s Verifies all Peers have Accepted the Contract", organizationName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)

	checkContract := func() error {
		contractsResp, err := org.ManagerInternal().GetContractsWithResponse(ctx, &models.GetContractsParams{
			Cursor:      nil,
			Limit:       nil,
			SortOrder:   nil,
			GrantType:   nil,
			ContentHash: &[]string{testConfig.ContentHash},
		})
		if err != nil {
			return fmt.Errorf("could not get contract: %v", err)
		}

		if contractsResp.StatusCode() != http.StatusOK {
			return fmt.Errorf("could not get contract: want status: 200, got status: %d: %s", contractsResp.StatusCode(), string(contractsResp.Body))
		}

		if len(contractsResp.JSON200.Contracts) != 1 {
			return fmt.Errorf("contract with hash %q not found", testConfig.ContentHash)
		}

		contract := contractsResp.JSON200.Contracts[0]

		if contract.Hash != testConfig.ContentHash {
			return fmt.Errorf("got wrong contract from manager: got: %q, want: %q", contract.Hash, testConfig.ContentHash)
		}

		testConfig.Contract = &contract

		for _, peer := range contract.Peers {
			_, ok := contract.Signatures.Accept[peer]
			if !ok {
				return fmt.Errorf("peer %q has not accepted the contract yet", peer)
			}
		}

		return nil
	}

	testConfig.callWithBackoff(ctx, checkContract)

	return nil

}
