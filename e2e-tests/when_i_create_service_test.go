// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"

	"github.com/cucumber/godog"
)

func whenICreateService(ctx context.Context) error {
	t := godog.T(ctx)

	t.Log("when I create a service")

	testConfig := testConfigFromCtx(ctx)

	b := testConfig.Browser

	page := b.MustPage(testConfig.Organizations.Get(GemeenteStijns).Info.ControllerUIURL, "services")

	page.MustElementR("a", "Service toevoegen").MustClick()

	page.MustElement("input[name=name]").MustInput(testConfig.getName("basisregister"))
	page.MustElement("input[name=endpointURL]").MustInput("https://example.com")
	page.MustElementR("button", "Service toevoegen").MustClick()

	testConfig.LastPage = page

	return nil
}
