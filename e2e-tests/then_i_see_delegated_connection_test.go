// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"github.com/cucumber/godog"
)

func thenISeeDelegatedConnection(ctx context.Context) error {
	t := godog.T(ctx)

	t.Log("then I see the delegated service connection grant")

	testConfig := testConfigFromCtx(ctx)

	hashFoundOnPage := false

	var err error

	pageNumber := 1

	for !hashFoundOnPage {
		filePath := fmt.Sprintf("./screenshots/then-i-see-delegated-connection-page-%d-%q.png", pageNumber, testConfig.ContentHash)
		testConfig.LastPage.MustScreenshotFullPage(filePath)

		hashFoundOnPage, _, err = testConfig.LastPage.Has(fmt.Sprintf(`a[href^="/contract/%s"]`, testConfig.ContentHash))
		if err != nil {
			return err
		}

		if hashFoundOnPage {
			continue
		}

		nextPageAvailable, hrefToNextPage, errHas := testConfig.LastPage.Has(fmt.Sprintf(`a[href*="/delegated-connections/%s?pagination_cursor="]`, testConfig.TabURLPath))
		if errHas != nil {
			return errHas
		}

		if !nextPageAvailable {
			return fmt.Errorf("delegated service connection with hash %q not found", testConfig.ContentHash)
		}

		path, _ := hrefToNextPage.Attribute("href")

		url := fmt.Sprintf("%s%s", testConfig.Organizations.Get(GemeenteStijns).Info.ControllerUIURL, *path)
		testConfig.LastPage = testConfig.Browser.MustPage(url)

		testConfig.LastPage.MustWaitLoad()

		pageNumber++
	}

	return nil
}
