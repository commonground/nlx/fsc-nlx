// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"strings"

	"github.com/cucumber/godog"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func givenKentekenAddedToParkeerrechten(ctx context.Context) error {
	t := godog.T(ctx)

	t.Logf("given kenteken added to parkeerrechten")

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(VergunningsoftwareBV)

	contractsResp, err := org.ManagerInternal().GetContractsWithResponse(ctx, &models.GetContractsParams{ //nolint:govet
		Cursor:      nil,
		Limit:       nil,
		SortOrder:   nil,
		GrantType:   nil,
		ContentHash: &[]string{testConfig.ContentHash},
	})
	require.NoError(t, err)

	require.Len(t, contractsResp.JSON200.Contracts, 1)

	contract := contractsResp.JSON200.Contracts[0]

	require.Equal(t, testConfig.ContentHash, contract.Hash)

	var (
		parkeerrechtenGrantHash string
		kentekenGrantHash       string
	)

	for _, g := range contract.Content.Grants {
		grant, err := g.AsGrantServiceConnection()
		require.NoError(t, err)

		s, err := grant.Service.AsService()
		require.NoError(t, err)

		if strings.HasSuffix(s.Name, "basisregister-fictieve-kentekens") {
			kentekenGrantHash = grant.Hash
		} else if strings.HasSuffix(s.Name, "parkeerrechten") {
			parkeerrechtenGrantHash = grant.Hash
		} else {
			t.Fatal("invalid service name found: %q", s.Name)
		}
	}

	testConfig.Parkeerrechten.ParkeerrechtenGrantHash = parkeerrechtenGrantHash

	b := testConfig.Browser

	page := b.MustPage(testConfig.Parkeerrechten.URL, "toevoegen")

	page.MustElement("#kentekenGrantHash").MustInput(kentekenGrantHash)
	page.MustElement("#parkeerrechtenGrantHash").MustInput(parkeerrechtenGrantHash)
	page.MustElementR("button", "Toevoegen").MustClick()

	page.MustElement(".alert--success").MustWaitVisible()

	return nil
}
