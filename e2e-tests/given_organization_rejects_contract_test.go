// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/cucumber/godog"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

//nolint:funlen,gocritic,gocyclo // this is a test
func givenOrganizationRejectsContract(ctx context.Context, organizationName string) error {
	t := godog.T(ctx)

	t.Logf("given organization %s rejects the contract", organizationName)

	testConfig := testConfigFromCtx(ctx)

	org := testConfig.Organizations.Get(organizationName)

	rejectContract := func() error {
		t.Logf("attempt to reject contract")

		res, err := org.ManagerInternal().RejectContractWithResponse(ctx, testConfig.ContentHash, &models.RejectContractParams{})
		if err != nil {
			return fmt.Errorf("could not reject contract: %v", err)
		}

		if res.StatusCode() != http.StatusNoContent {
			return fmt.Errorf("unexpected status code %d. response body %s", res.StatusCode(), string(res.Body))
		}

		return nil
	}

	testConfig.callWithBackoff(ctx, rejectContract)

	t.Logf("%s rejected the contract", organizationName)

	return nil
}
