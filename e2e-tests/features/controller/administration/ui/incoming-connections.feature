Feature: Incoming Connections

    @controller-administration-ui
    Scenario: Show incoming connections
        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And "RvRD" created the Service "basisregister-fictieve-kentekens"
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type        | service_type     | service_peer_id      | service_name                     | outway_peer_id       | outway_name                      |
            | serviceConnection | service          | 12345678901234567891 | basisregister-fictieve-kentekens | 12345678901234567890 | gemeente-stijns-fsc-nlx-outway   |
        And "RvRD" accepts the Contract
        When I go to the incoming connections page
        Then I see the Incoming Service Connection grant in the overview
