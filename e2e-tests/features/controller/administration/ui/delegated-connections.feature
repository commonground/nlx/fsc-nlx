Feature: Delegated Connections

    @controller-administration-ui
    Scenario: Show proposed delegated connections
        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type                 | service_type     | service_peer_id      | service_name                    | outway_delegator_peer_id    | outway_peer_id       | outway_name                      |
            | delegatedServiceConnection | service          | 12345678901234567891 | basisregister-fictieve-personen | 12345678901234567890        | 12345678901234567890 | gemeente-stijns-fsc-nlx-outway   |
        When I go to the delegated connections page on tab "proposed"
        Then I see the delegated service connection grant in the overview

    @controller-administration-ui
    Scenario: Show archived delegated connections
        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type                 | service_type     | service_peer_id      | service_name                    | outway_delegator_peer_id    | outway_peer_id       | outway_name                      |
            | delegatedServiceConnection | service          | 12345678901234567891 | basisregister-fictieve-personen | 12345678901234567890        | 12345678901234567890 | gemeente-stijns-fsc-nlx-outway   |
        And "RvRD" rejects the Contract
        And "Gemeente Stijns" verifies "RvRD" has Rejected the Contract
        When I go to the delegated connections page on tab "archive"
        Then I see the delegated service connection grant in the overview

    @controller-administration-ui
    Scenario: Show active delegated connections
        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type                 | service_type     | service_peer_id      | service_name                    | outway_delegator_peer_id    | outway_peer_id       | outway_name                      |
            | delegatedServiceConnection | service          | 12345678901234567891 | basisregister-fictieve-personen | 12345678901234567890        | 12345678901234567890 | gemeente-stijns-fsc-nlx-outway   |
        And "RvRD" accepts the Contract
        And "Gemeente Stijns" verifies all Peers have Accepted the Contract
        When I go to the delegated connections page on tab "active"
        Then I see the delegated service connection grant in the overview
