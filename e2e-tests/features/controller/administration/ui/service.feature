Feature: Service

    @controller-administration-ui
    Scenario: Create a service
        Given "Gemeente Stijns" is up and running
        When I create a service
        Then the service is created successfully

    @controller-administration-ui
    Scenario: Delete a service
        Given "Gemeente Stijns" is up and running
        And "Gemeente Stijns" created the Service "delete-service" with endpoint URL "http://delete-service"
        When I delete the service
        Then the service is deleted successfully
