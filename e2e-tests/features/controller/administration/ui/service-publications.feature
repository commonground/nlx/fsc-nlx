Feature: Service Publications

    @controller-administration-ui
    Scenario: Show proposed service publications
        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type         | service_peer_id      | service_name                     | directory_peer_id    |
            | servicePublication | 12345678901234567890 | basisregister-fictieve-kentekens | 12345678901234567891 |
        When I go to the service publications page on tab "proposed"
        Then I see the Service publication grant in the overview

    @controller-administration-ui
    Scenario: Show archived service publications
        Given "Gemeente Stijns" is up and running
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type         | service_peer_id      | service_name                     | directory_peer_id    |
            | servicePublication | 12345678901234567890 | basisregister-fictieve-kentekens | 12345678901234567899 |
        And "Gemeente Stijns" verifies all Peers have Accepted the Contract
        And "Gemeente Stijns" revokes the Contract
        When I go to the service publications page on tab "archive"
        Then I see the Service publication grant in the overview

    @controller-administration-ui
    Scenario: Show active service publications
        Given "Gemeente Stijns" is up and running
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type         | service_peer_id      | service_name                     | directory_peer_id    |
            | servicePublication | 12345678901234567890 | basisregister-fictieve-kentekens | 12345678901234567899 |
        And "Gemeente Stijns" verifies all Peers have Accepted the Contract
        When I go to the service publications page on tab "active"
        Then I see the Service publication grant in the overview

    @controller-administration-ui
    Scenario: Show active service publications on directory page
        Given "Gemeente Stijns" is up and running
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type         | service_peer_id      | service_name                     | directory_peer_id    |
            | servicePublication | 12345678901234567890 | basisregister-fictieve-kentekens | 12345678901234567899 |
        And "Gemeente Stijns" verifies all Peers have Accepted the Contract
        When I go to the directories page with the selected directory 12345678901234567899
        Then I see the Service publication on the directory page

    @controller-administrator-ui
    Scenario: Select a different directory on the directory page
        Given "Gemeente Stijns" is up and running
        And "Vergunningsoftware BV" is up and running
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type         | service_peer_id      | service_name                     | directory_peer_id    |
            | servicePublication | 12345678901234567890 | basisregister-fictieve-kentekens | 12345678901234567899 |
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type         | service_peer_id      | service_name                     | directory_peer_id    |
            | servicePublication | 12345678901234567890 | basisregister-fictieve-kentekens | 12345678901234567892 |
        And "Vergunningsoftware BV" accepts the Contract
        And "Gemeente Stijns" verifies all Peers have Accepted the Contract
        When "Gemeente Stijns" marks "Vergunningsoftware BV" as Directory
        When I go to the directories page with the selected directory 12345678901234567892
        Then I see the Service publication on the directory page
