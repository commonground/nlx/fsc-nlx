Feature: Outway

    @core
    Scenario: Integration-ConsumeService-1
        Consume a Service

        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And "RvRD" created the Service "basisregister-fictieve-kentekens"
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type        | service_type     | service_peer_id      | service_name                     | outway_peer_id       | outway_name                      |
            | serviceConnection | service          | 12345678901234567891 | basisregister-fictieve-kentekens | 12345678901234567890 | gemeente-stijns-fsc-nlx-outway   |
        And "RvRD" accepts the Contract
        When Outway "gemeente-stijns-nlx-outway" of "Gemeente Stijns" makes a request with the ServiceConnectionGrant from the Contract expecting a status code 200
        Then "Gemeente Stijns" receives a response with status code 200

    @core
    Scenario: Integration-ConsumeService-2
        Consume a Service without a valid Contract

        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And "RvRD" created the Service "basisregister-fictieve-kentekens"
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type        | service_type     | service_peer_id      | service_name                     | outway_peer_id       | outway_name                      |
            | serviceConnection | service          | 12345678901234567891 | basisregister-fictieve-kentekens | 12345678901234567890 | gemeente-stijns-fsc-nlx-outway   |
        When Outway "gemeente-stijns-nlx-outway" of "Gemeente Stijns" makes a request with the ServiceConnectionGrant from the Contract expecting a status code 400
        Then "Gemeente Stijns" receives a response with status code 400

    @core
    Scenario: Integration-ConsumeServiceAsDelegatee-1
        Consume a Service with a DelegatedServiceConnectionGrant

        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And "Vergunningsoftware BV" is up and running
        And "RvRD" created the Service "basisregister-fictieve-kentekens"
        And a Contract with the following grants is created by "Vergunningsoftware BV":
            | grant_type                 | service_type     | service_peer_id      | service_name                     | outway_delegator_peer_id | outway_peer_id        | outway_name                            |
            | delegatedServiceConnection | service          | 12345678901234567891 | basisregister-fictieve-kentekens | 12345678901234567890      | 12345678901234567892 | vergunningsoftware-bv-fsc-nlx-outway   |
        And "RvRD" accepts the Contract
        And "Gemeente Stijns" accepts the Contract
        And "Vergunningsoftware BV" verifies all Peers have Accepted the Contract
        When Outway "vergunningsoftware-bv-nlx-outway" of "Vergunningsoftware BV" makes a request with the DelegatedServiceConnectionGrant from the Contract expecting a status code 200
        Then "Vergunningsoftware BV" receives a response with status code 200

    @core
    Scenario: Integration-ConsumeDelegatedService-1
        Consume a Service with a ServiceConnectionGrant containing a Delegator

        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And "Gemeente Riemer" is up and running
        And "RvRD" created the Service "basisregister-fictieve-kentekens"
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type        | service_type     | service_delegator_peer_id | service_peer_id      | service_name                     | outway_peer_id       | outway_name                      |
            | serviceConnection | delegatedService | 12345678901234567893      | 12345678901234567891 | basisregister-fictieve-kentekens | 12345678901234567890 | gemeente-stijns-fsc-nlx-outway   |
        And "RvRD" accepts the Contract
        And "Gemeente Riemer" accepts the Contract
        And "Gemeente Stijns" verifies all Peers have Accepted the Contract
        When Outway "gemeente-stijns-nlx-outway" of "Gemeente Stijns" makes a request with the ServiceConnectionGrant from the Contract expecting a status code 200
        Then "Gemeente Stijns" receives a response with status code 200

    @core
    Scenario: Integration-ConsumeDelegatedService-2
        Consume a Service with a DelegatedServiceConnectionGrant containing a Delegator

        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And "Vergunningsoftware BV" is up and running
        And "Gemeente Riemer" is up and running
        And "RvRD" created the Service "basisregister-fictieve-kentekens"
        And a Contract with the following grants is created by "Vergunningsoftware BV":
            | grant_type                 | service_type     | service_delegator_peer_id | service_peer_id      | service_name                     | outway_delegator_peer_id | outway_peer_id       | outway_name                          |
            | delegatedServiceConnection | delegatedService | 12345678901234567893      | 12345678901234567891 | basisregister-fictieve-kentekens | 12345678901234567890     | 12345678901234567892 | vergunningsoftware-bv-fsc-nlx-outway |
        And "RvRD" accepts the Contract
        And "Gemeente Riemer" accepts the Contract
        And "Gemeente Stijns" accepts the Contract
        And "Vergunningsoftware BV" verifies all Peers have Accepted the Contract
        When Outway "vergunningsoftware-bv-nlx-outway" of "Vergunningsoftware BV" makes a request with the DelegatedServiceConnectionGrant from the Contract expecting a status code 200
        Then "Vergunningsoftware BV" receives a response with status code 200

    @logging
    Scenario: Integration-TransactionLog-1
        Create TransactionLog records

        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And "RvRD" created the Service "basisregister-fictieve-kentekens"
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type        | service_type     | service_peer_id      | service_name                     | outway_peer_id       | outway_name                      |
            | serviceConnection | service          | 12345678901234567891 | basisregister-fictieve-kentekens | 12345678901234567890 | gemeente-stijns-fsc-nlx-outway   |
        And "RvRD" accepts the Contract
        When Outway "gemeente-stijns-nlx-outway" of "Gemeente Stijns" makes a request with the ServiceConnectionGrant from the Contract expecting a status code 200
        Then "Gemeente Stijns" has a transaction record
        And "RvRD" has the same transaction record as "Gemeente Stijns"

    @auto-sign
    Scenario: Auto-sign Contract
        Vergunningsoftware BV should auto-sign Contracts from Gemeente Stijns

        Given "Gemeente Stijns" is up and running
        And "Vergunningsoftware BV" is up and running
        And a Contract with the following grants is created by "Gemeente Stijns":
            | grant_type        | service_type     | service_peer_id      | service_name                     | outway_peer_id       | outway_name                      |
            | serviceConnection | service          | 12345678901234567892 | basisregister-fictieve-kentekens | 12345678901234567890 | gemeente-stijns-fsc-nlx-outway   |
        When "Gemeente Stijns" retrieves the Contract
        Then all Peers have accepted the Contract

    @core
    Scenario: Mark a Peer as a Directory
        Given "Gemeente Stijns" is up and running
        And "Vergunningsoftware BV" is up and running
        When "Gemeente Stijns" marks "Vergunningsoftware BV" as Directory
        Then "Vergunningsoftware BV" has the role Directory for "Gemeente Stijns"
