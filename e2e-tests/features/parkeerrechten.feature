Feature: Parkeerrechten

    @parkeerrechten @only-run-in-ci
    Scenario: Retrieving Parkeerrechten
        Given "Gemeente Stijns" is up and running
        And "RvRD" is up and running
        And "Vergunningsoftware BV" is up and running
        And "RvRD" created the Service "basisregister-fictieve-kentekens" with endpoint URL "http://rvrd-basisregister-fictieve-kentekens"
        And "Gemeente Stijns" created the Service "parkeerrechten" with endpoint URL "http://gemeente-stijns-parkeerrechten-api:8000"
        And a Contract with the following grants is created by "Vergunningsoftware BV":
            | grant_type        | service_type     | service_peer_id      | service_name                     | outway_peer_id       | outway_name                      |
            | serviceConnection | service          | 12345678901234567891 | basisregister-fictieve-kentekens | 12345678901234567892 | vergunningsoftware-bv-fsc-nlx-outway   |
            | serviceConnection | service          | 12345678901234567890 | parkeerrechten                   | 12345678901234567892 | vergunningsoftware-bv-fsc-nlx-outway   |
        And "RvRD" accepts the Contract
        And "Gemeente Stijns" accepts the Contract
        And "Vergunningsoftware BV" verifies all Peers have Accepted the Contract
        And a kenteken is added to the parkeerrechten
        When I retrieve the parkeerrechten
        Then I see the parkeerrechten
