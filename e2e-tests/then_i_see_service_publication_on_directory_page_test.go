// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"context"
	"errors"
	"fmt"

	"github.com/cucumber/godog"
)

func thenISeeServicePublicationOnDirectoryPage(ctx context.Context) error {
	t := godog.T(ctx)

	t.Log("then I see the service publication on the directory page")

	testConfig := testConfigFromCtx(ctx)

	serviceNameFoundOnPage := false

	var err error

	filePath := fmt.Sprintf("./screenshots/then-i-see-service-publication-directory-page-%s.png", testConfig.ServiceName)
	testConfig.LastPage.MustScreenshotFullPage(filePath)

	t.Logf("Then I see the service publication on the directory page with service: %s/%s", testConfig.Organizations.Get(GemeenteStijns).Info.OIN, testConfig.ServiceName)

	serviceNameFoundOnPage, _, err = testConfig.LastPage.Has(fmt.Sprintf(`a[href^="/directory/%s/%s"]`, testConfig.Organizations.Get(GemeenteStijns).Info.OIN, testConfig.ServiceName))
	if err != nil {
		return err
	}

	if serviceNameFoundOnPage {
		return errors.New("the service was not found on the directory page")
	}

	return nil
}
