// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build e2e

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	controllerapi "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/administration/rest/api"
	managerinternalapi "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

const GemeenteStijns = "Gemeente Stijns"
const RvRD = "RvRD"
const VergunningsoftwareBV = "Vergunningsoftware BV"
const GemeenteRiemer = "Gemeente Riemer"

type Organizations map[string]*Organization

type Organization struct {
	Info  *OrganizationInfo
	State *OrganizationState
}

type OrganizationInfos map[string]*OrganizationInfo

type OrganizationInfo struct {
	OIN                          string
	AuthenticationRequired       bool
	DefaultOutwayURL             string
	ControllerUIURL              string
	ControllerUIUsername         string
	ControllerUIPassword         string
	ControllerRESTAPIURL         string
	ManagerInternalRESTAPIURL    string
	ManagerExternalRESTAPIURL    string
	CertBundleControllerInternal *tls.CertificateBundle
	CertBundleManagerInternal    *tls.CertificateBundle
}

func (o Organizations) Get(orgName string) *Organization {
	s, ok := o[orgName]
	if !ok {
		log.Fatalf("could not get org, unknown organization name: %q", orgName)
	}

	return s
}

func (o Organizations) GetByPeerID(orgPeerID string) *Organization {
	for _, org := range o {
		if org.Info.OIN != orgPeerID {
			continue
		}

		return org
	}

	log.Fatalf("could not find organization config with PeerID %q", orgPeerID)

	return nil
}

//nolint:dupl // this is not a duplicate
func (o *Organization) ManagerInternal() *managerinternalapi.ClientWithResponses {
	if o.State.managerInternalClient == nil {
		var err error

		o.State.managerInternalClient, err = managerinternalapi.NewClientWithResponses(o.Info.ManagerInternalRESTAPIURL, func(c *managerinternalapi.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = o.Info.CertBundleManagerInternal.TLSConfig()

			c.Client = &http.Client{Transport: t}

			return nil
		})
		if err != nil {
			log.Fatalf("could not create manager client: %v", err)
		}
	}

	return o.State.managerInternalClient
}

//nolint:dupl // this is not a duplicate
func (o *Organization) Controller() *controllerapi.ClientWithResponses {
	if o.State.controllerClient == nil {
		var err error

		o.State.controllerClient, err = controllerapi.NewClientWithResponses(o.Info.ControllerRESTAPIURL, func(c *controllerapi.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = o.Info.CertBundleControllerInternal.TLSConfig()

			c.Client = &http.Client{Transport: t}

			return nil
		})
		if err != nil {
			log.Fatalf("could not create controller client: %v", err)
		}
	}

	return o.State.controllerClient
}

type OrganizationState struct {
	IsLoggedIn            bool
	LatestRequestResponse *http.Response
	TxLogID               string
	managerInternalClient *managerinternalapi.ClientWithResponses
	controllerClient      *controllerapi.ClientWithResponses
}

func getOrganizations(orgInfos OrganizationInfos) Organizations {
	states := make(Organizations, len(orgInfos))

	for o := range orgInfos {
		states[o] = &Organization{
			Info:  orgInfos[o],
			State: &OrganizationState{},
		}
	}

	return states
}

func getOrganizationInfos() (OrganizationInfos, error) {
	orgs := OrganizationInfos{}

	orgACertBundleControllerInternal, err := tls.NewBundleFromFiles(
		getEnvString(os.Getenv("E2E_GEMEENTE_STIJNS_CONTROLLER_INTERNAL_CERT_PATH"), "../pki/internal/organization-a/certs/controller-api/cert.pem"),
		getEnvString(os.Getenv("E2E_GEMEENTE_STIJNS_CONTROLLER_INTERNAL_CERT_KEY_PATH"), "../pki/internal/organization-a/certs/controller-api/key.pem"),
		getEnvString(os.Getenv("E2E_GEMEENTE_STIJNS_CONTROLLER_INTERNAL_CERT_ROOT_CA_PATH"), "../pki/internal/organization-a/ca/root-1.pem"),
	)
	if err != nil {
		return nil, fmt.Errorf("error loading tls bundle for gemeente stijns: %v", err)
	}

	orgACertBundleManagerInternal, err := tls.NewBundleFromFiles(
		getEnvString(os.Getenv("E2E_GEMEENTE_STIJNS_MANAGER_INTERNAL_CERT_PATH"), "../pki/internal/organization-a/certs/controller-api/cert.pem"),
		getEnvString(os.Getenv("E2E_GEMEENTE_STIJNS_MANAGER_INTERNAL_CERT_KEY_PATH"), "../pki/internal/organization-a/certs/controller-api/key.pem"),
		getEnvString(os.Getenv("E2E_GEMEENTE_STIJNS_MANAGER_INTERNAL_CERT_ROOT_CA_PATH"), "../pki/internal/organization-a/ca/root-1.pem"),
	)
	if err != nil {
		return nil, fmt.Errorf("error loading tls bundle for gemeente stijns: %v", err)
	}

	orgs[GemeenteStijns] = &OrganizationInfo{
		OIN:                          getEnvString("E2E_GEMEENTE_STIJNS_PEER_ID", "12345678901234567890"),
		AuthenticationRequired:       getEnvBool("E2E_GEMEENTE_STIJNS_CONTROLLER_UI_AUTHENTICATION_REQUIRED", true),
		DefaultOutwayURL:             getEnvString("E2E_GEMEENTE_STIJNS_DEFAULT_OUTWAY_URL", "http://localhost:7605"),
		ControllerUIURL:              getEnvString("E2E_GEMEENTE_STIJNS_CONTROLLER_UI_URL", "http://controller.organization-a.nlx.localhost"),
		ControllerUIUsername:         getEnvString("E2E_GEMEENTE_STIJNS_CONTROLLER_UI_USERNAME", "admin"),
		ControllerUIPassword:         getEnvString("E2E_GEMEENTE_STIJNS_CONTROLLER_UI_PASSWORD", "password"),
		ControllerRESTAPIURL:         getEnvString("E2E_GEMEENTE_STIJNS_CONTROLLER_REST_API_URL", "https://controller-api.organization-a.nlx.localhost:7615"),
		ManagerInternalRESTAPIURL:    getEnvString("E2E_GEMEENTE_STIJNS_MANAGER_INTERNAL_REST_API_URL", "https://manager.organization-a.nlx.localhost:443"),
		ManagerExternalRESTAPIURL:    getEnvString("E2E_GEMEENTE_STIJNS_MANAGER_EXTERNAL_REST_API_URL", "https://manager.organization-a.nlx.localhost:8443"),
		CertBundleControllerInternal: orgACertBundleControllerInternal,
		CertBundleManagerInternal:    orgACertBundleManagerInternal,
	}

	orgBCertBundleControllerInternal, err := tls.NewBundleFromFiles(
		getEnvString(os.Getenv("E2E_RVRD_CONTROLLER_INTERNAL_CERT_PATH"), "../pki/internal/organization-b/certs/controller-api/cert.pem"),
		getEnvString(os.Getenv("E2E_RVRD_CONTROLLER_INTERNAL_CERT_KEY_PATH"), "../pki/internal/organization-b/certs/controller-api/key.pem"),
		getEnvString(os.Getenv("E2E_RVRD_CONTROLLER_INTERNAL_CERT_ROOT_CA_PATH"), "../pki/internal/organization-b/ca/root-1.pem"),
	)
	if err != nil {
		return nil, fmt.Errorf("error loading tls bundle for rvrd: %v", err)
	}

	orgBCertBundleManagerInternal, err := tls.NewBundleFromFiles(
		getEnvString(os.Getenv("E2E_RVRD_MANAGER_INTERNAL_CERT_PATH"), "../pki/internal/organization-b/certs/controller-api/cert.pem"),
		getEnvString(os.Getenv("E2E_RVRD_MANAGER_INTERNAL_CERT_KEY_PATH"), "../pki/internal/organization-b/certs/controller-api/key.pem"),
		getEnvString(os.Getenv("E2E_RVRD_MANAGER_INTERNAL_CERT_ROOT_CA_PATH"), "../pki/internal/organization-b/ca/root-1.pem"),
	)
	if err != nil {
		return nil, fmt.Errorf("error loading tls bundle for rvrd: %v", err)
	}

	orgs[RvRD] = &OrganizationInfo{
		OIN:                          getEnvString("E2E_RVRD_PEER_ID", "12345678901234567891"),
		AuthenticationRequired:       getEnvBool("E2E_RVRD_CONTROLLER_UI_AUTHENTICATION_REQUIRED", false),
		DefaultOutwayURL:             getEnvString("E2E_RVRD_DEFAULT_OUTWAY_URL", ""),
		ControllerUIURL:              getEnvString("E2E_RVRD_CONTROLLER_UI_URL", "http://controller.organization-b.nlx.localhost"),
		ControllerUIUsername:         getEnvString("E2E_RVRD_CONTROLLER_UI_USERNAME", ""),
		ControllerUIPassword:         getEnvString("E2E_RVRD_CONTROLLER_UI_PASSWORD", ""),
		ControllerRESTAPIURL:         getEnvString("E2E_RVRD_CONTROLLER_REST_API_URL", "https://controller-api.organization-b.nlx.localhost:7710"),
		ManagerInternalRESTAPIURL:    getEnvString("E2E_RVRD_MANAGER_INTERNAL_REST_API_URL", "https://manager.organization-b.nlx.localhost:443"),
		ManagerExternalRESTAPIURL:    getEnvString("E2E_RVRD_MANAGER_EXTERNAL_REST_API_URL", "https://manager.organization-b.nlx.localhost:8443"),
		CertBundleControllerInternal: orgBCertBundleControllerInternal,
		CertBundleManagerInternal:    orgBCertBundleManagerInternal,
	}

	orgCCertBundleControllerInternal, err := tls.NewBundleFromFiles(
		getEnvString(os.Getenv("E2E_VERGUNNINGSOFTWARE_BV_CONTROLLER_INTERNAL_CERT_PATH"), "../pki/internal/organization-c/certs/controller-api/cert.pem"),
		getEnvString(os.Getenv("E2E_VERGUNNINGSOFTWARE_BV_CONTROLLER_INTERNAL_CERT_KEY_PATH"), "../pki/internal/organization-c/certs/controller-api/key.pem"),
		getEnvString(os.Getenv("E2E_VERGUNNINGSOFTWARE_BV_CONTROLLER_INTERNAL_CERT_ROOT_CA_PATH"), "../pki/internal/organization-c/ca/root-1.pem"),
	)
	if err != nil {
		return nil, fmt.Errorf("error loading tls bundle for vergunningsoftware bv: %v", err)
	}

	orgCCertBundleManagerInternal, err := tls.NewBundleFromFiles(
		getEnvString(os.Getenv("E2E_VERGUNNINGSOFTWARE_BV_MANAGER_INTERNAL_CERT_PATH"), "../pki/internal/organization-c/certs/controller-api/cert.pem"),
		getEnvString(os.Getenv("E2E_VERGUNNINGSOFTWARE_BV_MANAGER_INTERNAL_CERT_KEY_PATH"), "../pki/internal/organization-c/certs/controller-api/key.pem"),
		getEnvString(os.Getenv("E2E_VERGUNNINGSOFTWARE_BV_MANAGER_INTERNAL_CERT_ROOT_CA_PATH"), "../pki/internal/organization-c/ca/root-1.pem"),
	)
	if err != nil {
		return nil, fmt.Errorf("error loading tls bundle for vergunningsoftware bv: %v", err)
	}

	orgs[VergunningsoftwareBV] = &OrganizationInfo{
		OIN:                          getEnvString("E2E_VERGUNNINGSOFTWARE_BV_PEER_ID", "12345678901234567892"),
		AuthenticationRequired:       getEnvBool("E2E_VERGUNNINGSOFTWARE_BV_CONTROLLER_UI_AUTHENTICATION_REQUIRED", false),
		DefaultOutwayURL:             getEnvString("E2E_VERGUNNINGSOFTWARE_BV_DEFAULT_OUTWAY_URL", "http://localhost:7802"),
		ControllerUIURL:              getEnvString("E2E_VERGUNNINGSOFTWARE_BV_CONTROLLER_UI_URL", "http://controller.organization-c.nlx.localhost"),
		ControllerUIUsername:         getEnvString("E2E_VERGUNNINGSOFTWARE_BV_CONTROLLER_UI_USERNAME", ""),
		ControllerUIPassword:         getEnvString("E2E_VERGUNNINGSOFTWARE_BV_CONTROLLER_UI_PASSWORD", ""),
		ControllerRESTAPIURL:         getEnvString("E2E_VERGUNNINGSOFTWARE_BV_CONTROLLER_REST_API_URL", "https://controller-api.organization-c.nlx.localhost:7809"),
		ManagerInternalRESTAPIURL:    getEnvString("E2E_VERGUNNINGSOFTWARE_BV_MANAGER_INTERNAL_REST_API_URL", "https://manager.organization-c.nlx.localhost:443"),
		ManagerExternalRESTAPIURL:    getEnvString("E2E_VERGUNNINGSOFTWARE_BV_MANAGER_EXTERNAL_REST_API_URL", "https://manager.organization-c.nlx.localhost:8443"),
		CertBundleControllerInternal: orgCCertBundleControllerInternal,
		CertBundleManagerInternal:    orgCCertBundleManagerInternal,
	}

	orgDCertBundleControllerInternal, err := tls.NewBundleFromFiles(
		getEnvString(os.Getenv("E2E_GEMEENTE_RIEMER_CONTROLLER_INTERNAL_CERT_PATH"), "../pki/internal/organization-d/certs/controller-api/cert.pem"),
		getEnvString(os.Getenv("E2E_GEMEENTE_RIEMER_CONTROLLER_INTERNAL_CERT_KEY_PATH"), "../pki/internal/organization-d/certs/controller-api/key.pem"),
		getEnvString(os.Getenv("E2E_GEMEENTE_RIEMER_CONTROLLER_INTERNAL_CERT_ROOT_CA_PATH"), "../pki/internal/organization-d/ca/root-1.pem"),
	)
	if err != nil {
		return nil, fmt.Errorf("error loading tls bundle for gemeente riemer: %v", err)
	}

	orgDCertBundleManagerInternal, err := tls.NewBundleFromFiles(
		getEnvString(os.Getenv("E2E_GEMEENTE_RIEMER_MANAGER_INTERNAL_CERT_PATH"), "../pki/internal/organization-d/certs/controller-api/cert.pem"),
		getEnvString(os.Getenv("E2E_GEMEENTE_RIEMER_MANAGER_INTERNAL_CERT_KEY_PATH"), "../pki/internal/organization-d/certs/controller-api/key.pem"),
		getEnvString(os.Getenv("E2E_GEMEENTE_RIEMER_MANAGER_INTERNAL_CERT_ROOT_CA_PATH"), "../pki/internal/organization-d/ca/root-1.pem"),
	)
	if err != nil {
		return nil, fmt.Errorf("error loading tls bundle for gemeente riemer: %v", err)
	}

	orgs[GemeenteRiemer] = &OrganizationInfo{
		OIN:                          getEnvString("E2E_GEMEENTE_RIEMER_PEER_ID", "12345678901234567893"),
		AuthenticationRequired:       getEnvBool("E2E_GEMEENTE_RIEMER_CONTROLLER_UI_AUTHENTICATION_REQUIRED", false),
		DefaultOutwayURL:             getEnvString("E2E_GEMEENTE_RIEMER_DEFAULT_OUTWAY_URL", ""),
		ControllerUIURL:              getEnvString("E2E_GEMEENTE_RIEMER_CONTROLLER_UI_URL", "http://controller.organization-d.nlx.localhost"),
		ControllerUIUsername:         getEnvString("E2E_GEMEENTE_RIEMER_CONTROLLER_UI_USERNAME", ""),
		ControllerUIPassword:         getEnvString("E2E_GEMEENTE_RIEMER_CONTROLLER_UI_PASSWORD", ""),
		ControllerRESTAPIURL:         getEnvString("E2E_GEMEENTE_RIEMER_CONTROLLER_REST_API_URL", "https://controller-api.organization-d.nlx.localhost:7909"),
		ManagerInternalRESTAPIURL:    getEnvString("E2E_GEMEENTE_RIEMER_MANAGER_INTERNAL_REST_API_URL", "https://manager.organization-d.nlx.localhost:443"),
		ManagerExternalRESTAPIURL:    getEnvString("E2E_GEMEENTE_RIEMER_MANAGER_EXTERNAL_REST_API_URL", "https://manager.organization-d.nlx.localhost:8443"),
		CertBundleControllerInternal: orgDCertBundleControllerInternal,
		CertBundleManagerInternal:    orgDCertBundleManagerInternal,
	}

	return orgs, nil
}

func getEnvString(key, fallback string) string {
	value, exists := os.LookupEnv(key)
	if !exists {
		value = fallback
	}

	return value
}

func getEnvBool(key string, fallback bool) bool {
	value, exists := os.LookupEnv(key)
	if !exists {
		return fallback
	}

	return value == "true"
}

func getEnvInt(key string, fallback int) int {
	return int(getEnvInt64(key, int64(fallback)))
}

func getEnvInt64(key string, fallback int64) int64 {
	value, exists := os.LookupEnv(key)
	if !exists {
		return fallback
	}

	i, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		log.Fatalf("could not parse env var to int: %q: %q", key, value)
	}

	return i
}
