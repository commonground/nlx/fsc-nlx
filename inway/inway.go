// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"regexp"
	"sync"
	"time"

	"github.com/cenkalti/backoff/v4"
	lru "github.com/hashicorp/golang-lru/v2"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/adapters/controller"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/plugins"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

var nameRegex = regexp.MustCompile(`^[a-zA-Z0-9-]{1,100}$`)

const (
	timeOut               = 30 * time.Second
	keepAlive             = 30 * time.Second
	maxIdleCons           = 100
	IdleConnTimeout       = 20 * time.Second
	TLSHandshakeTimeout   = 10 * time.Second
	ExpectContinueTimeout = 1 * time.Second
	DefaultChunkByteSize  = 1024
)

type Peer struct {
	ID   string
	Name string
}

type Inway struct {
	clock               clock.Clock
	name                string
	groupID             contract.GroupID
	peer                Peer
	selfAddress         Address
	orgCertBundle       *common_tls.CertificateBundle
	logger              *logger.Logger
	handler             http.Handler
	plugins             []plugins.Plugin
	controller          controller.Controller
	servicesLock        sync.RWMutex
	config              *config.Config
	serviceProxiesCache *lru.Cache[string, *httputil.ReverseProxy]
	crlCache            *common_tls.CRLsCache
}

func (i *Inway) Handler() http.Handler {
	return i.handler
}

func (i *Inway) RegisterToController(ctx context.Context) error {
	register := func() error {
		err := i.controller.RegisterInway(ctx, &controller.RegisterInwayArgs{
			GroupID: i.groupID.String(),
			Name:    i.name,
			Address: i.selfAddress.String(),
		})
		if err != nil {
			i.logger.Error("failed to register to controller api", err)

			return err
		}

		i.logger.Info("controller api registration successful")

		return nil
	}

	err := backoff.Retry(register, backoff.WithContext(backoff.NewExponentialBackOff(), ctx))
	if err != nil {
		i.logger.Error("permanently failed to register to controller api", err)

		return err
	}

	return nil
}

type NewInwayArgs struct {
	Context                      context.Context
	Logger                       *logger.Logger
	Txlogger                     transactionlog.TransactionLogger
	Name                         string
	GroupID                      string
	SelfAddress                  Address
	OrgCertBundle                *common_tls.CertificateBundle
	Controller                   controller.Controller
	AuthServiceURL               string
	AuthCAPath                   string
	AuthWithBody                 bool
	AuthMaxBodySize              int
	AuthBodyChunkSize            int
	ConfigRepository             config.Repository
	ServiceEndpointCacheDuration time.Duration
	Clock                        clock.Clock
	ServiceProxyCacheSize        int
	CRL                          *common_tls.CRLsCache
	TxLogMetadataHeaders         []string
}

// nolint:funlen,gocyclo // unagle to reduce length
func NewInway(args *NewInwayArgs) (*Inway, error) {
	lgr := args.Logger

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	groupID, err := contract.NewGroupID(args.GroupID)
	if err != nil {
		return nil, fmt.Errorf("invalid groupID in args: %w", err)
	}

	if !nameRegex.MatchString(args.Name) {
		return nil, errors.New("a valid name is required (alphanumeric & dashes, max. 100 characters)")
	}

	orgCert := args.OrgCertBundle.Certificate()

	if len(orgCert.Subject.Organization) != 1 {
		return nil, errors.New("cannot obtain organization name from self cert")
	}

	err = addressIsInOrgCert(args.SelfAddress.URL(), orgCert)
	if err != nil {
		return nil, err
	}

	if args.Context == nil {
		return nil, errors.New("context is nil. needed to close gracefully")
	}

	cfg, err := config.New(&config.NewConfigArgs{
		Repo:          args.ConfigRepository,
		CacheDuration: args.ServiceEndpointCacheDuration,
		Clock:         args.Clock,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create config")
	}

	peerName := orgCert.Subject.Organization[0]
	peerID := orgCert.Subject.SerialNumber

	lgr.Info(fmt.Sprintf("loaded certificates for inway. peer-id: %s, peer-name: %s ", peerID, peerName))

	cacheSize := args.ServiceProxyCacheSize

	if cacheSize == 0 {
		cacheSize = 1024
	}

	lruCache, err := lru.New[string, *httputil.ReverseProxy](cacheSize)
	if err != nil {
		return nil, errors.Wrap(err, "could not create service proxies cache")
	}

	i := &Inway{
		logger:  lgr,
		groupID: groupID,
		peer: Peer{
			ID:   peerID,
			Name: peerName,
		},
		selfAddress:   args.SelfAddress,
		orgCertBundle: args.OrgCertBundle,
		controller:    args.Controller,
		servicesLock:  sync.RWMutex{},
		plugins: []plugins.Plugin{
			plugins.NewAuthenticationPlugin(),
		},
		config:              cfg,
		clock:               args.Clock,
		serviceProxiesCache: lruCache,
		crlCache:            args.CRL,
	}

	authorizationPlugin, err := configureAuthorizationPlugin(args.Clock, args.OrgCertBundle, args.AuthCAPath, args.AuthServiceURL, args.AuthWithBody, args.AuthMaxBodySize, args.AuthBodyChunkSize)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("could not configure authorization plugin: %s", err))
	}

	i.plugins = append(i.plugins,
		plugins.NewLogRecordPlugin(plugins.NewLogRecordPluginArgs{
			SelfPeerID:     peerID,
			TxLogger:       args.Txlogger,
			MetadataFields: plugins.LogRecordMetadata{Headers: args.TxLogMetadataHeaders},
		}),
	)

	if authorizationPlugin != nil {
		i.plugins = append(i.plugins, authorizationPlugin)
	}

	crlPlugin, err := plugins.NewCrlPlugin(i.crlCache)
	if err != nil {
		return nil, err
	}

	if crlPlugin != nil {
		i.plugins = append(i.plugins, crlPlugin)
	}

	if args.Name != "" {
		i.name = args.Name
	} else {
		i.name = common_tls.X509CertificateThumbprint(orgCert)
	}

	serveMux := http.NewServeMux()
	serveMux.Handle("/.nlx/", http.NotFoundHandler())
	serveMux.HandleFunc("/", i.handleProxyRequest)
	serveMux.HandleFunc("/health/live", func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	i.handler = serveMux

	return i, nil
}

func addressIsInOrgCert(address *url.URL, orgCert *x509.Certificate) error {
	hostname := address.Hostname()

	if hostname == orgCert.Subject.CommonName {
		return nil
	}

	for _, dnsName := range orgCert.DNSNames {
		if hostname == dnsName {
			return nil
		}
	}

	return errors.Errorf("'%s' is not in the list of DNS names of the certificate, %v", hostname, orgCert.DNSNames)
}

func configureAuthorizationPlugin(c clock.Clock, externalCert *common_tls.CertificateBundle, authCAPath, authServiceURL string, withBody bool, maxBodyBytes, chunkSize int) (*plugins.AuthorizationPlugin, error) {
	if authServiceURL == "" {
		return plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
			ServiceURL:          "",
			AuthorizationClient: nil,
			AuthServerEnabled:   false,
			ExternalCert:        externalCert,
			Clock:               c,
		})
	}

	if authCAPath == "" {
		return nil, fmt.Errorf("authorization service URL set but no CA for authorization provided")
	}

	authURL, err := url.Parse(authServiceURL)
	if err != nil {
		return nil, err
	}

	if authURL.Scheme != "https" {
		return nil, errors.New("scheme of authorization service URL is not 'https'")
	}

	ca, err := common_tls.NewCertPoolFromFile(authCAPath)
	if err != nil {
		return nil, err
	}

	tlsConfig := common_tls.NewConfig(common_tls.WithTLS12())
	tlsConfig.RootCAs = ca

	if chunkSize == 0 {
		chunkSize = DefaultChunkByteSize
	}

	if maxBodyBytes == 0 {
		maxBodyBytes = DefaultChunkByteSize * DefaultChunkByteSize
	}

	return plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
		AuthorizationClient: &http.Client{
			Transport: createHTTPTransport(tlsConfig),
		},
		ServiceURL:        authURL.String(),
		AuthServerEnabled: true,
		ExternalCert:      externalCert,
		Clock:             c,
		WithBody:          withBody,
		MaxBodyBytes:      maxBodyBytes,
		ChunkSize:         chunkSize,
	})
}

func createHTTPTransport(tlsConfig *tls.Config) *http.Transport {
	return &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			a := &net.Dialer{
				Timeout:   timeOut,
				KeepAlive: keepAlive,
			}

			return a.DialContext(ctx, network, addr)
		},
		MaxIdleConns:          maxIdleCons,
		IdleConnTimeout:       IdleConnTimeout,
		TLSHandshakeTimeout:   TLSHandshakeTimeout,
		ExpectContinueTimeout: ExpectContinueTimeout,
		TLSClientConfig:       tlsConfig,
	}
}

func newRoundTripHTTPTransport() *http.Transport {
	return &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   timeOut,
			KeepAlive: timeOut,
		}).DialContext,
		MaxIdleConns:          maxIdleCons,
		MaxIdleConnsPerHost:   maxIdleCons,
		IdleConnTimeout:       IdleConnTimeout,
		TLSHandshakeTimeout:   TLSHandshakeTimeout,
		ExpectContinueTimeout: 1 * time.Second,
		ForceAttemptHTTP2:     true,
	}
}
