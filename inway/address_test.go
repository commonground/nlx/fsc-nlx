// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package inway_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/inway"
)

func TestNewAddress(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		address       string
		expectedError string
	}{
		"happy_flow": {
			address: "https://localhost:443",
		},
		"missing_scheme": {
			address:       "localhost:443",
			expectedError: "missing https prefix \"localhost:443\"",
		},
		"invalid_port": {
			address:       "https://localhost:444",
			expectedError: "invalid port in \"https://localhost:444\". Must be 443",
		},
		"missing_port": {
			address:       "https://invalid.loc@lhost:4h3",
			expectedError: "unable to parse inway address \"https://invalid.loc@lhost:4h3\" as URL\nparse \"https://invalid.loc@lhost:4h3\": invalid port \":4h3\" after host",
		},
		"empty_string": {
			address:       "",
			expectedError: "address is required",
		},
	}

	for _, tc := range testCases {
		a, err := inway.NewAddress(tc.address)

		if tc.expectedError != "" {
			assert.EqualError(t, err, tc.expectedError)
		} else {
			assert.Equal(t, tc.address, a.String())
		}
	}
}
