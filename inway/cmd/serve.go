// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package cmd

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/cmd"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logoptions"
	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	"gitlab.com/commonground/nlx/fsc-nlx/common/process"
	"gitlab.com/commonground/nlx/fsc-nlx/common/slogerrorwriter"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/version"
	controllerapi "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/inway"
	restconfig "gitlab.com/commonground/nlx/fsc-nlx/inway/adapters/config/rest"
	restcontroller "gitlab.com/commonground/nlx/fsc-nlx/inway/adapters/controller/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/pkg/health"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/intunauthenticated/rest/api/server"
	txlogapi "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

const readHeaderTimeout = time.Second * 60
const defaultProxyCacheSize = 1024

var serveOpts struct {
	ListenAddress               string
	Address                     string
	ControllerAPIAddress        string
	MonitoringAddress           string
	TxLogAPIAddress             string
	Name                        string
	GroupID                     string
	AuthorizationServiceAddress string
	AuthorizationCA             string
	AuthorizationWithBody       bool
	AuthorizationMaxBodySize    int
	AuthorizationChunkSize      int
	ManagerInternalAddress      string
	ConfigExpiresAfter          string
	ServiceProxyCacheSize       int
	CRLURLs                     []string
	TxLogMetadataHeaders        []string

	logoptions.LogOptions
	cmd.TLSGroupOptions
	cmd.TLSOptions
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddress, "listen-address", "", "127.0.0.1:8443", "Address for the inway to listen on.")
	serveCommand.Flags().StringVarP(&serveOpts.Address, "self-address", "", "", "The address that outways use to reach me. The address must contain the scheme https and port 443. E.g. https://my-inway.com:443")
	serveCommand.Flags().StringVarP(&serveOpts.ControllerAPIAddress, "controller-api-address", "", "", "The Controller API address")
	serveCommand.Flags().StringVarP(&serveOpts.MonitoringAddress, "monitoring-address", "", "127.0.0.1:8081", "Address for the inway monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.TxLogAPIAddress, "tx-log-api-address", "", "", "Address of the Transaction Log API")
	serveCommand.Flags().StringVarP(&serveOpts.Name, "name", "", "", "Name of the inway. Every inway should have a unique name within the organization.")
	serveCommand.Flags().StringVarP(&serveOpts.GroupID, "group-id", "", "", "Group ID of the FSC Group")
	serveCommand.Flags().StringVarP(&serveOpts.AuthorizationServiceAddress, "authorization-service-address", "", "", "Address of the authorization service. If set calls will go through the authorization service before being send to the service")
	serveCommand.Flags().StringVarP(&serveOpts.AuthorizationCA, "authorization-root-ca", "", "", "absolute path to root CA used to verify auth service certificate")
	serveCommand.Flags().BoolVarP(&serveOpts.AuthorizationWithBody, "authorization-with-body", "", false, "When set to true, the HTTP request body (if available) will be send to the Authorization Server in base64 encoded format.")
	serveCommand.Flags().IntVarP(&serveOpts.AuthorizationMaxBodySize, "authorization-max-body-size", "", 0, "The maximum HTTP request body size in bytes that is allowed for sending to the authorization server. If a body exceeds this limits, the body is not send to the Authorization Server.")
	serveCommand.Flags().IntVarP(&serveOpts.AuthorizationChunkSize, "authorization-body-chunk-size", "", 0, "The chunk size in bytes that is used to process each HTTP request body chunk.")
	serveCommand.Flags().StringVarP(&serveOpts.ManagerInternalAddress, "manager-internal-unauthenticated-address", "", "", "Internal Unauthenticated Manager address. This service provides certificates.")
	serveCommand.Flags().StringVarP(&serveOpts.ConfigExpiresAfter, "config-expires-after", "", "5s", "Time after which the config is invalided. See https://pkg.go.dev/time#ParseDuration")
	serveCommand.Flags().IntVarP(&serveOpts.ServiceProxyCacheSize, "service-proxy-cache-size", "", defaultProxyCacheSize, "The maximum size of the Service proxy cache")
	serveCommand.Flags().StringSliceVar(&serveOpts.CRLURLs, "crl-urls", []string{}, "List of URL's of Certificate Revocation Lists used to retrieve Certificate Revocation Lists")
	serveCommand.Flags().StringSliceVar(&serveOpts.TxLogMetadataHeaders, "tx-log-metadata-headers", []string{}, "List of HTTP headers to log as metadata")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "info", "Set loglevel")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupRootCert, "tls-group-root-cert", "", "", "Absolute or relative path to the NLX CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupCertFile, "tls-group-cert", "", "", "Absolute or relative path to the FSC Group cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupKeyFile, "tls-group-key", "", "", "Absolute or relative path to the FSC Group key .pem")

	if err := serveCommand.MarkFlagRequired("self-address"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("controller-api-address"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tx-log-api-address"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("name"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("group-id"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("manager-internal-unauthenticated-address"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the Inway Server",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		logger, err := zaplogger.New(serveOpts.LogLevel, serveOpts.LogType)
		if err != nil {
			panic("could not setup zap logger: " + err.Error())
		}

		logger.Info(fmt.Sprintf("version %s, source-hash %s", version.BuildVersion, version.BuildSourceHash))
		logger = logger.With("version", version.BuildVersion)

		ctx := context.Background()

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.GroupKeyFile); errValidate != nil {
			logger.Warn(fmt.Sprintf("invalid organization key permissions. file-path: %s", serveOpts.GroupKeyFile), errValidate)
		}

		groupCert, err := common_tls.NewBundleFromFiles(serveOpts.GroupCertFile, serveOpts.GroupKeyFile, serveOpts.GroupRootCert)
		if err != nil {
			logger.Fatal("loading TLS files", err)
		}

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn(fmt.Sprintf("invalid internal PKI key permissions. filepath: %s", serveOpts.KeyFile), errValidate)
		}

		internalCert, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("loading TLS files", err)
		}

		appClock := clock.New()

		txlogClient, err := txlogapi.NewClientWithResponses(serveOpts.TxLogAPIAddress, func(c *txlogapi.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = internalCert.TLSConfig()

			c.Client = &http.Client{Transport: t}
			return nil
		})
		if err != nil {
			logger.Fatal("could not create txlog client", err)
		}

		txLogger, err := transactionlog.NewRestAPITransactionLogger(&transactionlog.NewRestAPITransactionLoggerArgs{
			Logger: logger,
			Client: txlogClient,
		})
		if err != nil {
			logger.Fatal("unable to setup the transaction logger", err)
		}

		configExpiresAfter, err := time.ParseDuration(serveOpts.ConfigExpiresAfter)
		if err != nil {
			logger.Fatal("invalid Config expiration duration", err)
		}

		managerInternalClient, err := api.NewClientWithResponses(serveOpts.ManagerInternalAddress, func(c *api.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = internalCert.TLSConfig()

			c.Client = &http.Client{Transport: t}
			return nil
		})
		if err != nil {
			logger.Fatal("could not create manager internal client", err)
		}

		controllerClient, err := controllerapi.NewClientWithResponses(serveOpts.ControllerAPIAddress, func(c *controllerapi.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = internalCert.TLSConfig()

			c.Client = &http.Client{Transport: t}
			return nil
		})
		if err != nil {
			logger.Fatal("could not create rest controller client", err)
		}

		restConfigRepository, err := restconfig.New(&restconfig.NewRestConfigArgs{
			SelfAddress:      serveOpts.Address,
			ManagerClient:    managerInternalClient,
			ControllerClient: controllerClient,
			TrustedRootCert:  groupCert.RootCAs(),
			GroupID:          serveOpts.GroupID,
			Clock:            appClock,
		})
		if err != nil {
			logger.Fatal("failed to setup rest Config repository", err)
		}

		controller, err := restcontroller.New(controllerClient)
		if err != nil {
			logger.Fatal("failed to setup rest controller", err)
		}

		if !strings.HasPrefix(serveOpts.Address, "https://") {
			logger.Fatal(fmt.Sprintf("invalid self-address: %s", serveOpts.Address), errors.New("invalid self-address"))
		}

		selfAddress, err := inway.NewAddress(serveOpts.Address)
		if err != nil {
			logger.Fatal("failed to parse self address as a URL", err)
		}

		crl, err := common_tls.NewCRL(serveOpts.CRLURLs)
		if err != nil {
			logger.Fatal("cannot create CRL Cache", err)
		}

		iw, err := inway.NewInway(&inway.NewInwayArgs{
			Clock:                        appClock,
			Context:                      ctx,
			Logger:                       logger,
			Txlogger:                     txLogger,
			Controller:                   controller,
			GroupID:                      serveOpts.GroupID,
			Name:                         serveOpts.Name,
			SelfAddress:                  selfAddress,
			OrgCertBundle:                groupCert,
			AuthServiceURL:               serveOpts.AuthorizationServiceAddress,
			AuthCAPath:                   serveOpts.AuthorizationCA,
			AuthWithBody:                 serveOpts.AuthorizationWithBody,
			AuthBodyChunkSize:            serveOpts.AuthorizationChunkSize,
			AuthMaxBodySize:              serveOpts.AuthorizationMaxBodySize,
			ConfigRepository:             restConfigRepository,
			ServiceEndpointCacheDuration: configExpiresAfter,
			ServiceProxyCacheSize:        serveOpts.ServiceProxyCacheSize,
			CRL:                          crl,
			TxLogMetadataHeaders:         serveOpts.TxLogMetadataHeaders,
		})
		if err != nil {
			logger.Fatal("cannot setup inway", err)
		}

		err = iw.RegisterToController(ctx)
		if err != nil {
			logger.Fatal("failed to register with controller", err)
		}

		tlsConfig := groupCert.TLSConfig(groupCert.WithTLSClientAuth())
		serverTLS := &http.Server{
			Addr:              serveOpts.ListenAddress,
			Handler:           iw.Handler(),
			TLSConfig:         tlsConfig,
			ReadHeaderTimeout: readHeaderTimeout,
			ErrorLog:          log.New(slogerrorwriter.New(logger.Logger), "", 0),
		}

		inwayProbe := health.InwayProbe(groupCert, serveOpts.ListenAddress)
		txLogApProbe := health.TxLogAPIProbe(internalCert, serveOpts.TxLogAPIAddress)

		readinessProbes := map[string]monitoring.Check{
			"Readiness Probe Inway":     inwayProbe,
			"Readiness Probe TxLog API": txLogApProbe,
		}

		livenessProbes := map[string]monitoring.Check{
			"Liveness Probe Inway":     inwayProbe,
			"Liveness Probe TxLog API": txLogApProbe,
		}

		monitoringService, err := monitoring.NewMonitoringService(serveOpts.MonitoringAddress, logger, readinessProbes, livenessProbes)
		if err != nil {
			logger.Fatal("unable to create monitoring service", err)
		}

		go func() {
			if err := serverTLS.ListenAndServeTLS("", ""); err != nil {
				if !errors.Is(err, http.ErrServerClosed) {
					log.Fatal(err, errors.New("error listening on TLS server"))
				}
			}
		}()

		go func() {
			if err := monitoringService.Start(); err != nil {
				if !errors.Is(err, http.ErrServerClosed) {
					log.Fatal(err, errors.New("error listening on monitoring service"))
				}

				log.Fatal("cannot start monitoringService")
			}
		}()

		p.Wait()

		shutdown(logger, serverTLS, monitoringService)
	},
}

func shutdown(lgr *logger.Logger, serverTLS *http.Server, monitoringService *monitoring.Service) {
	lgr.Info("starting graceful shutdown")

	gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	err := serverTLS.Shutdown(gracefulCtx)
	if err != nil {
		lgr.Error("failed to shutdown http server", err)
	}

	if err = monitoringService.Stop(); err != nil {
		lgr.Error("failed to stop monitoringService", err)
	}
}
