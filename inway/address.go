// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package inway

import (
	"errors"
	"fmt"
	"net/url"
	"strings"
)

type Address string

func (a Address) String() string {
	return string(a)
}

func NewAddress(address string) (Address, error) {
	if address == "" {
		return "", fmt.Errorf("address is required")
	}

	if !strings.HasPrefix(address, "https://") {
		return "", fmt.Errorf("missing https prefix %q", address)
	}

	parsedURL, err := url.Parse(address)
	if err != nil {
		return "", errors.Join(fmt.Errorf("unable to parse inway address %q as URL", address), err)
	}

	port := parsedURL.Port()
	if port == "" {
		return "", fmt.Errorf("missing port in %q", address)
	}

	if port != "443" {
		return "", fmt.Errorf("invalid port in %q. Must be 443", address)
	}

	return Address(address), nil
}

func (a Address) URL() *url.URL {
	u, _ := url.Parse(a.String())

	return u
}
