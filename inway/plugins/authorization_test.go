// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins_test

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/plugins"
	common_testing "gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

//nolint:funlen // this is a test
func TestAuthorizationPlugin(t *testing.T) {
	var pkiDir = filepath.Join("..", "..", "testing", "pki")
	certBundle, err := common_testing.GetCertificateBundle(pkiDir, common_testing.NLXTestPeerA)
	require.NoError(t, err)

	leaf := base64.URLEncoding.EncodeToString(certBundle.Certificate().Raw)
	issuer := base64.URLEncoding.EncodeToString(certBundle.Cert().Certificate[1])

	tests := map[string]struct {
		authRequest                      *plugins.AuthRequest
		tokenOutwayPeerID                string
		tokenOutwayCertificateThumbprint string
		authServerEnabled                bool
		authServerResponse               interface{}
		authServerWithBody               bool
		authServerResponseStatusCode     int
		wantErr                          *httperrors.FSCNetworkError
		wantHTTPStatusCode               int
		wantAuthBodyInput                string
	}{
		"when_auth_server_returns_non_OK_status": {
			authRequest: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			tokenOutwayPeerID:                "00000000000000000001",
			tokenOutwayCertificateThumbprint: certBundle.CertificateThumbprint(),
			authServerResponse:               &plugins.AuthResponse{},
			authServerEnabled:                true,
			authServerWithBody:               false,
			authServerResponseStatusCode:     http.StatusUnauthorized,
			wantHTTPStatusCode:               http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Inway,
				Location: httperrors.IAS1,
				Code:     httperrors.ErrorWhileAuthorizingRequestErr,
				Message:  "error authorizing request: unexpected error while calling the authorization service",
			},
		},
		"when_auth_server_returns_invalid_response": {
			authRequest: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			tokenOutwayPeerID:                "00000000000000000001",
			tokenOutwayCertificateThumbprint: certBundle.CertificateThumbprint(),
			authServerResponse: struct {
				Invalid string `json:"invalid"`
			}{
				Invalid: "this is an invalid response",
			},
			authServerEnabled:            true,
			authServerWithBody:           false,
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Inway,
				Location: httperrors.IAS1,
				Code:     httperrors.UnauthorizedErr,
				Message:  "authorization server denied request: ",
			},
		},
		"when_auth_server_fails": {
			authRequest: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			tokenOutwayPeerID:                "00000000000000000001",
			tokenOutwayCertificateThumbprint: certBundle.CertificateThumbprint(),
			authServerEnabled:                true,
			authServerWithBody:               false,
			authServerResponseStatusCode:     http.StatusInternalServerError,
			wantHTTPStatusCode:               http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Inway,
				Location: httperrors.IAS1,
				Code:     httperrors.ErrorWhileAuthorizingRequestErr,
				Message:  "error authorizing request: unexpected error while calling the authorization service",
			},
		},
		"when_auth_server_returns_no_access": {
			authRequest: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			tokenOutwayPeerID:                "00000000000000000001",
			tokenOutwayCertificateThumbprint: certBundle.CertificateThumbprint(),
			authServerEnabled:                true,
			authServerWithBody:               false,
			authServerResponse: &plugins.AuthResponse{
				Result: plugins.AuthResponseData{
					Allowed: false,
					Status: plugins.AuthResponseStatus{
						Reason: "access denied",
					},
				},
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Inway,
				Location: httperrors.IAS1,
				Code:     httperrors.UnauthorizedErr,
				Message:  "authorization server denied request: access denied",
			},
		},
		"when_access_grant_not_found": {
			authRequest: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			tokenOutwayPeerID:                "00000000000000000002",
			tokenOutwayCertificateThumbprint: "mock-certificate-thumbprint",
			wantHTTPStatusCode:               http.StatusForbidden,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Inway,
				Location: httperrors.O1,
				Code:     httperrors.AccessDeniedErr,
				Message:  "permission denied, organization \"00000000000000000001\" or certificate thumbprint \"" + certBundle.CertificateThumbprint() + "\" is not allowed access.",
			},
		},
		"happy_flow_auth_server_enabled": {
			authRequest: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
					Path:   "/test",
					Method: http.MethodGet,
					InwayCertChain: []string{
						leaf,
						issuer,
					},
					OutwayCertChain: []string{},
				},
			},
			tokenOutwayPeerID:                "00000000000000000001",
			tokenOutwayCertificateThumbprint: certBundle.CertificateThumbprint(),
			authServerEnabled:                true,
			authServerWithBody:               false,
			authServerResponse: &plugins.AuthResponse{
				Result: plugins.AuthResponseData{
					Allowed: true,
				},
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusOK,
		},
		"happy_flow_auth_server_disabled": {
			authRequest: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			tokenOutwayPeerID:                "00000000000000000001",
			tokenOutwayCertificateThumbprint: certBundle.CertificateThumbprint(),
			authServerEnabled:                false,
			authServerWithBody:               false,
			wantHTTPStatusCode:               http.StatusOK,
		},
		"happy_flow_auth_server_enabled_with_body": {
			authRequest: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
					Path:   "/test",
					Method: http.MethodPost,
					InwayCertChain: []string{
						leaf,
						issuer,
					},
					OutwayCertChain: []string{},
					Body:            "This is a test",
				},
			},
			tokenOutwayPeerID:                "00000000000000000001",
			tokenOutwayCertificateThumbprint: certBundle.CertificateThumbprint(),
			authServerEnabled:                true,
			authServerWithBody:               true,
			authServerResponse: &plugins.AuthResponse{
				Result: plugins.AuthResponseData{
					Allowed: true,
				},
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusOK,
			wantAuthBodyInput:            "VGhpcyBpcyBhIHRlc3Q=",
		},
		"happy_flow_auth_server_enabled_with_body_exceeding_max": {
			authRequest: &plugins.AuthRequest{
				Input: &plugins.AuthRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
					Path:   "/test",
					Method: http.MethodPost,
					InwayCertChain: []string{
						leaf,
						issuer,
					},
					OutwayCertChain: []string{},
					Body:            "This is a test exceeding the maximum of bytes",
				},
			},
			tokenOutwayPeerID:                "00000000000000000001",
			tokenOutwayCertificateThumbprint: certBundle.CertificateThumbprint(),
			authServerEnabled:                true,
			authServerWithBody:               true,
			authServerResponse: &plugins.AuthResponse{
				Result: plugins.AuthResponseData{
					Allowed: true,
				},
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Inway,
				Location: httperrors.IAS1,
				Code:     httperrors.ErrorWhileAuthorizingRequestErr,
				Message:  "error authorizing request: HTTP request body exceeded maximum number of bytes",
			},
			wantAuthBodyInput: "",
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			context := FakeContext(certBundle, &accesstoken.DecodedToken{
				OutwayPeerID:                tc.tokenOutwayPeerID,
				OutwayCertificateThumbprint: tc.tokenOutwayCertificateThumbprint,
			})

			for k, values := range tc.authRequest.Input.Headers {
				for _, v := range values {
					context.Request.Header.Add(k, v)
				}
			}

			if tc.authServerWithBody {
				buf := bytes.Buffer{}
				buf.WriteString(tc.authRequest.Input.Body)
				context.Request.Body = io.NopCloser(&buf)

				context.Request.Method = tc.authRequest.Input.Method
			}

			var gotAuthorizationServerRequest []byte

			server := httptest.NewServer(
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					body := r.Body
					defer r.Body.Close()

					gotAuthorizationServerRequest, err = io.ReadAll(body)
					assert.NoError(t, err)

					var authInput plugins.AuthRequest
					err = json.Unmarshal(gotAuthorizationServerRequest, &authInput)
					assert.NoError(t, err)

					if tc.wantAuthBodyInput != "" {
						assert.Equal(t, tc.wantAuthBodyInput, authInput.Input.Body)
					}

					w.WriteHeader(tc.authServerResponseStatusCode)

					var b []byte

					b, err = json.Marshal(tc.authServerResponse)
					assert.NoError(t, err)

					_, err = w.Write(b)
					assert.NoError(t, err)
				}),
			)

			clck := clock.New()

			plugin, err := plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
				AuthorizationClient: http.DefaultClient,
				ServiceURL:          server.URL,
				AuthServerEnabled:   tc.authServerEnabled,
				WithBody:            tc.authServerWithBody,
				ChunkSize:           1024,
				MaxBodyBytes:        15,
				ExternalCert:        certBundle,
				Clock:               clck,
			})
			require.NoError(t, err)

			err = plugin.Serve(NopServeFunc)(context)
			require.NoError(t, err)

			response := context.Response.(*httptest.ResponseRecorder).Result()

			defer response.Body.Close()

			contents, err := io.ReadAll(response.Body)
			assert.NoError(t, err)

			assert.Equal(t, tc.wantHTTPStatusCode, response.StatusCode)

			if tc.wantErr != nil {
				gotError := &httperrors.FSCNetworkError{}
				err = json.Unmarshal(contents, gotError)
				assert.NoError(t, err)

				assert.Equal(t, tc.wantErr, gotError)
			} else if tc.authServerEnabled {
				if tc.authServerWithBody && len(tc.authRequest.Input.Body) < 15 {
					tc.authRequest.Input.Body = base64.StdEncoding.EncodeToString([]byte(tc.authRequest.Input.Body))
				}

				var wantAuthorizationServiceRequest []byte

				wantAuthorizationServiceRequest, err = json.Marshal(tc.authRequest)
				assert.NoError(t, err)

				assert.Equal(t, wantAuthorizationServiceRequest, gotAuthorizationServerRequest)
			}
		})
	}
}
