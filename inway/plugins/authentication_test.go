// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins_test

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/plugins"
	common_testing "gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestAuthenticationPlugin(t *testing.T) {
	authenticationPlugin := plugins.NewAuthenticationPlugin()

	var pkiDir = filepath.Join("..", "..", "testing", "pki")

	tests := map[string]struct {
		certifcate         *tls.CertificateBundle
		wantHTTPStatusCode int
		wantErr            *httperrors.FSCNetworkError
	}{
		"invalid_certificate": {
			certifcate: func() *tls.CertificateBundle {
				cert, err := common_testing.GetCertificateBundle(pkiDir, common_testing.OrgWithoutName)
				require.NoError(t, err)

				return cert
			}(),
			wantHTTPStatusCode: http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Inway,
				Location: httperrors.O1,
				Code:     httperrors.InvalidCertificateErr,
				Message:  "invalid certificate provided: missing organizations attribute in subject",
			},
		},
		"invalid_certificate_without_serial_number": {
			certifcate: func() *tls.CertificateBundle {
				cert, err := common_testing.GetCertificateBundle(pkiDir, common_testing.OrgWithoutSerialNumber)
				require.NoError(t, err)

				return cert
			}(),
			wantHTTPStatusCode: http.StatusUnauthorized,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Inway,
				Location: httperrors.O1,
				Code:     httperrors.InvalidCertificateErr,
				Message:  "invalid certificate provided: missing or invalid value for serial number in subject",
			},
		},
		"happy_flow": {
			certifcate: func() *tls.CertificateBundle {
				cert, err := common_testing.GetCertificateBundle(pkiDir, common_testing.NLXTestPeerA)
				require.NoError(t, err)

				return cert
			}(),
			wantHTTPStatusCode: http.StatusOK,
		},
	}

	for name, tt := range tests {
		tc := tt

		t.Run(name, func(t *testing.T) {
			context := FakeContext(tc.certifcate, &accesstoken.DecodedToken{})

			err := authenticationPlugin.Serve(NopServeFunc)(context)
			assert.NoError(t, err)

			response := context.Response.(*httptest.ResponseRecorder).Result()
			defer response.Body.Close()

			contents, err := io.ReadAll(response.Body)
			assert.NoError(t, err)

			assert.Equal(t, tc.wantHTTPStatusCode, response.StatusCode)

			if tc.wantErr != nil {
				gotError := &httperrors.FSCNetworkError{}
				err = json.Unmarshal(contents, gotError)
				assert.NoError(t, err)

				assert.Equal(t, tc.wantErr, gotError)
			}
		})
	}
}
