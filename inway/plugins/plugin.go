// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins

import (
	"net/http"
	"net/url"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
)

type Context struct {
	Logger            *logger.Logger
	Response          http.ResponseWriter
	Request           *http.Request
	ConnectionInfo    *ConnectionInfo
	Token             *accesstoken.DecodedToken
	EndpointURL       *url.URL
	RequestReceivedAt time.Time
}

type ConnectionInfo struct {
	PeerID                string
	CertificateThumbprint string
	RawDERCertificates    [][]byte
}

type ServeFunc func(context *Context) error

type Plugin interface {
	Serve(next ServeFunc) ServeFunc
}

func BuildChain(serve ServeFunc, pluginList ...Plugin) ServeFunc {
	if len(pluginList) == 0 {
		return serve
	}

	return pluginList[0].Serve(BuildChain(serve, pluginList[1:]...))
}
