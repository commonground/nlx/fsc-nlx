// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	common_http "gitlab.com/commonground/nlx/fsc-nlx/common/http"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	inway_http "gitlab.com/commonground/nlx/fsc-nlx/inway/http"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type AuthRequest struct {
	Input *AuthRequestInput `json:"input"`
}

type AuthRequestInput struct {
	Headers         http.Header `json:"headers"`
	Path            string      `json:"path"`
	Method          string      `json:"method"`
	Query           string      `json:"query"`
	InwayCertChain  []string    `json:"inway_certificate_chain"`
	OutwayCertChain []string    `json:"outway_certificate_chain"`
	Body            string      `json:"body"`
}

type AuthResponse struct {
	Result AuthResponseData `json:"result"`
}

type AuthResponseData struct {
	Allowed bool               `json:"allowed"`
	Status  AuthResponseStatus `json:"status"`
}

type AuthResponseStatus struct {
	Reason string `json:"reason"`
}

type AuthorizationPlugin struct {
	authServerEnabled   bool
	serviceURL          string
	authorizationClient *http.Client
	inwayCertChain      []string
	withBody            bool
	maxBodyBytes        int
	chunkSize           int
}

type NewAuthorizationPluginArgs struct {
	AuthorizationClient *http.Client
	ServiceURL          string
	AuthServerEnabled   bool
	ExternalCert        *common_tls.CertificateBundle
	Clock               clock.Clock
	WithBody            bool
	MaxBodyBytes        int
	ChunkSize           int
}

func NewAuthorizationPlugin(args *NewAuthorizationPluginArgs) (*AuthorizationPlugin, error) {
	if args.ExternalCert == nil {
		return nil, fmt.Errorf("ExternalCert cannot be nil")
	}

	if args.Clock == nil {
		return nil, fmt.Errorf("clock cannot be nil")
	}

	c, err := contract.NewPeerCertFromCertificate(args.Clock, args.ExternalCert.RootCAs(), args.ExternalCert.Cert().Certificate)
	if err != nil {
		return nil, fmt.Errorf("could not create peer cert from args: %w", err)
	}

	return &AuthorizationPlugin{
		authServerEnabled:   args.AuthServerEnabled,
		serviceURL:          args.ServiceURL,
		authorizationClient: args.AuthorizationClient,
		inwayCertChain:      rawDERstoBase64(c.RawDERs()),
		withBody:            args.WithBody,
		maxBodyBytes:        args.MaxBodyBytes,
		chunkSize:           args.ChunkSize,
	}, err
}

func (plugin *AuthorizationPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		if context.Token.OutwayPeerID != context.ConnectionInfo.PeerID ||
			context.Token.OutwayCertificateThumbprint != context.ConnectionInfo.CertificateThumbprint {
			inway_http.WriteError(context.Response, httperrors.O1, httperrors.AccessDenied(context.ConnectionInfo.PeerID, context.ConnectionInfo.CertificateThumbprint))
			return nil
		}

		if plugin.authServerEnabled {
			authorizationResponse, authErr := plugin.authorizeRequest(context.Request, context)
			if authErr != nil && strings.Contains(authErr.Error(), "body exceeds maxBytes and cannot be encoded") {
				context.Logger.Error("error authorizing request", authErr)
				inway_http.WriteError(context.Response, httperrors.IAS1, httperrors.ErrorWhileAuthorizingRequestMaxBodyExceeded())

				return nil
			}
			if authErr != nil {
				context.Logger.Error("error authorizing request", authErr)
				inway_http.WriteError(context.Response, httperrors.IAS1, httperrors.ErrorWhileAuthorizingRequest())

				return nil
			}

			context.Logger.With("authorized", authorizationResponse.Result.Allowed).Info("authorization result")

			if !authorizationResponse.Result.Allowed {
				inway_http.WriteError(
					context.Response,
					httperrors.IAS1,
					httperrors.Unauthorized(authorizationResponse.Result.Status.Reason),
				)

				return nil
			}
		}

		return next(context)
	}
}

func (plugin *AuthorizationPlugin) authorizeRequest(r *http.Request, c *Context) (*AuthResponse, error) {
	h := r.Header

	req, err := http.NewRequest(http.MethodPost, plugin.serviceURL, http.NoBody)
	if err != nil {
		return nil, err
	}

	authRequestInput := &AuthRequestInput{
		Headers:         h,
		Method:          c.Request.Method,
		Path:            c.Request.URL.Path,
		Query:           c.Request.URL.RawQuery,
		InwayCertChain:  plugin.inwayCertChain,
		OutwayCertChain: rawDERstoBase64(c.ConnectionInfo.RawDERCertificates),
	}

	if plugin.withBody {
		buf := bytes.Buffer{}
		tee := io.TeeReader(r.Body, &buf)

		r.Body = io.NopCloser(&buf)

		authzBody, err := common_http.EncodeBody(tee, plugin.maxBodyBytes, plugin.chunkSize)
		if err != nil {
			return nil, err
		}

		authRequestInput.Body = authzBody
	}

	body, err := json.Marshal(&AuthRequest{
		Input: authRequestInput,
	})
	if err != nil {
		return nil, err
	}

	req.Body = io.NopCloser(bytes.NewBuffer(body))

	resp, err := plugin.authorizationClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("authorization service return non 200 status code. status code: %d", resp.StatusCode)
	}

	authorizationResponse := &AuthResponse{}

	err = json.NewDecoder(resp.Body).Decode(authorizationResponse)
	if err != nil {
		return nil, err
	}

	return authorizationResponse, nil
}

func rawDERstoBase64(rawDERs [][]byte) []string {
	certs := make([]string, len(rawDERs))

	for i, c := range rawDERs {
		certs[i] = base64.URLEncoding.EncodeToString(c)
	}

	return certs
}
