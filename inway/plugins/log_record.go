// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins

import (
	ctx_context "context"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	inway_http "gitlab.com/commonground/nlx/fsc-nlx/inway/http"
	txlog_metadata "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type LogRecordPlugin struct {
	selfPeerID string
	txLogger   transactionlog.TransactionLogger
	metadata   LogRecordMetadata
}

type LogRecordMetadata struct {
	Headers []string
	// now only headers are supported, can be extended to include query params, body values
}

type NewLogRecordPluginArgs struct {
	SelfPeerID     string
	TxLogger       transactionlog.TransactionLogger
	MetadataFields LogRecordMetadata
}

func NewLogRecordPlugin(args NewLogRecordPluginArgs) *LogRecordPlugin {
	return &LogRecordPlugin{
		selfPeerID: args.SelfPeerID,
		txLogger:   args.TxLogger,
		metadata:   args.MetadataFields,
	}
}

func (plugin *LogRecordPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		unparsedID := context.Request.Header.Get("Fsc-Transaction-Id")
		if unparsedID == "" {
			fscErr := httperrors.MissingLogRecordID()
			context.Logger.With("connecting_peer_id", context.ConnectionInfo.PeerID).Warn("Received request with missing transaction id", errors.New(fscErr.Message))

			inway_http.WriteError(context.Response, httperrors.O1, fscErr)

			return nil
		}

		id, err := transactionlog.NewTransactionIDFromString(unparsedID)
		if err != nil {
			context.Logger.With("connecting_peer_id", context.ConnectionInfo.PeerID).Warn("Received request with invalid transaction id", err)

			inway_http.WriteError(context.Response, httperrors.O1, httperrors.InvalidLogRecordID())

			return nil
		}

		err = plugin.createLogRecord(context, id)
		if err != nil {
			context.Logger.Error("failed to store transactionlog record", err)

			inway_http.WriteError(context.Response, httperrors.O1, httperrors.LogRecordWriteError())

			return nil
		}

		err = plugin.createMetadataRecord(context, id, &plugin.metadata)
		if err != nil {
			context.Logger.Error("failed to store LogRecordMetadata record", err)

			inway_http.WriteError(context.Response, httperrors.O1, httperrors.LogRecordWriteError())

			return nil
		}

		return next(context)
	}
}

func (plugin *LogRecordPlugin) createLogRecord(context *Context, id *transactionlog.TransactionID) error {
	var source interface{}
	if context.Token.OutwayDelegatorPeerID == "" {
		source = &transactionlog.RecordSource{
			OutwayPeerID: context.Token.OutwayPeerID,
		}
	} else {
		source = &transactionlog.RecordDelegatedSource{
			OutwayPeerID:    context.Token.OutwayPeerID,
			DelegatorPeerID: context.Token.OutwayDelegatorPeerID,
		}
	}

	var destination interface{}
	if context.Token.ServiceDelegatorPeerID == "" {
		destination = &transactionlog.RecordDestination{
			ServicePeerID: context.Token.ServicePeerID,
		}
	} else {
		destination = &transactionlog.RecordDelegatedDestination{
			ServicePeerID:   context.Token.ServicePeerID,
			DelegatorPeerID: context.Token.ServiceDelegatorPeerID,
		}
	}

	rec := &transactionlog.Record{
		TransactionID: id,
		GroupID:       context.Token.GroupID,
		GrantHash:     context.Token.GrantHash,
		Direction:     record.DirectionIn,
		ServiceName:   context.Token.ServiceName,
		Source:        source,
		Destination:   destination,
		Data: map[string]interface{}{
			"request-path": context.Request.URL.Path,
		},
		CreatedAt: context.RequestReceivedAt,
	}

	if err := plugin.txLogger.AddRecords(ctx_context.TODO(), []*transactionlog.Record{rec}); err != nil {
		return errors.Wrap(err, "unable to add records to txlog")
	}

	return nil
}

func (plugin *LogRecordPlugin) createMetadataRecord(context *Context, id *transactionlog.TransactionID, metadataFields *LogRecordMetadata) error {
	if metadataFields == nil {
		return nil
	}

	metadataContent := make(map[string]map[string]interface{})

	if metadataFields.Headers != nil {
		metadataContent["HEADERS"] = mapMetadataHeaders(context.Request.Header, metadataFields)
	}

	rec := &transactionlog.MetadataRecord{
		TransactionID: id,
		GroupID:       context.Token.GroupID,
		Direction:     txlog_metadata.DirectionIn,
		Metadata:      metadataContent,
	}

	if err := plugin.txLogger.AddMetadataRecords(ctx_context.TODO(), []*transactionlog.MetadataRecord{rec}); err != nil {
		return errors.Wrap(err, "unable to add LogRecordMetadata records to txlog")
	}

	return nil
}

func mapMetadataHeaders(headers http.Header, metadataFields *LogRecordMetadata) map[string]interface{} {
	if len(metadataFields.Headers) == 0 {
		return nil
	}

	metadataHeaders := make(map[string]interface{})
	for _, field := range metadataFields.Headers {
		metadataHeaders[field] = headers.Get(field)
	}

	return metadataHeaders
}
