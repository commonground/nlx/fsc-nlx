// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins_test

import (
	"crypto/tls"
	"crypto/x509"
	"net/http"
	"net/http/httptest"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/plugins"
)

func NopServeFunc(_ *plugins.Context) error {
	return nil
}

func FakeContext(cert *common_tls.CertificateBundle, token *accesstoken.DecodedToken) *plugins.Context {
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/test", nil)

	request.TLS = &tls.ConnectionState{
		PeerCertificates: []*x509.Certificate{cert.Certificate()},
	}

	return &plugins.Context{
		Request:  request,
		Response: recorder,
		Logger:   discardlogger.New(),
		Token:    token,
		ConnectionInfo: &plugins.ConnectionInfo{
			PeerID:                cert.GetPeerInfo().SerialNumber,
			CertificateThumbprint: cert.CertificateThumbprint(),
		},
	}
}
