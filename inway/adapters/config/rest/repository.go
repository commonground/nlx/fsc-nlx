// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restconfig

import (
	"crypto/x509"
	"fmt"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"

	"github.com/pkg/errors"

	controllerapi "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/intunauthenticated/rest/api/server"
)

type Repository struct {
	managerClient    *api.ClientWithResponses
	controllerClient controllerapi.ClientWithResponsesInterface
	selfAddress      string
	trustedRootCerts *x509.CertPool
	groupID          contract.GroupID
	clock            clock.Clock
}

type NewRestConfigArgs struct {
	SelfAddress      string
	ManagerClient    *api.ClientWithResponses
	ControllerClient controllerapi.ClientWithResponsesInterface
	TrustedRootCert  *x509.CertPool
	GroupID          string
	Clock            clock.Clock
}

func New(args *NewRestConfigArgs) (config.Repository, error) {
	if args.ManagerClient == nil {
		return nil, errors.New("manager client is required")
	}

	if args.ControllerClient == nil {
		return nil, errors.New("controller client is required")
	}

	if args.TrustedRootCert == nil {
		return nil, errors.New("trusted root certificates are required")
	}

	if args.SelfAddress == "" {
		return nil, errors.New("self address is required")
	}

	groupID, err := contract.NewGroupID(args.GroupID)
	if err != nil {
		return nil, fmt.Errorf("invalid groupID in args: %w", err)
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	return &Repository{
		selfAddress:      args.SelfAddress,
		managerClient:    args.ManagerClient,
		controllerClient: args.ControllerClient,
		trustedRootCerts: args.TrustedRootCert,
		groupID:          groupID,
		clock:            args.Clock,
	}, nil
}
