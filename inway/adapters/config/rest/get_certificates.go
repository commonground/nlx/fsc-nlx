// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restconfig

import (
	"context"
	"encoding/base64"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *Repository) GetCertificates(ctx context.Context) (*contract.PeerCertificates, error) {
	response, err := r.managerClient.GetJSONWebKeySetWithResponse(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificates from rest manager")
	}

	if response.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not get certificates from rest manager, received invalid status code %d: %s", response.StatusCode(), string(response.Body))
	}

	rawCerts := make([][]byte, 0)

	for _, cert := range response.JSON200.Keys {
		certBytes := make([]byte, 0)

		for _, c := range *cert.X5c {
			var b []byte

			b, err = base64.StdEncoding.DecodeString(c)
			if err != nil {
				return nil, errors.Wrap(err, "unable to decode certificate base64 string")
			}

			certBytes = append(certBytes, b...)
		}

		rawCerts = append(rawCerts, certBytes)
	}

	peerCertificates, err := contract.NewPeerCertificates(r.clock, r.trustedRootCerts, rawCerts)
	if err != nil {
		return nil, err
	}

	return peerCertificates, nil
}
