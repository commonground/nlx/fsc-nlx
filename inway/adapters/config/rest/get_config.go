// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restconfig

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/pkg/errors"
)

func (r *Repository) GetServiceEndpointURL(ctx context.Context, serviceName string) (*url.URL, error) {
	resp, err := r.controllerClient.GetServiceWithResponse(ctx, r.groupID.String(), serviceName)
	if err != nil {
		return nil, errors.Wrap(err, "unable to get service from controller api")
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not get service from controller api, received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	if resp.JSON200.Service.InwayAddress != r.selfAddress {
		return nil, errors.Errorf("this service is not provided by this Inway. it is provided by %q instead", resp.JSON200.Service.InwayAddress)
	}

	parsedURL, err := url.Parse(resp.JSON200.Service.EndpointUrl)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse endpoint URL returned by the controller api. url: %s ", resp.JSON200.Service.EndpointUrl)
	}

	return parsedURL, nil
}
