// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restcontroller

import (
	"context"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/models"
	controllerapi "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/registration/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/adapters/controller"
)

type restController struct {
	controller controllerapi.ClientWithResponsesInterface
}

func New(client controllerapi.ClientWithResponsesInterface) (*restController, error) {
	if client == nil {
		return nil, fmt.Errorf("client is required")
	}

	return &restController{
		controller: client,
	}, nil
}

func (m *restController) RegisterInway(ctx context.Context, args *controller.RegisterInwayArgs) error {
	res, err := m.controller.RegisterInwayWithResponse(ctx, args.GroupID, args.Name, models.RegisterInwayJSONRequestBody{
		Address: args.Address,
	})
	if err != nil {
		return errors.Wrap(err, "could not register inway in rest controller")
	}

	if res.StatusCode() != http.StatusNoContent {
		return fmt.Errorf("could not register inway in rest controller, received invalid status code %d: %s", res.StatusCode(), string(res.Body))
	}

	return nil
}
