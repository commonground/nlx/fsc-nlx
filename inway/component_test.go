/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package inway_test

import (
	"context"
	"crypto/x509"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
	"time"

	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/inway"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var (
	orgACertBundle *tls.CertificateBundle
	orgBCertBundle *tls.CertificateBundle
	orgCCertBundle *tls.CertificateBundle
	orgOnCRL       *tls.CertificateBundle
	crlFile        *x509.RevocationList
	testClock      = testingutils.NewMockClock(time.Now())
)

const mockService = "mock-service"

func TestMain(m *testing.M) {
	var err error

	orgACertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestPeerA) //probably need another name since access token is
	if err != nil {
		log.Fatal(err)
	}

	orgBCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestPeerB)
	if err != nil {
		log.Fatal(err)
	}

	orgCCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestPeerC)
	if err != nil {
		log.Fatal(err)
	}

	orgOnCRL, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.OrgOnCRL)
	if err != nil {
		log.Fatal(err)
	}

	crlFileDer, err := os.ReadFile(filepath.Join("..", "testing", "pki", "ca.crl"))
	if err != nil {
		log.Fatal(err)
	}

	crlFile, err = x509.ParseRevocationList(crlFileDer)
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

func newInway(t *testing.T, configRepository config.Repository, transactionLogger transactionlog.TransactionLogger) *httptest.Server {
	inwayURL, _ := inway.NewAddress("https://localhost:443")

	if transactionLogger == nil {
		transactionLogger = newFakeTransactionLogger()
	}

	if configRepository == nil {
		configRepository = newFakeConfigRepository(testClock, "")
	}

	crlCache, err := tls.NewCRL(nil)
	if err != nil {
		t.Fatal("cannot create CRLsCache Cache")
	}

	err = crlCache.AddRevocationList("http://localhost", crlFile)
	if err != nil {
		t.Fatal("cannot add CRL to cache")
	}

	testInway, err := inway.NewInway(&inway.NewInwayArgs{
		Context:                      context.Background(),
		Logger:                       discardlogger.New(),
		Txlogger:                     transactionLogger,
		Name:                         "testInway",
		GroupID:                      "fsc-local",
		SelfAddress:                  inwayURL,
		OrgCertBundle:                orgACertBundle,
		Controller:                   nil,
		AuthServiceURL:               "",
		AuthCAPath:                   "",
		ConfigRepository:             configRepository,
		ServiceEndpointCacheDuration: 0,
		ServiceProxyCacheSize:        100,
		Clock:                        testClock,
		CRL:                          crlCache,
		TxLogMetadataHeaders:         []string{"TEST_HEADER", "SECOND_TEST_HEADER"},
	})
	if err != nil {
		t.Fatalf("could not create new Inway from parameters: %s", err)
	}

	srv := httptest.NewUnstartedServer(testInway.Handler())
	srv.TLS = orgACertBundle.TLSConfig(orgACertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv
}

func createInwayAPIClient(certBundle *tls.CertificateBundle) *http.Client {
	transport := &http.Transport{
		TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
	}

	return &http.Client{
		Transport: transport,
	}
}
