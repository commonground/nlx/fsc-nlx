// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway_test

import (
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

const (
	mockPath              = "/" + mockService
	validFSCTransactionID = "018bb8c2-7fee-75a5-9618-4c5728647726"
	errorSource           = "inway"
	errorLocation         = "O1"
)

// FSC Test suite test cases: `Inway-Incoming-Request-1`, `Inway-Incoming-Request-5`, `Inway-TransactionLog-1`, `Inway-TransactionLog-2`
func TestHappyFlow(t *testing.T) {
	t.Parallel()

	// Arrange
	accessToken, err := generateValidToken()
	assert.NoError(t, err)

	var apiServerRequestReceivedAt time.Time
	testAPIServer := newMockAPI(t, &apiServerRequestReceivedAt, accessToken)

	defer testAPIServer.Close()

	configRepository := newFakeConfigRepository(testClock, testAPIServer.URL)
	transactionLogger := newFakeTransactionLogger()

	inwayServer := newInway(t, configRepository, transactionLogger)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	request := createValidRequest(inwayServer.URL + mockPath)

	// Act
	response, err := client.Do(request)
	assert.NoError(t, err)

	defer response.Body.Close()

	// Assert
	if !assert.Equal(t, http.StatusOK, response.StatusCode) {
		responseBody, err := io.ReadAll(response.Body)
		assert.NoError(t, err)

		t.Logf("response body: %s", responseBody)
	}

	records := transactionLogger.records
	assert.True(t, apiServerRequestReceivedAt.After(transactionLogger.time)) // The testAPIServer must be called after the TxLogAPI

	assert.Equal(t, 1, len(transactionLogger.records))
	assert.Equal(t, orgBCertBundle.GetPeerInfo().SerialNumber, records[0].Source.(*transactionlog.RecordSource).OutwayPeerID)
	assert.Equal(t, orgACertBundle.GetPeerInfo().SerialNumber, records[0].Destination.(*transactionlog.RecordDestination).ServicePeerID)
	assert.Equal(t, record.DirectionIn, records[0].Direction)
	assert.Equal(t, "mock-service", records[0].ServiceName)
	assert.Equal(t, validFSCTransactionID, records[0].TransactionID.String())
	assert.Equal(t, mockPath, records[0].Data["request-path"])
}

// FSC Test suite test cases: `Inway-TransactionLog-4`
//
//nolint:dupl // looks like  TestHappyFlow but uses different token and assertions
func TestInwayValidTokenDelegatedServiceConnection(t *testing.T) {
	t.Parallel()

	validDelegatedAccessToken, err := generateValidDelegationToken(orgCCertBundle.GetPeerInfo(), nil)
	assert.NoError(t, err)

	// Arrange
	var apiServerRequestReceivedAt time.Time
	testAPIServer := newMockAPI(t, &apiServerRequestReceivedAt, validDelegatedAccessToken)

	defer testAPIServer.Close()

	configRepository := newFakeConfigRepository(testClock, testAPIServer.URL)
	transactionLogger := newFakeTransactionLogger()

	inwayServer := newInway(t, configRepository, transactionLogger)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)
	request := createRequestWithCustomToken(inwayServer.URL+mockPath, validDelegatedAccessToken)

	// Act
	response, err := client.Do(request)
	assert.NoError(t, err)

	defer response.Body.Close()

	// Assert
	if !assert.Equal(t, http.StatusOK, response.StatusCode) {
		responseBody, err := io.ReadAll(response.Body)
		assert.NoError(t, err)

		t.Logf("response body: %s", responseBody)
	}

	records := transactionLogger.records
	assert.True(t, apiServerRequestReceivedAt.After(transactionLogger.time)) // The testAPIServer must be called after the TxLogAPI

	assert.Equal(t, 1, len(transactionLogger.records))
	assert.Equal(t, orgBCertBundle.GetPeerInfo().SerialNumber, records[0].Source.(*transactionlog.RecordDelegatedSource).OutwayPeerID)
	assert.Equal(t, orgCCertBundle.GetPeerInfo().SerialNumber, records[0].Source.(*transactionlog.RecordDelegatedSource).DelegatorPeerID)
	assert.Equal(t, orgACertBundle.GetPeerInfo().SerialNumber, records[0].Destination.(*transactionlog.RecordDestination).ServicePeerID)
	assert.Equal(t, record.DirectionIn, records[0].Direction)
	assert.Equal(t, "mock-service", records[0].ServiceName)
	assert.Equal(t, validFSCTransactionID, records[0].TransactionID.String())
	assert.Equal(t, mockPath, records[0].Data["request-path"])
}

// FSC Test suite test cases: `Inway-TransactionLog-5`
//
//nolint:dupl // looks like  TestInwayValidTokenDelegatedServiceConnection but uses different token and assertions
func TestInwayValidTokenDelegatedServicePublication(t *testing.T) {
	t.Parallel()

	validDelegatedAccessToken, err := generateValidDelegationToken(nil, orgACertBundle.GetPeerInfo())
	assert.NoError(t, err)

	// Arrange
	var apiServerRequestReceivedAt time.Time
	testAPIServer := newMockAPI(t, &apiServerRequestReceivedAt, validDelegatedAccessToken)

	defer testAPIServer.Close()

	configRepository := newFakeConfigRepository(testClock, testAPIServer.URL)
	transactionLogger := newFakeTransactionLogger()

	inwayServer := newInway(t, configRepository, transactionLogger)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)
	request := createRequestWithCustomToken(inwayServer.URL+mockPath, validDelegatedAccessToken)

	// Act
	response, err := client.Do(request)
	assert.NoError(t, err)

	defer response.Body.Close()

	// Assert
	if !assert.Equal(t, http.StatusOK, response.StatusCode) {
		responseBody, err := io.ReadAll(response.Body)
		assert.NoError(t, err)

		t.Logf("response body: %s", responseBody)
	}

	records := transactionLogger.records
	assert.True(t, apiServerRequestReceivedAt.After(transactionLogger.time)) // The testAPIServer must be called after the TxLogAPI

	assert.Equal(t, 1, len(transactionLogger.records))
	assert.Equal(t, orgBCertBundle.GetPeerInfo().SerialNumber, records[0].Source.(*transactionlog.RecordSource).OutwayPeerID)
	assert.Equal(t, orgACertBundle.GetPeerInfo().SerialNumber, records[0].Destination.(*transactionlog.RecordDelegatedDestination).ServicePeerID)
	assert.Equal(t, orgACertBundle.GetPeerInfo().SerialNumber, records[0].Destination.(*transactionlog.RecordDelegatedDestination).DelegatorPeerID)
	assert.Equal(t, record.DirectionIn, records[0].Direction)
	assert.Equal(t, "mock-service", records[0].ServiceName)
	assert.Equal(t, validFSCTransactionID, records[0].TransactionID.String())
	assert.Equal(t, mockPath, records[0].Data["request-path"])
}

// FSC Test suite test case: `Inway-Incoming-Request-2`
func TestInwayNoToken(t *testing.T) {
	t.Parallel()

	// Arrange
	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(t, nil, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createValidRequest(inwayServer.URL + mockPath)
	validRequest.Header.Del("Fsc-Authorization")

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.MissingAuthHeader()
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, "Bearer", response.Header.Get("WWW-Authenticate"))
	assert.Equal(t, httperrors.MissingAuthHeaderErr.String(), response.Header.Get("Fsc-Error-Code"))
}

// FSC Test suite test cases: `Inway-TransactionLog-3`
func TestInwayNoTransactionID(t *testing.T) {
	t.Parallel()

	// Arrange
	testServer := newErrorMockAPI()
	defer testServer.Close()

	configRepository := newFakeConfigRepository(testClock, testServer.URL)

	inwayServer := newInway(t, configRepository, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createValidRequest(inwayServer.URL + mockPath)
	validRequest.Header.Del("Fsc-Transaction-Id")

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.MissingLogRecordID()
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.MissingLogRecordIDErr.String(), response.Header.Get("Fsc-Error-Code"))
}

func TestInwayInvalidTransactionID(t *testing.T) {
	t.Parallel()

	// Arrange
	testServer := newErrorMockAPI()
	defer testServer.Close()

	configRepository := newFakeConfigRepository(testClock, testServer.URL)

	inwayServer := newInway(t, configRepository, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createValidRequest(inwayServer.URL + mockPath)
	validRequest.Header.Set("Fsc-Transaction-Id", "I am not valid")

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.InvalidLogRecordID()
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.InvalidLogRecordIDErr.String(), response.Header.Get("Fsc-Error-Code"))
}

// FSC test suite test case: `Inway-Incoming-Request-3`
//
//nolint:dupl // looks like TestInwayWronglySignedToken but uses different token and assertions
func TestInwayWronglyBoundToken(t *testing.T) {
	t.Parallel()

	invalidTokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-local",
		GrantHash:                   "$1$4$5h6QgvF1IDMrGjnC1orUC2nbS2NfEkbROggi2q-Kcs_STXlOQcGXzbA_bdgwQqc6MJu_Xh2qGSDRJL_LoeEEAQ",
		OutwayPeerID:                orgBCertBundle.GetPeerInfo().SerialNumber,
		OutwayCertificateThumbprint: orgBCertBundle.CertificateThumbprint(),
		ServiceName:                 "mock-service",
		ServiceInwayAddress:         "https://inway.organization-a.nlx.local:443",
		ServicePeerID:               orgACertBundle.GetPeerInfo().SerialNumber,
		ExpiryDate:                  testClock.Now().Add(time.Hour),
		NotBefore:                   testClock.Now().Add(-time.Hour * 2),
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PrivateKey:            orgACertBundle.PrivateKey(),
		},
	}

	invalidToken, err := accesstoken.New(invalidTokenArgs)
	assert.NoError(t, err)

	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(t, nil, nil)
	defer inwayServer.Close()

	requestingOrg := orgCCertBundle

	client := createInwayAPIClient(requestingOrg)

	validRequest := createRequestWithCustomToken(inwayServer.URL+mockPath, invalidToken.Value())

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.AccessDenied(requestingOrg.GetPeerInfo().SerialNumber, requestingOrg.CertificateThumbprint())
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusForbidden, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.AccessDeniedErr.String(), response.Header.Get("Fsc-Error-Code"))
}

// FSC test suite test case: `Inway-Incoming-Request-6`
func TestExpiredToken(t *testing.T) {
	t.Parallel()

	expiredTokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-local",
		GrantHash:                   "$1$4$5h6QgvF1IDMrGjnC1orUC2nbS2NfEkbROggi2q-Kcs_STXlOQcGXzbA_bdgwQqc6MJu_Xh2qGSDRJL_LoeEEAQ",
		OutwayPeerID:                orgBCertBundle.GetPeerInfo().SerialNumber,
		OutwayCertificateThumbprint: orgBCertBundle.CertificateThumbprint(),
		ServiceName:                 "mock-service",
		ServiceInwayAddress:         "https://inway.organization-a.nlx.local:443",
		ServicePeerID:               orgACertBundle.GetPeerInfo().SerialNumber,
		ExpiryDate:                  testClock.Now().Add(-time.Hour),
		NotBefore:                   testClock.Now().Add(-time.Hour * 2),
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PrivateKey:            orgACertBundle.PrivateKey(),
		},
	}

	expiredToken, err := accesstoken.New(expiredTokenArgs)
	assert.NoError(t, err)

	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(t, nil, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createRequestWithCustomToken(inwayServer.URL+mockPath, expiredToken.Value())
	validRequest.Header.Set("Fsc-Authorization", "Bearer"+expiredToken.Value())

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.ExpiredAuthorizationToken("invalid token, expired on: " + testClock.Now().Add(-1*time.Hour).Truncate(time.Second).UTC().String())
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, "Bearer", response.Header.Get("WWW-Authenticate"))
	assert.Equal(t, httperrors.ExpiredAuthorizationTokenErr.String(), response.Header.Get("Fsc-Error-Code"))
}

func TestLogWriteError(t *testing.T) {
	t.Parallel()

	accessToken, err := generateValidToken()
	assert.NoError(t, err)

	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(t, nil, &errorTransactionLogger{})
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createRequestWithCustomToken(inwayServer.URL+mockPath, accessToken)

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.LogRecordWriteError()
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusInternalServerError, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.LogRecordWriteErr.String(), response.Header.Get("Fsc-Error-Code"))
}

// test invalid client certificate
func TestInwayInvalidClientCert(t *testing.T) {
	t.Parallel()

	inwayServer := newInway(t, nil, nil)
	defer inwayServer.Close()

	invalidCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestInternal)
	if err != nil {
		log.Panic(err)
	}

	client := createInwayAPIClient(invalidCertBundle)

	request := createValidRequest(inwayServer.URL + mockPath)

	// Act
	_, err = client.Do(request) //nolint:bodyclose // there is no body to close

	// Assert
	wantErr := x509.UnknownAuthorityError{}
	assert.ErrorAs(t, err, &wantErr)
	assert.ErrorContains(t, err, "tls: failed to verify certificate: x509: certificate signed by unknown authority")
}

func TestRequestPath(t *testing.T) {
	t.Parallel()

	// Arrange
	testAPIReceivedURLPaths := []string{}

	testAPIServer := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			testAPIReceivedURLPaths = append(testAPIReceivedURLPaths, r.URL.Path)
		}))

	defer testAPIServer.Close()

	serviceEndpointURL := testAPIServer.URL + "/api"

	configRepository := newFakeConfigRepository(testClock, serviceEndpointURL)
	transactionLogger := newFakeTransactionLogger()

	inwayServer := newInway(t, configRepository, transactionLogger)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	requests := []*http.Request{
		createValidRequest(inwayServer.URL),
		createValidRequest(inwayServer.URL + "/"),
		createValidRequest(inwayServer.URL + "/foobar"),
	}

	// Act
	for _, request := range requests {
		response, err := client.Do(request)
		assert.NoError(t, err)

		err = response.Body.Close()
		assert.NoError(t, err)
	}

	// Assert
	assert.Equal(t, []string{"/api/", "/api/", "/api/foobar"}, testAPIReceivedURLPaths)
}

// test revoked client certificate
func TestInwayRevokedClientCert(t *testing.T) {
	t.Parallel()

	accessTokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-local",
		GrantHash:                   "$1$4$5h6QgvF1IDMrGjnC1orUC2nbS2NfEkbROggi2q-Kcs_STXlOQcGXzbA_bdgwQqc6MJu_Xh2qGSDRJL_LoeEEAQ",
		OutwayPeerID:                orgOnCRL.GetPeerInfo().SerialNumber,
		OutwayCertificateThumbprint: orgOnCRL.CertificateThumbprint(),
		ServiceName:                 "mock-service",
		ServiceInwayAddress:         "https://inway.organization-a.nlx.local:443",
		ServicePeerID:               orgACertBundle.GetPeerInfo().SerialNumber,
		ExpiryDate:                  testClock.Now().Add(time.Minute),
		NotBefore:                   testClock.Now().Add(-time.Minute * 2),
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PrivateKey:            orgACertBundle.PrivateKey(),
		},
	}

	accessToken, err := accesstoken.New(accessTokenArgs)
	assert.NoError(t, err)

	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(t, nil, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgOnCRL)

	validRequest := createRequestWithCustomToken(inwayServer.URL+mockPath, accessToken.Value())
	validRequest.Header.Set("Fsc-Authorization", "Bearer"+accessToken.Value())

	// Act
	response, err := client.Do(validRequest) //nolint:bodyclose // there is no body to close
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.CertificateRevoked(fmt.Errorf("certificate with serialnumber: %s is present on a Certificate Revocation List", orgOnCRL.Certificate().SerialNumber))
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.CertificateRevokedErr.String(), response.Header.Get("Fsc-Error-Code"))
}

func TestLogMetadata(t *testing.T) {
	t.Parallel()

	// Arrange
	accessToken, err := generateValidToken()
	assert.NoError(t, err)

	var apiServerRequestReceivedAt time.Time
	testAPIServer := newMockAPI(t, &apiServerRequestReceivedAt, accessToken)

	defer testAPIServer.Close()

	configRepository := newFakeConfigRepository(testClock, testAPIServer.URL)
	transactionLogger := newFakeTransactionLogger()

	inwayServer := newInway(t, configRepository, transactionLogger)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	request := createValidRequest(inwayServer.URL + mockPath)
	request.Header.Set("TEST_HEADER", "test value")
	request.Header.Set("SECOND_TEST_HEADER", "second value")

	// Act
	response, err := client.Do(request)
	assert.NoError(t, err)

	defer response.Body.Close()

	// Assert
	if !assert.Equal(t, http.StatusOK, response.StatusCode) {
		responseBody, err := io.ReadAll(response.Body)
		assert.NoError(t, err)

		t.Logf("response body: %s", responseBody)
	}

	assert.Len(t, transactionLogger.records, 1)
	assert.Len(t, transactionLogger.metadata, 1)
	assert.Len(t, transactionLogger.metadata[0].Metadata["HEADERS"], 2)
	assert.Equal(t, "test value", transactionLogger.metadata[0].Metadata["HEADERS"]["TEST_HEADER"])
	assert.Equal(t, "second value", transactionLogger.metadata[0].Metadata["HEADERS"]["SECOND_TEST_HEADER"])
}

func newMockAPI(t *testing.T, apiServerRequestReceivedAt *time.Time, accessToken string) *httptest.Server {
	return httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			*apiServerRequestReceivedAt = time.Now()
			w.WriteHeader(http.StatusOK)
			assert.Equal(t, mockPath, r.URL.Path)
			assert.Equal(t, "Bearer "+accessToken, r.Header.Get("Fsc-Authorization"))
			assert.Equal(t, validFSCTransactionID, r.Header.Get("Fsc-Transaction-Id"))
			assert.Equal(t, orgBCertBundle.GetPeerInfo().SerialNumber, r.Header.Get("Fsc-Request-Peer-Id"))
		}))
}

// Certain test scenarios mandate the test server not to be called. This is forced by throwing an error
func newErrorMockAPI() *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Fatal("this test server should not be called")
	}))
}

func createValidRequest(url string) *http.Request {
	accessToken, err := generateValidToken()
	if err != nil {
		log.Fatal(err)
	}

	return createRequestWithCustomToken(url, accessToken)
}

func createRequestWithCustomToken(url, accessToken string) *http.Request {
	request, err := http.NewRequest(http.MethodGet, url, http.NoBody)
	if err != nil {
		log.Fatal(err)
	}

	request.Header.Set("Fsc-Authorization", "Bearer "+accessToken)
	request.Header.Set("Fsc-Transaction-Id", validFSCTransactionID)

	return request
}

func generateValidToken() (string, error) {
	return generateValidDelegationToken(nil, nil)
}

func generateValidDelegationToken(outwayDelegator, serviceDelegator *tls.OrganizationInformation) (string, error) {
	tokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-local",
		GrantHash:                   "$1$4$5h6QgvF1IDMrGjnC1orUC2nbS2NfEkbROggi2q-Kcs_STXlOQcGXzbA_bdgwQqc6MJu_Xh2qGSDRJL_LoeEEAQ",
		OutwayPeerID:                orgBCertBundle.GetPeerInfo().SerialNumber,
		OutwayCertificateThumbprint: orgBCertBundle.CertificateThumbprint(),
		ServiceName:                 "mock-service",
		ServiceInwayAddress:         "https://inway.organization-a.nlx.local:443",
		ServicePeerID:               orgACertBundle.GetPeerInfo().SerialNumber,
		ExpiryDate:                  testClock.Now().Add(time.Minute),
		NotBefore:                   testClock.Now().Add(-time.Second),
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PrivateKey:            orgACertBundle.PrivateKey(),
		},
	}
	if outwayDelegator != nil {
		tokenArgs.OutwayDelegatorPeerID = outwayDelegator.SerialNumber
	}

	if serviceDelegator != nil {
		tokenArgs.ServiceDelegatorPeerID = serviceDelegator.SerialNumber
	}

	signedToken, err := accesstoken.New(tokenArgs)
	if err != nil {
		return "", err
	}

	return signedToken.Value(), nil
}
