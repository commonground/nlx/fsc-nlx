// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package inway_test

import (
	"context"
	"errors"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
)

type fakeTransactionLogger struct {
	records []*transactionlog.Record
	metadata []*transactionlog.MetadataRecord
	time    time.Time
}

func (m *fakeTransactionLogger) AddMetadataRecords(ctx context.Context, recs []*transactionlog.MetadataRecord) error {
	m.metadata = append(m.metadata, recs...)
	m.time = time.Now()

	return nil
}

func newFakeTransactionLogger() *fakeTransactionLogger {
	return &fakeTransactionLogger{}
}

func (m *fakeTransactionLogger) AddRecords(_ context.Context, records []*transactionlog.Record) error {
	m.records = append(m.records, records...)
	m.time = time.Now()

	return nil
}

func (m *fakeTransactionLogger) Close() error {
	return nil
}

type errorTransactionLogger struct{}

func (m *errorTransactionLogger) AddMetadataRecords(ctx context.Context, recs []*transactionlog.MetadataRecord) error {
	return errors.New("cannot write metadata to transaction log")
}

func (m *errorTransactionLogger) AddRecords(_ context.Context, records []*transactionlog.Record) error {
	return errors.New("cannot write record to transaction log")
}

func (m *errorTransactionLogger) Close() error {
	return nil
}
