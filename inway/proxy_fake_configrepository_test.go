// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package inway_test

import (
	"context"
	"errors"
	"net/url"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type fakeConfigRepository struct {
	outwayCertBundle   *tls.CertificateBundle
	serviceEndpointURL string
	clock              clock.Clock
}

func newFakeConfigRepository(c clock.Clock, serviceEndpointURL string) *fakeConfigRepository {
	return &fakeConfigRepository{
		outwayCertBundle:   orgACertBundle,
		serviceEndpointURL: serviceEndpointURL,
		clock:              c,
	}
}

func (m *fakeConfigRepository) GetServiceEndpointURL(_ context.Context, serviceName string) (*url.URL, error) {
	if serviceName != mockService {
		return nil, errors.New("service not known in manager")
	}

	return url.Parse(m.serviceEndpointURL)
}

func (m *fakeConfigRepository) GetCertificates(_ context.Context) (*contract.PeerCertificates, error) {
	cert, err := contract.NewPeerCertFromCertificate(m.clock, m.outwayCertBundle.RootCAs(), m.outwayCertBundle.Cert().Certificate)
	if err != nil {
		return nil, errors.Join(err, errors.New("could not create PeerCertificate from CertBundle"))
	}

	return &contract.PeerCertificates{
		cert.CertificateThumbprint(): cert,
	}, nil
}

func (m *fakeConfigRepository) Close() error {
	return nil
}
