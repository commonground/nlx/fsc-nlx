// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway_test

import (
	"context"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/inway"
	common_testing "gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var pkiDir = filepath.Join("..", "testing", "pki")

func Test_NewInway(t *testing.T) {
	orgCertWithoutName, orgCert := getCertBundles()
	tests := map[string]struct {
		params               *inway.NewInwayArgs
		expectedErrorMessage string
	}{
		"missing_context": {
			params: &inway.NewInwayArgs{
				Context: nil,
				Clock:   testClock,
				GroupID: "fsc-local",
				SelfAddress: func() inway.Address {
					a, err := inway.NewAddress("https://inway-a.test:443")
					assert.NoError(t, err)

					return a
				}(),
				OrgCertBundle: orgCert,
				Name:          "my-inway",
			},
			expectedErrorMessage: "context is nil. needed to close gracefully",
		},
		"missing_group_id": {
			params: &inway.NewInwayArgs{
				Context: context.Background(),
				Clock:   testClock,
				GroupID: "",
				SelfAddress: func() inway.Address {
					a, err := inway.NewAddress("https://inway-a.test:443")
					assert.NoError(t, err)

					return a
				}(),
				OrgCertBundle: orgCert,
				Name:          "my-inway",
			},
			expectedErrorMessage: "invalid groupID in args: group ID cannot be empty",
		},
		"certificates_without_an_organization_name": {
			params: &inway.NewInwayArgs{
				Context: context.Background(),
				Clock:   testClock,
				GroupID: "fsc-local",
				SelfAddress: func() inway.Address {
					a, err := inway.NewAddress("https://inway-a.test:443")
					assert.NoError(t, err)

					return a
				}(),
				OrgCertBundle: orgCertWithoutName,
				Name:          "my-inway",
			},
			expectedErrorMessage: "cannot obtain organization name from self cert",
		},
		"self_address_not_in_certicate": {
			params: &inway.NewInwayArgs{
				Context: context.Background(),
				Clock:   testClock,
				GroupID: "fsc-local",
				SelfAddress: func() inway.Address {
					a, err := inway.NewAddress("https://test.com:443")
					assert.NoError(t, err)

					return a
				}(),
				OrgCertBundle: orgCert,
				Name:          "my-inway",
			},
			expectedErrorMessage: "'test.com' is not in the list of DNS names of the certificate, [localhost inway-a.test]",
		},
		"missing_name": {
			params: &inway.NewInwayArgs{
				Context: context.Background(),
				Clock:   testClock,
				GroupID: "fsc-local",
				SelfAddress: func() inway.Address {
					a, err := inway.NewAddress("https://inway-a.test:443")
					assert.NoError(t, err)

					return a
				}(),
				OrgCertBundle: orgCert,
				Name:          "",
			},
			expectedErrorMessage: "a valid name is required (alphanumeric & dashes, max. 100 characters)",
		},
		"invalid_name": {
			params: &inway.NewInwayArgs{
				Context: context.Background(),
				Clock:   testClock,
				GroupID: "fsc-local",
				SelfAddress: func() inway.Address {
					a, err := inway.NewAddress("https://inway-a.test:443")
					assert.NoError(t, err)

					return a
				}(),
				OrgCertBundle: orgCert,
				Name:          "#",
			},
			expectedErrorMessage: "a valid name is required (alphanumeric & dashes, max. 100 characters)",
		},
		"missing_config_repository": {
			params: &inway.NewInwayArgs{
				Context: context.Background(),
				Clock:   testClock,
				GroupID: "fsc-local",
				SelfAddress: func() inway.Address {
					a, err := inway.NewAddress("https://inway-a.test:443")
					assert.NoError(t, err)

					return a
				}(),
				OrgCertBundle: orgCert,
				Name:          "test",
			},
			expectedErrorMessage: "could not create config: repo is required",
		},
		"missing_clock": {
			params: &inway.NewInwayArgs{
				Context:          context.Background(),
				Clock:            nil,
				ConfigRepository: newFakeConfigRepository(testClock, ""),
				GroupID:          "fsc-local",
				SelfAddress: func() inway.Address {
					a, err := inway.NewAddress("https://inway-a.test:443")
					assert.NoError(t, err)

					return a
				}(),
				OrgCertBundle: orgCert,
				Name:          "test",
			},
			expectedErrorMessage: "clock is required",
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			_, err := inway.NewInway(tt.params)
			assert.EqualError(t, err, tt.expectedErrorMessage)
		})
	}
}

func getCertBundles() (orgCertWithoutName, orgCert *common_tls.CertificateBundle) {
	orgCertWithoutName, _ = common_testing.GetCertificateBundle(pkiDir, common_testing.OrgWithoutName)

	orgCert, _ = common_testing.GetCertificateBundle(pkiDir, common_testing.NLXTestPeerA)

	return orgCertWithoutName, orgCert
}
