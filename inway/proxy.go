// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	inway_http "gitlab.com/commonground/nlx/fsc-nlx/inway/http"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/plugins"
)

const FscAuthorizationHeader = "Fsc-Authorization"

func (i *Inway) handleProxyRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	now := i.clock.Now()

	logger := i.logger.With(
		"request-path", r.URL.Path,
		"request-remote-address", r.RemoteAddr,
	)

	encodedToken := strings.TrimSpace(strings.Replace(r.Header.Get(FscAuthorizationHeader), "Bearer", "", 1))
	if encodedToken == "" {
		fscErr := httperrors.MissingAuthHeader()

		logger.Warn(fmt.Sprintf("received request without %s header", FscAuthorizationHeader), errors.New(fscErr.Message))
		w.Header().Set("WWW-Authenticate", "Bearer")
		inway_http.WriteError(w, httperrors.O1, httperrors.MissingAuthHeader())

		return
	}

	certificateThumbprint, err := accesstoken.GetCertificateThumbprintFromToken(encodedToken)
	if err != nil {
		logger.Warn("could not get certificate thumbprint from token", err)
		inway_http.WriteError(w, httperrors.O1, httperrors.ErrorWhileAuthorizingRequest())

		return
	}

	cert, err := i.config.GetCertificate(ctx, *certificateThumbprint)
	if err != nil {
		logger.Warn("could not get certificate from config repository", err)
		inway_http.WriteError(w, httperrors.O1, httperrors.ErrorWhileAuthorizingRequest())

		return
	}

	token, err := accesstoken.DecodeFromString(i.clock, cert, encodedToken)

	if err != nil && strings.Contains(err.Error(), "expired") {
		logger.Warn("received request with expired token", err)
		w.Header().Set("WWW-Authenticate", "Bearer")
		inway_http.WriteError(w, httperrors.O1, httperrors.ExpiredAuthorizationToken(err.Error()))

		return
	}

	if err != nil {
		logger.Warn("received request with invalid authorization token", err)
		w.Header().Set("WWW-Authenticate", "Bearer")
		inway_http.WriteError(w, httperrors.O1, httperrors.InvalidAuthorizationToken(err.Error()))

		return
	}

	if !i.groupID.IsEqual(token.GroupID) {
		fscErr := httperrors.WrongGroupIDInToken(i.groupID.String(), token.GroupID)

		logger.Warn("received token with wrong group ID", errors.New(fscErr.Message))
		inway_http.WriteError(w, httperrors.O1, fscErr)

		return
	}

	endpointURL, err := i.config.GetEndpointURL(ctx, token.ServiceName)
	if err != nil {
		logger.Warn("could not get endpoint URL", err)
		// TODO: rename to httperrors.ErrorGettingEndpointURL
		inway_http.WriteError(w, httperrors.O1, httperrors.ServiceDoesNotExist(token.ServiceName))

		return
	}

	context := &plugins.Context{
		Logger:            logger,
		Response:          w,
		Request:           r,
		ConnectionInfo:    &plugins.ConnectionInfo{},
		Token:             token,
		RequestReceivedAt: now,
		EndpointURL:       endpointURL,
	}

	chain := plugins.BuildChain(func(context *plugins.Context) error {
		e := context.EndpointURL

		r.Host = e.Host

		proxy, ok := i.serviceProxiesCache.Get(endpointURL.String())

		if !ok {
			proxy = &httputil.ReverseProxy{
				Director: func(req *http.Request) {
					req.URL.Scheme = e.Scheme
					req.URL.Host = e.Host
					req.URL.Path = e.Path + req.URL.Path

					if e.RawQuery == "" || req.URL.RawQuery == "" {
						req.URL.RawQuery = e.RawQuery + req.URL.RawQuery
					} else {
						req.URL.RawQuery = e.RawQuery + "&" + req.URL.RawQuery
					}

					if _, ok := req.Header["User-Agent"]; !ok {
						// explicitly disable User-Agent so it's not set to default value
						req.Header.Set("User-Agent", "")
					}
				},
			}

			proxy.Transport = newRoundTripHTTPTransport()
			proxy.ErrorHandler = i.LogAPIErrors

			i.serviceProxiesCache.Add(endpointURL.String(), proxy)
		}

		proxy.ServeHTTP(w, r)

		return nil
	}, i.plugins...)

	if errExecutingChain := chain(context); errExecutingChain != nil {
		logger.Error("error executing plugin chain", errExecutingChain)

		inway_http.WriteError(w, httperrors.O1, httperrors.ErrorExecutingPluginChain())
	}
}

func (i *Inway) LogAPIErrors(w http.ResponseWriter, r *http.Request, err error) {
	i.logger.Error(fmt.Sprintf("failed internal API request to %s try again later. service api down/unreachable. check A1 error at https://docs.nlx.io/support/common-errors/", r.URL.String()), err)

	inway_http.WriteError(w, httperrors.A1, httperrors.ServiceUnreachable(r.URL.String()))
}
