#!/bin/sh
# Copyright © VNG Realisatie 2022
# Licensed under the EUPL

set -e

# ORG CERTIFICATES
# Create root CA certificates
cfssl gencert -initca ca-root-csr.json | cfssljson -bare ca-root
cfssl gencert -initca ca-root-second-csr.json | cfssljson -bare ca-root-second

# Create intermediate CA certificate
cfssl gencert -initca ca-intermediate-csr.json | cfssljson -bare ca-intermediate
cfssl sign -ca ca-root.pem -ca-key ca-root-key.pem -config ca-config.json -profile intermediate ca-intermediate.csr | cfssljson -bare ca-intermediate

# Create organisation certificates
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-nlx-test-a-csr.json | cfssljson -bare org-nlx-test-a
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-nlx-test-a-csr-ec.json | cfssljson -bare org-nlx-test-a-ec
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-nlx-test-b-csr.json | cfssljson -bare org-nlx-test-b
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-nlx-test-c-csr.json | cfssljson -bare org-nlx-test-c
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-nlx-test-d-csr.json | cfssljson -bare org-nlx-test-d
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-nlx-test-directory-csr.json | cfssljson -bare org-nlx-test-directory
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-without-name-csr.json | cfssljson -bare org-without-name
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-without-serial-number-csr.json | cfssljson -bare org-without-serial-number
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-on-crl-csr.json | cfssljson -bare org-on-crl
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config-low-expiration.json -profile=peer org-nlx-test-b-with-expired-cert-csr.json | cfssljson -bare org-nlx-test-b-lower-expiration

# Combine certificates
cat org-nlx-test-a.pem ca-intermediate.pem > org-nlx-test-a-chain.pem
cat org-nlx-test-a-ec.pem ca-intermediate.pem > org-nlx-test-a-ec-chain.pem
cat org-nlx-test-b.pem ca-intermediate.pem > org-nlx-test-b-chain.pem
cat org-nlx-test-c.pem ca-intermediate.pem > org-nlx-test-c-chain.pem
cat org-nlx-test-d.pem ca-intermediate.pem > org-nlx-test-d-chain.pem
cat org-nlx-test-directory.pem ca-intermediate.pem > org-nlx-test-directory-chain.pem
cat org-without-name.pem ca-intermediate.pem > org-without-name-chain.pem
cat org-without-serial-number.pem ca-intermediate.pem > org-without-serial-number-chain.pem
cat org-on-crl.pem ca-intermediate.pem > org-on-crl-chain.pem
cat org-nlx-test-b-lower-expiration.pem ca-intermediate.pem > org-nlx-test-b-lower-expiration-chain.pem

# Create CRL                                                             validity of the CRL in seconds (20years)
cfssl certinfo -cert testing/pki/org-on-crl.pem | jq -r .serial_number >> testing/pki/crl-serials.txt
cfssl gencrl crl-serials.txt ca-intermediate.pem ca-intermediate-key.pem 631139040 | base64 -d > ca.crl

# INTERNAL CERTIFICATES
cd internal

# Create root CA certificates
cfssl gencert -initca ca-root-csr.json | cfssljson -bare ca-root
cfssl gencert -initca ca-root-second-csr.json | cfssljson -bare ca-root-second

# Create intermediate CA certificate
cfssl gencert -initca ca-intermediate-csr.json | cfssljson -bare ca-intermediate
cfssl sign -ca ca-root.pem -ca-key ca-root-key.pem -config ca-config.json -profile intermediate ca-intermediate.csr | cfssljson -bare ca-intermediate

# Create organisation certificates
cfssl gencert -ca=ca-intermediate.pem -ca-key=ca-intermediate-key.pem -config=ca-config.json -profile=peer org-nlx-test-internal-csr.json | cfssljson -bare org-nlx-test-internal

# Combine certificates
cat org-nlx-test-internal.pem ca-intermediate.pem > org-nlx-test-internal-chain.pem
