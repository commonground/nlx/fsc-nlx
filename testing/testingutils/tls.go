// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package testingutils

import (
	"fmt"
	"path/filepath"

	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
)

type CertificateBundlePeerName string

const (
	NLXTestPeerA           CertificateBundlePeerName = "org-nlx-test-a"
	NLXTestPeerB           CertificateBundlePeerName = "org-nlx-test-b"
	NLXTestPeerC           CertificateBundlePeerName = "org-nlx-test-c"
	NLXTestPeerD           CertificateBundlePeerName = "org-nlx-test-d"
	NLXTestOrgDirectory    CertificateBundlePeerName = "org-nlx-test-directory"
	OrgWithoutName         CertificateBundlePeerName = "org-without-name"
	OrgWithoutSerialNumber CertificateBundlePeerName = "org-without-serial-number"
	NLXTestInternal        CertificateBundlePeerName = "internal/org-nlx-test-internal"
	OrgOnCRL               CertificateBundlePeerName = "org-on-crl"
)

func GetCertificateBundle(pkiDir string, name CertificateBundlePeerName) (*common_tls.CertificateBundle, error) {
	rootPEM := "ca-root.pem"

	if name == NLXTestInternal {
		rootPEM = "internal/ca-root.pem"
	}

	return common_tls.NewBundleFromFiles(
		filepath.Join(pkiDir, fmt.Sprintf("%s-chain.pem", name)),
		filepath.Join(pkiDir, fmt.Sprintf("%s-key.pem", name)),
		filepath.Join(pkiDir, rootPEM),
	)
}
