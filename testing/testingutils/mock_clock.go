// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package testingutils

import "time"

type MockClock struct {
	t time.Time
}

// NewMockClock creates a mock clock that can be used in tests
func NewMockClock(mockTime time.Time) *MockClock {
	return &MockClock{t: mockTime}
}

// Now will always return fixed time that's passed into the NewMock function
func (c *MockClock) Now() time.Time {
	return c.t
}

func (c *MockClock) UpdateTime(t time.Time) {
	c.t = t
}
