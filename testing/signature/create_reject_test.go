// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // this is a test
package testsignature_test

import (
	"crypto"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	testsignature "gitlab.com/commonground/nlx/fsc-nlx/testing/signature"
)

func TestCreateReject(t *testing.T) {
	type args struct {
		contentHash           string
		certificateThumbprint string
		privateKey            crypto.PrivateKey
		signedAt              time.Time
	}

	tests := map[string]struct {
		args    args
		want    string
		wantErr bool
	}{
		"happy_flow": {
			args: args{
				contentHash:           "my-content-hash",
				certificateThumbprint: getCertBundle(t).CertificateThumbprint(),
				privateKey:            getCertBundle(t).Cert().PrivateKey,
				signedAt:              time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC),
			},
			want:    "eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiUXBkR0Y1MTVCdkVnbVhYcmI1OEptcEloV3ZQRG5JT2NCSzNzR2dBdVZyTSJ9.eyJjb250ZW50SGFzaCI6Im15LWNvbnRlbnQtaGFzaCIsInR5cGUiOiJyZWplY3QiLCJzaWduZWRBdCI6MTY3MjYyODY0NX0.HMWl4SYREsnhtSKUsAGhDTXkZOvulTzCCEBciVZrsvrkWceVy21zdd1iRGwOJ_HiDa4thhW8fJCiShzSruegRskI3vBXk3IISfy9aasNIh7vra01Q_hwKn9FEQZaFWCq8AahDUMJLxjqf5u0k7rMWZq-Y7z48x1R0C5I_B3N8aslit2aWuReRnYufdQQcCrWmwCxhsEZ-fWZ2xPuwLctmg2VLyIQMFWEoJH0F9IWs3MIvw5MYbOB6mcnQ8nrJj0CZgiVpvFIuMgW3rK0EAwHUGQOiBHiE80zf8ed7koQeQ5_04HkPMdjhVi_jrrVABK7vSE2-I9bCS8NQlqW3Q3IoCQ4x3iN8kA9LN6bBHnnUvtUrXlQgeobkvMdeD4TnYvaB5INDN0Ec8KyGsXnl9zgBRuL9NjEpfzy1d7s3osgERpKxwaxYAl-Lsx4eler54fwjxm6pTeQvJ_h_bu8fqBs29wKdoLE724S-7wm20hrIaXTqSIU5kgoUTff-gQt46D8UU30WL8G-NFLZa5gp7uZ0QQCAEK1_1q4jig01KFDnHmFE2KtiC51hSfXsK2wI32JwnyITWF4ccuMHOmS0hiG3kdP6AU0cUUuXc_M-87oPzlxQcrEcaAuVxO1J1DZ_3kHvQYe4ChxoXWdPjMJUNzpZ27pUrLEy07Vemv0DHuom1g",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testsignature.CreateReject(tt.args.contentHash, tt.args.certificateThumbprint, tt.args.privateKey, tt.args.signedAt)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateReject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
