// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // this is a test
package testsignature_test

import (
	"crypto"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	testsignature "gitlab.com/commonground/nlx/fsc-nlx/testing/signature"
)

func TestCreateAccept(t *testing.T) {
	type args struct {
		contentHash           string
		certificateThumbprint string
		privateKey            crypto.PrivateKey
		signedAt              time.Time
	}

	tests := map[string]struct {
		args    args
		want    string
		wantErr bool
	}{
		"happy_flow": {
			args: args{
				contentHash:           "my-content-hash",
				certificateThumbprint: getCertBundle(t).CertificateThumbprint(),
				privateKey:            getCertBundle(t).Cert().PrivateKey,
				signedAt:              time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC),
			},
			want:    "eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiUXBkR0Y1MTVCdkVnbVhYcmI1OEptcEloV3ZQRG5JT2NCSzNzR2dBdVZyTSJ9.eyJjb250ZW50SGFzaCI6Im15LWNvbnRlbnQtaGFzaCIsInR5cGUiOiJhY2NlcHQiLCJzaWduZWRBdCI6MTY3MjYyODY0NX0.XOvCmnI1clPp2u77DIDgqMUeCupCatZOe7zOhzTyjSvM1XE2IxLadYBX2EOICAKd4zZGPxNYkotdD_SSld6jLqnFjC1HhO1WGkVKOLSxp9jC4VTNrAAtatyJQfVlMa4NucHP9hnWKw38UG9-VGlyaEgKinQH94EO6XGQqq0rMHOiJ6VIWq2VhwXMvW_vwz3pciYKS1pX1e4Ts4VNQQK5muQtoQRomMNFhfUS-NpaPvE0Q5UvtbHPEAId6P_MrsW-5ms7WfxAkBUsZVkV48dp7m8_l6kQDXzx7qWDO_YJYmDmw3G0dACXEqzRCQZz0dRjBjuFVsqg2j1udXaveItbF9P1RiCV5HYal9hwcuuFi8DwF_nTP3k4tJT7Il8nh_ExKXCiYdWly6wQzMErHCaWyd0BgvHBjX3FWB3eQ6nW651xEqQdaHLpKyx8zbWE-ay3JmW-VSU2qxZqkb2rW6AYIQxsHsv5xoyNUluyUVQCyOcqcypv6M1MKUZW1WrIfedYmc8YUlChIZNWg4biKkqKnHSlH0kaS-jWNG7_HEs2JGc28qSsk386DqAuBz0hiyt_23PRblnNZPO2Duvm51_X1ZIDywQ3KoMI7Hk7WHwZRWCFcUkdH6IpsGsrt1U9x9m4j6N-ZvhwjgoxPx_Q53ZXjULJQLkKYAlGtCNsre7T-Lw",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testsignature.CreateAccept(tt.args.contentHash, tt.args.certificateThumbprint, tt.args.privateKey, tt.args.signedAt)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateAccept() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
