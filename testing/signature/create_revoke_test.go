// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // this is a test
package testsignature_test

import (
	"crypto"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	testsignature "gitlab.com/commonground/nlx/fsc-nlx/testing/signature"
)

func TestCreateRevoke(t *testing.T) {
	type args struct {
		contentHash           string
		certificateThumbprint string
		privateKey            crypto.PrivateKey
		signedAt              time.Time
	}

	tests := map[string]struct {
		args    args
		want    string
		wantErr bool
	}{
		"happy_flow": {
			args: args{
				contentHash:           "my-content-hash",
				certificateThumbprint: getCertBundle(t).CertificateThumbprint(),
				privateKey:            getCertBundle(t).Cert().PrivateKey,
				signedAt:              time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC),
			},
			want:    "eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiUXBkR0Y1MTVCdkVnbVhYcmI1OEptcEloV3ZQRG5JT2NCSzNzR2dBdVZyTSJ9.eyJjb250ZW50SGFzaCI6Im15LWNvbnRlbnQtaGFzaCIsInR5cGUiOiJyZXZva2UiLCJzaWduZWRBdCI6MTY3MjYyODY0NX0.sieWPOgADVmhHp8_1l0WlkciEGhoI-HIjGXoeVl4R9Hj1W50oZDkz0wk0RZtlTmB1wO1pCByZ9QC9puACGMlZ5H0zVg43GA7lNY5UCXH7q7xH8-fDnriag1FZfIkOXynP_BgfrnG-P8J427ctU6GvFyT_2pINIptxGmt49T2MOttiIDvIgLmykZ8TaaUmzn8-XaX1S2kRHgD1jwWSXdmMtAtiDw4FJMSt7dUuCc94PK3bAD3hGCdTiEnUrwYYJnmx7hWBAWZJ81VYJ6OJzvB8oizW_-didxoRlG2ChHvG2zxGiu9uAE5ct79XclIXiUHDHpaiwKznDLK5pbUBP6LaxfgUA3XrjIEatv_-p0VY6IhS9IwjY8hLvQb7nhzd_eSjaWb1Xb86VjPHd3tWRJXyWdRafXNNqpUutq0knndtAIEBG6h1U2f8QtDNSZF-WIZQC_bikEP0ek_6U1hkkQxLX4dtLhqbC1VnE57WceSzUkyLgqJ7BWMxd8Ma8Oig8Z1B1ojwjfjYOVylYgXo_uOuh9J5I2AmC-47AB_QkJZsoxnFHAbhtCY1CFEY8LscXqZQHuyDex7AE8UqzbgJ5i5s95dPjc9fyc6WJc5RBW35LEfD2PUQ_R8IInp3OrtOdEgvsCX3IqVWI_2hRuL_icY5qPeo8WVZl8GLQRrKSRhZrU",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testsignature.CreateRevoke(tt.args.contentHash, tt.args.certificateThumbprint, tt.args.privateKey, tt.args.signedAt)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateRevoke() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
