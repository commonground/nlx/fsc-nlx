// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

// nolint:dupl // intentionally added all logic into a single function
package testhash

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"sort"

	"github.com/google/uuid"
	"golang.org/x/crypto/sha3"
)

type DelegatedServiceConnectionGrantArgs struct {
	ContractContentIV                 string
	ContractGroupID                   string
	ContractHashAlgorithm             int32
	DelegatorPeerID                   string
	OutwayPeerID                      string
	OutwayCertificateThumbprint       string
	ServicePeerID                     string
	ServiceName                       string
	ServicePublicationDelegatorPeerID string
}

func GenerateDelegatedServiceConnectionGrantHash(args *DelegatedServiceConnectionGrantArgs) (string, error) {
	ivAsUUID, err := uuid.Parse(args.ContractContentIV)
	if err != nil {
		return "", ErrInvalidUUID
	}

	ivAsBytes, err := ivAsUUID.MarshalBinary()
	if err != nil {
		return "", err
	}

	contentBytes := make([]byte, 0)

	contentBytes = append(contentBytes, []byte(args.ContractGroupID)...)
	contentBytes = append(contentBytes, ivAsBytes...)

	grantByteArrays := make([][]byte, 0)

	grantTypeDelegatedServiceConnection := 3

	grantBytes := make([]byte, 0)
	grantBytes = append(grantBytes, bytesFromInt32(int32(grantTypeDelegatedServiceConnection))...)
	grantBytes = append(grantBytes, []byte(args.OutwayPeerID)...)
	grantBytes = append(grantBytes, []byte(args.OutwayCertificateThumbprint)...)

	if args.ServicePublicationDelegatorPeerID == "" {
		serviceTypeService := 1
		grantBytes = append(grantBytes, bytesFromInt32(int32(serviceTypeService))...)
	} else {
		serviceTypeDelegatedService := 2
		grantBytes = append(grantBytes, bytesFromInt32(int32(serviceTypeDelegatedService))...)
	}

	grantBytes = append(grantBytes, []byte(args.ServicePeerID)...)
	grantBytes = append(grantBytes, []byte(args.ServiceName)...)

	if args.ServicePublicationDelegatorPeerID != "" {
		grantBytes = append(grantBytes, []byte(args.ServicePublicationDelegatorPeerID)...)
	}

	grantBytes = append(grantBytes, []byte(args.DelegatorPeerID)...)

	grantByteArrays = append(grantByteArrays, grantBytes)

	sort.Slice(grantByteArrays, func(i, j int) bool {
		return bytes.Compare(grantByteArrays[i], grantByteArrays[j]) < 0
	})

	for _, grantByteArray := range grantByteArrays {
		contentBytes = append(contentBytes, grantByteArray...)
	}

	h := sha3.Sum512(contentBytes)
	hashedBytes := h[:]

	base64EncodedHash := base64.RawURLEncoding.EncodeToString(hashedBytes)

	algorithm := fmt.Sprintf("$%d$", args.ContractHashAlgorithm)
	hashType := "4$"
	contentHash := fmt.Sprintf("%s%s%s", algorithm, hashType, base64EncodedHash)

	return contentHash, nil
}
