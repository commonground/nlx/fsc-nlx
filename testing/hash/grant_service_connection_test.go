// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testhash_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

func TestGenerateServiceConnectionGrantHash(t *testing.T) {
	const sha3_512 = 1

	tests := map[string]struct {
		args    *testhash.ServiceConnectionGrantArgs
		want    string
		wantErr bool
	}{
		"invalid_uuid": {
			args: &testhash.ServiceConnectionGrantArgs{
				ContractContentIV:           "invalid",
				ContractGroupID:             "fsc-test",
				ContractHashAlgorithm:       sha3_512,
				OutwayPeerID:                "12345678901234567899",
				OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:               "12345678901234567890",
				ServiceName:                 "parkeerrechten",
			},
			want:    "",
			wantErr: true,
		},
		"happy_flow_service_type_service": {
			args: &testhash.ServiceConnectionGrantArgs{
				ContractContentIV:           "495d29af-8004-4add-a329-9c0327d10f05",
				ContractGroupID:             "fsc-test",
				ContractHashAlgorithm:       sha3_512,
				OutwayPeerID:                "12345678901234567899",
				OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:               "12345678901234567890",
				ServiceName:                 "parkeerrechten",
			},
			want:    "$1$3$AFCvs4MlJZwsWdqrOKNN-HuZppTG7bBE96vUeaAGiLl-YKu9FwG-Eh4CE5cq26L6nlC0KMNfjC211nrrC6_z-A",
			wantErr: false,
		},
		"happy_flow_service_type_delegated_service": {
			args: &testhash.ServiceConnectionGrantArgs{
				ContractContentIV:                 "495d29af-8004-4add-a329-9c0327d10f05",
				ContractGroupID:                   "fsc-test",
				ContractHashAlgorithm:             sha3_512,
				OutwayPeerID:                      "12345678901234567899",
				OutwayCertificateThumbprint:       "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:                     "12345678901234567890",
				ServiceName:                       "parkeerrechten",
				ServicePublicationDelegatorPeerID: "12345678901234567891",
			},
			want:    "$1$3$EnUVtfw6pE4xO0hDfflpyp4OK7fSfW48Pb-YZS0ot2I2-afnqhQYlMt7DIIN8g7D-Egcs-_MoRBGSX1DPnLEiw",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testhash.GenerateServiceConnectionGrantHash(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateServiceConnectionGrantHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
