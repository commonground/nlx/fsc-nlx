// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

// nolint:dupl // intentionally added all logic into a single function
package testhash

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"sort"

	"github.com/google/uuid"
	"golang.org/x/crypto/sha3"
)

type ServicePublicationGrantArgs struct {
	ContractContentIV     string
	ContractGroupID       string
	ContractHashAlgorithm int32
	DirectoryPeerID       string
	ServicePeerID         string
	ServiceName           string
	ServiceProtocol       string
}

func GenerateServicePublicationGrantHash(args *ServicePublicationGrantArgs) (string, error) {
	ivAsUUID, err := uuid.Parse(args.ContractContentIV)
	if err != nil {
		return "", ErrInvalidUUID
	}

	ivAsBytes, err := ivAsUUID.MarshalBinary()
	if err != nil {
		return "", err
	}

	contentBytes := make([]byte, 0)

	contentBytes = append(contentBytes, []byte(args.ContractGroupID)...)
	contentBytes = append(contentBytes, ivAsBytes...)

	grantByteArrays := make([][]byte, 0)

	grantBytes := make([]byte, 0)
	grantBytes = append(grantBytes, bytesFromInt32(1)...)
	grantBytes = append(grantBytes, []byte(args.DirectoryPeerID)...)
	grantBytes = append(grantBytes, []byte(args.ServicePeerID)...)
	grantBytes = append(grantBytes, []byte(args.ServiceName)...)
	grantBytes = append(grantBytes, []byte(args.ServiceProtocol)...)

	grantByteArrays = append(grantByteArrays, grantBytes)

	sort.Slice(grantByteArrays, func(i, j int) bool {
		return bytes.Compare(grantByteArrays[i], grantByteArrays[j]) < 0
	})

	for _, grantByteArray := range grantByteArrays {
		contentBytes = append(contentBytes, grantByteArray...)
	}

	h := sha3.Sum512(contentBytes)
	hashedBytes := h[:]

	base64EncodedHash := base64.RawURLEncoding.EncodeToString(hashedBytes)

	algorithm := fmt.Sprintf("$%d$", args.ContractHashAlgorithm)
	hashType := "2$"
	contentHash := fmt.Sprintf("%s%s%s", algorithm, hashType, base64EncodedHash)

	return contentHash, nil
}
