// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testhash_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

//nolint:funlen,dupl // this is a test
func TestGenerateContractContentHash(t *testing.T) {
	const sha3_512 = 1

	tests := map[string]struct {
		args    *testhash.ContractContentArgs
		want    string
		wantErr bool
	}{
		"invalid_id": {
			args: &testhash.ContractContentArgs{
				IV:                      "invalid",
				GroupID:                 "fsc-test",
				HashAlgorithm:           sha3_512,
				ValidNotBefore:          time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:           time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{},
				CreatedAt:               time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "",
			wantErr: true,
		},
		"happy_flow_service_publication_grant": {
			args: &testhash.ContractContentArgs{
				IV:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ServicePublicationGrants: []testhash.ContractContentServicePublicationGrantArgs{
					{
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567890",
						ServiceName:     "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$Emm9bnohULAEIVuAt3VMC5bc5AJnU8XY7CxAZ39LKq-CT8r2L_WZTWsdqHEyRQlFQU3-IIY2m3V92dEBqqasiw",
			wantErr: false,
		},
		"happy_flow_service_connection_grant": {
			args: &testhash.ContractContentArgs{
				IV:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
					{
						OutwayPeerID:              "12345678901234567890",
						OutwayPublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
						ServicePeerID:             "12345678901234567891",
						ServiceName:               "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$UHl-GSepnzG1hTLywVps4W3BdfkCzvVxKI8mYEBaSTGR2mNkd3q2JYH3mn51B2TwQC2HqUTxuIcI9YFObJ3Zag",
			wantErr: false,
		},
		"happy_flow_delegated_service_connection_grant": {
			args: &testhash.ContractContentArgs{
				IV:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				DelegatedServiceConnectionGrants: []testhash.ContractContentDelegatedServiceConnectionGrantArgs{
					{
						DelegatorPeerID:           "12345678901234567892",
						OutwayPeerID:              "12345678901234567890",
						OutwayPublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
						ServicePeerID:             "12345678901234567891",
						ServiceName:               "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$VIPwd5m5JFifJDaQptCpwywGDvYfSsr2YZ-lpsDRE6L2vpGHjWbU1i1nNgJQKOXU3RGw4gdqU7gBoy54_qzWmQ",
			wantErr: false,
		},
		"happy_flow_delegated_service_publication_grant": {
			args: &testhash.ContractContentArgs{
				IV:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				DelegatedServicePublicationGrants: []testhash.ContractContentDelegatedServicePublicationGrantArgs{
					{
						DelegatorPeerID: "12345678901234567891",
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567892",
						ServiceName:     "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$ImhCf0o4X9Q4qMfEdcY9duQD8aljrltxVsKTtGOZRumZiTWUyKyZheVm4UvH2WYOt06-j5BPsDj-UT2OTY0umQ",
			wantErr: false,
		},
		"happy_flow_combination": {
			args: &testhash.ContractContentArgs{
				IV:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ServicePublicationGrants: []testhash.ContractContentServicePublicationGrantArgs{
					{
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567890",
						ServiceName:     "parkeerrechten",
					},
				},
				ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
					{
						OutwayPeerID:              "12345678901234567890",
						OutwayPublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
						ServicePeerID:             "12345678901234567891",
						ServiceName:               "parkeerrechten",
					},
				},
				DelegatedServiceConnectionGrants: []testhash.ContractContentDelegatedServiceConnectionGrantArgs{
					{
						DelegatorPeerID:           "12345678901234567892",
						OutwayPeerID:              "12345678901234567890",
						OutwayPublicKeyThumbprint: "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
						ServicePeerID:             "12345678901234567891",
						ServiceName:               "parkeerrechten",
					},
				},
				DelegatedServicePublicationGrants: []testhash.ContractContentDelegatedServicePublicationGrantArgs{
					{
						DelegatorPeerID: "12345678901234567891",
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567892",
						ServiceName:     "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$RA-piqM1EeYh97fXdVOJ7vyHkQZHStFY7J3p-9b25V6IMaaEFDyV7yHHF3SUlqLqZhyET8HWlWEeTIff625VaQ",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testhash.GenerateContractContentHash(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateContractContentHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
