// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testhash_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

func TestGenerateDelegatedServicePublicationGrantHash(t *testing.T) {
	const sha3_512 = 1

	tests := map[string]struct {
		args    *testhash.DelegatedServicePublicationGrantArgs
		want    string
		wantErr bool
	}{
		"happy_flow": {
			args: &testhash.DelegatedServicePublicationGrantArgs{
				ContractContentIV:     "495d29af-8004-4add-a329-9c0327d10f05",
				ContractGroupID:       "fsc-test",
				ContractHashAlgorithm: sha3_512,
				DelegatorPeerID:       "12345678901234567891",
				DirectoryPeerID:       "12345678901234567899",
				ServicePeerID:         "12345678901234567890",
				ServiceName:           "parkeerrechten",
				ServiceProtocol:       "PROTOCOL_TCP_HTTP_1.1",
			},
			want:    "$1$5$WaYnMo5OhU8rzBTydUJf6BKoguYbzgKQL51nu7JPEwtpl9Hl2EvRpQtQFi2wO7RpedQfTlivDqH-M1kzadawjw",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testhash.GenerateDelegatedServicePublicationGrantHash(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateDelegatedServicePublicationGrantHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
