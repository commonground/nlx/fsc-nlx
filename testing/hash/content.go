// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

// nolint:dupl // some grants are similar but not equal
package testhash

import (
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"sort"

	"github.com/google/uuid"
	"golang.org/x/crypto/sha3"
)

type ContractContentArgs struct {
	IV                                string
	GroupID                           string
	HashAlgorithm                     int32
	ValidNotBefore                    int64
	ValidNotAfter                     int64
	ServicePublicationGrants          []ContractContentServicePublicationGrantArgs
	ServiceConnectionGrants           []ContractContentServiceConnectionGrantArgs
	DelegatedServiceConnectionGrants  []ContractContentDelegatedServiceConnectionGrantArgs
	DelegatedServicePublicationGrants []ContractContentDelegatedServicePublicationGrantArgs
	CreatedAt                         int64
}

type ContractContentServicePublicationGrantArgs struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol string
}

type ContractContentServiceConnectionGrantArgs struct {
	OutwayPeerID              string
	OutwayPublicKeyThumbprint string
	ServicePeerID             string
	ServiceName               string
}

type ContractContentDelegatedServiceConnectionGrantArgs struct {
	DelegatorPeerID           string
	OutwayPeerID              string
	OutwayPublicKeyThumbprint string
	ServicePeerID             string
	ServiceName               string
}

type ContractContentDelegatedServicePublicationGrantArgs struct {
	DelegatorPeerID string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

var (
	ErrInvalidUUID = errors.New("failed to parse iv")
)

func GenerateContractContentHash(args *ContractContentArgs) (string, error) {
	ivAsUUID, err := uuid.Parse(args.IV)
	if err != nil {
		return "", ErrInvalidUUID
	}

	ivAsBytes, err := ivAsUUID.MarshalBinary()
	if err != nil {
		return "", err
	}

	contentBytes := make([]byte, 0)

	contentBytes = append(contentBytes, []byte(args.GroupID)...)
	contentBytes = append(contentBytes, ivAsBytes...)
	contentBytes = append(contentBytes, bytesFromInt64(args.ValidNotBefore)...)
	contentBytes = append(contentBytes, bytesFromInt64(args.ValidNotAfter)...)
	contentBytes = append(contentBytes, bytesFromInt64(args.CreatedAt)...)

	grantByteArrays, err := convertGrantsToByteArrays(args)
	if err != nil {
		return "", err
	}

	sort.Slice(grantByteArrays, func(i, j int) bool {
		return bytes.Compare(grantByteArrays[i], grantByteArrays[j]) < 0
	})

	for _, grantByteArray := range grantByteArrays {
		contentBytes = append(contentBytes, grantByteArray...)
	}

	h := sha3.Sum512(contentBytes)
	hashedBytes := h[:]

	base64EncodedHash := base64.RawURLEncoding.EncodeToString(hashedBytes)
	algorithm := fmt.Sprintf("$%d$", args.HashAlgorithm)
	hashType := "1$"

	contentHash := fmt.Sprintf("%s%s%s", algorithm, hashType, base64EncodedHash)

	return contentHash, nil
}

func convertGrantsToByteArrays(args *ContractContentArgs) ([][]byte, error) {
	grantByteArrays := make([][]byte, 0)

	spGrantHashes, err := getServicePublicationGrantHashes(args)
	if err != nil {
		return nil, err
	}

	grantByteArrays = append(grantByteArrays, spGrantHashes...)

	scGrantHashes, err := getServiceConnectionGrantHashes(args)
	if err != nil {
		return nil, err
	}

	grantByteArrays = append(grantByteArrays, scGrantHashes...)

	dscGrantHashes, err := getDelegatedServiceConnectionGrantHashes(args)
	if err != nil {
		return nil, err
	}

	grantByteArrays = append(grantByteArrays, dscGrantHashes...)

	dspGrantHashes, err := getDelegatedServicePublicationGrantHashes(args)
	if err != nil {
		return nil, err
	}

	grantByteArrays = append(grantByteArrays, dspGrantHashes...)

	return grantByteArrays, nil
}

func getServicePublicationGrantHashes(args *ContractContentArgs) ([][]byte, error) {
	result := make([][]byte, 0)

	for _, spGrant := range args.ServicePublicationGrants {
		grantHash, err := GenerateServicePublicationGrantHash(&ServicePublicationGrantArgs{
			ContractContentIV:     args.IV,
			ContractGroupID:       args.GroupID,
			ContractHashAlgorithm: args.HashAlgorithm,
			DirectoryPeerID:       spGrant.DirectoryPeerID,
			ServicePeerID:         spGrant.ServicePeerID,
			ServiceName:           spGrant.ServiceName,
			ServiceProtocol:       spGrant.ServiceProtocol,
		})
		if err != nil {
			return nil, errors.Join(err, errors.New("failed to generate service publication grant hash"))
		}

		result = append(result, []byte(grantHash))
	}

	return result, nil
}

func getServiceConnectionGrantHashes(args *ContractContentArgs) ([][]byte, error) {
	result := make([][]byte, 0)

	for _, scGrant := range args.ServiceConnectionGrants {
		grantHash, err := GenerateServiceConnectionGrantHash(&ServiceConnectionGrantArgs{
			ContractContentIV:           args.IV,
			ContractGroupID:             args.GroupID,
			ContractHashAlgorithm:       args.HashAlgorithm,
			OutwayPeerID:                scGrant.OutwayPeerID,
			OutwayCertificateThumbprint: scGrant.OutwayPublicKeyThumbprint,
			ServicePeerID:               scGrant.ServicePeerID,
			ServiceName:                 scGrant.ServiceName,
		})
		if err != nil {
			return nil, errors.Join(err, errors.New("failed to generate service connection grant hash"))
		}

		result = append(result, []byte(grantHash))
	}

	return result, nil
}

func getDelegatedServiceConnectionGrantHashes(args *ContractContentArgs) ([][]byte, error) {
	result := make([][]byte, 0)

	for _, dscGrant := range args.DelegatedServiceConnectionGrants {
		grantHash, err := GenerateDelegatedServiceConnectionGrantHash(&DelegatedServiceConnectionGrantArgs{
			ContractContentIV:           args.IV,
			ContractGroupID:             args.GroupID,
			ContractHashAlgorithm:       args.HashAlgorithm,
			DelegatorPeerID:             dscGrant.DelegatorPeerID,
			OutwayPeerID:                dscGrant.OutwayPeerID,
			OutwayCertificateThumbprint: dscGrant.OutwayPublicKeyThumbprint,
			ServicePeerID:               dscGrant.ServicePeerID,
			ServiceName:                 dscGrant.ServiceName,
		})
		if err != nil {
			return nil, errors.Join(err, errors.New("failed to generate delegated service connection grant hash"))
		}

		result = append(result, []byte(grantHash))
	}

	return result, nil
}

func getDelegatedServicePublicationGrantHashes(args *ContractContentArgs) ([][]byte, error) {
	result := make([][]byte, 0)

	for _, dspGrant := range args.DelegatedServicePublicationGrants {
		grantHash, err := GenerateDelegatedServicePublicationGrantHash(&DelegatedServicePublicationGrantArgs{
			ContractContentIV:     args.IV,
			ContractGroupID:       args.GroupID,
			ContractHashAlgorithm: args.HashAlgorithm,
			DelegatorPeerID:       dspGrant.DelegatorPeerID,
			DirectoryPeerID:       dspGrant.DirectoryPeerID,
			ServicePeerID:         dspGrant.ServicePeerID,
			ServiceName:           dspGrant.ServiceName,
		})
		if err != nil {
			return nil, errors.Join(err, errors.New("failed to generate delegated service publication grant hash"))
		}

		result = append(result, []byte(grantHash))
	}

	return result, nil
}

const int64Bytes = 8
const int32Bytes = 4

func bytesFromInt64(i int64) []byte {
	b := make([]byte, int64Bytes)
	binary.LittleEndian.PutUint64(b, uint64(i))

	return b
}

func bytesFromInt32(i int32) []byte {
	b := make([]byte, int32Bytes)
	binary.LittleEndian.PutUint32(b, uint32(i))

	return b
}
