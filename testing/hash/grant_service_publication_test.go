// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testhash_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

func TestGenerateServicePublicationGrantHash(t *testing.T) {
	const sha3_512 = 1

	tests := map[string]struct {
		args    *testhash.ServicePublicationGrantArgs
		want    string
		wantErr bool
	}{
		"happy_flow": {
			args: &testhash.ServicePublicationGrantArgs{
				ContractContentIV:     "495d29af-8004-4add-a329-9c0327d10f05",
				ContractGroupID:       "fsc-test",
				ContractHashAlgorithm: sha3_512,
				DirectoryPeerID:       "12345678901234567899",
				ServicePeerID:         "12345678901234567890",
				ServiceName:           "parkeerrechten",
				ServiceProtocol:       "PROTOCOL_TCP_HTTP_1.1",
			},
			want:    "$1$2$J7wMVYPafesGwNhy2WTqPWvePCdUlrNFF7KEeRbgOJOriTNpIe-4p5rbbJ_QqCSTCTF1CS4U1p_pyl6G5V1-ZA",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testhash.GenerateServicePublicationGrantHash(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateServicePublicationGrantHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
