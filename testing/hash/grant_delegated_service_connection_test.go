// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testhash_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

func TestGenerateDelegatedServiceConnectionGrantHash(t *testing.T) {
	const sha3_512 = 1

	tests := map[string]struct {
		args    *testhash.DelegatedServiceConnectionGrantArgs
		want    string
		wantErr bool
	}{
		"invalid_uuid": {
			args: &testhash.DelegatedServiceConnectionGrantArgs{
				ContractContentIV:           "invalid",
				ContractGroupID:             "fsc-test",
				ContractHashAlgorithm:       sha3_512,
				DelegatorPeerID:             "12345678901234567891",
				OutwayPeerID:                "12345678901234567899",
				OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:               "12345678901234567890",
				ServiceName:                 "parkeerrechten",
			},
			want:    "",
			wantErr: true,
		},
		"happy_flow_service_type_service": {
			args: &testhash.DelegatedServiceConnectionGrantArgs{
				ContractContentIV:           "495d29af-8004-4add-a329-9c0327d10f05",
				ContractGroupID:             "fsc-test",
				ContractHashAlgorithm:       sha3_512,
				DelegatorPeerID:             "12345678901234567891",
				OutwayPeerID:                "12345678901234567899",
				OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:               "12345678901234567890",
				ServiceName:                 "parkeerrechten",
			},
			want:    "$1$4$ueMHsWiCMXQ5L-o7eReEEBPsgvLOEZLLfIZEAb29IIaHceTSweFEKS3eszVopzgX3JQwJnyr6zwKVFH66EdWgg",
			wantErr: false,
		},
		"happy_flow_service_type_delegated_service": {
			args: &testhash.DelegatedServiceConnectionGrantArgs{
				ContractContentIV:                 "495d29af-8004-4add-a329-9c0327d10f05",
				ContractGroupID:                   "fsc-test",
				ContractHashAlgorithm:             sha3_512,
				DelegatorPeerID:                   "12345678901234567891",
				OutwayPeerID:                      "12345678901234567899",
				OutwayCertificateThumbprint:       "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:                     "12345678901234567890",
				ServiceName:                       "parkeerrechten",
				ServicePublicationDelegatorPeerID: "12345678901234567892",
			},
			want:    "$1$4$FP_vzT7_9qqTVFjT9uhyGxCUsbMKGDOs-9dkCcnrm7KnYRdaCE7WIlD0p9LXSz3V0nQGOiL3GGld_T9gOePxmQ",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testhash.GenerateDelegatedServiceConnectionGrantHash(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateDelegatedServiceConnectionGrantHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
