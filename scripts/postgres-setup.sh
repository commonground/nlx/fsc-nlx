#!/bin/sh
# Copyright © VNG Realisatie 2022
# Licensed under the EUPL


psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE DATABASE nlx_txlog_directory;
    CREATE DATABASE nlx_txlog_a;
    CREATE DATABASE nlx_txlog_b;
    CREATE DATABASE nlx_txlog_c;
    CREATE DATABASE nlx_txlog_d;
    CREATE DATABASE nlx_manager_directory;
    CREATE DATABASE nlx_manager_org_a;
    CREATE DATABASE nlx_manager_org_b;
    CREATE DATABASE nlx_manager_org_c;
    CREATE DATABASE nlx_manager_org_d;
    CREATE DATABASE nlx_controller_directory;
    CREATE DATABASE nlx_controller_org_a;
    CREATE DATABASE nlx_controller_org_b;
    CREATE DATABASE nlx_controller_org_c;
    CREATE DATABASE nlx_controller_org_d;
    CREATE DATABASE nlx_auditlog_org_a;
    CREATE DATABASE nlx_auditlog_org_b;
    CREATE DATABASE nlx_auditlog_org_c;
    CREATE DATABASE nlx_auditlog_org_d;
EOSQL
