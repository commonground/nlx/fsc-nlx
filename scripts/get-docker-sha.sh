#!/bin/bash
# Copyright © VNG Realisatie 2024
# Licensed under the EUPL

docker buildx imagetools inspect "${1}" --format "{{json .Manifest}}" | jq -r .digest
