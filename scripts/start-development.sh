#!/usr/bin/env bash
# Copyright © VNG Realisatie 2022
# Licensed under the EUPL

# Run this script from the root folder of the git repository with the following command: ./scripts/start-development.sh

compose="docker compose"

if command -v docker-compose &> /dev/null
then
    compose="docker-compose"
fi

if command -v podman-compose &> /dev/null
then
    compose="podman-compose"
fi

function finish {
  $compose down --remove-orphans

  # kill all left over debug processes
  ps w | grep '.*\.debugger/.*\.bin' | awk '{print $1}' | xargs kill -9 $1
}

trap finish EXIT

# Make sure permissions for pki files are ok
sh ./pki/fix-permissions.sh &

# UI assets
npm --prefix ./directory-ui/ports/ui/assets i &
npm --prefix ./controller/ports/administration/ui/assets/ i &

# Start $compose
# Note: the --build flag is needed to rebuild the auth-opa containers,
# since changes to the content of the files do not trigger a rebuild automatically
if ! $compose up -d --build --remove-orphans; then
    echo "Error while starting ${compose}, exiting now"
    exit
fi

# Wait for postgres to accept connections
until $compose exec postgres pg_isready
do
    sleep 1;
done;

# Migrate txlog databases
go run ./txlog-api migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_txlog_directory?sslmode=disable" &
go run ./txlog-api migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_txlog_a?sslmode=disable" &
go run ./txlog-api migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_txlog_b?sslmode=disable" &
go run ./txlog-api migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_txlog_c?sslmode=disable" &
go run ./txlog-api migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_txlog_d?sslmode=disable" &

# Migrate manager database
go run ./manager migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_manager_directory?sslmode=disable" &
go run ./manager migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_manager_org_a?sslmode=disable" &
go run ./manager migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_manager_org_b?sslmode=disable" &
go run ./manager migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_manager_org_c?sslmode=disable" &
go run ./manager migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_manager_org_d?sslmode=disable" &

# Migrate nlx_controller databases
go run ./controller migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_controller_directory?sslmode=disable" &
go run ./controller migrate up --authn-type oidc --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_controller_org_a?sslmode=disable" &
go run ./controller migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_controller_org_b?sslmode=disable" &
go run ./controller migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_controller_org_c?sslmode=disable" &
go run ./controller migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_controller_org_d?sslmode=disable" &

# Migrate nlx_auditlog databases
go run ./auditlog migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_auditlog_org_a?sslmode=disable" &
go run ./auditlog migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_auditlog_org_b?sslmode=disable" &
go run ./auditlog migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_auditlog_org_c?sslmode=disable" &
go run ./auditlog migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_auditlog_org_d?sslmode=disable" &

# Wait for keycloak to have imported the clients and users
sh ./wait-for-http.sh http://keycloak.shared.nlx.localhost:8080/health/ready

wait;

# start services
npm --prefix ./directory-ui/ports/ui/assets run watch &
npm --prefix ./controller/ports/administration/ui/assets/ run watch &

modd
