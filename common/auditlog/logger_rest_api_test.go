// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import (
	"errors"
	"net/netip"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/domain/auditlog"
	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/models"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var nowInUTC = time.Now().UTC()
var testClock = testingutils.NewMockClock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))

//nolint:funlen // table tests are long
func TestTransformAuditlogRequest(t *testing.T) {
	type testcase struct {
		input      []*Record
		want       models.CreateRecordsJSONRequestBody
		wantEvent  string
		wantSource string
		wantActor  string
		wantErr    error
	}

	id, err := uuid.FromString("018dd51f-0040-751b-a0b1-386db616a20c")
	assert.NoError(t, err)

	correlationID, err := uuid.FromString("018dd522-3ac3-7f48-8377-139faf99a049")
	assert.NoError(t, err)

	testCases := map[string]testcase{
		"happy_flow_user_unauthorized": {
			input: []*Record{
				{
					ID:            ID(id),
					GroupID:       "unit-test",
					CorrelationID: CorrelationID(correlationID),
					Actor: &ActorUser{
						Name:  "testUser",
						Email: "test@example.com",
					},
					Event: EventCreateService{ServiceName: "test-service"},
					Source: SourceHTTP{
						IPAddress: netip.IPv4Unspecified(),
						UserAgent: "curl/7.54.1",
					},
					Status: StatusUnauthorized{
						Error: "user was unauthorized to perform Action",
					},
					Component: Component(auditlog.ComponentManager),
					CreatedAt: testClock.Now(),
				},
			},
			want: models.CreateRecordsJSONRequestBody{
				Records: []models.Record{
					{
						Actor:         models.Record_Actor{},
						Component:     models.COMPONENTMANAGER,
						CorrelationId: correlationID.String(),
						CreatedAt:     testClock.Now().Unix(),
						GroupId:       "unit-test",
						Id:            id.String(),
					},
				},
			},
			wantActor:  "{\"email\":\"test@example.com\",\"name\":\"testUser\",\"type\":\"ACTOR_TYPE_USER\"}",
			wantEvent:  "{\"service_name\":\"test-service\",\"type\":\"EVENT_TYPE_CREATE_SERVICE\"}",
			wantSource: "{\"ip_address\":\"0.0.0.0\",\"type\":\"SOURCE_TYPE_HTTP\",\"user_agent\":\"curl/7.54.1\"}",
			wantErr:    nil,
		},
		"happy_flow_user_acceptcontract_succeeded": {
			input: []*Record{
				{
					ID:            ID(id),
					GroupID:       "unit-test",
					CorrelationID: CorrelationID(correlationID),
					Actor: &ActorService{
						SubjectCommonName:   "testSystem",
						PublicKeyThumbprint: "test-public-key-fingerprint",
					},
					Event: EventAcceptContract{ContractHash: "test-hash"},
					Source: SourceHTTP{
						IPAddress: netip.IPv4Unspecified(),
						UserAgent: "curl/7.54.1",
					},
					Status:    StatusSucceeded{},
					Component: Component(auditlog.ComponentManager),
					CreatedAt: testClock.Now(),
				},
			},
			want: models.CreateRecordsJSONRequestBody{
				Records: []models.Record{
					{
						Actor:         models.Record_Actor{},
						Component:     models.COMPONENTMANAGER,
						CorrelationId: correlationID.String(),
						CreatedAt:     testClock.Now().Unix(),
						GroupId:       "unit-test",
						Id:            id.String(),
					},
				},
			},
			wantActor:  "{\"public_key_thumbprint\":\"test-public-key-fingerprint\",\"subject_common_name\":\"testSystem\",\"type\":\"ACTOR_TYPE_SERVICE\"}",
			wantEvent:  "{\"content_hash\":\"test-hash\",\"type\":\"EVENT_TYPE_ACCEPT_CONTRACT\"}",
			wantSource: "{\"ip_address\":\"0.0.0.0\",\"type\":\"SOURCE_TYPE_HTTP\",\"user_agent\":\"curl/7.54.1\"}",
			wantErr:    nil,
		},
		"error_invalid_actor": {
			input: []*Record{
				{
					ID:            ID(id),
					GroupID:       "unit-test",
					CorrelationID: CorrelationID(correlationID),
					Actor:         "invalidactor",
					Event:         EventAcceptContract{ContractHash: "test-hash"},
					Source: SourceHTTP{
						IPAddress: netip.IPv4Unspecified(),
						UserAgent: "curl/7.54.1",
					},
					Status:    StatusSucceeded{},
					Component: Component(auditlog.ComponentManager),
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: errors.New("could not transform Actor, unknown Actor type"),
		},
		"error_invalid_event": {
			input: []*Record{
				{
					ID:            ID(id),
					GroupID:       "unit-test",
					CorrelationID: CorrelationID(correlationID),
					Actor: &ActorService{
						SubjectCommonName:   "testSystem",
						PublicKeyThumbprint: "test-public-key-fingerprint",
					},
					Event: "invalid event",
					Source: SourceHTTP{
						IPAddress: netip.IPv4Unspecified(),
						UserAgent: "curl/7.54.1",
					},
					Status:    StatusSucceeded{},
					Component: Component(auditlog.ComponentManager),
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: errors.New("could not transform Event, unknown Event type"),
		},
		"error_invalid_source": {
			input: []*Record{
				{
					ID:            ID(id),
					GroupID:       "unit-test",
					CorrelationID: CorrelationID(correlationID),
					Actor: &ActorService{
						SubjectCommonName:   "testSystem",
						PublicKeyThumbprint: "test-public-key-fingerprint",
					},
					Event:     EventAcceptContract{ContractHash: "test-hash"},
					Source:    "invalid source",
					Status:    StatusSucceeded{},
					Component: Component(auditlog.ComponentManager),
					CreatedAt: testClock.Now(),
				},
			},
			wantErr: errors.New("could not transform Source, unknown Source type"),
		},
	}

	for k, tt := range testCases {
		tc := tt

		t.Run(k, func(t *testing.T) {
			t.Parallel()

			auditLoggerRequest, errTransform := transformAuditLoggerRequest(tc.input)

			if tc.wantErr != nil {
				assert.NotNil(t, errTransform)
				assert.ErrorContains(t, errTransform, tc.wantErr.Error())
			} else {
				assert.NoError(t, errTransform)
				assert.Equal(t, len(tc.want.Records), len(auditLoggerRequest.Records))

				for i, record := range auditLoggerRequest.Records {
					sourceString, err := record.Source.MarshalJSON()
					assert.NoError(t, err)

					eventString, err := record.Event.MarshalJSON()
					assert.NoError(t, err)

					actorString, err := record.Actor.MarshalJSON()
					assert.NoError(t, err)

					assert.Equal(t, tc.want.Records[i].GroupId, record.GroupId)
					assert.Equal(t, tc.want.Records[i].CreatedAt, record.CreatedAt)
					assert.Equal(t, tc.want.Records[i].Id, record.Id)
					assert.Equal(t, tc.want.Records[i].CorrelationId, record.CorrelationId)
					assert.Equal(t, tc.wantSource, string(sourceString))
					assert.Equal(t, tc.wantEvent, string(eventString))
					assert.Equal(t, tc.wantActor, string(actorString))
				}
			}
		})
	}
}
