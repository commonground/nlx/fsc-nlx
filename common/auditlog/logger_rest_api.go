// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import (
	"context"
	"fmt"
	"net/http"
	"net/netip"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/auditlog/ports/rest/api/server"
)

type RestAPIAuditLogger struct {
	client api.ClientWithResponsesInterface
}

func NewRestAPIAuditLogger(client api.ClientWithResponsesInterface) (*RestAPIAuditLogger, error) {
	return &RestAPIAuditLogger{client: client}, nil
}

func (l *RestAPIAuditLogger) CreateRecords(ctx context.Context, records Records) error {
	reqRecords, err := transformAuditLoggerRequest(records)
	if err != nil {
		return err
	}

	res, err := l.client.CreateRecordsWithResponse(ctx, *reqRecords)
	if err != nil {
		return fmt.Errorf("could not create Audit log records in rest api: %w", err)
	}

	if res.StatusCode() != http.StatusNoContent {
		return fmt.Errorf("could not create Audit log records in rest api, invalid status code, want: %d, got: %d", http.StatusNoContent, res.StatusCode())
	}

	return nil
}

func (l *RestAPIAuditLogger) ListRecords(ctx context.Context, args *ListRecordsArgs) (Records, error) {
	res, err := l.client.GetRecordsWithResponse(ctx, mapArgs(args))
	if err != nil {
		return nil, fmt.Errorf("could not get records from auditlog api: %w", err)
	}

	if res.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("invalid status code received from auditlog api: want: %d, got: %d: %q", http.StatusOK, res.StatusCode(), res.Body)
	}

	records, err := mapRestToModel(res.JSON200.Records)
	if err != nil {
		return nil, fmt.Errorf("could not map rest response to records: %w", err)
	}

	return records, nil
}

func mapArgs(args *ListRecordsArgs) *models.GetRecordsParams {
	var before *int64

	if !args.Filters.Period.End.IsZero() {
		b := args.Filters.Period.End.Unix()
		before = &b
	}

	var after *int64

	if !args.Filters.Period.Start.IsZero() {
		a := args.Filters.Period.Start.Unix()
		after = &a
	}

	var ids *[]models.Uuid

	if len(args.Filters.IDs) > 0 {
		idsFilter := make([]models.Uuid, 0, len(args.Filters.IDs))

		idsFilter = append(idsFilter, args.Filters.IDs...)

		ids = &idsFilter
	}

	var correlationIDs *[]models.Uuid

	if len(args.Filters.CorrelationIDs) > 0 {
		correlationIDsFilter := make([]models.Uuid, 0, len(args.Filters.CorrelationIDs))

		correlationIDsFilter = append(correlationIDsFilter, args.Filters.CorrelationIDs...)

		correlationIDs = &correlationIDsFilter
	}

	var contentHashes *[]string

	if len(args.Filters.ContentHashes) > 0 {
		contentHashesFilter := make([]string, 0, len(args.Filters.ContentHashes))

		contentHashesFilter = append(contentHashesFilter, args.Filters.ContentHashes...)

		contentHashes = &contentHashesFilter
	}

	var serviceNames *[]string

	if len(args.Filters.ServiceNames) > 0 {
		serviceNamesFilter := make([]string, 0, len(args.Filters.ServiceNames))

		serviceNamesFilter = append(serviceNamesFilter, args.Filters.ServiceNames...)

		serviceNames = &serviceNamesFilter
	}

	return &models.GetRecordsParams{
		GroupId:       args.GroupID,
		Cursor:        &args.Pagination.StartID,
		Before:        before,
		After:         after,
		SortOrder:     mapSortOrder(args.Pagination.SortOrder),
		Limit:         &args.Pagination.Limit,
		Id:            ids,
		CorrelationId: correlationIDs,
		ContentHash:   contentHashes,
		ServiceName:   serviceNames,
	}
}

func mapSortOrder(s SortOrder) *models.SortOrder {
	var sortOrder models.SortOrder

	switch s {
	case SortOrderAscending:
		sortOrder = models.SORTORDERASCENDING
	case SortOrderDescending:
		sortOrder = models.SORTORDERDESCENDING
	default:
		sortOrder = models.SORTORDERASCENDING
	}

	return &sortOrder
}

func mapRestToModel(recs []models.Record) (Records, error) {
	records := make(Records, 0, len(recs))

	for i := range recs {
		id, err := uuid.FromString(recs[i].Id)
		if err != nil {
			return nil, fmt.Errorf("could not parse id: %w", err)
		}

		correlationID, err := uuid.FromString(recs[i].CorrelationId)
		if err != nil {
			return nil, fmt.Errorf("could not parse correlationID: %w", err)
		}

		actor, err := mapActorToModel(recs[i].Actor)
		if err != nil {
			return nil, fmt.Errorf("could not map actor: %w", err)
		}

		event, err := mapEventToModel(recs[i].Event)
		if err != nil {
			return nil, fmt.Errorf("could not map event: %w", err)
		}

		source, err := mapSourceToModel(recs[i].Source)
		if err != nil {
			return nil, fmt.Errorf("could not map source: %w", err)
		}

		status, err := mapStatusToModel(recs[i].Status)
		if err != nil {
			return nil, fmt.Errorf("could not map status: %w", err)
		}

		component, err := mapComponentToModel(recs[i].Component)
		if err != nil {
			return nil, fmt.Errorf("could not map component: %w", err)
		}

		records = append(records, &Record{
			ID:            ID(id),
			GroupID:       GroupID(recs[i].GroupId),
			CorrelationID: CorrelationID(correlationID),
			Actor:         actor,
			Event:         event,
			Source:        source,
			Status:        status,
			Component:     component,
			CreatedAt:     time.Unix(recs[i].CreatedAt, 0),
		})
	}

	return records, nil
}

func mapActorToModel(a models.Record_Actor) (Actor, error) {
	model, err := a.ValueByDiscriminator()
	if err != nil {
		return nil, err
	}

	var actor Actor

	switch ac := model.(type) {
	case models.ActorUnknown:
		actor = &ActorUnknown{}
	case models.ActorUser:
		actor = &ActorUser{
			Name:  ac.Name,
			Email: ac.Email,
		}
	case models.ActorService:
		actor = &ActorService{
			SubjectCommonName:   ac.SubjectCommonName,
			PublicKeyThumbprint: ac.PublicKeyThumbprint,
		}
	default:
		return nil, fmt.Errorf("unknown actor type: %T", ac)
	}

	return actor, nil
}

func mapEventToModel(e models.Record_Event) (Event, error) {
	model, err := e.ValueByDiscriminator()
	if err != nil {
		return nil, err
	}

	var event Event

	switch ev := model.(type) {
	case models.EventLogin:
		event = &EventLogin{}

	case models.EventCreateContract:
		event = &EventCreateContract{
			ContractHash: ev.ContentHash,
		}
	case models.EventAcceptContract:
		event = &EventAcceptContract{
			ContractHash: ev.ContentHash,
		}
	case models.EventRejectContract:
		event = &EventRejectContract{
			ContractHash: ev.ContentHash,
		}
	case models.EventRevokeContract:
		event = &EventRevokeContract{
			ContractHash: ev.ContentHash,
		}

	case models.EventCreateService:
		event = &EventCreateService{
			ServiceName: ev.ServiceName,
		}
	case models.EventUpdateService:
		event = &EventUpdateService{
			ServiceName: ev.ServiceName,
		}
	case models.EventDeleteService:
		event = &EventDeleteService{
			ServiceName: ev.ServiceName,
		}
	case models.EventCreatePeer:
		event = &EventCreatePeer{
			PeerID: ev.PeerId,
		}
	case models.EventUpdatePeer:
		event = &EventUpdatePeer{
			PeerID: ev.PeerId,
		}
	case models.EventSynchronizePeers:
		event = &EventSynchronizePeers{}
	case models.EventFailedDistributionRetry:
		event = &EventFailedDistributionRetry{
			ContentHash: ev.ContentHash,
			Action:      FailedDistributionRetryAction(ev.Action),
			PeerID:      ev.PeerId,
		}
	default:
		return nil, fmt.Errorf("unknown event type: %T", ev)
	}

	return event, nil
}

func mapSourceToModel(s models.Record_Source) (Source, error) {
	model, err := s.ValueByDiscriminator()
	if err != nil {
		return nil, err
	}

	var source Source

	switch so := model.(type) {
	case models.SourceHttp:
		ip, err := netip.ParseAddr(so.IpAddress)
		if err != nil {
			return nil, fmt.Errorf("could not parse ip address: %w", err)
		}

		source = &SourceHTTP{
			IPAddress: ip,
			UserAgent: so.UserAgent,
		}
	default:
		return nil, fmt.Errorf("unknown source type: %T", so)
	}

	return source, nil
}

func mapStatusToModel(s models.Record_Status) (Status, error) {
	model, err := s.ValueByDiscriminator()
	if err != nil {
		return nil, err
	}

	var status Status

	switch sa := model.(type) {
	case models.StatusInitiated:
		status = &StatusInitiated{}
	case models.StatusUnauthorized:
		status = &StatusUnauthorized{
			Error: sa.Error,
		}
	case models.StatusFailed:
		status = &StatusFailed{
			Error: sa.Error,
		}
	case models.StatusSucceeded:
		status = &StatusSucceeded{}
	default:
		return nil, fmt.Errorf("unknown status type: %T", sa)
	}

	return status, nil
}

func mapComponentToModel(c models.RecordComponent) (Component, error) {
	switch c {
	case models.COMPONENTCONTROLLER:
		return ComponentController, nil
	case models.COMPONENTMANAGER:
		return ComponentManager, nil
	default:
		return ComponentUnspecified, fmt.Errorf("unknown component: %s", c)
	}
}

func transformAuditLoggerRequest(records []*Record) (*models.CreateRecordsJSONRequestBody, error) {
	reqRecords := make([]models.Record, len(records))

	for i, record := range records {
		actor, err := mapActorToRest(record.Actor)
		if err != nil {
			return nil, err
		}

		event, err := mapEventToRest(record.Event)
		if err != nil {
			return nil, err
		}

		source, err := mapSourceToRest(record.Source)
		if err != nil {
			return nil, err
		}

		status, err := mapStatusToRest(record.Status)
		if err != nil {
			return nil, err
		}

		component, err := mapComponentToRest(record.Component)
		if err != nil {
			return nil, err
		}

		reqRecords[i] = models.Record{
			Actor:         *actor,
			Component:     component,
			CorrelationId: uuid.UUID(record.CorrelationID).String(),
			CreatedAt:     record.CreatedAt.Unix(),
			Event:         *event,
			GroupId:       string(record.GroupID),
			Id:            uuid.UUID(record.ID).String(),
			Source:        *source,
			Status:        *status,
		}
	}

	return &models.CreateRecordsJSONRequestBody{
		Records: reqRecords,
	}, nil
}

func mapStatusToRest(status Status) (*models.Record_Status, error) {
	translatedStatus := models.Record_Status{}
	switch status := status.(type) {
	case StatusSucceeded:
		err := translatedStatus.FromStatusSucceeded(models.StatusSucceeded{
			Type: models.STATUSTYPESUCCEEDED,
		})
		if err != nil {
			return nil, err
		}
	case StatusFailed:
		err := translatedStatus.FromStatusFailed(models.StatusFailed{
			Error: status.Error,
			Type:  models.STATUSTYPEFAILED,
		})
		if err != nil {
			return nil, err
		}
	case StatusUnauthorized:
		err := translatedStatus.FromStatusUnauthorized(models.StatusUnauthorized{
			Error: status.Error,
			Type:  models.STATUSTYPEUNAUTHORIZED,
		})
		if err != nil {
			return nil, err
		}
	case StatusInitiated:
		err := translatedStatus.FromStatusInitiated(models.StatusInitiated{
			Type: models.STATUSTYPEINITIATED,
		})
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("could not transform Status")
	}

	return &translatedStatus, nil
}

func mapSourceToRest(source Source) (*models.Record_Source, error) {
	translatedSource := models.Record_Source{}
	switch source := source.(type) {
	case SourceHTTP:
		err := translatedSource.FromSourceHttp(models.SourceHttp{
			IpAddress: source.IPAddress.String(),
			Type:      models.SOURCETYPEHTTP,
			UserAgent: source.UserAgent,
		})
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("could not transform Source, unknown Source type")
	}

	return &translatedSource, nil
}

func mapComponentToRest(c Component) (models.RecordComponent, error) {
	switch c {
	case ComponentController:
		return models.COMPONENTCONTROLLER, nil
	case ComponentManager:
		return models.COMPONENTMANAGER, nil
	default:
		return "", errors.New("could not transform Component, unknown Component type")
	}
}

//nolint:gocyclo,funlen // breaking switch would impact readability
func mapEventToRest(event Event) (*models.Record_Event, error) {
	translatedEvent := models.Record_Event{}
	switch e := event.(type) {
	case EventAcceptContract:
		err := translatedEvent.FromEventAcceptContract(models.EventAcceptContract{
			ContentHash: e.ContractHash,
			Type:        models.EVENTTYPEACCEPTCONTRACT,
		})
		if err != nil {
			return nil, err
		}
	case EventCreateContract:
		err := translatedEvent.FromEventCreateContract(models.EventCreateContract{
			ContentHash: e.ContractHash,
			Type:        models.EVENTTYPECREATECONTRACT,
		})
		if err != nil {
			return nil, err
		}
	case EventRejectContract:
		err := translatedEvent.FromEventRejectContract(models.EventRejectContract{
			ContentHash: e.ContractHash,
			Type:        models.EVENTTYPEREJECTCONTRACT,
		})
		if err != nil {
			return nil, err
		}
	case EventRevokeContract:
		err := translatedEvent.FromEventRevokeContract(models.EventRevokeContract{
			ContentHash: e.ContractHash,
			Type:        models.EVENTTYPEREVOKECONTRACT,
		})
		if err != nil {
			return nil, err
		}
	case EventCreateService:
		err := translatedEvent.FromEventCreateService(models.EventCreateService{
			ServiceName: e.ServiceName,
			Type:        models.EVENTTYPECREATESERVICE,
		})
		if err != nil {
			return nil, err
		}
	case EventUpdateService:
		err := translatedEvent.FromEventUpdateService(models.EventUpdateService{
			ServiceName: e.ServiceName,
			Type:        models.EVENTTYPEUPDATESERVICE,
		})
		if err != nil {
			return nil, err
		}
	case EventDeleteService:
		err := translatedEvent.FromEventDeleteService(models.EventDeleteService{
			ServiceName: e.ServiceName,
			Type:        models.EVENTTYPEDELETESERVICE,
		})
		if err != nil {
			return nil, err
		}
	case EventLogin:
		err := translatedEvent.FromEventLogin(models.EventLogin{Type: models.EVENTTYPELOGIN})
		if err != nil {
			return nil, err
		}
	case EventFailedDistributionRetry:
		var action models.FailedDistributionRetryAction

		switch e.Action {
		case FailedDistributionRetryActionSubmitRevokeSignature:
			action = models.SUBMITREVOKESIGNATURE
		case FailedDistributionRetryActionSubmitRejectSignature:
			action = models.SUBMITREJECTSIGNATURE
		case FailedDistributionRetryActionSubmitAcceptSignature:
			action = models.SUBMITACCEPTSIGNATURE
		case FailedDistributionRetryActionSubmitContract:
			action = models.SUBMITCONTRACT
		default:
			return nil, fmt.Errorf("uknown FailedDistributionRetryAction: %s", e.Action)
		}

		err := translatedEvent.FromEventFailedDistributionRetry(models.EventFailedDistributionRetry{
			Action:      action,
			ContentHash: e.ContentHash,
			PeerId:      e.PeerID,
			Type:        models.EVENTTYPEFAILEDDISTRIBUTIONRETRY,
		})
		if err != nil {
			return nil, err
		}
	case EventCreatePeer:
		err := translatedEvent.FromEventCreatePeer(models.EventCreatePeer{
			PeerId: e.PeerID,
			Type:   models.EVENTTYPECREATEPEER,
		})
		if err != nil {
			return nil, err
		}
	case EventUpdatePeer:
		err := translatedEvent.FromEventUpdatePeer(models.EventUpdatePeer{
			PeerId: e.PeerID,
			Type:   models.EVENTTYPEUPDATEPEER,
		})
		if err != nil {
			return nil, err
		}
	case EventSynchronizePeers:
		err := translatedEvent.FromEventSynchronizePeers(models.EventSynchronizePeers{
			Type: models.EVENTTYPESYNCHRONIZEPEERS,
		})
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("could not transform Event, unknown Event type")
	}

	return &translatedEvent, nil
}

func mapActorToRest(actor Actor) (*models.Record_Actor, error) {
	translatedActor := models.Record_Actor{}
	switch a := actor.(type) {
	case *ActorUser:
		err := translatedActor.FromActorUser(models.ActorUser{
			Email: a.Email,
			Name:  a.Name,
			Type:  models.ACTORTYPEUSER,
		})
		if err != nil {
			return nil, err
		}
	case *ActorService:
		err := translatedActor.FromActorService(models.ActorService{
			SubjectCommonName:   a.SubjectCommonName,
			PublicKeyThumbprint: a.PublicKeyThumbprint,
			Type:                models.ACTORTYPESERVICE,
		})
		if err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("could not transform Actor, unknown Actor type %v", a)
	}

	return &translatedActor, nil
}
