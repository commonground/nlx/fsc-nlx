// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import (
	"context"
	"encoding/json"
	"fmt"
)

type StdoutAuditLogger struct{}

func NewStdoutAuditlogger() (*StdoutAuditLogger, error) {
	return &StdoutAuditLogger{}, nil
}

func (l *StdoutAuditLogger) CreateRecords(_ context.Context, records Records) error {
	jsonRecords, err := transformAuditLoggerRequest(records)
	if err != nil {
		return fmt.Errorf("could not transform records into JSON records: %w", err)
	}

	for i := range jsonRecords.Records {
		record, err := json.Marshal(jsonRecords.Records[i])
		if err != nil {
			return fmt.Errorf("could not marshal record into JSON: %w", err)
		}

		fmt.Println(string(record))
	}

	return nil
}

// ListRecords is not possible with this logger
func (l *StdoutAuditLogger) ListRecords(_ context.Context, _ *ListRecordsArgs) (Records, error) {
	return Records{}, nil
}
