// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package auditlog

import (
	"context"
	"fmt"
	"net/netip"
	"time"

	"github.com/gofrs/uuid"

	"gitlab.com/commonground/nlx/fsc-nlx/common/idgenerator"
)

type AuditLogger interface {
	CreateRecords(ctx context.Context, records Records) error
	ListRecords(ctx context.Context, args *ListRecordsArgs) (Records, error)
}

type Records []*Record

type Record struct {
	ID            ID
	GroupID       GroupID
	CorrelationID CorrelationID
	Actor         Actor
	Event         Event
	Source        Source
	Status        Status
	Component     Component
	CreatedAt     time.Time
}

type SortOrder string

const (
	SortOrderAscending  SortOrder = "asc"
	SortOrderDescending SortOrder = "desc"
)

type Period struct {
	Start time.Time
	End   time.Time
}

type Filters struct {
	Period         *Period
	IDs            []string
	CorrelationIDs []string
	ContentHashes  []string
	ServiceNames   []string
}

type Pagination struct {
	StartID   string
	Limit     uint32
	SortOrder SortOrder
}

type ListRecordsArgs struct {
	GroupID    string
	Pagination *Pagination
	Filters    *Filters
}

func (r *Record) CloneWithStatus(status Status, createdAt time.Time, id idgenerator.IDGenerator) (*Record, error) {
	recordID, err := id.New()
	if err != nil {
		return nil, fmt.Errorf("could not generate ID for AuditLog record")
	}

	recordUUID, _ := uuid.FromString(recordID)

	newRecord := r
	newRecord.ID = ID(recordUUID)
	newRecord.Status = status
	newRecord.CreatedAt = createdAt

	return newRecord, nil
}

func (r *Record) CloneWithStatusAndEvent(status Status, createdAt time.Time, id idgenerator.IDGenerator, event Event) (*Record, error) {
	recordID, err := id.New()
	if err != nil {
		return nil, fmt.Errorf("could not generate ID for AuditLog record")
	}

	recordUUID, _ := uuid.FromString(recordID)

	newRecord := r
	newRecord.ID = ID(recordUUID)
	newRecord.Status = status
	newRecord.Event = event
	newRecord.CreatedAt = createdAt

	return newRecord, nil
}

type ID uuid.UUID
type CorrelationID uuid.UUID

func (c CorrelationID) String() string {
	return uuid.UUID(c).String()
}

type GroupID string

type Actor any

type ActorUser struct {
	Name  string
	Email string
}

type ActorService struct {
	SubjectCommonName   string
	PublicKeyThumbprint string
}

type ActorUnknown struct{}

type Event any

type EventCreateContract struct {
	ContractHash string
}

type EventAcceptContract struct {
	ContractHash string
}

type EventRejectContract struct {
	ContractHash string
}

type EventRevokeContract struct {
	ContractHash string
}

type EventCreateService struct {
	ServiceName string
}

type EventUpdateService struct {
	ServiceName string
}

type EventDeleteService struct {
	ServiceName string
}

type FailedDistributionRetryAction string

type EventCreatePeer struct {
	PeerID string
}

type EventUpdatePeer struct {
	PeerID string
}

type EventDeletePeer struct {
	PeerID string
}

type EventSynchronizePeers struct {
}

const (
	FailedDistributionRetryActionSubmitContract        FailedDistributionRetryAction = "SubmitContract"
	FailedDistributionRetryActionSubmitAcceptSignature FailedDistributionRetryAction = "SubmitAcceptSignature"
	FailedDistributionRetryActionSubmitRejectSignature FailedDistributionRetryAction = "SubmitRejectSignature"
	FailedDistributionRetryActionSubmitRevokeSignature FailedDistributionRetryAction = "SubmitRevokeSignature"
)

type EventFailedDistributionRetry struct {
	ContentHash string
	PeerID      string
	Action      FailedDistributionRetryAction
}

type EventLogin struct{}

type Component string

const (
	ComponentUnspecified Component = ""
	ComponentController  Component = "controller"
	ComponentManager     Component = "manager"
)

type Source any

type SourceHTTP struct {
	IPAddress netip.Addr
	UserAgent string
}

type Status any

type StatusFailed struct {
	Error string
}

type StatusUnauthorized struct {
	Error string
}

type StatusSucceeded struct{}

type StatusInitiated struct{}
