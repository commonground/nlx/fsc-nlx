// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package http

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncodeBodyWhereBodyIsSmallerThanChunk(t *testing.T) {
	encodedBody, err := EncodeBody(io.NopCloser(strings.NewReader("this is a test")), 15, 1024)
	assert.NoError(t, err)
	assert.NotNil(t, encodedBody)

	assert.Equal(t, base64.StdEncoding.EncodeToString([]byte("this is a test")), encodedBody)
}

func TestEncodeBodyLargerMultipleThanChunk(t *testing.T) {
	testBody := make([]byte, 2048)
	_, err := rand.Read(testBody)
	assert.NoError(t, err)

	encodedBody, err := EncodeBody(io.NopCloser(bytes.NewReader(testBody)), 2048, 1024)

	assert.NoError(t, err)
	assert.NotNil(t, encodedBody)

	assert.Equal(t, base64.StdEncoding.EncodeToString(testBody), encodedBody)
}

func TestEncodeBodyLargerThanChunk(t *testing.T) {
	testBody := make([]byte, 2053)
	_, err := rand.Read(testBody)
	assert.NoError(t, err)

	encodedBody, err := EncodeBody(io.NopCloser(bytes.NewReader(testBody)), 4096, 1024)

	assert.NoError(t, err)
	assert.NotNil(t, encodedBody)

	assert.Equal(t, base64.StdEncoding.EncodeToString(testBody), encodedBody)
}

func TestEncodeBodyLargerThanMaxBytes(t *testing.T) {
	testBody := make([]byte, 2048)
	_, err := rand.Read(testBody)
	assert.NoError(t, err)

	_, err = EncodeBody(io.NopCloser(bytes.NewReader(testBody)), 1024, 256)

	assert.ErrorContains(t, err, "body exceeds maxBytes and cannot be encoded")
}
