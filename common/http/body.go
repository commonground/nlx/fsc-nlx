// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package http

import (
	"bytes"
	"encoding/base64"
	"errors"
	"io"
)

func EncodeBody(body io.Reader, maxBytes, chunkSize int) (string, error) {
	buf := bytes.Buffer{}
	tee := io.TeeReader(body, &buf)

	var buffer bytes.Buffer

	for {
		chunk := make([]byte, chunkSize)

		bodyBytes, err := tee.Read(chunk)
		if err != nil && err != io.EOF {
			return "", errors.New("cannot read body")
		}

		if bodyBytes == 0 {
			break
		}

		//trim null bytes if body is smaller than chunk size
		chunk = bytes.Trim(chunk, "\x00")

		// Write the chunk to a buffer
		buffer.Write(chunk)

		if buffer.Len() > maxBytes {
			return "", errors.New("body exceeds maxBytes and cannot be encoded")
		}
	}

	return base64.StdEncoding.EncodeToString(buffer.Bytes()), nil
}
