// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package idgenerator

type IDGenerator interface {
	New() (string, error)
}
