// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uuidgenerator

import (
	"fmt"

	"github.com/gofrs/uuid"
)

type IDGenerator struct{}

func (s *IDGenerator) New() (string, error) {
	u, err := uuid.NewV7()
	if err != nil {
		return "", fmt.Errorf("could not generate uuid: %w", err)
	}

	return u.String(), nil
}

func New() *IDGenerator {
	return &IDGenerator{}
}
