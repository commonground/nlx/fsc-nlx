// Code generated by mockery v2.42.3. DO NOT EDIT.

package mock_id_generator

import mock "github.com/stretchr/testify/mock"

// MockIDGenerator is an autogenerated mock type for the IDGenerator type
type MockIDGenerator struct {
	mock.Mock
}

type MockIDGenerator_Expecter struct {
	mock *mock.Mock
}

func (_m *MockIDGenerator) EXPECT() *MockIDGenerator_Expecter {
	return &MockIDGenerator_Expecter{mock: &_m.Mock}
}

// New provides a mock function with given fields:
func (_m *MockIDGenerator) New() (string, error) {
	ret := _m.Called()

	if len(ret) == 0 {
		panic("no return value specified for New")
	}

	var r0 string
	var r1 error
	if rf, ok := ret.Get(0).(func() (string, error)); ok {
		return rf()
	}
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// MockIDGenerator_New_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'New'
type MockIDGenerator_New_Call struct {
	*mock.Call
}

// New is a helper method to define mock.On call
func (_e *MockIDGenerator_Expecter) New() *MockIDGenerator_New_Call {
	return &MockIDGenerator_New_Call{Call: _e.mock.On("New")}
}

func (_c *MockIDGenerator_New_Call) Run(run func()) *MockIDGenerator_New_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run()
	})
	return _c
}

func (_c *MockIDGenerator_New_Call) Return(_a0 string, _a1 error) *MockIDGenerator_New_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *MockIDGenerator_New_Call) RunAndReturn(run func() (string, error)) *MockIDGenerator_New_Call {
	_c.Call.Return(run)
	return _c
}

// NewMockIDGenerator creates a new instance of MockIDGenerator. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewMockIDGenerator(t interface {
	mock.TestingT
	Cleanup(func())
}) *MockIDGenerator {
	mock := &MockIDGenerator{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
