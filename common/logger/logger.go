// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package logger

import (
	"context"
	"log/slog"
	"os"
)

type Logger struct {
	*slog.Logger
}

func (l *Logger) Error(message string, err error) {
	l.Logger.Error(message, errAttr(err))
}

func (l *Logger) Info(message string) {
	l.Logger.Info(message)
}

func (l *Logger) Warn(message string, err error) {
	l.Logger.Warn(message, errAttr(err))
}

func (l *Logger) Fatal(message string, err error) {
	l.Logger.Log(context.Background(), slog.LevelError, message, errAttr(err))
	os.Exit(1)
}

func (l *Logger) With(args ...any) *Logger {
	return &Logger{
		Logger: l.Logger.With(args...),
	}
}

func errAttr(err error) slog.Attr {
	return slog.Any("error", err)
}
