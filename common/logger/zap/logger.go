// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package zaplogger

import (
	"errors"
	"log/slog"

	"go.uber.org/zap"
	"go.uber.org/zap/exp/zapslog"
	"go.uber.org/zap/zapcore"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
)

func New(logLevel, logType string) (*logger.Logger, error) {
	var config zap.Config

	switch logType {
	case "live":
		config = zap.Config{
			Development: true,
			Encoding:    "json",
			EncoderConfig: zapcore.EncoderConfig{
				MessageKey:     "message",
				TimeKey:        "time",
				LevelKey:       "level",
				NameKey:        "name",
				CallerKey:      "caller",
				StacktraceKey:  "stacktrace",
				LineEnding:     zapcore.DefaultLineEnding,
				EncodeLevel:    zapcore.CapitalLevelEncoder,
				EncodeTime:     zapcore.ISO8601TimeEncoder,
				EncodeDuration: zapcore.StringDurationEncoder,
				EncodeCaller:   zapcore.ShortCallerEncoder,
			},
			Level:            zap.NewAtomicLevelAt(zap.InfoLevel),
			OutputPaths:      []string{"stderr"},
			ErrorOutputPaths: []string{"stderr"},
		}
	case "local":
		config = zap.NewDevelopmentConfig()
		config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
		config.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	default:
		return nil, errors.New("invalid value " + logType + " for option log type")
	}

	switch logLevel {
	case "debug":
		config.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	case "info":
		config.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
	case "warn":
		config.Level = zap.NewAtomicLevelAt(zap.WarnLevel)
	case "":
		// ignore, use default loglevel for provided type
	default:
		return nil, errors.New("invalid value " + logLevel + " for option log level")
	}

	lgr, err := config.Build()
	if err != nil {
		return nil, err
	}

	defer func(lgr *zap.Logger) {
		err = lgr.Sync()
	}(lgr)

	zapSlogOptions := zapslog.WithCaller(true)

	zapHandler := zapslog.NewHandler(lgr.Core(), zapSlogOptions)

	return &logger.Logger{
		Logger: slog.New(zapHandler),
	}, nil
}
