// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package tls_test

import (
	"crypto/x509"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestCRL_CheckCertificateOnCRL(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	revokedCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.OrgOnCRL)
	assert.NoError(t, err)
	err = crl.CheckCertificate(revokedCertificate.Certificate())
	assert.Error(t, err)
	assert.Equal(t, fmt.Sprintf("certificate with serialnumber: %s is present on a Certificate Revocation List", revokedCertificate.Certificate().SerialNumber), err.Error())
}

func TestCRL_CheckCertificateWithDistributionPointOnCRL(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	revokedCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.OrgOnCRL)
	revokedCertificate.Certificate().CRLDistributionPoints = []string{"http://localhost"}
	assert.NoError(t, err)
	err = crl.CheckCertificate(revokedCertificate.Certificate())
	assert.Error(t, err)
	assert.Equal(t, fmt.Sprintf("certificate with serialnumber: %s is present on a Certificate Revocation List", revokedCertificate.Certificate().SerialNumber), err.Error())
}

func TestCRL_FetchAndCheckCRLWithDistributionPoint(t *testing.T) {
	crl, err := tls.NewCRL(nil)
	assert.NoError(t, err)

	crlServer := setupCRLEndpoint()
	defer crlServer.Close()

	revokedCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.OrgOnCRL)
	revokedCertificate.Certificate().CRLDistributionPoints = []string{crlServer.URL}
	assert.NoError(t, err)
	err = crl.CheckCertificate(revokedCertificate.Certificate())

	assert.Error(t, err)
	assert.Equal(t, fmt.Sprintf("certificate with serialnumber: %s is present on a Certificate Revocation List", revokedCertificate.Certificate().SerialNumber), err.Error())
}

func TestCRL_FetchAndCheckCRLFetchFails(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	crlServer := setupCRLEndpoint()
	defer crlServer.Close()

	testCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	testCertificate.Certificate().CRLDistributionPoints = []string{"http://idonotexist"}
	assert.NoError(t, err)
	err = crl.CheckCertificate(testCertificate.Certificate())
	assert.Error(t, err)
	assert.Equal(t, "cannot retrieve CRLsCache from revocationListURL: http://idonotexist", err.Error())
}

func TestCRL_InvalidCRLURL(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	crlFile, err := os.ReadFile(filepath.Join("..", "..", "testing", "pki", "ca.crl"))
	assert.NoError(t, err)

	revocationList, err := x509.ParseRevocationList(crlFile)
	assert.NoError(t, err)

	err = crl.AddRevocationList("invalid url", revocationList)

	assert.Error(t, err)
	assert.Equal(t, "parse \"invalid url\": invalid URI for request", err.Error())
}

func TestCRL_AddNilCRL(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	err = crl.AddRevocationList("http://localhost", nil)

	assert.Error(t, err)
	assert.Equal(t, "CRL is required", err.Error())
}

func TestCRL_CheckCertificateNotOnCRL(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	validCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	assert.NoError(t, err)
	err = crl.CheckCertificate(validCertificate.Certificate())
	assert.NoError(t, err)
}

func TestCRL_OnEvictEntry(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	revokedCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.OrgOnCRL)
	assert.NoError(t, err)
	time.Sleep(11 * time.Millisecond) // The created CRL cache has a TTL of 10 milliseconds so we wait for 11 milliseconds to force an eviction

	err = crl.CheckCertificate(revokedCertificate.Certificate())

	assert.NoError(t, err)
}

func TestCRL_NewCRLWithDefaults(t *testing.T) {
	crlFile, err := os.ReadFile(filepath.Join("..", "..", "testing", "pki", "ca.crl"))
	assert.NoError(t, err)

	_, err = x509.ParseRevocationList(crlFile)
	assert.NoError(t, err)

	crl, err := tls.NewCRLCustomSettings(nil, 0, 0)
	assert.NoError(t, err)

	assert.NotNil(t, crl)
}

func TestCRL_VerifyPeerCertificate(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	test := map[string]struct {
		certsRaw      [][]byte
		expectedError error
	}{
		"happy_flow": {
			certsRaw: func() [][]byte {
				var validCertificate *tls.CertificateBundle

				validCertificate, err = testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
				require.NoError(t, err)

				certsRaw := [][]byte{validCertificate.Certificate().Raw}
				return certsRaw
			}(),
		}, "revoked_certificate": {
			certsRaw: func() [][]byte {
				var revokedCertificate *tls.CertificateBundle

				revokedCertificate, err = testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.OrgOnCRL)
				require.NoError(t, err)

				certsRaw := [][]byte{revokedCertificate.Certificate().Raw}
				return certsRaw
			}(),
			expectedError: errors.New("is present on a Certificate Revocation List"),
		},
		"invalid_certificate": {
			certsRaw:      [][]byte{{byte('A')}},
			expectedError: errors.New("failed to parse certificate: x509: malformed certificate"),
		},
	}

	for _, tc := range test {
		err = crl.VerifyPeerCertificatesCRL(tc.certsRaw, nil)
		if tc.expectedError == nil {
			assert.NoError(t, err)
		} else {
			assert.ErrorContains(t, err, tc.expectedError.Error())
		}
	}
}

func setupCRL() (*tls.CRLsCache, error) {
	crlFile, err := os.ReadFile(filepath.Join("..", "..", "testing", "pki", "ca.crl"))
	if err != nil {
		return nil, err
	}

	revocationList, err := x509.ParseRevocationList(crlFile)
	if err != nil {
		return nil, err
	}

	crl, err := tls.NewCRLCustomSettings(nil, 10, 10*time.Millisecond)
	if err != nil {
		return nil, err
	}

	err = crl.AddRevocationList("http://localhost", revocationList)
	if err != nil {
		return nil, err
	}

	return crl, err
}

func setupCRLEndpoint() *httptest.Server {
	crlTestServer := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filepath.Join("..", "..", "testing", "pki", "ca.crl"))
	}))

	crlTestServer.Start()

	return crlTestServer
}
