// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package tls

import (
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/cloudflare/cfssl/log"
	lru "github.com/hashicorp/golang-lru/v2/expirable"
)

type CRLsCache struct {
	cache     *lru.LRU[string, map[string]*x509.RevocationList] // LRU[CRLURL, map[Serialnumber]RevocationList]
	mutexURLs sync.Mutex
	urls      map[string]*x509.RevocationList
}

const (
	LRUDefaultCacheSize = 128
	LRUTTL              = 24 * time.Hour
)

type CertificateRevokedError struct {
	message string
}

func (e CertificateRevokedError) Error() string {
	return e.message
}

func NewCertificateRevokedError(serialNumber string) *CertificateRevokedError {
	return &CertificateRevokedError{message: fmt.Sprintf("certificate with serialnumber: %s is present on a Certificate Revocation List", serialNumber)}
}

func NewCRL(cRLURLs []string) (*CRLsCache, error) {
	return NewCRLCustomSettings(cRLURLs, LRUDefaultCacheSize, LRUTTL)
}

func NewCRLCustomSettings(cRLURLs []string, cacheSize int, cacheTTL time.Duration) (*CRLsCache, error) {
	if cacheSize <= 0 {
		cacheSize = LRUDefaultCacheSize
	}

	if cacheTTL <= 0 {
		cacheTTL = LRUTTL
	}

	crl := &CRLsCache{
		mutexURLs: sync.Mutex{},
		urls:      make(map[string]*x509.RevocationList),
	}
	crl.cache = lru.NewLRU[string, map[string]*x509.RevocationList](cacheSize, crl.onEvictedEntry, cacheTTL)

	for _, URL := range cRLURLs {
		list, err := fetchRevocationList(URL)
		if err != nil {
			return nil, err
		}

		err = crl.AddRevocationList(URL, list)
		if err != nil {
			return nil, err
		}
	}

	return crl, nil
}

// fetch is performed in separate go routine since the cache is locked (via a mutex) until the onEvictedEntry function is finished
func (l *CRLsCache) onEvictedEntry(k string, _ map[string]*x509.RevocationList) {
	go func() {
		revocationList, err := fetchRevocationList(k)
		if err != nil {
			log.Error(err)
		}

		err = l.AddRevocationList(k, revocationList)
		if err != nil {
			log.Error(err)
		}
	}()
}

func fetchRevocationList(revocationListURL string) (*x509.RevocationList, error) {
	response, err := http.Get(revocationListURL)
	if err != nil {
		return nil, fmt.Errorf("cannot retrieve CRLsCache from revocationListURL: %s", revocationListURL)
	}
	defer response.Body.Close()

	rawCRL, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("cannot parse HTTP response from %q", revocationListURL))
	}

	revocationList, err := x509.ParseRevocationList(rawCRL)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("cannot parse CRLsCache from %q", revocationListURL))
	}

	return revocationList, nil
}

func (l *CRLsCache) AddRevocationList(crlURL string, revocationList *x509.RevocationList) error {
	if revocationList == nil {
		return errors.New("CRL is required")
	}

	_, err := url.ParseRequestURI(crlURL)
	if err != nil {
		return err
	}

	l.mutexURLs.Lock()
	defer l.mutexURLs.Unlock()
	l.urls[crlURL] = revocationList

	revokedSerialNumbers := make(map[string]*x509.RevocationList)
	for _, revokedSerialNumber := range revocationList.RevokedCertificateEntries {
		revokedSerialNumbers[revokedSerialNumber.SerialNumber.String()] = revocationList
	}

	l.cache.Add(crlURL, revokedSerialNumbers)

	return nil
}

func (l *CRLsCache) CheckCertificate(certificate *x509.Certificate) error {
	// Check if the cache is up-to-date.
	crlsNotInCache := l.validateCache(certificate.CRLDistributionPoints)

	// If there are crls not present in the cache, fetch the CRLs.
	if len(crlsNotInCache) > 0 {
		for _, crlURL := range crlsNotInCache {
			list, err := fetchRevocationList(crlURL)
			if err != nil {
				return err
			}

			err = l.AddRevocationList(crlURL, list)
			if err != nil {
				return err
			}
		}
	}

	// the cache is up-to-date, check if the certificate is not revoked.
	err := l.check(certificate)
	if err != nil {
		return err
	}

	return nil
}

// check verifies that the certificate is not revoked. this function expects the cache to be valid and up-to-date.
func (l *CRLsCache) check(certificate *x509.Certificate) error {
	for _, crl := range l.cache.Values() {
		if crl[certificate.SerialNumber.String()] != nil {
			return NewCertificateRevokedError(certificate.SerialNumber.String())
		}
	}

	return nil
}

func (l *CRLsCache) validateCache(cRLDistributionPoints []string) (crlsWithoutValidCache []string) {
	crlDistributionPointsToCheck := []string{}
	crlDistributionPointsToCheck = append(crlDistributionPointsToCheck, cRLDistributionPoints...)

	l.mutexURLs.Lock()
	defer l.mutexURLs.Unlock()

	for crlURL := range l.urls {
		crlDistributionPointsToCheck = append(crlDistributionPointsToCheck, crlURL)
	}

	for _, cRLDistributionPointURL := range crlDistributionPointsToCheck {
		crl, ok := l.urls[cRLDistributionPointURL]
		if !ok {
			crlsWithoutValidCache = append(crlsWithoutValidCache, cRLDistributionPointURL)
		} else if crl.NextUpdate.Before(time.Now()) {
			l.cache.Remove(cRLDistributionPointURL)
			crlsWithoutValidCache = append(crlsWithoutValidCache, cRLDistributionPointURL)
		}
	}

	return crlsWithoutValidCache
}

// VerifyPeerCertificatesCRL is used in crypto/tls.Config#VerifyPeerCertificate to also check client certificates against CRLs
func (l *CRLsCache) VerifyPeerCertificatesCRL(rawCerts [][]byte, _ [][]*x509.Certificate) error {
	for _, rawCert := range rawCerts {
		cert, err := x509.ParseCertificate(rawCert)
		if err != nil {
			return fmt.Errorf("failed to parse certificate: %v", err)
		}

		err = l.CheckCertificate(cert)
		if err != nil {
			return err
		}
	}

	return nil
}
