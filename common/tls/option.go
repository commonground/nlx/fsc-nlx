// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
package tls

import (
	"crypto/tls"
	"fmt"
)

type ConfigOption func(c *tls.Config)

func (c *CertificateBundle) WithTLSClientAuth() ConfigOption {
	return func(t *tls.Config) {
		t.ClientAuth = tls.RequireAndVerifyClientCert
		t.ClientCAs = c.rootCAs
	}
}

// WithTLS12 enables TLS v1.2 on a tls.Config instance
//
// Cipher suites are taken from https://ssl-config.mozilla.org/#server=go&config=intermediate&guideline=5.6
// Ordered by the ciphers with the most bits.
func WithTLS12() ConfigOption {
	return func(t *tls.Config) {
		t.MinVersion = tls.VersionTLS12
		t.CipherSuites = []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
		}
	}
}

func WithCRLPeerCertificateVerification(crlCache *CRLsCache) ConfigOption {
	return func(t *tls.Config) {
		t.VerifyPeerCertificate = crlCache.VerifyPeerCertificatesCRL
	}
}

func VerifyConnStatePeerID(peerID string) func(c tls.ConnectionState) error {
	return func(c tls.ConnectionState) error {
		if len(c.PeerCertificates) == 0 {
			return fmt.Errorf("no peer certificates found in tls conn state")
		}

		tlsPeerID := c.PeerCertificates[0].Subject.SerialNumber

		if tlsPeerID != peerID {
			return fmt.Errorf("invalid peer ID in tls conn certificate, got: %q, expected: %q", tlsPeerID, peerID)
		}

		return nil
	}
}

func WithVerifyFunc(verifyFunc func(tls.ConnectionState) error) ConfigOption {
	return func(t *tls.Config) {
		t.VerifyConnection = verifyFunc
	}
}
