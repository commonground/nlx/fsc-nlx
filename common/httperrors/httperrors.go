// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package httperrors

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
)

type Code string
type Source string
type Location string

type FSCNetworkError struct {
	Message    string   `json:"message"`
	Source     Source   `json:"source"`
	Location   Location `json:"location,omitempty"`
	Code       Code     `json:"code"`
	Metadata   any      `json:"metadata"`
	httpStatus int
}

func WriteError(w http.ResponseWriter, nlxErr *FSCNetworkError) {
	if nlxErr == nil {
		nlxErr = ServerError(errors.New("internal server error"))
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Fsc-Error-Code", nlxErr.Code.String())

	if nlxErr.httpStatus == 0 {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(nlxErr.httpStatus)
	}

	networkErr, err := json.Marshal(nlxErr)
	if err != nil {
		fmt.Fprintf(w, `{"source":%q,"code":%q,"message":"error while marshaling json error"}`, nlxErr.Source.String(), ServerErrorErr.String())
		return
	}

	_, _ = w.Write(networkErr)
}

// The Fsc-Authorization header is missing.
const MissingAuthHeaderErr Code = "ERROR_CODE_ACCESS_TOKEN_MISSING"

func MissingAuthHeader() *FSCNetworkError {
	return &FSCNetworkError{
		Code:       MissingAuthHeaderErr,
		Message:    "missing Fsc-Authorization header",
		httpStatus: http.StatusUnauthorized,
	}
}

// Invalid token in Fsc-Authorization header.
//
//nolint:gosec // these are not credentials
const InvalidAuthorizationTokenErr Code = "ERROR_CODE_ACCESS_TOKEN_INVALID"

func InvalidAuthorizationToken(err string) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       InvalidAuthorizationTokenErr,
		Message:    fmt.Sprintf("invalid token in authorization header: %s", err),
		httpStatus: http.StatusUnauthorized,
	}
}

// Invalid token in Fsc-Authorization header.
//
//nolint:gosec // these are not credentials
const ExpiredAuthorizationTokenErr Code = "ERROR_CODE_ACCESS_TOKEN_EXPIRED"

func ExpiredAuthorizationToken(err string) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       ExpiredAuthorizationTokenErr,
		Message:    fmt.Sprintf("expired token in authorization header: %s", err),
		httpStatus: http.StatusUnauthorized,
	}
}

// The requested service does not exist.
const ServiceDoesNotExistErr Code = "ERROR_CODE_SERVICE_NOT_FOUND"

func ServiceDoesNotExist(serviceName string) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       ServiceDoesNotExistErr,
		Message:    fmt.Sprintf("no endpoint for service '%s'", serviceName),
		httpStatus: http.StatusNotFound,
	}
}

// There was an error while executing the plugin chain
const ErrorExecutingPluginChainErr Code = "ERROR_EXECUTING_PLUGIN_CHAIN"

func ErrorExecutingPluginChain() *FSCNetworkError {
	return &FSCNetworkError{
		Code:       ErrorExecutingPluginChainErr,
		Message:    "error executing plugin chain",
		httpStatus: http.StatusInternalServerError,
	}
}

// The service is either unreachable or down.
const ServiceUnreachableErr Code = "ERROR_CODE_SERVICE_UNREACHABLE"

func ServiceUnreachable(serviceURL string) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       ServiceUnreachableErr,
		Message:    fmt.Sprintf("failed API request to %s try again later. service api down/unreachable. check error at https://docs.nlx.io/support/common-errors/", serviceURL),
		httpStatus: http.StatusBadGateway,
	}
}

// Missing peer certificate in connection to inway
const MissingPeerCertificateErr Code = "MISSING_PEER_CERTIFICATE"

func MissingPeerCertificate() *FSCNetworkError {
	return &FSCNetworkError{
		Code:       MissingPeerCertificateErr,
		Message:    "invalid connection: missing peer certificates",
		httpStatus: http.StatusUnauthorized,
	}
}

// Invalid certificate provided in connection to inway.
// Certificate must contain organization name, subject serial number and a organization issuer
const InvalidCertificateErr Code = "INVALID_CERTIFICATE"

func InvalidCertificate(msg string) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       InvalidCertificateErr,
		Message:    msg,
		httpStatus: http.StatusUnauthorized,
	}
}

// Access denied, no valid access grant was found,
// request access to this service via the Management
const AccessDeniedErr Code = "ACCESS_DENIED"

func AccessDenied(orgSerialNumber, orgCertificateThumbprint string) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       AccessDeniedErr,
		Message:    fmt.Sprintf(`permission denied, organization %q or certificate thumbprint %q is not allowed access.`, orgSerialNumber, orgCertificateThumbprint),
		httpStatus: http.StatusForbidden,
	}
}

// There was an error while authorizing the request via the authorization server
const ErrorWhileAuthorizingRequestErr Code = "ERROR_WHILE_AUTHORIZING_REQUEST"

func ErrorWhileAuthorizingRequest() *FSCNetworkError {
	return &FSCNetworkError{
		Code:       ErrorWhileAuthorizingRequestErr,
		Message:    "error authorizing request: unexpected error while calling the authorization service",
		httpStatus: http.StatusUnauthorized,
	}
}

func ErrorWhileAuthorizingRequestMaxBodyExceeded() *FSCNetworkError {
	return &FSCNetworkError{
		Code:       ErrorWhileAuthorizingRequestErr,
		Message:    "error authorizing request: HTTP request body exceeded maximum number of bytes",
		httpStatus: http.StatusUnauthorized,
	}
}

// The authorization server denied the request
const UnauthorizedErr Code = "UNAUTHORIZED"

func Unauthorized(message string) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       UnauthorizedErr,
		Message:    fmt.Sprintf("authorization server denied request: %s", message),
		httpStatus: http.StatusUnauthorized,
	}
}

// Missing log record id, the header 'Fsc-Transaction-ID' must be set with an unique id for this request
const MissingLogRecordIDErr Code = "MISSING_LOG_RECORD_ID"

func MissingLogRecordID() *FSCNetworkError {
	return &FSCNetworkError{
		Code:       MissingLogRecordIDErr,
		Message:    "missing logrecord id",
		httpStatus: http.StatusBadRequest,
	}
}

// Invalid log record id, the header 'Fsc-Transaction-ID' must be set with an unique id for this request
const InvalidLogRecordIDErr Code = "INVALID_LOG_RECORD_ID"

func InvalidLogRecordID() *FSCNetworkError {
	return &FSCNetworkError{
		Code:       InvalidLogRecordIDErr,
		Message:    "invalid logrecord id",
		httpStatus: http.StatusBadRequest,
	}
}

// Cannot write the log record to the Transaction Log
const LogRecordWriteErr Code = "TRANSACTION_LOG_WRITE_ERROR"

func LogRecordWriteError() *FSCNetworkError {
	return &FSCNetworkError{
		Code:       LogRecordWriteErr,
		Message:    "The TransactionLog record could not be created",
		httpStatus: http.StatusInternalServerError,
	}
}

// General server error, see message for more information
const ServerErrorErr Code = "SERVER_ERROR"

func ServerError(errDetails error) *FSCNetworkError {
	message := "server error"

	if errDetails != nil {
		message = fmt.Sprintf("%s: %v", message, errDetails)
	}

	return &FSCNetworkError{
		Code:       ServerErrorErr,
		Message:    message,
		httpStatus: http.StatusInternalServerError,
	}
}

// Unknown grant hash in header
const UnknownGrantHashInHeaderErr Code = "UNKNOWN_GRANT_HASH_IN_HEADER"

type Service struct {
	GrantHash string
	Name      string
	PeerID    string
}

func UnknownGrantHashInHeader(grantHash string, services []*Service) *FSCNetworkError {
	type Metadata struct {
		Services []*Service `json:"services"`
	}

	err := &FSCNetworkError{
		Code:       UnknownGrantHashInHeaderErr,
		Message:    fmt.Sprintf("unknown grant hash: %q in Fsc-Grant-Hash header", grantHash),
		httpStatus: http.StatusBadRequest,
	}

	if len(services) > 0 {
		err.Metadata = &Metadata{
			Services: services,
		}
	}

	return err
}

// Error happened while trying to get a token for the service via the grant hash
const UnableToGetTokenForGrantHashErr Code = "UNABLE_TO_GET_TOKEN_FOR_GRANT_HASH"

func UnableToGetTokenForGrantHash(grantHash string) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       UnableToGetTokenForGrantHashErr,
		Message:    fmt.Sprintf("could not get token for grant hash: %q", grantHash),
		httpStatus: http.StatusInternalServerError,
	}
}

// The group id in the token does not match the expected group id
const WrongGroupIDInTokenErr Code = "ERROR_CODE_WRONG_GROUP_ID_IN_TOKEN"

func WrongGroupIDInToken(expectedGroupID, tokenGroupID string) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       WrongGroupIDInTokenErr,
		Message:    fmt.Sprintf("wrong group id in token: %q: want group id %q", tokenGroupID, expectedGroupID),
		httpStatus: http.StatusForbidden,
	}
}

// Fsc-Authorization should not be set by the client
const AuthHeaderMustNotBeSetErr Code = "AUTH_HEADER_MUST_NOT_BE_SET"

func AuthHeaderMustNotBeSet() *FSCNetworkError {
	return &FSCNetworkError{
		Code:       AuthHeaderMustNotBeSetErr,
		Message:    "Fsc-Authorization header must not be set by the client",
		httpStatus: http.StatusBadRequest,
	}
}

// Could not setup connection to the inway
const CouldNotSetupConnToInwayErr Code = "COULD_NOT_SETUP_CONN_TO_INWAY"

func CouldNotSetupConnToInway(inwayAddress string, err error) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       CouldNotSetupConnToInwayErr,
		Message:    fmt.Sprintf("could not setup connection to inway: %s, got error: %s", inwayAddress, err.Error()),
		httpStatus: http.StatusServiceUnavailable,
	}
}

// Client used a certificate that is present on a Certificate Revocation List
const CertificateRevokedErr Code = "ERROR_CODE_PEER_CERTIFICATE_VERIFICATION_FAILED"

func CertificateRevoked(err error) *FSCNetworkError {
	return &FSCNetworkError{
		Code:       CertificateRevokedErr,
		Message:    fmt.Sprintf("client used a revoked certificate: %s", err.Error()),
		httpStatus: http.StatusBadRequest,
	}
}

// Source
const (
	// The error originated from the Inway
	Inway Source = "inway"

	// The error originated from the Outway
	Outway Source = "outway"
)

// Location
// Check https://docs.nlx.io/support/common-errors for a graphical overview
const (
	// The error happened between the Inway and the API
	A1 Location = "A1"

	// The error happened between the Inway and the Authorization server
	IAS1 Location = "IAS1"

	// The error happened between the Outway and the Outway
	C1 Location = "C1"

	// The error happened between the Outway and Inway
	O1 Location = "O1"

	// The error happened between the Outway and the Authorization server
	OAS1 Location = "OAS1"
)

func (l Location) String() string {
	return string(l)
}

func (l Location) GoString() string {
	return l.String()
}

func (l Code) String() string {
	return string(l)
}

func (l Code) GoString() string {
	return l.String()
}

func (l Source) String() string {
	return string(l)
}

func (l Source) GoString() string {
	return l.String()
}
