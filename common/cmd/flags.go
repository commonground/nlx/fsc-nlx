// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package cmd

import (
	"os"
	"strings"

	"github.com/spf13/cobra"
)

func setupCmdFlagForEnvironment(key, value string, command *cobra.Command) error {
	flag := command.Flags().Lookup(key)

	if flag != nil {
		if err := flag.Value.Set(value); err != nil {
			return err
		}

		// remove required flag if the value was set by an environment variable
		if value != "" {
			delete(flag.Annotations, cobra.BashCompOneRequiredFlag)
		}
	}

	for _, subCmd := range command.Commands() {
		err := setupCmdFlagForEnvironment(key, value, subCmd)
		if err != nil {
			return err
		}
	}

	return nil
}

// All flags can also be set using environment variables.
// Environment variable names are all caps and '-' is replaced by '_'.
// Example: 'listen-address' becomes 'LISTEN_ADDRESS'
func SetupFlagsForEnvironment(rootCmd *cobra.Command) error {
	// pass environment variables to the arguments
	for _, keyval := range os.Environ() {
		const numberOfSubstringsToReturn = 2
		components := strings.SplitN(keyval, "=", numberOfSubstringsToReturn)

		//nolint:gomnd // key value pair has to have two components
		if len(components) != 2 {
			continue
		}

		key := strings.ReplaceAll(strings.ToLower(components[0]), "_", "-")
		value := components[1]

		err := setupCmdFlagForEnvironment(key, value, rootCmd)
		if err != nil {
			return err
		}
	}

	return nil
}
