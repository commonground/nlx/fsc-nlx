// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package cmd

type TLSOptions struct {
	RootCertFile string
	CertFile     string
	KeyFile      string
}

type TLSGroupOptions struct {
	GroupRootCert string
	GroupCertFile string
	GroupKeyFile  string
}
