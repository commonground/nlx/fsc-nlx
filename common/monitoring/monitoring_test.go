// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package monitoring

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
)

func TestNewMonitoring(t *testing.T) {
	positiveCheck := func() error {
		return nil
	}

	readinessProbes := map[string]Check{
		"test readiness probe": positiveCheck,
	}

	livenessProbes := map[string]Check{
		"test liveness probe": positiveCheck,
	}

	m, err := NewMonitoringService("", nil, readinessProbes, livenessProbes)
	assert.Nil(t, m)
	assert.EqualError(t, err, "address required")

	m, err = NewMonitoringService("localhost:8080", nil, readinessProbes, livenessProbes)
	assert.Nil(t, m)
	assert.EqualError(t, err, "logger required")

	m, err = NewMonitoringService("localhost:8080", discardlogger.New(), readinessProbes, livenessProbes)
	assert.NotNil(t, m)
	assert.Nil(t, err)

	err = m.Stop()
	assert.NoError(t, err)

}

func TestProbes(t *testing.T) {
	positiveCheck := func() error {
		return nil
	}
	negativeCheck := func() error {
		return errors.New("arbitrary error")
	}

	passingProbe := map[string]Check{
		"test passing probe": positiveCheck,
	}

	failingProbe := map[string]Check{
		"test failing probe": negativeCheck,
	}

	tests := []struct {
		description        string
		probes             map[string]Check
		expectedStatusCode int
	}{
		{
			description:        "the monitoring service is ready",
			probes:             passingProbe,
			expectedStatusCode: http.StatusOK,
		},
		{
			description:        "the monitoring service is not ready",
			probes:             failingProbe,
			expectedStatusCode: http.StatusServiceUnavailable,
		},
	}

	logger := discardlogger.New()
	serviceAdddress := "localhost:8080"

	for _, test := range tests {
		recorder := httptest.NewRecorder()
		request := httptest.NewRequest("GET", "/health/ready", http.NoBody)
		request.Host = "host"
		service, err := NewMonitoringService(serviceAdddress, logger, test.probes, test.probes)
		assert.Nil(t, err)
		assert.NotNil(t, service)

		service.handleReadinessRequest(recorder, request)
		responseReadiness := recorder.Result()
		assert.Equal(t, test.expectedStatusCode, responseReadiness.StatusCode)
		responseReadiness.Body.Close()

		service.handleLivenessRequest(recorder, request)
		responseLiveness := recorder.Result()
		assert.Equal(t, test.expectedStatusCode, responseLiveness.StatusCode)
		responseLiveness.Body.Close()
	}
}
