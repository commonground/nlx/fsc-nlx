// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package monitoring

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/common/slogerrorwriter"
)

type Check func() error

// Service struct
type Service struct {
	logger          *logger.Logger
	server          *http.Server
	readinessChecks map[string]Check
	livenessChecks  map[string]Check
}

const readHeaderTimeout = time.Second * 60

// NewMonitoringService creates a new monitoring service
func NewMonitoringService(address string, lgr *logger.Logger, readinessChecks, livenessChecks map[string]Check) (*Service, error) {
	if address == "" {
		return nil, fmt.Errorf("address required")
	}

	if lgr == nil {
		return nil, fmt.Errorf("logger required")
	}

	m := &Service{
		logger:          lgr,
		readinessChecks: readinessChecks,
		livenessChecks:  livenessChecks,
	}

	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/health/live", m.handleLivenessRequest)
	serveMux.HandleFunc("/health/ready", m.handleReadinessRequest)
	serveMux.Handle("/metrics", promhttp.Handler())
	m.server = &http.Server{
		Addr:              address,
		Handler:           serveMux,
		ReadHeaderTimeout: readHeaderTimeout,
		ErrorLog:          log.New(slogerrorwriter.New(lgr.Logger), "", 0),
	}

	return m, nil
}

// Start is a blocking call which starts the internal http server
func (m *Service) Start() error {
	m.logger.Info("starting monitoring service")
	err := m.server.ListenAndServe()

	if errors.Is(err, http.ErrServerClosed) {
		return nil
	}

	return err
}

// Stop will gracefully shut down the internal http server
func (m *Service) Stop() error {
	m.logger.Info("stopping monitoring service")

	localCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel() // do not remove. Otherwise, it could cause implicit goroutine leak

	return m.server.Shutdown(localCtx)
}

func (m *Service) handleReadinessRequest(w http.ResponseWriter, r *http.Request) {
	m.logger.Debug("received readiness check", "from-host", r.Host)

	for probeName, check := range m.readinessChecks {
		err := check()
		if err != nil {
			m.logger.Error(fmt.Sprintf("readiness probe %q has failed", probeName), err)
			w.WriteHeader(http.StatusServiceUnavailable)

			return
		}
	}

	w.WriteHeader(http.StatusOK)
}

func (m *Service) handleLivenessRequest(w http.ResponseWriter, r *http.Request) {
	m.logger.Debug("received liveness check", "from-host", r.Host)

	for probeName, check := range m.livenessChecks {
		err := check()

		if err != nil {
			m.logger.Error(fmt.Sprintf("liveness probe %q has failed", probeName), err)
			w.WriteHeader(http.StatusServiceUnavailable)

			return
		}
	}

	w.WriteHeader(http.StatusOK)
}
