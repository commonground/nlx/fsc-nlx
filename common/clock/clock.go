// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package clock

import "time"

type Clock interface {
	Now() time.Time
	UpdateTime(t time.Time)
}

type RealClock struct{}

// New returns a real clock
func New() *RealClock {
	return &RealClock{}
}

// Now will return the current time
func (c *RealClock) Now() time.Time {
	return time.Now()
}

func (c *RealClock) UpdateTime(time.Time) {}
