// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package transactionlog

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

type RestAPITransactionLogger struct {
	logger *logger.Logger
	client api.ClientWithResponsesInterface
}

type NewRestAPITransactionLoggerArgs struct {
	Logger *logger.Logger
	Client api.ClientWithResponsesInterface
}

func NewRestAPITransactionLogger(args *NewRestAPITransactionLoggerArgs) (*RestAPITransactionLogger, error) {
	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if args.Client == nil {
		return nil, fmt.Errorf("client is required")
	}

	result := &RestAPITransactionLogger{
		logger: args.Logger,
		client: args.Client,
	}

	return result, nil
}

func (txl *RestAPITransactionLogger) AddRecords(ctx context.Context, recs []*Record) error {
	records, err := recordsToRestLogs(recs)
	if err != nil {
		return err
	}

	res, err := txl.client.CreateLogRecordsWithResponse(ctx, models.CreateLogRecordsJSONRequestBody{
		Records: records,
	})
	if err != nil {
		return fmt.Errorf("could not create log records in rest api: %w", err)
	}

	if res.StatusCode() != http.StatusNoContent {
		return fmt.Errorf("could not create log records in rest api, invalid status code, want: %d, got: %d", http.StatusNoContent, res.StatusCode())
	}

	return nil
}

func (txl *RestAPITransactionLogger) AddMetadataRecords(ctx context.Context, recs []*MetadataRecord) error {
	records := metadataRecordsToRestLogs(recs)

	for k, v := range records {
		res, err := txl.client.CreateMetadataRecordsWithResponse(ctx, k.String(), models.CreateMetadataRecordsJSONRequestBody{
			Records: v,
		})

		if err != nil {
			return fmt.Errorf("could not create metadata records in rest api: %w", err)
		}

		if res.StatusCode() != http.StatusNoContent {
			return fmt.Errorf("could not create metadata records in rest api, invalid status code, want: %d, got: %d", http.StatusNoContent, res.StatusCode())
		}
	}

	return nil
}

func recordsToRestLogs(records []*Record) ([]models.LogRecord, error) {
	recs := make([]models.LogRecord, len(records))

	for i, r := range records {
		data, err := mapSourceAndDirection(r.Source, r.Destination)
		if err != nil {
			return nil, err
		}

		recs[i] = models.LogRecord{
			GroupId:       r.GroupID,
			TransactionId: r.TransactionID.String(),
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Direction:     mapDirection(r.Direction),
			Source:        data.Source,
			Destination:   data.Destination,
			CreatedAt:     r.CreatedAt.Unix(),
		}
	}

	return recs, nil
}

func mapSourceAndDirection(source, destination interface{}) (*models.LogRecord, error) {
	data := &models.LogRecord{}

	switch s := source.(type) {
	case *RecordSource:
		err := data.Source.FromSource(models.Source{
			Type:         models.SOURCETYPESOURCE,
			OutwayPeerId: s.OutwayPeerID,
		})
		if err != nil {
			return nil, err
		}

	case *RecordDelegatedSource:
		err := data.Source.FromSourceDelegated(models.SourceDelegated{
			Type:            models.SOURCETYPEDELEGATEDSOURCE,
			OutwayPeerId:    s.OutwayPeerID,
			DelegatorPeerId: s.DelegatorPeerID,
		})
		if err != nil {
			return nil, err
		}

	default:
		return nil, fmt.Errorf("unknown source type: %T", s)
	}

	switch d := destination.(type) {
	case *RecordDestination:
		err := data.Destination.FromDestination(models.Destination{
			Type:          models.DESTINATIONTYPEDESTINATION,
			ServicePeerId: d.ServicePeerID,
		})
		if err != nil {
			return nil, err
		}

	case *RecordDelegatedDestination:
		err := data.Destination.FromDestinationDelegated(models.DestinationDelegated{
			Type:            models.DESTINATIONTYPEDELEGATEDDESTINATION,
			ServicePeerId:   d.ServicePeerID,
			DelegatorPeerId: d.DelegatorPeerID,
		})
		if err != nil {
			return nil, err
		}

	default:
		return nil, fmt.Errorf("unknown destination type: %T", d)
	}

	return data, nil
}

func mapDirection(d record.Direction) models.LogRecordDirection {
	switch d {
	case record.DirectionIn:
		return models.LogRecordDirectionDIRECTIONINCOMING
	case record.DirectionOut:
		return models.LogRecordDirectionDIRECTIONOUTGOING
	default:
		return models.LogRecordDirectionDIRECTIONINCOMING
	}
}

func metadataRecordsToRestLogs(records []*MetadataRecord) map[TransactionID][]models.Metadata {
	recs := make(map[TransactionID][]models.Metadata)

	for _, r := range records {
		recs[*r.TransactionID] = append(recs[*r.TransactionID], models.Metadata{
			Direction:            mapMetadataDirection(r.Direction),
			GroupId:              r.GroupID,
			TransactionId:        r.TransactionID.String(),
			AdditionalProperties: r.Metadata,
		})
	}

	return recs
}

func mapMetadataDirection(d metadata.Direction) models.MetadataDirection {
	switch d {
	case metadata.DirectionOut:
		return models.MetadataDirectionDIRECTIONOUTGOING
	case metadata.DirectionIn:
		return models.MetadataDirectionDIRECTIONINCOMING
	default:
		return models.MetadataDirectionDIRECTIONINCOMING
	}
}
