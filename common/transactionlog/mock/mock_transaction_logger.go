// Code generated by mockery v2.42.3. DO NOT EDIT.

package mock_transaction_logger

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	transactionlog "gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
)

// MockTransactionLogger is an autogenerated mock type for the TransactionLogger type
type MockTransactionLogger struct {
	mock.Mock
}

type MockTransactionLogger_Expecter struct {
	mock *mock.Mock
}

func (_m *MockTransactionLogger) EXPECT() *MockTransactionLogger_Expecter {
	return &MockTransactionLogger_Expecter{mock: &_m.Mock}
}

// AddMetadataRecords provides a mock function with given fields: ctx, recs
func (_m *MockTransactionLogger) AddMetadataRecords(ctx context.Context, recs []*transactionlog.MetadataRecord) error {
	ret := _m.Called(ctx, recs)

	if len(ret) == 0 {
		panic("no return value specified for AddMetadataRecords")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, []*transactionlog.MetadataRecord) error); ok {
		r0 = rf(ctx, recs)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// MockTransactionLogger_AddMetadataRecords_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'AddMetadataRecords'
type MockTransactionLogger_AddMetadataRecords_Call struct {
	*mock.Call
}

// AddMetadataRecords is a helper method to define mock.On call
//   - ctx context.Context
//   - recs []*transactionlog.MetadataRecord
func (_e *MockTransactionLogger_Expecter) AddMetadataRecords(ctx interface{}, recs interface{}) *MockTransactionLogger_AddMetadataRecords_Call {
	return &MockTransactionLogger_AddMetadataRecords_Call{Call: _e.mock.On("AddMetadataRecords", ctx, recs)}
}

func (_c *MockTransactionLogger_AddMetadataRecords_Call) Run(run func(ctx context.Context, recs []*transactionlog.MetadataRecord)) *MockTransactionLogger_AddMetadataRecords_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].([]*transactionlog.MetadataRecord))
	})
	return _c
}

func (_c *MockTransactionLogger_AddMetadataRecords_Call) Return(_a0 error) *MockTransactionLogger_AddMetadataRecords_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *MockTransactionLogger_AddMetadataRecords_Call) RunAndReturn(run func(context.Context, []*transactionlog.MetadataRecord) error) *MockTransactionLogger_AddMetadataRecords_Call {
	_c.Call.Return(run)
	return _c
}

// AddRecords provides a mock function with given fields: ctx, recs
func (_m *MockTransactionLogger) AddRecords(ctx context.Context, recs []*transactionlog.Record) error {
	ret := _m.Called(ctx, recs)

	if len(ret) == 0 {
		panic("no return value specified for AddRecords")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, []*transactionlog.Record) error); ok {
		r0 = rf(ctx, recs)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// MockTransactionLogger_AddRecords_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'AddRecords'
type MockTransactionLogger_AddRecords_Call struct {
	*mock.Call
}

// AddRecords is a helper method to define mock.On call
//   - ctx context.Context
//   - recs []*transactionlog.Record
func (_e *MockTransactionLogger_Expecter) AddRecords(ctx interface{}, recs interface{}) *MockTransactionLogger_AddRecords_Call {
	return &MockTransactionLogger_AddRecords_Call{Call: _e.mock.On("AddRecords", ctx, recs)}
}

func (_c *MockTransactionLogger_AddRecords_Call) Run(run func(ctx context.Context, recs []*transactionlog.Record)) *MockTransactionLogger_AddRecords_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].([]*transactionlog.Record))
	})
	return _c
}

func (_c *MockTransactionLogger_AddRecords_Call) Return(_a0 error) *MockTransactionLogger_AddRecords_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *MockTransactionLogger_AddRecords_Call) RunAndReturn(run func(context.Context, []*transactionlog.Record) error) *MockTransactionLogger_AddRecords_Call {
	_c.Call.Return(run)
	return _c
}

// NewMockTransactionLogger creates a new instance of MockTransactionLogger. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewMockTransactionLogger(t interface {
	mock.TestingT
	Cleanup(func())
}) *MockTransactionLogger {
	mock := &MockTransactionLogger{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
