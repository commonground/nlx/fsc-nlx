// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package transactionlog

import "context"

type TransactionLogger interface {
	AddRecords(ctx context.Context, recs []*Record) error
	AddMetadataRecords(ctx context.Context, recs []*MetadataRecord) error
}
