// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package transactionlog

import (
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/metadata"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

// Record encompasses the data stored in the transactionlog for a single recorded transaction.
type Record struct {
	TransactionID *TransactionID
	GroupID       string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	Data          map[string]interface{} // TODO
	CreatedAt     time.Time
}

type RecordSource struct {
	OutwayPeerID string
}

type RecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type RecordDestination struct {
	ServicePeerID string
}

type RecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

type TransactionID uuid.UUID

type MetadataRecord struct {
	TransactionID *TransactionID
	GroupID       string
	Direction     metadata.Direction
	Metadata      map[string]map[string]interface{}
}

func (l *TransactionID) String() string {
	return uuid.UUID(*l).String()
}

func NewTransactionID() (*TransactionID, error) {
	u, err := uuid.NewV7()
	if err != nil {
		return nil, errors.Wrap(err, "could not generate new transaction id")
	}

	l := TransactionID(u)

	return &l, nil
}

func NewTransactionIDFromString(t string) (*TransactionID, error) {
	id, err := record.NewTransactionIDFromString(t)
	if err != nil {
		return nil, errors.Wrap(err, "could not parse transaction id from string")
	}

	l := TransactionID(*id)

	return &l, nil
}
