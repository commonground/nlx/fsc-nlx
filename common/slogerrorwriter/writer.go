// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package slogerrorwriter

import "log/slog"

type Writer struct {
	logger *slog.Logger
}

func New(logger *slog.Logger) *Writer {
	return &Writer{logger: logger}
}

func (a *Writer) Write(p []byte) (n int, err error) {
	a.logger.Error(string(p))
	return len(p), nil
}
